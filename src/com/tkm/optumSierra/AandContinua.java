package com.tkm.optumSierra;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import com.tkm.optumSierra.bean.ClasssPressure;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.dal.Weight_db;
import com.tkm.optumSierra.service.AdviceService;
import com.tkm.optumSierra.service.BluetoothService;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.FontsOverride;
import com.tkm.optumSierra.util.Util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AandContinua extends Titlewindow {

	TextView a;
	final int REQUEST_ENABLE_BT = 1;
	BluetoothAdapter mBluetoothAdapter;
	String x = BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE;
	private static final String NAME = "PWAccessP";
	private static final UUID MY_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");
	boolean flag = false;
	private int mPort = -1;
	public String resultValue;
	ByteArrayOutputStream mPacket = new ByteArrayOutputStream(128);
	boolean mPDU_valid = true;
	static final String HEXES = "0123456789ABCDEF";
	private BluetoothServerSocket mmServerSocket;
	boolean started = false;
	boolean is_paired = false;

	private static final boolean D = true;
	private String TAG = "AandDcontinua";
	public static final String PREFS_NAME = "DroidPrefBPSierra";
	public static final String PREFS_NAME_SCHEDULE = "DroidPrefSc";
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	public Date SavedDate = new Date();
	public static final String SAVED_SERVER = "saved_server";

	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";
	public String result_message;
	public int back = 0;
	public String sys, dia, pulses;
	boolean flage = false;
	boolean flage_previous_data = false;
	TextView txtwelcom, txtReading;
	ImageView rocketImage;
	private int mPortNumber = 0;
	private BluetoothAdapter mBluetoothAdaptere = null;
	private BluetoothService mBluetoothService = null;
	private TextToSpeech tts;
	public String resultValuee;
	AnimationDrawable rocketAnimation;
	ByteArrayOutputStream mPackete = new ByteArrayOutputStream(128);
	boolean mPDU_valide = true;
	ObjectAnimator AnimPlz, ObjectAnimator;

	Weight_db dbcreateweight = new Weight_db(this);
	Pressure_db dbcreatepressure = new Pressure_db(this);
	int lastinsert_id = 0, fg = 0;

	private static final int REQUEST_ENABLE_BTE = 1;
private GlobalClass appClass;
	BluetoothAdapter bluetoothAdapter;

	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.US);
			tts.setSpeechRate(0);
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				
			} else {
			}
		} else {
			Log.e(TAG, "TTS Initilization Failed");
		}

		

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//stopService(new Intent(AandContinua.this, AdviceService.class));
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		mPortNumber = settings.getInt("PortNumber", -1);		
			
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_aand_dreader);
		mBluetoothAdaptere = BluetoothAdapter.getDefaultAdapter();
		appClass  = (GlobalClass)getApplicationContext();
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = false;
		Button btnmanual = (Button) findViewById(R.id.manuval);
		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");

		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);

		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		rocketImage.setVisibility(View.INVISIBLE);
		txtReading.setVisibility(View.INVISIBLE);

		Constants.setPIN("39121440");

		txtwelcom.setText(this.getString(R.string.enterpressurebluetooth));

		if (!Constants.getcontinua().equals("1")) {
			
			SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
			String languageId = settingsUserSp.getString("language_id", "0");
			if(languageId.equals("11")){
				
				Util.soundPlay("Messages/TakeBP_1.wav");
			}else{
				
				Util.soundPlay("Messages/TakeBP.wav");
			}
			

		}
		setwelcome();

		final ViewGroup mContainer = (ViewGroup) findViewById(
				android.R.id.content).getRootView();
		FontsOverride.overrideFonts(getApplicationContext(), mContainer);

		btnmanual.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Intent intentpl = new Intent(getApplicationContext(),
						PressureEntry.class);
				finish();
				startActivity(intentpl);
			}
		});

		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdaptere == null) {
			Log.i(TAG, "A and D ci Bluetooth is not available");
			finish();
			return;
		}

		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		CheckBlueToothState();
		getPairedDevices();

		registerReceiver(ActionFoundReceiver, new IntentFilter(
				BluetoothDevice.ACTION_FOUND));

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	private void getPairedDevices() {
		Set<BluetoothDevice> pairedDevice = mBluetoothAdaptere
				.getBondedDevices();
		if (pairedDevice.size() > 0) {
			Log.d(TAG, "get paired devices list");
			for (BluetoothDevice device : pairedDevice) {
				if (device.getName().contains("A&D UA-767PBT-Ci")) {
					is_paired = true;
					Log.i(TAG, "Device already  paired with the sensor");

				}
			}

		}
		if (!is_paired) {
			Log.i(TAG,
					"Device not paired with the sensor calling autp pair request");
			devicecheck();
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(ActionFoundReceiver);
	}

	private void CheckBlueToothState() {
		if (bluetoothAdapter == null) {
		} else {
			if (bluetoothAdapter.isEnabled()) {
				if (bluetoothAdapter.isDiscovering()) {
				} else {
				}
			} else {
				Intent enableBtIntent = new Intent(
						BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBtIntent, REQUEST_ENABLE_BTE);
			}
		}
	}

	public void devicecheck() {
		bluetoothAdapter.startDiscovery();
	}

	private final BroadcastReceiver ActionFoundReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				String action = intent.getAction();
				if (BluetoothDevice.ACTION_FOUND.equals(action)) {
					BluetoothDevice device = intent
							.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
					if (device.getName().contains("A&D UA-767PBT-Ci")) {
						try {
							Constants.setcontinua("1");
							Method method = device.getClass().getMethod(
									"createBond", (Class[]) null);
							method.invoke(device, (Object[]) null);
							Handler tim = new Handler();
							tim.postDelayed(new Runnable() {
								@Override
								public void run() {
									Log.i(TAG, "Device paired with the sensor");
									Intent intente = getIntent();
									overridePendingTransition(0, 0);
									intente.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
									finish();

									overridePendingTransition(0, 0);
									startActivity(intente);
								}
							}, 15000);

						} catch (Exception e) {
							Log.i(TAG,
									"Exception in Device paired with the sensor");

						}
					}
				}
			} catch (Exception e) {
				Log.e(TAG,
						"Exception  onReceive in Device paired with the sensor");

			}
		}
	};

	private void setwelcome() {

		String th = "";
		String deviceMessage = "";

		if (preferenceWelcome(0)) {

			deviceMessage = this.getString(R.string.enterpressurebluetooth);
			txtwelcom.setText(deviceMessage);

		} else {

			th = this.getString(R.string.enterpressurebluetooth);
			txtwelcom.setText(th);

		}
		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
		AnimPlz.setDuration(1000);
		AnimPlz.start();

		AnimPlz.addListener(new AnimatorListenerAdapter() {
			public void onAnimationEnd(Animator animation) {

				txtReading.setVisibility(View.VISIBLE);
				rocketImage.setVisibility(View.VISIBLE);

			}
		});

	}

	public Boolean preferenceWelcome(int mode) {
		Boolean flage = null;
		try {
			SharedPreferences settings = getSharedPreferences(
					PREFS_NAME_SCHEDULE, 0);
			int scheduletatus = settings.getInt("welcome", -1);
			if (mode == 1) {
				SharedPreferences.Editor editor = settings.edit();
				editor.putInt("welcome", 1);
				editor.commit();
			}
			if (scheduletatus == 0)

				flage = true;
			else
				flage = false;
		}

		catch (Exception e) {
			Log.e("A and D ci - Error", e.getMessage());
		}
		return flage;
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.e(TAG, "+++++++++++++++ on stop  continua+++++++++++++++++++");
		Constants.setcontinua("0");
		try {
			mBluetoothService.stop();

		} catch (Exception ec) {

		}
		/*--------------------------------------------*/
		try {
			if (mmServerSocket != null) {
				mmServerSocket.close();
			}
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (NullPointerException e) {
		}
		try {
			if (mmServerSocket != null) {
				mmServerSocket.close();
			}
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (NullPointerException e) {
		}

		if (!isMyAdviceServiceRunning()) {

			/*Intent schedule = new Intent();
			schedule.setClass(AandContinua.this, AdviceService.class);
			startService(schedule); // Starting Advice Service*/

		}
		
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.i(TAG, "+++++++++++++++ on resume  continua+++++++++++++++++++");
		Method m, n;
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		try {

			boolean disc_timeout = android.provider.Settings.System.putInt(
					getBaseContext().getContentResolver(),
					"bluetooth_discoverability_timeout", 0);

			Log.e("BluetoothService",
					(new StringBuilder())
							.append("Could not set discoverability timeout to ")
							.append(disc_timeout).append(" seconds.")
							.toString());

			m = mBluetoothAdapter.getClass().getMethod(
					"setDiscoverableTimeout", int.class);
			m.invoke(mBluetoothAdapter, 300);

			n = mBluetoothAdapter.getClass()
					.getMethod("getDiscoverableTimeout");
			int a = (Integer) n.invoke(mBluetoothAdapter);

			Log.d("discovrable time", "you r dicoverable for " + a);

			boolean Flag = false;
			Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
					.getBondedDevices();
			if (pairedDevices.size() > 0) {
				for (BluetoothDevice device : pairedDevices) {

					if (device.getName().contains("UA-767PBT-Ci")) { // "ANIL-PC",
						Flag = true;
						break;
					}
				}
			}

			if (!Flag) {
				if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
					Intent discoverableIntent = new Intent(
							BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
					discoverableIntent.putExtra(
							BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);

					startActivityForResult(discoverableIntent, 1);

				} else {
					if (!started) {
						AcceptThread aa = new AcceptThread();
						aa.start();
					}
				}
			} else {
				if (!started) {
					AcceptThread aa = new AcceptThread();
					aa.start();
				}
			}

		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == REQUEST_ENABLE_BTE) {
			CheckBlueToothState();
		} else {			
			switch (requestCode) {
			case 1:
				// When the request to enable Bluetooth returns
				Log.d("onactivityresult", "new thread created");
				AcceptThread aa = new AcceptThread();
				aa.start();
				started = true;

			}
		}

	}

	public synchronized void connected(BluetoothSocket socket) {

		String TAG = "connected";
		Log.d(TAG, "connected");

		ConnectedThread mConnectedThread = new ConnectedThread(socket);
		mConnectedThread.start();

	}

	private int getPortNr() {
		try {
			Field mSocketField = BluetoothServerSocket.class
					.getDeclaredField("mSocket");
			mSocketField.setAccessible(true);
			BluetoothSocket socket = (BluetoothSocket) mSocketField
					.get(mmServerSocket);
			mSocketField.setAccessible(false);

			Field mPortField = BluetoothSocket.class.getDeclaredField("mPort");
			mPortField.setAccessible(true);
			int port = (Integer) mPortField.get(socket);
			mPortField.setAccessible(false);

			Log.d("getport",
					"BluetoothListenThread:getPortNr: Listening Port: " + port);

			return port;
		} catch (Exception e) {
			Log.e("getpo rt", "SensorService: getPortNr ", e);
			return -1;
		}
	}

	@SuppressLint("NewApi")
	private class AcceptThread extends Thread {

		public AcceptThread() {

			BluetoothServerSocket tmp = null;

			if (mPort == -1) {
				try {
					tmp = mBluetoothAdapter
							.listenUsingInsecureRfcommWithServiceRecord(NAME,
									MY_UUID);
				} catch (IOException e) {
					// e.printStackTrace();
				}
				mmServerSocket = tmp;
				mPort = getPortNr();

			}

			Log.d("AcceptThread", "In AcceptThread mPort is " + mPort);

			Method m;
			try {
				m = mBluetoothAdapter.getClass().getMethod(
						"listenUsingInsecureRfcommOn",
						new Class[] { int.class });
				tmp = (BluetoothServerSocket) m
						.invoke(mBluetoothAdapter, mPort);
			} catch (NoSuchMethodException e) {
				// e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// e.printStackTrace();
			} catch (IllegalAccessException e) {
				// e.printStackTrace();
			} catch (InvocationTargetException e) {
				// e.printStackTrace();
			}

			mmServerSocket = tmp;
		}

		public void run() {
			BluetoothSocket socket = null;
			while (true) {
				try {
					Log.d("AcceptThread", "listening");
					socket = mmServerSocket.accept();
				} catch (IOException e) {
					Log.d("AcceptThread", "not listening");
					break;
				}
				if (socket != null) {
					connected(socket);
					try {
						mmServerSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					break;
				}
			}
		}

	}

	private class ConnectedThread extends Thread {
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;
		String TAG = "connected Thread";

		public ConnectedThread(BluetoothSocket socket) {
			Log.d(TAG, "create ConnectedThread" + socket);
			Log.d(TAG, "print socket " + socket.getRemoteDevice());
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;

		}

		public void run() {
			Log.i(TAG, "BEGIN mConnectedThread");

			// Keep listening to the InputStream while connected
			while (true) {
				try {
					int repeat = 0;
					while ((mmInStream.available() <= 0) && (repeat >= 0)) {
						try {
							Log.i(TAG, "inside repeat");
							Thread.sleep(500);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						repeat--;
					}

					byte[] buffer = new byte[1024];
					int bytes;

					bytes = mmInStream.read(buffer);
					Log.i(TAG, "Received  data from stream");

					Log.i(TAG,
							"Received " + bytes + " bytes" + buffer.toString());
					processInput(bytes, buffer);
					write("PWA4".getBytes());
				} catch (IOException e) {
					Log.e(TAG, "disconnected a and d");
					// connectionLost();
					break;
				}
			}
		}

		public void write(byte[] buffer) {
			try {

				mmOutStream.write(buffer);

			} catch (IOException e) {
				Log.e(TAG, "Exception during write");
			}
		}

	}

	public static String getHex(byte[] raw, int len) {
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(3 * raw.length);
		for (final byte b : raw) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4))
					.append(HEXES.charAt((b & 0x0F))).append(" ");
			if (--len == 0)
				break;
		}
		return hex.toString();
	}

	StringBuffer sb = new StringBuffer("");

	private void processInput(int len, byte[] rxpacket) {

		String TAG = " process";
		mPacket.write(rxpacket, 0, len);
		Log.d(TAG, "Hex data<<< " + getHex(rxpacket, len));
		DecimalFormat formatter = new DecimalFormat("00");

		byte H = 0;
		byte L = 0;
		byte M = 0;
		byte D = 0;
		byte hr = 0;
		byte mnt = 0;

		byte tM = 0;
		byte tD = 0;
		byte thr = 0;
		byte tmnt = 0;

		L = rxpacket[10];
		H = rxpacket[9];
		M = rxpacket[11];
		D = rxpacket[12];
		hr = rxpacket[13];
		mnt = rxpacket[14];

		tM = rxpacket[18];
		tD = rxpacket[19];
		thr = rxpacket[20];
		tmnt = rxpacket[21];
		String y1 = Integer.toHexString(H & 0XFF);
		String y2 = Integer.toHexString(L & 0XFF);

		String NewHex = y2 + y1;

		int year = Integer.parseInt(NewHex, 16);

		int month = Integer.parseInt(Integer.toHexString(M & 0XFF), 16);
		int Day = Integer.parseInt(Integer.toHexString(D & 0XFF), 16);
		int hrs = Integer.parseInt(Integer.toHexString(hr & 0XFF), 16);
		int mnts = Integer.parseInt(Integer.toHexString(mnt & 0XFF), 16);

		int tmonth = Integer.parseInt(Integer.toHexString(tM & 0XFF), 16);
		int tDay = Integer.parseInt(Integer.toHexString(tD & 0XFF), 16);
		int thrs = Integer.parseInt(Integer.toHexString(thr & 0XFF), 16);
		int tmnts = Integer.parseInt(Integer.toHexString(tmnt & 0XFF), 16);

		String measuredate = formatter.format(month) + formatter.format(Day)
				+ formatter.format(hrs) + formatter.format(mnts);
		String transmitteddate = formatter.format(tmonth)
				+ formatter.format(tDay) + formatter.format(thrs)
				+ formatter.format(tmnts);

		Log.i(TAG, "measur date.." + measuredate);
		Log.i(TAG, "transmitted date." + transmitteddate);

		if (year != 0) {

			if ((Integer.parseInt(transmitteddate) - Integer
					.parseInt(measuredate)) < 3) {
				flag = true;
				Log.i(TAG, "Got last data from A and D  continua...");
			} else {
				Log.i(TAG, "Reading previous data from A and D continua...");

			}

		}

		boolean ok = true;
		boolean done = false;
		int pktLen = 0;

		ByteArrayInputStream pdu = new ByteArrayInputStream(
				mPacket.toByteArray());

		int pktType = pdu.read() + (pdu.read() << 8);

		if (pktType != 2) {
			Log.e(TAG, "< Incorrect packet type");
			ok = false;
		}

		if (ok) {
			pktLen = pdu.read() + (pdu.read() << 8) + (pdu.read() << 16)
					+ (pdu.read() << 24);

			if ((pktLen + 60) == mPacket.size()) {
				done = true;
			} else if ((pktLen + 60) < mPacket.size()) {
				Log.d(TAG, "< Packet too big...pktLen=" + pktLen);
				ok = false;
			} else {
				Log.d(TAG, " Incomplete data from A and D continua..");
			}
		}

		if (ok) {
			if (done) {

				Log.d(TAG, "< type=" + pktType);
				Log.d(TAG, "< length=" + pktLen);

				byte reading[] = new byte[16];
				String dev = "";

				int devType = pdu.read() + (pdu.read() << 8);
				Log.d(TAG, "< devTap=" + devType);
				pdu.skip(1); // Skip the flag for now
				pdu.skip(7); // Skip the timestamp for now
				pdu.skip(7); // Skip the timestamp for now
				pdu.skip(6); // Skip the bdaddr for now
				pdu.skip(6); // Skip the bdaddr for now

				byte sn[] = new byte[16];
				pdu.read(sn, 0, 12); // Serial number

				pdu.skip(10); // Skip reserved
				pdu.skip(1); // Skip the battery status for now
				pdu.skip(1); // Skip reserved
				pdu.skip(1); // Skip firmware rev for now

				String strReading = null;

				if (devType == 767) { // BP
					// deviceTypeIn = 1;
					dev = "UA-767PBT";
					pdu.read(reading, 0, 10);
					strReading = new String(reading, 0, 9);
				} else {
					// deviceTypeIn = 0;
					dev = "UC-321PBT";
					pdu.read(reading, 0, 14);
					strReading = new String(reading, 0, 13);
				}

				String strSN = new String(sn, 0, 11);

				String dataResult = "";

				if (strReading.startsWith("80")) {
					int diaP = Integer.parseInt(
							"" + (char) strReading.charAt(4)
									+ (char) strReading.charAt(5), 16);
					int sysP = diaP
							+ Integer.parseInt("" + (char) strReading.charAt(2)
									+ (char) strReading.charAt(3), 16);
					int pulse = Integer.parseInt(
							"" + (char) strReading.charAt(6)
									+ (char) strReading.charAt(7), 16);
					Log.i(TAG, "(Sys-Dia)tolic P = " + sysP + " Diastolic P = "
							+ diaP + " Pluse " + pulse);

					dataResult = sysP + "," + diaP + "," + pulse;
					sb.append(dataResult);

					if (flag) {
						saveResult(dataResult);

						Intent intent = new Intent(getApplicationContext(),
								ShowPressureActivity.class);

						intent.putExtra("sys", sys);
						intent.putExtra("dia", dia);
						intent.putExtra("pulse", pulses);
						intent.putExtra("pressureid", lastinsert_id);
						startActivity(intent);
						finish();
					}

				} else {
					dataResult = "";
					Log.e(TAG, "Invalid BP Reading in A and D continua!!!");
				}

				Log.i(TAG, dev + " sn: " + strSN + " Reading: " + strReading);
				resultValue = strReading;

				mPacket.reset();
			}
		} else {
			mPDU_valid = false;
			mPacket.reset();
			Log.i(TAG, "Invalid PDU!!!");
		}

		try {
			pdu.close();
		} catch (IOException e) {
			Log.e(TAG, "Problem closing input stream.in A and D");
		}
	}

	private void saveResult(String result) {
		Log.i(TAG, "saving A and D continua bp data");

		String[] str = null;
		if (back == 1) {
			back = 0;
		} else {

			SharedPreferences settings1 = getSharedPreferences(
					CommonUtilities.USER_SP, 0);
			int PatientIdDroid = Integer.parseInt(settings1.getString(
					"patient_id", "-1"));

			if (result.trim().length() == 0) {

			} else {
				try {
					SharedPreferences flow = getSharedPreferences(
							CommonUtilities.USER_FLOW_SP, 0);
					int val = flow.getInt("flow", 0); // #1
					if (val == 1) {
						
						SharedPreferences section = getSharedPreferences(
								CommonUtilities.PREFS_NAME_date, 0);
						SharedPreferences.Editor editor_retake = section.edit();
						editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(this));
						editor_retake.commit();
					}
					str = result.split(",");
					
					SharedPreferences settings = getSharedPreferences(
							CommonUtilities.USER_TIMESLOT_SP, 0);
					String slot = settings.getString("timeslot", "AM");
					
					ClasssPressure pressure = new ClasssPressure();
					pressure.setPatient_Id(PatientIdDroid);

					pressure.setSystolic(Integer.parseInt(str[0]));
					pressure.setDiastolic(Integer.parseInt(str[1]));
					pressure.setPulse(Integer.parseInt(str[2]));
					pressure.setInputmode(0);
					pressure.setTimeslot(slot);
					
					SharedPreferences settings2 = getSharedPreferences(
							CommonUtilities.PREFS_NAME_date, 0);
					String sectiondate = settings2.getString("sectiondate", "0");
					pressure.setSectionDate(sectiondate);
					pressure.setPrompt_flag("4");
					lastinsert_id = dbcreatepressure.InsertPressure(pressure);

					Log.i(TAG, "inserting bp from in a and d");
					sys = "" + str[0];
					dia = "" + str[1];
					pulses = "" + str[2];

				} catch (Exception e) {
					Log.i(TAG,
							"Exception inserting bp from in a and d continua");

				}
			}

		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, " KEYCODE_BACK clicked: a and d");

			Intent intent = new Intent(this, FinalMainActivity.class);
			this.finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
}
