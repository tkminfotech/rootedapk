package com.tkm.optumSierra;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.tkm.optumSierra.bean.ClassWeight;
import com.tkm.optumSierra.bean.ClasssPressure;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.dal.Weight_db;
import com.tkm.optumSierra.service.AdviceService;
import com.tkm.optumSierra.service.BluetoothService;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.FontsOverride;
import com.tkm.optumSierra.util.PINReceiver;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AandDReader extends Titlewindow implements OnInitListener {

	// Debugging

	private static final boolean D = true;
	private String TAG = "AandDReaderActivity";
	public static final String PREFS_NAME = "DroidPrefBPSierra";
	public static final String PREFS_NAME_SCHEDULE = "DroidPrefSc";
	private final BroadcastReceiver mybroadcast = new PINReceiver();
	// Message types sent from the BluetoothService Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	public Date SavedDate = new Date();
	public static final String SAVED_SERVER = "saved_server";

	// Key names received from the BluetoothService Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";
	private int deviceType = -1;
	private int deviceTypeIn = -1;
	public String result_message;
	public int back = 0;
	public String sys, dia, pulse;
	// Intent request codes
	private static final int REQUEST_ENABLE_BT = 1;
	boolean flag = false;
	boolean flag_previous_data = false;
	private String dataResult = "";
	// Layout Views
	TextView txtwelcom, txtReading;
	BackgroundThread backgroundThread;
	ImageView rocketImage;
	// Name of the connected device
	private String mConnectedDeviceName = null;
	private int mPortNumber = 0;
	// Array adapter for the trace thread
	// Local Bluetooth adapter
	private BluetoothAdapter mBluetoothAdapter = null;
	// Member object for the bluetooth services
	private BluetoothService mBluetoothService = null;
	private TextToSpeech tts;
	public String resultValue;
	AnimationDrawable rocketAnimation;
	// Input packet storage and processing
	ByteArrayOutputStream mPacket = new ByteArrayOutputStream(128);
	boolean mPDU_valid = true;
	ObjectAnimator AnimPlz, ObjectAnimator;

	Weight_db dbcreateweight = new Weight_db(this);
	Pressure_db dbcreatepressure = new Pressure_db(this);
	int lastinsert_id = 0;
	private GlobalClass appClass;
	
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.US);
			
			tts.setSpeechRate(0); // set speech speed rate
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				
			} else {
				// speakOut();
			}
		} else {
			Log.e(TAG, "TTS Initilization Failed");
		}
		backgroundThread = new BackgroundThread();
		backgroundThread.setRunning(true);
		backgroundThread.start();
		//stopService(new Intent(AandDReader.this, AdviceService.class));

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		appClass = (GlobalClass) getApplicationContext();		
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = false;
		
		
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			appClass.setBundle(extras);
		}else{
			extras = appClass.getBundle();
		}
		
		
		
		deviceType = extras.getInt("deviceType");
		tts = new TextToSpeech(this, this);

		// Log.i(TAG, "deviceType" + deviceType);
		if (D)
			Log.e(TAG, "+++ ON CREATE A and D +++");

		// Get the port number from previous connection
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		mPortNumber = settings.getInt("PortNumber", -1);
		if (D)
			Log.d(TAG, "mPortNumber in OnCreate is " + mPortNumber);
		setContentView(R.layout.activity_aand_dreader);
		// this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		Button btnmanual = (Button) findViewById(R.id.manuval);
		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");

		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);

		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		rocketImage.setVisibility(View.INVISIBLE);
		txtReading.setVisibility(View.INVISIBLE);
		setwelcomesound();
		Constants.setPIN("39121440");

		if (deviceType == 0) {

			txtwelcom.setText(this.getString(R.string.enterweightbluetooth));
			
			SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
			String languageId = settingsUserSp.getString("language_id", "0");
			if(languageId.equals("11")){
				
				Util.soundPlay("Messages/StepOnScale_1.wav");
			}else{
				
				Util.soundPlay("Messages/StepOnScale.wav");
			}
			
			
			
		} else if (deviceType == 1) {
			txtwelcom.setText(this.getString(R.string.enterpressurebluetooth));

			SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
			String languageId = settingsUserSp.getString("language_id", "0");
			if(languageId.equals("11")){
				
				Util.soundPlay("Messages/TakeBP_1.wav");
			}else{
				
				Util.soundPlay("Messages/TakeBP.wav");
			}

		}

		final ViewGroup mContainer = (ViewGroup) findViewById(
				android.R.id.content).getRootView();
		FontsOverride.overrideFonts(getApplicationContext(), mContainer);

		btnmanual.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click

				Log.i(TAG, "APP in a and d page" + deviceType);
				if (deviceType == 0) {
					Intent intentwt = new Intent(getApplicationContext(),
							WeightEntry.class);
					finish();
					startActivity(intentwt);
				} else if (deviceType == 1) {
					Intent intentpl = new Intent(getApplicationContext(),
							PressureEntry.class);
					finish();
					startActivity(intentpl);
				}
			}
		});

		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdapter == null) {
			Toast.makeText(this,
					this.getString(R.string.bluetoothnotavailable),
					Toast.LENGTH_LONG).show();

			Log.i(TAG, "Bluetooth is not available");
			finish();
			return;
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		// setwelcome();
		if (D)
			Log.e(TAG, "++ ON START A and D ++");

		// If BT is not on, request that it be enabled.
		// setupChat() will then be called during onActivityResult
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			// Otherwise, setup the chat session
		} else {
			if (mBluetoothService == null)
				setupTrace();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}
	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		if (D)
			Log.e(TAG, "+ ON RESUME A and D +");

		// Performing this check in onResume() covers the case in which BT was
		// not enabled during onStart(), so we were paused to enable it...
		// onResume() will be called when ACTION_REQUEST_ENABLE activity
		// returns.
		if (mBluetoothService != null) {
			// Only if the state is STATE_NONE, do we know that we haven't
			// started already
			if (mBluetoothService.getState() == BluetoothService.STATE_NONE) {
				// Start the Bluetooth chat services
				mBluetoothService.start();
			}
		} else {
			setupTrace();
		}

		/******************************* new code to improver performance ********************************************/

		Method m, n;
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		try {
			m = mBluetoothAdapter.getClass().getMethod(
					"setDiscoverableTimeout", new Class[] { int.class });
			m.invoke(mBluetoothAdapter, 300);

			n = mBluetoothAdapter.getClass()
					.getMethod("getDiscoverableTimeout");
			int a = (Integer) n.invoke(mBluetoothAdapter);

			Log.d("discovrable time", "you r dicoverable for " + a);
			/*
			 * if (mBluetoothAdapter.getScanMode() !=
			 * BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) { Intent
			 * discoverableIntent = new Intent(
			 * BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			 * discoverableIntent.putExtra(
			 * BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
			 * 
			 * startActivityForResult(discoverableIntent, 1);
			 * 
			 * }
			 */

		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/******************************* new code to improver Performance ********************************************/

		IntentFilter filter = new IntentFilter();
		filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
		registerReceiver(mybroadcast, filter);

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		unregisterReceiver(mybroadcast);

		super.onPause();

	}

	private void setupTrace() {

		// Initialize the BluetoothService to perform bluetooth connections
		mBluetoothService = new BluetoothService(this, mHandler, mPortNumber);
	}

	@Override
	public void onStop() {
		super.onStop();
		if (D)
			Log.e(TAG, "-- ON STOP A and D --");
		// We need an Editor object to make preference changes.
		// All objects are from android.context.Context
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.putInt("portNumber", mBluetoothService.getPort());
		editor.putInt("portNumber", 1);
		// Commit the edits!
		editor.commit();
		try {
			if (mBluetoothService != null) {
				mBluetoothService.stop();
				Log.e(TAG, "mBluetoothService.stoped ");
			}
		} catch (Exception e) {
			Log.e(TAG, "mBluetoothService.stop() failed ");

		}

		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}

		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");

		
			/*if (!isMyAdviceServiceRunning()) {
				Intent schedule = new Intent();
				schedule.setClass(AandDReader.this, AdviceService.class);
				startService(schedule); // Starting Advice Service
			}*/
		
	}

	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, " KEYCODE_BACK clicked: a and d");

			Intent intent = new Intent(this, FinalMainActivity.class);
			this.finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Bluetooth chat services
		try {
			if (mBluetoothService != null)
				mBluetoothService.stop();
		} catch (Exception e) {
			Log.e(TAG, "accept() failed on a and d");

		}
		if (D)
			Log.e(TAG, "--- ON DESTROY  a and d---");
	}

	private void ensureDiscoverable() {
		if (D)
			Log.d(TAG, "ensure discoverable");
		if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(
					BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			startActivity(discoverableIntent);
		}
	}

	/**
	 * Sends a message.
	 * 
	 * @param message
	 *            A string of text to send.
	 */
	private void sendMessage(String message) {
		// Check that we're actually connected before trying anything
		if (mBluetoothService.getState() != BluetoothService.STATE_CONNECTED) {
			/*
			 * Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
			 * .show();
			 */
			return;
		}

		// Check that there's actually something to send
		if (message.length() > 0) {
			Log.d(TAG, " mBluetoothService.write(send) ");
			// Get the message bytes and tell the BluetoothService to write
			byte[] send = message.getBytes();
			mBluetoothService.write(send);
		}
	}

	// The Handler that gets information back from the BluetoothService
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				if (D)
					Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					// mTitle.setText(mConnectedDeviceName);
					// mTraceArrayAdapter.clear();
					mPDU_valid = true;
					break;
				case BluetoothService.STATE_CONNECTING:
					// mTitle.setText(R.string.title_connecting);
					break;
				case BluetoothService.STATE_LISTEN:

					Log.d(TAG, "listening A and D");
					if (flag) {

						if (deviceType == 0 && deviceTypeIn == 0) {
							Intent intent = new Intent(AandDReader.this,
									ShowWightActivity.class);

							intent.putExtra("text", result_message);
							intent.putExtra("measureId", lastinsert_id);
							intent.putExtra("type", 0);
							Log.e(TAG, "redirecting from a and wt to show");
							try {
								mBluetoothService.stop();
							} catch (Exception ec) {

							}
							startActivity(intent);
							AandDReader.this.finish();

						} else if (deviceType == 1 && deviceTypeIn == 1) {
							Intent intent = new Intent(AandDReader.this,
									ShowPressureActivity.class);

							intent.putExtra("sys", sys);
							intent.putExtra("dia", dia);
							intent.putExtra("pulse", pulse);
							intent.putExtra("pressureid", lastinsert_id);
							Log.e(TAG, "redirecting from a and bp to show");
							try {
								mBluetoothService.stop();
							} catch (Exception ec) {

							}
							startActivity(intent);
							AandDReader.this.finish();
						} else {
							Log.e(TAG,
									"Another A and D device connecting to tablet");
							Toast.makeText(AandDReader.this,
									R.string.retrywithpropermeasurment,
									Toast.LENGTH_LONG).show();
						}

					}

					// mTitle.setText("listening");
					break;
				case BluetoothService.STATE_NONE:
					// mTitle.setText(R.string.title_not_connected);
					break;
				}
				break;
			case MESSAGE_WRITE:
				byte[] writeBuf = (byte[]) msg.obj;
				// construct a string from the buffer
				String writeMessage = new String(writeBuf);
				// mTraceArrayAdapter.add("> " + writeMessage);
				break;
			case MESSAGE_READ:
				// mTraceArrayAdapter.add("< "+mConnectedDeviceName+" "+msg.arg1+" bytes");
				if (mPDU_valid)

					processInput(msg.arg1, (byte[]) msg.obj);

				break;
			case MESSAGE_DEVICE_NAME:
				// save the connected device's name
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				/*
				 * Toast.makeText(getApplicationContext(),
				 * "Connected to A and D ", Toast.LENGTH_SHORT).show();
				 */
				break;
			case MESSAGE_TOAST:
				/*
				 * Toast.makeText(getApplicationContext(),
				 * msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
				 */
				break;
			}
		}
	};

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (D) {
			// Log.d(TAG, "onActivityResult " + resultCode);
			// Log.d(TAG, "requestCode is " + requestCode);
		}
		switch (requestCode) {
		case REQUEST_ENABLE_BT:
			// When the request to enable Bluetooth returns
			if (resultCode == Activity.RESULT_OK) {
				// Bluetooth is now enabled, so set up a chat session
				setupTrace();
			} else {
				// User did not enable Bluetooth or an error occured
				Log.d(TAG, "BT not enabled in A and D page");
				Toast.makeText(this,
						this.getString(R.string.bt_not_enabled_leaving),
						Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}

	// Convert a byte array to a hex string
	static final String HEXES = "0123456789ABCDEF";

	public static String getHex(byte[] raw, int len) {
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(3 * raw.length);
		for (final byte b : raw) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4))
					.append(HEXES.charAt((b & 0x0F))).append(" ");
			if (--len == 0)
				break;
		}
		return hex.toString();
	}

	// Process bytes arriving from the device
	private void processInput(int len, byte[] rxpacket) {
		mPacket.write(rxpacket, 0, len);
		Log.d(TAG, "Hex data<<< " + getHex(rxpacket, len));
		// Log.i(TAG, "Received " + rxpacket + " bytes");
		DecimalFormat formatter = new DecimalFormat("00");

		byte H = 0;
		byte L = 0;
		byte M = 0;
		byte D = 0;
		byte hr = 0;
		byte mnt = 0;

		byte tH = 0;
		byte tL = 0;
		byte tM = 0;
		byte tD = 0;
		byte thr = 0;
		byte tmnt = 0;

		L = rxpacket[10];
		H = rxpacket[9];
		M = rxpacket[11];
		D = rxpacket[12];
		hr = rxpacket[13];
		mnt = rxpacket[14];

		tL = rxpacket[17];
		tH = rxpacket[16];
		tM = rxpacket[18];
		tD = rxpacket[19];
		thr = rxpacket[20];
		tmnt = rxpacket[21];
		String y1 = Integer.toHexString(H & 0XFF);
		String y2 = Integer.toHexString(L & 0XFF);

		String NewHex = y2 + y1;

		int year = Integer.parseInt(NewHex, 16);

		int month = Integer.parseInt(Integer.toHexString(M & 0XFF), 16);
		int Day = Integer.parseInt(Integer.toHexString(D & 0XFF), 16);
		int hrs = Integer.parseInt(Integer.toHexString(hr & 0XFF), 16);
		int mnts = Integer.parseInt(Integer.toHexString(mnt & 0XFF), 16);

		String ty1 = Integer.toHexString(tH & 0XFF);
		String ty2 = Integer.toHexString(tL & 0XFF);

		String tNewHex = ty2 + ty1;
		int i = 00;

		int tyear = Integer.parseInt(tNewHex, 16);

		int tmonth = Integer.parseInt(Integer.toHexString(tM & 0XFF), 16);
		int tDay = Integer.parseInt(Integer.toHexString(tD & 0XFF), 16);
		int thrs = Integer.parseInt(Integer.toHexString(thr & 0XFF), 16);
		int tmnts = Integer.parseInt(Integer.toHexString(tmnt & 0XFF), 16);

		String measuredate = formatter.format(month) + formatter.format(Day)
				+ formatter.format(hrs) + formatter.format(mnts);
		String transmitteddate = formatter.format(tmonth)
				+ formatter.format(tDay) + formatter.format(thrs)
				+ formatter.format(tmnts);
		Log.i(TAG, "tyear.." + tyear);
		Log.i(TAG, "m year.." + year);
		Log.i(TAG, "measur date.." + measuredate);
		Log.i(TAG, "transmitted date." + transmitteddate);

		if (year != 0) {
			int a = Integer.parseInt(transmitteddate)
					- Integer.parseInt(measuredate);
			if ((Integer.parseInt(transmitteddate) - Integer
					.parseInt(measuredate)) < 3) {
				// Log.i(TAG, "inside .");
				flag_previous_data = true;
				Log.i(TAG, "Got last data from A and D ...");
				flag = true;
			} else {
				Log.i(TAG, "Reading previous data from A and D ...");
				/*
				 * Toast.makeText(getApplicationContext(),
				 * "Reading previous data  ", Toast.LENGTH_SHORT).show();
				 */
			}
			if (year < 2015) {
				Log.i(TAG, "measure date less than 2015...");
				flag = false;
				flag_previous_data = false;
			}
			// Log.i(TAG, "time diff.." + a + "");
		}

		boolean ok = true;
		boolean done = false;
		int pktLen = 0;

		ByteArrayInputStream pdu = new ByteArrayInputStream(
				mPacket.toByteArray());

		// Check the packet type...
		int pktType = pdu.read() + (pdu.read() << 8);

		if (pktType != 2) {
			Log.e(TAG, "< Incorrect packet type");
			ok = false;
		}

		// Check the packet length...
		if (ok) {
			pktLen = pdu.read() + (pdu.read() << 8) + (pdu.read() << 16)
					+ (pdu.read() << 24);

			if ((pktLen + 60) == mPacket.size()) {
				done = true;
			} else if ((pktLen + 60) < mPacket.size()) {
				Log.d(TAG, "< Packet too big...pktLen=" + pktLen);
				ok = false;
			} else {
				Log.d(TAG, " Incomplete data from A and D..");
			}
		}

		// Respond if we can...
		if (ok) {
			if (done) {

				Log.d(TAG, "< type=" + pktType);
				Log.d(TAG, "< length=" + pktLen);

				byte reading[] = new byte[16];
				String dev = "";

				int devType = pdu.read() + (pdu.read() << 8);
				Log.d(TAG, "< devTap=" + devType);
				pdu.skip(1); // Skip the flag for now
				pdu.skip(7); // Skip the timestamp for now
				pdu.skip(7); // Skip the timestamp for now
				pdu.skip(6); // Skip the bdaddr for now
				pdu.skip(6); // Skip the bdaddr for now

				byte sn[] = new byte[16];
				pdu.read(sn, 0, 12); // Serial number

				pdu.skip(10); // Skip reserved
				pdu.skip(1); // Skip the battery status for now
				pdu.skip(1); // Skip reserved
				pdu.skip(1); // Skip firmware rev for now

				String strReading = null;

				if (devType == 767) { // BP
					deviceTypeIn = 1;
					dev = "UA-767PBT";
					pdu.read(reading, 0, 10);
					strReading = new String(reading, 0, 9);

				} else {
					deviceTypeIn = 0;
					dev = "UC-321PBT";
					pdu.read(reading, 0, 14);
					strReading = new String(reading, 0, 13);

				}

				String strSN = new String(sn, 0, 11);
				if (deviceType == 1) {// pressure
					deviceTypeIn = 1;
					if (strReading.startsWith("80")) {
						int diaP = Integer.parseInt(
								"" + (char) strReading.charAt(4)
										+ (char) strReading.charAt(5), 16);
						int sysP = diaP
								+ Integer.parseInt(
										"" + (char) strReading.charAt(2)
												+ (char) strReading.charAt(3),
										16);
						int pulse = Integer.parseInt(
								"" + (char) strReading.charAt(6)
										+ (char) strReading.charAt(7), 16);
						// pulse = Integer.parseInt(strReading.substring(6,
						// 8),16);
						Log.i(TAG, "(Sys-Dia)tolic P = " + sysP
								+ " Diastolic P = " + diaP + " Pluse " + pulse);

						dataResult = sysP + "," + diaP + "," + pulse;
					} else {
						dataResult = "";
						Log.e(TAG, "Invalid BP Reading in A and D!!!");
						deviceTypeIn = -1;
					}
				} else { // weight
					dataResult = strReading.substring(4);
					//Log.e(TAG, "weight from a & d:-" + dataResult);

				}
				if (flag_previous_data) {
					saveResult(dataResult);
					sendMessage("PWA3");
					flag_previous_data = false;
					Log.d(TAG, "PWA3 sent to A and d");
				}
				// mTraceArrayAdapter.add(dev+" SerialNo: "+strSN);
				// mTraceArrayAdapter.add(dev+" Reading : "+strReading);
				//Log.i(TAG, dev + " sn: " + strSN + " Reading: " + strReading);
				resultValue = strReading;
				sendMessage("PWA1");
				Log.d(TAG, "PWA1 sent to A and d");

				mPacket.reset();
			}
		} else {
			mPDU_valid = false;
			mPacket.reset();
			// mTraceArrayAdapter.add("Invalid PDU!!!");
			Log.i(TAG, "Invalid PDU!!!");
			sendMessage("PWA0");
		}

		try {
			pdu.close();
		} catch (IOException e) {
			Log.e(TAG, "Problem closing input stream.in A and D");
		}
	}

	private void saveResult(String result) {
		Log.i(TAG, "saving A and D data");

		String[] str = null;
		if (back == 1) {
			back = 0;
		} else {
			// progressDialog.dismiss();
			Log.i(TAG, "PROGRESS_BAR_TYPE end ");
			ContentValues values = new ContentValues();
			SharedPreferences settings1 = getSharedPreferences(
					CommonUtilities.USER_SP, 0);
			int PatientIdDroid = Integer.parseInt(settings1.getString(
					"patient_id", "-1"));

			result = dataResult;

			String message = result;
			String ttmessage = "";
			if (result.trim().length() == 0) {
				message = this.getString(R.string.unabletoconnectbluetooth);
				ttmessage = this.getString(R.string.pleasetry);
				Log.i(TAG, "Unable to connect a and d Bluetooth  device!");
			} else {
				try {

					SharedPreferences flow = getSharedPreferences(
							CommonUtilities.USER_FLOW_SP, 0);
					int val = flow.getInt("flow", 0); // #1
					if (val == 1) {
						
						SharedPreferences section = getSharedPreferences(
								CommonUtilities.PREFS_NAME_date, 0);
						SharedPreferences.Editor editor_retake = section.edit();
						editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(this));
						editor_retake.commit();
					}
					
					SharedPreferences settings = getSharedPreferences(
							PREFS_NAME + deviceType, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putInt("portNumber", mPortNumber);
					editor.commit();
					Log.i(TAG, "Writing  portNumber to file " + PREFS_NAME
							+ " Port Number " + mPortNumber);
					if (TestDate.getCurrentTime().length() == 2) {
						getSavedTime1();
					}

					if (deviceType == 0 && deviceTypeIn == 0) { // weight
						// scale

						String wt = "";

						double weightKG = 0.0;
						if (result.contains("kg")) {
							wt = result.replace("kg", "");
							weightKG =  Util.round(Double.parseDouble(wt) * 2.204);
							// weightKG = Double.parseDouble(wt);
						} else {
							wt = result.replace("lb", "");
							weightKG =  Util.round(Double.parseDouble(wt));
						}

						// message = "Weight = " +result;
						ttmessage = result.replace("lb", "pounds");
						//Log.i(TAG, "wait inserting to db strat PatientIdDroid-"
						//		+ PatientIdDroid + " weight " + result);
						message = weightKG + "";
						result_message = message;
						ClassWeight weight = new ClassWeight();
						weight.setPatientID(PatientIdDroid);
						weight.setWeightInKg(weightKG + "");
						weight.setInputmode(0);

						SharedPreferences settingswt = getSharedPreferences(
								CommonUtilities.USER_TIMESLOT_SP, 0);
						String slot = settingswt.getString("timeslot", "AM");
						weight.setTimeslot(slot);
						SharedPreferences settings11 = getSharedPreferences(
								CommonUtilities.PREFS_NAME_date, 0);
						String sectiondate = settings11.getString(
								"sectiondate", "0");
						weight.setSectionDate(sectiondate);

						Log.i(TAG, "inserting weight from in a and d");
						lastinsert_id = dbcreateweight.InsertWeight(weight);

					} else if (deviceType == 1 && deviceTypeIn == 1) { // BP
						str = result.split(",");
						ClasssPressure pressure = new ClasssPressure();
						pressure.setPatient_Id(PatientIdDroid);

						pressure.setSystolic(Integer.parseInt(str[0]));
						pressure.setDiastolic(Integer.parseInt(str[1]));
						pressure.setPulse(Integer.parseInt(str[2]));
						pressure.setInputmode(0);
						SharedPreferences settings2 = getSharedPreferences(
								CommonUtilities.PREFS_NAME_date, 0);
						String sectiondate = settings2.getString("sectiondate",
								"0");
						
						SharedPreferences setting = getSharedPreferences(
								CommonUtilities.USER_TIMESLOT_SP, 0);
						String slot = settings.getString("timeslot", "AM");
						
						pressure.setSectionDate(sectiondate);
						pressure.setTimeslot(slot);
						pressure.setPrompt_flag("4");
						lastinsert_id = dbcreatepressure
								.InsertPressure(pressure);
						ttmessage = str[0] + R.string.over + str[1]
								+ this.getString(R.string.pulseis) + str[2];
						message = this.getString(R.string.syspressure) + " "
								+ str[0] + this.getString(R.string.diapressure)
								+ " " + str[1]
								+ this.getString(R.string.pulseequal) + " "
								+ str[2];
						Log.i(TAG, "inserting bp from in a and d");
						sys = "" + str[0];
						dia = "" + str[1];
						pulse = "" + str[2];
					}
				} catch (Exception e) {
					message = this.getString(R.string.invaliddata);
				}
			}

		}

	}

	private static final String PREFS_NAME_TIME = "TimePrefSc";

	public void getSavedTime1() {

		SharedPreferences settings = getSharedPreferences(PREFS_NAME_TIME, 0);
		String savedTime = settings.getString("timezone", "00");

		// Log.i(TAG, "inside getSavedTime fn; rest servertime " + savedTime);

		int year = Integer.parseInt(savedTime.substring(6, 10));
		int month = Integer.parseInt(savedTime.substring(0, 2));
		int day = Integer.parseInt(savedTime.substring(3, 5));
		int hour = Integer.parseInt(savedTime.substring(11, 13));
		int minute = Integer.parseInt(savedTime.substring(14, 16));
		int seconds = Integer.parseInt(savedTime.substring(17, 19));
		TestDate.setCalender(year, month, day, hour, minute, seconds);

		

	}

	public double round(double d) {
		DecimalFormat twoDForm = new DecimalFormat("#.#");
		return Double.valueOf(twoDForm.format(d));
	}

	public Boolean preferenceWelcome(int mode) {
		Boolean flag = null;
		try {
			SharedPreferences settings = getSharedPreferences(
					PREFS_NAME_SCHEDULE, 0);
			int scheduletatus = settings.getInt("welcome", -1);
			if (mode == 1) {
				SharedPreferences.Editor editor = settings.edit();
				editor.putInt("welcome", 1);
				editor.commit();
			}
			if (scheduletatus == 0)

				flag = true;
			else
				flag = false;
		}

		catch (Exception e) {
			Log.e("Droid Error", e.getMessage());
		}
		return flag;
	}

	public class BackgroundThread extends Thread {
		volatile boolean running = false;
		int cnt;

		void setRunning(boolean b) {
			running = b;
			cnt = 10;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while (running) {
				try {
					sleep(60);
					if (cnt-- == 0) {
						running = false;
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			handler.sendMessage(handler.obtainMessage());
		}
	}

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub

			setProgressBarIndeterminateVisibility(false);
			// progressDialog.dismiss();

			boolean retry = true;
			while (retry) {
				try {
					backgroundThread.join();
					retry = false;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			setwelcome();
			// setwelcome();
			// setContentView(R.layout.activity_black);
		}

	};

	private void setwelcome() {

		String th = "";
		String deviceMessage = "";

		if (preferenceWelcome(0)) {

			if (deviceType == 0) {
				if (preferenceWelcome(1)) {

					deviceMessage = this
							.getString(R.string.enterweightbluetooth);

					/*
					 * Util.soundPlay("Sierra/Hello.wav");
					 * 
					 * try { Thread.sleep(800); } catch (InterruptedException e)
					 * { // TODO Auto-generated catch block //
					 * e.printStackTrace(); }
					 */

					txtwelcom.setText(deviceMessage);

				}
			} else if (deviceType == 1) {
				// th = "Good Morning, ";

				deviceMessage = this.getString(R.string.enterpressurebluetooth);
				txtwelcom.setText(deviceMessage);
				/*
				 * Util.soundPlay("Sierra/GoodMorning.wav"); try {
				 * Thread.sleep(1000); } catch (InterruptedException e) { //
				 * TODO Auto-generated catch block e.printStackTrace(); }
				 * tts.speak(welcome+" .", TextToSpeech.QUEUE_FLUSH, null);
				 */
			}

		} else {
			if (deviceType == 0) {

				deviceMessage = this.getString(R.string.enterweightbluetooth);

				txtwelcom.setText(deviceMessage);

			} else if (deviceType == 1) {
				th = this.getString(R.string.enterpressurebluetooth);
				txtwelcom.setText(th);
			}
		}

		if (deviceType == 0) {
			// tts.speak(welcome, TextToSpeech.QUEUE_FLUSH, null);
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}

			// Util.soundPlay("Sierra/Please Step on the scale.wav");
		} else if (deviceType == 1) {

			// Util.soundPlay("Sierra/Please chek BP.wav");

		}

		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
		AnimPlz.setDuration(1000);
		AnimPlz.start();

		AnimPlz.addListener(new AnimatorListenerAdapter() {
			public void onAnimationEnd(Animator animation) {

				Log.e(TAG, "AnimPlz");

				txtReading.setVisibility(View.VISIBLE);
				rocketImage.setVisibility(View.VISIBLE);

			}
		});

	}

	private void setwelcomesound() {

		if (deviceType == 0) {
			// tts.speak(welcome, TextToSpeech.QUEUE_FLUSH, null);
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}

			Util.soundPlay("Sierra/Please Step on the scale.wav");
		} else if (deviceType == 1) {

			Util.soundPlay("Sierra/Please chek BP.wav");

		}

	}

}
