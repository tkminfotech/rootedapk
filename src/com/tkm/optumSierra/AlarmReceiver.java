package com.tkm.optumSierra;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassReminder;
import com.tkm.optumSierra.bean.ClassUser;
import com.tkm.optumSierra.dal.Flow_Status_db;
import com.tkm.optumSierra.dal.Login_db;
import com.tkm.optumSierra.dal.Reminder_db;
import com.tkm.optumSierra.dal.Skip_Reminder_Value_db;
import com.tkm.optumSierra.util.Alam_util;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.Skip_Reminder_Values_Upload;
import com.tkm.optumSierra.util.Util;

public class AlarmReceiver extends BroadcastReceiver {

	private GlobalClass appstate;	
	private int intervalLimit;
	private Login_db login_db;
	private Reminder_db reminder_db;
	public Skip_Reminder_Value_db reminder_skip_db;
	private int patientCount;
	Flow_Status_db Flow_db;
	Context context;
	String PatientIdDroid;
	String serverurl;

	@Override
	public void onReceive(Context context, Intent arg1) {

		Log.i("Alarm Receive",
				"----------------onReceive----------------------------");

		appstate = (GlobalClass) context.getApplicationContext();

		reminder_db = new Reminder_db(context);
		Flow_db = new Flow_Status_db(context);
		reminder_skip_db = new Skip_Reminder_Value_db(context);

		intervalLimit = reminder_db.GetDuration();
		login_db = new Login_db(context);
		serverurl = Constants.getPostUrl();
		ClassUser classUser = new ClassUser();

		classUser = login_db.SelectNickName();

		PatientIdDroid = String.valueOf(classUser.getPatientId());

		this.context = context;
		Cursor c = login_db.UserIsPresent(login_db.Patient_Table);
		patientCount = c.getCount();
		c.close();
		login_db.cursorLogin.close();
		login_db.close();

		double value = (double) intervalLimit / 5;
		intervalLimit = (int) Math.round(value);

		// Increment alarm count by 1 each time an alarm is fired.
		appstate.setRegular_alarm_count(appstate.getRegular_alarm_count() + 1);
		Log.e("ALARMRECEIVER", "COUNT======================================>>>"
				+ appstate.getRegular_alarm_count());

		// If count has reached the limit
		if (appstate.getRegular_alarm_count() == intervalLimit + 1) {
			Alam_util.cancelAlarm(context);

			Flow_db.open();
			Flow_db.deleteAll();
			Flow_db.insert("1");
			Flow_db.close();

			/*
			 * android.provider.Settings.System.putInt(
			 * context.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT,
			 * (60000 * 5));
			 */

			ClassReminder reminderObj = new ClassReminder();

			if (reminder_db.GetStartedAlarm() != null) {
				reminderObj = reminder_db.GetStartedAlarm();
				reminder_db.close();
				reminderObj.setStatus(2);
				reminder_db.UpdateReminderStatus(reminderObj);
				reminder_db.close();
			}
			Intent intent = new Intent(context, Reminder_Landing_Activity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);			

		} else {

			if (appstate.getRegular_alarm_count() <= intervalLimit) {

				Log.i("onReceive", "inide appstate.getAlarm_count() < "
						+ intervalLimit);
				if (android.os.Build.VERSION.SDK_INT >= 19) {
					Alam_util.startAlarm(context);
				}
				if (appstate.getRegular_alarm_count() == 1) {					

					if( appstate.isAlarmUtilStart)
						LoginMember(context);
					else
						createIntent(context);
					
					/*if (ApkType.equals("1000000")) // Sierra//
					{
						Intent intent = new Intent(context, CallActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

						SharedPreferences flow = context.getSharedPreferences(
								CommonUtilities.USER_FLOW_SP, 0);

						SharedPreferences.Editor seditor = flow.edit();
						seditor.putInt("flow", 0);
						seditor.putInt("reminder_path", 1);
						seditor.commit();
						context.startActivity(intent);
					} else if (ApkType.equals("1000001")) // HFP//
					{

						LoginMember(context);
					}*/
				} else {
					createIntent(context);
				}

			}

		}

	}

	/**
	 * Call intent to which ever activity is currently in the foreground state.
	 * 
	 * @param context
	 */
	private void createIntent(Context context) {

		ActivityManager am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

		String mClassName = cn.getClassName();
		System.out.println("CLASSS RECEIVIED===========>>> " + mClassName);
		if (mClassName.equals("com.tkm.optumSierra.QuestionView")) {

			Util.soundPlay("Questions/Q-" + Constants.get_question_id()
					+ ".wav");
		} else {
			if (mClassName.contains("com.tkm.optumSierra")) {
				Class<?> c = null;
				if (mClassName != null) {
					try {
						c = Class.forName(mClassName);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}

				Intent intent = new Intent(context, c);

				if (appstate.getBundle() != null) {
					intent.putExtras(appstate.getBundle());
				}
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
			} else {
				Log.e("Alarm_Receiver", "**********Optum Is Closed**********");
			}
		}

	}

	private void LoginMember(Context context) {

		SharedPreferences settings3 = context.getSharedPreferences(
				CommonUtilities.USER_SP, 0);
		int Patientid = Integer.parseInt(settings3.getString("patient_id", ""));
		Constants.setPatientid(Patientid);

		Log.i("Alarm_Receiver", "Diverting to HFPMainActivity");
		if (patientCount != 0) {
			String Condition = " type='All Day'";
			if (login_db.checkUserLogin(login_db.Patient_Table, Condition) == 0)
				// if 'All Day' user, then no need of chkuser
				chkuser(context);
			else {
				// 1
				Log.i("Alarm_Receiver",
						"Diverting to HFPMainActivity===========1");
				Intent intent = new Intent(context, CallActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
			}
		} else {
			Log.i("Alarm_Receiver",
					"Diverting to HFPMainActivity===========ELSE1");
			Alam_util.cancelAlarm(context);

			if (!PatientIdDroid.equals("0")) {
				reminder_skip_db.open();
				reminder_skip_db.insert(PatientIdDroid,
						Util.get_patient_time_zone_time(context), "0");
				reminder_skip_db.close();

				new Skip_Reminder_Values_Upload(context, serverurl).execute();

				Flow_db.open();
				Flow_db.deleteAll();
				Flow_db.insert("1");
				Flow_db.close();
			}
		}
	}

	private String timepicker() {

		String patient_time = Util
				.get_patient_time_zone_time_24_format(context);
		Integer AmorPm = Integer.parseInt(patient_time.substring(11, 13));

		String type = "";
		// SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

		// Date date = new Date();
		// Integer AmorPm =
		// Integer.parseInt(dateFormat.format(date).substring(0,2));
		if (AmorPm >= 15)
			type = "PM";
		else if (AmorPm <= 14)
			type = "AM";
		return type;
	}

	private void chkuser(Context context) {

		SharedPreferences settings3 = context.getSharedPreferences(
				CommonUtilities.USER_SP, 0);
		int Patientid = Integer.parseInt(settings3.getString("patient_id", ""));
		Constants.setPatientid(Patientid);

		String Condition = "";

		Condition = " type='AM/PM'";
		int type = 0;

		int cfampm = login_db.checkUserLogin(login_db.Patient_Table, Condition);

		if (cfampm == 0) {
			Condition = " type='" + timepicker() + "'";
			// if AM or PM user(not AM/PM user)
		} else {
			type = 1;
		}

		Log.i("Droid", "Condition : " + Condition);

		int patientLoginCount = login_db.checkUserLogin(login_db.Patient_Table,
				Condition);

		if (patientLoginCount == 0) {

			Log.i("Alarm_Receiver", "Invalid User in hfp reminder");
			Alam_util.cancelAlarm(context);

			reminder_skip_db.open();
			reminder_skip_db.insert(PatientIdDroid,
					Util.get_patient_time_zone_time(context), "0");
			reminder_skip_db.close();

			new Skip_Reminder_Values_Upload(context, serverurl).execute();

			Flow_db.open();
			Flow_db.deleteAll();
			Flow_db.insert("1");
			Flow_db.close();

		} else {

			int a = Util.hfp_vitals_taken_status(context, type);

			Log.e("Alarm_Receiver", "hfp_vitals_taken_status------ " + a);
			if (a != 0) {
				Log.i("Alarm_Receiver",
						"Diverting to HFPMainActivity from reminder");
				Intent intent = new Intent(context, CallActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
			} else {
				Log.i("Alarm_Receiver", "Already taken reading --skip reminder");
				Alam_util.cancelAlarm(context);

				reminder_skip_db.open();
				reminder_skip_db.insert(PatientIdDroid,
						Util.get_patient_time_zone_time(context), "0");
				reminder_skip_db.close();

				new Skip_Reminder_Values_Upload(context, serverurl).execute();

				Flow_db.open();
				Flow_db.deleteAll();
				Flow_db.insert("1");
				Flow_db.close();
			}

		}
	}

}
