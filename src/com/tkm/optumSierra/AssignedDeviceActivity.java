package com.tkm.optumSierra;

import java.io.StringReader;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.tkm.optumSierra.bean.ClassMeasureType;
import com.tkm.optumSierra.bean.ClassSensor;
import com.tkm.optumSierra.bean.PairedDevice;
import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.ConnectionDetector;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.SkipVitals;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.UploadVitalReadingsTask;
import com.tkm.optumSierra.util.Util;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class AssignedDeviceActivity extends Titlewindow implements OnClickListener{

	private String TAG = "-------Assigned Device Page-------------";
	private Sensor_db sensor_db = new Sensor_db(this); //
	MeasureType_db db_measureType = new MeasureType_db(this);
	Sensor_db dbSensor = new Sensor_db(this);
	private ArrayList<ClassSensor> sensorList = new ArrayList<ClassSensor>();
	private ArrayList<PairedDevice> pairedList = new ArrayList<PairedDevice>();
	private BluetoothAdapter bluetoothAdapter;
	private static final int REQUEST_ENABLE_BTE = 1;
	private ListView listview_vitals;
	private AssignedDeviceAdapter deviceAdapter;
	private String unPairedMacId = "";
	private int selPos = -1;
	private Button btn_testDevice;
	private Button btn_close;
	public String PostUrl = "";
	public static String serialNo = "";
	public static String imeilNo = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assigned_device);

		listview_vitals = (ListView) findViewById(R.id.listview_vitals);
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		btn_testDevice = (Button)findViewById(R.id.btn_testDevice);
		btn_close = (Button)findViewById(R.id.btn_vitalListClose);
		btn_testDevice.setOnClickListener(this);
		btn_close.setOnClickListener(this);
		SharedPreferences settingsUrl = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);		
		PostUrl = settingsUrl.getString("Server_post_url", "-1");
		SharedPreferences settingswifi = getSharedPreferences(CommonUtilities.WiFi_SP, 0);
		serialNo = settingswifi.getString("wifi", "-1");
		imeilNo = settingswifi.getString("imei_no", "");		

		

		
		fetchPairedDevices();
		


	}

	// method is responsible for only the display of the paired status of the sensor.
	// an instance of ClassSensor is created for each sensor. Each time this method is called, new instances are created for each sensor.
	// the pairedStatus field is always initialized to zero...so we'll only care about setting this field value to 1
	private void getSensorDetailsFromDB(){
		sensorList.clear();
		Cursor cursorsensor = dbSensor.SelectSensorDetails();
		cursorsensor.moveToFirst();

		int cnt = cursorsensor.getCount();
		if (cnt > 0) {

			while (cursorsensor.isAfterLast() == false) {

				ClassSensor classSensor = new ClassSensor();
				classSensor.setMeasureTypeId(cursorsensor.getInt(1));
				classSensor.setSensorName(
						(cursorsensor.getString(3)).contains("and") ? 
						(cursorsensor.getString(3)).replace(" and ", "&") :
						cursorsensor.getString(3));
				classSensor.setMacId(cursorsensor.getString(9));
				classSensor.setPin(cursorsensor.getString(10));
				classSensor.setItem_sn(cursorsensor.getString(11));

				Log.d(TAG, "getSensorDetailsFromDB: sensor name: " + cursorsensor.getString(3));
				
				if (cursorsensor.getString(3).contains("Taidoc")) {
					SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);						

					if(contract_cod.getString("taidoc_paired", "").equals("pair")){
						classSensor.setPairedStatus(1);

					}else{

						classSensor.setPairedStatus(0);
					}

				}

				if (cursorsensor.getString(3).toLowerCase().contains("fora")) {
					SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);

					Log.d(TAG, "getSensorDetailsFromDB: pair status: " + contract_cod.getString("fora_paired", ""));

					if(contract_cod.getString("fora_paired", "").equals("pair"))
						classSensor.setPairedStatus(1);

					else
						classSensor.setPairedStatus(0);
				}

				if (cursorsensor.getString(3).contains("3230") || cursorsensor.getString(3).contains("9560")) {
					SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);						

					if(contract_cod.getString("nion_paired", "").equals("pair")){
						classSensor.setPairedStatus(1);

					}else{

						classSensor.setPairedStatus(0);
					}

				}

				if (cursorsensor.getString(3).contains("P724")) {
					SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);						

					if(contract_cod.getString("p724weight_paired", "").equals("pair")){
						classSensor.setPairedStatus(1);

					}else{

						classSensor.setPairedStatus(0);
					}
				}
				for(int i=0;i<pairedList.size();i++){

					PairedDevice devices = pairedList.get(i);
					String macId = devices.getMacId().replace(":", "");
					String sensorName = devices.getSensorName();
					if(sensorName == null)
						sensorName = "";

					// this is actually necessary so as to not override the above configuration!
					if (cursorsensor.getString(3).contains("3230") || cursorsensor.getString(3).contains("9560")) {
					} else if (cursorsensor.getString(3).contains("Taidoc")) {
					} else if (cursorsensor.getString(3).toLowerCase().contains("fora")) {

					// Accu-Chek Guide contains 'meter+' in its Bluetooth name
					} else if (sensorName.contains("meter+") && cursorsensor.getString(3).contains("923")) {
						
							
							classSensor.setPairedStatus(1);
							break;						
						
					} if(sensorName.contains("Accu-Chek") && cursorsensor.getString(3).contains("Aviva")){
						
						classSensor.setPairedStatus(1);
						break;
					} else {
						if(cursorsensor.getString(9).equals(macId) ){	//  || cursorsensor.getString(3).contains(sensorName)

							classSensor.setPairedStatus(1);
							break;
						}
					}



				}	
				if(!(classSensor.getSensorName().contains("Manual") || classSensor.getSensorName().contains("question"))){

					sensorList.add(classSensor);
				}

				cursorsensor.moveToNext();
			}


		}

		cursorsensor.close();
		dbSensor.cursorsensor.close();
		dbSensor.close();

		deviceAdapter = new AssignedDeviceAdapter(this,R.layout.assigned_vitals_row,sensorList);
		listview_vitals.setAdapter(deviceAdapter);
		listview_vitals.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				selPos = position;
				showPopup(sensorList.get(position));

			}
		});
		
		SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
		int flow_type = flowsp.getInt("is_reminder_flow", 0);
		if (flow_type == 1)// skip for reminder
		{
			Log.e("TitleWindow", "During reminder flow user click on START/HOM- Inserting skip data");
			SkipVitals.skip_patient_vitalse(getApplicationContext());
			SharedPreferences.Editor seditor = flowsp.edit();
			seditor.putInt("is_reminder_flow", 0);
			seditor.commit();
		} else {
			Log.e("TitleWindow", "User click on START/HOME in normal flow.");
			SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
			String posturl = settings.getString("Server_post_url", "-1");
			new UploadVitalReadingsTask(this, posturl).execute();

		}
	}

	

	

	private void fetchPairedDevices(){
		Log.e(TAG, "---------------------------"+pairedList.size());
		pairedList.clear();
		if (bluetoothAdapter == null) {

		} else {
			if (bluetoothAdapter.isEnabled()) {
//				if (bluetoothAdapter.isDiscovering()) {
//
//				} else {

					BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
					Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

					if (pairedDevices.size() > 0) {
						for (BluetoothDevice device : pairedDevices) {
							PairedDevice pairedDevice = new PairedDevice();
							pairedDevice.setSensorName(device.getName());
							pairedDevice.setMacId(device.getAddress());
							pairedList.add(pairedDevice);		

						}
						Log.e(TAG, "enable---------------------------"+pairedList.size());
					}
//				}
			} else {
				bluetoothAdapter.enable();
				final Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
				  @Override
				  public void run() {
					 
					  BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
						Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

						if (pairedDevices.size() > 0) {
							for (BluetoothDevice device : pairedDevices) {
								PairedDevice pairedDevice = new PairedDevice();
								pairedDevice.setSensorName(device.getName());
								pairedDevice.setMacId(device.getAddress());
								pairedList.add(pairedDevice);		

							}
							Log.e(TAG, "not enable---------------------------"+pairedList.size());
						}	
				  }
				}, 1300);
				
			}
			
			final Boolean isInternetPresent = (new ConnectionDetector(
					getApplicationContext())).isConnectingToInternet();
			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
			  @Override
			  public void run() {
				  if (isInternetPresent) {

						loadSensorDetails();
					}else{

						getSensorDetailsFromDB();
					}
			  }
			}, 3000);
		}
		
	}

	//	private void pairDevice(BluetoothDevice device) {
	//		try {
	//			Log.d("pairDevice()", "Start Pairing...");
	//			Method m = device.getClass().getMethod("createBond", (Class[]) null);
	//			m.invoke(device, (Object[]) null);
	//			Log.d("pairDevice()", "Pairing finished.");
	//		} catch (Exception e) {
	//			Log.e("pairDevice()", e.getMessage());
	//		}
	//	}


	//	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
	//		public void onReceive(Context context, Intent intent) {
	//			String action = intent.getAction();
	//
	//			if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
	//				//discovery starts, we can show progress dialog or perform other tasks
	//			} else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
	//				//discovery finishes, dismis progress dialog
	//			} else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
	//				//bluetooth device found
	//				BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	//
	//				if(device.getAddress().replace(":", "").equals(unPairedMacId)){
	//
	//					pairDevice(device);
	//
	//				}
	//			}
	//		}
	//	};

	private void showPopup(final ClassSensor sensor){

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.testdevice_popup);
		dialog.setTitle("Custom Alert Dialog");


		ImageView txtClose          = (ImageView) dialog.findViewById(R.id.imgview_testDevicePopupClose);
		TextView txtPair          = (TextView) dialog.findViewById(R.id.txt_testDevicePopupPair);
		TextView txtUnpair          = (TextView) dialog.findViewById(R.id.txt_testDevicePopupUnpair);
		TextView txtTest         = (TextView) dialog.findViewById(R.id.txt_testDevicePopupTest);

		if(sensor.getPairedStatus() == 1){
			txtPair.setVisibility(View.GONE);	
			txtUnpair.setVisibility(View.VISIBLE);	
			txtTest.setVisibility(View.VISIBLE);	

		} else{

			txtPair.setVisibility(View.VISIBLE);	
			txtUnpair.setVisibility(View.GONE);	
			txtTest.setVisibility(View.GONE);	
		}

		dialog.show();
		txtClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		txtPair.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				vitalRedirection(sensor,"pair");
				dialog.dismiss();
			}
		});
		txtUnpair.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				unpairDevice(sensor);
				dialog.dismiss();
			}
		});
		txtTest.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				vitalRedirection(sensor,"test");
				dialog.dismiss();
			}
		});

	}

	//	private void pairDevice(int position){
	//
	//		ClassSensor sensor = sensorList.get(position);
	//
	//		unPairedMacId = sensor.getMacId();
	//		BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
	//		IntentFilter filter = new IntentFilter();
	//
	//		filter.addAction(BluetoothDevice.ACTION_FOUND);
	//		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
	//		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
	//
	//		registerReceiver(mReceiver, filter);
	//		adapter.startDiscovery();
	//	}

	private void redirectPulse(ClassSensor sensor,String testFlag) {


		String Sensor_name = sensor.getSensorName();
		String Mac = sensor.getMacId();


		//Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
		if (Mac.trim().length() == 12) {

			if (Sensor_name.contains("3230")) {
				if (Build.VERSION.SDK_INT >= 18) {
					Intent intent = new Intent(getApplicationContext(),
							TestDeviceBLECheckActivity.class);
					intent.putExtra("macaddress", Mac);
					intent.putExtra("Position", selPos);
					intent.putExtra("TestFlag", testFlag);
					startActivity(intent);
					finish();
				} else {

					Toast.makeText(getBaseContext(), "not support ble",
							Toast.LENGTH_SHORT).show();
					finish();
					return;
				}
			} else if (Sensor_name.contains("9560")) {
				Intent intent = new Intent(getApplicationContext(),
						TestDeviceDataReaderActivity.class);
				intent.putExtra("macaddress", Mac);
				intent.putExtra("Position", selPos);
				intent.putExtra("TestFlag", testFlag);
				this.finish();
				startActivity(intent);
			}

			overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("Manual")) {
			Intent intent = new Intent(getApplicationContext(),
					PulseEntry.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else {

			/*
			 * Toast.makeText(this, "Please assign Blood Oxygen.",
			 * Toast.LENGTH_LONG).show();
			 */

		}

	}

	private void redirectTemp(ClassSensor sensor,String testFlag) {

		String tmpMac = sensor.getMacId();

		if (tmpMac.trim().length() > 4) {
			Intent intentfr = new Intent(getApplicationContext(),
					TestDeviceForaMainActivity.class);
			intentfr.putExtra("deviceType", 2);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
			intentfr.putExtra("macaddress", getForaMAC(tmpMac));
			intentfr.putExtra("Position", selPos);
			intentfr.putExtra("TestFlag", testFlag);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);

		} else if (tmpMac.equals("-1")) {
			Intent intentfr = new Intent(getApplicationContext(),
					TemperatureEntry.class);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else {

			/*
			 * Toast.makeText(this, "Please assign Temperature .",
			 * Toast.LENGTH_LONG).show();
			 */

		}

	}

	private void redirectBP(ClassSensor sensor,String testFlag) {
		Log.i(TAG, "start to bp  activity from home : ");
		Cursor cursorsensor = dbSensor.SelectBPSensorName();
		String Sensor_nameDB = "", MacDB = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Sensor_nameDB = cursorsensor.getString(3);
			MacDB = getForaMAC(cursorsensor.getString(9));

		}
		String Sensor_name = sensor.getSensorName();
		String Mac;
		if(Sensor_name.equals(Sensor_nameDB)){

			Mac = MacDB;
		}else{

			Mac = sensor.getMacId();
		}


		if (Sensor_name.contains("A&D Bluetooth smart")) {
			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						TestDeviceAandDSmart.class);
				intent.putExtra("macaddress", Mac);
				intent.putExtra("Position", selPos);
				intent.putExtra("TestFlag", testFlag);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		} else if (Sensor_name.contains("Omron HEM 9200-T")) {
			if (Build.VERSION.SDK_INT >= 18) {
				Constants.setPIN(sensor.getPin());
				Intent intent = new Intent(getApplicationContext(),
						TestDeviceOmronBlsActivity.class);
				intent.putExtra("macaddress", Mac);
				intent.putExtra("TestFlag", testFlag);
				intent.putExtra("Position", selPos);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		} else if (Sensor_name.contains("P724-BP")) {

			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						TestDeviceP724BpSmart.class);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {
				Log.i(TAG,
						"SensorName P724 : no ble fund redirecting to normal page ");
				Intent intent = new Intent(this, TestDeviceP724Bp.class);
				intent.putExtra("Position", selPos);
				finish();
				startActivity(intent);
				return;
			}
		}

		else if (Sensor_name.contains("Wellex")) {

			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						TestDevicePressureWellex.class);
				intent.putExtra("macaddress", Mac);
				intent.putExtra("Position", selPos);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		}

		else if (Sensor_name.contains("A and D")) {

			if (Sensor_name.contains("UA-767BT-Ci")) {
				Intent intentwt = new Intent(getApplicationContext(),
						TestDeviceAandContinua.class);
				intentwt.putExtra("Position", selPos);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			} else {

				Intent intentwt = new Intent(getApplicationContext(),
						TestDeviceAandDReader.class);
				intentwt.putExtra("deviceType", 1);
				intentwt.putExtra("Position", selPos);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			}

		} else if (Sensor_name.contains("FORA")) {
			if (Mac.trim().length() == 17) {
				Intent intentfr = new Intent(getApplicationContext(),
						TestDeviceForaMainActivity.class);
				intentfr.putExtra("deviceType", 1); // pressure
				intentfr.putExtra("macaddress", Mac);
				intentfr.putExtra("Position", selPos);
				this.finish();
				startActivity(intentfr);
				overridePendingTransition(0, 0);
			} else if (Sensor_name.contains("Manual")) {
				Intent intentfr = new Intent(getApplicationContext(),
						PressureEntry.class);

				this.finish();
				startActivity(intentfr);
				overridePendingTransition(0, 0);

			} else {
				/*
				 * Toast.makeText(this, "Please assign Blood Pressure.",
				 * Toast.LENGTH_LONG).show();
				 */
			}
		} else if (Sensor_name.contains("Manual")) {
			Intent intentfr = new Intent(getApplicationContext(),PressureEntry.class);
			//Intent intentfr = new Intent(Home.this, OmronBlsActivity.class);

			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);

		} else {

			/*
			 * Toast.makeText(this, "Please assign Blood Pressure.",
			 * Toast.LENGTH_LONG).show();
			 */
		}

	}

	private void redirectWT(ClassSensor sensor,String testFlag) {

		Log.i(TAG, "start to wt  activity from home : ");
		Cursor cursorsensor = dbSensor.SelectWeightSensorName();
		String Sensor_name = "", Mac = "";
		Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {
			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));
		}
		//		String Sensor_name = sensor.getSensorName();
		//		String Mac = sensor.getMacId();

		if (Sensor_name.contains("A and D")) {
			Intent intentwt = new Intent(getApplicationContext(),
					TestDeviceAandDReader.class);
			intentwt.putExtra("deviceType", 0);
			intentwt.putExtra("Position", selPos);
			this.finish();
			startActivity(intentwt);
			overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("FORA")) {
			Intent intentfr = new Intent(getApplicationContext(),
					TestDeviceForaMainActivity.class);
			intentfr.putExtra("deviceType", 0);
			intentfr.putExtra("macaddress", Mac);
			intentfr.putExtra("Position", selPos);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("P724")) {
			Intent intentfr = new Intent(getApplicationContext(),
					TestDeviceP724Activity.class);
			intentfr.putExtra("deviceType", 0);
			intentfr.putExtra("macaddress", Mac);
			intentfr.putExtra("Position", selPos);
			intentfr.putExtra("TestFlag", testFlag);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else if (Sensor_name.trim().length() > 0) {
			final Context context = this;
			Intent intent = new Intent(context, WeightEntry.class);
			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);
		} else {

			/*
			 * Toast.makeText(this, "Please assign Body Weight.",
			 * Toast.LENGTH_LONG).show();
			 */

		}

	}

	private void redirectBG(ClassSensor sensor,String testFlag){

		String sensor_name = sensor.getSensorName();

		if (sensor_name.contains("One Touch Ultra")) {

			Intent intent = new Intent(getApplicationContext(),
					TestDeviceGlucoseReader.class);
			intent.putExtra("Position", selPos);
			intent.putExtra("TestFlag", testFlag);
			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Bayer")) {

			Intent intent = new Intent(getApplicationContext(),
					TestDeviceGlucoseReader.class);
			intent.putExtra("Position", selPos);
			intent.putExtra("TestFlag", testFlag);
			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Accu-Chek")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						TestDeviceGlucoseAccuChek.class);
				intent.putExtra("Position", selPos);
				intent.putExtra("TestFlag", testFlag);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				finish();
				startActivity(intent);

			}

		} else if (sensor_name.contains("Taidoc")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						TestDeviceGlucoseTaidoc.class);
				intent.putExtra("TestFlag", testFlag);
				intent.putExtra("Position", selPos);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();

				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				finish();
				startActivity(intent);

			}

		}

		else if (sensor_name.trim().length() > 0) {
			Intent intent = new Intent(getApplicationContext(),
					GlucoseEntry.class);
			finish();
			startActivity(intent);
		}

		dbSensor.cursorsensor.close();
		dbSensor.close();


	}
	private void vitalRedirection(ClassSensor sensor,String testFlag){

		int typeId = sensor.getMeasureTypeId();
		if(typeId == 1){

			redirectBG(sensor,testFlag);
		}else if(typeId == 2){

			redirectPulse(sensor,testFlag);
		}else if(typeId == 3){

			redirectBP(sensor,testFlag);
		}else if(typeId == 6){

			redirectTemp(sensor,testFlag);
		}else if(typeId == 7){

			redirectWT(sensor,testFlag);
		}
	}

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		//Log.i(TAG, " : Connect to macAddress " + macAddress);
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_testDevice:

			Intent testDeviceIntent = new Intent(getApplicationContext(), TestADeviceActivity.class);
			startActivity(testDeviceIntent);
			finish();
			break;
		case R.id.btn_vitalListClose:
			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);
			break;
		default:
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}


	private class DownloadSensorDetailsTask extends
	AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			Log.i(TAG, "onPostExecute SensorDetails");	
			
				  getSensorDetailsFromDB();
			 
			
		}

		protected String doInBackground(String... urls) {

			String response = "";
			try {
				//Log.i(TAG, "sensor request: "+PostUrl
				//		+ "/droid_website/sensor_details.ashx");
				// sensor_db.deleteplayorder();//
				HttpResponse response1 = Util.connect(PostUrl
						+ "/droid_website/sensor_details.ashx", new String[] {
								"serial_no", "imei_no" }, new String[] { serialNo,
										imeilNo });

				if (response1 == null) {

					Log.e(TAG, "Connection Failed!");

					return ""; // process
				}
				// Log.e("Optum_Sierra","serialNo"+serialNo);

				String str = Util
						.readStream(response1.getEntity().getContent());
				if (str.trim().length() == 0) {
					Log.i(TAG, " nnull Sensor details");
					sensor_db.delete();
					sensor_db.deleteplayorder();
					return "";
				}
				str = str.replace("&", "");
				//Log.i(TAG, "sensor response: "+str);
				DocumentBuilder dbr = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = dbr.parse(is);

				NodeList nodes = doc.getElementsByTagName("sensor");

				sensor_db.delete(); // deleting entire table
				sensor_db.deleteplayorder();//

				db_measureType.delete();
				ClassSensor sensor = new ClassSensor();

				String s_time = TestDate.getCurrentTime();
				SharedPreferences settings1 = getSharedPreferences(
						CommonUtilities.PREFS_NAME_date, 0);
				SharedPreferences.Editor editor_retake = settings1.edit();
				editor_retake.putString("sectiondate",
						Util.get_patient_time_zone_time(AssignedDeviceActivity.this));
				editor_retake.commit();

				for (int i = 0; i < nodes.getLength(); i++) {

					sensor.setPatientId(Integer.parseInt(Util.getTagValue(
							nodes, i, "patient_id")));
					sensor.setSensorId(Integer.parseInt(Util.getTagValue(nodes,
							i, "sensor_id")));
					sensor.setSensorName(Util.getTagValue(nodes, i,
							"sensor_name"));
					sensor.setMeasureTypeId(Integer.parseInt(Util.getTagValue(
							nodes, i, "measure_type_id")));
					sensor.setMeasureTypeName(Util.getTagValue(nodes, i,
							"measure_type_name"));
					sensor.setMacId((Util.getTagValue(nodes, i, "id")));
					sensor.setPin((Util.getTagValue(nodes, i, "sn")));
					sensor.setItem_sn((Util.getTagValue(nodes, i, "item_serial_number")));
					Element advice_idelement = (Element) nodes.item(i);
					NodeList advice_idname = advice_idelement
							.getElementsByTagName("weight_unit_id");

					if (advice_idname.getLength() > 0) {
						sensor.setWeightUnitId(Integer.parseInt(Util
								.getTagValue(nodes, i, "weight_unit_id")));
					} else {
						sensor.setWeightUnitId(0);
					}

					Element advice_idelement1 = (Element) nodes.item(i);
					NodeList advice_idname1 = advice_idelement1
							.getElementsByTagName("weight_unit_name");

					if (advice_idname1.getLength() > 0) {
						sensor.setWeightUnitName(Util.getTagValue(nodes, i,
								"weight_unit_name"));
					} else

					{
						sensor.setWeightUnitName("");
					}

					ContentValues values = new ContentValues();
					ContentValues valuesorder = new ContentValues();

					values.put("Patient_Id", sensor.getPatientId());

					values.put("Sensor_id", sensor.getSensorId());
					values.put("Sensor_name", sensor.getSensorName());
					values.put("Measure_type_id", sensor.getMeasureTypeId());
					values.put("Measure_type_name", sensor.getMeasureTypeName());
					values.put("Weight_unit_id", sensor.getWeightUnitId());
					values.put("Weight_unit_name", sensor.getWeightUnitName());
					values.put("Mac_id", sensor.getMacId());
					values.put("Status", 0);
					values.put("pin", sensor.getPin());
					values.put("item_sn", sensor.getItem_sn());
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					Date date = new Date();
					values.put("Download_Date", dateFormat.format(date));

					sensor_db.insertSensor(values);// inserting downloaded date

					int order = orderofvital(sensor.getMeasureTypeId());
					int homeorder = homeOrderofvital(sensor.getMeasureTypeId());
					ClassMeasureType sensor1 = new ClassMeasureType();
					sensor1.setMeasuretypeId(sensor.getMeasureTypeId());
					sensor1.setPatientId(sensor.getPatientId());
					sensor1.setStatus(0);
					sensor1.setDate(s_time);

					db_measureType.insert(sensor1);

					valuesorder.put("Patient_Id", sensor.getPatientId());
					valuesorder.put("Measure_type_name",
							sensor.getMeasureTypeName());
					valuesorder.put("Measure_type_id",
							sensor.getMeasureTypeId());
					valuesorder.put("playorder", order);
					valuesorder.put("Status", 0);
					valuesorder.put("homeOrder", homeorder);

					sensor_db.insertSensor_play_order(valuesorder);

					response = "Success";
					/*
					 * Log.i(TAG, "Sensor details...SensorId " +
					 * sensor.getSensorId() + " Meassure_type " +
					 * sensor.getMeasureTypeName() + " Sensor_name " +
					 * sensor.getSensorName() + " Patient_Id " +
					 * sensor.getPatientId() + " Status " + sensor.getStatus());
					 */

				}
				// close();
				sensor_db.close();
			} catch (Exception e) {
				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Sensor Details Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.Other_Upload_Download_Error);
			}

			return response;
		}
	}

	public void loadSensorDetails() {

		DownloadSensorDetailsTask task = new DownloadSensorDetailsTask();
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

	}

	private int orderofvital(int measure_type_id) {
		int a = 0;

		if (measure_type_id == 7) {
			a = 1;
		} else if (measure_type_id == 101) {
			a = 2;
		} else if (measure_type_id == 2) {
			a = 3;
		} else if (measure_type_id == 3) {
			a = 4;
		} else if (measure_type_id == 6) {
			a = 5;
		} else if (measure_type_id == 1) {
			a = 6;

		} else {
			a = 0;

		}

		return a;
	}

	// home page order
	private int homeOrderofvital(int measure_type_id) {
		int a = 0;

		if (measure_type_id == 7) {
			a = 1;
		} else if (measure_type_id == 101) {
			a = 3;
		} else if (measure_type_id == 2) {
			a = 5;
		} else if (measure_type_id == 3) {
			a = 4;
		} else if (measure_type_id == 6) {
			a = 2;
		} else if (measure_type_id == 1) {
			a = 6;

		}

		return a;
	}

	// contains the un-pairing logic. Some sensors are only 'logically' paired - meaning that they won't appear in the Bluetooth Settings paired device list.
	private void unpairDevice(ClassSensor sensor) {
		if (sensor.getSensorName().contains("3230") || sensor.getSensorName().contains("9560")) {

			SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);
			SharedPreferences.Editor contract_editor = contract_cod.edit();
			contract_editor.putString("nion_paired", "notpair");
			contract_editor.commit();
		}
		if (sensor.getSensorName().contains("Taidoc")) {

			SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);
			SharedPreferences.Editor contract_editor = contract_cod.edit();
			contract_editor.putString("taidoc_paired", "notpair");
			contract_editor.commit();
		}

		if (sensor.getSensorName().toLowerCase().contains("fora")) {
			SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);
			SharedPreferences.Editor contract_editor = contract_cod.edit();
			contract_editor.putString("fora_paired", "notPaired");
			contract_editor.apply();
		}

		if (sensor.getSensorName().contains("P724")) {

			SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);
			SharedPreferences.Editor contract_editor = contract_cod.edit();
			contract_editor.putString("p724weight_paired", "notpair");
			contract_editor.commit();
		}
		
		BluetoothDevice pairDevice = null;
		BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

		if (pairedDevices.size() > 0) {
			for (BluetoothDevice device : pairedDevices) {

				// some sensors on some tablets are stored in the system without a name, so this check 'sensor.getSensorName().contains(name)'
				// always returns true for those sensors. The result being a different sensor is unpaired.
				String name =  "Undefined Sensor";
				if(device.getName() != null){
					name = device.getName();
				}

				if(sensor.getSensorName().contains(name) || sensor.getMacId().equals(device.getAddress().replace(":", ""))){
					pairDevice = device;
					break;	

				// in case of Accu-Chek 923, the Bluetooth device name is "meter+{serial_number}"
				// so, check for it explicitly
				// the above MAC address check fails because in the case of Aviva/Guide, the MAC address is not stored
				} else if (sensor.getSensorName().contains("Accu-Chek") && name.contains("meter+")) {
					pairDevice = device;
					break;
				}
			}

		}
		if(pairDevice != null) {

			try {
				Method m = pairDevice.getClass()
						.getMethod("removeBond", (Class[]) null);
				m.invoke(pairDevice, (Object[]) null);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}

		}
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
		  @Override
		  public void run() {
			  fetchPairedDevices();
			  
			  final Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
				  @Override
				  public void run() {
					  getSensorDetailsFromDB();
					listview_vitals.deferNotifyDataSetChanged();
				  }
				}, 1800);
					
		  }
		}, 500);
		
		
	}

}
