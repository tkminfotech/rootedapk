package com.tkm.optumSierra;

import java.util.List;

import com.tkm.optumSierra.bean.ClassSensor;
import com.tkm.optumSierra.bean.PairedDevice;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AssignedDeviceAdapter extends ArrayAdapter<ClassSensor>{
	private final Context context;
	private List<ClassSensor> sensorList;
	public AssignedDeviceAdapter(Context context, int resource, List<ClassSensor> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		sensorList = objects;
		this.context = context;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.assigned_vitals_row, parent, false);
		TextView txt_sensorName = (TextView)rowView.findViewById(R.id.txt_sensorName);
		TextView txt_pairedFlag = (TextView)rowView.findViewById(R.id.txt_pairedFlag);
		TextView txt_macId = (TextView)rowView.findViewById(R.id.txt_macId);
		TextView txt_serialNum = (TextView)rowView.findViewById(R.id.txt_serialNum);
		ImageView imgview_tick = (ImageView)rowView.findViewById(R.id.imgview_tick);
		LinearLayout layout_rowBorder = (LinearLayout)rowView.findViewById(R.id.layout_rowBorder);
		ClassSensor sensor = sensorList.get(position);
		// Change icon based on name
		
		txt_sensorName.setText(sensor.getSensorName());
		if(sensor.getMacId().equals("-1")){
			
			txt_macId.setText(context.getResources().getString(R.string.bluetoothAddress));
		}else{
			
			txt_macId.setText(context.getResources().getString(R.string.bluetoothAddress)+"     "+sensor.getMacId());
		}				
		txt_serialNum.setText(context.getResources().getString(R.string.serial)+"    "+sensor.getItem_sn());
		if(sensor.getPairedStatus() == 1){
			imgview_tick.setBackgroundResource(R.drawable.tick_orange);
			txt_sensorName.setTextColor(context.getResources().getColor(R.color.oragne));
			txt_pairedFlag.setTextColor(context.getResources().getColor(R.color.oragne));			
			txt_pairedFlag.setText(""+context.getResources().getString(R.string.paired));
			layout_rowBorder.setBackgroundResource(R.drawable.border_orange_layout);
		}else{
			imgview_tick.setBackgroundResource(R.drawable.tick_gray);
			txt_sensorName.setTextColor(context.getResources().getColor(R.color.Mob_Gray5));
			txt_pairedFlag.setTextColor(context.getResources().getColor(R.color.Mob_Gray5));
			txt_pairedFlag.setText(""+context.getResources().getString(R.string.notPaired));
			layout_rowBorder.setBackgroundResource(R.drawable.border_gray_layout);
		}

		return rowView;
	}







}
