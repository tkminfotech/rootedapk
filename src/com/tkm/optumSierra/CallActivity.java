package com.tkm.optumSierra;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.tkm.optumSierra.util.CommonUtilities;

public class CallActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_call);		

		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);

		SharedPreferences.Editor seditor = flow.edit();
		seditor.putInt("flow", 0);
		seditor.putInt("reminder_path", 1);
		seditor.putInt("is_reminder_flow", 1);
		seditor.commit();
		
			Intent intent = new Intent(this, HFPMainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
		

	}

}
