package com.tkm.optumSierra;


import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Log;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class DisclaimerActivity extends Titlewindow implements OnClickListener{

	private WebView webview_disclaimerNote;
	private Button btn_disclaimerOK;
	public static String imeilNo = "";
	public static String PostUrl = "";
	private GlobalClass appClass;
	Bundle extras;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_disclaimer);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isEllipsisEnable = true;
		webview_disclaimerNote = (WebView)findViewById(R.id.webview_disclaimerNote);		
		btn_disclaimerOK = (Button)findViewById(R.id.btn_disclaimerOK);
		btn_disclaimerOK.setOnClickListener(this);	
		extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		
		SharedPreferences settings = getSharedPreferences(CommonUtilities.DISCLAIMER_SP, 0);
		String disclaimerNote = settings.getString("disclaimer_note", "").
				replace(".:;", "\"").replace("http", "#");		
		webview_disclaimerNote.setBackgroundColor(0);
		webview_disclaimerNote.loadDataWithBaseURL("", disclaimerNote, "text/html", "UTF-8", "");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_disclaimerOK:
			redirectActivity();
			break;

		default:
			break;
		}
	}

	private void redirectActivity(){
		
		if(extras.getString("nextFlow").equals("1")){
			
			Intent intent = new Intent(DisclaimerActivity.this,
					PopActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			
			startActivity(intent);
		}else{
			
			SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
			int val = flowsp.getInt("flow", 0); 
			if(val == 1){

				Intent intentSc = new Intent(getApplicationContext(),
						Home.class);
				finish();
				startActivity(intentSc);
				overridePendingTransition(0, 0);
			}else{
				Log.e("********************DisclaimerActivity", "*********************Welcome_Activity**************");
				
				Intent intent = new Intent(DisclaimerActivity.this, Welcome_Activity.class);
				startActivity(intent);
				overridePendingTransition(0, 0);
			}
		}
			
	}
	
}
