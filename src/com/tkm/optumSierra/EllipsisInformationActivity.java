package com.tkm.optumSierra;

import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.CustomKeyboard;
import com.tkm.optumSierra.util.OnInit_Data_Access;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import com.tkm.optumSierra.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EllipsisInformationActivity extends Activity{
	String mobile_network_type = "UNKNOWN";	
	TelephonyManager telephonyManager;
	MyPhoneStateListener psListener;
	CustomKeyboard mCustomKeyboard;
	EditText edtPasscode;
	String passcode = "";
	private String simType;
	public TextView txt_signalStrength;
	//public TextView txt_signalStrengthdBmAsu;
	private LinearLayout layout_passcode;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

		// ...but notify us that it happened.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
		this.setFinishOnTouchOutside(false);
		setContentView(R.layout.information_popup);
		TextView txt_imei = (TextView) findViewById(R.id.txt_imei);
		TextView version = (TextView) findViewById(R.id.txt_version);
		TextView udi = (TextView) findViewById(R.id.txt_udi);
		txt_signalStrength = (TextView) findViewById(R.id.txt_signalStrength);
		txt_signalStrength.setText("0");
//		txt_signalStrengthdBmAsu = (TextView) findViewById(R.id.txt_signalStrengthIndBmAsu);
//		txt_signalStrengthdBmAsu.setText("0 dBm  99 asu");
		SharedPreferences settingseifi = getSharedPreferences(CommonUtilities.WiFi_SP, 0);

		
		String imeilNo = settingseifi.getString("imei_no", "");
		if (imeilNo.trim().length() > 5) {
			if (imeilNo.trim().length() > 14) {
				imeilNo = imeilNo.substring(0, 14);				
			}
		}	
		txt_imei.setText(imeilNo);
		findSignalStrength();
		Button informationClose = (Button) findViewById(R.id.btn_infromationClose);
		informationClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				finish();
			}
		});
	}

	private void findSignalStrength() {
		boolean isWifi 		= false;
		boolean isMobile 	= true;	
		try {
			
		

		ConnectivityManager connectivitymanager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
		NetworkInfo[] networkInfo = connectivitymanager.getAllNetworkInfo();

		for(NetworkInfo netInfo : networkInfo)
		{
			//			isWifi 		= false;
			//			isMobile 	= false;

			if(netInfo.isAvailable() && netInfo.isConnected())
			{
				if(ConnectivityManager.TYPE_WIFI   == netInfo.getType()) isWifi   = true;
				else
					if(ConnectivityManager.TYPE_MOBILE == netInfo.getType()) isMobile = true;
					else
						continue;
			}
			else
			{
				continue;
			}

			if(isMobile)
			{
				if(netInfo.getExtraInfo().equalsIgnoreCase("VZWINTERNET"))
					mobile_network_type = "Public, Dynamic";
				else
					if(netInfo.getExtraInfo().equalsIgnoreCase("NE01.VZWSTATIC"))
						mobile_network_type = "Public, Static";
					else
						if(netInfo.getExtraInfo().equalsIgnoreCase("PRTL.GW12.VZWENTP"))
							mobile_network_type = "Private, Static";
						else
							mobile_network_type = "Unknown";
			}
			//			mobile_network_type = String.valueOf(netInfo.getExtraInfo().equalsIgnoreCase("vzwinternet"));

			//			if (netInfo.getTypeName().equalsIgnoreCase("WIFI")) if (netInfo.isConnected()) isWifi = true;

			if(netInfo.equals(null)) continue;

			Log.i("NETWORK INFO", "getExtraInfo: " 				+ String.valueOf(netInfo.getExtraInfo()));
			//			Log.i("NETWORK INFO", "getReason: " 				+ String.valueOf(netInfo.getReason()));
			Log.i("NETWORK INFO", "getSubtypeName: " 			+ String.valueOf(netInfo.getSubtypeName()));
			Log.i("NETWORK INFO", "getTypeName: " 				+ String.valueOf(netInfo.getTypeName()));
			//			Log.i("NETWORK INFO", "toString: " 					+ String.valueOf(netInfo.toString()));
			//			Log.i("NETWORK INFO", "getDetailedState: " 			+ String.valueOf(netInfo.getDetailedState()));
			//			Log.i("NETWORK INFO", "getState: " 					+ String.valueOf(netInfo.getState()));			
			//			Log.i("NETWORK INFO", "getSubtype: " 				+ Integer.toString(netInfo.getSubtype()));

			if(ConnectivityManager.TYPE_WIFI   == netInfo.getType())  Log.i("NETWORK INFO", "TYPE_WIFI");
			if(ConnectivityManager.TYPE_MOBILE == netInfo.getType())  Log.i("NETWORK INFO", "TYPE_MOBILE");

			Log.i("NETWORK INFO", "isAvailable: " 				+ Boolean.toString(netInfo.isAvailable()));
			Log.i("NETWORK INFO", "isConnected: " 				+ Boolean.toString(netInfo.isConnected()));
			Log.i("NETWORK INFO", "isConnectedOrConnecting: " 	+ Boolean.toString(netInfo.isConnectedOrConnecting()));
			Log.i("NETWORK INFO", "isFailover: " 				+ Boolean.toString(netInfo.isFailover()));
			Log.i("NETWORK INFO", "isRoaming: " 				+ Boolean.toString(netInfo.isRoaming()));	
			if(isMobile || isWifi)
				break;
		}

		if (isWifi)
		{			
			WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
			int numberOfLevels = 5;
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
			txt_signalStrength.setText(""+level);

			//txt_network_type.setText("You are on WIFI Network");
		}
		else
			if (isMobile)
			{
				psListener = new MyPhoneStateListener();
				telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				
				if(telephonyManager.getNetworkType() == TelephonyManager.NETWORK_TYPE_LTE)
                    simType = "lte";
                else if(telephonyManager.getNetworkType() == TelephonyManager.NETWORK_TYPE_CDMA)
                    simType = "cdma";
                else
                    simType = "gsm";
				telephonyManager.listen(psListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

				//txt_network_type.setText("Your Mobile Network Type is: " + mobile_network_type);
			}
			else
			{
				txt_signalStrength.setText("0");
				//txt_network_type.setText("");
			}
		} catch (NullPointerException e) {
			// TODO: handle exception
			txt_signalStrength.setText("0");
			String Error = this.getClass().getName() + " : "
					+ e.getMessage();
			OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "APk signal strength Error", Error);
		    OnInit_Data_Access.insert_incident_detail(Error, Constants.After_the_tablet_starts_up);
		}
	}


	 private class MyPhoneStateListener extends PhoneStateListener
   {
       /* Get the Signal strength from the provider, each time there is an update */

       public void onSignalStrengthsChanged(SignalStrength signalStrength)
       {
           final ConnectivityManager connMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
           final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

           if (wifi.isConnectedOrConnecting()) return;
           super.onSignalStrengthsChanged(signalStrength);

           String ssignal = signalStrength.toString();
           String[] parts = ssignal.split(" ");

           if(simType.equals("lte")) {

               int mLteRsrp = Integer.parseInt(parts[9]);
               setTheLteSignalStrength(mLteRsrp);
           }  else if(simType.equals("cdma")) {

               int mLteRsrp = Integer.parseInt(parts[3]);
               setTheCDMASignalStrength(mLteRsrp);
           }else {
               int mLteRsrp = Integer.parseInt(parts[1]);
               setTheGSMSignalStrength(mLteRsrp);

           }

           //Log.i("NETWORK INFO", "mLteRsrp: " + String.valueOf(parts[9]));

       }
   }

  
   private void setTheLteSignalStrength(int mLteRsrp){	 
	   int asu = mLteRsrp + 140;
	   //txt_signalStrengthdBmAsu.setText(mLteRsrp+" dBm  "+ asu +" asu");
       final int[] RSRP_THRESH_STRICT  = new int[] {-140, -115, -105,  -95, -75, -44};
       int[] threshRsrp = RSRP_THRESH_STRICT;

       if (mLteRsrp > threshRsrp[5]) txt_signalStrength.setText("5");
       else
       if (mLteRsrp >= threshRsrp[4]) txt_signalStrength.setText("4");
       else
       if (mLteRsrp >= threshRsrp[3]) txt_signalStrength.setText("3");
       else
       if (mLteRsrp >= threshRsrp[2]) txt_signalStrength.setText("2");
       else
       if (mLteRsrp >= threshRsrp[1]) txt_signalStrength.setText("1");
       else
       	txt_signalStrength.setText("0");

   }
   private void setTheCDMASignalStrength(int mLteRsrp){
	   	    int asu = mLteRsrp + 116;
	  // txt_signalStrengthdBmAsu.setText(mLteRsrp+" dBm  "+ asu +" asu");
       final int[] RSRP_THRESH_STRICT  = new int[] { -100, -95,  -85, -75, -44};
       int[] threshRsrp = RSRP_THRESH_STRICT;

       if (mLteRsrp > threshRsrp[4]) txt_signalStrength.setText("5");
       else
       if (mLteRsrp >= threshRsrp[3]) txt_signalStrength.setText("4");
       else
       if (mLteRsrp >= threshRsrp[2]) txt_signalStrength.setText("3");
       else
       if (mLteRsrp >= threshRsrp[1]) txt_signalStrength.setText("2");
       else
       if (mLteRsrp >= threshRsrp[0]) txt_signalStrength.setText("1");
       else
        txt_signalStrength.setText("0");

   }
   private void setTheGSMSignalStrength(int mLteRsrp){
	   int asu = 99;
	   int dBm = 0;
	   asu = mLteRsrp;
	   if(asu != 99)		   
		   dBm = 2*asu - 113;
	  // txt_signalStrengthdBmAsu.setText(dBm+" dBm  "+asu+" asu");
       if ((mLteRsrp <= 2 || mLteRsrp == 99)) txt_signalStrength.setText("0");
       else
       if (mLteRsrp >= 17) txt_signalStrength.setText("5");
       else
       if (mLteRsrp >= 12) txt_signalStrength.setText("4");
       else
       if (mLteRsrp >= 8) txt_signalStrength.setText("3");
       else
       if (mLteRsrp >= 5) txt_signalStrength.setText("2");
       else
       	txt_signalStrength.setText("1");
   }
}
