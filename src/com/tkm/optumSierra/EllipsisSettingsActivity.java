package com.tkm.optumSierra;

import com.tkm.optumSierra.util.CustomKeyboard;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class EllipsisSettingsActivity extends Activity{
	CustomKeyboard mCustomKeyboard;
	EditText edtPasscode;
	String passcode = "";
	private LinearLayout layout_passcode;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

		// ...but notify us that it happened.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
		this.setFinishOnTouchOutside(false);
		setContentView(R.layout.settings_popup);
		Button btnInformation = (Button) findViewById(R.id.btn_information);
		Button btnDevices = (Button) findViewById(R.id.btn_devices);
		Button btnClose = (Button) findViewById(R.id.btn_settingsClose);

		btnInformation.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intentDevicePasscode = new Intent(getApplicationContext(),EllipsisInformationActivity.class);
				startActivity(intentDevicePasscode);
				finish();
			}
		});
		btnDevices.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				Intent intentDevicePasscode = new Intent(getApplicationContext(),SettingsPasscodeActivity.class);
				startActivity(intentDevicePasscode);
				finish();
			}
		});
		btnClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				finish();

			}
		});
	}



}
