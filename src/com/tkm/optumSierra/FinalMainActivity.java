package com.tkm.optumSierra;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.tkm.optumSierra.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.tkm.optumSierra.bean.ClassSensor;
import com.tkm.optumSierra.dal.Flow_Status_db;
import com.tkm.optumSierra.dal.MydataSensor_db;
import com.tkm.optumSierra.service.AlarmService;
import com.tkm.optumSierra.service.CheckService;
import com.tkm.optumSierra.service.TenMinuteService;
import com.tkm.optumSierra.util.Alam_util;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.ConnectionDetector;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Regular_Alam_util;
import com.tkm.optumSierra.util.Upload_Incident_Detail_Task;
import com.tkm.optumSierra.util.Util;

@SuppressLint({ "HandlerLeak", "InflateParams" })
public class FinalMainActivity extends Titlewindow implements OnInitListener, OnClickListener {

	public static String PostUrl = "";
	public static int PatientIdDroid;
	public static String serialNo = "";
	public static String imeilNo = "";
	private String ApkType = "";
	private int is_mydata_enable = 0;
	private int is_home_enable = 0;
	private int is_support_enable = 0;
	private String TAG = "FinalMainActivity";
	String Config_file_path = Environment.getExternalStorageDirectory()
			.toString() + "/versions/config.ini.txt";
	private MydataSensor_db myDataSensor_db = new MydataSensor_db(this);
	private TextToSpeech tts;
	public int a = 0;
	View paranepview;
	// ProgressDialog pd = null;
	public int flag_login = 0;
	BackgroundThread backgroundThread;
	Dialog dialog;
	private View pview_back;
	GlobalClass appClass;
	Flow_Status_db Flow_db;
	private TextView txt_welcome;
	private TextView txt_uploadTime;
	private TextView txt_uploadStatus;
	private Button btn_Start;

	@SuppressLint({ "NewApi", "InflateParams" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_final_main);
		appClass = (GlobalClass)getApplicationContext();
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = true;
		appClass.isSessionStart = false;
		appClass.isAlarmUtilStart = false;
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		pview_back = inflater.inflate(R.layout.activity_final_main, null);

		SharedPreferences file_data_settings = getApplicationContext().getSharedPreferences("file_data", 0);
		int sleep_system_idle_timeout = Integer.parseInt(file_data_settings.getString("sleep_system_idle_timeout", "600")); // defualt as 600 sec ie 10 min					
		android.provider.Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, (60000 * sleep_system_idle_timeout/60));

		//Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, (60000 * 5 * 2));

		txt_welcome = (TextView)findViewById(R.id.txt_welcome);
		txt_uploadTime = (TextView)findViewById(R.id.txt_uploadTime);
		txt_uploadStatus = (TextView)findViewById(R.id.txt_uploadStatus);
		btn_Start = (Button)findViewById(R.id.btn_homeStart);
		btn_Start.setOnClickListener(this);
		setSplashScreen();
		
		Flow_db = new Flow_Status_db(FinalMainActivity.this);
		Flow_db.open();
		Flow_db.insert("1");

		Cursor cursor1 = Flow_db.selectAll();
		Status = new String[cursor1.getCount()];
		int j = 0;

		while (cursor1.moveToNext()) {
			Status[j] = cursor1.getString(cursor1.getColumnIndex("Status"));

			Status_Val = Status[0];

			j += 1;

		}
		//String a="f";
		//Integer.parseInt(a);

		Flow_db.close();

		insConfig(); // dev build call the function

		checkWiFi();
		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		SharedPreferences.Editor editor = flow.edit();
		editor.putInt("flow", 0);
		editor.putInt("reminder_flow", 0);
		editor.putInt("start_flow", 0); // for normal reloading
		editor.commit();

		start_check_service();
		start_TenMinute_service();
		try {

			Alam_util.cancelAlarm(FinalMainActivity.this);
			Regular_Alam_util.cancelAlarm(FinalMainActivity.this);
		} catch (Exception e) {
			Log.e(TAG, "exception while closing alam in final main act... ");
		}	
		
		

		/*	OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "system boot", "system boot");
		OnInit_Data_Access.insert_incident_detail("system boot", Constants.After_the_tablet_starts_up);
		Intent i = new Intent(getApplicationContext(), RebootDataManager.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);*/
		//appClass.isRemSessionStart = false;
	}

	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.US);

			tts.setSpeechRate(0); // set speech speed rate
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e(TAG, "TTS Language is not supported");
			} else {
				// speakOut();
			}
		} else {
			Log.e(TAG, "TTS Initilization Failed");
		}

	}

	public void onResume(){
		
		super.onResume();
		start.setVisibility(View.GONE);
		
	}
	private void checkWiFi() {
		try {

			SharedPreferences settings = getSharedPreferences(
					CommonUtilities.WiFi_SP, 0);

			WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

			/*if (!wifiManager.isWifiEnabled()) {

				wifiManager.setWifiEnabled(true);
				Thread.sleep(4000);

			}*/

			TelephonyManager tManager = (TelephonyManager) this
					.getSystemService(Context.TELEPHONY_SERVICE);
			imeilNo = tManager.getDeviceId();
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("imei_no", imeilNo);

			serialNo="0000000000000";
			editor.putString("wifi", serialNo);	
			editor.commit();

			//WifiInfo wInfo = wifiManager.getConnectionInfo();
			String macAddress = serialNo;
			if (macAddress.trim().length() > 4) {
				serialNo = macAddress.replace(":", "");
				editor.putString("wifi", serialNo);
				editor.commit();

				// Toast.makeText(getApplicationContext(),
				// "WiFi MAc on final main: "+serialNo,
				// Toast.LENGTH_LONG).show();
			} else {
				/*Toast.makeText(getApplicationContext(),
						this.getString(R.string.turnonwifi), Toast.LENGTH_LONG)
						.show();*/

			}

		} catch (Exception e) {
			Log.e(TAG, "checkWiFi wifi/imei number issue");
			/*Toast.makeText(this, this.getString(R.string.turnonwifi),
					Toast.LENGTH_LONG).show();*/
		}
		checkConfig();
	}

	public static void CancelNotification(Context ctx, int notifyId) {
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager nMgr = (NotificationManager) ctx
				.getSystemService(ns);
		nMgr.cancel(notifyId);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked on final main page:");

			Intent intent = new Intent(this, FinalMainActivity.class);
			this.finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	private void checkConfig() {

		PostUrl = CommonUtilities.SERVER_URL;

		{

			SharedPreferences settingseifi = getSharedPreferences(
					CommonUtilities.WiFi_SP, 0);
			serialNo = settingseifi.getString("wifi", "-1");
			imeilNo = settingseifi.getString("imei_no", "");

			SharedPreferences settings = getSharedPreferences(
					CommonUtilities.SERVER_URL_SP, 0);
			String server1 = settings.getString("Server_url", "-1");
			String server2 = settings.getString("Server_port", "-1");
			String posturl = settings.getString("Server_post_url", "-1");

			Constants.setIMEI_No(imeilNo);
			Constants.setParams(server1, server2, posturl, serialNo);
			PostUrl = posturl;

			Boolean isInternetPresent = (new ConnectionDetector(
					getApplicationContext())).isConnectingToInternet();

			// Redirect to HFP / sierra

		

			try {
				if (!isMyReminderServiceRunning()) {
					Log.e(TAG, " starting  AlarmService ");

					Intent reminder = new Intent();
					reminder.setClass(this, AlarmService.class);
					startService(reminder);
				} else {

				}

			} catch (Exception e) {
				Log.e(TAG, "exception while starting or closing alam... ");
			}
			
			if (isInternetPresent) {

				Log.i(TAG, "Loading apk details...");
				loadApkDetails(); // if there is a connection , fetch
				// Patient
				// Details to local Sql_Lite table

			} else {

			}

		}
	}

	@SuppressWarnings("deprecation")
	private void popup() {
		Log.e(TAG, " showing popup... ");
		try {

			LayoutInflater inflater = (LayoutInflater) this
					.getSystemService(LAYOUT_INFLATER_SERVICE);

			View pview;
			pview = inflater.inflate(R.layout.popup_messages, null);

			TextView msg = (TextView) pview.findViewById(R.id.textpopup);
			TextView mac = (TextView) pview.findViewById(R.id.txtmac);

			Typeface type = Typeface.createFromAsset(getAssets(),
					"fonts/FrutigerLTStd-Roman.otf");
			msg.setTypeface(type, Typeface.NORMAL);
			msg.setText(this.getString(R.string.tabletnotregistered));

			mac.setTypeface(type, Typeface.NORMAL);
			if (imeilNo.trim().length() > 5) {
				if (imeilNo.trim().length() > 14) {

					String asubstring = imeilNo.substring(0, 14);

					mac.setText(asubstring);
				} else {
					mac.setText(imeilNo);
				}

			} else {
				mac.setText(serialNo.toUpperCase());
			}

			closeversion = (Button) pview.findViewById(R.id.exitversion);

			final PopupWindow cp = new PopupWindow(pview,
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
			cp.setBackgroundDrawable(new BitmapDrawable(getApplicationContext()
					.getResources()));
			cp.showAtLocation(pview_back, Gravity.CENTER, 0, 0);

			// cp.update(0, 0, 350, 350);
			// cp.update(0, 0, 750, 500);
			DisplayMetrics displaymetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
			int height = (displaymetrics.heightPixels);
			int width = displaymetrics.widthPixels / 2;
			height = (int) (height / 1.2);

			// cp.update(0, 0, 350, 350);
			// cp.update(0, 0, 750, 500);
			cp.update(0, 0, width, height);

			closeversion.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {

					cp.dismiss();
				}
			});
		} catch (Exception e) {
			Log.e(TAG, "exception while showing popup... ");
		}
	}

	public void loadApkDetails() {
		DownloadApkDetailsTask task = new DownloadApkDetailsTask();
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	@Override
	public void onStop() {
		// Commit the edits!
		super.onStop();
		flag_login = 1;

		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}

		// HFPUtil.WriteLog(TAG,PatientIdDroid+"");

		Log.i(TAG, "onStop on finalmain activity");

	}

	/***** Download Apk Details ********/

	@SuppressLint("DefaultLocale")
	private class DownloadApkDetailsTask extends
	AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {


			try {
				// pd = ProgressDialog.show(FinalMainActivity.this, "",
				// "Loading Details. Please wait...",true);
				setProgressBarIndeterminateVisibility(true);
			} catch (Exception e) {
				Log.i(TAG, "Exception in ProgressDialog.show");
			}
		}

		@Override
		protected void onPostExecute(String result) {
			Log.i(TAG, "onPostExecute clustor download");

			// setProgressBarIndeterminateVisibility(false);

			if (flag_login == 0) {
			
				SharedPreferences settings2 = getSharedPreferences(
						CommonUtilities.CLUSTER_SP, 0);
				ApkType = settings2.getString("clustorNo", "-1"); // #1

				if (ApkType.equals("1000000")) // Sierra//
				{

				} else if (ApkType.equals("1000001")) // HFP//
				{

				} else {
					if (!PostUrl.equals("-1"))
						popup();
				}
			}
			if(is_mydata_enable == 1 && timeDifference() >= 7){
				
				SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
				PostUrl = settings.getString("Server_post_url", "-1");
				DownloadMyDataSensorTask taskUpdate = new DownloadMyDataSensorTask();
				taskUpdate.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			}
		}

		protected String doInBackground(String... urls) {
			String response = "";
			try {

				Log.i(TAG, "Checking device registration MAC-" + serialNo
						+ " IMEI-" + imeilNo);

				HttpResponse response1 = Util.connect(PostUrl
						+ "/droid_website/cluster.ashx", new String[] {
								"gw_id", "imei_no" },
						new String[] { serialNo, imeilNo });

				if (response1 == null) {
					Log.e(TAG, "Connection Failed!");
					return ""; // process
				}

				// login_db.delete_patient();
				String str = Util
						.readStream(response1.getEntity().getContent());

				if (str.trim().length() == 0) {
					Log.i(TAG,
							"No aPK details for the current serial number from server");
					return "";
				}
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");
				//Log.e(TAG, "Cluster details----->"+str);
				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);

				NodeList nodes = doc.getElementsByTagName("optum");
				for (int i = 0; i < nodes.getLength(); i++) {

					ApkType = Util.getTagValue(nodes, i, "cluster");
					is_home_enable = Integer.parseInt(Util.getTagValue(nodes, i, "is_home_enable"));
					is_support_enable = Integer.parseInt(Util.getTagValue(nodes, i, "is_support_enable"));
					is_mydata_enable = Integer.parseInt(Util.getTagValue(nodes, i, "chart_status"));
					/*
					 * String contract_code = Util.getTagValue(nodes, i,
					 * "contractcode");
					 */
					int time_zone = Integer.parseInt(Util.getTagValue(nodes, i,
							"time_zone_id"));
					String patient_id = Util
							.getTagValue(nodes, i, "patient_id");
					String passcode = Util
							.getTagValue(nodes, i, "passcode");

					SharedPreferences settings = getSharedPreferences(
							CommonUtilities.CLUSTER_SP, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putString("clustorNo", ApkType);
					editor.putInt("time_zone_id", time_zone);
					editor.putInt("is_home_enable", is_home_enable);
					editor.putInt("is_support_enable", is_support_enable);
					editor.putInt("chart_status", is_mydata_enable);
					editor.putString("Passcode", passcode);


					editor.commit();

					SharedPreferences contract_cod = getSharedPreferences(
							CommonUtilities.USER_SP, 0);
					SharedPreferences.Editor contract_editor = contract_cod
							.edit();
					contract_editor.putString("patient_id", patient_id);
					contract_editor.commit();

				}

			} catch (Exception e) {
				e.printStackTrace();
				String Error = this.getClass().getName()+ " : "+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "APk Details Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
			}

			return response;
		}
	}

	private void insConfig() {

		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.SERVER_URL_SP, 0);

		String server3 = settings.getString("Server_url", "-1");

		if (server3.contains("-1")) {

			Intent intent = new Intent(getApplicationContext(),
					SettingsActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

			startActivity(intent);
			overridePendingTransition(0, 0);
			return;
		}

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	public class BackgroundThread extends Thread {
		volatile boolean running = false;
		int cnt;

		void setRunning(boolean b) {
			running = b;
			cnt = 5;
		}

		@Override
		public void run() {
			while (running) {
				try {
					sleep(400);
					if (cnt-- == 0) {
						running = false;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			handler.sendMessage(handler.obtainMessage());
		}
	}

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			setProgressBarIndeterminateVisibility(false);
			// progressDialog.dismiss();

			boolean retry = true;
			while (retry) {
				try {
					backgroundThread.join();
					retry = false;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			checkWiFi();

			// setwelcome();
			// setContentView(R.layout.activity_black);
		}

	};

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.e(TAG, "onRestart starting page");

		Intent intent = new Intent(getApplicationContext(),
				FinalMainActivity.class);

		startActivity(intent);
		this.finish();

	}

	private boolean isMyReminderServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AlarmService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private boolean is_cCheckService_Running() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.CheckService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private void start_check_service() {
		try {
			if (!is_cCheckService_Running()) {
				Log.e(TAG, " starting  CheckService ");

				Intent reminder = new Intent();
				reminder.setClass(this, CheckService.class);
				startService(reminder);

			} else {

				Log.e(TAG, " Already   CheckService started ");
			}

		} catch (Exception e) {
			Log.e(TAG, "exception while starting or closing CheckService... ");
		}
	}

	private void start_TenMinute_service() {
		try {
			if (!is_cCheckService_Running()) {
				Log.e(TAG, "Starting TenMinuteService");
				Intent tenMinute = new Intent();
				tenMinute.setClass(this, TenMinuteService.class);
				startService(tenMinute);
			} else {
				Log.e(TAG, "Already TenMinuteService Started");
			}
		} catch (Exception e) {
			Log.e(TAG, "Exception while starting or closing TenMinuteService...!");
		}
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_homeStart:
			startSession();
			break;

		default:
			break;
		}
	}
	
	private void setSplashScreen(){		
		
		SharedPreferences lastStatus = getSharedPreferences(CommonUtilities.LASTUPLOAD_STATUS, 0);
		if(lastStatus.getString("nick_name", "").equals("")){

			txt_welcome.setText(getResources().getString(R.string.welcome));
		} else{

			txt_welcome.setText(getResources().getString(R.string.welcome) + " " +
					lastStatus.getString("nick_name", "") + " !");
		}
		
		
		txt_uploadTime.setText(" " + lastStatus.getString("upload_time", ""));
		if(lastStatus.getString("upload_status", "").equals("success")){
			
			txt_uploadStatus.setText(" "+getResources().getString(R.string.uploadSuccessful));
		}else if(lastStatus.getString("upload_status", "").equals("failed")){
			
			txt_uploadStatus.setText(" "+getResources().getString(R.string.uploadUnsuccessful));
		}else{
			
			txt_uploadStatus.setText("");
		}
		
		
	}
	
	
	private class DownloadMyDataSensorTask extends
	AsyncTask<String, Void, String> {
		SharedPreferences settingswifi = getSharedPreferences(CommonUtilities.WiFi_SP, 0);

		String imeilNo = settingswifi.getString("imei_no", "");

		@Override
		protected void onPreExecute() {
			Log.i("onPreExecute", "onPreExecute MainActivity");

		}

		@Override
		protected void onPostExecute(String result) {


			Cursor c = myDataSensor_db.SelectMydataSensors();
			int size = c.getCount();

			if (size > 0) {

				Intent myDataIntent = new Intent(getApplicationContext(), MyDataActivity.class);
				startActivity(myDataIntent);
			} else {

				Toast.makeText(getApplicationContext(), getResources().getString(R.string.noVitals), Toast.LENGTH_SHORT).show();

			}

		}

		protected String doInBackground(String... urls) {

			String response = "";
			try {
				//sensor_db.deleteplayorder();//
				SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
				String token = tokenPreference.getString("Token_ID", "-1");
				String patientId = tokenPreference.getString("patient_id", "-1");
				HttpResponse response1 = Util.connect(PostUrl
						+ "/droid_website/rtd_sensor_details_mydata.ashx", new String[]{
								"imei_no", "patient_id"}, new String[]{imeilNo,
										patientId});

				if (response1 == null) {


					return ""; // process
				}
				// Log.e("Optum_Sierra","serialNo"+serialNo);

				String str = Util
						.readStream(response1.getEntity().getContent());
				if (str.trim().length() == 0) {

					//sensor_db.delete();
					return "";
				}
				str = str.replace("&", "");
				//Log.i("response:", "sensor_response:" + str);
				DocumentBuilder dbr = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = dbr.parse(is);
				NodeList AF_Nodes = doc.getElementsByTagName("patient_sensor_details");
				for (int i = 0; i < AF_Nodes.getLength(); i++) {
					response = Util.getTagValue(AF_Nodes, i, "mob_response");

				}
				if (response.equals("AuthenticationError")) {

					return response;
				}
				NodeList nodes = doc.getElementsByTagName("sensor");
				myDataSensor_db.delete(); // deleting entire table
				ClassSensor sensor = new ClassSensor();

				for (int i = 0; i < nodes.getLength(); i++) {

					sensor.setSensorId(Integer.parseInt(Util.getTagValue(nodes, i, "sensor_id")));
					sensor.setSensorName(Util.getTagValue(nodes, i, "vital_name"));
					ContentValues values = new ContentValues();
					values.put("Sensor_id", sensor.getSensorId());
					values.put("Vital_name", sensor.getSensorName());
					myDataSensor_db.insertMydataSensor(values);// inserting downloaded date
					response = "Success";
				}
				myDataSensor_db.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			return response;
		}
	}
	
	private int timeDifference() {
        SharedPreferences mydata = getSharedPreferences("mydata", 0);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        Date lastClickDate = null;
        Date currentDate = null;
     
           
				try {
					lastClickDate = simpleDateFormat.parse(mydata.getString("click_time", appClass.get_myData_date()));
					currentDate = simpleDateFormat.parse(appClass.get_myData_date());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
            
        
        long milliseconds = currentDate.getTime() - lastClickDate.getTime();
        int days = (int) (milliseconds / (1000 * 60 * 60 * 24));
        return days;
    }
	
	
}
