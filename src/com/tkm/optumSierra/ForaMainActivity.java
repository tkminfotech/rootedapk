package com.tkm.optumSierra;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.taidoc.pclinklibrary.android.bluetooth.util.BluetoothUtil;
import com.taidoc.pclinklibrary.connection.AndroidBluetoothConnection;
import com.taidoc.pclinklibrary.connection.util.ConnectionManager;
import com.taidoc.pclinklibrary.constant.PCLinkLibraryConstant;
import com.taidoc.pclinklibrary.constant.PCLinkLibraryEnum;
import com.taidoc.pclinklibrary.exceptions.NotSupportMeterException;
import com.taidoc.pclinklibrary.meter.AbstractMeter;
import com.taidoc.pclinklibrary.meter.record.AbstractRecord;
import com.taidoc.pclinklibrary.meter.record.TemperatureRecord;
import com.taidoc.pclinklibrary.meter.util.MeterManager;
import com.tkm.optumSierra.bean.ClassTemperature;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.dal.Temperature_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.Log;
import com.tkm.optumSierra.util.PINReceiver;
import com.tkm.optumSierra.util.Util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class ForaMainActivity extends Titlewindow implements OnInitListener {
	private static final String TAG = "foraMainActivity Sierra";

	private static final int REQUEST_ENABLE_BT = 1;

	private static final int SENSOR_TYPE_BLE = 1;
	private static final int SENSOR_TYPE_CLASSIC = 0;
	private static final int SENSOR_TYPE_UNKNOWN = -1;

	private static final String HEXES = "0123456789ABCDEF";
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

	private GlobalClass appClass;
	ImageView rocketImage;
	AnimationDrawable rocketAnimation;
	ObjectAnimator AnimPlz;
	TextView txtwelcom, txtReading;
	private ProgressDialog progressDialog = null;
	private ProgressDialog pDialog;
	private TextToSpeech tts;

	private final BroadcastReceiver pinReceiver = new PINReceiver();
	private BluetoothAdapter mBluetoothAdapter = null;
	private BluetoothSocket btSocket = null;
	private OutputStream outStream = null;
	private InputStream inStream = null;

	int Dec = 0;
	int tempId;
	private int deviceType = -1;
	private String result = "0, 0";

	String macAddress = "";
	private BTConnector btFora = new BTConnector();
	BluetoothDevice device;

	Sensor_db dbSensor = new Sensor_db(this);
	Temperature_db dbtemperature = new Temperature_db(this);

	private AndroidBluetoothConnection mConnection; // BLE connection (PCLink library)
	private AbstractMeter mForaMeter = null;

	// bluetooth recovery variables
	private boolean mBluetoothSocketTimedOut = false; // used to indicate that the BluetoothSocket connection has timed out
	private int failedBleConnectionCount = 0; // used to indicate how many BLE connections have failed

	// step 1
	// BT classic AsyncTask is executed here
	// le scanning is started here
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aand_dreader);

		appClass = (GlobalClass) getApplicationContext();
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = false;
		tts = new TextToSpeech(this, this);

		device = null;
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		deviceType = extras.getInt("deviceType"); // device type will always be 2 (temperature)
		macAddress = extras.getString("macaddress"); // get assigned MAC address

		Button btnmanual = (Button) findViewById(R.id.manuval);

		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);

		Typeface type = Typeface.createFromAsset(getAssets(), "fonts/FrutigerLTStd-Roman.otf");

		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);

		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		rocketImage.setVisibility(View.INVISIBLE);
		txtReading.setVisibility(View.INVISIBLE);
		setwelcome();

		btnmanual.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intentTemperature = new Intent(getApplicationContext(), TemperatureEntry.class);

				if (btFora != null && !btFora.isCancelled())
					btFora.cancel(true);

				finish();
				startActivity(intentTemperature);
			}
		});

		if (macAddress.trim().length() == 17) {
			device = mBluetoothAdapter.getRemoteDevice(macAddress);
		}

		if (device == null) {
			createOKProgressDialog(this.getString(R.string.Bluetooth), this.getString(R.string.unabletoconnectbluetoothAandD));
			finish();

		} else {
			Log.i(TAG, "DROID : Connect to device " + device.toString());
			if (!enableBluetooth()) {
				finish();
			}

			int sensorBluetoothType = getSensorBluetoothType(macAddress);

			// start the BT classic cycle if the sensor BT type is either BLE or UNKNOWN
			if (sensorBluetoothType != SENSOR_TYPE_BLE)
				btFora.execute(device);

			// start the BT classic cycle if the sensor BT type is either BLE or UNKNOWN
			if (Build.VERSION.SDK_INT >= 18 && sensorBluetoothType != SENSOR_TYPE_CLASSIC)
				leConnect();

		}
		Log.i(TAG, "-------------------oncreate--------------  fora  activity");
	}

	@Override
	protected void onResume() {
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
		registerReceiver(pinReceiver, filter);
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause: called");
		super.onPause();
	}

	// called before onStart, recreates the activity!
	@Override
	protected void onRestart() {
		super.onRestart();

		Intent intent = new Intent(getApplicationContext(), ForaMainActivity.class);
		this.finish();
		startActivity(intent);
	}

	@Override
	public void onStop() {
		super.onStop();

		mBluetoothAdapter.cancelDiscovery();

		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}

		if (btFora != null) {
			btFora.cancel(true);
			btFora = null;
		}

		try {
			if (btSocket != null) {
				btSocket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		leDisconnect();

		try {
			unregisterReceiver(pinReceiver);

		} catch (Exception e) {
			e.printStackTrace();
		}

		Log.e(TAG, "-- ON STOP Fora activity --");
		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
	}

	public boolean enableBluetooth() {

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		mBluetoothAdapter.enable();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return true;
	}

	// step 2 (classic) called in onCreate
	private class BTConnector extends AsyncTask<BluetoothDevice, String, String> {
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected String doInBackground(BluetoothDevice... device) {
			try {
				for (Integer port = 1; port <= 1500; port++) {

					if (isCancelled()) {
						Log.i(TAG, "cancelled in background");
						return "";
					}

					if (connect(device[0])) {

						Log.i(TAG, " Connection successful for " + port);
						return result;
					}
					try {
						Log.i(TAG, ": Waiting. port.." + port);
						Thread.sleep(2000);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			}
			return "";
		}

		// Updating progress bar
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		// After completing background task Dismiss the progress dialog
		@Override
		protected void onPostExecute(String result) {
			// dismiss the dialog after the file was downloaded

			if (pDialog != null)
				pDialog.dismiss();
			if (progressDialog != null)
				progressDialog.dismiss();

			if (result.trim().length() > 0) {
				Intent intent = new Intent(ForaMainActivity.this,
						ShowTemperatureActivity.class);
				intent.putExtra("temp", result);
				intent.putExtra("tempid", tempId);
				btFora.cancel(true);
				ForaMainActivity.this.finish();
				startActivity(intent);
			}
		}

		protected boolean connect(BluetoothDevice device) {
			boolean finished = false;
			byte[] data = new byte[34];
			result = "0, 0";
			// mBluetoothAdapter.startDiscovery(); changed this as follows
			mBluetoothAdapter.cancelDiscovery();

			SharedPreferences flow = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
			int val = flow.getInt("flow", 0); // #1
			if (val == 1) {
				SharedPreferences section = getSharedPreferences(CommonUtilities.PREFS_NAME_date, 0);
				SharedPreferences.Editor editor_retake = section.edit();
				editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(ForaMainActivity.this));
				editor_retake.apply();
			}
			try {
				btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);

			} catch (IOException e) {
				Log.e(TAG, "ON RESUME: Socket creation failed.", e);
				e.printStackTrace();
			}
			if (btSocket == null) {
				return false;
			}

			try {
				try {
					// (from doc) This call will block until a connection is made or the connection fails
					btSocket.connect();
					Log.e(TAG, "BTConnector#connect: BT connection established, data transfer link open.");
				} catch (IOException e) {
					try {
						Log.e(TAG, "BTConnector#connect: btSocket.connect drror");
						btSocket.close();

						mBluetoothSocketTimedOut = true;

						return false;
					} catch (IOException e2) {
						Log.e(TAG, "BTConnector#connect: btSocket.close error ", e2);
					}
				}

				// if we're able to successfully connect to the socket, it means that the sensor is classic bluetooth.
				// so we'll stop BLE stuff
				leDisconnect();
				// save the sensor type as BT classic
				saveSensorBluetoothType(macAddress, SENSOR_TYPE_CLASSIC);

				// set the Fora as paired
				Util.setPairStatus(ForaMainActivity.this, "fora_paired", "pair");

				// attempt to connect to device
				try {

					outStream = btSocket.getOutputStream();
					inStream = btSocket.getInputStream();

					Log.i(TAG, "deviceType" + deviceType);

					int checkSum = (0x51 + 0x26 + 0x00 + 0x00 + 0x00 + 0x00 + 0xA3) & 0xFF;
					int cmd[] = { 0x51, 0x26, 0x00, 0x00, 0x00, 0x00, 0xA3, checkSum };

					try {
						for (int aCmd : cmd) {
							outStream.write((aCmd & 0xFF));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

					if (inStream.available() > 0) {
						inStream.read(data);
					}
					try {
						for (int aCmd : cmd) {
							outStream.write((aCmd & 0xFF));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						Thread.sleep(1500);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

					byte H;
					byte L;
					try {
						System.out.print("Data received ");

						if (inStream.available() > 0) {
							inStream.read(data);

							System.out.println(getHex(data, data.length));

							if (deviceType == 2) // Temperature
							{
								H = data[3];
								L = data[2];
								String hex1 = Integer.toHexString(H & 0XFF);
								String hex2 = Integer.toHexString(L & 0XFF);
								String NewHex = hex1 + hex2;

								Log.i(TAG, "Temperature value from device hex "
										+ NewHex);

								Dec = Integer.parseInt(NewHex, 16);
								Log.i(TAG, "Temperature value from device C " + Dec);

								float temp = (float) Dec / 10;
								double finalValue = (temp * (1.8)) + 32;

								double tempValue;
								tempValue = Util.round(finalValue);

								result = tempValue + "";

								insertReading(tempValue); // inserts the reading to the SQLite database

								byte messageTurnOff[] = { 81, 80, 0, 0, 0, 0, -93, 68 };

								try {
									outStream.write(messageTurnOff);
								} catch (IOException e) {
									Log.e(TAG, "ON RESUME: Exception during turn off write cmd in temp device .", e);
									e.printStackTrace();
								}

							}
							try {
								inStream.close();
								outStream.close();
								btSocket.close();
							} catch (IOException e2) {
								Log.e(TAG, "IOException while closing the socket or stream after the reading was taken", e2);
								e2.printStackTrace();
							}

							return true;
						}

					} catch (IOException e) {
						Log.e(TAG, "ON RESUME: Exception during read.", e);
						e.printStackTrace();
					}

				} catch (Exception e) {
					Log.e(this.toString(), "IOException " + e.getMessage());
					e.printStackTrace();
				}
			} catch (Exception ex) {
				Log.e(this.toString(), "Exception " + ex.getMessage());
				ex.printStackTrace();
			}

			try {
				inStream.close();
				outStream.close();
				btSocket.close();
			} catch (Exception cl) {
				cl.printStackTrace();
			}

			return false;
		}

	}

	// step 2 (BLE) - called in onCreate
	private void leConnect() {
		Log.d(TAG, "leConnect()");

		setupAndroidBluetoothConnection();
		connectMeter();
	}

	// step 2.1 (BLE) - called in leConnect
	private void setupAndroidBluetoothConnection() {
		Log.d(TAG, "setupAndroidBluetoothConnection() - mConnection: " + mConnection);
		if (mConnection == null) {
			try {
				mConnection = ConnectionManager.createAndroidBluetoothConnection(mBTConnectionHandler);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// step 2.2 (BLE) - called in leConnect
	private void connectMeter() {
		Log.d(TAG, "#IN connectMeter()");
		new Thread(new Runnable() {
			@Override
			public void run() {
				Looper.prepare();
				try {
					Log.d(TAG, "connectMeter -> updatePairedList");
					updatePairedList();

					if (mConnection.getState() == AndroidBluetoothConnection.STATE_NONE) {
						Log.d(TAG, "connectMeter -> setLeConnectedListener");
						mConnection.setLeConnectedListener(mLeConnectedListener);

						Log.d(TAG, "connectMeter -> LeListen!!");
						mConnection.LeListen();
					}

				} catch (Exception e) {
					Log.e(TAG, e.getMessage(), e);

				} finally {
					getTemperatureValue();
				}

				Looper.loop();
			}

		}).start();
	}

	// release BLE resources
	private void leDisconnect() {
		Log.d(TAG, "leDisconnect: stopping BLE scanning/communication.");
		new Thread(new Runnable() {
			@Override
			public void run() {
				Looper.prepare();
				try {
					if (mConnection != null) {
						mConnection.setLeConnectedListener(null);
						mConnection.LeDisconnect();
						mConnection = null;
					}

				} catch (Exception e) {
					Log.e(TAG, e.getMessage(), e);
				}

				Looper.loop();
			}

		}).start();
	}

	// release BT classic resources
	private void classicDisconnect() {
		Log.d(TAG, "classicDisconnect: stopping BT classic scanning/communication.");
		if (btFora != null) {
			btFora.cancel(true);
			btFora = null;
		}

		if (btSocket != null) {
			try {
				btSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			btSocket = null;
		}
	}

	//region BLE connection handler
	private Handler mBTConnectionHandler = new Handler() {
		@TargetApi(Build.VERSION_CODES.KITKAT)
		@Override
		public void handleMessage(Message msg) {
			try {
				switch (msg.what) {
					case PCLinkLibraryConstant.MESSAGE_STATE_CHANGE:

						int mLeConnectionState = msg.arg1;
						Log.d(TAG, "mBTConnectionHandler#handleMessage -- MESSAGE_STATE_CHANGE: " + mLeConnectionState);

						switch (msg.arg1) {
							case AndroidBluetoothConnection.STATE_CONNECTED_BY_LISTEN_MODE:

								try {
									Log.d(TAG, "mBTConnectionHandler#handleMessage -> set mForaMeter. mConnection: " + mConnection);
									mForaMeter = MeterManager.detectConnectedMeter(mConnection);

								} catch (Exception e) {
									e.printStackTrace();
									throw new NotSupportMeterException();
								}
								if (mForaMeter == null) {
									throw new NotSupportMeterException();
								}
								break;

							case AndroidBluetoothConnection.STATE_CONNECTING:
								Log.d(TAG, "mBTConnectionHandler#handleMessage: connecting");
								break;

							case AndroidBluetoothConnection.STATE_SCANED_DEVICE:
								// LE sensor found, release BT classic resources
								classicDisconnect();
								// save the sensor type as BLE
								saveSensorBluetoothType(macAddress, SENSOR_TYPE_BLE);

								Log.d(TAG, "mBTConnectionHandler#handleMessage -- connected device address: " + mConnection.getConnectedDeviceAddress());
								BluetoothDevice device = BluetoothUtil.getPairedDevice(mConnection.getConnectedDeviceAddress());
								mConnection.LeConnect(getApplicationContext(), device);

								break;

							case AndroidBluetoothConnection.STATE_LISTEN:
								break;

							case AndroidBluetoothConnection.STATE_NONE:
								break;

							case AndroidBluetoothConnection.STATE_BLUETOOTH_STACK_NULL_EXCEPTION:
								recoverBluetooth();
								break;
						} /* end of switch */
						break;

				} /* end of switch */

				Log.d(TAG, "mBTConnectionHandler#handleMessage -- Message: " + msg.what);

			} catch (NotSupportMeterException e) {
				Log.e(TAG, "not support meter", e);
			}
		}
	};
	//endregion

	private AndroidBluetoothConnection.LeConnectedListener mLeConnectedListener = new AndroidBluetoothConnection.LeConnectedListener() {

		@Override
		public void onConnectionStateChange_Disconnect(BluetoothGatt gatt, int status, int newState) {
			mConnection.LeDisconnect();
			if (shouldRecoverBluetooth())
				recoverBluetooth();
		}

		@SuppressLint("NewApi")
		@Override
		public void onDescriptorWrite_Complete(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
			Log.d(TAG , "onDescriptorWrite_Complete" );
			mConnection.LeConnected(gatt.getDevice());
		}

		@Override
		public void onCharacteristicChanged_Notify(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

			new Thread(new Runnable() {
				@TargetApi(Build.VERSION_CODES.KITKAT)
				@Override
				public void run() {
					Looper.prepare();

					try {
						mForaMeter = MeterManager.detectConnectedMeter(mConnection);
						Log.e(TAG, "mForaMeter reference: " + mForaMeter);
						Log.e(TAG, "mConnection reference: " + mConnection);

						// set sensor as "paired"
						SharedPreferences pairingSharedPreferences = getSharedPreferences(CommonUtilities.NION_SP, 0);
						SharedPreferences.Editor editor = pairingSharedPreferences.edit();
						editor.putString("fora_paired", "pair");
						editor.apply();

						Log.d(TAG, "onCharacteristicChanged_Notify - run: status updated to paired");

					} catch (Exception e) {
						e.printStackTrace();
						mConnection.LeDisconnect(); // disconnect and try again

						// nullifying the mConnection reference makes the updatePairedList() method create a new connection.
						// this makes the connection more robust in the P724 tablet (even if the sensor disconnects, it always reconnects)
						if (Build.MODEL.equals("MN-724"))
							mConnection = null;
					}

					Looper.loop();
				}
			}).start();
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {}
	};

	// updates the mConnection with the assigned MAC address
	private void updatePairedList() {
		Map<String, String> addrs = new HashMap<String, String>();
		String addrKey = "BLE_PAIRED_METER_ADDR_" + String.valueOf(0);
		addrs.put(addrKey, macAddress);

		Log.d(TAG, "updatePairedList -- set mConnection");

		if (mConnection == null) {
			mConnection = ConnectionManager.createAndroidBluetoothConnection(mBTConnectionHandler);
		}

		Log.d(TAG, "updatedPairedList -- MAC address: " + macAddress);
		mConnection.updatePairedList(addrs, 1);
	}

	public void getTemperatureValue() {
		try {
			if (mForaMeter == null) {

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						// check whether the activity that is currently shown on screen is in fact the session, rather than the test device page
						ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
						ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
						String mClassName = cn.getClassName();
						if (!mClassName.equals(ForaMainActivity.this.getClass().getCanonicalName())) {
							Log.d(TAG, "getTemperatureValue#run: trying to run the thread while outside of the activity.");
							return;
						}

						Log.d(TAG, "getTemperatureValue -> connectMeter (handler). mForaMeter is NULL mConnection isNull: " + (mConnection == null));

						setupAndroidBluetoothConnection();
						connectMeter();
					}
				}, 10000);

			} else {
				Log.e(TAG, "ForaMeter reference is NOT NULL");

				AbstractRecord record = mForaMeter.getStorageDataRecord(0, PCLinkLibraryEnum.User.CurrentUser);

				double valueInFahrenheit = ((TemperatureRecord) record).getObjectTemperatureValue();

				double finalValue = Util.round((valueInFahrenheit * (1.8)) + 32);
				result = String.valueOf(finalValue);

				insertReading(finalValue);
				startResultActivity(result, tempId);

				try {
					mForaMeter.turnOffMeterOrBluetooth(0);
					leDisconnect();
					mForaMeter = null;

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// on Text to Speech initialization
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.US);
			Log.i(TAG, "Initilization Success");
			tts.setSpeechRate(0); // set speech speed rate
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e(TAG, "Language is not supported");
			} else {
				// speakOut();
			}
		} else {
			Log.e(TAG, "Initilization Failed");
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, " KEYCODE_BACK clicked: fora");

			Intent intent = new Intent(this, FinalMainActivity.class);
			this.finish();
			startActivity(intent);
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onDestroy() {

		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		super.onDestroy();
	}

	//region Helpers
	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		Log.i(TAG, "DROID : Connect to macAddress " + macAddress);

		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}
	public double round(double d) {
		DecimalFormat twoDForm = new DecimalFormat("#.#");
		return Double.valueOf(twoDForm.format(d));
	}
	public String getHex(byte[] raw, int len) {
		Log.i(TAG, "Get Hex...");
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(3 * raw.length);
		for (final byte b : raw) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F))).append(" ");
			if (--len == 0)
				break;
		}
		return hex.toString();
	}
	static String byteArrayToHexString(byte in[]) {

		byte ch = 0x00;
		int i = 0;
		if (in == null || in.length <= 0)
			return null;

		String pseudo[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };

		StringBuffer out = new StringBuffer(in.length * 2);
		while (i < in.length) {
			ch = (byte) (in[i] & 0xF0); // Strip off high nibble
			ch = (byte) (ch >>> 4);
			// shift the bits down
			ch = (byte) (ch & 0x0F);
			// must do this is high order bit is on!
			out.append(pseudo[(int) ch]); // convert the nibble to a String
			// Character
			ch = (byte) (in[i] & 0x0F); // Strip off low nibble
			out.append(pseudo[(int) ch]); // convert the nibble to a String
			// Character
			i++;
		}

		String rslt = new String(out);

		return rslt;

	}
	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	public BluetoothDevice getDevice(String devName) {

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
			// Device does not support Bluetooth

			Log.i(TAG, " Device does NOT support Bluetooth");
			return null;
		}

		// check whether bluetooth is enabled
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}

		if ((mBluetoothAdapter != null)) {
			setProgressBarIndeterminateVisibility(true);

			// Paired devices
			Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
					.getBondedDevices();
			if (pairedDevices.size() > 0) {
				for (BluetoothDevice device : pairedDevices) {
					// Add the name and address to an array adapter to show in a ListView
					if (device.getName().contains(devName)) {
						// "ANIL-PC", BlackBerry 9800,UC-321PBT, NONIN
						return device;
					}
				}
			}
		}
		return null;
	}

	private void insertReading(double value) {
		ClassTemperature temperature = new ClassTemperature();
		temperature.setTemperatureValue(value + "");
		temperature.setInputmode(0);
		SharedPreferences settings1 = getSharedPreferences( CommonUtilities.PREFS_NAME_date, 0);
		String sectionDate = settings1.getString("sectiondate", "0");

		SharedPreferences settings = getSharedPreferences( CommonUtilities.USER_TIMESLOT_SP, 0);
		String slot = settings.getString("timeslot", "AM");

		temperature.setSectionDate(sectionDate);
		temperature.setTimeslot(slot);
		tempId = dbtemperature.InsertTemperatureMeasurement(temperature); // tempId is a member!
	}

	private void startResultActivity(String value, int id) {
		if (progressDialog != null)
			progressDialog.dismiss();

		if (value.trim().length() > 0) {
			try {
				Intent intent = new Intent(ForaMainActivity.this, ShowTemperatureActivity.class);
				intent.putExtra("temp", value);
				intent.putExtra("tempid", id);

				startActivity(intent);
				ForaMainActivity.this.finish();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void saveSensorBluetoothType(String mac, int type) {
		SharedPreferences preferences = getSharedPreferences("", 0);
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(mac, type);
		editor.apply();
	}
	private int getSensorBluetoothType(String mac) {
		SharedPreferences preferences = getSharedPreferences("", 0);
		int type = preferences.getInt(mac, SENSOR_TYPE_UNKNOWN);

		Log.d(TAG, "getSensorBluetoothType: type: " + type);

		return type;
	}

	// this method is called when disconnecting from a BLE connection
	private boolean shouldRecoverBluetooth() {
		boolean shouldRecover = mBluetoothSocketTimedOut && getSensorBluetoothType(macAddress) != SENSOR_TYPE_CLASSIC;

		failedBleConnectionCount++;
		shouldRecover = shouldRecover || failedBleConnectionCount >= 2;

		Log.d(TAG, "shouldRecoverBluetooth: shouldRecover: " + shouldRecover);

		return shouldRecover;
	}
	private void recoverBluetooth() {
		Log.d(TAG, "recoverBluetooth: called.");
		if (mBluetoothAdapter != null) {
			mBluetoothAdapter.disable();

			// reinitialize recovery variables
			mBluetoothSocketTimedOut = false;
			failedBleConnectionCount = 0;
		}
	}
	//endregion

	//region UI methods/helpers
	private void createOKProgressDialog(String title, String message) {
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle(title);
		progressDialog.setMessage(message);
		progressDialog.setCancelable(false);
		progressDialog.setButton(DialogInterface.BUTTON_POSITIVE,
				this.getString(R.string.OK),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						btFora.cancel(true); // Canceling AsyTask
						if (dialog != null)
							dialog.dismiss();
						progressDialog.dismiss();
						if (tts != null) {
							tts.stop();
							tts.shutdown();
						}
						RedirectionActivity();

					}
				});
		progressDialog.show();
	}
	private void RedirectionActivity() {
		if (deviceType == 0) {

			Intent intent = new Intent(getApplicationContext(), UploadMeasurement.class);
			intent.putExtra("reType", 1);

			finish();
			startActivity(intent);
			// redirect to pulse
		} else if (deviceType == 1) {
			String macaddress = getForaMAC(dbSensor.SelectTempSensorName());
			Intent intentfr = new Intent(getApplicationContext(), ForaMainActivity.class);
			intentfr.putExtra("deviceType", 2);

			intentfr.putExtra("macaddress", macaddress);
			finish();
			startActivity(intentfr);

		} else if (deviceType == 2) {
			Intent intent = new Intent(getApplicationContext(), GlucoseEntry.class);

			finish();
			startActivity(intent);
		}
	}
	private void setwelcome() {

		String deviceMessage;

		String Pin = dbSensor.SelectTempPIN();
		Constants.setPIN(Pin);// set pin for auto pairing

		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")) {
			Util.soundPlay("Messages/TakeTemp_1.wav");

		} else {
			Util.soundPlay("Messages/TakeTemp.wav");
		}

		deviceMessage = this.getString(R.string.entertemperaturebluetooth);
		txtwelcom.setText(deviceMessage);

		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
		AnimPlz.setDuration(1000);
		AnimPlz.start();

		AnimPlz.addListener(new AnimatorListenerAdapter() {
			public void onAnimationEnd(Animator animation) {

				txtReading.setVisibility(View.VISIBLE);
				rocketImage.setVisibility(View.VISIBLE);

			}
		});

	}
	public Boolean preference() {
		Boolean flag = null;
		try {

			String PREFS_NAME = "DroidPrefSc";
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			int scheduletatus = settings.getInt("Scheduletatus", -1);
			Log.i("Droid", " SharedPreferences : " + getApplicationContext()
					+ " is : " + scheduletatus);
			if (scheduletatus == 1)
				flag = true;
			else
				flag = false;
		}

		catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		return flag;
	}
	//endregion
}
