package com.tkm.optumSierra;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.telephony.TelephonyManager;
import com.tkm.optumSierra.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.tkm.optumSierra.bean.ClassSensor;
import com.tkm.optumSierra.bean.PairedDevice;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.LogcatCapture;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Util;

/**
 * Global Application class-- Used to store temp data Data is maintained when
 * ever the app is available in background or foreground thread.
 *
 */
public class GlobalClass extends Application implements OnInitListener{

	OnInit_Data_Access dataAccess = new OnInit_Data_Access();
	public ClassSensor selectedSensor;
	public String selectedFlag;
	public int selectedPos;
	public boolean isEllipsisEnable = true;
	public boolean isSupportEnable = true;
	public boolean is_omron_start_flow = true;
	public TextToSpeech tts;
	public BroadcastReceiver mReceiver;
	public boolean isSessionStart = false;
	//public boolean isRemSessionStart = false;
	public boolean isAlarmUtilStart = false;
	@Override
	public void onCreate() {
		dataAccess.FileReaderMethod();
		OnInit_Data_Access.ctx = getApplicationContext();
		super.onCreate();
		tts = new TextToSpeech(this, this);

		mReceiver = new range_receiver();
		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		registerReceiver(mReceiver, filter);

		// capture logcat logs in a file if debug mode is active
		if (com.tkm.optumSierra.util.Log.FILE_LOG_ACTIVE)
			LogcatCapture.startCapturing();

	}

	public class range_receiver extends BroadcastReceiver {

		long screenOffTime = 0;
		long screenOnTimeDiff = 0;
		@Override
		public void onReceive(Context context, Intent intent) {

			// receiver to detect screen lock event
			if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {

				screenOffTime = SystemClock.uptimeMillis();
				if(isSessionStart){

					PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
					WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
					wakeLock.acquire();

				}

			}
			if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {

				screenOnTimeDiff = SystemClock.uptimeMillis() - screenOffTime;
				int secs = (int) (screenOnTimeDiff / 1000);
				int mins = secs / 60;
				if(mins >= 10){

					Intent finalIntent = new Intent(getApplicationContext(), FinalMainActivity.class);
					finalIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(finalIntent);
				}

			}
		}
	}

	//private int alarm_count;
	private PowerManager.WakeLock wakelock;
	private Bundle bundle;
	public int regular_alarm_count = 0;
	public int Start_Alarm_Count = 0;

	public int getStart_Alarm_Count() {
		return Start_Alarm_Count;
	}

	public void setStart_Alarm_Count(int start_Alarm_Count) {
		Start_Alarm_Count = start_Alarm_Count;
	}

	public int getRegular_alarm_count() {
		return regular_alarm_count;
	}

	public void setRegular_alarm_count(int regular_alarm_count) {
		this.regular_alarm_count = regular_alarm_count;
	}

	private boolean Regular_Alarm_Triggered = false;

	/** flag to indicates whether regular alarm is already triggered */
	public boolean isRegular_Alarm_Triggered() {
		return Regular_Alarm_Triggered;
	}

	public void setRegular_Alarm_Triggered(boolean regular_Alarm_Triggered) {
		Regular_Alarm_Triggered = regular_Alarm_Triggered;
	}

	/**
	 * Used to manage wake lock (allows process to run during sleep) This should
	 * only be retained while required and released when all tasks are done.
	 */
	public void acquireWakelock() {
		if (wakelock == null) {
			PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			wakelock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
					"Optum_Wake_Lock");
			wakelock.setReferenceCounted(false);
		}
		wakelock.acquire();
	}

	/**
	 * Release the wake lock
	 */
	public void releaseWakelock() {
		if (wakelock != null) {
			wakelock.release();
		}
	}

	public Bundle getBundle() {
		return bundle;
	}

	public void setBundle(Bundle bundle) {
		this.bundle = bundle;
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public static boolean isAirplaneModeOn(Context context) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
			return Settings.System.getInt(context.getContentResolver(),
					Settings.System.AIRPLANE_MODE_ON, 0) != 0;
		} else {
			return Settings.Global.getInt(context.getContentResolver(),
					Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
		}
	}

	public void populateSpinner(final Spinner sItems,final int pos,final Activity activity,final String testFlag){


		Sensor_db dbSensor = new Sensor_db(this);
		final ArrayList<ClassSensor> sensorList = new ArrayList<ClassSensor>();
		ClassSensor classSensor1 = new ClassSensor();
		classSensor1.setSensorName(""+activity.getResources().getString(R.string.selectADevice));
		sensorList.add(classSensor1);
		ArrayList<PairedDevice> pairedList = new ArrayList<PairedDevice>();
		Cursor cursorsensor = dbSensor.SelectSensorDetails();
		cursorsensor.moveToFirst();

		int cnt = cursorsensor.getCount();
		if (cnt > 0) {

			while (cursorsensor.isAfterLast() == false) {

				ClassSensor classSensor = new ClassSensor();
				classSensor.setMeasureTypeId(cursorsensor.getInt(1));
				classSensor.setSensorName(
						(cursorsensor.getString(3)).contains("and") ? 
						(cursorsensor.getString(3)).replace(" and ", "&") :
						cursorsensor.getString(3));
				classSensor.setMacId(cursorsensor.getString(9));
				classSensor.setPin(cursorsensor.getString(10));
				for(int i=0;i<pairedList.size();i++){

					PairedDevice devices = pairedList.get(i);
					String macId = devices.getMacId().replace(":", "");
					if(cursorsensor.getString(9).equals(macId)){

						classSensor.setPairedStatus(1);
					}
				}
				if(!(classSensor.getSensorName().contains("Manual") || classSensor.getSensorName().contains("question"))){

					sensorList.add(classSensor);
				}

				cursorsensor.moveToNext();
			}


		}

		cursorsensor.close();
		dbSensor.cursorsensor.close();
		dbSensor.close();


		SpinnerAdapter adapter = new SpinnerAdapter(activity, android.R.layout.simple_spinner_item, sensorList,testFlag);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sItems.setAdapter(adapter);

		int preSelPos = pos;
		//		if(testFlag.equals("pair") || testFlag.equals("test")){

		sItems.setSelection(pos+1);
		//		}

		sItems.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				if(position != 0){
					((TextView) parent.getChildAt(0)).setTextColor(
							activity.getResources().getColor(R.color.oragne));
					//					selectedSensor = sensorList.get(position-1);
					//					selectedFlag = "testAll";
					//					selectedPos = position;
				}
				if(pos != position-1){

					if(position == 0){

						Intent intent = new Intent(getApplicationContext(),	TestADeviceActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
						activity.finish();
					}else{

						vitalRedirection(sensorList.get(position),"test",activity,position-1);
					}


				}


				//					if(isTestAll){
				//
				//
				//						vitalRedirection(sensorList.get(position-1),"testAll",activity,position);
				//					}



			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

	}

	private void vitalRedirection(ClassSensor sensor,String testFlag,Activity activity, int pos){

		int typeId = sensor.getMeasureTypeId();
		if(typeId == 1){

			redirectBG(sensor,testFlag,activity,pos);
		}else if(typeId == 2){

			redirectPulse(sensor,testFlag,activity,pos);
		}else if(typeId == 3){

			redirectBP(sensor,testFlag,activity,pos);
		}else if(typeId == 6){

			redirectTemp(sensor,testFlag,activity,pos);
		}else if(typeId == 7){

			redirectWT(sensor,testFlag,activity,pos);
		}
	}

	private void redirectPulse(ClassSensor sensor,String testFlag,Activity activity, int pos) {


		String Sensor_name = sensor.getSensorName();
		String Mac = sensor.getMacId();



		if (Mac.trim().length() == 12) {

			if (Sensor_name.contains("3230")) {
				if (Build.VERSION.SDK_INT >= 18) {
					Intent intent = new Intent(getApplicationContext(),
							TestDeviceBLECheckActivity.class);
					intent.putExtra("TestFlag", testFlag);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("macaddress", Mac);
					intent.putExtra("Position", pos);
					startActivity(intent);
					activity.finish();
				} else {

					Toast.makeText(getBaseContext(), "not support ble",
							Toast.LENGTH_SHORT).show();
					activity.finish();
					return;
				}
			} else if (Sensor_name.contains("9560")) {
				Intent intent = new Intent(getApplicationContext(),
						TestDeviceDataReaderActivity.class);
				intent.putExtra("macaddress", Mac);
				intent.putExtra("Position", pos);
				intent.putExtra("TestFlag", testFlag);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.finish();
				startActivity(intent);
			}

			activity.overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("Manual")) {
			Intent intent = new Intent(getApplicationContext(),
					PulseEntry.class);

			activity.finish();
			startActivity(intent);
			activity.overridePendingTransition(0, 0);

		} else {

			/*
			 * Toast.makeText(this, "Please assign Blood Oxygen.",
			 * Toast.LENGTH_LONG).show();
			 */

		}

	}

	private void redirectTemp(ClassSensor sensor,String testFlag,Activity activity, int pos) {

		String tmpMac = sensor.getMacId();

		if (tmpMac.trim().length() > 4) {
			Intent intentfr = new Intent(getApplicationContext(),
					TestDeviceForaMainActivity.class);
			intentfr.putExtra("deviceType", 2);
			intentfr.putExtra("Position", pos);
			intentfr.putExtra("TestFlag", testFlag);
			intentfr.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
			intentfr.putExtra("macaddress", getForaMAC(tmpMac));
			activity.finish();
			startActivity(intentfr);
			activity.overridePendingTransition(0, 0);

		} else if (tmpMac.equals("-1")) {
			Intent intentfr = new Intent(getApplicationContext(),
					TemperatureEntry.class);
			activity.finish();
			startActivity(intentfr);
			activity.overridePendingTransition(0, 0);
		} else {

			/*
			 * Toast.makeText(this, "Please assign Temperature .",
			 * Toast.LENGTH_LONG).show();
			 */

		}

	}

	private void redirectBP(ClassSensor sensor,String testFlag,Activity activity, int pos) {

		String Sensor_name = sensor.getSensorName();
		String Mac = sensor.getMacId();

		if (Sensor_name.contains("A&D Bluetooth smart")) {
			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						TestDeviceAandDSmart.class);
				intent.putExtra("macaddress", Mac);
				intent.putExtra("Position", pos);
				intent.putExtra("TestFlag", testFlag);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.finish();
				startActivity(intent);
				activity.overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				activity.finish();
				return;
			}

		} else if (Sensor_name.contains("Omron HEM 9200-T")) {
			if (Build.VERSION.SDK_INT >= 18) {
				Constants.setPIN(sensor.getPin());
				Intent intent = new Intent(getApplicationContext(),
						TestDeviceOmronBlsActivity.class);

				intent.putExtra("macaddress", Mac);
				intent.putExtra("TestFlag", testFlag);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.putExtra("Position", pos);
				activity.finish();
				startActivity(intent);
				activity.overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				activity.finish();
				return;
			}

		} else if (Sensor_name.contains("P724-BP")) {

			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						TestDeviceP724BpSmart.class);
				intent.putExtra("TestFlag", testFlag);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.putExtra("Position", pos);
				activity.finish();
				startActivity(intent);
				activity.overridePendingTransition(0, 0);
			} else {

				Intent intent = new Intent(this, TestDeviceP724Bp.class);
				intent.putExtra("Position", pos);
				intent.putExtra("TestFlag", testFlag);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.finish();
				startActivity(intent);
				return;
			}
		}

		else if (Sensor_name.contains("Wellex")) {

			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						TestDevicePressureWellex.class);
				intent.putExtra("Position", pos);
				intent.putExtra("macaddress", Mac);
				intent.putExtra("TestFlag", testFlag);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.finish();
				startActivity(intent);
				activity.overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				activity.finish();
				return;
			}

		}

		else if (Sensor_name.contains("A and D")) {

			if (Sensor_name.contains("UA-767BT-Ci")) {
				Intent intentwt = new Intent(getApplicationContext(),
						TestDeviceAandContinua.class);
				intentwt.putExtra("Position", pos);
				intentwt.putExtra("TestFlag", testFlag);
				intentwt.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.finish();
				startActivity(intentwt);
				activity.overridePendingTransition(0, 0);
			} else {

				Intent intentwt = new Intent(getApplicationContext(),
						TestDeviceAandDReader.class);
				intentwt.putExtra("deviceType", 1);
				intentwt.putExtra("Position", pos);
				intentwt.putExtra("TestFlag", testFlag);
				intentwt.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.finish();
				startActivity(intentwt);
				activity.overridePendingTransition(0, 0);
			}

		} else if (Sensor_name.contains("FORA")) {
			if (Mac.trim().length() == 17) {
				Intent intentfr = new Intent(getApplicationContext(),
						TestDeviceForaMainActivity.class);
				intentfr.putExtra("deviceType", 1); // pressure
				intentfr.putExtra("macaddress", Mac);
				intentfr.putExtra("Position", pos);
				intentfr.putExtra("TestFlag", testFlag);
				intentfr.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.finish();
				startActivity(intentfr);
				activity.overridePendingTransition(0, 0);
			} else if (Sensor_name.contains("Manual")) {
				Intent intentfr = new Intent(getApplicationContext(),
						PressureEntry.class);

				activity.finish();
				startActivity(intentfr);
				activity.overridePendingTransition(0, 0);

			} else {
				/*
				 * Toast.makeText(this, "Please assign Blood Pressure.",
				 * Toast.LENGTH_LONG).show();
				 */
			}
		} else if (Sensor_name.contains("Manual")) {
			Intent intentfr = new Intent(getApplicationContext(),PressureEntry.class);
			//Intent intentfr = new Intent(Home.this, OmronBlsActivity.class);

			activity.finish();
			startActivity(intentfr);
			activity.overridePendingTransition(0, 0);

		} else {

			/*
			 * Toast.makeText(this, "Please assign Blood Pressure.",
			 * Toast.LENGTH_LONG).show();
			 */
		}

	}

	private void redirectWT(ClassSensor sensor,String testFlag,Activity activity, int pos) {

		Sensor_db dbSensor = new Sensor_db(this);

		Cursor cursorsensor = dbSensor.SelectWeightSensorName();
		String Sensor_name = "", Mac = "";
		Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {
			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));
		}

		if (Sensor_name.contains("A and D")) {
			Intent intentwt = new Intent(getApplicationContext(),
					TestDeviceAandDReader.class);
			intentwt.putExtra("deviceType", 0);
			intentwt.putExtra("Position", pos);
			intentwt.putExtra("TestFlag", testFlag);
			intentwt.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.finish();
			startActivity(intentwt);
			activity.overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("FORA")) {
			Intent intentfr = new Intent(getApplicationContext(),
					TestDeviceForaMainActivity.class);
			intentfr.putExtra("deviceType", 0);
			intentfr.putExtra("macaddress", Mac);
			intentfr.putExtra("Position", pos);
			intentfr.putExtra("TestFlag", testFlag);
			intentfr.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.finish();
			startActivity(intentfr);
			activity.overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("P724")) {
			Intent intentfr = new Intent(getApplicationContext(),
					TestDeviceP724Activity.class);
			intentfr.putExtra("deviceType", 0);
			intentfr.putExtra("macaddress", Mac);
			intentfr.putExtra("Position", pos);
			intentfr.putExtra("TestFlag", testFlag);
			intentfr.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.finish();
			startActivity(intentfr);
			activity.overridePendingTransition(0, 0);
		} else if (Sensor_name.trim().length() > 0) {
			final Context context = this;
			Intent intent = new Intent(context, WeightEntry.class);
			activity.finish();
			startActivity(intent);
			activity.overridePendingTransition(0, 0);
		} else {

			/*
			 * Toast.makeText(this, "Please assign Body Weight.",
			 * Toast.LENGTH_LONG).show();
			 */

		}

	}

	private void redirectBG(ClassSensor sensor,String testFlag,Activity activity, int pos){

		String sensor_name = sensor.getSensorName();

		if (sensor_name.contains("One Touch Ultra")) {

			Intent intent = new Intent(getApplicationContext(),
					TestDeviceGlucoseReader.class);
			intent.putExtra("Position", pos);
			intent.putExtra("TestFlag", testFlag);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.finish();
			startActivity(intent);
			activity.overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Bayer")) {

			Intent intent = new Intent(getApplicationContext(),
					TestDeviceGlucoseReader.class);
			intent.putExtra("Position", pos);
			intent.putExtra("TestFlag", testFlag);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.finish();
			startActivity(intent);
			activity.overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Accu-Chek")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						TestDeviceGlucoseAccuChek.class);
				intent.putExtra("Position", pos);
				intent.putExtra("TestFlag", testFlag);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				activity.finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				activity.finish();
				startActivity(intent);

			}

		} else if (sensor_name.contains("Taidoc")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						TestDeviceGlucoseTaidoc.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.putExtra("TestFlag", testFlag);
				intent.putExtra("Position", pos);
				startActivity(intent);
				activity.finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();

				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				activity.finish();
				startActivity(intent);

			}

		}

		else if (sensor_name.trim().length() > 0) {
			Intent intent = new Intent(getApplicationContext(),
					GlucoseEntry.class);
			activity.finish();
			startActivity(intent);
		}




	}

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);


		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}

	//Field
	private UncaughtExceptionHandler defaultUEH;


	// Constructor
	public GlobalClass() {
		defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		// setup handler for uncaught exception
		Thread.setDefaultUncaughtExceptionHandler(_unCaughtExceptionHandler);
	}



	//Handler
	private Thread.UncaughtExceptionHandler _unCaughtExceptionHandler =
			new Thread.UncaughtExceptionHandler() {
		@Override
		public void uncaughtException(Thread thread, Throwable ex) {

			final Writer result = new StringWriter();
			final PrintWriter printWriter = new PrintWriter(result);
			ex.printStackTrace(printWriter);
			String stacktrace = result.toString();
			printWriter.close();
			OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "APk Details Download Error", stacktrace);
			OnInit_Data_Access.insert_incident_detail(stacktrace, Constants.After_the_tablet_starts_up);

			// Sleep for 5 sec before re-throw, to grantee to write the

			// exception in file before system take over control
			try {
				Thread.sleep(5000,0);

			} catch (InterruptedException e)
			{

				e.printStackTrace();

}
			// re-throw critical exception further to the os (important)
			defaultUEH.uncaughtException(thread, ex);
		}
	};
	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
		int result = 0;
		if (status == TextToSpeech.SUCCESS) {

			int language = Constants.getLanguage();

			if (language == 11) {

				Locale locSpanish = new Locale("es");
				result = tts.setLanguage(locSpanish);
				tts.setSpeechRate(0);

			} else {

				result = tts.setLanguage(Locale.US);
				//Log.i(TAG, "Initilization Success");
				tts.setSpeechRate(0); // set speech speed rate

			}
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				//Log.e(TAG, "Language is not supported");
			} else {
				// speakOut();
			}
		} else {
			//Log.e(TAG, "Initilization Failed");
		}
	}

	public void uploadStatus(String status){

		SharedPreferences lastStatus = getSharedPreferences(CommonUtilities.LASTUPLOAD_STATUS, 0);
		SharedPreferences.Editor lastStatusEditor = lastStatus.edit();

		if(status.equals("success"))
			lastStatusEditor.putString("upload_status", "success");
		else
			lastStatusEditor.putString("upload_status", "failed");
		lastStatusEditor.commit();
	}

	public void uploadStatusTime(){

		SharedPreferences lastStatus = getSharedPreferences(CommonUtilities.LASTUPLOAD_STATUS, 0);
		SharedPreferences.Editor lastStatusEditor = lastStatus.edit();
		lastStatusEditor.putString("upload_time", Util.get_patient_time_zone_time_LastActivity(this));
		lastStatusEditor.commit();
	}



//	public void uploadSessionStatus(String status){
//
//		SharedPreferences lastStatus = getSharedPreferences(CommonUtilities.LASTUPLOAD_STATUS, 0);
//		SharedPreferences.Editor lastStatusEditor = lastStatus.edit();
//
//		if(status.equals("success"))
//			lastStatusEditor.putString("upload_session_status", "success");
//		else
//			lastStatusEditor.putString("upload_session_status", "failed");
//		lastStatusEditor.commit();
//	}
//
//	public void uploadSessionStatusTime(){
//
//		SharedPreferences lastStatus = getSharedPreferences(CommonUtilities.LASTUPLOAD_STATUS, 0);
//		SharedPreferences.Editor lastStatusEditor = lastStatus.edit();
//		lastStatusEditor.putString("upload_session_time", Util.get_patient_time_zone_time_LastActivity(this));
//		lastStatusEditor.commit();
//	}

	public String get_myData_date() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        Date today = new Date();
        String dt = df.format(today);
        return dt;
    }

}
