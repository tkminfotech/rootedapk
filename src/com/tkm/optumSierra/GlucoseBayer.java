package com.tkm.optumSierra;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import com.tkm.optumSierra.GlucoseReader.BackgroundThread;
import com.tkm.optumSierra.bean.ClassMeasurement;
import com.tkm.optumSierra.dal.Glucose_db;
import com.tkm.optumSierra.service.AdviceService;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import com.tkm.optumSierra.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class GlucoseBayer extends Titlewindow {
	private String TAG = "P724Activity Sierra";
	public static final String PREFS_NAME_SCHEDULE = "DroidPrefSc";
	static final String HEXES = "0123456789ABCDEF";
	public static final int PROGRESS_BAR_TYPE = 0;
	public int back = 0;
	int glucoseId;
	TextView textView = null;
	ImageView rocketImage;
	int measureId;
	AnimationDrawable rocketAnimation;
	BackgroundThread backgroundThread;
	Util utilobjct = new Util();
	ObjectAnimator AnimPlz, ObjectAnimator;
	public boolean cancel_flag = false;
	TextView txtwelcom, txtReading;
	boolean started = false;
	private BluetoothServerSocket mmServerSocket;
	BluetoothAdapter mBluetoothAdapter;
	private int mPort = -1;
	ByteArrayOutputStream mPacket = new ByteArrayOutputStream(128);
	private static final String NAME = "PWAccessP";
	// "AMC_JandJ";
	// "PWAccessP";
	// SPP UUID
	private static final UUID MY_UUID = UUID
			.fromString("00001234-0000-1000-8000-00805F9B34FB");

	public void onInit(int status) {
		/*
		 * if (status == TextToSpeech.SUCCESS) { int result =
		 * tts.setLanguage(Locale.US); Log.i(TAG, "Initilization Success");
		 * tts.setSpeechRate(0); // set speech speed rate if (result ==
		 * TextToSpeech.LANG_MISSING_DATA || result ==
		 * TextToSpeech.LANG_NOT_SUPPORTED) { Log.e(TAG,
		 * "Language is not supported"); } else { // speakOut(); } } else {
		 * Log.e(TAG, "TTS Initilization Failed"); }
		 */

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aand_dreader);
		Log.e(TAG, "P724 BT page started");

		Button btnmanual = (Button) findViewById(R.id.manuval);

		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		// txtwelcom.setText("Please step on the scale.");
		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);
		
		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		rocketImage.setVisibility(View.INVISIBLE);
		txtReading.setVisibility(View.INVISIBLE);

		//stopService(new Intent(GlucoseBayer.this, AdviceService.class));

		btnmanual.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click

				Intent intentwt = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				finish();
				startActivity(intentwt);

			}
		});

		backgroundThread = new BackgroundThread();
		backgroundThread.setRunning(true);
		backgroundThread.start();
		Log.i(TAG, "-------------------oncreate--------------  Bayer BG activity");
	}

	public class BackgroundThread extends Thread {
		volatile boolean running = false;
		int cnt;

		void setRunning(boolean b) {
			running = b;
			cnt = 6;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while (running) {
				try {
					sleep(60);
					if (cnt-- == 0) {
						running = false;
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			handler.sendMessage(handler.obtainMessage());
		}
	}

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub

			setProgressBarIndeterminateVisibility(false);
			// progressDialog.dismiss();

			boolean retry = true;
			while (retry) {
				try {
					backgroundThread.join();
					retry = false;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			setwelcome();
		}

	};

	private void setwelcome() {

		Util.soundPlay("Messages/BloodGlucose.wav");

		txtwelcom.setText(R.string.pleae_take_blood_glucose);
		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
		AnimPlz.setDuration(1500);
		AnimPlz.start();

		AnimPlz.addListener(new AnimatorListenerAdapter() {
			public void onAnimationEnd(Animator animation) {

				Log.e(TAG, "AnimPlz");

				txtReading.setVisibility(View.VISIBLE);
				rocketImage.setVisibility(View.VISIBLE);

			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Method m, n;
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		AcceptThread aa = new AcceptThread();
		aa.start();

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.d("onActivityResult ", "onActivityResult " + resultCode);
		Log.d("onActivityResult ", "requestCode is " + requestCode);

		switch (requestCode) {
		case 1:
			// When the request to enable Bluetooth returns
			Log.d("onactivityresult", "new thread created");
			AcceptThread aa = new AcceptThread();
			aa.start();
			started = true;

		}
	}

	public synchronized void connected(BluetoothSocket socket) {

		String TAG = "....connected..........";
		Log.d(TAG, "connected");

		// Start the thread to manage the connection and perform transmissions
		ConnectedThread mConnectedThread = new ConnectedThread(socket);
		mConnectedThread.start();

	}

	private int getPortNr() {
		try {
			// Retrieve the port number. For A&D devices you need this.
			Field mSocketField = BluetoothServerSocket.class
					.getDeclaredField("mSocket");
			mSocketField.setAccessible(true);
			BluetoothSocket socket = (BluetoothSocket) mSocketField
					.get(mmServerSocket);
			mSocketField.setAccessible(false);

			Field mPortField = BluetoothSocket.class.getDeclaredField("mPort");
			mPortField.setAccessible(true);
			int port = (Integer) mPortField.get(socket);
			mPortField.setAccessible(false);

			Log.d("getport",
					"BluetoothListenThread:getPortNr: Listening Port: " + port);

			return port;
		} catch (Exception e) {
			Log.e("getpo rt", "SensorService: getPortNr ", e);
			return -1;
		}
	}

	@SuppressLint("NewApi")
	private class AcceptThread extends Thread {

		public AcceptThread() {
			Log.d("reached", ".........AcceptThread().......");

			// Use a temporary object that is later assigned to mmServerSocket,
			// because mmServerSocket is final

			BluetoothServerSocket tmp = null;

			if (mPort == -1) {
				try {

					tmp = mBluetoothAdapter
							.listenUsingInsecureRfcommWithServiceRecord(NAME,
									MY_UUID);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.d("catsh", "catsh" + mPort);
				}
				// tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME,
				// MY_UUID);
				// tmp =
				// mAdapter.getRemoteDevice("00:80:25:C3:05:FF").createRfcommSocketToServiceRecord(MY_UUID);
				mmServerSocket = tmp;
				mPort = getPortNr();
				mPort = 4;
			}

			Log.d("AcceptThread", "In AcceptThread mPort is...... " + mPort);

			// Only listen to this specific port if we have one
			Method m;
			try {
				m = mBluetoothAdapter.getClass().getMethod(
						"listenUsingInsecureRfcommOn",
						new Class[] { int.class });
				tmp = (BluetoothServerSocket) m
						.invoke(mBluetoothAdapter, mPort);
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			mmServerSocket = tmp;
		}

		public void run() {
			BluetoothSocket socket = null;
			// Keep listening until exception occurs or a socket is returned
			while (true) {
				try {
					Log.d("AcceptThread", "listening..............");

					socket = mmServerSocket.accept();
				} catch (IOException e) {
					Log.d("AcceptThread", "not listening");
					break;
				}
				// If a connection was accepted
				if (socket != null) {
					// Do work to manage the connection (in a separate thread)
					Log.d("reached", ".........connected().......");
					connected(socket);
					try {
						if (mmServerSocket != null)
							mmServerSocket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				}
			}
		}

		/** Will cancel the listening socket, and cause the thread to finish */
		public void cancel() {
			try {
				mmServerSocket.close();
			} catch (IOException e) {
			}
		}
	}

	/**
	 * This thread runs during a connection with a remote device. It handles all
	 * incoming and outgoing transmissions.
	 */
	private class ConnectedThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;
		String TAG = "connected Thread";

		

		public ConnectedThread(BluetoothSocket socket) {
			Log.d(TAG, "create ConnectedThread...." + socket);
			mmSocket = socket;
			Log.d(TAG, "print socket " + socket.getRemoteDevice());
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			// Get the BluetoothSocket input and output streams
			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;

		}

		public void run() {

			Log.i(TAG, "BEGIN mConnectedThread");

			// Keep listening to the InputStream while connected
			while (true) {
				try {
					int repeat = 0;
					while ((mmInStream.available() <= 0) && (repeat >= 0)) {
						try {
							Log.i(TAG, "inside repeat");
							Thread.sleep(500);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						repeat--;
					}

					byte[] buffer = new byte[1024];
					int bytes;

					// Read from the InputStream
					bytes = mmInStream.read(buffer);
					Log.i(TAG, "Received  data from stream");

					Log.i(TAG,
							"Received " + bytes + " bytes" + buffer.toString());
					processInput(bytes, buffer);
					// Send the obtained bytes to the UI Activity
					// mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
					// .sendToTarget();
					write("PWA4".getBytes());
				} catch (IOException e) {
					Log.e(TAG, "disconnected a and d");
					// connectionLost();
					break;
				}
			}
		
		}

		public void write(byte[] buffer) {
			try {
				// cleast data
				mmOutStream.write(buffer);
				mmOutStream.flush();

				// Share the sent message back to the UI Activity
				// mHandler.obtainMessage(MESSAGE_WRITE, -1, -1, buffer)
				// .sendToTarget();
			} catch (IOException e) {
				Log.e(TAG, "Exception during write");
			}
		}

		public void cancel() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed");
			}
		}
	}

	public byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
					.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

	public static String getHex(byte[] raw, int len) {
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(3 * raw.length);
		for (final byte b : raw) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4))
					.append(HEXES.charAt((b & 0x0F))).append(" ");
			if (--len == 0)
				break;
		}
		return hex.toString();
	}

	// Process bytes arriving from the device
	public String getCheckSum(ArrayList<String> data) {

		int checkSum = 0;
		for (int i = 0; i < data.size(); i++)
			checkSum = checkSum + Integer.parseInt(data.get(i), 16);

		Log.e("Check Sum",
				"" + checkSum + " hex :" + Integer.toHexString(checkSum));
		String sum = "FD";
		int Check = Integer.parseInt(sum, 16) - checkSum;

		String test = Integer.toHexString(Check);
		String C = test.substring(test.length() - 2);

		Log.e("Check Sum ", "" + C);
		return C;
	}

	public static String convert(int n) {
		return Integer.toHexString(n);
	}

	private byte[] processDateTime(StringBuffer sb) {

		boolean reqSent = false;
		String TAG = "processDateTime";
		Log.i(TAG, "Received  data from stream");

		Log.i(TAG, "Received bytes");
		ArrayList<String> notification = new ArrayList<String>();
		// String senddata =
		// "80 0F 32 02 47 48 5A 36 38 33 36 43 59 00 DE";
		notification.clear();

		String data = sb.toString();
		String[] x = data.split(" ");

		Log.d("", data);

		// collect array
		for (int i = 0; i <= x.length - 1; i++) {
			notification.add(x[i]);
		}

		// Create reqdata commmand from notification array
		ArrayList<String> reqData = new ArrayList<String>();

		reqData.add("80");
		reqData.add("15");
		reqData.add("E6");

		// String senddata2 =
		// "800F320247485A36383336435900DE";
		for (int i = 3; i < 14; i++)
			reqData.add(notification.get(i));
		// reqData = notification.toString();

		Calendar c = Calendar.getInstance();

		int year = c.get(Calendar.YEAR) - 2000;
		if (convert(year).length() > 1)
			reqData.add("" + convert(year));
		else
			reqData.add("0" + convert(year));

		int month = c.get(Calendar.MONTH) + 1;
		if (convert(month).length() > 1)
			reqData.add("" + convert(month));
		else
			reqData.add("0" + convert(month));

		int day = c.get(Calendar.DAY_OF_MONTH);
		if (convert(day).length() > 1)
			reqData.add("" + convert(day));
		else
			reqData.add("0" + convert(day));

		int hour = c.get(Calendar.HOUR_OF_DAY);
		if (hour == 0) {
			hour = 12;
		}
		if (convert(hour).length() > 1)
			reqData.add("" + convert(hour));
		else
			reqData.add("0" + convert(hour));

		int min = c.get(Calendar.MINUTE);
		if (convert(min).length() > 1)
			reqData.add("" + convert(min));
		else
			reqData.add("0" + convert(min));

		int seconds = c.get(Calendar.SECOND);
		if (convert(seconds).length() > 1)
			reqData.add("" + convert(seconds));
		else
			reqData.add("0" + convert(seconds));

		String datatime = "" + year + " " + month + " " + day + " " + hour
				+ " " + min + " " + seconds;

		String check = getCheckSum(reqData);

		String command = "";

		for (int i = 0; i < reqData.size(); i++)
			command = command.concat(reqData.get(i));

		command = command.concat(check);
		command = command.toUpperCase();
		Log.e("reqData",
				command + "      " + datatime + "   " + command.length());

		byte[] ww = hexStringToByteArray(command);
		return ww;
	}

	private byte[] processReq(StringBuffer sb) {

		boolean reqSent = false;
		String TAG = "processReq";
		Log.i(TAG, "Received  data from stream");

		Log.i(TAG, "Received bytes");
		ArrayList<String> notification = new ArrayList<String>();
		// String senddata =
		// "80 0F 32 02 47 48 5A 36 38 33 36 43 59 00 DE";
		notification.clear();

		String data = sb.toString();
		String[] x = data.split(" ");

		Log.d("", data);

		// collect array
		for (int i = 0; i <= x.length - 1; i++) {
			notification.add(x[i]);
		}

		// Create reqdata commmand from notification array
		ArrayList<String> reqData = new ArrayList<String>();

		reqData.add("80");
		reqData.add("0F");
		reqData.add("32");

		// String senddata2 =
		// "800F320247485A36383336435900DE";
		for (int i = 3; i < 14; i++)
			reqData.add(notification.get(i));
		// reqData = notification.toString();

		String check = getCheckSum(reqData);

		String command = "";

		for (int i = 0; i < reqData.size(); i++)
			command = command.concat(reqData.get(i));

		command = command.concat(check);
		Log.e("reqData", command);

		byte[] dd = hexStringToByteArray(command);

		return dd;
	}

	String output = "";

	private void processInput(int len, byte[] rxpacket) {
		
		String TAG = " process";
		mPacket.write(rxpacket, 0, len);
		String data = getHex(rxpacket, len);
		Log.d(TAG, "Hex data<<< " + data);
		// Log.i(TAG, "Received " + rxpacket + " bytes");z
		String[] x =data.split(" ");
		
		

		/*boolean ack = false;
		String TAG = " process";
		// mPacket.write(rxpacket, 0, len);
		String data = sb.toString();
		Log.d(TAG, "Hex data received<<< " + data);
		// Log.i(TAG, "Received " + rxpacket + " bytes");z
		String[] x = data.split(" ");*/
		
		String deviceType;
		if (x.length > 3) {
			// stx correct
			if (x[2].equals("33") && x.length > 23) {

				String index = x[14].concat(x[15]);
				String hex = x[22].concat(x[23]);

				int year = Integer.parseInt(x[16], 16) + 2000;
				int month = Integer.parseInt(x[17], 16);
				int day = Integer.parseInt(x[18], 16);
				int hrs = Integer.parseInt(x[19], 16);
				int mnts = Integer.parseInt(x[20], 16);
				int sec = Integer.parseInt(x[21], 16);

				// byte size = rxpacket[1];
				int indvalue = Integer.parseInt(index, 16);
				int value = Integer.parseInt(hex, 16);
				Log.d(TAG, "Hex data for process<<< " + "value is " + value);
				Intent intent = new Intent(GlucoseBayer.this,
						ShowGlucoseActivity.class);

				String unit = x[24];
				if (unit.equals("00")) {
					unit = "mg/dl";
				} else {
					unit = "mmol/L";
				}

				deviceType = x[25];
				/*
				 * switch (deviceType) { case "00": deviceType = "Ultra"; break;
				 * 
				 * case "01": deviceType = "Ultra2"; break;
				 * 
				 * case "02": deviceType = "Ultra Mini/Easy"; break; case "03":
				 * deviceType = "Contour"; break;
				 * 
				 * }
				 */
				
				output += "index:" + indvalue + " Value: " + value + " " + unit
						+ "  " + day + "/" + month + "/" + year + "  " + hrs
						+ ":" + mnts + ":" + sec + "   device type: "
						+ deviceType + "\n";
				Log.d(TAG, "output<<< " + output);
				
				if (indvalue == 0) {
					glucose_valuew(value + "");
					intent.putExtra("text", value + "");
					intent.putExtra("glucoseid", glucoseId);
					intent.putExtra("Reading", output);
					startActivity(intent);
				}
				//ack = true;
				//return ack;
			} else if (x[2].equals("35")) {

				/*
				 * Intent intent = new Intent(GlucoseBayer.this,
				 * ShowGlucoseActivity.class); // output = ""; output +=
				 * "No DATA Available"; intent.putExtra("Reading", output);
				 * startActivity(intent);
				 */
				//ack = false;
				//return ack;

			}
		}
		if (x[0].equals("80"))
		{
			Log.d(TAG, "inside<<<  x[0]");
		}
		
	}

	private void glucose_valuew(String data) {
		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = flow.getInt("flow", 0); // #1
		if (val == 1) {
			
			SharedPreferences section = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);
			SharedPreferences.Editor editor_retake = section.edit();
			editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(this));
			editor_retake.commit();
		}
		Glucose_db dbcreateglucose = new Glucose_db(GlucoseBayer.this);
		int Patientid = Constants.getdroidPatientid();
		ClassMeasurement glucose = new ClassMeasurement();

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"MM/dd/yyyy hh:mm:ss aa");
		String currentDateandTime = dateFormat.format(new Date());
		glucose.setLast_Update_Date(currentDateandTime);
		glucose.setStatus(0);
		glucose.setGlucose_Value(Integer.parseInt(data.toString()));
		glucose.setInputmode(0);
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.PREFS_NAME_date, 0);		
		String sectiondate=settings1.getString("sectiondate", "0");
		
		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.USER_TIMESLOT_SP, 0);
		String slot = settings.getString("timeslot", "AM");

		glucose.setSectionDate(sectiondate);
		glucose.setTimeslot(slot);
		glucose.setPrompt_flag("3");
		
		Log.e(TAG, "Saving Glucose value to db from bluetooth device");

		glucoseId = dbcreateglucose.InsertGlucoseMeasurement(glucose);
	}
	@Override
	protected void onStop() {
		/*if (!isMyAdviceServiceRunning()) {

			Intent schedule = new Intent();
			schedule.setClass(GlucoseBayer.this, AdviceService.class);
			startService(schedule); // Starting Advice Service

		}*/
		super.onStop();
	}
	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
}
