package com.tkm.optumSierra;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import com.tkm.optumSierra.bean.ClassMeasurement;
import com.tkm.optumSierra.dal.Glucose_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.CustomKeyboard;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ActionBar.LayoutParams;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import com.tkm.optumSierra.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class GlucoseEntry extends Titlewindow {

	private TextToSpeech tts;
	int glucoseId;
	Dialog dialog;
	private View pview_back;
	private static final String TAG = "Glucose Manual Entry Sierra";
	RadioButton Pre_Meal, Post_Meal;
	int Prompt_Flag = 3;
	private GlobalClass appClass;
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// switch (msg.what) {

			

			Glucose_db dbcreateglucose = new Glucose_db(GlucoseEntry.this);

			EditText edtpulse = (EditText) findViewById(R.id.glucosevalue);
			if ((edtpulse.getText().toString()).length() != 0
					&& (edtpulse.getText().toString()).length() >= 1
					&& !(edtpulse.getText().toString()).equals("000")
					&& !(edtpulse.getText().toString()).equals("00")
					&& !(edtpulse.getText().toString()).equals(".0")
					&& !(edtpulse.getText().toString()).equals("0")
					&& !(edtpulse.getText().toString()).equals("0.0")
					&& !(edtpulse.getText().toString()).contains(".")) {

				int Patientid = Constants.getdroidPatientid();
				ClassMeasurement glucose = new ClassMeasurement();
				
				SharedPreferences flow = getSharedPreferences(
						CommonUtilities.USER_FLOW_SP, 0);
				int val = flow.getInt("flow", 0); // #1
				if (val == 1) {
					
					SharedPreferences section = getSharedPreferences(
							CommonUtilities.PREFS_NAME_date, 0);
					SharedPreferences.Editor editor_retake = section.edit();
					editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(GlucoseEntry.this));
					editor_retake.commit();
				}
				

				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"MM/dd/yyyy hh:mm:ss aa");
				String currentDateandTime = dateFormat.format(new Date());
				
				if (Pre_Meal.isChecked()) {
					Prompt_Flag = 1;
				}
				if (Post_Meal.isChecked()) {
					Prompt_Flag = 2;
				}
				
				glucose.setLast_Update_Date(currentDateandTime);
				glucose.setStatus(0);
				glucose.setGlucose_Value(Integer.parseInt(edtpulse.getText()
						.toString()));
				glucose.setInputmode(1);
				SharedPreferences settings1 = getSharedPreferences(
						CommonUtilities.PREFS_NAME_date, 0);		
				String sectiondate=settings1.getString("sectiondate", "0");
				
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.USER_TIMESLOT_SP, 0);
				String slot = settings.getString("timeslot", "AM");
				
				glucose.setSectionDate(sectiondate);
				glucose.setTimeslot(slot);
				Log.e(TAG, "PROMPT_FLAG========>"+Prompt_Flag);
				glucose.setPrompt_flag(Prompt_Flag+"");

				Log.e(TAG, "Saving Glucose value to db");
				Log.e(TAG, "current server time-"+TestDate.getCurrentTime());
				
				glucoseId=dbcreateglucose.InsertGlucoseMeasurement(glucose);

				

				Intent intent = new Intent(getApplicationContext(),
						ShowGlucoseActivity.class);
				String sss = edtpulse.getText().toString();
				intent.putExtra("text", edtpulse.getText().toString());
				intent.putExtra("glucoseid", glucoseId);
				edtpulse.setText("");
				Log.e(TAG, "Redirecting to show Glucose page");
				startActivity(intent);
				Util.stopPlaySingle();
				overridePendingTransition(0, 0);

			} else {
				Log.e(TAG, "User enter invalid measure  details");

				invalid_popup();
				return;
			}

			// insert function

			// }
		}
	};
	TextView glucoseMessage, glucoseText,glucose;

	CustomKeyboard mCustomKeyboard;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_glucose_entry);
		appClass  = (GlobalClass)getApplicationContext();
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = true;
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		pview_back = inflater
				.inflate(R.layout.activity_glucose_entry, null);
		
		try
		{
		mCustomKeyboard = new CustomKeyboard(this, R.id.keyboardview,
				R.xml.hexkbd, mHandler);
		mCustomKeyboard.registerEditText(R.id.glucosevalue);
		glucoseMessage = (TextView) findViewById(R.id.glucose);
		glucose = (TextView) findViewById(R.id.glucosemessage);
		glucoseText = (TextView) findViewById(R.id.glucose);
		
		Pre_Meal = (RadioButton) findViewById(R.id.Pre_Meal);
		Post_Meal = (RadioButton) findViewById(R.id.Post_Meal);
		
		Typeface type1 = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		glucoseMessage.setTypeface(type1, Typeface.NORMAL);
		glucoseText.setTypeface(type1, Typeface.NORMAL);
		glucose.setTypeface(type1, Typeface.NORMAL);
		
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){
			
			Util.soundPlay("Messages/BLsugarEnter_1.wav");
		}else{
			
			Util.soundPlay("Messages/BLsugarEnter.wav");
		}
		
		}
		catch (Exception e) {
			// Log.e("Droid Error",e.getMessage());
			Log.i(TAG, "oncreate  glucose Exception- ");
		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG,"KEYCODE_BACK clicked: glucose");

			Intent intent = new Intent(this, FinalMainActivity.class);

			this.finish();
			startActivity(intent); 

		}
		return super.onKeyDown(keyCode, event);
	}
	private void popup()
	{

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		dialog = adb.setView(new View(this)).create();
		// (That new View is just there to have something inside the dialog that can grow big enough to cover the whole screen.)

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = 790;
		lp.height = 500;
		dialog = new Dialog(this, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.popup_messages);
		
		TextView msg = (TextView) dialog
				.findViewById(R.id.textpopup);
		TextView mac = (TextView) dialog
				.findViewById(R.id.txtmac);	

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);		
		msg.setText(this.getString(R.string.entervalidglucosemeasurment));
		
		mac.setTypeface(type, Typeface.NORMAL);		
		mac.setVisibility(View.INVISIBLE);
		
		dialog.show();
		dialog.getWindow().setAttributes(lp);		
		closeversion = (Button) dialog.findViewById(R.id.exitversion);
		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				dialog.dismiss();
			}
		});

		
	}
	private void invalid_popup() {

		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);

		View pview;
		pview = inflater.inflate(R.layout.popup_messages, null);

		TextView msg = (TextView) pview.findViewById(R.id.textpopup);
		TextView mac = (TextView) pview.findViewById(R.id.txtmac);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);
		msg.setText(this.getString(R.string.entervalidglucosemeasurment));

		mac.setTypeface(type, Typeface.NORMAL);
		mac.setVisibility(View.INVISIBLE);

		closeversion = (Button) pview.findViewById(R.id.exitversion);

		final PopupWindow cp = new PopupWindow(pview,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		cp.setBackgroundDrawable(new BitmapDrawable(getApplicationContext()
				.getResources()));
		cp.showAtLocation(pview_back, Gravity.CENTER, 0, 0);

		// cp.update(0, 0, 350, 350);
		//cp.update(0, 0, 750, 500);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = (displaymetrics.heightPixels);
		int width = displaymetrics.widthPixels/2;
		height=(int) (height/1.2);
		

		//cp.update(0, 0, 350, 350);
		//cp.update(0, 0, 750, 500);
		cp.update(0, 0,  width,height);

		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				cp.dismiss();
			}
		});

	}
	@Override
	public void onStop() {
		
		Util.WriteLog(TAG,Constants.getdroidPatientid()+"");	

		
		super.onStop();
	}


}
