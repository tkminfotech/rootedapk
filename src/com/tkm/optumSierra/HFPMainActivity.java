package com.tkm.optumSierra;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.tkm.optumSierra.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.tkm.optumSierra.bean.ClassMeasureType;
import com.tkm.optumSierra.bean.ClassSchedule;
import com.tkm.optumSierra.bean.ClassSensor;
import com.tkm.optumSierra.bean.ClassUser;
import com.tkm.optumSierra.dal.Login_db;
import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.dal.Questions_db;
import com.tkm.optumSierra.dal.Schedule_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.service.ScheduleService;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.ConnectionDetector;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

public class HFPMainActivity extends Titlewindow {

	MeasureType_db db_measureType = new MeasureType_db(this);
	private Schedule_db dbSchedule = new Schedule_db(this);
	private static final String TAG = "MainActivity HFP";
	private static final String TAG_temp = "MainActivity HFP temp";
	public static final String PREFS_NAME = "MyLogFileCreatedDateHFP";
	public static final String PREFS_NAME_SCHEDULE = "DroidPrefSc";
	public static final String PREFS_NAME_TIME = "TimePrefSc";

	private Login_db login_db = new Login_db(this);

	private Sensor_db sensor_db = new Sensor_db(this);

	public static String serialNo = "";
	public static String imeilNo = "";
	

	public String NickName = "", ContractCode = "";
	public static String PostUrl = "";
	private static String port = "";
	private static String server = "";
	public static int PatientIdDroid;
	private int is_home_enable = 0;
	private int is_support_enable = 0;
	private int is_mydata_enable = 0;

	EditText userName;
	String log_status = "0";

	public int flag_login = 0;
	Dialog dialog;
	String message;
	private View pview_back;
private GlobalClass appClass;
	Questions_db dbcreate1 = new Questions_db(this);
	// ProgressDialog pd = null;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_hfpmain);
		appClass = (GlobalClass)getApplicationContext();
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
		pview_back = inflater.inflate(R.layout.activity_hfpmain, null);
		appClass.isSessionStart = false;
		// getWindow().getDecorView().setSystemUiVisibility(
		// View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		appClass.isAlarmUtilStart = false;
		getAlbumDir(); // creating directory if not exists

		Log.i(TAG, "HFP Main actiity started");
		Boolean isInternetPresent = (new ConnectionDetector(getApplicationContext())).isConnectingToInternet();

		SetConfig();

		// Sensor Details

		// stopService();
		// load patient details
		if (isInternetPresent) {

			Log.i(TAG, "Wifi connected , Loading patient details...");
			loadPatientDetails(); // if there is a connection , fetch
			// Patient
			// Details to local Sql_Lite table

		} else {

			/*
			 * Toast.makeText(this, this.getString(R.string.turnonwifi),
			 * Toast.LENGTH_LONG).show();
			 */
			Log.i(TAG, "No net connection...");

			ClassUser classUser = new ClassUser();

			Login_db login_db1 = new Login_db(this);

			classUser = login_db1.SelectNickName();

			NickName = classUser.getNickName();
			PatientIdDroid = classUser.getPatientId();
			CommonUtilities.Login_UserName = NickName;
			if (NickName == null) {
				{

					Toast.makeText(this, this.getString(R.string.connecttointernet), Toast.LENGTH_LONG).show();

					return;
				}
			} else {
				try {
				} catch (NullPointerException e) {

				}
			}
			LoginMember();

		}
		try {
			rebootTimer.cancel();
			timerReloader = 0;
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * Toast.makeText(HFPMainActivity.this, "Connecting to server...",
		 * Toast.LENGTH_LONG).show();
		 */

	}

	@Override
	protected void onPostResume() {
		super.onPostResume();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		// Commit the edits!
		super.onStop();
		flag_login = 1;
		clickedhpf = 1;
		// this.finish();
		Util.WriteLog(TAG, PatientIdDroid + "");
		Log.i(TAG, "onStop on main activity");

		try {
			check_logfile();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void chkuser() {
		String Condition = "";

		Condition = " type='AM/PM'";
		int type = 0;

		int cfampm = login_db.checkUserLogin(login_db.Patient_Table, Condition);

		if (cfampm == 0) {
			Condition = " type='" + timepicker() + "'"; // if AM or PM user(not
			// AM/PM user)
		} else {
			type = 1;
		}

		Log.i("Droid", "Condition : " + Condition);

		int patientLoginCount = login_db.checkUserLogin(login_db.Patient_Table, Condition);

		if (patientLoginCount == 0) { // condition for AM or PM
			chkService();
			Cursor c = login_db.UserIsPresent(login_db.Patient_Table);
			int patientCount = c.getCount();
			c.close();
			login_db.cursorLogin.close();
			login_db.close();
			if (patientCount != 0) {
				message = this.getString(R.string.weightmeasurmentnotneed);
				new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
					@Override
					public void run() {
						popup(message, 0);
					}
				}, 1000);
			}
		} else {

			int a = Util.hfp_vitals_taken_status(HFPMainActivity.this, type);

			//Log.e(TAG, "hfp_vitals_taken_status------ " + a);
			if (a != 0) {
				redirect(); // if no pressure are taken for this time period
			} else {
				message = this.getString(R.string.alreadytakenmeasurment);
				new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
					@Override
					public void run() {
						popup(message, 0);
					}
				}, 1000);
			}
		}
	}

	private void LoginMember() {

		SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
		SharedPreferences flow = getSharedPreferences(CommonUtilities.USER_TIMESLOT_SP, 0);
		SharedPreferences.Editor editor = flow.edit();

		int val = flowsp.getInt("flow", 0);
		if (val == 1) {
			editor.putString("timeslot", "AP");
		} else {
			editor.putString("timeslot", timepicker());
		}
		editor.commit();

		Cursor c = login_db.UserIsPresent(login_db.Patient_Table);
		int patientCount = c.getCount();
		c.close();
		login_db.cursorLogin.close();
		login_db.close();
		if (patientCount != 0) {
			if (val == 1) {
				Log.i(TAG, "click Home button redirecting member login in process ");
				redirect();
			} else {
				String Condition = " type='All Day'";
				if (login_db.checkUserLogin(login_db.Patient_Table, Condition) == 0) // if
					// 'All
					// Day'
					// user,
					// then
					// no
					// need
					// of
					// chkuser
					chkuser();
				else {
					Log.i(TAG, "All day member login in ");
					redirect();
				}
			}

		} else {
			message = this.getString(R.string.tabletnotassigned);
			popup(message, 1);
		}

	}

	private void redirect() {

		SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
		int flow_type = flowsp.getInt("reminder_path", 0);
		if (flow_type == 1) {
			flag_login = 0;
			clickedhpf = 0;
			Log.i(TAG, "  reminder flow set login");
		}

		/*************************************************/
		if (clickedhpf == 0) {

			Log.i(TAG, "Redirecting to weight entry");

			Intent intentwt = new Intent(getApplicationContext(), Login.class);
			intentwt.putExtra("startingwith", 0);
			finish();
			startActivity(intentwt);
			overridePendingTransition(0, 0);

		} else {
			Log.i(TAG, "user click the tab multiple times, starting th eapp again ");
		}
	}

	private String timepicker() {

		String patient_time = Util.get_patient_time_zone_time_24_format(this);
		Integer AmorPm = Integer.parseInt(patient_time.substring(11, 13));

		String type = "";
		// SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

		// Date date = new Date();
		// Integer AmorPm =
		// Integer.parseInt(dateFormat.format(date).substring(0,2));
		if (AmorPm >= 15)
			type = "PM";
		else if (AmorPm <= 14)
			type = "AM";
		return type;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked on hfp main page:");

			Intent intent = new Intent(this, FinalMainActivity.class);
			HFPMainActivity.this.finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	private void getAlbumDir() {

		Log.i(TAG_temp, "Get album Dire Function");

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

			File storageDir = new File(Environment.getExternalStorageDirectory().toString() + "/versions/HFP/Images");

			// storageDir =
			// mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

			if (storageDir != null) {
				if (!storageDir.mkdirs()) {
					if (!storageDir.exists()) {
						Log.d(TAG, "failed to create directory");
					}
				}
			}

		} else {
			Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
		}
	}

	private void setPic() {

		try {
			Log.i(TAG, "started downloading Support image ");

			SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);

			String contractcode = settings.getString("contract_code_name", "0");

			String mCurrentPhotoPath = Environment.getExternalStorageDirectory().toString() + "/versions/HFP/Images/";

			File directoryFile = new File(mCurrentPhotoPath);

			File myFile = new File(directoryFile.getAbsolutePath() + "/" + contractcode + ".png");
			//Log.i(TAG, "contractcode-" + contractcode);

			if (myFile.exists()) {

				// myFile.delete();
				LoginMember();

				Log.i(TAG, "contractcode already exists");

			} else {
				DownloadProfilePicServer();
				Log.i(TAG, "contractcode not  exists");
			}
			/*
			 * ActivityProfilepicture apf=new ActivityProfilepicture();
			 * apf.DownloadProfilePicServer
			 * (PatientIdDroid,serverurl,ContractCode); Thread.sleep(1000);
			 */

		} catch (Exception e) {
			Log.i(TAG, "Exception in set profile picture");
		}

	}

	public void loadPatientDetails() {
		DownloadPatientDetailsTask task = new DownloadPatientDetailsTask();
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	private boolean isMyServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.ScheduleService".equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	/***** Download Patient Login Details ********/

	private class DownloadPatientDetailsTask extends AsyncTask<String, Void, String> {
		String disclaimer_status = "0";

		@Override
		protected void onPreExecute() {

			Log.i(TAG, "onPreExecute patient ddetails download");

		}

		@Override
		protected void onPostExecute(String result) {
			Log.i(TAG, "onPostExecute");
			if (disclaimer_status.equals("1")) {

				loadDisclaimerDetails();
			} else {

				// downloadSchedule();
				loadSensorDetails();
			}

			if (log_status.trim().equals("1")) {
				upload_log_toserver();
			}

		}

		protected String doInBackground(String... urls) {
			String response = "";
			String language = "0";
			try {
				//Log.e(TAG, "patient details url" + PostUrl + "/droid_website/patient_authentication.ashx");
				String VersionNumber = getResources().getString(R.string.version);
				HttpResponse response1 = Util.connect(PostUrl + "/droid_website/patient_authentication.ashx",
						new String[] { "serial_no", "cluster_id", "version_number", "imei_no" },
						new String[] { serialNo, "1000001", VersionNumber, imeilNo });
				if (response1 == null) {
					Log.e(TAG, "Connection Failed!");
					return ""; // process
				}
				// int a = response1.getStatusLine().getStatusCode(); //
				// checking for 200 success code
				login_db.delete_patient();
				String str = Util.readStream(response1.getEntity().getContent());

				if (str.trim().length() == 0) {
					Log.i(TAG, "No patient details for the current serial number from server");
					return "";
				} else {
					login_db.delete_patient();
				}
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");
				//Log.i(TAG, "string from server-->" + str);
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);

				ClassUser classUser = new ClassUser();
				NodeList nodes = doc.getElementsByTagName("patient");
				TestDate td = new TestDate();
				for (int i = 0; i < nodes.getLength(); i++) {

					classUser.setPatientId(Integer.parseInt(Util.getTagValue(nodes, i, "patient_id")));
					classUser.setFullName("-");
					classUser.setNickName(Util.getTagValue(nodes, i, "nick_name"));
					classUser.setVsb_code(Util.getTagValue(nodes, i, "vsb_password"));
					classUser.setDbo("-");
					classUser.setSex("-");
					is_home_enable = Integer.parseInt(Util.getTagValue(nodes, i, "is_home_enable"));
					is_support_enable = Integer.parseInt(Util.getTagValue(nodes, i, "is_support_enable"));
					is_mydata_enable = Integer.parseInt(Util.getTagValue(nodes, i, "chart_status"));
					// classUser.setType("AM"); // testing
					classUser.setType(Util.getTagValue(nodes, i, "ampm"));
					classUser.setContractcode(Util.getTagValue(nodes, i, "contract_code_name"));
					ContractCode = Util.getTagValue(nodes, i, "contract_code_name");

					Constants.setPatientid(classUser.getPatientId());
					PatientIdDroid = classUser.getPatientId();

					String ServerTime = Util.getTagValue(nodes, i, "server_time");

					log_status = Util.getTagValue(nodes, i, "log_status");
					language = Util.getTagValue(nodes, i, "language_id");

					disclaimer_status = Util.getTagValue(nodes, i, "disclaimer_status");
					SharedPreferences settings = getSharedPreferences(CommonUtilities.DISCLAIMER_SP, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putString("disclaimer_status", disclaimer_status);
					editor.commit();

					setprefferenceForTimezone(ServerTime);

					int year = Integer.parseInt(ServerTime.substring(6, 10));
					int month = Integer.parseInt(ServerTime.substring(0, 2));
					int day = Integer.parseInt(ServerTime.substring(3, 5));
					int hour = Integer.parseInt(ServerTime.substring(11, 13));
					int minute = Integer.parseInt(ServerTime.substring(14, 16));
					int seconds = Integer.parseInt(ServerTime.substring(17, 19));

					TestDate.setCalender(year, month, day, hour, minute, seconds);
				}
				SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
				if(!settings.getString("patient_id", "0").equals(""+classUser.getPatientId())){
					
					SharedPreferences lastStatus = getSharedPreferences(CommonUtilities.LASTUPLOAD_STATUS, 0);
					SharedPreferences.Editor lastStatusEditor = lastStatus.edit();
					lastStatusEditor.putString("upload_time", "");					
					lastStatusEditor.putString("upload_status", "");
					lastStatusEditor.putString("upload_session_time", "");
					lastStatusEditor.putString("upload_session_status", "");	
					lastStatusEditor.commit();
				}
				CommonUtilities.Login_UserName = classUser.getNickName();
				login_db.insert_Patient_Data(classUser);
				Log.i(TAG, "Inserting new patient data to local DB");

				// save uname and id to sp
				
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("patient_name", classUser.getNickName());
				editor.putString("patient_id", classUser.getPatientId() + "");
				editor.putString("contract_code_name", ContractCode);
				editor.putString("language_id", language);
				editor.commit();

				SharedPreferences settings_cluster = getSharedPreferences(CommonUtilities.CLUSTER_SP, 0);
				SharedPreferences.Editor editor_cluster = settings_cluster.edit();
				editor_cluster.putInt("is_home_enable", is_home_enable);
				editor_cluster.putInt("is_support_enable", is_support_enable);
				editor_cluster.putInt("chart_status", is_mydata_enable);
				editor_cluster.commit();

				SharedPreferences lastStatus = getSharedPreferences(CommonUtilities.LASTUPLOAD_STATUS, 0);
				SharedPreferences.Editor lastStatusEditor = lastStatus.edit();
				lastStatusEditor.putString("nick_name", classUser.getNickName());
				lastStatusEditor.commit();
				
			} catch (Exception e) {
				e.printStackTrace();
				String Error = this.getClass().getName() + " : " + e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "HFP Authentication Details Download Error",
						Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
			}

			return response;
		}
	}

	private void loadDisclaimerDetails() {

		DownloadDisclaimerTask task = new DownloadDisclaimerTask();
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	private class DownloadDisclaimerTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {

			// downloadSchedule();
			loadSensorDetails();
		}

		@Override
		protected void onPreExecute() {

			Log.e(TAG, "-------onPreExecute loadDisclaimerDetails----------");
			try {
				/*
				 * pd = ProgressDialog.show(MainActivity.this, "",
				 * "Loading Details. Please wait...", true);
				 */
				setProgressBarIndeterminateVisibility(true);
			} catch (Exception e) {
				Log.i(TAG, "Exception in ProgressDialog.show");
			}
		}

		protected String doInBackground(String... urls) {
			String jsonResponse = "";

			String disclaimer_status = "";
			String disclaimer_note = "";
			try {
				Log.e("DROID", "patient details url" + PostUrl + "/droid_website/Disclaimer_message.ashx");

				HttpResponse response1 = Util.connect(PostUrl + "/droid_website/Disclaimer_message.ashx",
						new String[] { "imei_no" }, new String[] { imeilNo });

				if (response1 != null) {
					InputStream is = response1.getEntity().getContent();

					BufferedReader reader = new BufferedReader(new InputStreamReader(is));
					StringBuilder sb = new StringBuilder();

					String line = null;
					try {
						while ((line = reader.readLine()) != null) {
							sb.append(line + "\n");
						}
						if (sb.length() == 0) {

							return "";
						} else {

							jsonResponse = sb.toString();
						}

					} catch (IOException e) {
						e.printStackTrace();
						return "";
					} finally {
						try {
							is.close();
						} catch (IOException e) {
							e.printStackTrace();
							return "";
						}
					}
				} else {

					return "";
				}
				// text = sb.toString();
				JSONObject disclaimerObject = new JSONObject(jsonResponse);
				disclaimer_status = disclaimerObject.optString("disclaimer_status").toString();
				disclaimer_note = disclaimerObject.optString("disclaimer_message").toString();

				SharedPreferences settings = getSharedPreferences(CommonUtilities.DISCLAIMER_SP, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("disclaimer_status", disclaimer_status);
				editor.putString("disclaimer_note", disclaimer_note);
				editor.commit();

			} catch (Exception e) {
				// e.printStackTrace();
			}

			return jsonResponse;
		}
	}

	/***** Download Patient Login Details END ********/

	private class DownloadSensorDetailsTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			Log.i(TAG, "onPostExecute SensorDetails");

			try {
				new QuestionFetch().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		protected String doInBackground(String... urls) {

			String response = "";
			try {
				// sensor_db.deleteplayorder();//
				HttpResponse response1 = Util.connect(PostUrl + "/droid_website/sensor_details.ashx",
						new String[] { "serial_no", "imei_no" }, new String[] { serialNo, imeilNo });

				if (response1 == null) {

					Log.e(TAG, "Connection Failed!");

					return ""; // process
				}
				// Log.e("DROID","serialNo"+serialNo);

				String str = Util.readStream(response1.getEntity().getContent());
				if (str.trim().length() == 0) {
					Log.i(TAG, " nnull Sensor details");
					sensor_db.delete();
					sensor_db.deleteplayorder();
					return "";
				}

				str = str.replace("&", "");
				DocumentBuilder dbr = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = dbr.parse(is);

				NodeList nodes = doc.getElementsByTagName("sensor");

				sensor_db.delete(); // deleting entire table
				sensor_db.deleteplayorder();
				// db_sensor.delete(sensor_Table,null,null); // deleting entire
				// table
				String s_time = TestDate.getCurrentTime();
				db_measureType.delete();
				ClassSensor sensor = new ClassSensor();

				for (int i = 0; i < nodes.getLength(); i++) {

					sensor.setPatientId(Integer.parseInt(Util.getTagValue(nodes, i, "patient_id")));
					sensor.setSensorId(Integer.parseInt(Util.getTagValue(nodes, i, "sensor_id")));
					sensor.setSensorName(Util.getTagValue(nodes, i, "sensor_name"));
					sensor.setMeasureTypeId(Integer.parseInt(Util.getTagValue(nodes, i, "measure_type_id")));
					sensor.setMeasureTypeName(Util.getTagValue(nodes, i, "measure_type_name"));
					sensor.setMacId((Util.getTagValue(nodes, i, "id")));
					sensor.setPin((Util.getTagValue(nodes, i, "sn")));
					sensor.setItem_sn((Util.getTagValue(nodes, i, "item_serial_number")));

					/************
					 * Checking whether node is present or not
					 **********************/

					Element advice_idelement = (Element) nodes.item(i);
					NodeList advice_idname = advice_idelement.getElementsByTagName("weight_unit_id");

					if (advice_idname.getLength() > 0) {
						sensor.setWeightUnitId(Integer.parseInt(Util.getTagValue(nodes, i, "weight_unit_id")));
					} else {
						sensor.setWeightUnitId(0);
					}

					Element advice_idelement1 = (Element) nodes.item(i);
					NodeList advice_idname1 = advice_idelement1.getElementsByTagName("weight_unit_name");

					if (advice_idname1.getLength() > 0) {
						sensor.setWeightUnitName(Util.getTagValue(nodes, i, "weight_unit_name"));
					} else

					{
						sensor.setWeightUnitName("");
					}
					
					
					
					NodeList temp_id = advice_idelement
                            .getElementsByTagName("temperature_unit_id");

                    if (temp_id.getLength() > 0) {
                        int temperature_unit_id = (Integer.parseInt(Util
                                .getTagValue(nodes, i, "temperature_unit_id")));

                        SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putInt("temperature_unit_id", temperature_unit_id);
                        editor.commit();

                        Log.i(TAG, "temperature_unit_id:" + temperature_unit_id);
                    } else {
                        //sensor.setWeightUnitId(0);
                        Log.i(TAG, "temperature_unit_id: null");
                    }
					
					

					ContentValues values = new ContentValues();
					ContentValues valuesorder = new ContentValues();

					values.put("Patient_Id", sensor.getPatientId());

					values.put("Sensor_id", sensor.getSensorId());
					values.put("Sensor_name", sensor.getSensorName());
					values.put("Measure_type_id", sensor.getMeasureTypeId());
					values.put("Measure_type_name", sensor.getMeasureTypeName());
					values.put("Weight_unit_id", sensor.getWeightUnitId());
					values.put("Weight_unit_name", sensor.getWeightUnitName());
					values.put("Mac_id", sensor.getMacId());
					values.put("Status", 0);
					values.put("pin", sensor.getPin());
					values.put("item_sn", sensor.getItem_sn());

					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date date = new Date();
					values.put("Download_Date", dateFormat.format(date)); // inserting
					// downloaded
					// date

					// db_sensor.insert("Droid_Sensordetails", null, values);

					sensor_db.insertSensor(values);

					int order = orderofvital(sensor.getMeasureTypeId());
					int homeorder = homeOrderofvital(sensor.getMeasureTypeId());
					ClassMeasureType sensor1 = new ClassMeasureType();
					sensor1.setMeasuretypeId(sensor.getMeasureTypeId());
					sensor1.setPatientId(sensor.getPatientId());
					sensor1.setStatus(0);
					sensor1.setDate(s_time);

					db_measureType.insert(sensor1);

					valuesorder.put("Patient_Id", sensor.getPatientId());
					valuesorder.put("Measure_type_name", sensor.getMeasureTypeName());
					valuesorder.put("Measure_type_id", sensor.getMeasureTypeId());
					valuesorder.put("playorder", order);
					valuesorder.put("Status", 0);
					valuesorder.put("homeOrder", homeorder);
					sensor_db.insertSensor_play_order(valuesorder);
					response = "Success";
				}
				// close();
				sensor_db.close();
			} catch (Exception e) {
				e.printStackTrace();
				String Error = this.getClass().getName() + " : " + e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "HFP Sensor Details Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
			}

			return response;
		}
	}

	public void loadSensorDetails() {

		DownloadSensorDetailsTask task = new DownloadSensorDetailsTask();
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

	}

	public void DownloadProfilePicServer() {

		/*
		 * patientId=id; serverurl=PostUrl; contratCode=ContratCode;
		 */
		Log.i(TAG, "Download image Server started ");
		DownloadWebPageTask task = new DownloadWebPageTask();
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	private class DownloadWebPageTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {

			Log.e(TAG, "-----------DownloadProfilePicServer--------");
			LoginMember();

		}

		@SuppressWarnings("unused")
		protected HttpResponse connect(String url, String[] header, String[] value) {
			try {

				HttpClient client = new DefaultHttpClient();
				//Log.i(TAG, url);
				HttpPost post = new HttpPost(url);
				List<NameValuePair> pairs = new ArrayList<NameValuePair>();
				post.setEntity(new UrlEncodedFormEntity(pairs));
				for (int i = 0; i < header.length; i++) {
					post.setHeader(header[i], value[i]);
				}
				return client.execute(post);
			} catch (Exception e) {
				Log.e(TAG, e.toString());
			}
			return null;
		}

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			String response = "";

			try {

				URL url;
				File file;

				if (ContractCode.equals("")) {
					url = new URL(PostUrl + "/droid_website/user_images/" + PatientIdDroid + ".jpg");
					file = new File(PatientIdDroid + ".png");
				} else {
					url = new URL(PostUrl + "/droid_website/contract_images/" + ContractCode + ".jpg");
					file = new File(ContractCode + ".png");
				}

				if (PatientIdDroid != 0) {
					/* Open a connection to that URL. */
					URLConnection con = url.openConnection();

					InputStream is = con.getInputStream();
					BufferedInputStream bis = new BufferedInputStream(is, 1024 * 50);
					String SDCardRoot = Environment.getExternalStorageDirectory().toString() + "/versions/HFP/Images/";
					FileOutputStream fos = new FileOutputStream(SDCardRoot + file);
					byte[] buffer = new byte[1024 * 50];

					int current = 0;
					while ((current = bis.read(buffer)) != -1) {
						fos.write(buffer, 0, current);
					}

					fos.flush();
					fos.close();
					bis.close();
				}
				Log.d(TAG, "download contract code completed");

			} catch (Exception e) {
				e.printStackTrace();
				Log.d(TAG, "download contract code exception");
				String Error = this.getClass().getName() + " : " + e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Download Contract Code Image Error",
						Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
			}

			return response;
		}

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	public void upload_log_toserver() {
		Upload_LogTask taskLogUpdate = new Upload_LogTask();
		taskLogUpdate.execute(new String[] { PostUrl + "/droid_website/patient_log.ashx" });
	}

	private class Upload_LogTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {

			/*
			 * try { Thread.sleep(4000); Intent intent = new
			 * Intent(Questions.this, BlackActivity.class);
			 * Questions.this.finish(); startActivity(intent); } catch
			 * (InterruptedException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); }
			 */

		}

		@Override
		protected String doInBackground(String... params) {
			// String query =
			// "Select
			// a.Item_Number,a.Sequence_Number,a.Measure_Date,a.Patient_Id,a.treeNumber,a.Item_Content
			// from HFP_DMP_User_Response a where a.Status=0";

			String complete_log = Util.grtLogData(PatientIdDroid + "");

			// Log.i(TAG, "finalxml ..." + s);
			String finalxml = "";

			finalxml = "<logmain>" + complete_log + "</logmain>";

			// Log.i(TAG, "finalxml ..." + finalxml.toString());

			if (!complete_log.equals("")) {

				try {

					HttpClient client = new DefaultHttpClient();
					//Log.i(TAG, "Updating serverurl" + PostUrl + "/droid_website/patient_log.ashx");
					HttpPost post = new HttpPost(PostUrl + "/droid_website/patient_log.ashx");
					StringEntity se = new StringEntity(finalxml, HTTP.UTF_8);
					List<NameValuePair> pairs = new ArrayList<NameValuePair>();
					post.setHeader("patient_id", PatientIdDroid + "");
					// post.setHeader("mode", "2");

					//Log.i(TAG, "Header values in log upload" + PatientIdDroid);

					post.setEntity(se);
					HttpResponse response1 = client.execute(post);
					InputStream in = response1.getEntity().getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(in));
					StringBuilder str = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						str.append(line + "\n");
					}
					in.close();
					str.toString();
					if (str.length() > 0) {

						DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
						InputSource is1 = new InputSource();
						is1.setCharacterStream(new StringReader(str.toString()));
						Document doc = db.parse(is1);

					}

					/*
					 * db_log.cursorLog.close(); db_log.close();
					 */

				} catch (Exception e) {
					Log.e(TAG, e.toString());
					String Error = this.getClass().getName() + " : " + e.getMessage();
					OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Patient Log Upload Error", Error);
					OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
				}

			}

			return null;
		}

	}

	/***************************************
	 * Upload log Response to Server end
	 ********************************************/
	private void SetConfig() {

		{

			SharedPreferences settingswifi = getSharedPreferences(CommonUtilities.WiFi_SP, 0);
			serialNo = settingswifi.getString("wifi", "-1");
			imeilNo = settingswifi.getString("imei_no", "");

			SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
			server = settings.getString("Server_url", "-1");
			port = settings.getString("Server_port", "-1");
			PostUrl = settings.getString("Server_post_url", "-1");

			CommonUtilities.SERVER_URL = PostUrl;
			Constants.setParams(server, port, PostUrl, serialNo);
		}
	}

	private void popup(String mssg, int a) {

		LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

		View pview;
		pview = inflater.inflate(R.layout.popup_messages, null);

		TextView msg = (TextView) pview.findViewById(R.id.textpopup);
		TextView mac = (TextView) pview.findViewById(R.id.txtmac);

		Typeface type = Typeface.createFromAsset(getAssets(), "fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);
		msg.setText(mssg);
		if (a == 1) {
			mac.setTypeface(type, Typeface.NORMAL);
			if (imeilNo.trim().length() > 5) {

				if (imeilNo.trim().length() > 14) {

					String asubstring = imeilNo.substring(0, 14);

					mac.setText(asubstring);
				} else {
					mac.setText(imeilNo);
				}
			} else {
				mac.setText(serialNo.toUpperCase());
			}
		} else {
			mac.setText("");
		}

		closeversion = (Button) pview.findViewById(R.id.exitversion);

		final PopupWindow cp = new PopupWindow(pview, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		// as an extra precaution, added try/catch block
		try {
			cp.showAtLocation(pview_back, Gravity.CENTER, 0, 0);

		} catch (Exception e) {
			e.printStackTrace();
		}

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = (displaymetrics.heightPixels);
		int width = displaymetrics.widthPixels / 2;
		height = (int) (height / 1.2);

		cp.update(0, 0, width, height);

		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				cp.dismiss();

				Intent intent = new Intent(HFPMainActivity.this, FinalMainActivity.class);
				startActivity(intent);
				overridePendingTransition(0, 0);
			}
		});

	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.e(TAG, "onRestart starting page");

		/*
		 * Intent intent = new Intent(this, HFPMainActivity.class);
		 * intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); this.finish();
		 * 
		 * startActivity(intent); overridePendingTransition(0, 0);
		 */

	}

	private void check_logfile() {

		String Log_created_date = "";
		SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		if (Util.WriteLog(TAG, PatientIdDroid + "")) // true, if log file
														// newly created --
														// (false, if log file
														// is already exists and
														// re writing to the
														// current file)
		{
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();

			java.util.Date d1 = null;
			Calendar cal = Calendar.getInstance();
			try {
				d1 = dfDate.parse(dfDate.format(cal.getTime()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			editor.putString("Logfile_created_date", d1.toString()); // saving
																		// log
																		// file
																		// created
																		// date
			editor.commit();
		} else // if, its Overwriting existing log file
		{
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			Log_created_date = settings.getString("Logfile_created_date", "");

			if (Log_created_date.trim().length() > 0) {

				Calendar cal = Calendar.getInstance();
				Date current_date = null;
				Date Log_date = new Date();

				try {
					Log_date = dfDate.parse(Log_created_date); // log created
																// date
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				try {
					current_date = dfDate.parse(dfDate.format(cal.getTime())); // current
																				// date
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				int diffInDays = 0;

				diffInDays = (int) ((current_date.getTime() - Log_date.getTime()) / (1000 * 60 * 60 * 24)); // difference
																											// in
																											// days

				if (diffInDays > 2) {
					Util.ToFileClear(Constants.getdroidPatientid() + ".txt"); // clear
																				// 3
																				// day
																				// older
																				// log
																				// files
				}
			}
		}
	}

	// login page order
	private int orderofvital(int measure_type_id) {
		int a = 0;

		if (measure_type_id == 7) {
			a = 1;
		} else if (measure_type_id == 101) {
			a = 2;
		} else if (measure_type_id == 2) {
			a = 3;
		} else if (measure_type_id == 3) {
			a = 4;
		} else if (measure_type_id == 6) {
			a = 5;
		} else if (measure_type_id == 1) {
			a = 6;

		} else {
			a = 0;

		}

		return a;
	}

	// home page order
	private int homeOrderofvital(int measure_type_id) {
		int a = 0;

		if (measure_type_id == 7) {
			a = 1;
		} else if (measure_type_id == 101) {
			a = 3;
		} else if (measure_type_id == 2) {
			a = 5;
		} else if (measure_type_id == 3) {
			a = 4;
		} else if (measure_type_id == 6) {
			a = 2;
		} else if (measure_type_id == 1) {
			a = 6;

		}

		return a;
	}

	/***************************************
	 * DownloadQuestionFromServer start
	 ********************************************/

	private class QuestionFetch extends AsyncTask<String, Void, String> {

		protected String getTagValue(NodeList nodes, int i, String tag) {
			return getCharacterDataFromElement(
					(org.w3c.dom.Element) (((org.w3c.dom.Element) nodes.item(i)).getElementsByTagName(tag)).item(0));
		}

		@Override
		protected void onPostExecute(String result) {
			Log.e(TAG, "---------------onPostExecute QuestionFetch-----------");
			setPic();
			downloadSchedule();
		}

		protected HttpResponse connect(String url, String[] header, String[] value) {
			try {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(url);
				List<NameValuePair> pairs = new ArrayList<NameValuePair>();
				post.setEntity(new UrlEncodedFormEntity(pairs));
				for (int i = 0; i < header.length; i++) {
					post.setHeader(header[i], value[i]);
				}
				return client.execute(post);
			} catch (Exception e) {
				Log.e(TAG, e.toString());
			}
			return null;
		}

		public Boolean preference() {
			Boolean flag = null;
			try {

				String PREFS_NAME = "DroidPrefSc";
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				int scheduletatus = settings.getInt("Scheduletatus", -1);
				//Log.i("Droid", " SharedPreferences : " + getApplicationContext() + " is : " + scheduletatus);
				if (scheduletatus == 1)
					flag = true;
				else
					flag = false;
			}

			catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			return flag;
		}

		protected String readStream(InputStream in) {
			try {

				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				StringBuilder str = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					str.append(line + "\n");
				}

				in.close();

				return str.toString();
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			return "";
		}

		protected String doInBackground(String... urls) {
			String response = "";

			try {
				HttpResponse response1 = null;

				//Log.i(TAG, "hfp question url serverurl" + PostUrl + "/droid_website/patient_questions.ashx");
				response1 = connect(PostUrl + "/droid_website/patient_questions.ashx",
						new String[] { "serial_no", "mode", "imei_no" },
						new String[] { Constants.getSerialNo(), "0", imeilNo });

				int Statuscode = response1.getStatusLine().getStatusCode(); //
				// checking for 200 success code

				String str = readStream(response1.getEntity().getContent());
				// System.out.println(str);
				response = str;
				if (Statuscode == 200) {
					Log.i(TAG, "Question status 200 from the server");		
					dbcreate1.Cleartable();
				}

				if (str.trim().length() == 0) {
					Log.i(TAG, "No Question from the server");
					return "";
				}
				
				Log.i(TAG, "Question from the server downloaded");
				str = str.replaceAll("&", "and");
				// str.replaceAll(" & ", " and ");
				str = str.replaceAll("\r\n", "");
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");

				// str=str.replaceAll("\\s","");

				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);

				String patient_id = "";
				String question_id = "";
				String pattern_name = "";
				String top_node_number = "";
				String node_number = "";
				String sequence_number = "";
				String item_number = "";
				String child_node_number = "";
				String guidance = "";
				String sequence_number1 = "";
				String explanation = "";
				String kind = "";
				String sequence_number2 = "";
				String item_number1 = "";
				String item_content = "";

				NodeList nodes = doc.getElementsByTagName("tree");
				for (int i = 0; i < nodes.getLength(); i++) {
					patient_id = getTagValue(nodes, i, "patient_id");
					question_id = getTagValue(nodes, i, "question_id");
					pattern_name = getTagValue(nodes, i, "pattern_name");
					top_node_number = getTagValue(nodes, i, "top_node_number");
					dbcreate1.InsertDmp(Integer.parseInt(patient_id), Integer.parseInt(question_id), pattern_name,
							Integer.parseInt(top_node_number));
				}

				NodeList branch_tree = doc.getElementsByTagName("branch_tree");

				for (int i = 0; i < branch_tree.getLength(); i++) {
					// b = getTagValue(nodes, i, "question_id");
					// Node item=branch_tree.item(i) ;
					NodeList branch = doc.getElementsByTagName("branch");

					for (int j = 0; j < branch.getLength(); j++) {

						node_number = getTagValue(branch, j, "node_number");
						sequence_number = getTagValue(branch, j, "sequence_number");
						item_number = getTagValue(branch, j, "item_number");
						child_node_number = getTagValue(branch, j, "child_node_number");

						if (item_number.equals("")) {
							item_number = "0";
						}

						dbcreate1.InsertDmpBranching(Integer.parseInt(patient_id), node_number, child_node_number,
								Integer.parseInt(sequence_number), Integer.parseInt(item_number), "",
								Integer.parseInt(question_id));
						// dbcreate1.InsertDmpBranching(Patient_Code,
						// Node_Number, Child_Node_Number, Sequence_Number,
						// Item_Number, Last_Update_Date, Question_Id)
					}
					Constants.setPatientid(Integer.parseInt(patient_id));

				}

				NodeList interview = doc.getElementsByTagName("interview");
				for (int i = 0; i < interview.getLength(); i++) {
					// b = getTagValue(nodes, i, "question_id");
					// Node item=branch_tree.item(i) ;
					NodeList interview_content = doc.getElementsByTagName("interview_content");

					for (int j = 0; j < interview_content.getLength(); j++) {

						sequence_number1 = getTagValue(interview_content, j, "sequence_number");
						guidance = getTagValue(interview_content, j, "guidance");
						explanation = getTagValue(interview_content, j, "explanation");
						kind = getTagValue(interview_content, j, "kind");

						dbcreate1.InsertDmpQuestion(Integer.parseInt(sequence_number1), explanation, guidance,
								Integer.parseInt(kind), "");

					}

				}

				NodeList interview_response = doc.getElementsByTagName("interview_response");
				for (int i = 0; i < interview_response.getLength(); i++) {
					// b = getTagValue(nodes, i, "question_id");
					// Node item=branch_tree.item(i) ;
					NodeList answer = doc.getElementsByTagName("response");

					for (int j = 0; j < answer.getLength(); j++) {

						sequence_number2 = getTagValue(answer, j, "sequence_number");
						item_number1 = getTagValue(answer, j, "item_number");
						item_content = getTagValue(answer, j, "item_content");
						dbcreate1.InsertDmpAnswers(Integer.parseInt(sequence_number2), Integer.parseInt(item_number1),
								item_content);

					}

				}

			} catch (Exception e) {
				String Error = this.getClass().getName() + " : " + e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Question Data Upload/Download Error",
						Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Questions_failure_upload);
			}

			return response;
		}

	}

	/***************************************
	 * DownloadQuestionFromServer end
	 ********************************************/

	public String getCharacterDataFromElement(org.w3c.dom.Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

	public void setprefferenceForThanks(String n) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME_SCHEDULE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("Scheduleid", n);
		editor.commit();
	}

	public void setprefference(int n) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME_SCHEDULE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("Scheduletatus", n);
		editor.commit();
	}

	public void setprefferenceForTimezone(String n) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME_TIME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("timezone", n);
		editor.commit();
	}

	private void chkService() {

		// Lock service
		Log.e(TAG, "Checking service status");

		// Schedule service

		if (!isMyServiceRunning()) {

			Intent schedule = new Intent();
			schedule.setClass(HFPMainActivity.this, ScheduleService.class);
			startService(schedule);

		} else {
			Log.i(TAG, "schedule Serice is already running");
		}

		// advice service

	}

	/*******************************
	 * Download Schedule
	 *************************************************/

	public void downloadSchedule() {

		Log.i(TAG, "medication message:-DownloadWebPageTask");
		downloadSchedule task = new downloadSchedule();
		// task.execute(new String[] { PostUrl + "/medication_handler.ashx" });
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

	}

	private class downloadSchedule extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			Log.e(TAG, "-----------------onPostExecute downloadSchedule-------------");
			if (!isMyServiceRunning()) {// ##

				Intent schedule = new Intent();
				schedule.setClass(HFPMainActivity.this, ScheduleService.class);
				startService(schedule);

			} else {
				Log.i(TAG, "schedule Serice is already running");
			}

			// loginmyUser();

			// setProgressBarIndeterminateVisibility(false);

		}

		protected String doInBackground(String... urls) {
			String response = "";

			int n = Constants.getdroidPatientid();
			String id = Integer.toString(n);

			try {

				HttpResponse response1 = Util.connect(PostUrl + "/droid_website/schedule_handler.ashx",
						new String[] { "patient_id" }, new String[] { id });

				if (response1 == null) {
					Log.e(TAG, "Connection Failed!");
					return "";
				}

				String str = Util.readStream(response1.getEntity().getContent());

				str = str.replaceAll("&", "and");
				str = str.replaceAll("\r\n", "");
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");

				str.toString();

				if (str.length() < 10) {
					Log.i(TAG, "Schedule data null");
					return "";
				} else {
					Log.i(TAG, "Schedule data get from server");
					// dbSchedule.DeletPatint();
					dbSchedule.DeletAllSchedule();
				}

				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);
				NodeList nodes = doc.getElementsByTagName("schedule");
				// NodeList timeNode = doc.getElementsByTagName("times");
				ClassSchedule classSchedule = new ClassSchedule();
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
				String currentDateandTime = dateFormat.format(new Date());

				for (int i = 0; i < nodes.getLength(); i++) {

					classSchedule.setScheduleId((Util.getTagValue(nodes, i, "schedule_id")));
					classSchedule.setPatientId(Integer.parseInt(Util.getTagValue(nodes, i, "patient_id")));
					classSchedule.setLastUpdateDate((Util.getTagValue(nodes, i, "last_updateDate")));
					classSchedule.setDownloadDate(currentDateandTime);
					classSchedule.setStatus(0);

					Constants.setPatientid(classSchedule.getPatientId());

					classSchedule.setTimeout(Integer.parseInt(Util.getTagValue(nodes, i, "late_minutes")));

					Element advice_idelement = (Element) nodes.item(i);
					NodeList timeNode = advice_idelement.getElementsByTagName("times");
					NodeList actvivitiesNode = advice_idelement.getElementsByTagName("actvivities");

					for (int k = 0; k < timeNode.getLength(); k++) {
						classSchedule.setScheduleTime(Util.getTagValue(timeNode, k, "time"));
						for (int j = 0; j < actvivitiesNode.getLength(); j++) {

							classSchedule.setMeasureTypeId(
									Integer.parseInt(Util.getTagValue(actvivitiesNode, j, "measure_type_id")));

							dbSchedule.insertSchedule(classSchedule);
						}

					}
				}

			} catch (Exception e) {

				Log.e(TAG, "Error schedule downloading and saving  " + e.getMessage());
				// e.printStackTrace();

				SharedPreferences settings = getSharedPreferences(PREFS_NAME_SCHEDULE, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putInt("Scheduletatus", 0);
				editor.commit();

				String Error = this.getClass().getName() + " : " + e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Schedule Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
			}

			return response;
		}

	}
	
	
	
}
