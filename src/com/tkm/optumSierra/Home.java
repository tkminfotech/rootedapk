package com.tkm.optumSierra;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tkm.optumSierra.dal.Flow_Status_db;
import com.tkm.optumSierra.dal.Schedule_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.Alam_util;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.Regular_Alam_util;
import com.tkm.optumSierra.util.Util;

public class Home extends Titlewindow implements OnClickListener {

	private String TAG = "Optum_Home";
	Schedule_db dbSchedule = new Schedule_db(this);
	Sensor_db dbSensor = new Sensor_db(this);
	TextView txtwelcom;
	ObjectAnimator AnimPlz, ObjectAnimator;
	private Sensor_db sensor_db = new Sensor_db(this);
	private Flow_Status_db Flow_db = new Flow_Status_db(this);
	private GlobalClass globalClass;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_my_home);
		globalClass = (GlobalClass)getApplicationContext();
		//globalClass.isRemSessionStart = false;
		globalClass.isEllipsisEnable = true;
		globalClass.is_omron_start_flow=true;
		globalClass.isSupportEnable = true;
		txtwelcom = (TextView) findViewById(R.id.animtext);
		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);

		SharedPreferences.Editor editor = flow.edit();
		editor.putInt("flow", 1);
		editor.putInt("start_flow", 0);// for reloading 5 minutes
		editor.commit();
		anim();
		getUI();

		Flow_db.open();
		Flow_db.deleteAll();
		Flow_db.insert("1");
		Flow_db.close();

		// clear retak sp
		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);
		SharedPreferences.Editor editor_retake = settings.edit();
		editor_retake.putInt("retake_status", 0);
		editor_retake.commit();

		try {
			Log.i(TAG, "canceling all the alams");
			Regular_Alam_util.cancelAlarm(Home.this);
			Alam_util.cancelAlarm(Home.this);

		} catch (Exception e) {
			// TODO: handle exception
		}

		/*----------------------CALLING REEBOOTCHECKER ---- START-----------------------*/
		try {
			rebootTimer.cancel();
			timerReloader = 0;
			rebootTimer.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*----------------------CALLING REEBOOTCHECKER ---- END-----------------------*/
		
		SharedPreferences is_home = getSharedPreferences(
				CommonUtilities.CLUSTER_SP, 0);	

	int	is_home_enable = is_home.getInt("is_home_enable", 0);

		if (is_home_enable==1) { // Home is enabled for this user
			
				
			} else {
				Intent intent = new Intent(this, FinalMainActivity.class);
				startActivity(intent);
				overridePendingTransition(0, 0);
			}
			

		Log.i(TAG, "---------on create completed home----------");
	}

	private void getUI() {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = (displaymetrics.heightPixels);
		int w = (displaymetrics.widthPixels);
		int width = (displaymetrics.widthPixels / 4) - 120;
		Log.i(TAG, " screen width/4- 120 -" + width);
		// Log.i(TAG, "split_width-"+split_width);

		LinearLayout row1 = (LinearLayout) findViewById(R.id.button_layout1);
		LinearLayout row2 = (LinearLayout) findViewById(R.id.button_layout2);
		// row2.setVisibility(View.GONE);
		Cursor c = sensor_db.SelecthomeOrder();
		c.moveToFirst();
		int i = 0;
		Log.i(TAG, "---------creating dynamic button----------");
		if (c.getCount() > 0) {

			while (c.isAfterLast() == false) {

				int a = Integer.parseInt(c.getString(6));
				int measuretype = Integer.parseInt(c.getString(3));

				c.moveToNext();

				Button ivBowl = new Button(this);

				if (i < 3)

				{
					if (a == 1)
						ivBowl.setBackgroundResource(R.drawable.wtclick);
					else if (a == 2)
						ivBowl.setBackgroundResource(R.drawable.tempclick);
					else if (a == 3)
						ivBowl.setBackgroundResource(R.drawable.qsclick);
					else if (a == 4)
						ivBowl.setBackgroundResource(R.drawable.bpclick);
					else if (a == 5)
						ivBowl.setBackgroundResource(R.drawable.boclick);
					else if (a == 6)
						ivBowl.setBackgroundResource(R.drawable.bgclick);
					ivBowl.setId(measuretype);
					ivBowl.setOnClickListener(this);
					// ivBowl.setBackgroundResource(R.drawable.wtclick);
					LinearLayout.LayoutParams layoutParams;
					if (Build.MODEL.equals("MN-724"))
						 layoutParams = new LinearLayout.LayoutParams(230,260);
					else
						layoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

					// cp.update(0, 0, 350, 350);
					// cp.update(0, 0, 750, 500);
					// cp.update(0, 0, width,height);

					// layoutParams.setMargins(155, 45, 0, 0); // left, top,
					// right,
					// bottom
					if (i == 0) {
						if (width > 160) {
							layoutParams.setMargins((int) (width / 2.5), 45, 0,
									0);
						} else {
							layoutParams.setMargins(width, 45, 0, 0);

						}
					} else {
						layoutParams.setMargins(width, 45, 0, 0);
					}

					ivBowl.setLayoutParams(layoutParams);
					row1.addView(ivBowl);
				}

				else {

					if (a == 1)
						ivBowl.setBackgroundResource(R.drawable.wtclick);
					else if (a == 2)
						ivBowl.setBackgroundResource(R.drawable.tempclick);
					else if (a == 3)
						ivBowl.setBackgroundResource(R.drawable.qsclick);
					else if (a == 4)
						ivBowl.setBackgroundResource(R.drawable.bpclick);
					else if (a == 5)
						ivBowl.setBackgroundResource(R.drawable.boclick);
					else if (a == 6)
						ivBowl.setBackgroundResource(R.drawable.bgclick);
					ivBowl.setId(measuretype);
					ivBowl.setOnClickListener(this);
					LinearLayout.LayoutParams layoutParams;
					if (Build.MODEL.equals("MN-724"))
						 layoutParams = new LinearLayout.LayoutParams(230,260);
					else
						layoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
						// layoutParams.setMargins(width, 25, 0, 0); // left, top,
					// right,
					// bottom

					if (i == 3) {
						if (width > 160) {

							layoutParams.setMargins((int) (width / 2.5), 45, 0,
									0);

						} else {
							layoutParams.setMargins(width, 45, 0, 0);

						}
					} else {

						layoutParams.setMargins(width, 45, 0, 0);

					}

					ivBowl.setLayoutParams(layoutParams);
					row2.addView(ivBowl);
				}
				i++;
			}
		}
		c.close();
		sensor_db.cursorsensor.close();
		sensor_db.close();

	}

	public void onClick(View v) {

		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.PREFS_NAME_date, 0);
		SharedPreferences.Editor editor_retake = settings1.edit();
		editor_retake.putString("sectiondate",
				Util.get_patient_time_zone_time(this));
		editor_retake.commit();

		if (v.getId() == 7) {
			redirectWT(); // wt
		} else if (v.getId() == 101) {

			String question = dbSensor.SelectQuestion();
			if (question.trim().length() > 0) {
				Intent intent = new Intent(getApplicationContext(),
						StartQuestion.class);
				finish();
				Constants.setquestionPresent(0);
				startActivity(intent);
			} else {
				/*
				 * Toast.makeText(Home.this, "No Question for this time",
				 * Toast.LENGTH_LONG).show();
				 */
			}
			// qs
		} else if (v.getId() == 2) {
			redirectPulse();// puls
		} else if (v.getId() == 3) {

			redirectBP(); // bp

		} else if (v.getId() == 6) {
			redirectTemp();// temp
		} else if (v.getId() == 1) {
			// String tmpMac = dbSensor.SelectGlucoName();
			String sensor_name = dbSensor.SelectGlucose_sensor_Name();

			if (sensor_name.contains("One Touch Ultra")) {

				Intent intent = new Intent(getApplicationContext(),
						GlucoseReader.class);

				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);

			} else if (sensor_name.contains("Bayer")) {

				Intent intent = new Intent(getApplicationContext(),
						GlucoseReader.class);

				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);

			} else if (sensor_name.contains("Accu-Chek")) {

				if (Build.VERSION.SDK_INT >= 18) {
					Intent intent = new Intent(getApplicationContext(),
							GlucoseAccuChek.class);
					startActivity(intent);
					finish();
				} else {

					Toast.makeText(getBaseContext(), "not support ble",
							Toast.LENGTH_SHORT).show();
					Intent intent = new Intent(getApplicationContext(),
							GlucoseEntry.class);
					finish();
					startActivity(intent);

				}

			} else if (sensor_name.contains("Taidoc")) {

				if (Build.VERSION.SDK_INT >= 18) {
					Intent intent = new Intent(getApplicationContext(),
							GlucoseTaidoc.class);
					startActivity(intent);
					finish();
				} else {

					Toast.makeText(getBaseContext(), "not support ble",
							Toast.LENGTH_SHORT).show();

					Intent intent = new Intent(getApplicationContext(),
							GlucoseEntry.class);
					finish();
					startActivity(intent);

				}

			}

			else if (sensor_name.trim().length() > 0) {
				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				finish();
				startActivity(intent);
			}

			dbSensor.cursorsensor.close();
			dbSensor.close();

		}

		Log.i("home", "dynamic btn clicked  id. " + v.getId());

	}

	private void anim() {

//		WindowManager.LayoutParams layout = getWindow().getAttributes();
//		layout.screenBrightness = 1F;
//		getWindow().setAttributes(layout);

		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
		AnimPlz.setDuration(11000);
		AnimPlz.start();

		AnimPlz.addListener(new AnimatorListenerAdapter() {
			public void onAnimationEnd(Animator animation) {

//				WindowManager.LayoutParams layout = getWindow().getAttributes();
//				layout.screenBrightness = 0.05F;
//				getWindow().setAttributes(layout);

			}
		});
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		anim();

		// TODO Auto-generated method stub
		return super.onTouchEvent(event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, " KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);
			this.finish();
			startActivity(intent);
		} else if (keyCode == KeyEvent.KEYCODE_HOME) {
			Log.i(TAG, " home back   clicked:");
			finish();
		}

		return super.onKeyDown(keyCode, event);
	}

	private void redirectPulse() {

		Log.i(TAG, "start to Datareader activity from home : ");

		Cursor cursorsensor = dbSensor.SelectMeasuredweight();
		String Sensor_name = "", Mac = "";
		Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));

		}

		Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
		if (Mac.trim().length() == 17) {

			if (Sensor_name.contains("3230")) {
				if (Build.VERSION.SDK_INT >= 18) {
					Intent intent = new Intent(getApplicationContext(),
							BLECheckActivity.class);
					intent.putExtra("macaddress", Mac);
					startActivity(intent);
					finish();
				} else {

					Toast.makeText(getBaseContext(), "not support ble",
							Toast.LENGTH_SHORT).show();
					finish();
					return;
				}
			} else if (Sensor_name.contains("9560")) {
				Intent intent = new Intent(getApplicationContext(),
						DataReaderActivity.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
			}

			overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("Manual")) {
			Intent intent = new Intent(getApplicationContext(),
					PulseEntry.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else {

			/*
			 * Toast.makeText(this, "Please assign Blood Oxygen.",
			 * Toast.LENGTH_LONG).show();
			 */

		}

	}

	private void redirectTemp() {

		String tmpMac = dbSensor.SelectTempSensorName();
		Log.i(TAG, "start to tepm  activity from home : ");
		Log.i(TAG, "tmpMac" + tmpMac);

		if (tmpMac.trim().length() > 4) {
			Intent intentfr = new Intent(getApplicationContext(),
					ForaMainActivity.class);
			intentfr.putExtra("deviceType", 2);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
			intentfr.putExtra("macaddress", getForaMAC(tmpMac));
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);

		} else if (tmpMac.equals("-1")) {
			Intent intentfr = new Intent(getApplicationContext(),
					TemperatureEntry.class);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else {

			/*
			 * Toast.makeText(this, "Please assign Temperature .",
			 * Toast.LENGTH_LONG).show();
			 */

		}

	}

	private void redirectBP() {
		Log.i(TAG, "start to bp  activity from home : ");
		Cursor cursorsensor = dbSensor.SelectBPSensorName();
		String Sensor_name = "", Mac = "";
		Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));

		}
		Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);

		if (Sensor_name.contains("A and D Bluetooth smart")) {
			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						AandDSmart.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		} else if (Sensor_name.contains("Omron HEM 9200-T")) {
			if (Build.VERSION.SDK_INT >= 18) {
				Constants.setPIN(cursorsensor.getString(10));
				Intent intent = new Intent(getApplicationContext(),
						OmronBlsActivity.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		} else if (Sensor_name.contains("P724-BP")) {

			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						P724BpSmart.class);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {
				Log.i(TAG,
						"SensorName P724 : no ble fund redirecting to normal page ");
				Intent intent = new Intent(this, P724Bp.class);
				finish();
				startActivity(intent);
				return;
			}
		}

		else if (Sensor_name.contains("Wellex")) {

			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						PressureWellex.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		}

		else if (Sensor_name.contains("A and D")) {

			if (Sensor_name.contains("UA-767BT-Ci")) {
				Intent intentwt = new Intent(getApplicationContext(),
						AandContinua.class);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			} else {

				Intent intentwt = new Intent(getApplicationContext(),
						AandDReader.class);
				intentwt.putExtra("deviceType", 1);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			}

		} else if (Sensor_name.contains("FORA")) {
			if (Mac.trim().length() == 17) {
				Intent intentfr = new Intent(getApplicationContext(),
						ForaMainActivity.class);
				intentfr.putExtra("deviceType", 1); // pressure
				intentfr.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intentfr);
				overridePendingTransition(0, 0);
			} else if (Sensor_name.contains("Manual")) {
				Intent intentfr = new Intent(getApplicationContext(),
						PressureEntry.class);

				this.finish();
				startActivity(intentfr);
				overridePendingTransition(0, 0);

			} else {
				/*
				 * Toast.makeText(this, "Please assign Blood Pressure.",
				 * Toast.LENGTH_LONG).show();
				 */
			}
		} else if (Sensor_name.contains("Manual")) {
			Intent intentfr = new Intent(getApplicationContext(),PressureEntry.class);
			//Intent intentfr = new Intent(Home.this, OmronBlsActivity.class);

			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);

		} else {

			/*
			 * Toast.makeText(this, "Please assign Blood Pressure.",
			 * Toast.LENGTH_LONG).show();
			 */
		}

	}

	private void redirectWT() {

		Log.i(TAG, "start to wt  activity from home : ");
		Cursor cursorsensor = dbSensor.SelectWeightSensorName();
		String Sensor_name = "", Mac = "";
		Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {
			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));
		}
		Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
		if (Sensor_name.contains("A and D")) {
			Intent intentwt = new Intent(getApplicationContext(),
					AandDReader.class);
			intentwt.putExtra("deviceType", 0);
			this.finish();
			startActivity(intentwt);
			overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("FORA")) {
			Intent intentfr = new Intent(getApplicationContext(),
					ForaMainActivity.class);
			intentfr.putExtra("deviceType", 0);
			intentfr.putExtra("macaddress", Mac);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("P724")) {
			Intent intentfr = new Intent(getApplicationContext(),
					P724Activity.class);
			intentfr.putExtra("deviceType", 0);
			intentfr.putExtra("macaddress", Mac);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else if (Sensor_name.trim().length() > 0) {
			final Context context = this;
			Intent intent = new Intent(context, WeightEntry.class);
			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);
		} else {

			/*
			 * Toast.makeText(this, "Please assign Body Weight.",
			 * Toast.LENGTH_LONG).show();
			 */

		}

	}

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		Log.i(TAG, " : Connect to macAddress " + macAddress);
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}

	@Override
	public void onStop() {

		Log.i(TAG, "-------------------onStop--------------  home  activity");
		// Commit the edits!
		super.onStop();

		Util.Stopsoundplay();
		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");

		Log.i(TAG, "onStop on home  activity");

	}

}
