/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: E:\\studiows\\BleSampleOmron\\app\\src\\main\\aidl\\com\\example\\blesample\\IBleListener.aidl
 */
package com.tkm.optumSierra;
public interface IBleListener extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.tkm.optumSierra.IBleListener
{
private static final java.lang.String DESCRIPTOR = "com.tkm.optumSierra.IBleListener";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.tkm.optumSierra.IBleListener interface,
 * generating a proxy if needed.
 */
public static com.tkm.optumSierra.IBleListener asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.tkm.optumSierra.IBleListener))) {
return ((com.tkm.optumSierra.IBleListener)iin);
}
return new com.tkm.optumSierra.IBleListener.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_BleAdvCatch:
{
data.enforceInterface(DESCRIPTOR);
this.BleAdvCatch();
reply.writeNoException();
return true;
}
case TRANSACTION_BleConnected:
{
data.enforceInterface(DESCRIPTOR);
this.BleConnected();
reply.writeNoException();
return true;
}
case TRANSACTION_BleDisConnected:
{
data.enforceInterface(DESCRIPTOR);
this.BleDisConnected();
reply.writeNoException();
return true;
}
case TRANSACTION_BleWmDataRecv:
{
data.enforceInterface(DESCRIPTOR);
byte[] _arg0;
_arg0 = data.createByteArray();
this.BleWmDataRecv(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_BleBatteryDataRecv:
{
data.enforceInterface(DESCRIPTOR);
byte[] _arg0;
_arg0 = data.createByteArray();
this.BleBatteryDataRecv(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_BleCtsDataRecv:
{
data.enforceInterface(DESCRIPTOR);
byte[] _arg0;
_arg0 = data.createByteArray();
this.BleCtsDataRecv(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_BleBpmDataRecv:
{
data.enforceInterface(DESCRIPTOR);
byte[] _arg0;
_arg0 = data.createByteArray();
this.BleBpmDataRecv(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_BleBpfDataRecv:
{
data.enforceInterface(DESCRIPTOR);
byte[] _arg0;
_arg0 = data.createByteArray();
this.BleBpfDataRecv(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_BleAdvCatchDevice:
{
data.enforceInterface(DESCRIPTOR);
android.bluetooth.BluetoothDevice _arg0;
if ((0!=data.readInt())) {
_arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.BleAdvCatchDevice(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_BleWsfDataRecv:
{
data.enforceInterface(DESCRIPTOR);
byte[] _arg0;
_arg0 = data.createByteArray();
this.BleWsfDataRecv(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_BleConnectionStateRecv:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.BleConnectionStateRecv(_arg0);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.tkm.optumSierra.IBleListener
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void BleAdvCatch() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_BleAdvCatch, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void BleConnected() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_BleConnected, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void BleDisConnected() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_BleDisConnected, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void BleWmDataRecv(byte[] data) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeByteArray(data);
mRemote.transact(Stub.TRANSACTION_BleWmDataRecv, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void BleBatteryDataRecv(byte[] data) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeByteArray(data);
mRemote.transact(Stub.TRANSACTION_BleBatteryDataRecv, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void BleCtsDataRecv(byte[] data) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeByteArray(data);
mRemote.transact(Stub.TRANSACTION_BleCtsDataRecv, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void BleBpmDataRecv(byte[] data) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeByteArray(data);
mRemote.transact(Stub.TRANSACTION_BleBpmDataRecv, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void BleBpfDataRecv(byte[] data) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeByteArray(data);
mRemote.transact(Stub.TRANSACTION_BleBpfDataRecv, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void BleAdvCatchDevice(android.bluetooth.BluetoothDevice dev) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((dev!=null)) {
_data.writeInt(1);
dev.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_BleAdvCatchDevice, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void BleWsfDataRecv(byte[] data) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeByteArray(data);
mRemote.transact(Stub.TRANSACTION_BleWsfDataRecv, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void BleConnectionStateRecv(int state_code) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(state_code);
mRemote.transact(Stub.TRANSACTION_BleConnectionStateRecv, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_BleAdvCatch = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_BleConnected = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_BleDisConnected = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_BleWmDataRecv = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_BleBatteryDataRecv = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_BleCtsDataRecv = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_BleBpmDataRecv = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_BleBpfDataRecv = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
static final int TRANSACTION_BleAdvCatchDevice = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
static final int TRANSACTION_BleWsfDataRecv = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
static final int TRANSACTION_BleConnectionStateRecv = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
}
public void BleAdvCatch() throws android.os.RemoteException;
public void BleConnected() throws android.os.RemoteException;
public void BleDisConnected() throws android.os.RemoteException;
public void BleWmDataRecv(byte[] data) throws android.os.RemoteException;
public void BleBatteryDataRecv(byte[] data) throws android.os.RemoteException;
public void BleCtsDataRecv(byte[] data) throws android.os.RemoteException;
public void BleBpmDataRecv(byte[] data) throws android.os.RemoteException;
public void BleBpfDataRecv(byte[] data) throws android.os.RemoteException;
public void BleAdvCatchDevice(android.bluetooth.BluetoothDevice dev) throws android.os.RemoteException;
public void BleWsfDataRecv(byte[] data) throws android.os.RemoteException;
public void BleConnectionStateRecv(int state_code) throws android.os.RemoteException;
}
