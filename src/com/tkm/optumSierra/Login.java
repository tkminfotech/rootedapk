package com.tkm.optumSierra;

import java.io.StringReader;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import com.tkm.optumSierra.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.tkm.optumSierra.bean.ClassAdvice;
import com.tkm.optumSierra.dal.AdviceMessage_db;
import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.ConnectionDetector;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Regular_Alam_util;
import com.tkm.optumSierra.util.Util;

@SuppressLint("HandlerLeak")
public class Login extends Titlewindow {
	private TextToSpeech tts;
	private String TAG = "Login";
	private String serverurl = "";
	private String serialNo = "";
	private String imeilNo = "";
	private GlobalClass appClass;
	AdviceMessage_db dbcreate = new AdviceMessage_db(this);
	MeasureType_db db_measureType = new MeasureType_db(this);

	public void onInit(int status) {
		Log.e(TAG, "TTS Initilization ---onInit---");
		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.US);
			Log.i(TAG, "TTS Initilization Success");
			tts.setSpeechRate(0); // set speech speed rate
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e(TAG, "TTS Language is not supported");
			} else {
				// speakOut();
			}
		} else {
			Log.e(TAG, "TTS Initilization Failed");
		}

	}

	@SuppressLint("InlinedApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_hfpmain);

		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.SERVER_URL_SP, 0);
		serverurl = settings.getString("Server_post_url", "-1");

		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.WiFi_SP, 0);
		serialNo = settings1.getString("wifi", "-1");
		imeilNo = settings1.getString("imei_no", "");
		appClass = (GlobalClass) getApplicationContext();
		call_alams();
		Boolean isInternetPresent = (new ConnectionDetector(
				getApplicationContext())).isConnectingToInternet();
		if (isInternetPresent) {
			DownloadAdviceMessage();
		} else {

			SharedPreferences settings2 = getSharedPreferences(CommonUtilities.DISCLAIMER_SP, 0);
			if(settings2.getString("disclaimer_status", "0").equals("1")){

				Intent intent = new Intent(Login.this, DisclaimerActivity.class);
				intent.putExtra("nextFlow", "0");//normal Flow
				startActivity(intent);
				overridePendingTransition(0, 0);
			}else{

				SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
				int val = flowsp.getInt("flow", 0); 
				if(val == 1){

					Intent intentSc = new Intent(getApplicationContext(),
							Home.class);
					finish();
					startActivity(intentSc);
					overridePendingTransition(0, 0);
				}else{
					Log.e("********************"+TAG, "*********************Welcome_Activity**************");
					
					Intent intent = new Intent(this, Welcome_Activity.class);
					startActivity(intent);
					overridePendingTransition(0, 0);
				}
			}
		}
	}

	public void DownloadAdviceMessage() {
		DownloadWebPageTask task = new DownloadWebPageTask();
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

	}

	private class DownloadWebPageTask extends AsyncTask<String, Void, String> {
		boolean fala_adv = false;

		protected String doInBackground(String... urls) {
			String response = "";

			try {

				Log.i(TAG, "Checking device registration MAC-" + serialNo
						+ " IMEI-" + imeilNo);

				HttpResponse response1 = Util.connect(serverurl
						+ "/droid_website/rm_feedback.ashx", new String[] {
								"serial_no", "mode", "imei_no" }, new String[] {
										serialNo, "0", imeilNo });

				if (response1 == null) {
					Log.e(TAG, "Connection Failed!");
					return ""; // process
				}

				String str = Util
						.readStream(response1.getEntity().getContent());

				str = str.replaceAll("&", "and");
				str = str.replaceAll("\r\n", "");
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");

				str.toString();
				Log.i(TAG, "Advice message:-" + str);

				if (str.length() < 10) {
					Log.i(TAG, "Advice message data null");
					return "";
				}

				dbcreate.Cleartable();
				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);
				NodeList nodes = doc.getElementsByTagName("advice");

				ClassAdvice classAdvice = new ClassAdvice();

				for (int i = 0; i < nodes.getLength(); i++) {

					classAdvice.setPatientId(Integer.parseInt(Util.getTagValue(
							nodes, i, "patient_id")));
					classAdvice.setAdviceId(Integer.parseInt(Util.getTagValue(
							nodes, i, "advice_id")));
					classAdvice.setUserId(Integer.parseInt(Util.getTagValue(
							nodes, i, "user_id")));
					classAdvice.setAdviceSubject(Util.getTagValue(nodes, i,
							"advice_subject"));
					classAdvice.setAdviceText(Util.getTagValue(nodes, i,
							"advice_text"));
					classAdvice.setMessageDate(Util.getTagValue(nodes, i,
							"create_date"));
					//					SharedPreferences sharedPref = getSharedPreferences("advice_message",0);
					//					SharedPreferences.Editor editor = sharedPref.edit();
					//					editor.putString("message", classAdvice.getAdviceText());
					//					editor.commit();
					// saving advice to db
					Constants.setPatientid(classAdvice.getPatientId());

					if (dbcreate.InsertAdviceMessage(classAdvice)) // if there i
						// a new
						// advice
						// msg
					{
						fala_adv = true;
						// notification();

						
						
//						dbcreate.UpdateAdviceMessageByDate(classAdvice
//								.getMessageDate());

						//						try {
						//							Log.i(TAG, "Redirection start");
						//							
						//							SharedPreferences settings = getSharedPreferences(CommonUtilities.DISCLAIMER_SP, 0);
						//							if(settings.getString("disclaimer_status", "0").equals("1")){
						//								
						//								Intent intent = new Intent(Login.this, DisclaimerActivity.class);
						//								intent.putExtra("nextFlow", "1");//Feedback Flow
						//								startActivity(intent);
						//								overridePendingTransition(0, 0);
						//							}else{
						//								
						//								Intent intent = new Intent(Login.this,
						//										PopActivity.class);
						//								intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						//								
						//								startActivity(intent);
						//							}
						//							
						//
						//						} catch (NullPointerException e) {
						//							// TODO Auto-generated catch block
						//							// e.printStackTrace();
						//							Log.i(TAG, "IllegalArgumentException");
						//							fala_adv = false;
						//						}
					}

				}

			} catch (Exception e) {
				String Error = this.getClass().getName()+ " : "+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Feedback Message Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
				fala_adv = false;
			}

			return response;
		}

		@Override
		protected void onPostExecute(String result) {

			loadApkDetails();


			SharedPreferences settings = getSharedPreferences(CommonUtilities.DISCLAIMER_SP, 0);
			if(settings.getString("disclaimer_status", "0").equals("1")){

				Intent intent = new Intent(Login.this, DisclaimerActivity.class);
				finish();
				if (fala_adv) {
					
					intent.putExtra("nextFlow", "1");// Feedback Flow
				}else{
					
					intent.putExtra("nextFlow", "0");// normal flow
				}
				
				startActivity(intent);
				overridePendingTransition(0, 0);
			}else{

				if (fala_adv) {

					Intent intent = new Intent(Login.this,
							PopActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

					startActivity(intent);
				}else{

					SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
					int val = flowsp.getInt("flow", 0); 
					if(val == 1){

						Intent intentSc = new Intent(getApplicationContext(),
								Home.class);
						finish();
						startActivity(intentSc);
						overridePendingTransition(0, 0);
					}else{
						Log.e("********************"+TAG, "*********************Welcome_Activity**************");
						
						Intent intent = new Intent(getApplicationContext(), Welcome_Activity.class);
						startActivity(intent);
						overridePendingTransition(0, 0);
					}
				}
			}

		}
	}

	private void call_alams() {
		SharedPreferences flowsp = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, Context.MODE_PRIVATE);
		int val = flowsp.getInt("start_flow", 5); // #1

		if (val == 1) {
			Log.i(TAG, "starting Regular alam---");

			Regular_Alam_util.cancelAlarm(getApplicationContext());
			appClass.setRegular_Alarm_Triggered(false);
			appClass.setStart_Alarm_Count(0);
			Regular_Alam_util.startAlarm(getApplicationContext());// start
			// Regular
			// alarm
			// to
			// set
			// the
			// five
			// minutes
			// time
			// out
			appClass.setRegular_Alarm_Triggered(true);
			SharedPreferences.Editor editor = flowsp.edit();
			editor.putInt("start_flow", 0);// for reloading 5 minutes
			editor.commit();

			SharedPreferences settings1 = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);
			SharedPreferences.Editor editor_retake = settings1.edit();
			editor_retake
			.putString("measure_time_skip", Util.get_server_time());
			editor_retake.putString("sectiondate",
					Util.get_patient_time_zone_time(Login.this));

			editor_retake.commit();

		} else {
			Log.i(TAG, "Cancel Regular alam---");

			Regular_Alam_util.cancelAlarm(getApplicationContext());

		}
		Log.i(TAG,
				"Util.get_server_time()---------------"
						+ Util.get_server_time());
		db_measureType.updateStatusforAll(1);
	}

	/***** Download Apk Details ********/

	public void loadApkDetails() {
		DownloadApkDetailsTask task = new DownloadApkDetailsTask();
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	@SuppressLint("DefaultLocale")
	private class DownloadApkDetailsTask extends
	AsyncTask<String, Void, String> {

		protected String doInBackground(String... urls) {
			String response = "";
			try {

				Log.i(TAG, "Checking device registration MAC-" + serialNo
						+ " IMEI-" + imeilNo);

				HttpResponse response1 = Util.connect(serverurl
						+ "/droid_website/cluster.ashx", new String[] {
								"gw_id", "imei_no" },
						new String[] { serialNo, imeilNo });

				if (response1 == null) {
					Log.e(TAG, "Connection Failed!");
					return ""; // process
				}

				// login_db.delete_patient();
				String str = Util
						.readStream(response1.getEntity().getContent());

				if (str.trim().length() == 0) {
					Log.i(TAG,
							"No aPK details for the current serial number from server");
					return "";
				}
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");

				//				Log.i(TAG, "tablet details from server" + str);
				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);

				NodeList nodes = doc.getElementsByTagName("optum");
				for (int i = 0; i < nodes.getLength(); i++) {

					int time_zone = Integer.parseInt(Util.getTagValue(nodes, i,
							"time_zone_id"));
					String passcode = Util
							.getTagValue(nodes, i, "passcode");
					Log.i(TAG, "tablet time_zone_id-" + time_zone);

					SharedPreferences settings = getSharedPreferences(
							CommonUtilities.CLUSTER_SP, 0);
					SharedPreferences.Editor editor = settings.edit();

					editor.putInt("time_zone_id", time_zone);
					editor.putString("Passcode", passcode);					
					editor.commit();

				}

			} catch (Exception e) {
				e.printStackTrace();
				String Error = this.getClass().getName()+ " : "+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Apk Details Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
			}

			return response;
		}
	}
}
