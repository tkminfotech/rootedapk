package com.tkm.optumSierra;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import com.tkm.optumSierra.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.tkm.optumSierra.bean.ClassMeasureType;
import com.tkm.optumSierra.bean.ClassSchedule;
import com.tkm.optumSierra.bean.ClassSensor;
import com.tkm.optumSierra.bean.ClassUser;
import com.tkm.optumSierra.dal.Login_db;
import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.dal.Questions_db;
import com.tkm.optumSierra.dal.Schedule_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.service.ScheduleService;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.ConnectionDetector;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

public class MainActivity extends Titlewindow {

	private Login_db login_db = new Login_db(this);

	private Sensor_db sensor_db = new Sensor_db(this); //
	MeasureType_db db_measureType = new MeasureType_db(this);
	private Schedule_db dbSchedule = new Schedule_db(this);

	private String TAG = "MainActiviy";
	private View pview_back;
	String log_status = "0";
	private BluetoothAdapter mBluetoothAdapter = null;
	public static String serialNo = "";
	public static String imeilNo = "";
	private String NickName = "";
	public static String PostUrl = "";
	private static String port = "";
	private static String server = "";
	public static int PatientIdDroid;
	EditText userName;

	public static final String PREFS_NAME_SCHEDULE = "DroidPrefSc";
	public static final String PREFS_NAME_TIME = "TimePrefSc";
	public static final String PREFS_NAME = "MyLogFileCreatedDateSierra";

	Questions_db dbcreate1 = new Questions_db(this);

	ProgressDialog pd = null;
	public int flag_login = 0;
	Dialog dialog;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "MainActivity started");
		// flag_login=0;

		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		// requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// setContentView(R.layout.activity_main);
		setContentView(R.layout.activity_final_main);
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		pview_back = inflater.inflate(R.layout.activity_hfpmain, null);

		/*
		 * getWindow().getDecorView().setSystemUiVisibility(
		 * View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		 */
		Log.e(TAG, "Started Main Activity");
		setprefference(0);
		setprefferenceForThanks("AA");
		setprefferenceWelcome();

		ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
		Boolean isInternetPresent = (new ConnectionDetector(
				getApplicationContext())).isConnectingToInternet();
		Log.e(TAG, "Checking Config details in db");

		checkConfig();

		//Log.i("Droid", "Device Serial Number : " + serialNo);

		// load patient details
		if (isInternetPresent) {
			Log.e(TAG, "Internet present downloading patient details started");

			// Toast.makeText(this, "  loadPatientDetails post started.",
			// Toast.LENGTH_LONG).show();
			Log.i(TAG, "Loading patient details...");

			loadPatientDetails(); // if there is a connection , fetch
			// Patient
			// Details to local Sql_Lite table

		} else {
			Log.e(TAG, "Internet not present loading patient details from db");
			/*
			 * Toast.makeText(this,
			 * this.getString(R.string.turnonwifiorchecknetconnection),
			 * Toast.LENGTH_LONG).show();
			 */
			Log.i(TAG, "No net connection...");

			ClassUser classUser = new ClassUser();

			Login_db login_db1 = new Login_db(this);

			classUser = login_db1.SelectNickName();

			NickName = classUser.getNickName();
			PatientIdDroid = classUser.getPatientId();
			CommonUtilities.Login_UserName = NickName;
			Cursor Patientdetails = null;
			// login_db.SelectNickName();
			if (NickName == null) {
				// if (NickName.trim().length() == 0)
				{

					Toast.makeText(this,
							this.getString(R.string.connecttointernet),
							Toast.LENGTH_LONG).show();

					return;
				}
			} else {
				// userName.setText(NickName);
			}
			loginmyUser();
		}

		// loginmyUser();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
	}

	@Override
	protected void onPostResume() {
		super.onPostResume();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();

		Util.WriteLog(TAG, PatientIdDroid + "");

		flag_login = 1;
		clickedhpf = 1;
		// this.finish();

		Log.i(TAG, "onStop on main activity");

		try {
			check_logfile();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void check_logfile() {

		String Log_created_date = "";
		SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		if (Util.WriteLog(TAG, PatientIdDroid + "")) // true, if log file newly
		// created -- (false, if
		// log file is already
		// exists and re writing
		// to the current file)
		{
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();

			java.util.Date d1 = null;
			Calendar cal = Calendar.getInstance();
			try {
				d1 = dfDate.parse(dfDate.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			editor.putString("Logfile_created_date", d1.toString()); // saving
			// log
			// file
			// created
			// date
			editor.commit();
		} else // if, its Overwriting existing log file
		{
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			Log_created_date = settings.getString("Logfile_created_date", "");

			if (Log_created_date.trim().length() > 0) {

				Calendar cal = Calendar.getInstance();
				Date current_date = null;
				Date Log_date = new Date();

				try {
					Log_date = dfDate.parse(Log_created_date); // log created
					// date
				} catch (ParseException e1) {
					e1.printStackTrace();
				}

				try {
					current_date = dfDate.parse(dfDate.format(cal.getTime())); // current
					// date
				} catch (ParseException e) {
					e.printStackTrace();
				}

				int diffInDays = 0;

				diffInDays = (int) ((current_date.getTime() - Log_date
						.getTime()) / (1000 * 60 * 60 * 24)); // difference in
				// days

				if (diffInDays > 2) {
					Util.ToFileClear(Constants.getdroidPatientid() + ".txt"); // clear
					// 3
					// day
					// older
					// log
					// files
				}
			}
		}
	}

	public static void CancelNotification(Context ctx, int notifyId) {
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager nMgr = (NotificationManager) ctx
				.getSystemService(ns);
		nMgr.cancel(notifyId);
	}

	public void setprefferenceWelcome() {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME_SCHEDULE,
				0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("welcome", 0);
		editor.commit();
	}

	public void setprefferenceForThanks(String n) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME_SCHEDULE,
				0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("Scheduleid", n);
		editor.commit();
	}

	public void setprefference(int n) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME_SCHEDULE,
				0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("Scheduletatus", n);
		editor.commit();
	}

	public void setprefferenceForTimezone(String n) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME_TIME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("timezone", n);
		editor.commit();
	}

	public boolean checkDevice() {
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		mBluetoothAdapter.enable();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// e.printStackTrace();
		}

		return true;
	}

	private boolean isMyServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.ScheduleService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked on login page:");

			Intent intent = new Intent(this, FinalMainActivity.class);
			MainActivity.this.finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	public void setNickName() {

		// load from database #
		// Cursor Patientdetails = login_db.SelectNickName();
		ClassUser classUser = new ClassUser();

		Login_db login_db1 = new Login_db(this);

		classUser = login_db1.SelectNickName();

		NickName = classUser.getNickName();
		PatientIdDroid = classUser.getPatientId();

		Constants.setPatientid(PatientIdDroid);
		// Patientdetails.close();
		// userName.setText(NickName);
		Constants.setpatienrName(NickName);

	}

	public void loadPatientDetails() {
		DownloadPatientDetailsTask task = new DownloadPatientDetailsTask();
		task.execute(new String[] { PostUrl
				+ "/droid_website/patient_authentication.ashx" });
	}

	public void loginmyUser() {
		Log.i(TAG, "  loginmyUser called--------------");
		//Log.i(TAG, "  flag_login--" + flag_login);
		SharedPreferences flowsp = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = flowsp.getInt("flow", 0); // #1
		int flow_type = flowsp.getInt("reminder_path", 0); // #1
		System.out.println("SP VAL============flow_type==========>> "
				+ flow_type);
		if (flow_type == 1) {
			flag_login = 0;
			clickedhpf = 0;
			Log.i(TAG, "  reminder flow set login");
		}

		if (flag_login == 0) {
			setNickName();
			Log.i(TAG, "Reach loginmyUser ");
			final AlertDialog ad = new AlertDialog.Builder(this).create();
			int patientCount;
			Cursor c = login_db.UserIsPresent(login_db.Patient_Table);
			patientCount = c.getCount();
			c.close();
			login_db.cursorLogin.close();
			login_db.close();
			if (patientCount != 0) {

				chkService();

				CancelNotification(this, 0);
				if (clickedhpf == 0) {

					if (val == 1) {
						SharedPreferences settings = getSharedPreferences(
								CommonUtilities.DISCLAIMER_SP, 0);
						if (settings.getString("disclaimer_status", "0")
								.equals("1")) {

							Intent intentSc = new Intent(
									getApplicationContext(),
									DisclaimerActivity.class);
							intentSc.putExtra("nextFlow", "0");// normal flow
							finish();
							startActivity(intentSc);
							overridePendingTransition(0, 0);

						} else {

							Intent intentSc = new Intent(
									getApplicationContext(), Home.class);
							finish();
							startActivity(intentSc);
							overridePendingTransition(0, 0);
						}

					} else {

						Intent intentSc = new Intent(getApplicationContext(),
								Login.class);
						intentSc.putExtra("startingwith", 0);
						finish();
						startActivity(intentSc);
						overridePendingTransition(0, 0);
					}
				} else {
					Log.i(TAG,
							"user click the tab multiple times, starting th eapp again ");
				}

			} else {

				popup();
			}
		}

	}

	/***** Download Patient Login Details ********/

	private class DownloadPatientDetailsTask extends
			AsyncTask<String, Void, String> {
		String disclaimer_status = "0";

		@Override
		protected void onPostExecute(String result) {
			Log.i(TAG, "onPostExecute patient");
			if (disclaimer_status.equals("1")) {

				loadDisclaimerDetails();
			} else {

				// downloadSchedule();
				loadSensorDetails();
			}

			// upload_log_toserver();
			if (log_status.trim().equals("1")) {
				upload_log_toserver();
			}

		}

		@Override
		protected void onPreExecute() {

			Log.i(TAG, "onPreExecute patient");
			try {
				/*
				 * pd = ProgressDialog.show(MainActivity.this, "",
				 * "Loading Details. Please wait...", true);
				 */
				setProgressBarIndeterminateVisibility(true);
			} catch (Exception e) {
				Log.i(TAG, "Exception in ProgressDialog.show");
			}
		}

		protected String doInBackground(String... urls) {
			String response = "";
			String language = "0";
			try {
				//Log.e("DROID", "patient details url" + PostUrl
				//		+ "/droid_website/patient_authentication.ashx");
				String VersionNumber = getResources().getString(
						R.string.version);

				HttpResponse response1 = Util.connect(PostUrl
						+ "/droid_website/patient_authentication.ashx",
						new String[] { "serial_no", "cluster_id",
								"version_number", "imei_no" }, new String[] {
								serialNo, "1000000", VersionNumber, imeilNo });

				if (response1 == null) {
					Log.e(TAG, "Connection Failed!");
					return ""; // process
				}

				// int a = response1.getStatusLine().getStatusCode(); //
				// checking for 200 success code
				login_db.delete_patient();
				String str = Util
						.readStream(response1.getEntity().getContent());

				if (str.trim().length() == 0) {
					Log.i(TAG,
							"No patient details for the current serial number from server");
					return "";
				} else {
					login_db.delete_patient();
				}
				//Log.i("patient_authentication_Response", str.toString());
				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);

				ClassUser classUser = new ClassUser();
				NodeList nodes = doc.getElementsByTagName("patient");
				TestDate td = new TestDate();
				for (int i = 0; i < nodes.getLength(); i++) {

					classUser.setPatientId(Integer.parseInt(Util.getTagValue(
							nodes, i, "patient_id")));
					classUser.setFullName("-");
					classUser.setNickName(Util.getTagValue(nodes, i,
							"nick_name"));
					classUser
							.setVsb_code(Util.getTagValue(nodes, i, "vsb_password"));
					classUser.setDbo("-");
					classUser.setSex("-");

					String ServerTime = Util.getTagValue(nodes, i,
							"server_time");

					log_status = Util.getTagValue(nodes, i, "log_status");
					language = Util.getTagValue(nodes, i, "language_id");

					disclaimer_status = Util.getTagValue(nodes, i,
							"disclaimer_status");
					SharedPreferences settings = getSharedPreferences(
							CommonUtilities.DISCLAIMER_SP, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putString("disclaimer_status", disclaimer_status);
					editor.commit();

					Constants.setpatienrName(classUser.getFullName());
					Constants.setPatientid(classUser.getPatientId());

					PatientIdDroid = classUser.getPatientId();

					setprefferenceForTimezone(ServerTime);

					int year = Integer.parseInt(ServerTime.substring(6, 10));
					int month = Integer.parseInt(ServerTime.substring(0, 2));
					int day = Integer.parseInt(ServerTime.substring(3, 5));
					int hour = Integer.parseInt(ServerTime.substring(11, 13));
					int minute = Integer.parseInt(ServerTime.substring(14, 16));
					int seconds = Integer
							.parseInt(ServerTime.substring(17, 19));

					TestDate.setCalender(year, month, day, hour, minute,
							seconds);

					//Log.i("DROID", year + " " + month + " " + day + " " + hour
					//		+ " " + minute + " " + seconds);
				}
				CommonUtilities.Login_UserName = classUser.getNickName();

				login_db.insert_Patient_Data(classUser);
				Log.i(TAG, "Inserting new patient data to local DB");

				// save uname and id to sp
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.USER_SP, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("patient_name", classUser.getNickName());
				editor.putString("patient_id", classUser.getPatientId() + "");
				editor.putString("language_id", language);
				editor.commit();			
			
				

			} catch (Exception e) {
				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Authentication Details Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.Other_Upload_Download_Error);
			}

			return response;
		}
	}

	private void loadDisclaimerDetails() {

		DownloadDisclaimerTask task = new DownloadDisclaimerTask();
		task.execute(new String[] { PostUrl
				+ "/droid_website/Disclaimer_message.ashx" });
	}

	private class DownloadDisclaimerTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {

			// downloadSchedule();
			loadSensorDetails();
		}

		@Override
		protected void onPreExecute() {

			Log.i(TAG, "onPreExecute patient");
			try {
				/*
				 * pd = ProgressDialog.show(MainActivity.this, "",
				 * "Loading Details. Please wait...", true);
				 */
				setProgressBarIndeterminateVisibility(true);
			} catch (Exception e) {
				Log.i(TAG, "Exception in ProgressDialog.show");
			}
		}

		protected String doInBackground(String... urls) {
			String jsonResponse = "";

			String disclaimer_status = "";
			String disclaimer_note = "";
			try {
				//Log.e("DROID", "patient details url" + PostUrl
				//		+ "/droid_website/Disclaimer_message.ashx");
				String VersionNumber = getResources().getString(
						R.string.version);

				HttpResponse response1 = Util.connect(PostUrl
						+ "/droid_website/Disclaimer_message.ashx",
						new String[] { "imei_no" }, new String[] { imeilNo });

				if (response1 != null) {
					jsonResponse = Util.readStream(response1.getEntity()
							.getContent());
					//Log.i("Disclaimer_message_RESPONSE", jsonResponse);

				} else {

					return "";
				}
				// text = sb.toString();
				JSONObject disclaimerObject = new JSONObject(jsonResponse);
				disclaimer_status = disclaimerObject.optString(
						"disclaimer_status").toString();
				disclaimer_note = disclaimerObject.optString(
						"disclaimer_message").toString();

				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.DISCLAIMER_SP, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("disclaimer_status", disclaimer_status);
				editor.putString("disclaimer_note", disclaimer_note);
				editor.commit();

			} catch (Exception e) {
				e.printStackTrace();
			}

			return jsonResponse;
		}
	}

	/***** Download Patient Login Details END ********/

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			// macAddress.substring(i, i+2)
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		//Log.i(TAG, "DROID : Connect to macAddress " + macAddress);

		// String macAddress = "00:1C:05:00:40:64"; //"";
		// find MAC address of oxymeter from the DB
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}

	private void chkService() {

		// Lock service
		Log.e(TAG, "Checking service status");

		// Schedule service

		if (!isMyServiceRunning()) {

			Intent schedule = new Intent();
			schedule.setClass(MainActivity.this, ScheduleService.class);
			startService(schedule);

		} else {
			Log.i(TAG, "schedule Serice is already running");
		}

		// advice service

	}

	/******************************* Download Schedule *************************************************/

	public void downloadSchedule() {

		Log.i(TAG, "medication message:-DownloadWebPageTask");
		downloadSchedule task = new downloadSchedule();
		// task.execute(new String[] { PostUrl + "/medication_handler.ashx" });
		task.execute(new String[] { "" });

	}

	private class downloadSchedule extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			Log.i(TAG, "onPostExecute downloadSchedule");

			try {
				if (pd != null)
					pd.dismiss();

			} catch (Exception e) {
				Log.i(TAG, "Exception in pd.dismiss()");
			}
			if (!isMyServiceRunning()) {// ##

				Intent schedule = new Intent();
				schedule.setClass(MainActivity.this, ScheduleService.class);
				startService(schedule);

			} else {
				Log.i(TAG, "schedule Serice is already running");
			}

			loginmyUser();

			// setProgressBarIndeterminateVisibility(false);

		}

		protected String doInBackground(String... urls) {
			String response = "";

			int n = Constants.getdroidPatientid();
			String id = Integer.toString(n);

			try {

				HttpResponse response1 = Util.connect(PostUrl
						+ "/droid_website/schedule_handler.ashx",
						new String[] { "patient_id" }, new String[] { id });

				if (response1 == null) {
					Log.e(TAG, "Connection Failed!");
					return "";
				}

				String str = Util
						.readStream(response1.getEntity().getContent());

				str = str.replaceAll("&", "and");
				str = str.replaceAll("\r\n", "");
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");

				str.toString();

				if (str.length() < 10) {
					Log.i(TAG, "Schedule data null");
					return "";
				} else {
					Log.i(TAG, "Schedule data get from server");
					// dbSchedule.DeletPatint();
					dbSchedule.DeletAllSchedule();
				}

				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);
				NodeList nodes = doc.getElementsByTagName("schedule");
				// NodeList timeNode = doc.getElementsByTagName("times");
				ClassSchedule classSchedule = new ClassSchedule();
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"MM/dd/yyyy hh:mm:ss aa");
				String currentDateandTime = dateFormat.format(new Date());

				for (int i = 0; i < nodes.getLength(); i++) {

					classSchedule.setScheduleId((Util.getTagValue(nodes, i,
							"schedule_id")));
					classSchedule.setPatientId(Integer.parseInt(Util
							.getTagValue(nodes, i, "patient_id")));
					classSchedule.setLastUpdateDate((Util.getTagValue(nodes, i,
							"last_updateDate")));
					classSchedule.setDownloadDate(currentDateandTime);
					classSchedule.setStatus(0);

					Constants.setPatientid(classSchedule.getPatientId());

					classSchedule.setTimeout(Integer.parseInt(Util.getTagValue(
							nodes, i, "late_minutes")));

					Element advice_idelement = (Element) nodes.item(i);
					NodeList timeNode = advice_idelement
							.getElementsByTagName("times");
					NodeList actvivitiesNode = advice_idelement
							.getElementsByTagName("actvivities");

					for (int k = 0; k < timeNode.getLength(); k++) {
						classSchedule.setScheduleTime(Util.getTagValue(
								timeNode, k, "time"));
						for (int j = 0; j < actvivitiesNode.getLength(); j++) {

							classSchedule.setMeasureTypeId(Integer
									.parseInt(Util.getTagValue(actvivitiesNode,
											j, "measure_type_id")));

							dbSchedule.insertSchedule(classSchedule);
						}

					}
				}

			} catch (Exception e) {

				Log.e(TAG,
						"Error schedule downloading and saving  "
								+ e.getMessage());
				// e.printStackTrace();

				SharedPreferences settings = getSharedPreferences(
						PREFS_NAME_SCHEDULE, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putInt("Scheduletatus", 0);
				editor.commit();

				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Schedule Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.Other_Upload_Download_Error);
			}

			return response;
		}

	}

	private class DownloadSensorDetailsTask extends
			AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			Log.i(TAG, "onPostExecute SensorDetails");			
			new QuestionFetch().execute();
		}

		protected String doInBackground(String... urls) {

			String response = "";
			try {
				// sensor_db.deleteplayorder();//
				HttpResponse response1 = Util.connect(PostUrl
						+ "/droid_website/sensor_details.ashx", new String[] {
						"serial_no", "imei_no" }, new String[] { serialNo,
						imeilNo });

				if (response1 == null) {

					Log.e(TAG, "Connection Failed!");

					return ""; // process
				}
				// Log.e("Optum_Sierra","serialNo"+serialNo);

				String str = Util
						.readStream(response1.getEntity().getContent());
				if (str.trim().length() == 0) {
					Log.i(TAG, " nnull Sensor details");
					sensor_db.delete();
					sensor_db.deleteplayorder();
					return "";
				}
				str = str.replace("&", "");
				DocumentBuilder dbr = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = dbr.parse(is);

				NodeList nodes = doc.getElementsByTagName("sensor");

				sensor_db.delete(); // deleting entire table
				sensor_db.deleteplayorder();//

				db_measureType.delete();
				ClassSensor sensor = new ClassSensor();

				String s_time = TestDate.getCurrentTime();
				SharedPreferences settings1 = getSharedPreferences(
						CommonUtilities.PREFS_NAME_date, 0);
				SharedPreferences.Editor editor_retake = settings1.edit();
				editor_retake.putString("sectiondate",
						Util.get_patient_time_zone_time(MainActivity.this));
				editor_retake.commit();

				for (int i = 0; i < nodes.getLength(); i++) {

					sensor.setPatientId(Integer.parseInt(Util.getTagValue(
							nodes, i, "patient_id")));
					sensor.setSensorId(Integer.parseInt(Util.getTagValue(nodes,
							i, "sensor_id")));
					sensor.setSensorName(Util.getTagValue(nodes, i,
							"sensor_name"));
					sensor.setMeasureTypeId(Integer.parseInt(Util.getTagValue(
							nodes, i, "measure_type_id")));
					sensor.setMeasureTypeName(Util.getTagValue(nodes, i,
							"measure_type_name"));
					sensor.setMacId((Util.getTagValue(nodes, i, "id")));
					sensor.setPin((Util.getTagValue(nodes, i, "sn")));
					sensor.setItem_sn((Util.getTagValue(nodes, i, "item_serial_number")));
					Element advice_idelement = (Element) nodes.item(i);
					NodeList advice_idname = advice_idelement
							.getElementsByTagName("weight_unit_id");

					if (advice_idname.getLength() > 0) {
						sensor.setWeightUnitId(Integer.parseInt(Util
								.getTagValue(nodes, i, "weight_unit_id")));
					} else {
						sensor.setWeightUnitId(0);
					}

					Element advice_idelement1 = (Element) nodes.item(i);
					NodeList advice_idname1 = advice_idelement1
							.getElementsByTagName("weight_unit_name");

					if (advice_idname1.getLength() > 0) {
						sensor.setWeightUnitName(Util.getTagValue(nodes, i,
								"weight_unit_name"));
					} else

					{
						sensor.setWeightUnitName("");
					}

					ContentValues values = new ContentValues();
					ContentValues valuesorder = new ContentValues();

					values.put("Patient_Id", sensor.getPatientId());

					values.put("Sensor_id", sensor.getSensorId());
					values.put("Sensor_name", sensor.getSensorName());
					values.put("Measure_type_id", sensor.getMeasureTypeId());
					values.put("Measure_type_name", sensor.getMeasureTypeName());
					values.put("Weight_unit_id", sensor.getWeightUnitId());
					values.put("Weight_unit_name", sensor.getWeightUnitName());
					values.put("Mac_id", sensor.getMacId());
					values.put("Status", 0);
					values.put("pin", sensor.getPin());
					values.put("item_sn", sensor.getItem_sn());
					
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					Date date = new Date();
					values.put("Download_Date", dateFormat.format(date));

					sensor_db.insertSensor(values);// inserting downloaded date

					int order = orderofvital(sensor.getMeasureTypeId());
					int homeorder = homeOrderofvital(sensor.getMeasureTypeId());
					ClassMeasureType sensor1 = new ClassMeasureType();
					sensor1.setMeasuretypeId(sensor.getMeasureTypeId());
					sensor1.setPatientId(sensor.getPatientId());
					sensor1.setStatus(0);
					sensor1.setDate(s_time);

					db_measureType.insert(sensor1);

					valuesorder.put("Patient_Id", sensor.getPatientId());
					valuesorder.put("Measure_type_name",
							sensor.getMeasureTypeName());
					valuesorder.put("Measure_type_id",
							sensor.getMeasureTypeId());
					valuesorder.put("playorder", order);
					valuesorder.put("Status", 0);
					valuesorder.put("homeOrder", homeorder);

					sensor_db.insertSensor_play_order(valuesorder);

					response = "Success";
					/*
					 * Log.i(TAG, "Sensor details...SensorId " +
					 * sensor.getSensorId() + " Meassure_type " +
					 * sensor.getMeasureTypeName() + " Sensor_name " +
					 * sensor.getSensorName() + " Patient_Id " +
					 * sensor.getPatientId() + " Status " + sensor.getStatus());
					 */

				}
				// close();
				sensor_db.close();
			} catch (Exception e) {
				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Sensor Details Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.Other_Upload_Download_Error);
			}

			return response;
		}
	}

	public void loadSensorDetails() {

		DownloadSensorDetailsTask task = new DownloadSensorDetailsTask();
		task.execute(new String[] { PostUrl
				+ "/droid_website/sensor_details.ashx" });

	}

	// login page order
	private int orderofvital(int measure_type_id) {
		int a = 0;

		if (measure_type_id == 7) {
			a = 1;
		} else if (measure_type_id == 101) {
			a = 2;
		} else if (measure_type_id == 2) {
			a = 3;
		} else if (measure_type_id == 3) {
			a = 4;
		} else if (measure_type_id == 6) {
			a = 5;
		} else if (measure_type_id == 1) {
			a = 6;

		} else {
			a = 0;

		}

		return a;
	}

	// home page order
	private int homeOrderofvital(int measure_type_id) {
		int a = 0;

		if (measure_type_id == 7) {
			a = 1;
		} else if (measure_type_id == 101) {
			a = 3;
		} else if (measure_type_id == 2) {
			a = 5;
		} else if (measure_type_id == 3) {
			a = 4;
		} else if (measure_type_id == 6) {
			a = 2;
		} else if (measure_type_id == 1) {
			a = 6;

		}

		return a;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	public void upload_log_toserver() {
		Upload_LogTask taskLogUpdate = new Upload_LogTask();
		taskLogUpdate.execute(new String[] { PostUrl
				+ "/droid_website/patient_log.ashx" });
	}

	private class Upload_LogTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {

			/*
			 * try { Thread.sleep(4000); Intent intent = new
			 * Intent(Questions.this, BlackActivity.class);
			 * Questions.this.finish(); startActivity(intent); } catch
			 * (InterruptedException e) { e.printStackTrace(); }
			 */

		}

		@Override
		protected String doInBackground(String... params) {
			// String query =
			// "Select a.Item_Number,a.Sequence_Number,a.Measure_Date,a.Patient_Id,a.treeNumber,a.Item_Content from HFP_DMP_User_Response a where a.Status=0";

			String complete_log = Util.grtLogData(PatientIdDroid + "");

			// Log.i(TAG, "finalxml ..." + s);
			String finalxml = "";

			finalxml = "<logmain>" + complete_log + "</logmain>";

			// Log.i(TAG, "finalxml ..." + finalxml.toString());

			if (!complete_log.equals("")) {

				try {

					HttpClient client = new DefaultHttpClient();
					//Log.i(TAG, "Updating PostUrl" + PostUrl
					//		+ "/droid_website/patient_log.ashx");
					HttpPost post = new HttpPost(PostUrl
							+ "/droid_website/patient_log.ashx");
					StringEntity se = new StringEntity(finalxml, HTTP.UTF_8);
					List<NameValuePair> pairs = new ArrayList<NameValuePair>();
					post.setHeader("patient_id", PatientIdDroid + "");
					// post.setHeader("mode", "2");

					//Log.i("Droid", "Header values in log upload"
					//		+ PatientIdDroid);

					post.setEntity(se);
					HttpResponse response1 = client.execute(post);
					int a = response1.getStatusLine().getStatusCode();
					InputStream in = response1.getEntity().getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(in));
					StringBuilder str = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						str.append(line + "\n");
					}
					in.close();
					str.toString();
					if (str.length() > 0) {

						DocumentBuilder db = DocumentBuilderFactory
								.newInstance().newDocumentBuilder();
						InputSource is1 = new InputSource();
						is1.setCharacterStream(new StringReader(str.toString()));
						Document doc = db.parse(is1);
						NodeList nodes = doc
								.getElementsByTagName("success_response");

					}

					/*
					 * db_log.cursorLog.close(); db_log.close();
					 */

				} catch (Exception e) {
					Log.e(TAG, e.toString());
					String Error = this.getClass().getName() + " : "
							+ e.getMessage();
					OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
							"Patient Log Update Error", Error);
					OnInit_Data_Access.insert_incident_detail(Error,
							Constants.Other_Upload_Download_Error);
				}

			}

			return null;
		}

	}

	/*************************************** Upload log Response to Server end ********************************************/

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.e(TAG, "onRestart");

		/*
		 * Intent intent = new Intent(getApplicationContext(),
		 * MainActivity.class); this.finish(); startActivity(intent);
		 */

	}

	private void checkConfig() {

		{

			SharedPreferences settingswifi = getSharedPreferences(
					CommonUtilities.WiFi_SP, 0);
			serialNo = settingswifi.getString("wifi", "-1");

			imeilNo = settingswifi.getString("imei_no", "");
			SharedPreferences settings = getSharedPreferences(
					CommonUtilities.SERVER_URL_SP, 0);
			server = settings.getString("Server_url", "-1");
			port = settings.getString("Server_port", "-1");
			PostUrl = settings.getString("Server_post_url", "-1");
			String server4 = settings.getString("Server_url", "-1");

			CommonUtilities.SERVER_URL = PostUrl;
			Constants.setParams(server, port, PostUrl, serialNo);
		}
	}

	private void popup1() {

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		dialog = adb.setView(new View(this)).create();
		// (That new View is just there to have something inside the dialog that
		// can grow big enough to cover the whole screen.)

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = 790;
		lp.height = 500;
		dialog = new Dialog(this, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.popup_messages);

		TextView msg = (TextView) dialog.findViewById(R.id.textpopup);
		TextView mac = (TextView) dialog.findViewById(R.id.txtmac);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);
		msg.setText(this.getString(R.string.tabletnotassigned));

		mac.setTypeface(type, Typeface.NORMAL);
		if (imeilNo.trim().length() > 5) {
			if (imeilNo.trim().length() > 14) {

				String asubstring = imeilNo.substring(0, 14);

				mac.setText(asubstring);
			} else {
				mac.setText(imeilNo);
			}
		} else {
			mac.setText(serialNo.toUpperCase());
		}

		dialog.show();
		dialog.getWindow().setAttributes(lp);
		closeversion = (Button) dialog.findViewById(R.id.exitversion);
		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				dialog.dismiss();
			}
		});

	}

	private void popup() {

		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);

		View pview;
		pview = inflater.inflate(R.layout.popup_messages, null);

		TextView msg = (TextView) pview.findViewById(R.id.textpopup);
		TextView mac = (TextView) pview.findViewById(R.id.txtmac);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);
		msg.setText(this.getString(R.string.tabletnotassigned));

		mac.setTypeface(type, Typeface.NORMAL);
		if (imeilNo.trim().length() > 5) {
			if (imeilNo.trim().length() > 14) {

				String asubstring = imeilNo.substring(0, 14);

				mac.setText(asubstring);
			} else {
				mac.setText(imeilNo);
			}
		} else {
			mac.setText(serialNo.toUpperCase());
		}

		closeversion = (Button) pview.findViewById(R.id.exitversion);

		final PopupWindow cp = new PopupWindow(pview,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		cp.setBackgroundDrawable(new BitmapDrawable(getApplicationContext()
				.getResources()));
		cp.showAtLocation(pview_back, Gravity.CENTER, 0, 0);

		// cp.update(0, 0, 350, 350);
		// cp.update(0, 0, 750, 500);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = (displaymetrics.heightPixels);
		int width = displaymetrics.widthPixels / 2;
		height = (int) (height / 1.2);

		// cp.update(0, 0, 350, 350);
		// cp.update(0, 0, 750, 500);
		cp.update(0, 0, width, height);

		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				cp.dismiss();

				Intent i = new Intent(MainActivity.this,
						FinalMainActivity.class);
				startActivity(i);
				overridePendingTransition(0, 0);
			}
		});

	}

	/*************************************** DownloadQuestionFromServer start ********************************************/

	private class QuestionFetch extends AsyncTask<String, Void, String> {

		protected String getTagValue(NodeList nodes, int i, String tag) {
			return getCharacterDataFromElement((org.w3c.dom.Element) (((org.w3c.dom.Element) nodes
					.item(i)).getElementsByTagName(tag)).item(0));
		}

		@Override
		protected void onPostExecute(String result) {
			Log.i(TAG, "onPostExecute");
			downloadSchedule();
		}

		protected HttpResponse connect(String url, String[] header,
				String[] value) {
			try {
				InputStream content = null;
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(url);
				List<NameValuePair> pairs = new ArrayList<NameValuePair>();
				post.setEntity(new UrlEncodedFormEntity(pairs));
				for (int i = 0; i < header.length; i++) {
					post.setHeader(header[i], value[i]);
				}
				return client.execute(post);
			} catch (Exception e) {
				Log.e(TAG, e.toString());
			}
			return null;
		}

		public Boolean preference() {
			Boolean flag = null;
			try {

				String PREFS_NAME = "DroidPrefSc";
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				int scheduletatus = settings.getInt("Scheduletatus", -1);
				//Log.i("Droid", " SharedPreferences : "
				//		+ getApplicationContext() + " is : " + scheduletatus);
				if (scheduletatus == 1)
					flag = true;
				else
					flag = false;
			}

			catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			return flag;
		}

		protected String readStream(InputStream in) {
			try {

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(in));
				StringBuilder str = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					str.append(line + "\n");
				}

				in.close();

				return str.toString();
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			return "";
		}

		protected String doInBackground(String... urls) {
			String response = "";
			SharedPreferences settings = getSharedPreferences("DroidPrefSc", 0);

			String scheduletatus = settings.getString("Scheduleid", "AA");
			int patientId = Constants.getdroidPatientid();
			// Log.i(TAG, " Scheduleid"+scheduletatus+"");
			try {
				HttpResponse response1 = null;

				if (preference()) {

					//Log.i(TAG, "download question with Scheduleid"
					//		+ scheduletatus + "");

					Log.i(TAG, "Updating PostUrl" + PostUrl
							+ "/droid_website/patient_schedule_question.ashx");
					response1 = connect(PostUrl
							+ "/droid_website/patient_schedule_question.ashx",
							new String[] { "patient_id", "schedule_id" },
							new String[] { patientId + "", scheduletatus });
				} else {
					Log.i(TAG, "Updating PostUrl" + PostUrl
							+ "/droid_website/patient_questions.ashx");
					response1 = connect(PostUrl
							+ "/droid_website/patient_questions.ashx",
							new String[] { "serial_no", "mode", "imei_no" },
							new String[] { Constants.getSerialNo(), "0",
									imeilNo });
				}

				int Statuscode = response1.getStatusLine().getStatusCode(); //
				// checking for 200 success code

				String str = readStream(response1.getEntity().getContent());
				// System.out.println(str);
				response = str;
				if (Statuscode == 200) {
					Log.i(TAG, "Question status 200 from the server");

					dbcreate1.Cleartable();
				}

				if (str.trim().length() == 0) {
					Log.i(TAG, "No Question from the server");
					return "";
				}
				Log.i(TAG, "Question from the server downloaded");
				str = str.replaceAll("&", "and");
				// str.replaceAll(" & ", " and ");
				str = str.replaceAll("\r\n", "");
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");

				// str=str.replaceAll("\\s","");

				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);

				String patient_id = "";
				String question_id = "";
				String pattern_name = "";
				String top_node_number = "";
				String node_number = "";
				String sequence_number = "";
				String item_number = "";
				String child_node_number = "";
				String guidance = "";
				String sequence_number1 = "";
				String explanation = "";
				String kind = "";
				String sequence_number2 = "";
				String item_number1 = "";
				String item_content = "";

				NodeList nodes = doc.getElementsByTagName("tree");
				for (int i = 0; i < nodes.getLength(); i++) {
					patient_id = getTagValue(nodes, i, "patient_id");
					question_id = getTagValue(nodes, i, "question_id");
					pattern_name = getTagValue(nodes, i, "pattern_name");
					top_node_number = getTagValue(nodes, i, "top_node_number");
					dbcreate1.InsertDmp(Integer.parseInt(patient_id),
							Integer.parseInt(question_id), pattern_name,
							Integer.parseInt(top_node_number));
				}

				NodeList branch_tree = doc.getElementsByTagName("branch_tree");

				for (int i = 0; i < branch_tree.getLength(); i++) {
					// b = getTagValue(nodes, i, "question_id");
					// Node item=branch_tree.item(i) ;
					NodeList branch = doc.getElementsByTagName("branch");

					for (int j = 0; j < branch.getLength(); j++) {

						node_number = getTagValue(branch, j, "node_number");
						sequence_number = getTagValue(branch, j,
								"sequence_number");
						item_number = getTagValue(branch, j, "item_number");
						child_node_number = getTagValue(branch, j,
								"child_node_number");

						if (item_number.equals("")) {
							item_number = "0";
						}

						dbcreate1.InsertDmpBranching(
								Integer.parseInt(patient_id), node_number,
								child_node_number,
								Integer.parseInt(sequence_number),
								Integer.parseInt(item_number), "",
								Integer.parseInt(question_id));
						// dbcreate1.InsertDmpBranching(Patient_Code,
						// Node_Number, Child_Node_Number, Sequence_Number,
						// Item_Number, Last_Update_Date, Question_Id)
					}
					Constants.setPatientid(Integer.parseInt(patient_id));

				}

				NodeList interview = doc.getElementsByTagName("interview");
				for (int i = 0; i < interview.getLength(); i++) {
					// b = getTagValue(nodes, i, "question_id");
					// Node item=branch_tree.item(i) ;
					NodeList interview_content = doc
							.getElementsByTagName("interview_content");

					for (int j = 0; j < interview_content.getLength(); j++) {

						sequence_number1 = getTagValue(interview_content, j,
								"sequence_number");
						guidance = getTagValue(interview_content, j, "guidance");
						explanation = getTagValue(interview_content, j,
								"explanation");
						kind = getTagValue(interview_content, j, "kind");

						dbcreate1.InsertDmpQuestion(
								Integer.parseInt(sequence_number1),
								explanation, guidance, Integer.parseInt(kind),
								"");

					}

				}

				NodeList interview_response = doc
						.getElementsByTagName("interview_response");
				for (int i = 0; i < interview_response.getLength(); i++) {
					// b = getTagValue(nodes, i, "question_id");
					// Node item=branch_tree.item(i) ;
					NodeList answer = doc.getElementsByTagName("response");

					for (int j = 0; j < answer.getLength(); j++) {

						sequence_number2 = getTagValue(answer, j,
								"sequence_number");
						item_number1 = getTagValue(answer, j, "item_number");
						item_content = getTagValue(answer, j, "item_content");
						dbcreate1.InsertDmpAnswers(
								Integer.parseInt(sequence_number2),
								Integer.parseInt(item_number1), item_content);

					}

				}

			} catch (Exception e) {
				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Question Data Upload/Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.Questions_failure_upload);
			}

			return response;
		}

	}

	/*************************************** DownloadQuestionFromServer end ********************************************/

	public String getCharacterDataFromElement(org.w3c.dom.Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

}