package com.tkm.optumSierra;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;

import com.tkm.optumSierra.util.Log;

import android.util.Xml;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.tkm.optumSierra.bean.DailyReadingValues;
import com.tkm.optumSierra.bean.DateValues;
import com.tkm.optumSierra.bean.MyData;
//import com.tkm.optumSierra.dal.MyDataTracker_db;
import com.tkm.optumSierra.dal.MydataSensor_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.CommonUtilities;

import com.tkm.optumSierra.util.Util;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class MyDataActivity extends Titlewindow implements View.OnClickListener {
    private GlobalClass globalClass;
    private String pageName = "";
    private MydataSensor_db myDataSensor_db = new MydataSensor_db(this);
    String imeilNo;
//    private MyDataTracker_db myDataTracker_db = new MyDataTracker_db(this);
    private HashMap<String, ArrayList<DailyReadingValues>> weightReadigList;
    private ArrayList<DateValues> dateReadingList;
    private HashMap<String, ArrayList<DailyReadingValues>> glucoseReadingList;
    private ArrayList<DateValues> glucoseDateReadingList;
    private MyMarkerView mv;
    private String modeFlag = "";
    private String serviceUrl;
    private MyData myData;
    private String patientId;
    private String token;
    private String vitalClickFlag = "";
    private int fastingFlag = 0;         //not selected
    private int nonFastingFlag = 0;         //not selected
    private int allFlag = 0;         //not selected
    ArrayList<Integer> colors;
    ArrayList<Integer> colorsText;
    private double firstReading = 0;
    private double firstSysReading = 0;
    private double firstDiaReading = 0;
    private double firstPulseReading = 0;
    private double glucoseTotaleCount = 0;
    private double glucoseTotaleValue = 0;
    private LinearLayout layout_mydataFrame1;
    private LinearLayout layout_mydataFrame2;
    private LinearLayout layout_tableChart;
    private LinearLayout layout_average;
    private LinearLayout layout_tableCharRecycler1;
   private LinearLayout layout_tableCharRecycler2;
    private LinearLayout layout_tableCharRecycler3;
    private LinearLayout layout_pressureTableHeader1;
    private LinearLayout layout_pressureTableHeader2;
    private LinearLayout layout_pressureTableHeader3;

    private RelativeLayout layout_sensorHead;
    private RelativeLayout layout_weight;
    private RelativeLayout layout_bloodPressure;
    private RelativeLayout layout_bloodGlucose;
    private RelativeLayout layout_bloodOxygen;
    private RelativeLayout layout_temperature;

    private TextView txt_sensorName;
    private TextView txt_average;
    private TextView txt_averageHeader;
    private TextView txt_myDataHeader;
    private TextView txt_averageMin;
    private TextView txt_averageMax;
    private TextView txt_averageMinUnit;
    private TextView txt_averageMaxUnit;
    private TextView txt_pressureTableHeader;
    private ImageButton imgbtn_sensorImage;
   // private ImageButton imgbtn_downArrow;
    //private Button btn_average;
    private Button btn_lineChart;
    private Button btn_barChart;
    private Button btn_table;

    private LineChart lineChart;
    private BarChart barChart;

    private RecyclerView tableChartRecyclerView1;
    private RecyclerView tableChartRecyclerView2;
    private RecyclerView tableChartRecyclerView3;

    private RecyclerView.LayoutManager tableChartRecyclerLayoutManager1;
    private RecyclerView.LayoutManager tableChartRecyclerLayoutManager2;
    private RecyclerView.LayoutManager tableChartRecyclerLayoutManager3;

    private LinearLayout layout_glucoseFilter;
    private Button btn_glucoseAll;
    private Button btn_glucoseFasting;
    private Button btn_glucoseNonFasting;
    private FrameLayout layout_mydata;
    private LinearLayout layout_bottomButtons;
    Typeface type;
    private int yMaxValue = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mydata);
        modeFlag = "";

        globalClass = (GlobalClass) getApplicationContext();
        globalClass.isSupportEnable = true;
        SharedPreferences mydataSettings = getSharedPreferences("mydata", 0);
        SharedPreferences.Editor editor = mydataSettings.edit();
        editor.putString("click_time", globalClass.get_myData_date());
        editor.commit();
        type = Typeface.createFromAsset(this.getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        Integer a = 0;
        while (a < 10) {

            System.out.println("Immutable" + a);
            a += 1;

        }

        SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
        serviceUrl = settings.getString("Server_post_url", "-1");

//        SharedPreferences mydataSettings = getSharedPreferences("mydata", 0);
//        SharedPreferences.Editor editor = mydataSettings.edit();
//        editor.putString("click_time", globalClass.get_myData_date());
//        editor.commit();
        layout_mydata = (FrameLayout) findViewById(R.id.layout_mydata);
        layout_mydataFrame1 = (LinearLayout) findViewById(R.id.layout_mydataFrame1);
        layout_mydataFrame2 = (LinearLayout) findViewById(R.id.layout_mydataFrame2);
        layout_sensorHead = (RelativeLayout) findViewById(R.id.layout_sensorHead);
        layout_bottomButtons = (LinearLayout) findViewById(R.id.layout_bottomButtons);
        layout_weight = (RelativeLayout) findViewById(R.id.layout_weight);
        layout_bloodGlucose = (RelativeLayout) findViewById(R.id.layout_bloodGlucose);
        layout_bloodPressure = (RelativeLayout) findViewById(R.id.layout_bloodPressure);
        layout_bloodOxygen = (RelativeLayout) findViewById(R.id.layout_bloodOxygen);
        layout_temperature = (RelativeLayout) findViewById(R.id.layout_temperature);
        layout_glucoseFilter = (LinearLayout) findViewById(R.id.layout_glucoseFilter);
       // btn_average = (Button) findViewById(R.id.btn_average);
        btn_lineChart = (Button) findViewById(R.id.btn_lineChart);
        btn_barChart = (Button) findViewById(R.id.btn_barChart);
        btn_table = (Button) findViewById(R.id.btn_table);
        btn_glucoseAll = (Button) findViewById(R.id.btn_glucoseAll);
        btn_glucoseFasting = (Button) findViewById(R.id.btn_glucoseFasting);
        btn_glucoseNonFasting = (Button) findViewById(R.id.btn_glucoseNonFasting);
        layout_weight.setOnClickListener(this);
        layout_bloodPressure.setOnClickListener(this);
        layout_bloodOxygen.setOnClickListener(this);
        layout_bloodGlucose.setOnClickListener(this);
        layout_temperature.setOnClickListener(this);
        layout_sensorHead.setOnClickListener(this);
       // btn_average.setOnClickListener(this);
        btn_lineChart.setOnClickListener(this);
        btn_barChart.setOnClickListener(this);
        btn_table.setOnClickListener(this);
        btn_glucoseAll.setOnClickListener(this);
        btn_glucoseFasting.setOnClickListener(this);
        btn_glucoseNonFasting.setOnClickListener(this);

        imgbtn_sensorImage = (ImageButton) findViewById(R.id.imgbtn_sensorImage);
//        imgbtn_downArrow = (ImageButton) findViewById(R.id.imgbtn_downArrow);
//        imgbtn_downArrow.setOnClickListener(this);
        txt_sensorName = (TextView) findViewById(R.id.txt_sensorName);
        txt_averageHeader = (TextView) findViewById(R.id.txt_averageHeader);
        txt_myDataHeader = (TextView) findViewById(R.id.txt_myDataHeader);
        txt_myDataHeader.setTypeface(type, Typeface.BOLD);
        txt_average = (TextView) findViewById(R.id.txt_average);
        txt_averageMin = (TextView) findViewById(R.id.txt_averageMin);
        txt_averageMinUnit = (TextView) findViewById(R.id.txt_averageMinUnit);
        txt_averageMax = (TextView) findViewById(R.id.txt_averageMax);
        txt_averageMaxUnit = (TextView) findViewById(R.id.txt_averageMaxUnit);
        txt_pressureTableHeader = (TextView) findViewById(R.id.txt_tableChartValueHeader);
        txt_pressureTableHeader.setTypeface(type, Typeface.BOLD);
        lineChart = (LineChart) findViewById(R.id.lineChart);
        barChart = (BarChart) findViewById(R.id.barChart);

        layout_tableCharRecycler1 = (LinearLayout) findViewById(R.id.layout_tableCharRecycler1);
        layout_tableCharRecycler2 = (LinearLayout) findViewById(R.id.layout_tableCharRecycler2);
        layout_tableCharRecycler3 = (LinearLayout) findViewById(R.id.layout_tableCharRecycler3);
        layout_pressureTableHeader1 = (LinearLayout) findViewById(R.id.layout_pressureTableHeader);
        layout_pressureTableHeader2 = (LinearLayout) findViewById(R.id.layout_pressureTableHeader2);
        layout_pressureTableHeader3 = (LinearLayout) findViewById(R.id.layout_pressureTableHeader3);
        layout_tableChart = (LinearLayout) findViewById(R.id.layout_tableChart);
        layout_average = (LinearLayout) findViewById(R.id.layout_average);
        tableChartRecyclerView1 = (RecyclerView) findViewById(R.id.review_tableChart1);
        tableChartRecyclerView2 = (RecyclerView) findViewById(R.id.review_tableChart2);
        tableChartRecyclerView3 = (RecyclerView) findViewById(R.id.review_tableChart3);
        tableChartRecyclerLayoutManager1 = new LinearLayoutManager(this);
        tableChartRecyclerLayoutManager2 = new LinearLayoutManager(this);
        tableChartRecyclerLayoutManager3 = new LinearLayoutManager(this);
        tableChartRecyclerView1.setLayoutManager(tableChartRecyclerLayoutManager1);
        tableChartRecyclerView2.setLayoutManager(tableChartRecyclerLayoutManager2);
        tableChartRecyclerView3.setLayoutManager(tableChartRecyclerLayoutManager3);

//        tableChartRecyclerView1.addOnScrollListener(new RecyclerView.OnScrollListener() {
//
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
////                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
////                    Log.i("toched", "toched");
////                }
//                if (newState == RecyclerView.SCROLL_STATE_SETTLING) {
//
//                    MyDataTracking("3", "5");
//                }
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//
//                Log.i("toched", "toched");
//                //MyDataTracking("3","5");
//
//
//            }
//        });

        showVitalButtons();
        SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
        token = tokenPreference.getString("Token_ID", "-1");
        patientId = tokenPreference.getString("patient_id", "-1");//"2000001"
        String p = patientId;
SharedPreferences settingswifi = getSharedPreferences(CommonUtilities.WiFi_SP, 0);
		
		imeilNo = settingswifi.getString("imei_no", "");
		String i = imeilNo;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.layout_weight:
                pageName = "My Data Weight Page";
                //globalClass.appTracking("My Data Page", "Click on Weight button");
                weightClick();
                break;
            case R.id.layout_temperature:
                pageName = "My Data Temperature Page";
                //globalClass.appTracking("My Data Page", "Click on Temperature button");
                tempClick();
                break;
            case R.id.layout_bloodGlucose:
                pageName = "My Data Blood Glucose Page";
                //globalClass.appTracking("My Data Page", "Click on Blood Glucose button");
                glucoseClick();
                break;
            case R.id.layout_bloodOxygen:
                pageName = "My Data Blood Oxygen Page";
               // globalClass.appTracking("My Data Page", "Click on Blood Oxygen button");
                oxygenClick();
                break;
            case R.id.layout_bloodPressure:
                pageName = "My Data Blood Pressure Page";
                //globalClass.appTracking("My Data Page", "Click on Blood Pressure button");
                pressureClick();
                break;
            case R.id.layout_sensorHead:

                layout_mydataFrame1.setVisibility(View.VISIBLE);
                layout_mydataFrame2.setVisibility(View.GONE);
                break;
//            case R.id.imgbtn_downArrow:
//
//                layout_mydataFrame1.setVisibility(View.VISIBLE);
//                layout_mydataFrame2.setVisibility(View.GONE);
//                break;
//            case R.id.btn_average:
//               // globalClass.appTracking(pageName, "Change to Avg");
//                //averageButtonClick();
//                break;
            case R.id.btn_lineChart:
                //globalClass.appTracking(pageName, "Change to Line graph");
                lineChartButtonClick();
                break;
            case R.id.btn_barChart:
               // globalClass.appTracking(pageName, "Change to Bar graph");
                barChartButtonClick();
                break;
            case R.id.btn_table:
                //globalClass.appTracking(pageName, "Change to Table");
                tableChartButtonClick();
                break;
            case R.id.btn_glucoseAll:
                allFlag = 1;
                glucoseAllClick();
                break;
            case R.id.btn_glucoseFasting:
                glucoseFastingClick();
                break;
            case R.id.btn_glucoseNonFasting:
                glucoseNonFastingClick();
                break;
        }
    }

    private void showVitalButtons() {

        layout_bottomButtons.setVisibility(View.GONE);
        Cursor c = myDataSensor_db.SelectMydataSensors();
        c.moveToFirst();
        int size = c.getCount();

        if (size > 0) {

            while (c.isAfterLast() == false) {

                int a = Integer.parseInt(c.getString(0));
                c.moveToNext();

                if (a == 7) {

                    layout_weight.setVisibility(View.VISIBLE);
                    if (size == 1) {
                        weightClick();
                    }

                } else if (a == 6) {

                    layout_temperature.setVisibility(View.VISIBLE);
                    if (size == 1) {
                        tempClick();
                    }

                } else if (a == 3) {

                    layout_bloodPressure.setVisibility(View.VISIBLE);
                    if (size == 1) {
                        pressureClick();
                    }

                } else if (a == 2) {

                    layout_bloodOxygen.setVisibility(View.VISIBLE);
                    if (size == 1) {
                        oxygenClick();
                    }

                } else if (a == 1) {

                    layout_bloodGlucose.setVisibility(View.VISIBLE);
                    if (size == 1) {
                        glucoseClick();
                    }

                }
            }
            if (size > 1) {

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.myDataMultipleSensorMessage), Toast.LENGTH_LONG).show();

            }
            layout_mydataFrame1.setVisibility(View.VISIBLE);
        } else {

            Toast.makeText(getApplicationContext(), this.getString(R.string.noVitalAssigned), Toast.LENGTH_SHORT).show();
        }
    }

    private void weightClick() {

        vitalClickFlag = "weight";

        layout_mydataFrame1.setVisibility(View.GONE);
        layout_mydataFrame2.setVisibility(View.VISIBLE);
        layout_bottomButtons.setVisibility(View.VISIBLE);
        imgbtn_sensorImage.setBackgroundResource(R.drawable.wt);
        txt_sensorName.setText(getResources().getString(R.string.weight));
        layout_sensorHead.setVisibility(View.VISIBLE);
        txt_averageHeader.setText(getResources().getString(R.string.averageWeightHeader));
        txt_myDataHeader.setText(getResources().getString(R.string.weightHeader));
        txt_averageMinUnit.setVisibility(View.VISIBLE);
        txt_averageMaxUnit.setVisibility(View.VISIBLE);
        layout_glucoseFilter.setVisibility(View.GONE);
        //if (getResources().getBoolean(R.bool.isTab)) {
            layout_weight.setBackgroundResource(R.drawable.vital_button_selector);
            layout_temperature.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodPressure.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodOxygen.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodGlucose.setBackgroundResource(R.drawable.home_layout_background);

        //} //lineChartButtonClick();
        ClearList();

        DownloadWeightData task = new DownloadWeightData();
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void tempClick() {
        vitalClickFlag = "temp";
        layout_mydataFrame1.setVisibility(View.GONE);
        layout_mydataFrame2.setVisibility(View.VISIBLE);
        layout_bottomButtons.setVisibility(View.VISIBLE);
        imgbtn_sensorImage.setBackgroundResource(R.drawable.temp);
        txt_sensorName.setText(getResources().getString(R.string.temp));
        layout_sensorHead.setVisibility(View.VISIBLE);
        txt_averageHeader.setText(getResources().getString(R.string.averageTempHeader));
        txt_myDataHeader.setText(getResources().getString(R.string.tempHeader));
        txt_averageMinUnit.setVisibility(View.VISIBLE);
        txt_averageMaxUnit.setVisibility(View.VISIBLE);
        layout_glucoseFilter.setVisibility(View.GONE);
       // if (getResources().getBoolean(R.bool.isTab)) {
            layout_weight.setBackgroundResource(R.drawable.home_layout_background);
            layout_temperature.setBackgroundResource(R.drawable.vital_button_selector);
            layout_bloodPressure.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodOxygen.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodGlucose.setBackgroundResource(R.drawable.home_layout_background);
       // }//lineChartButtonClick();
        ClearList();

        DownloadTemperatureData task = new DownloadTemperatureData();
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void pressureClick() {
        vitalClickFlag = "pressure";
        layout_mydataFrame1.setVisibility(View.GONE);
        layout_mydataFrame2.setVisibility(View.VISIBLE);
        layout_bottomButtons.setVisibility(View.VISIBLE);
        imgbtn_sensorImage.setBackgroundResource(R.drawable.bp);
        txt_sensorName.setText(getResources().getString(R.string.bloodPressure));
        layout_sensorHead.setVisibility(View.VISIBLE);
        txt_averageHeader.setText(getResources().getString(R.string.averagePressureHeader));
        txt_myDataHeader.setText(getResources().getString(R.string.pressureHeader));
        txt_averageMinUnit.setVisibility(View.GONE);
        txt_averageMaxUnit.setVisibility(View.GONE);
        layout_glucoseFilter.setVisibility(View.GONE);
        //if (getResources().getBoolean(R.bool.isTab)) {
            layout_weight.setBackgroundResource(R.drawable.home_layout_background);
            layout_temperature.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodPressure.setBackgroundResource(R.drawable.vital_button_selector);
            layout_bloodOxygen.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodGlucose.setBackgroundResource(R.drawable.home_layout_background);
        //}//lineChartButtonClick();
        ClearList();

        DownloadBloodPressureData task = new DownloadBloodPressureData();
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void oxygenClick() {
        vitalClickFlag = "oxygen";
        layout_mydataFrame1.setVisibility(View.GONE);
        layout_mydataFrame2.setVisibility(View.VISIBLE);
        layout_bottomButtons.setVisibility(View.VISIBLE);
        imgbtn_sensorImage.setBackgroundResource(R.drawable.bo);
        txt_sensorName.setText(getResources().getString(R.string.bloodoxygen));
        layout_sensorHead.setVisibility(View.VISIBLE);
        txt_averageHeader.setText(getResources().getString(R.string.averageOxygenHeader));
       txt_myDataHeader.setText(getResources().getString(R.string.oxygenHeader));
        txt_averageMinUnit.setVisibility(View.GONE);
        txt_averageMaxUnit.setVisibility(View.GONE);
        layout_glucoseFilter.setVisibility(View.GONE);
       // if (getResources().getBoolean(R.bool.isTab)) {
            layout_weight.setBackgroundResource(R.drawable.home_layout_background);
            layout_temperature.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodPressure.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodOxygen.setBackgroundResource(R.drawable.vital_button_selector);
            layout_bloodGlucose.setBackgroundResource(R.drawable.home_layout_background);
       // }//lineChartButtonClick();
        ClearList();

        DownloadBloodOxygenData task = new DownloadBloodOxygenData();
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void glucoseClick() {
        vitalClickFlag = "glucose";
        layout_mydataFrame1.setVisibility(View.GONE);
        layout_mydataFrame2.setVisibility(View.VISIBLE);
        layout_bottomButtons.setVisibility(View.VISIBLE);
        imgbtn_sensorImage.setBackgroundResource(R.drawable.bg);
        txt_sensorName.setText(getResources().getString(R.string.bloodglucose));
        layout_sensorHead.setVisibility(View.VISIBLE);
        txt_averageHeader.setText(getResources().getString(R.string.averageGlucoseHeader));
        txt_myDataHeader.setText(getResources().getString(R.string.glucoseHeader));
        txt_averageMinUnit.setVisibility(View.GONE);
        txt_averageMaxUnit.setVisibility(View.GONE);
        //layout_glucoseFilter.setVisibility(View.VISIBLE);
       // if (getResources().getBoolean(R.bool.isTab)) {
            layout_weight.setBackgroundResource(R.drawable.home_layout_background);
            layout_temperature.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodPressure.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodOxygen.setBackgroundResource(R.drawable.home_layout_background);
            layout_bloodGlucose.setBackgroundResource(R.drawable.vital_button_selector);
       // }//lineChartButtonClick();
        ClearList();

        DownloadBloodGlucoseData task = new DownloadBloodGlucoseData();
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void glucoseAllClick() {

        fastingFlag = 0;
        nonFastingFlag = 0;
        glucoseTotaleValue = 0;
        glucoseTotaleCount = 0;
        dateReadingList.clear();
        weightReadigList.clear();

        for (int i = 0; i < glucoseDateReadingList.size(); i++) {

            DateValues dateValues = glucoseDateReadingList.get(i);
            String key = dateValues.getDateValue();
            ArrayList<DailyReadingValues> resultList = new ArrayList<DailyReadingValues>();
            ArrayList<DailyReadingValues> glucoseDailyReadingList = glucoseReadingList.get(key);
            double value = 0;
            for (int j = 0; j < glucoseDailyReadingList.size(); j++) {

                DailyReadingValues dailyReadingValues = glucoseDailyReadingList.get(j);
                resultList.add(dailyReadingValues);
                value += dailyReadingValues.getValue();
                if (dateValues.getDataFlag() == 1)
                    glucoseTotaleValue += dailyReadingValues.getValue();

            }

            if (dateValues.getDataFlag() == 0) {

                dateReadingList.add(dateValues);
                weightReadigList.put(key, resultList);
            } else {
                if (resultList.size() > 0) {
                    glucoseTotaleCount += resultList.size();
                    dateValues.setDailyAverage(value / resultList.size());
                    dateReadingList.add(dateValues);
                    weightReadigList.put(key, resultList);
                }
            }

        }
        double dd = glucoseTotaleValue / glucoseTotaleCount;
        myData.setAvgValue(dd);
        btn_glucoseAll.setBackgroundResource(R.drawable.filter_button_pressed);
        btn_glucoseAll.setTextColor(Color.parseColor("#e87722"));
        btn_glucoseFasting.setBackgroundResource(R.drawable.filter_button_default);
        btn_glucoseFasting.setTextColor(Color.parseColor("#b2b2b2"));
        btn_glucoseNonFasting.setBackgroundResource(R.drawable.filter_button_default);
        btn_glucoseNonFasting.setTextColor(Color.parseColor("#b2b2b2"));
        if (allFlag == 1) {
            tableChartMobile_Tab_Handling();
            showLineChart();
            lineChart.invalidate();
            showBarChart();
            barChart.invalidate();
        }
    }

    private void glucoseFastingClick() {
        glucoseTotaleValue = 0;
        glucoseTotaleCount = 0;
        if (fastingFlag == 1) {
            fastingFlag = 0;
            nonFastingFlag = 0;
            glucoseAllClick();
        } else {
            fastingFlag = 1;
            nonFastingFlag = 0;
            dateReadingList.clear();
            weightReadigList.clear();
            for (int i = 0; i < glucoseDateReadingList.size(); i++) {

                DateValues dateValues = glucoseDateReadingList.get(i);
                String key = dateValues.getDateValue();
                ArrayList<DailyReadingValues> resultList = new ArrayList<DailyReadingValues>();
                ArrayList<DailyReadingValues> glucoseDailyReadingList = glucoseReadingList.get(key);
                double value = 0;
                for (int j = 0; j < glucoseDailyReadingList.size(); j++) {

                    DailyReadingValues dailyReadingValues = glucoseDailyReadingList.get(j);
                    if (dailyReadingValues.getMeal().equalsIgnoreCase("fasting")) {

                        resultList.add(dailyReadingValues);
                        value += dailyReadingValues.getValue();
                        if (dateValues.getDataFlag() == 1)
                            glucoseTotaleValue += dailyReadingValues.getValue();

                    }
                }
                if (dateValues.getDataFlag() == 0) {

                    dateReadingList.add(dateValues);
                    weightReadigList.put(key, resultList);
                } else {
                    if (resultList.size() > 0) {
                        glucoseTotaleCount += resultList.size();
                        dateValues.setDailyAverage(value / resultList.size());
                        dateReadingList.add(dateValues);
                        weightReadigList.put(key, resultList);
                    }
                }
            }
            myData.setAvgValue(glucoseTotaleValue / glucoseTotaleCount);
            btn_glucoseAll.setBackgroundResource(R.drawable.filter_button_default);
            btn_glucoseAll.setTextColor(Color.parseColor("#b2b2b2"));
            btn_glucoseFasting.setBackgroundResource(R.drawable.filter_button_pressed);
            btn_glucoseFasting.setTextColor(Color.parseColor("#e87722"));
            btn_glucoseNonFasting.setBackgroundResource(R.drawable.filter_button_default);
            btn_glucoseNonFasting.setTextColor(Color.parseColor("#b2b2b2"));
            tableChartMobile_Tab_Handling();
            showLineChart();
            lineChart.invalidate();
            showBarChart();
            barChart.invalidate();
        }


    }

    private void glucoseNonFastingClick() {
        glucoseTotaleValue = 0;
        glucoseTotaleCount = 0;
        if (nonFastingFlag == 1) {
            nonFastingFlag = 0;
            fastingFlag = 0;
            glucoseAllClick();
        } else {
            nonFastingFlag = 1;
            fastingFlag = 0;
            dateReadingList.clear();
            weightReadigList.clear();
            for (int i = 0; i < glucoseDateReadingList.size(); i++) {

                DateValues dateValues = glucoseDateReadingList.get(i);
                String key = dateValues.getDateValue();
                ArrayList<DailyReadingValues> resultList = new ArrayList<DailyReadingValues>();
                ArrayList<DailyReadingValues> glucoseDailyReadingList = glucoseReadingList.get(key);
                double value = 0;
                for (int j = 0; j < glucoseDailyReadingList.size(); j++) {

                    DailyReadingValues dailyReadingValues = glucoseDailyReadingList.get(j);
                    if (!dailyReadingValues.getMeal().equalsIgnoreCase("fasting")) {

                        resultList.add(dailyReadingValues);
                        value += dailyReadingValues.getValue();
                        if (dateValues.getDataFlag() == 1)
                            glucoseTotaleValue += dailyReadingValues.getValue();

                    }
                }
                if (dateValues.getDataFlag() == 0) {

                    dateReadingList.add(dateValues);
                    weightReadigList.put(key, resultList);
                } else {
                    if (resultList.size() > 0) {
                        glucoseTotaleCount += resultList.size();
                        dateValues.setDailyAverage(value / resultList.size());
                        dateReadingList.add(dateValues);
                        weightReadigList.put(key, resultList);
                    }
                }
            }
            myData.setAvgValue(glucoseTotaleValue / glucoseTotaleCount);
            btn_glucoseAll.setBackgroundResource(R.drawable.filter_button_default);
            btn_glucoseAll.setTextColor(Color.parseColor("#b2b2b2"));
            btn_glucoseFasting.setBackgroundResource(R.drawable.filter_button_default);
            btn_glucoseFasting.setTextColor(Color.parseColor("#b2b2b2"));
            btn_glucoseNonFasting.setBackgroundResource(R.drawable.filter_button_pressed);
            btn_glucoseNonFasting.setTextColor(Color.parseColor("#e87722"));
            tableChartMobile_Tab_Handling();
            showLineChart();
            lineChart.invalidate();
            showBarChart();
            barChart.invalidate();
        }

    }

//    private void averageButtonClick() {
//        String avg = "";
//        String min = "";
//        String max = "";
//        modeFlag = "";
//        txt_myDataHeader.setVisibility(View.GONE);
//        if (vitalClickFlag.equals("pressure")) {
//            avg = (int) myData.getSystolicAvgValue() + " / " +
//                    (int) myData.getDiastolicAvgValue() + " / " +
//                    (int) myData.getPulseAvgValue();
//            min = (int) myData.getSystolicMinValue() + " /\n" +
//                    (int) myData.getDiastolicMinValue() + " /\n" +
//                    (int) myData.getPulseMinValue();
//            max = (int) myData.getSystolicMaxValue() + " /\n" +
//                    (int) myData.getDiastolicMaxValue() + " /\n" +
//                    (int) myData.getPulseMaxValue();
//        } else if (vitalClickFlag.equals("oxygen")) {
//            avg = "" + (int) myData.getOxygenAvgValue();// + " / " +
//            //Util.round(myData.getPulseAvgValue());
//            min = "" + (int) myData.getOxygenMinValue();// + " / " +
//            //Util.round(myData.getPulseMinValue());
//            max = "" + (int) myData.getOxygenMaxValue();// + " / " +
//            //Util.round(myData.getPulseMaxValue());
//        } else if (vitalClickFlag.equals("weight")) {
//            avg = "" + Util.round(myData.getAvgValue());
//            if (myData.getUnit().equals("Kg")) {
//                if (myData.getMinValue() != 0) {
//                    double res = myData.getMinValue() / 2.2046;
//                    min = "" + Util.round(res);
//                } else {
//
//                    min = "" + Util.round(myData.getMinValue());
//                }
//                if (myData.getMaxValue() != 0) {
//                    double res = myData.getMaxValue() / 2.2046;
//                    max = "" + Util.round(res);
//                } else {
//
//                    max = "" + Util.round(myData.getMaxValue());
//                }
//            } else {
//                max = "" + Util.round(myData.getMaxValue());
//                min = "" + Util.round(myData.getMinValue());
//            }
//
//
//        } else if (vitalClickFlag.equals("temp")) {
//
//            avg = "" + Util.round(myData.getAvgValue());
//            if (myData.getUnit().equals("C")) {
//                if (myData.getMinValue() != 0) {
//                    double res = myData.getMinValue() - 32;
//                    double res1 = (res * 5) / 9;
//                    min = "" + Util.round(res1);
//                } else {
//
//                    min = "" + Util.round(myData.getMinValue());
//                }
//                if (myData.getMaxValue() != 0) {
//                    double res = myData.getMaxValue() - 32;
//                    double res1 = (res * 5) / 9;
//                    max = "" + Util.round(res1);
//                } else {
//
//                    max = "" + Util.round(myData.getMaxValue());
//                }
//            } else {
//                max = "" + Util.round(myData.getMaxValue());
//                min = "" + Util.round(myData.getMinValue());
//            }
//        } else {
//
//            avg = "" + Util.round(myData.getAvgValue());
//            min = "" + Util.round(myData.getMinValue());
//            max = "" + Util.round(myData.getMaxValue());
//        }
//        if (vitalClickFlag.equals("glucose")) {
//
//            layout_glucoseFilter.setVisibility(View.GONE);
//        }
//        layout_pressureTableHeader1.setVisibility(View.GONE);
//        layout_pressureTableHeader2.setVisibility(View.GONE);
//        layout_pressureTableHeader3.setVisibility(View.GONE);
//        txt_average.setText(avg);
//        txt_averageMin.setText(min);
//        txt_averageMax.setText(max);
//        txt_averageMinUnit.setText(myData.getUnit());
//        txt_averageMaxUnit.setText(myData.getUnit());
//        btn_average.setTextColor(Color.parseColor("#E87722"));
//        btn_lineChart.setBackgroundResource(R.drawable.line_chart_icon);
//        btn_barChart.setBackgroundResource(R.drawable.bar_chart_icon);
//        btn_table.setBackgroundResource(R.drawable.table_icon);
//        layout_average.setVisibility(View.VISIBLE);
//        lineChart.setVisibility(View.GONE);
//        barChart.setVisibility(View.GONE);
//        layout_tableChart.setVisibility(View.GONE);
//
//    }

    private void averageButtonClickTab() {
        String avg = "";
        String min = "";
        String max = "";
        txt_myDataHeader.setVisibility(View.VISIBLE);
        if (dateReadingList.size() > 0) {
            if (vitalClickFlag.equals("pressure")) {
                avg = Util.round((double) myData.getSystolicAvgValue()) + " / " +
                		Util.round((double) myData.getDiastolicAvgValue()) + " / " +
                		Util.round((double) myData.getPulseAvgValue());
                min = Util.round((double) myData.getSystolicMinValue()) + " /" +
                		Util.round((double) myData.getDiastolicMinValue()) + " /" +
                		Util.round((double) myData.getPulseMinValue());
                max = Util.round((double) myData.getSystolicMaxValue()) + " / " +
                		Util.round((double) myData.getDiastolicMaxValue()) + " /" +
                		Util.round((double) myData.getPulseMaxValue());
            } else if (vitalClickFlag.equals("oxygen")) {
                avg = "" + Util.round((double) myData.getOxygenAvgValue()); // + " / " +
                //Util.round(myData.getPulseAvgValue());
                min = "" + Util.round((double) myData.getOxygenMinValue());// + " / " +
                // Util.round(myData.getPulseMinValue());
                max = "" + Util.round((double) myData.getOxygenMaxValue());// + " / " +
                //Util.round(myData.getPulseMaxValue());
            
            } else {
                avg = "" + Util.round(myData.getAvgValue());
                min = "" + Util.round(myData.getMinValue());
                max = "" + Util.round(myData.getMaxValue());
            }
            if (vitalClickFlag.equals("glucose")) {

                layout_glucoseFilter.setVisibility(View.GONE);
            }
            layout_pressureTableHeader1.setVisibility(View.GONE);
            layout_pressureTableHeader2.setVisibility(View.GONE);
            layout_pressureTableHeader3.setVisibility(View.GONE);
            txt_average.setText(avg);
            txt_averageMin.setText(min);
            txt_averageMax.setText(max);
            txt_averageMinUnit.setText(myData.getUnit());
            txt_averageMaxUnit.setText(myData.getUnit());
//            btn_average.setTextColor(Color.parseColor("#E87722"));
//            btn_lineChart.setBackgroundResource(R.drawable.line_chart_icon);
//            btn_barChart.setBackgroundResource(R.drawable.bar_chart_icon);
//            btn_table.setBackgroundResource(R.drawable.table_icon);
//            layout_average.setVisibility(View.VISIBLE);
//            lineChart.setVisibility(View.GONE);
//            barChart.setVisibility(View.GONE);
//            layout_tableChart.setVisibility(View.GONE);
        } else {

        }
    }

    private void lineChartButtonClick() {

        if (mv != null) {
        	lineChart.fitScreen();
            lineChart.clear();
            lineChart.removeAllViews();
            lineChart.removeAllViewsInLayout();
            mv.removeAllViews();
            mv.removeAllViewsInLayout();
            mv.clearFocus();
            mv.invalidate();

            lineChart.invalidate();


        }
        txt_myDataHeader.setVisibility(View.VISIBLE);
        modeFlag = "line";
        //MyDataTracking("1", "4");
        if (vitalClickFlag.equals("pressure")) {

            showBloodPressureLineChart();
        } else if (vitalClickFlag.equals("oxygen")) {

            showBloodOxygenLineChart();
        } else {

            showLineChart();
        }

        layout_pressureTableHeader1.setVisibility(View.GONE);
        layout_pressureTableHeader2.setVisibility(View.GONE);
        layout_pressureTableHeader3.setVisibility(View.GONE);
        //btn_average.setTextColor(Color.parseColor("#9f9f9f"));
        btn_lineChart.setBackgroundResource(R.drawable.line_chart_icon_sel);
        btn_barChart.setBackgroundResource(R.drawable.bar_chart_icon);
        btn_table.setBackgroundResource(R.drawable.table_icon);
        //if (getResources().getBoolean(R.bool.isTab)) {
            layout_average.setVisibility(View.VISIBLE);
            averageButtonClickTab();
//        } else {
//
//            layout_average.setVisibility(View.GONE);
//        }

        lineChart.setVisibility(View.VISIBLE);
        barChart.setVisibility(View.GONE);
        layout_tableChart.setVisibility(View.GONE);
        lineChart.invalidate();
//        if (vitalClickFlag.equals("glucose")) {
//
//            layout_glucoseFilter.setVisibility(View.VISIBLE);
//        }

    }

    private void barChartButtonClick() {
        if (mv != null) {
        	barChart.fitScreen();
            barChart.clear();
            barChart.removeAllViews();
            barChart.removeAllViewsInLayout();
            mv.removeAllViews();
            mv.removeAllViewsInLayout();
            mv.invalidate();
            barChart.invalidate();


        }
        txt_myDataHeader.setVisibility(View.VISIBLE);
        modeFlag = "bar";
        //MyDataTracking("2", "4");
        if (vitalClickFlag.equals("pressure")) {

            showBloodPressureBarChart();
        } else if (vitalClickFlag.equals("oxygen")) {

            showBloodOxygenBarChart();
        } else {

            showBarChart();
        }

        layout_pressureTableHeader1.setVisibility(View.GONE);
        layout_pressureTableHeader2.setVisibility(View.GONE);
        layout_pressureTableHeader3.setVisibility(View.GONE);
        //btn_average.setTextColor(Color.parseColor("#9f9f9f"));
        btn_lineChart.setBackgroundResource(R.drawable.line_chart_icon);
        btn_barChart.setBackgroundResource(R.drawable.bar_chart_icon_sel);
        btn_table.setBackgroundResource(R.drawable.table_icon);
       // if (getResources().getBoolean(R.bool.isTab)) {
            layout_average.setVisibility(View.VISIBLE);
            averageButtonClickTab();
//        } else {
//
//            layout_average.setVisibility(View.GONE);
//        }
        lineChart.setVisibility(View.GONE);
        barChart.setVisibility(View.VISIBLE);
        layout_tableChart.setVisibility(View.GONE);
//        if (vitalClickFlag.equals("glucose")) {
//
//            layout_glucoseFilter.setVisibility(View.VISIBLE);
//        }

    }

    private void tableChartButtonClick() {
       txt_myDataHeader.setVisibility(View.VISIBLE);
        modeFlag = "table";
        //MyDataTracking("3", "4");


        //if (getResources().getBoolean(R.bool.isTab)) {
            layout_average.setVisibility(View.VISIBLE);
            averageButtonClickTab();
//        } else {
//
//            layout_average.setVisibility(View.GONE);
//        }
        //btn_average.setTextColor(Color.parseColor("#9f9f9f"));
        btn_lineChart.setBackgroundResource(R.drawable.line_chart_icon);
        btn_barChart.setBackgroundResource(R.drawable.bar_chart_icon);
        btn_table.setBackgroundResource(R.drawable.table_icon_sel);
        lineChart.setVisibility(View.GONE);
        barChart.setVisibility(View.GONE);
        layout_tableChart.setVisibility(View.VISIBLE);

//        if (vitalClickFlag.equals("glucose")) {
//
//            layout_glucoseFilter.setVisibility(View.VISIBLE);
//        }
        if (vitalClickFlag.equals("pressure")) {
            layout_pressureTableHeader1.setVisibility(View.VISIBLE);
            layout_pressureTableHeader2.setVisibility(View.VISIBLE);
            layout_pressureTableHeader3.setVisibility(View.VISIBLE);
        } else {
            layout_pressureTableHeader1.setVisibility(View.GONE);
            layout_pressureTableHeader2.setVisibility(View.GONE);
            layout_pressureTableHeader3.setVisibility(View.GONE);

        }

        tableChartMobile_Tab_Handling();
    }


    private class DownloadWeightData extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
//            if (result.equalsIgnoreCase("AuthenticationError")) {
//                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
//                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
//
//                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
//               // logoutClick();
//            } else {
                if (result.equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.noVitals), Toast.LENGTH_SHORT).show();
                } else {
                    if (modeFlag.equals("bar")) {

                        barChartButtonClick();
                    } else if (modeFlag.equals("table")) {

                        tableChartButtonClick();
                    } else {

                        lineChartButtonClick();
                    }
                }
//            }
        }

        @Override
        protected void onPreExecute() {
            try {
                progressDialog = new ProgressDialog(MyDataActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loadingMydata));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        protected String doInBackground(String... urls) {
            String jsonResponse = "";
            boolean isFirstReading = false;
            firstReading = 0;
            try {
                Log.e("DROID", "patient details url" + serviceUrl
                        + "/droid_website/rtd_get_tpm_body_weight.ashx");
                String VersionNumber = getResources().getString(
                        R.string.version);

                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/rtd_get_tpm_body_weight.ashx",
                        new String[]{"imei_no", "patient_id"}, new String[]{imeilNo, patientId});


                if (response1 != null) {
                    jsonResponse = Util
                            .readStream(response1.getEntity().getContent());

                } else {

                    return "";
                }
                //text = sb.toString();
                //Log.i("myData response", "myData response:-" + jsonResponse);
                JSONObject jsonObject = new JSONObject(jsonResponse);
//                if (jsonObject.optString("mob_response").equals("AuthenticationError")) {
//                    jsonResponse = jsonObject.optString("mob_response");
//                    return jsonResponse;
//                }
               
                myData.setAvgValue(jsonObject.optDouble("average_value"));
                myData.setUnit(jsonObject.optString("unit"));
                if (myData.getUnit().equals("Kg")) {

                    double res = jsonObject.optDouble("min_value") / 2.2046;
                    double minValue = Util.round(res);
                    double res1 = jsonObject.optDouble("max_value") / 2.2046;
                    double maxValue = Util.round(res1);
                    myData.setMinValue(minValue);
                    myData.setMaxValue(maxValue);
                } else {
                    myData.setMinValue(jsonObject.optDouble("min_value"));
                    myData.setMaxValue(jsonObject.optDouble("max_value"));
                }
                int[] limitRange = calculateLimitRange((double) myData.getMaxValue(),
                        (double) jsonObject.optDouble("highest_value"),
                        (double) myData.getMinValue(),
                        (double) jsonObject.optDouble("lowest_value"));
                myData.setHighestValue(limitRange[0]);
                myData.setLowestValue(limitRange[1]);

                JSONArray readingsJsonArray = jsonObject.getJSONArray("readings");
                for (int i = 0; i < readingsJsonArray.length(); i++) {

                    JSONObject readingsObject = readingsJsonArray.getJSONObject(i);
                    ArrayList<DailyReadingValues> dailyReadingValueList = new ArrayList<DailyReadingValues>();
                    JSONArray dailyReadingsJsonArray = readingsObject.getJSONArray("day_time");

                    for (int j = 0; j < dailyReadingsJsonArray.length(); j++) {

                        DailyReadingValues dailyReadingValues = new DailyReadingValues();
                        JSONObject dailyReadingsObject = dailyReadingsJsonArray.getJSONObject(j);
                        dailyReadingValues.setTime(dailyReadingsObject.optString("time"));
                        dailyReadingValues.setValue(dailyReadingsObject.optDouble("value"));
                        dailyReadingValueList.add(dailyReadingValues);
                    }

                    String date = readingsObject.optString("date");
                    double dailyAvg = readingsObject.optDouble("avg_day_value");
                    int dataFlag = readingsObject.optInt("flag");
                    if (isFirstReading == false) {
                        firstReading = dailyAvg;
                        if (firstReading > 0) {
                            isFirstReading = true;
                        }
                    }
                    DateValues dateValues = new DateValues();
                    dateValues.setDateValue(date);
                    dateValues.setDailyAverage(dailyAvg);
                    dateValues.setDataFlag(dataFlag);
                    dateReadingList.add(dateValues);
                    weightReadigList.put(date, dailyReadingValueList);
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }

            return jsonResponse;
        }
    }

    private class DownloadTemperatureData extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
//            if (result.equalsIgnoreCase("AuthenticationError")) {
//                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
//                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
//
//                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
//               // logoutClick();
//            } else {
                if (result.equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.noVitals), Toast.LENGTH_SHORT).show();
                } else {
                    if (modeFlag.equals("bar")) {

                        barChartButtonClick();
                    } else if (modeFlag.equals("table")) {

                        tableChartButtonClick();
                    } else {

                        lineChartButtonClick();
                    }
                }
//            }
        }

        @Override
        protected void onPreExecute() {
            try {
                progressDialog = new ProgressDialog(MyDataActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loadingMydata));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        protected String doInBackground(String... urls) {
            String jsonResponse = "";
            boolean isFirstReading = false;
            firstReading = 0;
            try {
                Log.e("DROID", "patient details url" + serviceUrl
                        + "/droid_website/rtd_get_tpm_body_temperature.ashx");
                String VersionNumber = getResources().getString(
                        R.string.version);

                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/rtd_get_tpm_body_temperature.ashx",
                        new String[]{"imei_no", "patient_id"}, new String[]{imeilNo, patientId});


                if (response1 != null) {
                    try {
                        jsonResponse = Util
                                .readStream(response1.getEntity().getContent());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {

                    return "";
                }
                //text = sb.toString();
                //Log.i("myData response", "myData response:-" + jsonResponse);
                JSONObject jsonObject = new JSONObject(jsonResponse);
//                if (jsonObject.optString("mob_response").equals("AuthenticationError")) {
//                    jsonResponse = jsonObject.optString("mob_response");
//                    return jsonResponse;
//                }
                myData.setMinValue(jsonObject.optDouble("min_value"));
                myData.setMaxValue(jsonObject.optDouble("max_value"));
                myData.setAvgValue(jsonObject.optDouble("average_value"));
                myData.setUnit(jsonObject.optString("unit"));
                if (myData.getUnit().equals("C")) {
                    if (myData.getMinValue() != 0) {
                        double res = myData.getMinValue() - 32;
                        double res1 = (res * 5) / 9;
                        myData.setMinValue( Util.round(res1));
                    } else {

                        myData.setMinValue( Util.round(myData.getMinValue()));
                    }
                    if (myData.getMaxValue() != 0) {
                        double res = myData.getMaxValue() - 32;
                        double res1 = (res * 5) / 9;
                        myData.setMaxValue( Util.round(res1));
                    } else {

                        myData.setMaxValue(Util.round(myData.getMaxValue()));
                    }
                }
                int[] limitRange = calculateLimitRange((double) myData.getMaxValue(),
                        (double) jsonObject.optDouble("highest_value"),
                        (double) myData.getMinValue(),
                        (double) jsonObject.optDouble("lowest_value"));
                myData.setHighestValue(limitRange[0]);
                myData.setLowestValue(limitRange[1]);

                JSONArray readingsJsonArray = jsonObject.getJSONArray("readings");
                int size = readingsJsonArray.length();
                for (int i = 0; i < size; i++) {

                    JSONObject readingsObject = readingsJsonArray.getJSONObject(i);
                    ArrayList<DailyReadingValues> dailyReadingValueList = new ArrayList<DailyReadingValues>();
                    JSONArray dailyReadingsJsonArray = readingsObject.getJSONArray("day_time");

                    for (int j = 0; j < dailyReadingsJsonArray.length(); j++) {

                        DailyReadingValues dailyReadingValues = new DailyReadingValues();
                        JSONObject dailyReadingsObject = dailyReadingsJsonArray.getJSONObject(j);
                        dailyReadingValues.setTime(dailyReadingsObject.optString("time"));
                        dailyReadingValues.setValue(dailyReadingsObject.optDouble("value"));
                        dailyReadingValueList.add(dailyReadingValues);
                    }

                    String date = readingsObject.optString("date");
                    double dailyAvg = readingsObject.optDouble("avg_day_value");
                    int dataFlag = readingsObject.optInt("flag");
                    if (isFirstReading == false) {
                        firstReading = dailyAvg;
                        if (firstReading > 0) {
                            isFirstReading = true;
                        }
                    }

                    DateValues dateValues = new DateValues();
                    dateValues.setDateValue(date);
                    dateValues.setDailyAverage(dailyAvg);
                    dateValues.setDataFlag(dataFlag);
                    dateReadingList.add(dateValues);
                    weightReadigList.put(date, dailyReadingValueList);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }


            return jsonResponse;
        }
    }

    private class DownloadBloodPressureData extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
//            if (result.equalsIgnoreCase("AuthenticationError")) {
//                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
//                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
//
//                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
////                logoutClick();
//            } else {
                if (result.equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.noVitals), Toast.LENGTH_SHORT).show();
                } else {
                    if (modeFlag.equals("bar")) {

                        barChartButtonClick();
                    } else if (modeFlag.equals("table")) {

                        tableChartButtonClick();
                    } else {

                        lineChartButtonClick();
                    }
                }
//            }
        }

        @Override
        protected void onPreExecute() {
            try {
                progressDialog = new ProgressDialog(MyDataActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loadingMydata));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        protected String doInBackground(String... urls) {
            String jsonResponse = "";
            boolean isFirstReading = false;
            firstSysReading = 0;
            firstDiaReading = 0;
            firstPulseReading = 0;
            try {
                Log.e("DROID", "patient details url" + serviceUrl
                        + "/droid_website/rtd_get_tpm_blood_pressure.ashx");
                String VersionNumber = getResources().getString(
                        R.string.version);

                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/rtd_get_tpm_blood_pressure.ashx",
                        new String[]{"imei_no", "patient_id"}, new String[]{imeilNo, patientId});


                if (response1 != null) {
                    jsonResponse = Util
                            .readStream(response1.getEntity().getContent());

                } else {

                    return "";
                }
                //text = sb.toString();
                //Log.i("myData response", "myData response:-" + jsonResponse);
                JSONObject jsonObject = new JSONObject(jsonResponse);
//                if (jsonObject.optString("mob_response").equals("AuthenticationError")) {
//                    jsonResponse = jsonObject.optString("mob_response");
//                    return jsonResponse;
//                }
                myData.setSystolicMinValue(jsonObject.optDouble("systolic_min_value"));
                myData.setSystolicMaxValue(jsonObject.optDouble("systolic_max_value"));
                myData.setSystolicAvgValue(jsonObject.optDouble("avg_systolic"));

                myData.setDiastolicMinValue(jsonObject.optDouble("diastolic_min_value"));
                myData.setDiastolicMaxValue(jsonObject.optDouble("diastolic_max_value"));
                myData.setDiastolicAvgValue(jsonObject.optDouble("avg_diastolic"));

                myData.setPulseMinValue(jsonObject.optDouble("pulse_min_value"));
                myData.setPulseMaxValue(jsonObject.optDouble("pulse_max_value"));
                myData.setPulseAvgValue(jsonObject.optDouble("avg_pulse"));
                myData.setUnit(jsonObject.optString("unit"));
                double highest = 0;
                double highest_sys = (double) jsonObject.optDouble("highest_systolic");
                double highest_dia = (double) jsonObject.optDouble("highest_diastolic");
                double highest_pulse = (double) jsonObject.optDouble("highest_pulse");
                if (highest_sys >= highest_dia) {
                    highest = highest_sys;
                } else {

                    highest = highest_dia;
                }
                if (highest < highest_pulse) {

                    highest = highest_pulse;
                }
                
                double lowest = 0;
                double lowest_sys = (double) jsonObject.optDouble("lowest_systolic");
                double lowest_dia = (double) jsonObject.optDouble("lowest_diastolic");
                double lowest_pulse = (double) jsonObject.optDouble("lowest_pulse");
                if (lowest_sys <= lowest_dia) {
                    lowest = lowest_sys;
                } else {

                    lowest = lowest_dia;
                }
                if (lowest > lowest_pulse) {

                    lowest = lowest_pulse;
                }

                double max = 0;
                double max_sys = (double) jsonObject.optDouble("systolic_max_value");
                double max_dia = (double) jsonObject.optDouble("diastolic_max_value");
                double max_pulse = (double) jsonObject.optDouble("pulse_max_value");
                if (max_sys >= max_dia) {
                    max = max_sys;
                } else {

                    max = max_dia;
                }
                if (max < max_pulse) {

                    max = max_pulse;
                }

                int[] limitRange = calculateLimitRange(0,highest,0,lowest);
                myData.setHighestValue(limitRange[0]);
                myData.setLowestValue(limitRange[1]);

                JSONArray readingsJsonArray = jsonObject.getJSONArray("readings");
                for (int i = 0; i < readingsJsonArray.length(); i++) {

                    JSONObject readingsObject = readingsJsonArray.getJSONObject(i);
                    ArrayList<DailyReadingValues> dailyReadingValueList = new ArrayList<DailyReadingValues>();
                    JSONArray dailyReadingsJsonArray = readingsObject.getJSONArray("day_time");

                    for (int j = 0; j < dailyReadingsJsonArray.length(); j++) {

                        DailyReadingValues dailyReadingValues = new DailyReadingValues();
                        JSONObject dailyReadingsObject = dailyReadingsJsonArray.getJSONObject(j);
                        dailyReadingValues.setTime(dailyReadingsObject.optString("time"));
                        dailyReadingValues.setPulseValue(dailyReadingsObject.optInt("pulse"));
                        dailyReadingValues.setSystolicValue(dailyReadingsObject.optInt("systolic"));
                        dailyReadingValues.setDiastolicValue(dailyReadingsObject.optInt("diastolic"));
                        dailyReadingValueList.add(dailyReadingValues);
                    }

                    String date = readingsObject.optString("date");
                    double dailySystolicAvg = readingsObject.optDouble("avg_systolic");
                    double dailyDiastolicAvg = readingsObject.optDouble("avg_diastolic");
                    double dailyPulseAvg = readingsObject.optDouble("avg_pulse");
                    int dataFlag = readingsObject.optInt("flag");
                    if (isFirstReading == false) {
                        firstSysReading = dailySystolicAvg;
                        firstDiaReading = dailyDiastolicAvg;
                        firstPulseReading = dailyPulseAvg;
                        if (firstSysReading > 0) {
                            isFirstReading = true;
                        }
                    }
                    DateValues dateValues = new DateValues();
                    dateValues.setDateValue(date);
                    dateValues.setDailySystolicAverage(dailySystolicAvg);
                    dateValues.setDailyDiastolicAverage(dailyDiastolicAvg);
                    dateValues.setDailyPulseAverage(dailyPulseAvg);
                    dateValues.setDataFlag(dataFlag);
                    dateReadingList.add(dateValues);
                    weightReadigList.put(date, dailyReadingValueList);
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }

            return jsonResponse;
        }
    }

    private class DownloadBloodOxygenData extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
//            if (result.equalsIgnoreCase("AuthenticationError")) {
//                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
//                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
//
//                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
//                logoutClick();
//            } else {
                if (result.equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.noVitals), Toast.LENGTH_SHORT).show();
                } else {
                    if (modeFlag.equals("bar")) {

                        barChartButtonClick();
                    } else if (modeFlag.equals("table")) {

                        tableChartButtonClick();
                    } else {

                        lineChartButtonClick();
                    }
                }
 //           }
        }

        @Override
        protected void onPreExecute() {
            try {
                progressDialog = new ProgressDialog(MyDataActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loadingMydata));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        protected String doInBackground(String... urls) {
            String jsonResponse = "";
            boolean isFirstReading = false;
            firstReading = 0;
            try {
                Log.e("DROID", "patient details url" + serviceUrl
                        + "/droid_website/rtd_get_tpm_blood_oxygen.ashx");
                String VersionNumber = getResources().getString(
                        R.string.version);

                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/rtd_get_tpm_blood_oxygen.ashx",
                        new String[]{"imei_no", "patient_id"}, new String[]{imeilNo, patientId});


                if (response1 != null) {
                    jsonResponse = Util
                            .readStream(response1.getEntity().getContent());

                } else {

                    return "";
                }
                //text = sb.toString();
                //Log.i("myData response", "myData response:-" + jsonResponse);
                JSONObject jsonObject = new JSONObject(jsonResponse);
//                if (jsonObject.optString("mob_response").equals("AuthenticationError")) {
//                    jsonResponse = jsonObject.optString("mob_response");
//                    return jsonResponse;
//                }
                myData.setOxygenMinValue(jsonObject.optDouble("oxygen_min_value"));
                myData.setOxygenMaxValue(jsonObject.optDouble("oxygen_max_value"));
                myData.setOxygenAvgValue(jsonObject.optDouble("avg_oxygen"));

                myData.setPulseMinValue(jsonObject.optDouble("pulse_min_value"));
                myData.setPulseMaxValue(jsonObject.optDouble("pulse_max_value"));
                myData.setPulseAvgValue(jsonObject.optDouble("avg_pulse"));
                myData.setUnit(jsonObject.optString("unit"));
                double highest = 0;
                double highest_oxy = (double) jsonObject.optDouble("highest_oxygen");
                double highest_pulse = (double) jsonObject.optDouble("highest_pulse");
                if (highest_oxy >= highest_pulse) {

                    highest = highest_oxy;
                } else {

                    highest = highest_pulse;
                }
                double max = 0;
                double max_oxy = (double) jsonObject.optDouble("oxygen_max_value");
                double max_pulse = (double) jsonObject.optDouble("pulse_max_value");
                if (max_oxy >= max_pulse) {
                    max = max_oxy;
                } else {

                    max = max_pulse;
                }

                int[] limitRange = calculateLimitRange((double)jsonObject.optDouble("oxygen_max_value"),
                        (double) jsonObject.optDouble("highest_oxygen"),
                        (double) jsonObject.optDouble("oxygen_min_value"),
                        (double) jsonObject.optDouble("lowest_oxygen"));
                myData.setHighestValue(limitRange[0]);
                myData.setLowestValue(limitRange[1]);

                JSONArray readingsJsonArray = jsonObject.getJSONArray("readings");
                for (int i = 0; i < readingsJsonArray.length(); i++) {

                    JSONObject readingsObject = readingsJsonArray.getJSONObject(i);
                    ArrayList<DailyReadingValues> dailyReadingValueList = new ArrayList<DailyReadingValues>();
                    JSONArray dailyReadingsJsonArray = readingsObject.getJSONArray("day_time");

                    for (int j = 0; j < dailyReadingsJsonArray.length(); j++) {

                        DailyReadingValues dailyReadingValues = new DailyReadingValues();
                        JSONObject dailyReadingsObject = dailyReadingsJsonArray.getJSONObject(j);
                        dailyReadingValues.setTime(dailyReadingsObject.optString("time"));
                        dailyReadingValues.setPulseValue(dailyReadingsObject.optInt("pulse"));
                        dailyReadingValues.setOxygenValue(dailyReadingsObject.optDouble("oxygen"));

                        dailyReadingValueList.add(dailyReadingValues);
                    }

                    String date = readingsObject.optString("date");
                    double dailyOxygenAvg = readingsObject.optDouble("avg_oxygen_day");
                    double dailyPulseAvg = readingsObject.optDouble("avg_pulse_day");
                    int dataFlag = readingsObject.optInt("flag");
                    if (isFirstReading == false) {
                        firstReading = dailyOxygenAvg;
                        if (firstReading > 0) {
                            isFirstReading = true;
                        }
                    }
                    DateValues dateValues = new DateValues();
                    dateValues.setDateValue(date);
                    dateValues.setDailyOxygenAverage(dailyOxygenAvg);
                    dateValues.setDailyPulseAverage(dailyPulseAvg);
                    dateValues.setDataFlag(dataFlag);
                    dateReadingList.add(dateValues);
                    weightReadigList.put(date, dailyReadingValueList);
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }

            return jsonResponse;
        }
    }


    private class DownloadBloodGlucoseData extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
//            if (result.equalsIgnoreCase("AuthenticationError")) {
//                SharedPreferences ERROR_MSG = getSharedPreferences("ERROR_MSG", 0);
//                String msg_authentication_error = ERROR_MSG.getString("msg_authentication_error_1", "-1");
//
//                Toast.makeText(getApplicationContext(), msg_authentication_error, Toast.LENGTH_LONG).show();
//                logoutClick();
//            } else {

                if (result.equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.noVitals), Toast.LENGTH_SHORT).show();
                } else {
                    if (dateReadingList.size() > 0) {
                        allFlag = 0;
                        glucoseAllClick();
                        if (modeFlag.equals("bar")) {

                            barChartButtonClick();
                        } else if (modeFlag.equals("table")) {

                            tableChartButtonClick();
                        } else {

                            lineChartButtonClick();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.noVitals), Toast.LENGTH_SHORT).show();

                    }
                }
 //           }

        }

        @Override
        protected void onPreExecute() {

            glucoseDateReadingList = new ArrayList<DateValues>();
            glucoseReadingList = new HashMap<String, ArrayList<DailyReadingValues>>();
            try {
                progressDialog = new ProgressDialog(MyDataActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loadingMydata));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Log.i("Exception", "Exception in ProgressDialog.show");
            }
        }

        protected String doInBackground(String... urls) {
            String jsonResponse = "";
            boolean isFirstReading = false;
            firstReading = 0;
            try {
                Log.e("DROID", "patient details url" + serviceUrl
                        + "/droid_website/rtd_get_tpm_blood_glucose.ashx");
                String VersionNumber = getResources().getString(
                        R.string.version);

                HttpResponse response1 = Util.connect(serviceUrl
                                + "/droid_website/rtd_get_tpm_blood_glucose.ashx",
                        new String[]{"imei_no", "patient_id"}, new String[]{imeilNo, patientId});


                if (response1 != null) {
                    jsonResponse = Util
                            .readStream(response1.getEntity().getContent());

                } else {

                    return "";
                }
                //text = sb.toString();
                // Log.i("myData response", "myData response:-" + jsonResponse);
                JSONObject jsonObject = new JSONObject(jsonResponse);
//                if (jsonObject.optString("mob_response").equals("AuthenticationError")) {
//                    jsonResponse = jsonObject.optString("mob_response");
//                    return jsonResponse;
//                }
                myData.setMinValue(jsonObject.optDouble("min_value"));
                myData.setMaxValue(jsonObject.optDouble("max_value"));
                myData.setAvgValue(jsonObject.optDouble("average_value"));
                myData.setUnit(jsonObject.optString("unit"));
                int[] limitRange = calculateLimitRange((double) jsonObject.optDouble("max_value"),
                        (double) jsonObject.optDouble("highest_value"),
                        (double) jsonObject.optDouble("min_value"),
                        (double) jsonObject.optDouble("lowest_value"));
                myData.setHighestValue(limitRange[0]);
                myData.setLowestValue(limitRange[1]);

                JSONArray readingsJsonArray = jsonObject.getJSONArray("readings");
                for (int i = 0; i < readingsJsonArray.length(); i++) {

                    JSONObject readingsObject = readingsJsonArray.getJSONObject(i);
                    ArrayList<DailyReadingValues> dailyReadingValueList = new ArrayList<DailyReadingValues>();
                    JSONArray dailyReadingsJsonArray = readingsObject.getJSONArray("day_time");

                    for (int j = 0; j < dailyReadingsJsonArray.length(); j++) {

                        DailyReadingValues dailyReadingValues = new DailyReadingValues();
                        JSONObject dailyReadingsObject = dailyReadingsJsonArray.getJSONObject(j);
                        dailyReadingValues.setTime(dailyReadingsObject.optString("time"));
                        dailyReadingValues.setValue(dailyReadingsObject.optDouble("value"));
                        dailyReadingValues.setMeal(dailyReadingsObject.optString("meal"));
                        dailyReadingValueList.add(dailyReadingValues);
                    }

                    String date = readingsObject.optString("date");
                    double dailyAvg = readingsObject.optDouble("avg_day_value");
                    int dataFlag = readingsObject.optInt("flag");
                    if (isFirstReading == false) {
                        firstReading = dailyAvg;
                        if (firstReading > 0) {
                            isFirstReading = true;
                        }
                    }
                    DateValues dateValues = new DateValues();
                    dateValues.setDateValue(date);
                    dateValues.setDailyAverage(dailyAvg);
                    dateValues.setDataFlag(dataFlag);
                    dateReadingList.add(dateValues);
                    weightReadigList.put(date, dailyReadingValueList);
                    glucoseDateReadingList.add(dateValues);
                    glucoseReadingList.put(date, dailyReadingValueList);
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }

            return jsonResponse;
        }
    }

    private void ClearList() {
        if (dateReadingList != null) {
            dateReadingList.clear();
            weightReadigList.clear();
            lineChart.clear();
            barChart.clear();

        }

        myData = new MyData();
        dateReadingList = new ArrayList<DateValues>();
        weightReadigList = new HashMap<String, ArrayList<DailyReadingValues>>();
    }

    private void showLineChart() {
    	lineChart.clear();
        List<Entry> entries = new ArrayList<Entry>();
        ArrayList<String> labels = new ArrayList<String>();
        colors = new ArrayList<Integer>();
        colorsText = new ArrayList<Integer>();
        // final HashMap<Integer, String> numMap = new HashMap<>();
        //for (int i = 0; i < dateReadingList.size(); i++) {
        for (int i = 0; i < dateReadingList.size(); i++) {
            DateValues dateValues = dateReadingList.get(i);
            double yValue = dateValues.getDailyAverage();
            if (yValue == 0) {
                yValue = firstReading;
            }
            entries.add(new Entry((float) yValue, i,dateValues.getDataFlag()));
            //numMap.put(i,parseDate(dateValues.getDateValue()));
            labels.add(parseDate(dateValues.getDateValue()));
            int value = dateValues.getDataFlag();
            colors.add(value == 1 ? ColorTemplate.rgb("#e87722") : Color.TRANSPARENT);
            colorsText.add(value == 1 ? ColorTemplate.rgb("#000000") : Color.TRANSPARENT);

        }
        final LineDataSet dataSet = new LineDataSet(entries, vitalClickFlag);

        dataSet.setColor(Color.parseColor("#e87722"));
        //dataSet.setCircleColor(Color.parseColor("#e87722"));
        dataSet.setCircleColors(colors);

        dataSet.setValueTextColors(colorsText);
        dataSet.setDrawCircles(true);
        dataSet.setDrawCircleHole(false);
        dataSet.setDrawValues(false);
        dataSet.setValueFormatter(new MyValueFormatter());
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setDrawHighlightIndicators(false);

        dataSet.setLineWidth(3);
        dataSet.setCircleRadius(10);
        dataSet.setValueTextSize(10);
        LineData data = new LineData(labels, dataSet);


        lineChartSettings();
        lineChart.setData(data);
//        if (!getResources().getBoolean(R.bool.isTab)) {
//            lineChart.setVisibleXRangeMaximum(6);
//        }
        lineChart.setVisibleYRangeMaximum(myData.getHighestValue() + 50, YAxis.AxisDependency.LEFT);

        lineChart.invalidate();


    }

    private void showBarChart() {
        colors = new ArrayList<Integer>();
        colorsText = new ArrayList<Integer>();
        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();
        ArrayList<String> labels = new ArrayList<String>();
        // final HashMap<Integer, String> numMap = new HashMap<>();
        for (int i = 0; i < dateReadingList.size(); i++) {

            DateValues dateValues = dateReadingList.get(i);
            int value = dateValues.getDataFlag();
            double yValue = dateValues.getDailyAverage();
            if (value == 0) {
                yValue = 0;
            }
            entries.add(new BarEntry((float) yValue, i));
            //numMap.put(i,parseDate(dateValues.getDateValue()));
            labels.add(parseDate(dateValues.getDateValue()));
            colors.add(value == 1 ? ColorTemplate.rgb("#e87722") : Color.TRANSPARENT);
            colorsText.add(value == 1 ? ColorTemplate.rgb("#000000") : Color.TRANSPARENT);
        }
        BarDataSet dataset = new BarDataSet(entries, vitalClickFlag);
        //dataset.setColor(Color.parseColor("#e87722"));
        dataset.setColors(colors);
        dataset.setValueTextColors(colorsText);
        dataset.setDrawValues(false);
        dataset.setValueFormatter(new MyValueFormatter());
        dataset.setValueTextSize(10);
        dataset.setBarSpacePercent(30f);
        dataset.setHighLightColor(Color.YELLOW);
        BarData data = new BarData(labels, dataset);
//        List<Integer> c = new ArrayList<>();
//        c.add(ColorTemplate.rgb("#e87722"));
//        List<String> s = new ArrayList<>();
//        s.add(vitalClickFlag);
//        Legend legend = barChart.getLegend();
//        legend.setCustom(c, s);
//        legend.setEnabled(false);
        barChartSettings();
        barChart.setData(data);

//        if (!getResources().getBoolean(R.bool.isTab)) {
//            barChart.setVisibleXRangeMaximum(7);
//        }
        // barChart.getLegend().setWordWrapEnabled(true);

        barChart.invalidate();

    }


    private void showBloodPressureLineChart() {

        ArrayList<Entry> entriesSys = new ArrayList<Entry>();
        ArrayList<Entry> entriesDias = new ArrayList<Entry>();
        ArrayList<Entry> entriesPulse = new ArrayList<Entry>();
        ArrayList<String> labels = new ArrayList<String>();
        List<Integer> colorsSys = new ArrayList<Integer>();
        List<Integer> colorsDia = new ArrayList<Integer>();
        List<Integer> colorsPulse = new ArrayList<Integer>();
        colorsText = new ArrayList<Integer>();
        //final HashMap<Integer, String> numMap = new HashMap<>();
        for (int i = 0; i < dateReadingList.size(); i++) {

            DateValues dateValues = dateReadingList.get(i);
            int value = dateValues.getDataFlag();
            double ySysValue = dateValues.getDailySystolicAverage();
            double yDiaValue = dateValues.getDailyDiastolicAverage();
            double yPulseValue = dateValues.getDailyPulseAverage();
            if (ySysValue == 0) {
                ySysValue = firstSysReading;
            }
            if (yDiaValue == 0) {
                yDiaValue = firstDiaReading;
            }
            if (yPulseValue == 0) {
                yPulseValue = firstPulseReading;
            }

            entriesSys.add(new Entry((float) ySysValue, i,dateValues.getDataFlag()));
            entriesDias.add(new Entry((float) yDiaValue, i,dateValues.getDataFlag()));
            entriesPulse.add(new Entry((float) yPulseValue, i,dateValues.getDataFlag()));
            //numMap.put(i,parseDate(dateValues.getDateValue()));
            labels.add(parseDate(dateValues.getDateValue()));
            colorsSys.add(value == 1 ? ColorTemplate.rgb("#e87722") : Color.TRANSPARENT);
            colorsDia.add(value == 1 ? ColorTemplate.rgb("#ad520a") : Color.TRANSPARENT);
            colorsPulse.add(value == 1 ? ColorTemplate.rgb("#4a7768") : Color.TRANSPARENT);
            colorsText.add(value == 1 ? ColorTemplate.rgb("#000000") : Color.TRANSPARENT);
        }

        final LineDataSet dataSetSys = new LineDataSet(entriesSys, getResources().getString(R.string.systolic1));
        dataSetSys.setDrawValues(false);
        dataSetSys.setValueFormatter(new MyValueFormatter());
        dataSetSys.setColor(Color.parseColor("#e87722"));
        // dataSetSys.setCircleColor(Color.parseColor("#e87722"));
        dataSetSys.setCircleColors(colorsSys);
        dataSetSys.setValueTextColors(colorsText);
        dataSetSys.setDrawCircles(true);
        dataSetSys.setDrawCircleHole(false);
        dataSetSys.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSetSys.setDrawHighlightIndicators(false);
        dataSetSys.setLineWidth(3);
        dataSetSys.setCircleRadius(10);
        dataSetSys.setValueTextSize(10);


        final LineDataSet dataSetDia = new LineDataSet(entriesDias, getResources().getString(R.string.diastolic1));
        dataSetDia.setDrawValues(false);
        dataSetDia.setValueFormatter(new MyValueFormatter());
        dataSetDia.setColor(Color.parseColor("#ad520a"));
        //dataSetDia.setCircleColor(Color.parseColor("#ad520a"));
        dataSetDia.setCircleColors(colorsDia);
        dataSetDia.setValueTextColors(colorsText);
        dataSetDia.setDrawCircles(true);
        dataSetDia.setDrawCircleHole(false);
        dataSetDia.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSetDia.setDrawHighlightIndicators(false);
        dataSetDia.setLineWidth(3);
        dataSetDia.setCircleRadius(10);
        dataSetDia.setValueTextSize(10);


        final LineDataSet dataSetPulse = new LineDataSet(entriesPulse, getResources().getString(R.string.heartRate1));
        dataSetPulse.setDrawValues(false);
        dataSetPulse.setValueFormatter(new MyValueFormatter());
        dataSetPulse.setColor(Color.parseColor("#4a7768"));
        //dataSetPulse.setCircleColor(Color.parseColor("#4a7768"));
        dataSetPulse.setCircleColors(colorsPulse);
        dataSetPulse.setValueTextColors(colorsText);
        dataSetPulse.setDrawCircles(true);
        dataSetPulse.setDrawCircleHole(false);
        dataSetPulse.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSetPulse.setDrawHighlightIndicators(false);
        dataSetPulse.setLineWidth(3);
        dataSetPulse.setCircleRadius(10);
        dataSetPulse.setValueTextSize(10);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSetSys);
        dataSets.add(dataSetDia);
        dataSets.add(dataSetPulse);

        LineData data = new LineData(labels, dataSets);


        lineChartSettings();

        lineChart.setData(data);
//        if (!getResources().getBoolean(R.bool.isTab)) {
//            lineChart.setVisibleXRangeMaximum(6);
//        }
        //lineChart.setVisibleYRangeMaximum(myData.getHighestValue() + 50, YAxis.AxisDependency.LEFT);
        lineChart.invalidate();

    }

    private void showBloodPressureBarChart() {
        //colors = new ArrayList<>();
        colorsText = new ArrayList<Integer>();
        ArrayList<BarEntry> entriesSys = new ArrayList<BarEntry>();
        ArrayList<BarEntry> entriesDias = new ArrayList<BarEntry>();
        ArrayList<BarEntry> entriesPulse = new ArrayList<BarEntry>();
        ArrayList<String> labels = new ArrayList<String>();
        //final HashMap<Integer, String> numMap = new HashMap<>();
        List<Integer> colorsSys = new ArrayList<Integer>();
        List<Integer> colorsDia = new ArrayList<Integer>();
        List<Integer> colorsPulse = new ArrayList<Integer>();


        for (int i = 0; i < dateReadingList.size(); i++) {

            DateValues dateValues = dateReadingList.get(i);
            int value = dateValues.getDataFlag();
            double ySysValue = dateValues.getDailySystolicAverage();
            double yDiaValue = dateValues.getDailyDiastolicAverage();
            double yPulseValue = dateValues.getDailyPulseAverage();
            if (value == 0) {
                ySysValue = 0;
                yDiaValue = 0;
                yPulseValue = 0;
            }
//            float dif = (float) dateValues.getDailySystolicAverage() - (float) dateValues.getDailyDiastolicAverage();
//            entriesSys.add(new BarEntry(new float[]{(float) dateValues.getDailyDiastolicAverage(),
//                    dif}, i));
            entriesSys.add(new BarEntry((float) ySysValue, i));
            entriesDias.add(new BarEntry((float) yDiaValue, i));
            entriesPulse.add(new BarEntry((float) yPulseValue, i));
            labels.add(parseDate(dateValues.getDateValue()));
            //numMap.put(i,parseDate(dateValues.getDateValue()));
            colorsSys.add(value == 1 ? ColorTemplate.rgb("#e87722") : Color.TRANSPARENT);
            colorsDia.add(value == 1 ? ColorTemplate.rgb("#ad520a") : Color.TRANSPARENT);
            colorsPulse.add(value == 1 ? ColorTemplate.rgb("#4a7768") : Color.TRANSPARENT);
            colorsText.add(value == 1 ? ColorTemplate.rgb("#000000") : Color.TRANSPARENT);

        }
        final BarDataSet dataSetSys = new BarDataSet(entriesSys, getResources().getString(R.string.systolic1));
        dataSetSys.setDrawValues(false);
        dataSetSys.setValueFormatter(new MyValueFormatter());
        dataSetSys.setValueTextSize(10);
        dataSetSys.setHighLightColor(Color.YELLOW);
        dataSetSys.setBarSpacePercent(5f);
        //dataSetSys.setColor(Color.parseColor("#e87722"));
        dataSetSys.setColors(colorsSys);
        dataSetSys.setValueTextColors(colorsText);

        final BarDataSet dataSetDia = new BarDataSet(entriesDias, getResources().getString(R.string.diastolic1));
        dataSetDia.setDrawValues(false);
        dataSetDia.setValueFormatter(new MyValueFormatter());
        dataSetDia.setValueTextSize(10);
        dataSetDia.setBarSpacePercent(5f);
        dataSetDia.setHighLightColor(Color.YELLOW);
        //dataSetDia.setColor(Color.parseColor("#ad520a"));
        dataSetDia.setColors(colorsDia);
        dataSetDia.setValueTextColors(colorsText);

        final BarDataSet dataSetPulse = new BarDataSet(entriesPulse, getResources().getString(R.string.heartRate1));
        dataSetPulse.setDrawValues(false);
        dataSetPulse.setValueFormatter(new MyValueFormatter());
        dataSetPulse.setValueTextSize(10);
        dataSetPulse.setBarSpacePercent(5f);
        dataSetPulse.setHighLightColor(Color.YELLOW);
        //dataSetPulse.setColor(Color.parseColor("#4a7768"));
        dataSetPulse.setColors(colorsPulse);
        dataSetPulse.setValueTextColors(colorsText);

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(dataSetSys);
        dataSets.add(dataSetDia);
        dataSets.add(dataSetPulse);

        BarData data = new BarData(labels, dataSets);

        barChartSettings();
        barChart.setData(data);

//        if (!getResources().getBoolean(R.bool.isTab)) {
//            barChart.setVisibleXRangeMaximum(26);
//        }
        //barChart.setVisibleYRangeMaximum(myData.getHighestValue() + 50, YAxis.AxisDependency.LEFT);
        barChart.invalidate();

    }


    private void showBloodOxygenLineChart() {
        colors = new ArrayList<Integer>();
        colorsText = new ArrayList<Integer>();
        ArrayList<Entry> entriesOxygen = new ArrayList<Entry>();
        //ArrayList<Entry> entriesPulse = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        // final HashMap<Integer, String> numMap = new HashMap<>();
        // List<Integer> colorsOxy = new ArrayList<>();
        // List<Integer> colorsPulse = new ArrayList<>();
        for (int i = 0; i < dateReadingList.size(); i++) {
            DateValues dateValues = dateReadingList.get(i);
            double yValue = dateValues.getDailyOxygenAverage();
            if (yValue == 0) {
                yValue = firstReading;
            }
            entriesOxygen.add(new Entry((float) yValue, i,dateValues.getDataFlag()));
            //entriesPulse.add(new Entry((float) dateValues.getDailyPulseAverage(), i));
            labels.add(parseDate(dateValues.getDateValue()));
            //numMap.put(i,parseDate(dateValues.getDateValue()));
            int value = dateValues.getDataFlag();
            colors.add(value == 1 ? ColorTemplate.rgb("#e87722") : Color.TRANSPARENT);
            colorsText.add(value == 1 ? ColorTemplate.rgb("#000000") : Color.TRANSPARENT);
        }

        final LineDataSet dataSetOxygen = new LineDataSet(entriesOxygen, "Oxygen");
        dataSetOxygen.setDrawValues(false);
        dataSetOxygen.setValueFormatter(new MyValueFormatter());
        dataSetOxygen.setColor(Color.parseColor("#e87722"));
        //dataSetOxygen.setCircleColor(Color.parseColor("#e87722"));
        dataSetOxygen.setCircleColors(colors);
        dataSetOxygen.setValueTextColors(colorsText);
        dataSetOxygen.setDrawCircles(true);
        dataSetOxygen.setDrawCircleHole(false);
        dataSetOxygen.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSetOxygen.setDrawHighlightIndicators(false);
        //dataSetOxygen.setCircleColors(colorsOxy);
        dataSetOxygen.setLineWidth(3);
        dataSetOxygen.setCircleRadius(10);
        dataSetOxygen.setValueTextSize(10);

//        final LineDataSet dataSetPulse = new LineDataSet(entriesPulse, "Pulse");
//        dataSetPulse.setDrawValues(false);
//        dataSetPulse.setColor(Color.parseColor("#1a62ba"));
//        dataSetPulse.setCircleColor(Color.parseColor("#1a62ba"));
//        dataSetPulse.setDrawCircles(true);
//        dataSetPulse.setDrawCircleHole(false);
//        dataSetPulse.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        dataSetPulse.setDrawHighlightIndicators(false);
//        //dataSetPulse.setCircleColors(colorsPulse);
//        dataSetPulse.setLineWidth(3);
//        dataSetPulse.setCircleRadius(6);
//        dataSetPulse.setValueTextSize(14);

//        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
//        dataSets.add(dataSetOxygen);
        //dataSets.add(dataSetPulse);

        LineData data = new LineData(labels, dataSetOxygen);
        lineChartSettings();
        lineChart.setData(data);
//        if (!getResources().getBoolean(R.bool.isTab)) {
//            lineChart.setVisibleXRangeMaximum(6);
//        }
        lineChart.setVisibleYRangeMaximum(myData.getHighestValue() + 50, YAxis.AxisDependency.LEFT);
        lineChart.invalidate();
    }

    private void showBloodOxygenBarChart() {
        colors = new ArrayList<Integer>();
        colorsText = new ArrayList<Integer>();
        ArrayList<BarEntry> entriesOxygen = new ArrayList<BarEntry>();
        // ArrayList<BarEntry> entriesPulse = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();
        //final HashMap<Integer, String> numMap = new HashMap<>();

        for (int i = 0; i < dateReadingList.size(); i++) {
            DateValues dateValues = dateReadingList.get(i);
            int value = dateValues.getDataFlag();
            double yValue = dateValues.getDailyOxygenAverage();
            if (value == 0) {
                yValue = 0;
            }
            entriesOxygen.add(new BarEntry((float) yValue, i));
            // entriesPulse.add(new BarEntry((float) dateValues.getDailyPulseAverage(), i));
            labels.add(parseDate(dateValues.getDateValue()));
            // numMap.put(i,parseDate(dateValues.getDateValue()));
            colors.add(value == 1 ? ColorTemplate.rgb("#e87722") : Color.TRANSPARENT);
            colorsText.add(value == 1 ? ColorTemplate.rgb("#000000") : Color.TRANSPARENT);
        }
        final BarDataSet dataSetOxygen = new BarDataSet(entriesOxygen, "Oxygen");
        dataSetOxygen.setDrawValues(false);
        dataSetOxygen.setValueFormatter(new MyValueFormatter());
        dataSetOxygen.setValueTextSize(10);
        //dataSetOxygen.setColor(Color.parseColor("#e87722"));
        dataSetOxygen.setColors(colors);
        dataSetOxygen.setValueTextColors(colorsText);
        dataSetOxygen.setBarSpacePercent(30f);
        dataSetOxygen.setHighLightColor(Color.YELLOW);
//        final BarDataSet dataSetPulse = new BarDataSet(entriesPulse, "Pulse");
//        dataSetPulse.setDrawValues(false);
//        dataSetPulse.setColor(Color.parseColor("#1a62ba"));

//        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
//        dataSets.add(dataSetOxygen);
        // dataSets.add(dataSetPulse);
        BarData data = new BarData(labels, dataSetOxygen);
        barChartSettings();
        barChart.setData(data);
//        if (!getResources().getBoolean(R.bool.isTab)) {
//            barChart.setVisibleXRangeMaximum(7);
//        }
        barChart.invalidate();
    }


    private void lineChartSettings() {
//        lineChart.setPinchZoom(false);
//        lineChart.setDoubleTapToZoomEnabled(false);
//        lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
//            @Override
//            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
//
//                DateValues dateValues = dateReadingList.get(e.getXIndex());
//                int value = dateValues.getDataFlag();
//                if (value == 1) {
//                    globalClass.appTracking(pageName, "Clicking on a dot in the line graph");
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected() {
//
//            }
//        });

        lineChart.setScaleEnabled(false);
        lineChart.setScaleYEnabled(false);
        lineChart.setDescription("");
        lineChart.setExtraRightOffset(20f);
        //lineChart.setExtraTopOffset(30f);
        Legend legend = lineChart.getLegend();
        if (vitalClickFlag.equals("pressure")) {

        	legend.setEnabled(true);
        }else{
        	
        	legend.setEnabled(false);
        }
        
        mv = new MyMarkerView(this,layout_mydata, R.layout.marker,
                dateReadingList, weightReadigList, vitalClickFlag, lineChart.getHeight(), "line");
        //mv.removeAllViews();
        lineChart.setMarkerView(mv);

        YAxis yAxis = lineChart.getAxisLeft();
        yAxis.setAxisMaxValue((float) myData.getHighestValue());
        yAxis.setAxisMinValue((float) myData.getLowestValue()); 
        //yAxis.setDrawLabels(false);
        yAxis.setLabelCount(1, true);
        yAxis.setDrawGridLines(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setZeroLineColor(Color.parseColor("#000000"));
        yAxis.setGridColor(Color.parseColor("#45a4d2"));
        yAxis.setGridLineWidth(4f);
        yAxis.enableGridDashedLine(9, 9, 4);
        yAxis.setDrawZeroLine(false);
        yAxis.removeAllLimitLines();
        //yAxis.setValueFormatter(new MyYAxisValueFormatter());
        yAxis.setTextColor(Color.parseColor("#000000"));
        if (vitalClickFlag.equals("pressure")) {

            pressureLimitLine(yAxis);
        } else if (vitalClickFlag.equals("oxygen")) {

            oxygenLimitLine(yAxis);
        } else if (vitalClickFlag.equals("weight")) {

            weightLimitLine(yAxis);
        } else {

            commonLimitLine(yAxis);
        }
        YAxis yAxisRight = lineChart.getAxisRight();
        yAxisRight.setEnabled(false);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setSpaceBetweenLabels(1);
        xAxis.setLabelsToSkip(0);
        xAxis.setAxisMinValue(-1.5f);
        xAxis.setTextColor(Color.parseColor("#000000"));
    }

    private void barChartSettings() {
//        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
//            @Override
//            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
//                DateValues dateValues = dateReadingList.get(e.getXIndex());
//                int value = dateValues.getDataFlag();
//                if (value == 1) {
//                    globalClass.appTracking(pageName, "Clicking on a bar in the bar graph");
//                }
//            }
//
//            @Override
//            public void onNothingSelected() {
//
//            }
//        });
//        barChart.setPinchZoom(false);
//        barChart.setDoubleTapToZoomEnabled(false);
        barChart.setScaleEnabled(false);
        //lineChart.setScaleYEnabled(false);
        barChart.setDescription("");
        barChart.setExtraRightOffset(20f);
        Legend legend = barChart.getLegend();  
        
        if (vitalClickFlag.equals("pressure")) {

        	 ArrayList<String> labelsLegend = new ArrayList<String>();
        	 labelsLegend.add(getResources().getString(R.string.systolic1));
        	 labelsLegend.add(getResources().getString(R.string.diastolic1));
        	 labelsLegend.add(getResources().getString(R.string.heartRate1));
             //final HashMap<Integer, String> numMap = new HashMap<>();
             List<Integer> colorsSLegend = new ArrayList<Integer>();
             colorsSLegend.add(ColorTemplate.rgb("#e87722"));
             colorsSLegend.add(ColorTemplate.rgb("#ad520a"));
             colorsSLegend.add(ColorTemplate.rgb("#4a7768"));
             legend.setCustom(colorsSLegend, labelsLegend);
        	legend.setEnabled(true);
        }else{
        	
        	legend.setEnabled(false);
        }
        MyMarkerView mv = new MyMarkerView(this,layout_mydata, R.layout.marker, dateReadingList, weightReadigList, vitalClickFlag, barChart.getHeight(), "bar");
        barChart.setMarkerView(mv);
        YAxis yAxis = barChart.getAxisLeft();
        yAxis.setAxisMaxValue(myData.getHighestValue());
        yAxis.setAxisMinValue((float) myData.getLowestValue());
        //yAxis.setDrawLabels(false);
        yAxis.setLabelCount(1, true);
        yAxis.setDrawGridLines(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawZeroLine(false);
        yAxis.setGridColor(Color.parseColor("#45a4d2"));
        yAxis.removeAllLimitLines();
        if (vitalClickFlag.equals("pressure")) {

            pressureLimitLine(yAxis);
        } else if (vitalClickFlag.equals("oxygen")) {

            oxygenLimitLine(yAxis);
        } else if (vitalClickFlag.equals("weight")) {

            weightLimitLine(yAxis);
        } else {

            commonLimitLine(yAxis);
        }
        YAxis yAxisRight = barChart.getAxisRight();
        yAxisRight.setEnabled(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setSpaceBetweenLabels(1);
        xAxis.setLabelsToSkip(0);
        xAxis.setAxisMinValue(-1.5f);
        // xAxis.setAxisMaxValue(7f);
    }

    public String parseDate(String time) {
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "EEE dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void commonLimitLine(YAxis yAxis) {
        if (!((myData.getMinValue() == 0) && (myData.getMaxValue() == 0))) {

            LimitLine min = new LimitLine((float) Util.round(myData.getMinValue()), "");
            min.setLineColor(Color.parseColor("#6699cc"));
            min.setTextColor(Color.parseColor("#6699cc"));
            limitLineSettings(min);
            min.setLabel(getResources().getString(R.string.minCap) + " " + Util.round(myData.getMinValue()));
            LimitLine max = new LimitLine((float) Util.round(myData.getMaxValue()), "");
            limitLineSettings(max);
            max.setLineColor(Color.parseColor("#6699cc"));
            max.setTextColor(Color.parseColor("#6699cc"));
            max.setLabel(getResources().getString(R.string.maxCap) + " " + (float) Util.round(myData.getMaxValue()));
            yAxis.addLimitLine(min);
            yAxis.addLimitLine(max);
        }

//        LimitLine average = new LimitLine((float) Util.round(myData.getAvgValue()), "");
//        limitLineSettings(average);
//        average.setLineColor(Color.parseColor("#c15805"));
//        average.setTextColor(Color.parseColor("#000000"));
//
//        average.setLabel(getResources().getString(R.string.avg) + "\n" + (float) Util.round(myData.getAvgValue()));


        LimitLine highest = new LimitLine(myData.getHighestValue(), "");
        highest.setLineWidth(2f);
        highest.enableDashedLine(20f, 20f, 0f);
        highest.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        highest.setTextSize(10f);
        highest.setLineColor(Color.parseColor("#b2b2b2"));
        highest.setLabel("");
//        yAxis.addLimitLine(average);
        yAxis.addLimitLine(highest);
        LimitLine lowest = new LimitLine((float) myData.getLowestValue(), "");
        lowest.setLineWidth(2f);
        lowest.enableDashedLine(20f, 20f, 0f);
        lowest.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        lowest.setTextSize(10f);
        lowest.setLineColor(Color.parseColor("#b2b2b2"));
        //lowest.setLabel("  0");
        yAxis.addLimitLine(lowest);

    }

    private void weightLimitLine(YAxis yAxis) {
        if (!((myData.getMinValue() == 0) && (myData.getMaxValue() == 0))) {
            double minValue;
            double maxValue;          
                maxValue = Util.round(myData.getMaxValue());
                minValue = Util.round(myData.getMinValue());
           
            LimitLine min = new LimitLine((float) minValue, "");
            min.setLineColor(Color.parseColor("#6699cc"));
            min.setTextColor(Color.parseColor("#6699cc"));
            limitLineSettings(min);
            min.setLabel(getResources().getString(R.string.minCap) + " " + minValue);
            LimitLine max = new LimitLine((float) maxValue, "");
            limitLineSettings(max);
            max.setLineColor(Color.parseColor("#6699cc"));
            max.setTextColor(Color.parseColor("#6699cc"));
            max.setLabel(getResources().getString(R.string.maxCap) + " " + (float) maxValue);
            yAxis.addLimitLine(min);
            yAxis.addLimitLine(max);
        }

//        LimitLine average = new LimitLine((float) Util.round(myData.getAvgValue()), "");
//        limitLineSettings(average);
//        average.setLineColor(Color.parseColor("#c15805"));
//        average.setTextColor(Color.parseColor("#000000"));
//
//        average.setLabel(getResources().getString(R.string.avg) + "\n" + (float) Util.round(myData.getAvgValue()));


        LimitLine highest = new LimitLine(myData.getHighestValue(), "");
        highest.setLineWidth(2f);
        highest.enableDashedLine(20f, 20f, 0f);
        highest.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        highest.setTextSize(10f);
        highest.setLineColor(Color.parseColor("#b2b2b2"));
        highest.setLabel("");
        yAxis.addLimitLine(highest);
       
        LimitLine lowest = new LimitLine((float) myData.getLowestValue(), "");
        lowest.setLineWidth(2f);
        lowest.enableDashedLine(20f, 20f, 0f);
        lowest.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        lowest.setTextSize(10f);
        lowest.setLineColor(Color.parseColor("#b2b2b2"));
        //lowest.setLabel("  0");
        yAxis.addLimitLine(lowest);
//        yAxis.addLimitLine(average);
        
        

    }

    private void pressureLimitLine(YAxis yAxis) {
//        LimitLine sysMin = new LimitLine((float) Util.round(myData.getSystolicMinValue()), "");
//        limitLineSettings(sysMin);
//        sysMin.setLineColor(Color.parseColor("#e87722"));
//        sysMin.setTextColor(Color.parseColor("#000000"));
//        sysMin.setLabel("Min " + Util.round(myData.getSystolicMinValue()));
//        LimitLine sysMax = new LimitLine((float) Util.round(myData.getSystolicMaxValue()), "");
//        limitLineSettings(sysMax);
//        sysMax.setLineColor(Color.parseColor("#e87722"));
//        sysMax.setTextColor(Color.parseColor("#000000"));
//        sysMax.setLabel("Max " + (float) Util.round(myData.getSystolicMaxValue()));
//        LimitLine sysAverage = new LimitLine((float) Util.round(myData.getSystolicAvgValue()), "");
//        limitLineSettings(sysAverage);
//        sysAverage.setLineColor(Color.parseColor("#e87722"));
//        sysAverage.setTextColor(Color.parseColor("#000000"));
//        sysAverage.setLabel("Avg " + (float) Util.round(myData.getSystolicAvgValue()));
//
//        LimitLine diaMin = new LimitLine((float) Util.round(myData.getDiastolicMinValue()), "");
//        limitLineSettings(diaMin);
//        diaMin.setLineColor(Color.parseColor("#ad520a"));
//        diaMin.setTextColor(Color.parseColor("#000000"));
//        diaMin.setLabel("Min " + Util.round(myData.getDiastolicMinValue()));
//        LimitLine diaMax = new LimitLine((float) Util.round(myData.getDiastolicMaxValue()), "");
//        limitLineSettings(diaMax);
//        diaMax.setLineColor(Color.parseColor("#ad520a"));
//        diaMax.setTextColor(Color.parseColor("#000000"));
//        diaMax.setLabel("Max " + (float) Util.round(myData.getDiastolicMaxValue()));
//        LimitLine diaAverage = new LimitLine((float) Util.round(myData.getDiastolicAvgValue()), "");
//        limitLineSettings(diaAverage);
//        diaAverage.setLineColor(Color.parseColor("#ad520a"));
//        diaAverage.setTextColor(Color.parseColor("#000000"));
//        diaAverage.setLabel("Avg " + (float) Util.round(myData.getDiastolicAvgValue()));
//
//        LimitLine pulseMin = new LimitLine((float) Util.round(myData.getPulseMinValue()), "");
//        limitLineSettings(pulseMin);
//        pulseMin.setLineColor(Color.parseColor("#4a7768"));
//        pulseMin.setTextColor(Color.parseColor("#000000"));
//        pulseMin.setLabel("Min " + Util.round(myData.getPulseMinValue()));
//        LimitLine pulseMax = new LimitLine((float) Util.round(myData.getPulseMaxValue()), "");
//        limitLineSettings(pulseMax);
//        pulseMax.setLineColor(Color.parseColor("#4a7768"));
//        pulseMax.setTextColor(Color.parseColor("#000000"));
//        pulseMax.setLabel("Max " + (float) Util.round(myData.getPulseMaxValue()));
//        LimitLine pulseAverage = new LimitLine((float) Util.round(myData.getPulseAvgValue()), "");
//        limitLineSettings(pulseAverage);
//        pulseAverage.setLineColor(Color.parseColor("#4a7768"));
//        pulseAverage.setTextColor(Color.parseColor("#000000"));
//        pulseAverage.setLabel("Avg " + (float) Util.round(myData.getPulseAvgValue()));

        LimitLine highest = new LimitLine(myData.getHighestValue(), "");
        highest.setLineWidth(2f);
        highest.enableDashedLine(20f, 20f, 0f);
        highest.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        highest.setTextSize(10f);
        highest.setLineColor(Color.parseColor("#b2b2b2"));
        highest.setLabel("");

//        yAxis.addLimitLine(sysMin);
//        yAxis.addLimitLine(sysMax);
//        yAxis.addLimitLine(sysAverage);
//        yAxis.addLimitLine(diaMin);
//        yAxis.addLimitLine(diaMax);
//        yAxis.addLimitLine(diaAverage);
//        yAxis.addLimitLine(pulseMin);
//        yAxis.addLimitLine(pulseMax);
//        yAxis.addLimitLine(pulseAverage);
        yAxis.addLimitLine(highest);
        
        LimitLine lowest = new LimitLine((float) myData.getLowestValue(), "");
        lowest.setLineWidth(2f);
        lowest.enableDashedLine(20f, 20f, 0f);
        lowest.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        lowest.setTextSize(10f);
        lowest.setLineColor(Color.parseColor("#b2b2b2"));
        //lowest.setLabel("  0");
        yAxis.addLimitLine(lowest);
    }

    private void oxygenLimitLine(YAxis yAxis) {
        if (!((myData.getOxygenMinValue() == 0) && (myData.getOxygenMaxValue() == 0))) {

            LimitLine oxymin = new LimitLine((float) Util.round(myData.getOxygenMinValue()), "");
            limitLineSettings(oxymin);
            oxymin.setLineColor(Color.parseColor("#6699cc"));
            oxymin.setTextColor(Color.parseColor("#6699cc"));
            oxymin.setLabel(getResources().getString(R.string.minCap) + " " + Util.round(myData.getOxygenMinValue()));
            LimitLine oxymax = new LimitLine((float) Util.round(myData.getOxygenMaxValue()), "");
            limitLineSettings(oxymax);
            oxymax.setLineColor(Color.parseColor("#6699cc"));
            oxymax.setTextColor(Color.parseColor("#6699cc"));
            oxymax.setLabel(getResources().getString(R.string.maxCap) + " " + (float) Util.round(myData.getOxygenMaxValue()));
            yAxis.addLimitLine(oxymin);
            yAxis.addLimitLine(oxymax);
        }

//        LimitLine oxyaverage = new LimitLine((float) Util.round(myData.getOxygenAvgValue()), "");
//        limitLineSettings(oxyaverage);
//        oxyaverage.setLineColor(Color.parseColor("#e87722"));
//        oxyaverage.setTextColor(Color.parseColor("#000000"));
//        oxyaverage.setLabel(getResources().getString(R.string.avg) + " " + (float) Util.round(myData.getOxygenAvgValue()));

//        LimitLine pulsemin = new LimitLine((float) Util.round(myData.getPulseMinValue()), "");
//        limitLineSettings(pulsemin);
//        pulsemin.setLineColor(Color.parseColor("#6699cc"));
//        pulsemin.setLabel("Min " + Util.round(myData.getPulseMinValue()));
//        LimitLine pulsemax = new LimitLine((float) Util.round(myData.getPulseMaxValue()), "");
//        limitLineSettings(pulsemax);
//        pulsemax.setLineColor(Color.parseColor("#6699cc"));
//        pulsemax.setLabel("Ma x" + (float) Util.round(myData.getPulseMaxValue()));
//        LimitLine pulseaverage = new LimitLine((float) Util.round(myData.getPulseAvgValue()), "");
//        limitLineSettings(pulseaverage);
//        pulseaverage.setLineColor(Color.parseColor("#e87722"));
//        pulseaverage.setLabel("Avg " + (float) Util.round(myData.getPulseAvgValue()));

        LimitLine highest = new LimitLine(myData.getHighestValue(), "");
        highest.setLineWidth(2f);
        highest.enableDashedLine(20f, 20f, 0f);
        highest.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        highest.setTextSize(10f);
        highest.setLineColor(Color.parseColor("#b2b2b2"));
        highest.setLabel("");
        yAxis.addLimitLine(highest);
        
        LimitLine lowest = new LimitLine((float) myData.getLowestValue(), "");
        lowest.setLineWidth(2f);
        lowest.enableDashedLine(20f, 20f, 0f);
        lowest.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        lowest.setTextSize(10f);
        lowest.setLineColor(Color.parseColor("#b2b2b2"));
        //lowest.setLabel("  0");
        yAxis.addLimitLine(lowest);

 //       yAxis.addLimitLine(oxyaverage);
//        yAxis.addLimitLine(pulsemin);
//        yAxis.addLimitLine(pulsemax);
//        yAxis.addLimitLine(pulseaverage);

    }

    private void limitLineSettings(LimitLine limitLine) {
        limitLine.setLineWidth(2f);
        limitLine.enableDashedLine(20f, 20f, 0f);
        limitLine.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        limitLine.setTextSize(16f);

        //limitLine.setTypeface(Typeface.DEFAULT_BOLD);
    }

//    private void MyDataTracking(String mode, String event) {
////Vital: 1-glucose,2-oxygen,3-pressure,6-temperature,7-weight
//        //mode: 1-line graph,2-bar graph,3-table chart
//        //event: 4-click,5-scroll
//        String vital = "7";
//        if (vitalClickFlag.equals("glucose"))
//            vital = "1";
//        else if (vitalClickFlag.equals("oxygen"))
//            vital = "2";
//        else if (vitalClickFlag.equals("pressure"))
//            vital = "3";
//        else if (vitalClickFlag.equals("temp"))
//            vital = "6";
//        else if (vitalClickFlag.equals("weight"))
//            vital = "7";
//        ContentValues values = new ContentValues();
//        values.put("Patient_Id", patientId);
//        values.put("Date_Time", globalClass.get_patient_time_zone_time());
//        values.put("Device", globalClass.getDeviceType());
//        values.put("OS", globalClass.getDeviceOSVersion());
//        values.put("Version", getResources().getString(R.string.version));
//        values.put("Vital", vital);
//        values.put("Mode", mode);
//        values.put("Event", event);
//        values.put("Value", 1);
//        myDataTracker_db.GetMatchingTrackEvent(values);
//    }

    private void tableChartMobile_Tab_Handling() {
        HashMap<String, ArrayList<DailyReadingValues>> timeReadingList = new
                HashMap<String, ArrayList<DailyReadingValues>>();
        ArrayList<DateValues> dateReadingListTable = new ArrayList<DateValues>();
        ArrayList<DailyReadingValues> timeList;
        ArrayList<DailyReadingValues> timeSortingList = null;
        for (int i = 0; i < dateReadingList.size(); i++) {
            DateValues dateValues = dateReadingList.get(i);
            if (dateValues.getDataFlag() == 1) {
                dateReadingListTable.add(dateValues);
                timeList = weightReadigList.get(dateValues.getDateValue());
                timeSortingList = new ArrayList<DailyReadingValues>();
                for (int j = timeList.size() - 1; j >= 0; j--) {

                    timeSortingList.add(timeList.get(j));
                }
                timeReadingList.put(dateValues.getDateValue(), timeSortingList);
            }
        }

        //if (getResources().getBoolean(R.bool.isTab)) {
            if (dateReadingListTable.size() > 14) {
                layout_tableCharRecycler1.setVisibility(View.VISIBLE);
                layout_tableCharRecycler2.setVisibility(View.VISIBLE);
                layout_tableCharRecycler3.setVisibility(View.VISIBLE);
                TableChartRecyclerAdapter1 tableChartRecyclerAdapter1 = new
                        TableChartRecyclerAdapter1(timeReadingList, dateReadingListTable, vitalClickFlag, true, getBaseContext());
                tableChartRecyclerView1.setAdapter(tableChartRecyclerAdapter1);

                ArrayList<DateValues> dateReadingList2 = new ArrayList<DateValues>();
                for (int i = 0; i < dateReadingListTable.size() - 7; i++)
                    dateReadingList2.add(dateReadingListTable.get(i));
                TableChartRecyclerAdapter2 tableChartRecyclerAdapter2 = new
                        TableChartRecyclerAdapter2(timeReadingList, dateReadingList2, vitalClickFlag, getBaseContext());
                tableChartRecyclerView2.setAdapter(tableChartRecyclerAdapter2);

                ArrayList<DateValues> dateReadingList3 = new ArrayList<DateValues>();
                for (int i = 0; i < dateReadingListTable.size() - 14; i++)
                    dateReadingList3.add(dateReadingListTable.get(i));
                TableChartRecyclerAdapter3 tableChartRecyclerAdapter3 = new
                        TableChartRecyclerAdapter3(timeReadingList, dateReadingList3, vitalClickFlag, getBaseContext());
                tableChartRecyclerView3.setAdapter(tableChartRecyclerAdapter3);


            } else if (dateReadingListTable.size() > 7) {

                layout_tableCharRecycler1.setVisibility(View.VISIBLE);
                layout_tableCharRecycler2.setVisibility(View.VISIBLE);
                layout_tableCharRecycler3.setVisibility(View.GONE);
                TableChartRecyclerAdapter1 tableChartRecyclerAdapter1 = new
                        TableChartRecyclerAdapter1(timeReadingList, dateReadingListTable, vitalClickFlag, true, getBaseContext());
                tableChartRecyclerView1.setAdapter(tableChartRecyclerAdapter1);
               txt_pressureTableHeader.setTypeface(type, Typeface.BOLD);
                ArrayList<DateValues> dateReadingList2 = new ArrayList<DateValues>();
                for (int i = 0; i < dateReadingListTable.size() - 7; i++)
                    dateReadingList2.add(dateReadingListTable.get(i));
                TableChartRecyclerAdapter2 tableChartRecyclerAdapter2 = new
                        TableChartRecyclerAdapter2(timeReadingList, dateReadingList2, vitalClickFlag, getBaseContext());
                tableChartRecyclerView2.setAdapter(tableChartRecyclerAdapter2);
                txt_pressureTableHeader.setTypeface(type, Typeface.BOLD);

            } else {

                layout_tableCharRecycler1.setVisibility(View.VISIBLE);
                layout_tableCharRecycler2.setVisibility(View.GONE);
                layout_tableCharRecycler3.setVisibility(View.GONE);
                TableChartRecyclerAdapter1 tableChartRecyclerAdapter1 = new
                        TableChartRecyclerAdapter1(timeReadingList, dateReadingListTable, vitalClickFlag, true, getBaseContext());
                tableChartRecyclerView1.setAdapter(tableChartRecyclerAdapter1);
            }
//        } else {
//
//            layout_tableCharRecycler1.setVisibility(View.VISIBLE);
//            layout_tableCharRecycler2.setVisibility(View.GONE);
//            layout_tableCharRecycler3.setVisibility(View.GONE);
//            TableChartRecyclerAdapter1 tableChartRecyclerAdapter1 = new
//                    TableChartRecyclerAdapter1(timeReadingList, dateReadingListTable, vitalClickFlag, false, getBaseContext());
//            tableChartRecyclerView1.setAdapter(tableChartRecyclerAdapter1);
//        }

    }

    public class MyValueFormatter implements ValueFormatter {

        private DecimalFormat mFormat;

        public MyValueFormatter() {
            mFormat = new DecimalFormat("###,###,##0.0"); // use one decimal
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            // write your logic here
            return mFormat.format(value); // e.g. append a dollar-sign
        }
    }

    @Override
    public void onBackPressed() {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
    }
    
    private int[] calculateLimitRange(double max, double high, double min, double low){
    	double highestValue = 999;
    	double lowestValue = 0;
        double limitRange = 0;

        if (max > high)
            highestValue = max;
        else
            highestValue = high;

        if (min == 0)
            lowestValue = low;
        else if(min < low)
            lowestValue = min;
        else
            lowestValue = low;

        limitRange = ((highestValue - lowestValue)*0.1);
        if(limitRange < 1){

            limitRange = 1;
        }
        double highestLimitRange = highestValue+limitRange;
        double lowestLimitRange = lowestValue-limitRange;

        int[] rangeArray = new int[]{(int)Math.ceil(highestLimitRange), (int)Math.floor(lowestLimitRange)};
        return rangeArray;
    }
}
