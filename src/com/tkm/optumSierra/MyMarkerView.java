
package com.tkm.optumSierra;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;

import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.Utils;
import com.tkm.optumSierra.bean.DailyReadingValues;
import com.tkm.optumSierra.bean.DateValues;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


/**
 * Custom implementation of the MarkerView.
 *
 * @author Philipp Jahoda
 */
public class MyMarkerView extends MarkerView {

    private HashMap<String, ArrayList<DailyReadingValues>> weightReadingList;
    private ArrayList<DateValues> dateReadingList;
    //private ArrayList<Integer> colors;
    private TextView txt_myMarkerDate;
    private TextView txt_myMarkerTime;
    private TextView txt_myMarkerValue;
    private TextView txt_myMarkerMealIndicator;
    private LinearLayout layout_markerView;
    private String flag = "";
    private LinearLayout layout_markerViewTime;
    private Context context;
    private int graphHeight;
    private String modeFlag = "";
    public PopupWindow pwindo;
    FrameLayout layout_mydata;
    int highlightValueIndex;
    public MyMarkerView(Context context, FrameLayout layout_mydata,int layoutResource, ArrayList<DateValues> dateList,
                        HashMap<String, ArrayList<DailyReadingValues>> readingList,
                        String vitalFlag, int height, String modeFlag) {
        super(context, layoutResource);
        this.context = context;
        graphHeight = height;
        this.layout_mydata = layout_mydata;
        //this.colors = colors;
        flag = vitalFlag;
        this.modeFlag = modeFlag;
        dateReadingList = dateList;
        weightReadingList = readingList;
        Typeface type = Typeface.createFromAsset(context.getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");


        txt_myMarkerDate = (TextView) findViewById(R.id.txt_myMarkerDate);
        txt_myMarkerDate.setTypeface(type, Typeface.BOLD);
        txt_myMarkerTime = (TextView) findViewById(R.id.txt_myMarkerTime);
        txt_myMarkerValue = (TextView) findViewById(R.id.txt_myMarkerValue);
        layout_markerView = (LinearLayout) findViewById(R.id.layout_markerView);
        txt_myMarkerMealIndicator = (TextView) findViewById(R.id.txt_myMarkerMealIndicator);
        layout_markerViewTime = (LinearLayout) findViewById(R.id.layout_markerview_time);
        txt_myMarkerMealIndicator.setVisibility(GONE);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        txt_myMarkerMealIndicator.setVisibility(GONE);
        highlightValueIndex = highlight.getXIndex();
        if (flag.equals("pressure")) {

            showPressureValues(highlight);
        } else if (flag.equals("oxygen")) {

            showOxygenValues(highlight);
        } else if (flag.equals("glucose")) {
        	
        	txt_myMarkerMealIndicator.setVisibility(VISIBLE);
            showValues(highlight);
        } else {

            showValues(highlight);
        }


    }

    public int getXOffset(float xpos) {
        // this will center the marker-view horizontally
        return -(getWidth() / 2);
    }

    public int getYOffset(float ypos) {
        // this will cause the marker-view to be above the selected value
        return -getHeight();
    }

    @Override
    public void draw(Canvas canvas, float posx, float posy) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        int lwidth = layout_markerView.getWidth();
        int lheight = layout_markerView.getHeight();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float d = metrics.densityDpi;
       // if (getResources().getBoolean(R.bool.isTab)) {
            if ((width - posx) < lwidth) {
                posx = width - lwidth;
            }
//        } else {
//            if (modeFlag.equals("line")) {
////                if (flag.equals("glucose")) {
////                    if (width == 1440 && d == 560) {
////                        if ((width * 2.5 - posx) < lwidth) {
////                            posx = posx - lwidth;
////                        }
////                    } else {
////                        if ((width * 2.9 - posx) < lwidth) {
////                            posx = posx - lwidth;
////                        }
////                    }
////                } else {
//                    if (width == 1440 && d == 560) {
//                        if ((width * 2.4 - posx) < lwidth) {
//                            posx = posx - lwidth;
//                        }
//                    } else {
//                        if ((width * 2.8 - posx) < lwidth) {
//                            posx = posx - lwidth;
//                        }
//                    }
//
// //               }
//            } else {
//
//                if ((width - posx) < lwidth) {
//                    posx = width - lwidth;
//                }
//            }
//
//
//        }
//        if (getResources().getBoolean(R.bool.isTab)) {
            if (height < 1200) {
//                if (flag.equals("glucose")) {
//
//                    if ((height / 2.6) - posy < lheight) {
//                        posy = (float) (height / 2.6) - lheight;
//                    }
//                } else {

//                    if ((height / 2.2) - posy < lheight) {
//                        posy = (float) (height / 1.9) - lheight;
//                    }
//                }

            } else {
//                if (flag.equals("glucose")) {
//
//                    if ((height / 2.1) - posy < lheight) {
//                        posy = (float) (height / 1.9) - lheight;
//                    }
//                } else {

                    if ((height / 1.9) - posy < lheight) {
                        posy = (float) (height / 1.7) - lheight;
                    }
                }
 //           }
//        } else {
////            if (flag.equals("glucose")) {
////                if ((height / 2.1) - posy < lheight) {
////                    posy = (float) (height / 2.3) - lheight;
////                }
////            } else {
//
//                if ((height / 1.9) - posy < lheight) {
//                    posy = (float) (height / 1.9) - lheight;
//                }
//     //       }
//        }

            if(posy < 0)
                posy = 0;
        // translate to the correct position and draw

//        canvas.translate(posx, posy);
//        draw(canvas);
            popup(posx, posy);
        //canvas.translate(-posx, -posy);
    }

    public String parseDate(String time) {
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "EEE, MMM dd, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void showValues(Highlight highlight) {

        DateValues dateValues = dateReadingList.get((int) highlight.getXIndex());
       // if (dateValues.getDataFlag() == 0) {

            layout_markerView.setVisibility(GONE);
//        } else {
//
//            layout_markerView.setVisibility(VISIBLE);
//        }
        txt_myMarkerDate.setText(parseDate(dateValues.getDateValue()));

        ArrayList<DailyReadingValues> dailyReadingValueList = weightReadingList.get(dateValues.getDateValue());
        String time = "";
        String value = "";
        String meal = "";
        for (int i = 0; i < dailyReadingValueList.size(); i++) {

            DailyReadingValues dailyReadingValues = dailyReadingValueList.get(i);
            if (i == dailyReadingValueList.size() - 1) {

                time += parseTime(dailyReadingValues.getTime());
                value += Util.round(dailyReadingValues.getValue());
                meal += dailyReadingValues.getMeal();
            } else {

                int ln=0;
                SharedPreferences settings_n = context.getSharedPreferences(CommonUtilities.USER_SP, 0);
                ln = Integer.parseInt(settings_n.getString("language_id", "0"));
                if (ln == 11) {
                	
                    if (dailyReadingValues.getMeal().equals("Other") || dailyReadingValues.getMeal().equals("Otro")) {


                        meal += dailyReadingValues.getMeal() + "\n";
                        value += Util.round(dailyReadingValues.getValue()) + "\n";
                        time += parseTime(dailyReadingValues.getTime()) + "\n";
                    } else if (dailyReadingValues.getMeal().contains("Antes")) {

                        meal += dailyReadingValues.getMeal() + "\n";
                        value += Util.round(dailyReadingValues.getValue()) + "\n\n";
                        time += parseTime(dailyReadingValues.getTime()) + "\n\n";
                    } else {
                        //if (context.getResources().getBoolean(R.bool.isTab)) {

                            meal += dailyReadingValues.getMeal() + "\n";
                            value += Util.round(dailyReadingValues.getValue()) + "\n\n";
                            time += parseTime(dailyReadingValues.getTime()) + "\n\n";
//                        } else {
//
//                            meal += dailyReadingValues.getMeal() + "\n";
//                            value += Util.round(dailyReadingValues.getValue()) + "\n\n";
//                            time += parseTime(dailyReadingValues.getTime()) + "\n\n";
//                        }

                    }
                }else{
                	
                    meal += dailyReadingValues.getMeal() + "\n";
                    value += Util.round(dailyReadingValues.getValue()) + "\n";
                    time += parseTime(dailyReadingValues.getTime()) + "\n";
                }
            }

        }

        txt_myMarkerTime.setText(time);
        txt_myMarkerValue.setText(value);
        txt_myMarkerMealIndicator.setText(meal);
    }

    private void showPressureValues(Highlight highlight) {

        DateValues dateValues = dateReadingList.get((int) highlight.getXIndex());
//        if (dateValues.getDataFlag() == 0) {
//
            layout_markerView.setVisibility(GONE);
//        } else {
//
//            layout_markerView.setVisibility(VISIBLE);
//        }
        txt_myMarkerDate.setText(parseDate(dateValues.getDateValue()));

        ArrayList<DailyReadingValues> dailyReadingValueList = weightReadingList.get(dateValues.getDateValue());
        String time = "";
        String value = "";
        for (int i = 0; i < dailyReadingValueList.size(); i++) {

            DailyReadingValues dailyReadingValues = dailyReadingValueList.get(i);
            if (i == dailyReadingValueList.size() - 1) {

                time += parseTime(dailyReadingValues.getTime());

                if (highlight.getDataSetIndex() == 0) {
                    value += dailyReadingValues.getSystolicValue();
                } else if (highlight.getDataSetIndex() == 1) {

                    value += dailyReadingValues.getDiastolicValue();
                } else if (highlight.getDataSetIndex() == 2) {

                    value += dailyReadingValues.getPulseValue();
                }

            } else {

                time += parseTime(dailyReadingValues.getTime()) + "\n";
                if (highlight.getDataSetIndex() == 0) {
                    value += dailyReadingValues.getSystolicValue() + "\n";
                } else if (highlight.getDataSetIndex() == 1) {

                    value += dailyReadingValues.getDiastolicValue() + "\n";
                } else if (highlight.getDataSetIndex() == 2) {

                    value += dailyReadingValues.getPulseValue() + "\n";
                }
            }

        }
        txt_myMarkerTime.setText(time);
        txt_myMarkerValue.setText(value);
    }

    private void showOxygenValues(Highlight highlight) {

        DateValues dateValues = dateReadingList.get((int) highlight.getXIndex());
//        if (dateValues.getDataFlag() == 0) {
//
            layout_markerView.setVisibility(GONE);
//        } else {
//
//            layout_markerView.setVisibility(VISIBLE);
//        }
        txt_myMarkerDate.setText(parseDate(dateValues.getDateValue()));

        ArrayList<DailyReadingValues> dailyReadingValueList = weightReadingList.get(dateValues.getDateValue());
        String time = "";
        String value = "";
        for (int i = 0; i < dailyReadingValueList.size(); i++) {

            DailyReadingValues dailyReadingValues = dailyReadingValueList.get(i);
            if (i == dailyReadingValueList.size() - 1) {

                time += parseTime(dailyReadingValues.getTime());

                if (highlight.getDataSetIndex() == 0)
                    value += dailyReadingValues.getOxygenValue();
                else if (highlight.getDataSetIndex() == 1)
                    value += dailyReadingValues.getPulseValue();

            } else {

                time += parseTime(dailyReadingValues.getTime()) + "\n";
                if (highlight.getDataSetIndex() == 0)
                    value += dailyReadingValues.getOxygenValue() + "\n";
                else if (highlight.getDataSetIndex() == 1)
                    value += dailyReadingValues.getPulseValue() + "\n";
            }

        }
        txt_myMarkerTime.setText(time);
        txt_myMarkerValue.setText(value);
    }

    public String parseTime(String time) {
        String inputPattern = "H:mm:ss";
        String outputPattern = "hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date).toUpperCase();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    
    private void popup(float posx, float posy){
        if(pwindo != null) {
            if (pwindo.isShowing()) {
                pwindo.dismiss();
            }
        }
    	LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    			View layout = inflater.inflate(R.layout.marker,null);
        if(posy < 10) {
            //posy = 150;
            pwindo = new PopupWindow(layout, LayoutParams.WRAP_CONTENT, 550, true);
        }
        else
            pwindo = new PopupWindow(layout, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);



//        Activity activity = (Activity) context;
//        dialog = new Dialog(activity);
//        // Include dialog.xml file
//        dialog.setContentView(R.layout.marker);

        // Set dialog title


        // set values for custom dialog components - text, image and button

        Typeface type = Typeface.createFromAsset(context.getAssets(),
                "fonts/FrutigerLTStd-Roman.otf");
        TextView txt_myMarkerDate1 = (TextView) layout.findViewById(R.id.txt_myMarkerDate);
        txt_myMarkerDate1.setTypeface(type, Typeface.BOLD);
        TextView txt_myMarkerTime1 = (TextView) layout.findViewById(R.id.txt_myMarkerTime);
        TextView txt_myMarkerValue1 = (TextView) layout.findViewById(R.id.txt_myMarkerValue);       
        TextView txt_myMarkerMealIndicator1 = (TextView) layout.findViewById(R.id.txt_myMarkerMealIndicator);
      
        if (flag.equals("glucose"))
            txt_myMarkerMealIndicator1.setVisibility(VISIBLE);
        else
            txt_myMarkerMealIndicator1.setVisibility(GONE);
        txt_myMarkerDate1.setText(txt_myMarkerDate.getText().toString());
        txt_myMarkerTime1.setText(txt_myMarkerTime.getText().toString());
        txt_myMarkerValue1.setText(txt_myMarkerValue.getText().toString());
        txt_myMarkerMealIndicator1.setText(txt_myMarkerMealIndicator.getText().toString());
//        dialog.show();
        pwindo.setBackgroundDrawable(getResources().getDrawable(R.color.transparent));
        pwindo.setOutsideTouchable(true);
        if(!txt_myMarkerTime1.getText().toString().equals("null")){
        	 pwindo.showAtLocation(layout_mydata,Gravity.LEFT,(int)posx,(int)posy-200);
        }
       


    }
}
