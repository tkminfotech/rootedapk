package com.tkm.optumSierra;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Set;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.UUID;

import com.tkm.optumSierra.bean.ClassWeight;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.dal.Weight_db;
import com.tkm.optumSierra.service.AdviceService;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.PINReceiver;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.app.ActivityManager.RunningServiceInfo;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import com.tkm.optumSierra.util.Log;
import com.tkm.optumSierra.util.OmronPinReceiver;

import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class P724Activity extends Titlewindow implements OnInitListener {

	private final BroadcastReceiver mybroadcast = new PINReceiver();
	private static final int REQUEST_ENABLE_BT = 1;
	private String TAG = "------------------P724Activity-----------------------";
	public static final String PREFS_NAME_SCHEDULE = "DroidPrefSc";
	// Droid_Measurement_db dbcreate1 = new Droid_Measurement_db(this);
	Weight_db dbcreateweight = new Weight_db(this);
	Sensor_db dbSensor = new Sensor_db(this);
	private BluetoothAdapter mBluetoothAdapter = null;
	private static final String DEVICE_NAME = "Electronic Scale";
	// Progress Dialog
	private ProgressDialog pDialog;
	private String result_re = "0, 0";
	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int PROGRESS_BAR_TYPE = 0;
	public int back = 0;
	TextView textView = null;
	private ProgressDialog progressDialog = null;
	private TextToSpeech tts;
	String macAddress = "";
	private static final String HEXES = "0123456789ABCDEF";

	BluetoothDevice device = null;
	private int deviceType = 0;
	ImageView rocketImage;
	int measureId;
	AnimationDrawable rocketAnimation;
	BackgroundThread backgroundThread;
	private String dataResult = "";
	private static final UUID MY_UUID_SECURE = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");


	private GlobalClass appClass;
	Util utilobjct = new Util();
	ObjectAnimator AnimPlz, ObjectAnimator;
	public boolean cancel_flag = false;
	TextView txtwelcom, txtReading;
	Handler h = new Handler();
	int delay = 14000; //14 seconds
	Runnable runnable;
	boolean isSocketClosed = false;
	ConnectThread ct;
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.US);
			Log.i(TAG, "Initilization Success");
			tts.setSpeechRate(0); // set speech speed rate
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e(TAG, "Language is not supported");
			} else {
				// speakOut();
			}
		} else {
			Log.e(TAG, "TTS Initilization Failed");
		}

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aand_dreader);

		appClass = (GlobalClass) getApplicationContext();		
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = false;
		Log.e(TAG, "P724 BT page started");
		tts = new TextToSpeech(this, this);

		Button btnmanual = (Button) findViewById(R.id.manuval);

		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		// txtwelcom.setText("Please step on the scale.");
		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);

		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		rocketImage.setVisibility(View.INVISIBLE);
		txtReading.setVisibility(View.INVISIBLE);
		Log.i(TAG, "-------------------------------- "+Build.MODEL);
		try {
			String Pin = dbSensor.SelectWTPIN();
			Constants.setPIN(Pin);

		} catch (Exception e) {

		}

		//Constants.setPIN(Pin);// set pin for auto pairing
		btnmanual.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Intent intent = new Intent(P724Activity.this, WeightEntry.class);
				//btc.cancel(true);
				cancel_flag = true;
				finish();
				startActivity(intent);
			}
		});

		textView = new TextView(this);

		cancel_flag = false;
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
		registerReceiver(mybroadcast, filter);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();	

		Bundle extras = getIntent().getExtras();
		if(extras != null){
			appClass.setBundle(extras);
		}else{
			extras = appClass.getBundle();
		}
		if (extras.getString("macaddress").length() > 1)
			macAddress = extras.getString("macaddress");

		if (Build.MODEL.equals("MN-724")){

			if (macAddress.trim().length() == 17) { // getting a configured device
				device = mBluetoothAdapter.getRemoteDevice(macAddress);
				Log.d(TAG, device.getAddress().toString()+"-------"+device.getName());
				ct = new ConnectThread(device);
				ct.start();
				h.postDelayed(new Runnable() {
					public void run() {
						if(isSocketClosed){
							device = mBluetoothAdapter.getRemoteDevice(macAddress);
							ct = new ConnectThread(device);
							ct.start();		
						}
						if(cancel_flag == false){
							runnable=this;
							h.postDelayed(runnable, 5000);
						}

					}
				}, 5000);
			}
		}else if (Build.MODEL.equals("SCH-i705") || Build.MODEL.contains("Nexus")){

			if (macAddress.trim().length() == 17) { // getting a configured device
				device = mBluetoothAdapter.getRemoteDevice(macAddress);
				Log.d(TAG, device.getAddress().toString()+"-------"+device.getName());
				ct = new ConnectThread(device);
				ct.start();
				h.postDelayed(new Runnable() {
					public void run() {
						if(isSocketClosed){
							device = mBluetoothAdapter.getRemoteDevice(macAddress);
							ct = new ConnectThread(device);
							ct.start();		
						}
						if(cancel_flag == false){
							runnable=this;
							h.postDelayed(runnable, 2000);
						}

					}
				}, 2000);
			}
		}else{

			IntentFilter filter1 = new IntentFilter(BluetoothDevice.ACTION_FOUND);
			registerReceiver(mReceiver, filter1);		
			mBluetoothAdapter.startDiscovery();
			h.postDelayed(new Runnable() {
				public void run() {
					mBluetoothAdapter.startDiscovery();				
					if(cancel_flag == false){
						runnable=this;
						h.postDelayed(runnable, delay);
					}
				}
			}, delay);
		}

		setwelcome();

		Log.i(TAG, "-------------------oncreate--------------  p724  activity");
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, " KEYCODE_BACK clicked:");
			back = 1;
			//btc.cancel(true);
			Intent intent = new Intent(this, FinalMainActivity.class);			
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}	

	@Override
	public void onStop() {	
		super.onStop();
		isSocketClosed = false;
		cancel_flag = true;		
		h.removeCallbacks(runnable);
		Log.i(TAG, "-------------------onstop--------------  p724  activity");

		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");

		// this.finish();

		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		if (ct != null) {
			try {
				ct.interrupt();
				ct.cancel();

				Log.e(TAG, "*****************************interrupted********");
			} catch (Exception e) {
				Log.e(TAG, "*****************************"+e.getMessage());
			}
		}

	}		

	public Boolean preferenceWelcome(int mode) {
		Boolean flag = null;
		try {
			SharedPreferences settings = getSharedPreferences(
					PREFS_NAME_SCHEDULE, 0);
			int scheduletatus = settings.getInt("welcome", -1);
			if (mode == 1) {
				SharedPreferences.Editor editor = settings.edit();
				editor.putInt("welcome", 1);
				editor.commit();
			}
			if (scheduletatus == 0)

				flag = true;
			else
				flag = false;
		}

		catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		return flag;
	}

	private static final String PREFS_NAME_TIME = "TimePrefSc";

	public void getSavedTime1() {

		SharedPreferences settings = getSharedPreferences(PREFS_NAME_TIME, 0);
		String savedTime = settings.getString("timezone", "00");

		Log.i(TAG, "inside getSavedTime fn; rest servertime " + savedTime);

		int year = Integer.parseInt(savedTime.substring(6, 10));
		int month = Integer.parseInt(savedTime.substring(0, 2));
		int day = Integer.parseInt(savedTime.substring(3, 5));
		int hour = Integer.parseInt(savedTime.substring(11, 13));
		int minute = Integer.parseInt(savedTime.substring(14, 16));
		int seconds = Integer.parseInt(savedTime.substring(17, 19));
		TestDate.setCalender(year, month, day, hour, minute, seconds);

	}

	public double round(double d) {
		DecimalFormat twoDForm = new DecimalFormat("#.#");
		return Double.valueOf(twoDForm.format(d));
	}

	public String getHex(byte[] raw, int len) {
		Log.i(TAG, "Get Hex...");
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(3 * raw.length);
		for (final byte b : raw) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4))
			.append(HEXES.charAt((b & 0x0F))).append(" ");
			if (--len == 0)
				break;
		}
		return hex.toString();
	}

	public class BackgroundThread extends Thread {
		volatile boolean running = false;
		int cnt;

		void setRunning(boolean b) {
			running = b;
			cnt = 6;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while (running) {
				try {
					sleep(60);
					if (cnt-- == 0) {
						running = false;
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			handler.sendMessage(handler.obtainMessage());
		}
	}

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub

			setProgressBarIndeterminateVisibility(false);
			// progressDialog.dismiss();

			boolean retry = true;
			while (retry) {
				try {
					backgroundThread.join();
					retry = false;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			setwelcome();
			// setwelcome();
			// setContentView(R.layout.activity_black);
		}

	};

	private void setwelcome() {

		String th = "";
		String deviceMessage = "";

		if (preferenceWelcome(0)) {
			if (deviceType == 0) {
				if (preferenceWelcome(1)) {
					th = "Hello, ";
					deviceMessage = this
							.getString(R.string.enterweightbluetooth);

					/*
					 * Util.soundPlay("Sierra/Hello.wav");
					 * 
					 * try { Thread.sleep(800); } catch (InterruptedException e)
					 * { // TODO Auto-generated catch block //
					 * e.printStackTrace(); }
					 */

					txtwelcom.setText(deviceMessage);

				}

			} else if (deviceType == 1) {
				// th = "Good Morning, ";

				deviceMessage = this.getString(R.string.checkbloodpressure);
				txtwelcom.setText(deviceMessage);
				/*
				 * Util.soundPlay("Sierra/GoodMorning.wav"); try {
				 * Thread.sleep(1000); } catch (InterruptedException e) { //
				 * TODO Auto-generated catch block e.printStackTrace(); }
				 * tts.speak(welcome+" .", TextToSpeech.QUEUE_FLUSH, null);
				 */
			}
		} else {
			if (deviceType == 0) {

				deviceMessage = this.getString(R.string.enterweightbluetooth);

				txtwelcom.setText(deviceMessage);

			} else if (deviceType == 1) {
				th = this.getString(R.string.checkbloodpressure);
				txtwelcom.setText(th);
			}
		}

		if (deviceType == 0) {
			// tts.speak(welcome, TextToSpeech.QUEUE_FLUSH, null);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}

			SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
			String languageId = settingsUserSp.getString("language_id", "0");
			if(languageId.equals("11")){

				Util.soundPlay("Messages/StepOnScale_1.wav");
			}else{

				Util.soundPlay("Messages/StepOnScale.wav");
			}
		} else if (deviceType == 1) {
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
			// Util.soundPlay("Sierra/Please chek BP.wav");

		}

		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
		AnimPlz.setDuration(1500);
		AnimPlz.start();

		AnimPlz.addListener(new AnimatorListenerAdapter() {
			public void onAnimationEnd(Animator animation) {

				Log.e(TAG, "AnimPlz");

				txtReading.setVisibility(View.VISIBLE);
				rocketImage.setVisibility(View.VISIBLE);

			}
		});

	}

	@Override
	protected void onRestart() {
		super.onRestart();


	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		cancel_flag = false;
	}

	@Override
	protected void onPause() {
		try {
			unregisterReceiver(mybroadcast);
			//if(isPaired)
			if (!Build.MODEL.equals("SCH-i705") || !Build.MODEL.equals("MN-724"))
				unregisterReceiver(mReceiver);


		} catch (Exception e) {
			e.printStackTrace();
		}

		super.onPause();

	}
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {


		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			Log.e(TAG, "--------onReceive--------");
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				// Discovery has found a device. Get the BluetoothDevice
				// object and its info from the Intent.
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				String deviceName = device.getName();
				String deviceHardwareAddress = device.getAddress(); // MAC address
				Log.e(TAG, "--------search--------"+deviceHardwareAddress);

				if(deviceHardwareAddress.equals(macAddress)){
					Log.e(TAG, "-----------founded-------------"+deviceHardwareAddress+"+++"+macAddress);

					ct = new ConnectThread(device);
					ct.start();


				}

			}
		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();


		// Don't forget to unregister the ACTION_FOUND receiver.
		//if(isPaired)
		try {
			unregisterReceiver(mReceiver);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	private class ConnectThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final BluetoothDevice mmDevice;

		public ConnectThread(BluetoothDevice device) {
			// Use a temporary object that is later assigned to mmSocket
			// because mmSocket is final.
			BluetoothSocket tmp = null;
			mmDevice = device;

			try {
				// Get a BluetoothSocket to connect with the given BluetoothDevice.
				// MY_UUID is the app's UUID string, also used in the server code.
				if (Build.MODEL.equals("SCH-i705") || Build.MODEL.equals("MN-724"))
					tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID_SECURE);
				else
					tmp = device.createRfcommSocketToServiceRecord(MY_UUID_SECURE);


			} catch (IOException e) {
				Log.e(TAG, "Socket's create() method failed", e);
			}
			mmSocket = tmp;
		}

		public void run() {

			// Cancel discovery because it otherwise slows down the connection.
			Log.e(TAG, "-----------run-------------");
			mBluetoothAdapter.cancelDiscovery();

			try {
				// Connect to the remote device through the socket. This call blocks
				// until it succeeds or throws an exception.
				mmSocket.connect();
			} catch (IOException connectException) {
				// Unable to connect; close the socket and return.
				try {
					mmSocket.close();
				} catch (IOException closeException) {
					Log.e(TAG, "Could not close the client socket", closeException);
				}			
			}

			// The connection attempt succeeded. Perform work associated with
			// the connection in a separate thread.
			Log.e(TAG, "-----------Socket connected-----");
			manageMyConnectedSocket(mmSocket);

		}

		// Closes the client socket and causes the thread to finish.
		public void cancel() {
			Log.e(TAG, "##############Cancel#############");
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "Could not close the client socket", e);
			}
		}
	}

	private void manageMyConnectedSocket(BluetoothSocket socket){

		isSocketClosed = false;
		InputStream inStream = null;
		OutputStream outStream = null;
		try {
			inStream = socket.getInputStream();
			outStream = socket.getOutputStream();
		} catch (IOException exx) {
			exx.printStackTrace();
			Log.e(TAG, " : Conenction Failed...\n");
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		if (inStream == null || outStream == null) {
			Log.e(TAG, " : Stream is null in the scale ");
			try {
				inStream.close();
				outStream.close();
				socket.close();
			} catch (Exception cl) {
			}

		}

		byte[] buffer = new byte[1024];
		int bytes;

		if (null != inStream) {
			try {
				bytes = inStream.read(buffer);

				Log.i(TAG, "< " + getHex(buffer, bytes));

				byte a = 0;
				byte b = 0;
				a = buffer[5];
				b = buffer[4];			

				String hex1 = Integer.toHexString(a & 0XFF);
				String hex2 = Integer.toHexString(b & 0XFF);

				if (hex1.trim().length() == 1) {
					hex1 = "0" + hex1;
				}
				if (hex2.trim().length() == 1) {
					hex2 = "0" + hex2;
				}

				// String hex1 =formatter.format( Integer.toHexString(a
				// & 0XFF));
				// String hex2 = formatter.format(Integer.toHexString(b
				// & 0XFF));

				Log.i(TAG,
						"Got wt kg..."
								+ ((float) Integer.parseInt(
										hex2 + hex1, 16)) / 10);

				Log.i(TAG,
						"Got wt old lbs..."
								+ (float)  Util.round((((float) Integer
										.parseInt(hex2 + hex1, 16)) / 10) * 2.204));

				String bin = Integer.toHexString(Integer.parseInt(""
						+ (Integer.parseInt(hex2 + hex1, 16)) * 10));

				Log.i(TAG, " hex val in kg bin.." + bin);

				String val = bin;
				String val2 = "2B0F";
				String val3 = "C350";
				String res = ""
						+ (float) (((Integer.parseInt(val, 16)
								* Integer.parseInt(val2, 16) / Integer
								.parseInt(val3, 16)) + 1) / 2 * 2)
						/ 10;

				Log.i(TAG, "Got wt new hex formula  lbs wt..." + res);

				/*
				 * result_re = "" + (float) round((((float)
				 * Integer.parseInt(hex2 + hex1, 16)) / 10) * 2.204);
				 */
				result_re = res;
				// 2.2046226218487758072297380134503 new lbs conversion
				// 2.204 old lbs

				Log.i(TAG, "-------Got wt pound..." + result_re);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				isSocketClosed = true;
				Log.i(TAG, "+++++++++++++IOException.............." + result_re);
				e.printStackTrace();

				return;
			}
		}

		try {
			inStream.close();
			outStream.close();
			socket.close();
		} catch (Exception cl) {
		}

		if (cancel_flag) {
			Log.i(TAG, "return on post exec)");
			return;
		}

		Log.i(TAG, "onPostExecute()");
		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = flow.getInt("flow", 0); // #1
		if (val == 1) {				
			SharedPreferences section = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);
			SharedPreferences.Editor editor_retake = section.edit();
			editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(P724Activity.this));
			editor_retake.commit();
		}

		if (result_re.contains("-")) {

			Log.i(TAG, "return on post, exit due to negative reading.");
			return;

		} else {

			if (back == 1) {
				back = 0;
			} else {
				// progressDialog.dismiss();
				Log.i(TAG, "PROGRESS_BAR_TYPE end ");
				ContentValues values = new ContentValues();
				int PatientIdDroid = Constants.getdroidPatientid();
				String result;
				result = result_re;
				Log.i(TAG, "onPostExecute()");
				String message = result;
				String ttmessage = "";

				if (result.trim().length() == 0) {
					// message=Resources.getSystem().getString(R.string.unabletoconnectbluetooth);
					message = getApplicationContext().getString(
							R.string.unabletoconnectbluetooth);
					ttmessage = getApplicationContext().getString(
							R.string.pleasetry);
				} else {
					try {

						if (TestDate.getCurrentTime().length() == 2) {
							getSavedTime1();
						}

						if (deviceType == 0) { // weight
							// scale

							String wt = "";
							ClassWeight weight = new ClassWeight();
							weight.setPatientID(PatientIdDroid);
							weight.setWeightInKg(result_re);
							weight.setInputmode(0);
							SharedPreferences settingswt = getSharedPreferences(
									CommonUtilities.USER_TIMESLOT_SP, 0);
							String slot = settingswt.getString("timeslot",
									"AM");
							weight.setTimeslot(slot);
							SharedPreferences settings1 = getSharedPreferences(
									CommonUtilities.PREFS_NAME_date, 0);		
							String sectiondate=settings1.getString("sectiondate", "0");
							weight.setSectionDate(sectiondate);
							measureId = dbcreateweight.InsertWeight(weight);

						}
						if (deviceType == 0) {
							Intent intent = new Intent(P724Activity.this,
									ShowWightActivity.class);
							SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);
							SharedPreferences.Editor contract_editor = contract_cod.edit();
							contract_editor.putString("p724weight_paired", "pair");
							contract_editor.commit();
							intent.putExtra("text", result_re);
							intent.putExtra("measureId", measureId);

							startActivity(intent);
							P724Activity.this.finish();
							return;

						}

					} catch (Exception e) {
						message = getApplicationContext().getString(
								R.string.invaliddata);
					}
				}
				if (deviceType == 0) {
					Intent intent = new Intent(P724Activity.this,
							ShowWightActivity.class);
					SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);
					SharedPreferences.Editor contract_editor = contract_cod.edit();
					contract_editor.putString("p724weight_paired", "pair");
					contract_editor.commit();
					intent.putExtra("text", result_re);
					intent.putExtra("measureId", measureId);
					intent.putExtra("type", 0);
					startActivity(intent);
					P724Activity.this.finish();
					return;
				}
			}
		}
		Log.i(TAG, "onPostExecute() end");
	}
}
