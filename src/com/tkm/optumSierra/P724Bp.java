package com.tkm.optumSierra;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import com.tkm.optumSierra.P724Activity.BackgroundThread;
import com.tkm.optumSierra.bean.ClasssPressure;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.service.AdviceService;
import com.tkm.optumSierra.service.BluetoothServiceP724Bp;
import com.tkm.optumSierra.util.BatteryInfo;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.DeviceInfoBean;
import com.tkm.optumSierra.util.PINReceiver;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

import android.os.Bundle;
import android.os.Handler;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import com.tkm.optumSierra.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class P724Bp extends Titlewindow implements OnInitListener {

	int lastinsert_id = 0;
	Pressure_db dbcreatepressure = new Pressure_db(this);
	public String sys, dia, pul;
	private String TAG = "P724BpSmart";

	private TextToSpeech tts;
	TextView textView = null;
	ObjectAnimator AnimPlz, ObjectAnimator;
	TextView txtwelcom, txtReading;
	AnimationDrawable rocketAnimation;
	BackgroundThread backgroundThread;
	ImageView rocketImage;
	// private TextView showInfo;
	private int bt_view_cnt = 0;
	private final int mTempDelayCnt = 12;
	private BluetoothAdapter mBluetoothAdapter = null;
	private Timer timer = null;
	private BluetoothServiceP724Bp mChatService = null;
	public static final int MESSAGE_OK = 0;
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;
	public static final int MESSAGE_RESULT = 6;
	public static final int MESSAGE_ERRER = 7;
	public static final int MESSAGE_DEVICE = 8;
	public static final int MESSAGE_BATTERY = 9;
	private DeviceInfoBean deviceInfo;
	private BatteryInfo batteryInfo;
	String print;
	private final BroadcastReceiver mybroadcast = new PINReceiver();
private GlobalClass appClass;
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.US);
			Log.i(TAG, "Initilization Success");
			tts.setSpeechRate(0); // set speech speed rate
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e(TAG, "Language is not supported");
			} else {
				// speakOut();
			}
		} else {
			Log.e(TAG, "TTS Initilization Failed");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aand_dreader);
		Log.e(TAG, "P724 BT page started");
		// tts = new TextToSpeech(this, this);
		appClass  = (GlobalClass)getApplicationContext();
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = false;
		Button btnmanual = (Button) findViewById(R.id.manuval);
		// this.showInfo = (TextView) findViewById(R.id.textView1);
		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		// txtwelcom.setText("Please step on the scale.");
		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		rocketImage.setVisibility(View.INVISIBLE);
		txtReading.setVisibility(View.INVISIBLE);
		setwelcome();
		Animations();
		initpropertyvalue();
		start_connection();
		Constants.setPIN("1234");// set pin for auto pairing
		btnmanual.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Intent intent = new Intent(P724Bp.this, PressureEntry.class);
				finish();
				startActivity(intent);

			}
		});

		textView = new TextView(this);
		//stopService(new Intent(P724Bp.this, AdviceService.class));
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	public void start_connection() {

		TimerTask task = new TimerTask() {
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {

						Log.d("mg", "Send start()>>>>");

						initdialog();
						timer.cancel();
					}
				});
			}
		};
		timer = new Timer();
		timer.schedule(task, 10000, 350);

	}

	private void setwelcome() {
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){
			
			Util.soundPlay("Messages/TakeBP_1.wav");
		}else{
			
			Util.soundPlay("Messages/TakeBP.wav");
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		// tts.speak(welcome, TextToSpeech.QUEUE_FLUSH, null);
	}

	private void Animations() {
		txtwelcom.setText(this.getString(R.string.enterpressurebluetooth));
		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
		AnimPlz.setDuration(1000);
		AnimPlz.start();
		AnimPlz.addListener(new AnimatorListenerAdapter() {
			public void onAnimationEnd(Animator animation) {

				Log.e("", "AnimPlz");

				txtReading.setVisibility(View.VISIBLE);
				rocketImage.setVisibility(View.VISIBLE);

			}
		});
	}

	private void initpropertyvalue() {
		mChatService = new BluetoothServiceP724Bp(getApplicationContext(),
				myHandler);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		filter.addAction(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothDevice.ACTION_NAME_CHANGED);
		filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
		// filter.addAction(PAIRING);
		filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
		registerReceiver(mBtChangeReceiver, filter);
	}

	private long startT;

	private void initdialog() {
		if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
			mBluetoothAdapter.enable();
		}
		if (!mBluetoothAdapter.isDiscovering()) {
			mBluetoothAdapter.startDiscovery();
			Log.e("mg", "---> start");
		}
		// showInfo.setText("start");
		startT = System.currentTimeMillis();
	}

	private BroadcastReceiver mBtChangeReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action == null)
				return;
			if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
				Log.e("msg", "time: finished..."
						+ (System.currentTimeMillis() - startT));
			} else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
				int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
						BluetoothAdapter.ERROR);
				if (state == BluetoothAdapter.STATE_ON) {
					if ((!mBluetoothAdapter.isDiscovering())) {
						mBluetoothAdapter.startDiscovery();
					}
				} else if (state == BluetoothAdapter.SCAN_MODE_CONNECTABLE) {
					//Log.e("msg", "time: connet..." + System.currentTimeMillis());
				}
			} else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
				final BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				switch (device.getBondState()) {
				case BluetoothDevice.BOND_BONDING:
					break;
				case BluetoothDevice.BOND_BONDED:
					break;
				case BluetoothDevice.BOND_NONE:
				default:
					break;
				}
			} else if ((BluetoothDevice.ACTION_FOUND.equals(action))) {
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				String name = device.getName();
				String address = device.getAddress();
				//Log.e("msg", "time: found..." + name + "==>" + address);
				if (name != null && name.startsWith("MUMU-BP")) {
					mBluetoothAdapter.cancelDiscovery();
					mChatService.connect(device);
					return;
				}
			} else if (BluetoothDevice.ACTION_NAME_CHANGED.equals(action)) {
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				String name = device.getName();
				String address = device.getAddress();
				//Log.e("msg", "time: name change....." + name + "==>" + address);
				if (name != null && name.startsWith("MUMU-BP")) {
					mBluetoothAdapter.cancelDiscovery();
					mChatService.connect(device);
					return;
				}
			}
		}
	};

	private Handler myHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				switch (msg.arg1) {
				case BluetoothServiceP724Bp.STATE_CONNECTED:
					// showInfo.setText("now connected to a remote device");
					// getDevInfo();
					getBattery();
					doStartMeasure();
					break;
				case BluetoothServiceP724Bp.STATE_CONNECTING:
					// showInfo.setText("now initiating an outgoing");
					break;
				case BluetoothServiceP724Bp.STATE_LISTEN:
					// showInfo.setText("now listening for incoming");
					initdialog();
					break;
				case BluetoothServiceP724Bp.STATE_NONE:
					// showInfo.setText("we're doing nothing");
					break;
				}
				break;
			case MESSAGE_BATTERY:
				batteryInfo = (BatteryInfo) msg.obj;
				//Log.e("mg", "batteryInfo: " + batteryInfo.value);
				break;
			case MESSAGE_DEVICE:
				deviceInfo = (DeviceInfoBean) msg.obj;
				//Log.e("mg", "batteryInfo: " + deviceInfo.getDeviceId());
				break;
			case MESSAGE_WRITE:
				break;
			case MESSAGE_OK:
				// batt_info = 1;
				break;
			case MESSAGE_READ:
				int str[] = (int[]) msg.obj;
				measureIng(str[0], str[1], str[2], str[3]);
				break;
			case MESSAGE_RESULT:
				str = (int[]) msg.obj;
				measureOver(str[0], str[1], str[2], str[3]);
				break;
			case MESSAGE_ERRER:
				stopError();
				break;
			case MESSAGE_DEVICE_NAME:
				break;
			case MESSAGE_TOAST:
				initdialog();
				Log.e("mg", "===> lost connet...");
				break;
			}
		};
	};

	public void doStartMeasure() {

		// getDevInfo();
		startMeasure();
		TimerTask taskcc = new TimerTask() {
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {
						getReuslt();
						// startMeasure();
						if (bt_view_cnt >= mTempDelayCnt) {
							Log.d("mg",
									"Send Get Battery Info Command>>>>>>>>>>");
							// getBattery();
							bt_view_cnt = 0;
						} else {
							bt_view_cnt++;
						}
					}
				});
			}
		};
		timer = new Timer();
		timer.schedule(taskcc, 1000, 350);
	}

	private void measureIng(final int g_press_value, final int g_lp_value,
			final int g_hr_value, final int g_nibp_state) {
		if (((g_nibp_state & 0x3F) != 0)
				|| ((g_press_value == 179) && (g_lp_value == 179) && (g_hr_value == 179))
				|| ((g_press_value >= 200) || (g_lp_value >= 200)
						&& (g_hr_value >= 200))) {
			stopError();
			return;
		}
	}

	private void stopError() {
		if (timer != null)
			timer.cancel();
		stopMeasure();
	}

	private void measureOver(final int g_hp_value, final int g_lp_value,
			final int g_hr_value, final int g_nibp_state) {
		if (timer != null)
			timer.cancel();
		if (((g_nibp_state & 0x3F) != 0)
				|| ((g_hp_value == 179) && (g_lp_value == 179) && (g_hr_value == 179))
				|| ((g_hp_value >= 200) || (g_lp_value >= 200)
						&& (g_hr_value >= 200))) {
			return;
		}

		print = "systolic " + g_hp_value + " diastolic  " + g_lp_value
				+ " pulse  " + g_hr_value;
		//Log.d(TAG, "your blood pressure is" + print);

		print = g_hp_value + "," + g_lp_value + "," + g_hr_value;

		if ((g_hp_value > 0) && (g_lp_value > 0) && (g_hr_value > 0)) {

			if (g_hp_value < 999 && g_lp_value < 999 && g_hr_value < 999) {

				save(print);
			}
		}

	}

	private void startMeasure() {
		byte[] cmd = { (byte) 0xFA, 0x20, 0x00, (byte) 0xE6, (byte) 0xFA };
		sendMessage(cmd, 5);
	}

	private void stopMeasure() {
		byte[] cmd = { (byte) 0xFA, 0x20, 0x01, (byte) 0xE5, (byte) 0xFA };
		sendMessage(cmd, 5);
	}

	private void getDevInfo() {
		byte[] cmd = { (byte) 0xFA, 0x01, 0x05, (byte) 0xFA };
		sendMessage(cmd, 4);
	}

	private void getBattery() {
		byte[] cmd = { (byte) 0xFA, 0x02, 0x04, (byte) 0xFA };
		sendMessage(cmd, 4);
	}

	private void getReuslt() {
		byte[] cmd = { (byte) 0xFA, 0x21, (byte) 0xE5, (byte) 0xFA };
		sendMessage(cmd, 4);
	}

	/**
	 * Sends a message.
	 * 
	 * @param message
	 *            A string of text to send.
	 */
	private void sendMessage(byte[] message, int len) {
		if (mChatService.getState() != BluetoothServiceP724Bp.STATE_CONNECTED) {
			return;
		}
		if (len > 0) {
			mChatService.write(message, 0, len);
		}
	}

	private void save(String result) {
		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = flow.getInt("flow", 0); // #1
		if (val == 1) {
			
			SharedPreferences section = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);
			SharedPreferences.Editor editor_retake = section.edit();
			editor_retake.putString("sectiondate",Util.get_patient_time_zone_time(this));
			editor_retake.commit();
		}
		

		String ttmessage = "";
		String message = result;
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.USER_SP, 0);
		int PatientIdDroid = Integer.parseInt(settings1.getString("patient_id",
				"-1"));
		

		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.USER_TIMESLOT_SP, 0);
		String slot = settings.getString("timeslot", "AM");

		String[] str = null;
		str = print.split(",");
		ClasssPressure pressure = new ClasssPressure();
		pressure.setPatient_Id(PatientIdDroid);

		pressure.setSystolic(Integer.parseInt(str[0]));
		pressure.setDiastolic(Integer.parseInt(str[1]));
		pressure.setPulse(Integer.parseInt(str[2]));
		pressure.setInputmode(0);
		pressure.setTimeslot(slot);
		
		SharedPreferences settings2 = getSharedPreferences(
				CommonUtilities.PREFS_NAME_date, 0);		
		String sectiondate=settings2.getString("sectiondate", "0");
		pressure.setSectionDate(sectiondate);
		pressure.setPrompt_flag("4");
		lastinsert_id = dbcreatepressure.InsertPressure(pressure);
		Intent intent = new Intent(P724Bp.this, ShowPressureActivity.class);
		sys = "" + str[0];
		dia = "" + str[1];
		pul = "" + str[2];

		intent.putExtra("sys", sys);
		intent.putExtra("dia", dia);
		intent.putExtra("pulse", pul);
		intent.putExtra("pressureid", lastinsert_id);
		// Log.e(TAG, "redirecting from a and bp to show");
		/*
		 * try { mBluetoothService.stop(); } catch (Exception ec) {
		 * 
		 * }
		 */
		startActivity(intent);
		P724Bp.this.finish();

	}

	@Override
	protected void onResume() {

		Log.i(TAG,
				"-----###------------onResume------##--------  p724 bp activity");
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
		registerReceiver(mybroadcast, filter);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		unregisterReceiver(mBtChangeReceiver);
		unregisterReceiver(mybroadcast);
		super.onPause();

	}

	@Override
	public void onStop() {

		Log.i(TAG, "-------------------onstop--------------  p724 bp activity");

		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");

		// this.finish();
		try {
			if (mChatService != null) {
				mChatService.stop();
				Log.e(TAG, "mChatService.stoped ");
			}
		} catch (Exception e) {
			Log.e(TAG, "mChatService.stop() failed ");

		}

		/*if (!isMyAdviceServiceRunning()) {

			Intent schedule = new Intent();
			schedule.setClass(P724Bp.this, AdviceService.class);
			startService(schedule); // Starting Advice Service

		}*/
		
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		if (timer != null)
			timer.cancel();
		super.onStop();
	}
	
	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
}
