package com.tkm.optumSierra;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import com.tkm.optumSierra.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.tkm.optumSierra.dal.AdviceMessage_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.ConnectionDetector;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Util;

public class PopActivity extends Activity{

	private String serverurl = "";
	//private TextToSpeech tts;
	private String TAG = "PopActivity Sierra";
	private GlobalClass appClass;
	Cursor adviceMessageCursor;
//	String[] desc_message, data_subject, date_userid, data_patientid,
//			data_date = new String[] {};
	TextView txtadvice;
	//int[] Advice_id, userid, patientid, status;
	AdviceMessage_db dbcreate = new AdviceMessage_db(this);

//	public void onInit(int status) {
//
//		int result = 0;
//		if (status == TextToSpeech.SUCCESS) {
//
//			int language = Constants.getLanguage();
//
//			if (language == 11) {
//
//				Locale locSpanish = new Locale("es");
//				result = tts.setLanguage(locSpanish);
//				tts.setSpeechRate(0);
//
//			} else {
//
//				result = tts.setLanguage(Locale.US);
//				Log.i(TAG, "Initilization Success");
//				tts.setSpeechRate(0); // set speech speed rate
//
//			}
//			if (result == TextToSpeech.LANG_MISSING_DATA
//					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
//				Log.e(TAG, "Language is not supported");
//			} else {
//				// speakOut();
//			}
//		} else {
//			Log.e(TAG, "Initilization Failed");
//		}
//		tts.speak(adviceMessageCursor.getString(4), TextToSpeech.QUEUE_FLUSH, null);
//		//SharedPreferences sharedPref = getSharedPreferences("advice_message",0);		
//		//tts.speak(sharedPref.getString("message", ""), TextToSpeech.QUEUE_FLUSH, null);
//	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_pop);
		appClass = (GlobalClass) getApplicationContext();
		//tts = new TextToSpeech(this, this);

		txtadvice = (TextView) findViewById(R.id.advice);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		txtadvice.setTypeface(type, Typeface.NORMAL);

		//SharedPreferences sharedPref = getSharedPreferences("advice_message",0);		
		//txtadvice.setText(sharedPref.getString("message", ""));

		// tts.speak(extras.getString("message"), TextToSpeech.QUEUE_FLUSH,
		// null);

		Log.i(TAG, "on create Advice Message shown Sierra ...");

		serverurl = Constants.getPostUrl();
		adviceMessageCursor = dbcreate.SelectAdviceMessageForDisplay();
		Log.i("cursorCount:   ", ""+adviceMessageCursor.getCount());
		if (adviceMessageCursor.getCount() > 0) {
			adviceMessageCursor.moveToFirst();
			showAdviceMessage();
		}
//		UpdateAdviceMessage();

		Button btnmanual = (Button) findViewById(R.id.exit);

		btnmanual.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				adviceMessageCursor.moveToNext();
				showAdviceMessage();
				
//				SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
//				int val = flowsp.getInt("flow", 0); 
//				if(val == 1){
//
//					Intent intentSc = new Intent(getApplicationContext(),
//							Home.class);
//					finish();
//					startActivity(intentSc);
//					overridePendingTransition(0, 0);
//				}else{
//					
//					Intent intent = new Intent(getApplicationContext(), Welcome_Activity.class);
//					finish();
//					startActivity(intent);
//					overridePendingTransition(0, 0);
//				}
			
				
				
//				Intent intent = new Intent(PopActivity.this,
//						Welcome_Activity.class);
//				startActivity(intent);
//				finish();
//				overridePendingTransition(0, 0);
			}
		});

		Log.i(TAG, "-------------------oncreate--------------  popup  activity");
	}
	
	private void showAdviceMessage(){
		
		if(adviceMessageCursor.isAfterLast() == false){
			
			dbcreate.UpdateAdviceMessageById(Integer.parseInt(adviceMessageCursor.getString(8)));
			txtadvice.setText(adviceMessageCursor.getString(4));
			appClass.tts.speak(adviceMessageCursor.getString(4), TextToSpeech.QUEUE_FLUSH, null);
			
			
		}else{
			UpdateAdviceMessage();
			adviceMessageCursor.close();
			SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
			int val = flowsp.getInt("flow", 0); 
			if(val == 1){

				Intent intentSc = new Intent(getApplicationContext(),
						Home.class);
				finish();
				startActivity(intentSc);
				overridePendingTransition(0, 0);
			}else{
				Log.e("********************"+TAG, "*********************Welcome_Activity**************");
				Intent intent = new Intent(getApplicationContext(), Welcome_Activity.class);
				finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			}
		}
	}

	@Override
	public void onStop() {
		super.onStop();

//		if (appClass.tts != null) {
//			appClass.tts.stop();
//			appClass.tts.shutdown();
//		}

		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
	}

//	@Override
//	protected void onRestart() {
//		// TODO Auto-generated method stub
//		super.onRestart();
//
//		tts = new TextToSpeech(this, this);
//
//		Bundle extras = getIntent().getExtras();
//		if (extras != null) {
//			appClass.setBundle(extras);
//		} else {
//			extras = appClass.getBundle();
//		}
//		tts.speak(extras.getString("message"), TextToSpeech.QUEUE_FLUSH, null);
//
////		Log.e(TAG, "onRestart" + extras.getString("message"));
//	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		Log.e(TAG, "onPause");

//		if (appClass.tts != null) {
//			appClass.tts.stop();
//			appClass.tts.shutdown();
//		}
	}

	/***************************************************************** update advicemessage to server start ****************************************************/
	public void UpdateAdviceMessage() {
	
			
			Update taskUpdate = new Update();
			taskUpdate.execute(new String[] { serverurl
					+ "/droid_website/handler.ashx" });		
		

	}

	private class Update extends AsyncTask<String, Void, String> {
		
		protected String doInBackground(String... urls) {
			String response = "";

			try {

				Cursor c = dbcreate.SelectAdviceMessageForupload();
				
				c.moveToFirst();
				if (c.getCount() > 0) {
					while (c.isAfterLast() == false) {
						Log.i(TAG,
								"Uploading advice message to server started ...");

						HttpClient client = new DefaultHttpClient();
						Log.i(TAG, "UploadAdviceMessage url ..." + serverurl
								+ "/droid_website/rm_feedback.ashx");
						HttpPost post = new HttpPost(serverurl
								+ "/droid_website/rm_feedback.ashx");
						List<NameValuePair> pairs = new ArrayList<NameValuePair>();

						post.setEntity(new UrlEncodedFormEntity(pairs));
						post.setHeader("mode", "1");
						post.setHeader("user_id", c.getString(5));
						// Log.i("Droid","Uploading user_id ..."+c.getString(2));
						post.setHeader("patient_id", c.getString(2));
						post.setHeader("advice_id", c.getString(8));
						// Log.i("Droid","Uploading create_date ..."+c.getString(1));
						HttpResponse response1 = client.execute(post);

						String str = Util.readStream(response1.getEntity()
								.getContent());

						str.toString();
						if (str.length() > 0) {
							DocumentBuilder db = DocumentBuilderFactory
									.newInstance().newDocumentBuilder();
							InputSource is1 = new InputSource();
							is1.setCharacterStream(new StringReader(str
									.toString()));
							Document doc = db.parse(is1);
							NodeList nodes = doc
									.getElementsByTagName("success_response");
							if (nodes.getLength() == 1) {
								// /// if success delete the value from the
								// tablet db
								Log.i(TAG,
										"Deleted adviceid ..." + c.getString(8));
								if (Integer.parseInt(c.getString(7)) == 1) {
									dbcreate.DeleteMessageByIdAfterAcknowldgeSuccess(Integer
											.parseInt(c.getString(8)));
								} else if (Integer.parseInt(c.getString(7)) == 3) {
									dbcreate.Delete(Integer.parseInt(c
											.getString(8)));
								}
							}

						}
						c.moveToNext();

					}
					c.close();
					dbcreate.cursorAdviceMessage.close();

				}
			} catch (Exception e) {
				String Error = this.getClass().getName()+ " : "+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "feedback Message Status Update Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
			}
			// DownloadAdviceMessage();
			return response;
		}

	}

}
