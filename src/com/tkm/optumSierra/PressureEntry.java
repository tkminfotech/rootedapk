package com.tkm.optumSierra;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.tkm.optumSierra.bean.ClasssPressure;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.CustomKeyboard;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import com.tkm.optumSierra.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class PressureEntry extends Titlewindow {
	Pressure_db dbcreatepressure = new Pressure_db(this);

	private TextToSpeech tts;
	int pressureId;
	private String TAG = "PressureEntry";
	Dialog dialog;
	CustomKeyboard mCustomKeyboard;
	TextView oxygenText, percent, pulseText, pulseMessage;
	private View pview_back;
private GlobalClass appClass;
	@SuppressLint("HandlerLeak")
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			int PatientIdDroid = Constants.getdroidPatientid();

			EditText edtsystolic = (EditText) findViewById(R.id.edtsys);
			EditText edtdiastolic = (EditText) findViewById(R.id.edtdia);
			EditText edtupulse = (EditText) findViewById(R.id.edtpulse);
			if ((edtsystolic.getText().toString()).length() >= 1
					&& (edtdiastolic.getText().toString()).length() >= 1
					&& (edtupulse.getText().toString()).length() >= 1
					&& !(edtsystolic.getText().toString()).equals("0")
					&& !(edtsystolic.getText().toString()).equals("0.0")
					&& !(edtsystolic.getText().toString()).equals("00")
					&& !(edtsystolic.getText().toString()).contains(".")
					&& !(edtdiastolic.getText().toString()).equals("0")
					&& !(edtdiastolic.getText().toString()).equals("0.0")
					&& !(edtdiastolic.getText().toString()).equals("00")
					&& !(edtdiastolic.getText().toString()).contains(".")
					&& !(edtupulse.getText().toString()).equals("0")
					&& !(edtupulse.getText().toString()).equals("0.0")
					&& !(edtupulse.getText().toString()).equals("00")
					&& !(edtupulse.getText().toString()).equals(".0")
					&& !(edtupulse.getText().toString()).contains(".")
					&& !(edtupulse.getText().toString().length()>3)		
					&& !(edtsystolic.getText().toString().length()>3)	
					&& !(edtdiastolic.getText().toString().length()>3)	
					
					
					) {
				SharedPreferences flow = getSharedPreferences(
						CommonUtilities.USER_FLOW_SP, 0);
				int val = flow.getInt("flow", 0); // #1
				if (val == 1) {
					
					SharedPreferences section = getSharedPreferences(
							CommonUtilities.PREFS_NAME_date, 0);
					SharedPreferences.Editor editor_retake = section.edit();
					editor_retake.putString("sectiondate",Util.get_patient_time_zone_time(PressureEntry.this));
					editor_retake.commit();
				}
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.USER_TIMESLOT_SP, 0);
				String slot = settings.getString("timeslot", "AM");
				
				ClasssPressure pressure = new ClasssPressure();
				pressure.setPatient_Id(PatientIdDroid);
				pressure.setSystolic(Integer.parseInt(edtsystolic.getText()
						.toString()));
				pressure.setDiastolic(Integer.parseInt(edtdiastolic.getText()
						.toString()));
				pressure.setPulse(Integer.parseInt(edtupulse.getText()
						.toString()));
				pressure.setInputmode(1);
				pressure.setTimeslot(slot);
				Log.e(TAG, "Slot >>>"+slot);
				
				SharedPreferences settings1 = getSharedPreferences(
						CommonUtilities.PREFS_NAME_date, 0);		
				String sectiondate=settings1.getString("sectiondate", "0");
				pressure.setSectionDate(sectiondate);
				pressure.setPrompt_flag("4");
				Log.e(TAG, "Pressure value saving to db");
				pressureId = dbcreatepressure.InsertPressure(pressure);
				/*Toast.makeText(getApplicationContext(), "Saved Successfully",
						Toast.LENGTH_SHORT).show();*/

				Intent intent = new Intent(PressureEntry.this,
						ShowPressureActivity.class);
				intent.putExtra("sys", edtsystolic.getText().toString());
				intent.putExtra("dia", edtdiastolic.getText().toString());
				intent.putExtra("pulse", edtupulse.getText().toString());
				intent.putExtra("pressureid", pressureId);

				edtsystolic.setText("");
				edtdiastolic.setText("");
				edtupulse.setText("");
				Log.e(TAG, "Redirecting to show Pressure Page");
				startActivity(intent);
				finish();
				Util.stopPlaySingle();
				overridePendingTransition(0, 0);

			} else {
				Log.e(TAG, "Invalid Pressure value entered by user");
				invalid_popup();
				
				return;
			}

			// insert function

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pressure_entry);
		appClass  = (GlobalClass)getApplicationContext();
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = true;
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		pview_back = inflater
				.inflate(R.layout.activity_pressure_entry, null);
		

		mCustomKeyboard = new CustomKeyboard(this, R.id.keyboardview,
				R.xml.hexkbd, mHandler);

		mCustomKeyboard.registerEditText(R.id.edtsys);
		mCustomKeyboard.registerEditText(R.id.edtdia);
		mCustomKeyboard.registerEditText(R.id.edtpulse);
		
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){
			
			Util.soundPlay("Messages/EnterBP_1.wav");
		}else{
			
			Util.soundPlay("Messages/EnterBP.wav");
		}
		

	
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);

			this.finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}
	private void popup()
	{

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		dialog = adb.setView(new View(this)).create();
		// (That new View is just there to have something inside the dialog that can grow big enough to cover the whole screen.)

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = 790;
		lp.height = 500;
		dialog = new Dialog(this, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.popup_messages);
		
		TextView msg = (TextView) dialog
				.findViewById(R.id.textpopup);
		TextView mac = (TextView) dialog
				.findViewById(R.id.txtmac);	

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);		
		msg.setText(this.getString(R.string.entervalidpressure));
		Log.e(TAG, "invalid pressure measurement");
		mac.setTypeface(type, Typeface.NORMAL);		
		mac.setVisibility(View.INVISIBLE);
		
		dialog.show();
		dialog.getWindow().setAttributes(lp);		
		closeversion = (Button) dialog.findViewById(R.id.exitversion);
		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				dialog.dismiss();
			}
		});

		
	}
	private void invalid_popup() {

		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);

		View pview;
		pview = inflater.inflate(R.layout.popup_messages, null);

		TextView msg = (TextView) pview.findViewById(R.id.textpopup);
		TextView mac = (TextView) pview.findViewById(R.id.txtmac);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);
		msg.setText(this.getString(R.string.entervalidpressure));

		mac.setTypeface(type, Typeface.NORMAL);
		mac.setVisibility(View.INVISIBLE);

		closeversion = (Button) pview.findViewById(R.id.exitversion);

		final PopupWindow cp = new PopupWindow(pview,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		cp.setBackgroundDrawable(new BitmapDrawable(getApplicationContext()
				.getResources()));
		cp.showAtLocation(pview_back, Gravity.CENTER, 0, 0);

		// cp.update(0, 0, 350, 350);
		//cp.update(0, 0, 750, 500);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = (displaymetrics.heightPixels);
		int width = displaymetrics.widthPixels/2;
		height=(int) (height/1.2);
		

		// cp.update(0, 0, 350, 350);
		//cp.update(0, 0, 750, 500);
		cp.update(0, 0,  width,height);

		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				cp.dismiss();
			}
		});

	}
	@Override
	public void onStop() {
		
		Util.WriteLog(TAG,Constants.getdroidPatientid()+"");	

		
		super.onStop();
	}


}
