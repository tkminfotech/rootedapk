package com.tkm.optumSierra;



import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import com.tkm.optumSierra.bean.ClasssPressure;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.GattByteBuffer;
import com.tkm.optumSierra.util.GattUtils;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;


import android.os.Bundle;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint; 
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.text.method.ScrollingMovementMethod;
import com.tkm.optumSierra.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class PressureWellex extends Titlewindow {

	// BluetoothLeService mBluetoothLeService;
	BluetoothManager manager;
	BluetoothGatt mBluetoothGatt;
	BluetoothAdapter mBluetoothAdapter;
	// LeScanCallback mLeScanCallback;
	ArrayAdapter<String> mLeDeviceListAdapter;
	// ListAdapter mListAdapter;
	BluetoothDevice deviceL;
	int lastinsert_id = 0;
	private int mConnectionState = STATE_DISCONNECTED;
	private static final int STATE_DISCONNECTED = 0;
	private static final int STATE_CONNECTING = 1;
	private static final int STATE_CONNECTED = 2;
	public String sys, dia, pulse;
	public final static String ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
	public final static String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
	public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
	public final static String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
	public final static String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";
	String print;
	Button b1;
	TextView txtwelcom, txtReading;
	ImageView rocketImage;
	AnimationDrawable rocketAnimation;
	ObjectAnimator AnimPlz, ObjectAnimator;
	Pressure_db dbcreatepressure = new Pressure_db(this);
	int flag=0;
	String macAddress = "";
	private int deviceType = -1;
	public boolean cancel_flag = false;
	public static final long leastSigBits = 0x800000805f9b34fbL;
	// service
	public static final UUID BLOOD_PRESSURE = new UUID(
			(0x1810L << 32) | 0x1000, leastSigBits);

	public static final UUID CHAR_BLOOD_PRESSURE_MEASUREMENT = new UUID(
			(0x2781L << 32) | 0x1000, leastSigBits);
	// char read

	public static final UUID CLIENT_CHARACTERISTIC_CONFIGURATION = new UUID(
			(0x2902L << 32) | 0x1000, leastSigBits);
	public final static UUID UUID_HEART_RATE_MEASUREMENT = UUID
			.fromString("46A970E0-0D5F-11E2-8B5E-0002A5D5C51B");

	public static final UUID BLOOD_PRESSURE_MEASUREMENT = new UUID(
			(0x2A35L << 32) | 0x1000, leastSigBits);
	public static final UUID BLOOD_PRESSURE_FEATURE = new UUID(
			(0x2A49L << 32) | 0x1000, leastSigBits);
	public static final UUID INTERMEDIATE_CUFF_PRESSURE = new UUID(
			(0x2A36L << 32) | 0x1000, leastSigBits);
	Button btnmanual;
	int a1,b11,c1;
	//	TextView text;
	private GlobalClass appClass;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pressure_wellex);
		appClass = (GlobalClass) getApplicationContext();
		btnmanual = (Button) findViewById(R.id.manuval);		
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = false;
		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");

		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);

		rocketImage = (ImageView) findViewById(R.id.loading);

		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		rocketImage.setVisibility(View.INVISIBLE);
		txtReading.setVisibility(View.INVISIBLE);

		Bundle extras = getIntent().getExtras();
		if(extras != null){
			appClass.setBundle(extras);
		}else{
			extras = appClass.getBundle();
		}
		macAddress = extras.getString("macaddress");
		setwelcome();
		Animations();

		manager = (BluetoothManager) getBaseContext().getSystemService(
				Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = manager.getAdapter();

		//text = (TextView) findViewById(R.id.textView1);

		//text.setTextSize(26);
		//text.setMovementMethod(new ScrollingMovementMethod());
		if (mBluetoothAdapter == null) {
			Toast.makeText(getBaseContext(), "no adapter found",
					Toast.LENGTH_SHORT).show();
			finish();
			return;
		}
		else{
			
			search();

		}



		addListenerOnButton();
		
	}

	private void addListenerOnButton() {
		btnmanual.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click




				Intent intentpl = new Intent(getApplicationContext(),
						PressureEntry.class);

				//mBluetoothGatt.close();

				finish();
				startActivity(intentpl);

			}
		});

	}

	private void setwelcome() {
		// TextView txtwelcom=(TextView)findViewById(R.id.t);
		// String welcome="Please check your pulse oximetry now";
		// txtwelcom.setText(welcome);


		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){
			
			Util.soundPlay("Messages/TakeBP_1.wav");
		}else{
			
			Util.soundPlay("Messages/TakeBP.wav");
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		// tts.speak(welcome, TextToSpeech.QUEUE_FLUSH, null);
	}
	private void Animations() {
		txtwelcom.setText(this.getString(R.string.enterpressurebluetooth));
		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
		AnimPlz.setDuration(1000);
		AnimPlz.start();

		AnimPlz.addListener(new AnimatorListenerAdapter() {
			public void onAnimationEnd(Animator animation) {

				Log.e("", "AnimPlz");

				txtReading.setVisibility(View.VISIBLE);
				rocketImage.setVisibility(View.VISIBLE);

			}
		});
	}
	public void onStop() {

		flag=1;
		super.onStop();
		
		mBluetoothAdapter.stopLeScan(mLeScanCallback);

		if (mBluetoothGatt == null) {
			// finish();
			return;
		}
		try{
			mBluetoothGatt.close();
		}catch(RuntimeException e){
			Log.e("Exception", ""+e);
		}
		
		mBluetoothGatt = null;

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mBluetoothAdapter.stopLeScan(mLeScanCallback);

		if (mBluetoothGatt == null) {
			// finish();
			return;
		}
		try{
			mBluetoothGatt.close();
		}catch(RuntimeException e){
			Log.e("Exception", ""+e);
		}
		mBluetoothGatt = null;
	}

	public void search() {
		boolean x = mBluetoothAdapter.startLeScan(mLeScanCallback);
		if (x) {
			Log.e("scan status", "true");

		} else {
			Log.e("scan status", "false");
		}

	}

	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
		@Override
		public void onLeScan(final BluetoothDevice device, int rssi,
				byte[] scanRecord) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Log.e("Foundddd", device.toString());

					deviceL = device;
					Log.e("mac id of wellex"+"'", device.toString());
					/*Toast.makeText(getBaseContext(),
							"device found " + device.toString(),
							Toast.LENGTH_SHORT).show();*/
					if (device.toString().toUpperCase().equals((String) (macAddress.toUpperCase()))){
						connect();
					}


				}
			});
		}
	};

	private void connect() {

		mBluetoothAdapter.stopLeScan(mLeScanCallback);
		mBluetoothGatt = deviceL.connectGatt(getBaseContext(), false,
				mGattCallback);
		Log.i("zzzzzzz", "connectttttttttt");

		// List<BluetoothGattCharacteristic> list =
		// service.getCharacteristics();

	}

	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {

			// String intentAction;
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				// intentAction = ACTION_GATT_CONNECTED;
				mConnectionState = STATE_CONNECTED;
				// broadcastUpdate(intentAction);
				Log.i("zzzzzzz", "Connected to GATT server.");
				Log.i("zzzzzzz", "Attempting to start service discovery:"
						+ mBluetoothGatt.discoverServices());

			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				// intentAction = ACTION_GATT_DISCONNECTED;
				mConnectionState = STATE_DISCONNECTED;
				Log.i("zzzzzzzzz", "Disconnected from GATT server.");
				// broadcastUpdate(intentAction);
			}

		}

		public void onServicesDiscovered(BluetoothGatt gatt, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS) {


				try {
					BluetoothGattService service = mBluetoothGatt
							.getService(BLOOD_PRESSURE);

					update(service);

				} catch (NullPointerException e) {
					Log.e("service error", "no uiiiid");
				}

				// BluetoothGattCharacteristic characteristic;
				// mBluetoothGatt.readCharacteristic(characteristic.)
			} else {
				Log.w("zzzzzzzz", "onServicesDiscovered received: " + status);
			}

		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			// TODO Auto-generated method stub
			broadcastUpdate(characteristic);
			super.onCharacteristicChanged(gatt, characteristic);
		}

		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// if (status == BluetoothGatt.GATT_SUCCESS) {
			broadcastUpdate(characteristic);
			Log.w("zzzzzzzz", "characteristic read");
			// }

		}
		// public void onCharacteristicChanged(BluetoothGatt gatt,
		// BluetoothGattCharacteristic) {}
	};

	public void update(BluetoothGattService service) {

		List<BluetoothGattCharacteristic> list = service.getCharacteristics();

		for (int i = 0; i < list.size(); i++) {
			Log.e("charactaristic", list.get(i).toString());
			mBluetoothGatt.readCharacteristic(list.get(i));

			if (list.get(i).getUuid().equals(INTERMEDIATE_CUFF_PRESSURE)) {
				Log.e("char  fouuuuuuund", list.get(i).getUuid().toString());
				mBluetoothGatt.setCharacteristicNotification(list.get(i), true);
				BluetoothGattDescriptor descriptor = list.get(i).getDescriptor(
						CLIENT_CHARACTERISTIC_CONFIGURATION);
				Log.e("dis fouuuuuuund", descriptor.toString());
				descriptor
				.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
				mBluetoothGatt.writeDescriptor(descriptor);
				// mBluetoothGatt.readCharacteristic(list.get(i));
			}

			if (list.get(i).getUuid().equals(BLOOD_PRESSURE_MEASUREMENT)) {
				Log.e("read char", "second if");
				mBluetoothGatt.setCharacteristicNotification(list.get(i), true);
				BluetoothGattDescriptor descriptor = list.get(i).getDescriptor(
						CLIENT_CHARACTERISTIC_CONFIGURATION);
				descriptor
				.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
				mBluetoothGatt.writeDescriptor(descriptor);

				// mBluetoothGatt.readCharacteristic(list.get(i));
			}

			if (list.get(i).getUuid().equals(BLOOD_PRESSURE_FEATURE)) {
				Log.e("read char", "third if");
				// BluetoothGattDescriptor descriptor =
				// list.get(i).getDescriptor(
				// CLIENT_CHARACTERISTIC_CONFIGURATION);

				// mBluetoothGatt.writeDescriptor(descriptor);

				mBluetoothGatt.readCharacteristic(list.get(i));
			}

		}
	}

	private void broadcastUpdate(
			final BluetoothGattCharacteristic characteristic) {
		// final Intent intent = new Intent(action);

		// This is special handling for the Heart Rate Measurement profile. Data
		// parsing is carried out as per profile specifications.
		// if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
		// Toast.makeText(getBaseContext(), "reading",Toast.LENGTH_LONG).show();
		// int flag = characteristic.getProperties();
		// int format = -1;
		// if ((flag & 0x01) != 0) {
		// / format = BluetoothGattCharacteristic.FORMAT_UINT16;
		// Log.d("format", "Heart rate format UINT16.");
		// } else {
		// format = BluetoothGattCharacteristic.FORMAT_UINT8;
		// Log.d("format", "Heart rate format UINT8.");
		// }
		// final int heartRate = characteristic.getIntValue(format, 1);
		// Log.d("rrrrr", String.format("Received heart rate: %d", heartRate));
		// intent.putExtra(EXTRA_DATA, String.valueOf(heartRate));
		// } else {
		// For all other profiles, writes the data formatted in HEX.

		byte[] data = characteristic.getValue();
		if (data != null && data.length > 0) {
			StringBuilder stringBuilder = new StringBuilder(" ");
			;
			// = new StringBuilder(data.length);
			Log.d("data", data.toString() + "  " + data.length);
			GattByteBuffer bb = GattByteBuffer.wrap(data);

			//byte flags = bb.getInt8();

			float a,b,c;



			a = GattUtils.getFloatValue(data, GattUtils.FORMAT_SFLOAT, 1);
			a1=(int) a;
			b = GattUtils.getFloatValue(data, GattUtils.FORMAT_SFLOAT, 3);
			b11=(int) b;
			c = GattUtils.getFloatValue(data, GattUtils.FORMAT_SFLOAT, 14 );
			c1=(int) c;
			// }
			print = a1+","+b11 +","+ c1;
			Log.d("sa", print);
			// print.

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					if(flag==0){


						if((a1>0) &&(b11>0)&&(c1>0) ){

							save(print);
						}



					}

				}

				private void save(String result) {

					SharedPreferences flow = getSharedPreferences(
							CommonUtilities.USER_FLOW_SP, 0);
					int val = flow.getInt("flow", 0); // #1
					if (val == 1) {						
						SharedPreferences section = getSharedPreferences(
								CommonUtilities.PREFS_NAME_date, 0);
						SharedPreferences.Editor editor_retake = section.edit();
						editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(PressureWellex.this));
						editor_retake.commit();
					}
					
					flag=1;

					String ttmessage = "";
					String message = result;
					SharedPreferences settings1 = getSharedPreferences(
							CommonUtilities.USER_SP, 0);
					int PatientIdDroid = Integer.parseInt(settings1.getString(
							"patient_id", "-1"));
					
					SharedPreferences settings = getSharedPreferences(
							CommonUtilities.USER_TIMESLOT_SP, 0);
					String slot = settings.getString("timeslot", "AM");
					
					String[] str = null;
					str = print.split(",");
					ClasssPressure pressure = new ClasssPressure();
					pressure.setPatient_Id(PatientIdDroid);

					pressure.setSystolic(Integer.parseInt(str[0]));
					pressure.setDiastolic(Integer.parseInt(str[1]));
					pressure.setPulse(Integer.parseInt(str[2]));
					pressure.setInputmode(0);
					pressure.setTimeslot(slot);
					
					SharedPreferences settings2 = getSharedPreferences(
							CommonUtilities.PREFS_NAME_date, 0);		
					String sectiondate=settings2.getString("sectiondate", "0");
					pressure.setSectionDate(sectiondate);
					pressure.setPrompt_flag("4");
					lastinsert_id = dbcreatepressure
							.InsertPressure(pressure);
					Intent intent = new Intent(PressureWellex.this,
							ShowPressureActivity.class);
					sys = "" + str[0];
					dia = "" + str[1];
					pulse = "" + str[2];

					intent.putExtra("sys", sys);
					intent.putExtra("dia", dia);
					intent.putExtra("pulse", pulse);
					intent.putExtra("pressureid", lastinsert_id);
					//	Log.e(TAG, "redirecting from a and bp to show");
					/*	try {
						mBluetoothService.stop();
					} catch (Exception ec) {

					}*/
					startActivity(intent);
					PressureWellex.this.finish();
					//ttmessage = str[0] + R.string.over + str[1] +this.getString(R.string.pulseis)
					//	+ str[2];
					//message = this.getString(R.string.syspressure) +" "+ str[0]
					//	+this.getString(R.string.diapressure)+ " " + str[1]
					//+this.getString(R.string.pulseequal)+ " " + str[2];	// TODO Auto-generated method stub

				}
			});


		}
	}

}
