package com.tkm.optumSierra;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import com.tkm.optumSierra.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.tkm.optumSierra.bean.ClassMeasurement;
import com.tkm.optumSierra.dal.Pulse_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.CustomKeyboard;
import com.tkm.optumSierra.util.Util;

public class PulseEntry extends Titlewindow {

	Pulse_db dbcreate1 = new Pulse_db(this);
	int pulseId;
	CustomKeyboard mCustomKeyboard;
	TextView oxygenText, percent, pulseText, pulseMessage;
	private String TAG = "PulseEntry";
	Dialog dialog;
	private View pview_back;
private GlobalClass appClass;
	@SuppressLint("HandlerLeak")
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			EditText edtpulse = (EditText) findViewById(R.id.pulsevalue);
			EditText edtoxy = (EditText) findViewById(R.id.oxygenvalue);

			if ((edtoxy.getText().toString()).length() >= 1
					&& (edtpulse.getText().toString()).length() >= 1
					&& !(edtoxy.getText().toString()).equals("000")
					&& !(edtoxy.getText().toString()).contains(".")
					&& !(edtoxy.getText().toString()).equals(".0")
					&& !(edtoxy.getText().toString()).equals("0.0")
					&& !(edtoxy.getText().toString()).equals("0")
					&& !(edtoxy.getText().toString()).equals("00")
					&& !(edtpulse.getText().toString()).equals("000")
					&& !(edtpulse.getText().toString()).equals("00")
					&& !(edtpulse.getText().toString()).equals(".0")
					&& !(edtpulse.getText().toString()).equals("0.0")
					&& !(edtpulse.getText().toString()).equals("0")
					&& !(edtpulse.getText().toString()).contains(".")
					&& !(edtpulse.getText().toString().length()>3)
					&& !(edtoxy.getText().toString().length()>3)
					
					
					) {
				SharedPreferences flow = getSharedPreferences(
						CommonUtilities.USER_FLOW_SP, 0);
				int val = flow.getInt("flow", 0); // #1
				if (val == 1) {					
					SharedPreferences section = getSharedPreferences(
							CommonUtilities.PREFS_NAME_date, 0);
					SharedPreferences.Editor editor_retake = section.edit();
					editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(PulseEntry.this));
					editor_retake.commit();
				}
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"MM/dd/yyyy hh:mm:ss aa");
				String currentDateandTime = dateFormat.format(new Date());
				ClassMeasurement measurement = new ClassMeasurement();

				measurement.setStatus(0);
				measurement.setOxygen_Value(Integer.parseInt(edtoxy.getText()
						.toString()));
				measurement.setLast_Update_Date(currentDateandTime);
				measurement.setPressure_Value(Integer.parseInt(edtpulse
						.getText().toString()));
				measurement.setInputmode(1);
				SharedPreferences settings1 = getSharedPreferences(
						CommonUtilities.PREFS_NAME_date, 0);		
				String sectiondate=settings1.getString("sectiondate", "0");
				
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.USER_TIMESLOT_SP, 0);
				String slot = settings.getString("timeslot", "AM");
				
				measurement.setSectionDate(sectiondate);
				measurement.setTimeslot(slot);
				
				pulseId = dbcreate1.InsertOxymeterMeasurement(measurement);
				Log.e(TAG, "Saving pulse value to db");
				// dbcreate1.InsertOxymeterMeasurement(100,classOxy.getOxygen(),
				// currentDateandTime, 0,classOxy.getOxyPulse());

				// PresureActivity();

				/*
				 * Toast.makeText(getApplicationContext(), "Saved Successfully",
				 * Toast.LENGTH_SHORT).show();
				 */

				Intent intent = new Intent(PulseEntry.this,
						ShowPulseActivity.class);
				intent.putExtra("pulse", edtoxy.getText().toString());
				intent.putExtra("pulseid", pulseId);
				edtoxy.setText("");
				edtpulse.setText("");
				Log.e(TAG, "Redirecting to show pulse page");
				startActivity(intent);
				finish();
				Util.stopPlaySingle();
				overridePendingTransition(0, 0);
			} else {
				Log.e(TAG, "invalid pulse entry by user");
				 invalid_popup();
				return;
			}
			// insert function

		}
	};

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pulse_entry);
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		pview_back = inflater
				.inflate(R.layout.activity_pulse_entry, null);
		appClass  = (GlobalClass)getApplicationContext();
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = true;
		mCustomKeyboard = new CustomKeyboard(this, R.id.keyboardview,
				R.xml.hexkbd, mHandler);
		oxygenText = (TextView) findViewById(R.id.oxygentext);
		percent = (TextView) findViewById(R.id.percent);
		pulseMessage = (TextView) findViewById(R.id.pulsemessage);
		pulseText = (TextView) findViewById(R.id.pulsetext);
		Typeface type1 = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		oxygenText.setTypeface(type1, Typeface.NORMAL);
		Typeface type2 = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Light.otf");
		percent.setTypeface(type2, Typeface.NORMAL);
		pulseText.setTypeface(type1, Typeface.NORMAL);
		pulseMessage.setTypeface(type1, Typeface.NORMAL);
		mCustomKeyboard.registerEditText(R.id.oxygenvalue);
		mCustomKeyboard.registerEditText(R.id.pulsevalue);

		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){
			
			Util.soundPlay("Messages/CheckBL-OXlevels_1.wav");
		}else{
			
			Util.soundPlay("Messages/CheckBL-OXlevels.wav");
		}
		

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);

			this.finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	private void popup() {

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		dialog = adb.setView(new View(this)).create();
		// (That new View is just there to have something inside the dialog that
		// can grow big enough to cover the whole screen.)

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = 790;
		lp.height = 500;
		dialog = new Dialog(this, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.popup_messages);

		TextView msg = (TextView) dialog.findViewById(R.id.textpopup);
		TextView mac = (TextView) dialog.findViewById(R.id.txtmac);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);
		msg.setText(this.getString(R.string.entervalidpulse));

		mac.setTypeface(type, Typeface.NORMAL);
		mac.setVisibility(View.INVISIBLE);

		dialog.show();
		dialog.getWindow().setAttributes(lp);
		closeversion = (Button) dialog.findViewById(R.id.exitversion);
		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				dialog.dismiss();
			}
		});

	}
	private void invalid_popup() {

		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);

		View pview;
		pview = inflater.inflate(R.layout.popup_messages, null);

		TextView msg = (TextView) pview.findViewById(R.id.textpopup);
		TextView mac = (TextView) pview.findViewById(R.id.txtmac);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);
		msg.setText(this.getString(R.string.entervalidpulse));

		mac.setTypeface(type, Typeface.NORMAL);
		mac.setVisibility(View.INVISIBLE);

		closeversion = (Button) pview.findViewById(R.id.exitversion);

		final PopupWindow cp = new PopupWindow(pview,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		cp.setBackgroundDrawable(new BitmapDrawable(getApplicationContext()
				.getResources()));
		cp.showAtLocation(pview_back, Gravity.CENTER, 0, 0);

		// cp.update(0, 0, 350, 350);
		//cp.update(0, 0, 750, 500);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = (displaymetrics.heightPixels);
		int width = displaymetrics.widthPixels/2;
		height=(int) (height/1.2);
		

		// cp.update(0, 0, 350, 350);
		//cp.update(0, 0, 750, 500);
		cp.update(0, 0,  width,height);

		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				cp.dismiss();
			}
		});

	}
	@Override
	public void onStop() {

		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");

		super.onStop();
	}

}
