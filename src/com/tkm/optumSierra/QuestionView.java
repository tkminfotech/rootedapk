package com.tkm.optumSierra;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;


import com.tkm.optumSierra.bean.ClassQuestion;
import com.tkm.optumSierra.bean.ClassWeight;
import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.dal.Question_Check_db;
import com.tkm.optumSierra.dal.Questions_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.Typeface;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import com.tkm.optumSierra.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

public class QuestionView extends Titlewindow implements OnClickListener,
OnInitListener {
	MeasureType_db dbsensor1=new MeasureType_db(this);
	Questions_db dbcreate1 = new Questions_db(this);
	Question_Check_db dbQuestion_Check = new Question_Check_db(this);
	private Vector<Node> nodeList = new Vector<Node>();
	private String TAG = "QuestionView Sierra";
	protected static final int RESULT_SPEECH = 1;
	private ProgressDialog progressDialog = null;
	private int treeNo = 0;
	private String nodeNo = "";
    public int page_load=1;
	int[] treeList = null;
	String[] treeName = null;
	String[] topNodeNo = null;
	Node currentNode = null;

	Button backButton = null;
	int patientId = 0;
	boolean firstTime = true;

	private TextToSpeech tts;
	MediaPlayer mediaPlayer;

	private int stateMediaPlayer;
	private int count = 0;
	private String qPath = "";
	private final int stateMP_NotStarter = 1;
	private final int stateMP_Playing = 2;
	private final int stateMP_Pausing = 3;
	int ir_is_reminder=0;
	private GlobalClass appClass;
	public void onInit(int status) {

		int result=0;
		if (status == TextToSpeech.SUCCESS) {

			int language=Constants.getLanguage();

			if(language==11){

				Locale locSpanish = new Locale("es");
				result=tts.setLanguage(locSpanish);
				tts.setSpeechRate(0);

			}

			else{

				result = tts.setLanguage(Locale.US);
				Log.i(TAG, "Initilization Success");
				tts.setSpeechRate(0);
			}

			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e(TAG, "Language is not supported");
			}
		} else {
			Log.e(TAG, "Initilization Failed");
		}
		//starting();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_question_view);
		appClass  = (GlobalClass)getApplicationContext();
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = true;
		tts = new TextToSpeech(this, this);
		firstTime = false;
		//setcount();
		SharedPreferences flowsp = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		 ir_is_reminder = flowsp.getInt("is_reminder_flow", 0);
		 dbsensor1.updateStatus(101);
	}

	@Override
	protected void onDestroy() {
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		super.onDestroy();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);

			this.finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	protected void starting() {
		Log.i(TAG, "ON  starting .........");

		if (currentNode == null) {

			loadTreeList();
			if (topNodeNo.length > 0) {
				nodeNo = topNodeNo[0];
				treeNo = treeList[0];
				currentNode = getNode(treeNo, nodeNo);
				nodeList.add(currentNode); // add to a vector for back_tracking
				// #
				showTree(currentNode);

			} else {
				/*createCancelProgressDialog(this.getString(R.string.noquestionmessage),
					this.getString(R.string.noquestionforthistime));*/

				Intent intent = new Intent(this, Questions.class);
				finish();
				startActivity(intent);
				overridePendingTransition(0, 0);

				// tts.speak("You have no Question  for this time",
				// TextToSpeech.QUEUE_FLUSH, null);

			}
			// group question #
		} else {

			showTree(currentNode);
		}
	}

	private void createCancelProgressDialog(String title, String message) {
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle(title);
		progressDialog.setMessage(message);
		progressDialog.setCancelable(false);
		progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, this.getString(R.string.OK),
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// code
				dialog.dismiss();
				finish();
				Intent intent = new Intent(QuestionView.this,
						ThanksActivity.class);
				startActivity(intent);
			}
		});
		progressDialog.show();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	public void onStart() {

		super.onStart();
		starting();

	}

	public Node getNode(int questionId, String nodeNo) {
		// set to Node class

		int node_type = 0; // 0 : question, 1 : Message
		String sequence = ""; // node_no
		String question_message = ""; //
		String[] choices = null; //
		int[] switchTo = null; //
		String message = "";
		String Switchmode = "";
		String currentDate = TestDate.getCurrentTime() + "";
		String query = "select  a.Sequence_Number, b.kind, b.Explanation, b.Guidance, a.Child_node_Number"
				+ " from Droid_DMP_Branching a,Droid_DMP_Questions b "
				+ "where a.Question_Id="
				+ questionId
				+ " and a.Node_Number ="
				+ nodeNo + " and b.Sequence_Number= a.Sequence_Number";

		query.toString();

		Cursor questions = dbcreate1.SelectDBValues(query);

		switchTo = new int[questions.getCount()];
		questions.moveToFirst();
		int k = 0;
		while (questions.isAfterLast() == false) {

			node_type = Integer.parseInt(questions.getString(1));
			sequence = questions.getString(0);
			question_message = questions.getString(2);
			message = questions.getString(3);
			if (questions.getString(4).length() != 0) {

				switchTo[k] = Integer.parseInt(questions.getString(4));

			} else {
				switchTo[k] = 0;
			}


			questions.moveToNext();
			k++;
		}

		String Answer = " select   a.Node_Number, a.Child_Node_Number, b.Item_Content, a.Sequence_Number,b.Item_Number"
				+ " from Droid_DMP_Branching  a inner join  Droid_DMP_Answers  b on  a.Sequence_Number = b.Sequence_Number and a.Item_Number=b.Item_number"
				+ " where  a.Sequence_Number ="
				+ sequence
				+ " and a.Node_Number=" + nodeNo + " order by a.Item_Number";

		Cursor Ans = dbcreate1.SelectDBValues(Answer);

		choices = new String[Ans.getCount()];

		Ans.moveToFirst();

		int j = 0;
		while (Ans.isAfterLast() == false) {

			choices[j] = Ans.getString(2);

			Ans.moveToNext();
			j++;

		}

		if (node_type == 5) {
			question_message = message;
			choices = new String[] { "OK" };

		}
		// switchTo = new int[] { 3 }; // next node : 0 represents END

		String CurrentDate = TestDate.getCurrentTime() + "";

		Node node = new Node(node_type, sequence, question_message, choices,
				switchTo, CurrentDate);

		questions.close();
		Ans.close();
		dbcreate1.cursorQuestions.close();
		dbcreate1.closeopendbConnection();
		return node;

		// return null; // if there is no successor node or error #
	}

	public String getDate() {

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"MM/dd/yyyy hh:mm:ss aa");
		String currentDateandTime = dateFormat.format(new Date());
		return currentDateandTime;
	}

	private void setcount() {
		String Qname = dbcreate1.SelectQuestainName();
		Log.i(TAG, Qname);
		if (Qname.equals("Question Set 2")) {
			count = 16;
		} else if (Qname.equals("Question Set 1")) {
			count = 1;
		} else if (Qname.equals("Sierra Question Set 1")) {
			count = 1;
			qPath = "Sierra/";
		} else {
			count = -50;
		}
	}

	public int[] loadTreeList() {
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.PREFS_NAME_date, 0);		
		String sectiondate=settings1.getString("sectiondate", "0");
		Cursor c = dbcreate1.SelectDMP();

		treeList = new int[c.getCount()];
		treeName = new String[c.getCount()];
		topNodeNo = new String[c.getCount()];
		c.moveToFirst();

		if (c.isAfterLast() == false) {
			patientId = Integer.parseInt(c.getString(0));
		}

		int i = 0;

		while (c.isAfterLast() == false) {

			treeList[i] = Integer.parseInt(c.getString(1));
			treeName[i] = c.getString(2);
			topNodeNo[i] = c.getString(3);
			c.moveToNext();
			i++;
		}
		c.close();
    if(topNodeNo.length>0)
    {
		if(topNodeNo[0].equals("0")){


			SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
			int flow_type = flowsp.getInt("is_reminder_flow", 0);

			if (flow_type == 1)// skip for reminder
			{
				page_load=0;
			}
			String query = "select Sequence_Number, kind, Explanation"
					+ " from Droid_DMP_Questions where Sequence_Number in(select Sequence_Number from Droid_DMP_Branching where Question_Id=" + treeList[0]+")";

			query.toString();

			Cursor questions = dbcreate1.SelectDBValues(query);

			questions.moveToFirst();

			while (questions.isAfterLast() == false) {

				int node_type = Integer.parseInt(questions.getString(1));
				String sequence = questions.getString(0);
				String question_message = questions.getString(2);
				String CurrentDate = TestDate.getCurrentTime() + "";

				
				dbcreate1.InsertDmpUserResponse("" + patientId, question_message,
						sequence, node_type, CurrentDate, 101,
						"101", page_load, treeList[0], "",sectiondate,ir_is_reminder);
				Log.e(TAG, "sequence----->"+sequence);
				questions.moveToNext();				

			}
			
			questions.close();
		}
     }
		dbcreate1.closeopendbConnection();
		dbcreate1.cursorQuestions.close();
		printTable();
		return treeList;
	}

	@SuppressWarnings("unused")
	public void showTree(Node node) {

		if (node == null) {
			Log.e(TAG, "Invalid Tree Node");
			// alert #
			return;
		}
		SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
		int flow_type = flowsp.getInt("is_reminder_flow", 0);
		if (flow_type == 1)// skip for reminder
		{
			page_load=0;
		}
		if
		(topNodeNo.length>0)
		{
		if(topNodeNo[0].equals("1")){
			String CurrentDate = TestDate.getCurrentTime() + "";
			SharedPreferences settings1 = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);		
			String sectiondate=settings1.getString("sectiondate", "0");			
			dbcreate1.InsertDmpUserResponse("" + patientId, node.getQuestion(),
					node.getSequence(), node.getNodeType(), CurrentDate, 101,
					"101", page_load, treeList[0], "",sectiondate,ir_is_reminder);
			dbcreate1.closeopendbConnection();
			dbcreate1.cursorQuestions.close();
			page_load=0;
		}
		}
		
		//TextView tv1 = (TextView) findViewById(R.id.title1);
		//tv1.setText("Questions  ");
		//tv1.setVisibility(View.INVISIBLE);
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");

		TextView tv2 = (TextView) findViewById(R.id.question);
		tv2.setTypeface(type, Typeface.NORMAL);
		tv2.setText(node.getQuestion());

		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {

			File storageDir = new File(Environment
					.getExternalStorageDirectory().toString()
					+ "/versions/Optum/Questions/Q-"
					+ node.getSequence() + ".wav");
			if (storageDir != null) {

				if (storageDir.exists()) {
					Util.soundPlay("Questions/Q-" + node.getSequence() + ".wav");
					Constants.set_question_id(node.getSequence());
				} else {
					tts.speak(node.getQuestion(), TextToSpeech.QUEUE_FLUSH,
							null);
				}
			} else {
				tts.speak(node.getQuestion(), TextToSpeech.QUEUE_FLUSH, null);
			}

		} else {
			tts.speak(node.getQuestion(), TextToSpeech.QUEUE_FLUSH, null);
		}

		/*
		 * if (Integer.parseInt(node.getSequence()) < 19) { Util.soundPlay("Q-"
		 * + node.getSequence() + ".wav");
		 * 
		 * // playsound(node.getSequence()); } else {
		 * tts.speak(node.getQuestion(), TextToSpeech.QUEUE_FLUSH, null); }
		 */
		LinearLayout ll = (LinearLayout) findViewById(R.id.button_layout);		
		ll.removeAllViews();
		String[] choices = node.getChoices();
		LinearLayout  row = new LinearLayout(this);		    
		row.setOrientation(LinearLayout.HORIZONTAL);
		LayoutParams LLParams = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		LLParams.setMargins(10,0,10,0);
		row.setGravity(Gravity.CENTER_HORIZONTAL);
		row.setLayoutParams(LLParams);
		for (int i = 0; i < choices.length; i++) {			


			Button b = new Button(this);

			// b.setMinWidth(200);
			// b.setMinHeight(50);
			b.setTextSize(35);
			b.setLeft(30);

			b.setText(node.getChoices()[i]);

			LayoutParams params = new LayoutParams( LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			params.setMargins(5,25,5,25);

			b.setLayoutParams(params);

			b.setId(node.getSwitchTo()[i]); // set the correct id # // 0 for
			b.setBackgroundResource(R.drawable.yesclick);
			b.setTextColor(getResources().getColor(R.color.white));

			// last node
			b.setTypeface(type, Typeface.NORMAL);
			Display display = getWindowManager().getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			int width = size.x;
			if(width < 1200){

				b.setMinWidth(170);
				b.setMaxWidth(300);
			}else{

				b.setMinWidth(350);
				b.setMaxWidth(600);
			}

			b.setOnClickListener(this);
			//			row.addView(b);
			//			ll.addView(row);


			if( i != 0 && i%3 == 0){
				row = new LinearLayout(this);		    
				row.setOrientation(LinearLayout.HORIZONTAL);				
				row.setLayoutParams(LLParams);
			}

			row.addView(b);

			if(choices.length == 1){

				ll.addView(row);
			}else if(i> 0 && choices.length == 2){

				ll.addView(row);
			}else{

				if( i == 2 || i==5 || i== 8|| i== 11 ||i==14 || i==17 ||i == 20 
						|| i==23 || i== 26|| i== 29 ||i== 32 || i==35 || i==38 
						|| i== 41|| i== 44 ||i== 47 || i==50){

					ll.addView(row);
				}else if(choices.length-1 == i){

					ll.addView(row);
				}
			}



		}

		if (nodeList.size() > 1) {
			/*
			 * backButton = new Button(this); backButton.setText("Back");
			 * backButton.setTextColor(Color.MAGENTA);
			 * 
			 * backButton.setMinWidth(200); backButton.setTextSize(30);
			 * 
			 * // backButton.setMinWidth(10); backButton.setLayoutParams(new
			 * LayoutParams( LayoutParams.WRAP_CONTENT,
			 * LayoutParams.WRAP_CONTENT)); backButton.setId(-1); // set the
			 * correct id # backButton.setOnClickListener(this);
			 * ll.addView(backButton);
			 */

		}

		// initspeech(); // starting speech recognition

	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) {
	 * getMenuInflater().inflate(R.menu.activity_question, menu); return true; }
	 */

	public void save() {
//		if(appClass.isRemSessionStart){
//
//			appClass.uploadSessionStatusTime();
//		} else{

	//		appClass.uploadStatusTime();
//		}
//		String questionMessage, sequenceNumber, currentdate, patientCode = "";
//		String itemContent = "";
		dbcreate1.Cleartableresponse();

//		int ans, nodeType = 0;
//		int itemNumber = 0;
		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = flow.getInt("flow", 0); // #1
		if (val == 1) {					
			SharedPreferences section = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);
			SharedPreferences.Editor editor_retake = section.edit();
			editor_retake.putString("sectiondate",  Util.get_patient_time_zone_time(this));
			editor_retake.commit();
		}
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.PREFS_NAME_date, 0);		
		String sectiondate=settings1.getString("sectiondate", "0");
		
		//		for (int i = 0; i < nodeList.size(); i++) {
		//			String query, query1, query2 = "";
		//			currentNode = nodeList.get(i);
		//			questionMessage = currentNode.question_message;
		//			sequenceNumber = currentNode.sequence;
		//			nodeType = currentNode.node_type;
		//			ans = currentNode.answer;
		//			currentdate = currentNode.currentDate;
		//			// TestDate.getCurrentTime();
		//			// getDate();
		//			itemNumber = currentNode.getAnswer();
		//			itemContent = currentNode.choices[currentNode.getAnswer()];
		//			// Saving response
		//			
		//			dbcreate1.InsertDmpUserResponse("" + patientId, questionMessage,
		//					sequenceNumber, nodeType, currentdate, itemNumber,
		//					itemContent, 0, treeNo, treeName[0],sectiondate);
		//
		//		}
		try {

			/*String patient_time = Util.get_patient_time_zone_time(this);
			String AM_PM = patient_time.substring(20, 22);
			Log.e("Time", AM_PM);*/

			SharedPreferences settings = getSharedPreferences(
					CommonUtilities.USER_TIMESLOT_SP, 0);
			String slot = settings.getString("timeslot", "AM");


			ClassQuestion questions = new ClassQuestion();
			questions.setSectionDate(sectiondate);
			questions.setTimeslot(slot);
			//dbQuestion_Check.delete_data();
			dbQuestion_Check.InsertQuestion(questions);

		} catch (Exception e) {

		}

	}

	public int getAnswerIndex(Node currentNode, int switchToValue) {

		for (int i = 0; i < currentNode.switchTo.length; i++) {
			if (currentNode.switchTo[i] == switchToValue) {
				return i;
			}
		}
		return -1;

	}

	public int getAnswerIndex(Node currentNode, String label) {

		for (int i = 0; i < currentNode.switchTo.length; i++) {
			if (currentNode.choices[i].equals(label)) {
				return i;
			}
		}
		return -1;

	}

	public void onClick(View v) {

		Log.i(TAG, "User Selected Choice " + v.getId());
		int language=Constants.getLanguage();
		appClass.setRegular_alarm_count(0);
		appClass.setStart_Alarm_Count(0);
		if (v.getId() == 0) {

			Button bt = (Button) v;
			String label = bt.getText().toString();
			//label = label.substring(4, label.length()-4);
			Log.i(TAG, "User Selected label " + label);		


			play_question_answer(label);

			SharedPreferences settings1 = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);		
			String sectiondate=settings1.getString("sectiondate", "0");
			
			int answer = getAnswerIndex(currentNode, label);
			currentNode.setAnswer(answer);
			String sequenceNumber = currentNode.sequence;
			int itemNumber = currentNode.getAnswer();
			String itemContent = currentNode.choices[currentNode.getAnswer()];
			String currentdate = currentNode.currentDate;
			dbcreate1.updateToQuestionAnswerResponse(getApplicationContext(),
					itemNumber,itemContent,currentdate,treeNo,treeName[0],sequenceNumber,sectiondate);
			appClass.uploadStatusTime();
			save();

			// #
			//Toast.makeText(getApplicationContext(), "Questions Finished",
			//	Toast.LENGTH_LONG).show();

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
			Constants.set_question_id("0");
			Intent intent = new Intent(this, Questions.class);
			finish();
			startActivity(intent);
			overridePendingTransition(0, 0);
			firstTime = true;

		} else if (v.getId() >= 0) { // v.getId() : Node No (Choice)

			Button bt = (Button) v;
			String label = bt.getText().toString();
			//label = label.substring(4, label.length()-4);

			play_question_answer(label);


			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}

			int answer = getAnswerIndex(currentNode, label);

			// int answer = getAnswerIndex(currentNode, v.getId());
			currentNode.setAnswer(answer);
			String sequenceNumber = currentNode.sequence;
			int itemNumber = currentNode.getAnswer();
			String itemContent = currentNode.choices[currentNode.getAnswer()];
			String currentdate = currentNode.currentDate;
			SharedPreferences settings1 = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);		
			String sectiondate=settings1.getString("sectiondate", "0");
			
			dbcreate1.updateToQuestionAnswerResponse(getApplicationContext(),
					itemNumber,itemContent,currentdate,treeNo,treeName[0],sequenceNumber,sectiondate);
			appClass.uploadStatusTime();
			currentNode = (getNode(treeNo, "" + v.getId()));			
			//nodeList.add(currentNode);

			count++;
			Log.i(TAG, "count on next " + count);
			showTree(getNode(treeNo, "" + v.getId()));

		} else if (v.getId() < 0) { // v.getId() : Node No (Choice)
			int listSize = nodeList.size();
			Log.i(TAG, "Node List Size " + listSize);
			if (listSize > 1) {
				nodeList.remove(listSize - 1);
				Log.i(TAG, "Node List Size " + nodeList.size());
				currentNode = nodeList.get(nodeList.size() - 1);

				Util.soundPlay("Back.wav");

				// tts.speak("back", TextToSpeech.QUEUE_FLUSH, null);

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}

				count--;
				Log.i(TAG, "count on back " + count);

				showTree(currentNode);

			}

		}

	}



	public class Node {
		private int node_type = 0; // 0 : question, 1 : Message
		private String sequence = "";
		private String question_message = "";
		private String[] choices = null;
		private int[] switchTo = null;
		private int answer = -1;
		private String currentDate = "";

		public Node() {
			this.node_type = 0;
			this.sequence = "Q100";
			this.question_message = "Do you have fever ? How do you rate it ?";
			this.choices = new String[] { "High", "Medium", "Low" };
			this.switchTo = new int[] { 2, 3, 4 };
		}

		public Node(int node_type) {
			this.node_type = node_type;
		}

		public Node(int node_type, String sequence, String question_message) {
			this.node_type = node_type;
			this.sequence = sequence;
			this.question_message = question_message;
		}

		public Node(int node_type, String sequence, String question_message,
				String[] choices, int[] switchTo, String CurrentDate) {
			this.node_type = node_type;
			this.sequence = sequence;
			this.question_message = question_message;
			this.choices = choices;
			this.switchTo = switchTo;
			this.currentDate = CurrentDate;
		}

		public void setCurrentDate(String currentDate) {
			this.currentDate = currentDate;
		}

		public void setSequence(String sequence) {
			this.sequence = sequence;
		}

		public void setQuestion(String question) {
			this.question_message = question;
		}

		public void setMessage(String message) {
			this.question_message = message;
		}

		public void setChoices(String[] choices) {
			this.choices = choices;
		}

		public void setSwitchTo(int[] switchTo) {
			this.switchTo = switchTo;
		}

		public void setAnswer(int answer) {
			this.answer = answer;
		}

		public int getNodeType() {
			return node_type;
		}

		public String getSequence() {
			return sequence;
		}

		public String getQuestion() {
			return question_message;
		}

		public String getCurrentDate() {
			return currentDate;
		}

		public String getMessage() {
			return question_message;
		}

		public String[] getChoices() {
			return choices;
		}

		public int[] getSwitchTo() {
			return switchTo;
		}

		public int getAnswer() {
			return answer;
		}

	}


	@Override
	public void onStop() {

		Util.WriteLog(TAG, patientId + "");

		super.onStop();
		//Util.stopPlaySingle();
		Util.Stopsoundplay();
	}
	public void play_question_answer(String answer) {
		Log.i(TAG, "selected answer- " + answer);
		if (answer.equals("S�")) {
			answer="Yes";			
		}
		int language = Constants.getLanguage();

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

			File storageDir = new File(Environment.getExternalStorageDirectory().toString()
					+ "/versions/Optum/Messages/" + answer + ".wav");

			if (language == 0) {

				storageDir = new File(Environment.getExternalStorageDirectory().toString() + "/versions/Optum/Messages/"
						+ answer + ".wav");

			} else {

				storageDir = new File(Environment.getExternalStorageDirectory().toString()
						+ "/versions/Optum/spanish/Messages/" + answer + ".wav");
			}

			if (storageDir != null) {

				if (storageDir.exists()) {
					Util.soundPlay("Messages/" + answer + ".wav");

				} else {
					tts.speak(answer, TextToSpeech.QUEUE_FLUSH, null);
				}
			} else {
				tts.speak(answer, TextToSpeech.QUEUE_FLUSH, null);
			}

		} else {
			tts.speak(answer, TextToSpeech.QUEUE_FLUSH, null);
		}
	}
	private void printTable(){

		String query = "Select a.Item_Number,a.Sequence_Number,a.Measure_Date,"
				+ "a.Patient_Id,a.treeNumber,a.Item_Content,a.sectionDate from "
				+ "Droid_DMP_User_Response a where a.Status=0";

		Cursor response = dbcreate1.SelectDBValues(query);
		if (response.getCount() <= 0) {
			Log.i(TAG, "Upload question to server Cursor is null ");

		}
		String s = "";
		while (response.moveToNext()) {
			s += "itemtNumber:-"+response.getString(0)
			+"  Sequence_Number:-"+response.getString(1)
			+"  Measure_Date:-"+response.getString(2)
			+"  Patient_Id:-"+response.getString(3)
			+"  treeNumber:-"+response.getString(4)
			+"  Item_Content:-"+response.getString(5)
			+"  sectionDate:-"+response.getString(6)+"next";

		}
		Log.i("AnswerTable", "AnswerTable"+s);
		response.close();
		dbcreate1.cursorQuestions.close();
		dbcreate1.closeopendbConnection();
	}
}
