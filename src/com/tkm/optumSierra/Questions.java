package com.tkm.optumSierra;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.widget.Button;
import android.widget.Toast;

import com.tkm.optumSierra.dal.Questions_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Util;

public class Questions extends Activity {

	Button startquestion;
	private String serverurl = "";
	private String serialnumber = "";
	Questions_db dbcreate1 = new Questions_db(this);
	Sensor_db dbSensor = new Sensor_db(this);
	private String TAG = "Questions Sierra";
	int PatientIdDroid = 0;
	private GlobalClass appClass;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "ON  starting .........*****************************");
		setContentView(R.layout.activity_questions);
		appClass = (GlobalClass)getApplicationContext();
		
		/*
		 * requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		 * setContentView(R.layout.activity_questions);
		 * getWindow().setFeatureInt
		 * (Window.FEATURE_CUSTOM_TITLE,R.layout.header);
		 */

		// MainActivity main = new MainActivity();
		// main.getURL(this);
		// SetPublicParamAcitvity publicVariable= new SetPublicParamAcitvity();
		// ##
		// publicVariable.SetURLParams(this);

		serverurl = Constants.getPostUrl();// main.PostUrl.toString();
		serialnumber = Constants.getSerialNo();// main.serialNo.toString();
		PatientIdDroid = Constants.getdroidPatientid();

		Log.i(TAG, "Rdirctin to Upload Questionto Server : ");
		UploadQuestiontoServer();
		Redirect();

		// Intent intent = new Intent(this, ThanksActivity.class);
		// startActivity(intent);

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			final Context context = this;
			Intent intent = new Intent(context, MainActivity.class);
			// startActivity(intent);
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	private void Redirect() {

		/*
		 * if (ApkType.equals("1000001")) // HFP { Log.i(TAG,
		 * "Question finished redirecting to thanks activity  by hfp flow ");
		 * Intent intentwt = new Intent(getApplicationContext(),
		 * ScheduleRedirect.class); finish(); startActivity(intentwt);
		 * overridePendingTransition(0, 0); return;
		 * 
		 * }
		 */

		SharedPreferences flow = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
		int val = flow.getInt("flow", 0); // #1

		if (val == 1) {

			Intent intentSc = new Intent(getApplicationContext(), Home.class);
			finish();
			startActivity(intentSc);
			overridePendingTransition(0, 0);
			return;
		}

		Log.i(TAG, "Rdirctin to Datareader activity : ");

		String PREFS_NAME = "DroidPrefSc";
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		int scheduletatus = settings.getInt("Scheduletatus", -1);
		if (scheduletatus == 1) {
			Log.i(TAG, "Rdirctin to schedule home activity : ");
			Intent intentSc = new Intent(getApplicationContext(), ScheduleRedirect.class);
			finish();
			startActivity(intentSc);
			overridePendingTransition(0, 0);
		}

		else {
			Cursor cursorsensor = dbSensor.SelectMeasuredweight();
			String Sensor_name = "", Mac = "";
			//Log.i("Droid", "First SensorName : " + Sensor_name);
			cursorsensor.moveToFirst();
			if (cursorsensor.getCount() > 0) {

				Sensor_name = cursorsensor.getString(3);
				Mac = getForaMAC(cursorsensor.getString(9));

			}

			//Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
			if (Mac.trim().length() == 17) {

				if (Sensor_name.contains("3230")) {
					if (Build.VERSION.SDK_INT >= 18) {

						Intent intent = new Intent(getApplicationContext(), BLECheckActivity.class);
						intent.putExtra("macaddress", Mac);
						startActivity(intent);
						finish();
					} else {
						Log.i("Sierra", "No BLE device found for Nonin 3230, redirecting to next activity.  ");
						PresureActivity();

					}
				} else if (Sensor_name.contains("9560")) {
					Intent intent = new Intent(getApplicationContext(), DataReaderActivity.class);
					intent.putExtra("macaddress", Mac);
					this.finish();
					startActivity(intent);
				}
			} else if (Sensor_name.trim().length() > 0) {
				Intent intent = new Intent(getApplicationContext(), PulseEntry.class);
				finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {
				/*
				 * Intent intent = new
				 * Intent(getApplicationContext(),ThanksActivity.class);
				 * finish(); startActivity(intent);
				 */
				// Toast.makeText(Questions.this,
				// "Please assign Nonin BT Device.", Toast.LENGTH_LONG).show();

				PresureActivity();
			}
		}
	}

	private void PresureActivity() {

		Cursor cursorsensor = dbSensor.SelectBPSensorName();
		String Sensor_name = "", Mac = "";
		//Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));

		}
		//Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
		if (Sensor_name.contains("A and D Bluetooth smart")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(), AandDSmart.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found", Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		} else if (Sensor_name.contains("Omron HEM 9200-T")) {
			if (Build.VERSION.SDK_INT >= 18) {
				Constants.setPIN(cursorsensor.getString(10));
				Intent intent = new Intent(getApplicationContext(), OmronBlsActivity.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found", Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		}

		else if (Sensor_name.contains("Wellex")) {
			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(), PressureWellex.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found", Toast.LENGTH_SHORT).show();
				finish();
				return;
			}
		} else if (Sensor_name.contains("P724-BP")) {

			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(), P724BpSmart.class);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {
				Log.i(TAG, "SensorName P724 : no ble fund redirecting to normal page ");
				Intent intent = new Intent(this, P724Bp.class);
				finish();
				startActivity(intent);
				return;
			}
		}

		else if (Sensor_name.contains("A and D")) {

			if (Sensor_name.contains("UA-767BT-Ci")) {
				Intent intentwt = new Intent(getApplicationContext(), AandContinua.class);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			} else {

				Intent intentwt = new Intent(getApplicationContext(), AandDReader.class);
				intentwt.putExtra("deviceType", 1);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			}

		} else if (Sensor_name.contains("FORA")) {
			if (Mac.trim().length() == 17) {
				Intent intentfr = new Intent(getApplicationContext(), ForaMainActivity.class);
				intentfr.putExtra("deviceType", 1); // pressure
				intentfr.putExtra("macaddress", Mac);
				finish();
				startActivity(intentfr);
				overridePendingTransition(0, 0);
			} else {
				Intent intentfr = new Intent(getApplicationContext(), ThanksActivity.class);
				finish();
				startActivity(intentfr);
				overridePendingTransition(0, 0);
				// Toast.makeText(getApplicationContext(),
				// "Please assign BT device or Check MAC id",
				// Toast.LENGTH_LONG).show();
			}
		} else if (Sensor_name.trim().length() > 0) {
			Intent intentfr = new Intent(getApplicationContext(), PressureEntry.class);
			finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else {
			/*
			 * Intent intentfr = new
			 * Intent(getApplicationContext(),ThanksActivity.class);
			 * startActivity(intentfr);
			 */
			// Toast.makeText(getApplicationContext(),
			// "Please assign BP BT device", Toast.LENGTH_LONG).show();
			redirectTemp();
		}

	}

	private void redirectTemp() {

		String tmpMac = dbSensor.SelectTempSensorName();

		//Log.i(TAG, "tmpMac" + tmpMac);

		if (tmpMac.trim().length() > 4) {
			Intent intentfr = new Intent(getApplicationContext(), ForaMainActivity.class);
			intentfr.putExtra("deviceType", 2);
			intentfr.putExtra("macaddress", getForaMAC(tmpMac));
			finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);

		} else if (tmpMac.trim().length() > 0) {
			Intent intentfr = new Intent(getApplicationContext(), TemperatureEntry.class);
			finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else {
			/*
			 * Intent intentfr = new Intent(getApplicationContext(),
			 * ThanksActivity.class); startActivity(intentfr);
			 */
			// Toast.makeText(this,
			// "Please assign tmp BT device.", Toast.LENGTH_LONG).show();
			redirectGlucode();
		}

	}

	private void redirectGlucode() {

		// String tmpMac = dbSensor.SelectGlucoName();
		String sensor_name = dbSensor.SelectGlucose_sensor_Name();

		if (sensor_name.contains("One Touch Ultra")) {

			Intent intent = new Intent(getApplicationContext(), GlucoseReader.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Bayer")) {

			Intent intent = new Intent(getApplicationContext(), GlucoseReader.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Accu-Chek")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(), GlucoseAccuChek.class);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble", Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(), GlucoseEntry.class);
				finish();
				startActivity(intent);

			}

		} else if (sensor_name.contains("Taidoc")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(), GlucoseTaidoc.class);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble", Toast.LENGTH_SHORT).show();

				Intent intent = new Intent(getApplicationContext(), GlucoseEntry.class);
				finish();
				startActivity(intent);

			}

		} else if (sensor_name.trim().length() > 0) {
			Intent intent = new Intent(getApplicationContext(), GlucoseEntry.class);
			finish();
			startActivity(intent);
		} else {
			Intent intentfr = new Intent(getApplicationContext(), ThanksActivity.class);

			startActivity(intentfr);
			finish();
			// Toast.makeText(getApplicationContext(), "Please assign glucose.",
			// Toast.LENGTH_LONG).show();
		}
		dbSensor.cursorsensor.close();
		dbSensor.close();
	}

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			// macAddress.substring(i, i+2)
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		//Log.i(TAG, "DROID : Connect to macAddress " + macAddress);
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}

	public void getDMP() {
		Cursor c = dbcreate1.SelectDMP();

		c.moveToFirst();

		while (c.isAfterLast() == false) {
			c.moveToNext();
		}
		c.close();
		dbcreate1.cursorQuestions.close();
		dbcreate1.closeopendbConnection();

	}

	/***************************************
	 * Upload Question resp to Server start
	 ********************************************/
	public void UploadQuestiontoServer() {
		Log.i(TAG, "UploadQuestiontoServer started ");
		UploadWebPageTask task1 = new UploadWebPageTask();
		task1.execute(new String[] { serverurl + "/droid_website/add_tpm_interview_result_branchable.ashx" });
	}

	private class UploadWebPageTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			printTable();
			// String query =
			// "Select
			// a.Item_Number,a.Sequence_Number,a.Measure_Date,a.Patient_Id,a.treeNumber
			// from Droid_DMP_User_Response a where a.Status=0";
			String query = "Select a.Item_Number,a.Sequence_Number,a.Measure_Date,a.Patient_Id,a.treeNumber,a.Item_Content,a.sectionDate, a.ir_is_reminder, a.Node_Type from Droid_DMP_User_Response a where a.Status=0";

			Cursor response = dbcreate1.SelectDBValues(query);
			if (response.getCount() <= 0) {
				Log.i(TAG, "Upload question to server Cursor is null ");
				return null;
			}
			int j = 0;
			String[] childxml = new String[response.getCount()];
			String[] treeNumber = new String[response.getCount()];
			String[] finalxml = new String[response.getCount()];
			String[] PatientId = new String[response.getCount()];
			String[] MeasureDate = new String[response.getCount()];
			String[] ItemContent = new String[response.getCount()];
			String[] sectionDate = new String[response.getCount()];
			String[] ir_is_reminder = new String[response.getCount()];

			while (response.moveToNext()) {
				String xml1, xml2, xml3, xml4, xml5 = "";
				xml1 = "<response>";
				xml2 = "<sequencenumber>" + response.getString(1) + "</sequencenumber>";
				// xml3 = "<itemnumber>" + response.getString(0) +
				// "</itemnumber>";

				if (response.getString(8).equals("5")) {
					if (response.getInt(0) == 0) {
						xml3 = "<itemnumber>-1</itemnumber>";
					} else {
						xml3 = "<itemnumber>-2</itemnumber>";

					}

				} else {
					ItemContent[j] = String
							.valueOf(dbcreate1.SelectItemNumber(response.getString(5), response.getString(1)));
					xml3 = "<itemnumber>" + ItemContent[j] + "</itemnumber>";
				}

				xml4 = "<measure_date>" + response.getString(2) + "</measure_date>";
				xml5 = "</response>";
				childxml[j] = childxml[j] + xml1 + xml2 + xml3 + xml4 + xml5;
				if (childxml[j] != null) {
					finalxml[j] = "<responsetree>" + childxml[j] + "</responsetree>";
				}
				PatientId[j] = response.getString(3);
				MeasureDate[j] = response.getString(2);
				treeNumber[j] = response.getString(4);
				sectionDate[j] = response.getString(6);
				ir_is_reminder[j] = response.getString(7);

				try {
					HttpClient client = new DefaultHttpClient();
					//Log.i(TAG, "Updating serverurl" + serverurl
					//		+ "/droid_website/add_tpm_interview_result_branchable.ashx");
					HttpPost post = new HttpPost(serverurl + "/droid_website/add_tpm_interview_result_branchable.ashx");

					finalxml[j] = finalxml[j].replace("<responsetree>null<response>", "<responsetree><response>");
					//Log.i("Questions", "finalxml ..." + finalxml[j].toString());
					StringEntity se = new StringEntity(finalxml[j], HTTP.UTF_8);
					post.setHeader("patient_id", PatientId[j]);
					post.setHeader("measure_date", MeasureDate[j]);
					post.setHeader("tree_number", treeNumber[j]);
					post.setHeader("section_date", sectionDate[j]);
					post.setHeader("ir_is_reminder", ir_is_reminder[j]);
					post.setEntity(se);
					HttpResponse response1 = client.execute(post);
					InputStream in = response1.getEntity().getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(in));
					StringBuilder str = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						str.append(line + "\n");
					}
					in.close();
					str.toString();
					//Log.i("Droid_AnswerResponse", "Response ..." + str);
					if (str.length() > 0) {
						DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
						InputSource is1 = new InputSource();
						is1.setCharacterStream(new StringReader(str.toString()));
						Document doc = db.parse(is1);
						NodeList nodes = doc.getElementsByTagName("success_response");
						if (nodes.getLength() == 1) {
							//if(appClass.isRemSessionStart){

							//	appClass.uploadSessionStatus("success");
							//}else{

								appClass.uploadStatus("success");
							//}

							dbcreate1.updateToQuestionToServer(Questions.this);
						} else{

//							if(appClass.isRemSessionStart){
//
//								appClass.uploadSessionStatus("failed");
//							}else{
								
								appClass.uploadStatus("failed");
//							}
						}

					} else{

//						if(appClass.isRemSessionStart){
//
//							appClass.uploadSessionStatus("failed");
//						}else{
							
							appClass.uploadStatus("failed");
						}
//					}

				} catch (Exception e) {
//					if(appClass.isRemSessionStart){
//
//						appClass.uploadSessionStatus("failed");
//					}else{
						
						appClass.uploadStatus("failed");
//					}
					Log.e(TAG, e.toString());
					String Error = this.getClass().getName() + " : " + e.getMessage();
					OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Question Answer Upload Error", Error);
					OnInit_Data_Access.insert_incident_detail(Error, Constants.Questions_failure_upload);
				}

				j += 1;
			}
			response.close();
			dbcreate1.cursorQuestions.close();
			dbcreate1.closeopendbConnection();
			return null;
		}

	}

	/***************************************
	 * Upload Question Response to Server end
	 ********************************************/

	public String getCharacterDataFromElement(org.w3c.dom.Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

	@Override
	public void onStop() {

		Util.WriteLog(TAG, PatientIdDroid + "");

		super.onStop();
	}

	private void printTable() {

		String query = "Select a.Item_Number,a.Sequence_Number,a.Measure_Date,"
				+ "a.Patient_Id,a.treeNumber,a.Item_Content,a.sectionDate from "
				+ "Droid_DMP_User_Response a where a.Status=0";

		Cursor response = dbcreate1.SelectDBValues(query);
		if (response.getCount() <= 0) {
			Log.i(TAG, "Upload question to server Cursor is null ");

		}
		String s = "";
		while (response.moveToNext()) {
			s += "itemtNumber:-" + response.getString(0) + "  Sequence_Number:-" + response.getString(1)
			+ "  Measure_Date:-" + response.getString(2) + "  Patient_Id:-" + response.getString(3)
			+ "  treeNumber:-" + response.getString(4) + "  Item_Content:-" + response.getString(5)
			+ "  sectionDate:-" + response.getString(6) + "next";

		}
		//Log.i("AnswerTable", "AnswerTable" + s);
		response.close();
		dbcreate1.cursorQuestions.close();
		dbcreate1.closeopendbConnection();
	}

}
