package com.tkm.optumSierra;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import com.tkm.optumSierra.util.Log;
import android.view.Window;

import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Util;

public class RebootActivity extends Activity {
	private static String TAG = "RebootActivity";
	private static final String	CMD_REBOOT				= "com.prtl724.p724_lgp.CMD_REBOOT";
	private static final int	ACTION_ENABLE			= 1;
	private static final int	ACTION_DISABLE			= 0;
	private static final String	CMD_ENABLE_AIRPLANE_MODE		= "com.prtl724.p724_lga.CMD_ENABLE_AIRPLANE_MODE";
	private static final String	CMD_DISABLE_AIRPLANE_MODE		= "com.prtl724.p724_lga.CMD_DISABLE_AIRPLANE_MODE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setFinishOnTouchOutside(false);
		setContentView(R.layout.activity_reboot);

		// Check For AirplaneMode and disable if it's ON
		if(Util.is_lg())
			toggle_airplane_mode(ACTION_ENABLE);
		else
			disableAirplaneMode();

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				AppReboot();
			}
		}, 5 * 1000);
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	/* APP REBOOT */
	public void AppReboot() {
		Log.i(TAG, "AppReboot");
		if(Util.is_lg()) // if LGE
		{
			sendOrderedBroadcast(new Intent(CMD_REBOOT), null, new BroadcastReceiver()
			{
				@Override
				public void onReceive(Context context, Intent intent)
				{	

				}
			}, null, 0, null, null);
		}
		else
		{
			try {
				Process proc = Runtime.getRuntime().exec(
						new String[] { "su", "-c", "reboot" });
				proc.waitFor();
//				 String command ="reboot";
//			        executeCommandWithoutWait(getApplicationContext(), "-c", command);
				
//				Process su_proc = Runtime.getRuntime().exec("su");					
//				DataInputStream 	dis = new DataInputStream (su_proc.getInputStream ());
//				DataOutputStream 	dos = new DataOutputStream(su_proc.getOutputStream());			
//
//				if(dis != null && dos != null)
//				{
//				
//					
//				
//					dos.writeBytes("shutdown -h now");
//					
//					
//					dos.flush();		
//				}		
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*@SuppressWarnings("deprecation")
	public void disableAirplaneMode() {
		try {
			boolean aipPlaneStatus;
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
				aipPlaneStatus = Settings.System.getInt(getContentResolver(),
						Settings.System.AIRPLANE_MODE_ON, 0) != 0;
			} else {
				aipPlaneStatus = Settings.Global.getInt(getContentResolver(),
						Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
			}
			Log.i(TAG, "aipPlaneStatus=" + aipPlaneStatus);
			if (aipPlaneStatus) {
				// turn off airplane mode
				Process su_proc;
				su_proc = Runtime.getRuntime().exec("su");
				DataInputStream dis_start = new DataInputStream(
						su_proc.getInputStream());
				DataOutputStream dos_start = new DataOutputStream(
						su_proc.getOutputStream());

				if (dis_start != null && dos_start != null) {
					dos_start
							.writeBytes("settings put global airplane_mode_on 0 \n");
					dos_start.flush();
				}
			}
		} catch (Exception e) {
			String Error = this.getClass().getName() + " : " + e.getMessage();
			OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
					"AirplaneMode Disable Error", Error);
			OnInit_Data_Access.insert_incident_detail(Error,
					Constants.Disabling_Airplane_mode);
		}
	}*/
	
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public void disableAirplaneMode() {
		try {
			@SuppressWarnings("unused")
			int airplaneStatus = 0;
			boolean aipPlaneStatus;
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
				aipPlaneStatus = Settings.System.getInt(getContentResolver(),
						Settings.System.AIRPLANE_MODE_ON, 0) != 0;
			} else {
				aipPlaneStatus = Settings.Global.getInt(getContentResolver(),
						Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
			}
			Log.i(TAG, "aipPlaneStatus=" + aipPlaneStatus);
			if (aipPlaneStatus) {
				// turn off airplane mode
				if (android.os.Build.VERSION.SDK_INT < 17) {
					try {
						// toggle airplane mode
						Settings.System.putInt(getContentResolver(),
								Settings.System.AIRPLANE_MODE_ON,
								aipPlaneStatus ? 0 : 1);

						// Post an intent to reload
						Intent intent = new Intent(
								Intent.ACTION_AIRPLANE_MODE_CHANGED);
						intent.putExtra("state", !aipPlaneStatus);
						sendBroadcast(intent);
					} catch (Exception e) {
						String Error = this.getClass().getName() + " : " + e.getMessage();
						OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
								"AirplaneMode Disable Error", Error);
						OnInit_Data_Access.insert_incident_detail(Error,
								Constants.Disabling_Airplane_mode);
					}
				} else {
					airplaneStatus = 1;
					try {
						Process su_proc;
						su_proc = Runtime.getRuntime().exec("su");
						DataInputStream dis_start = new DataInputStream(
								su_proc.getInputStream());
						DataOutputStream dos_start = new DataOutputStream(
								su_proc.getOutputStream());

						if (dis_start != null && dos_start != null) {
							dos_start
									.writeBytes("settings put global airplane_mode_on 0 \n");// "settings put global airplane_mode_on 0 \n");
							dos_start.flush();
							dos_start
									.writeBytes("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false");
							dos_start.flush();
						}
					} catch (Exception e) {
						String Error = this.getClass().getName() + " : " + e.getMessage();
						OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
								"AirplaneMode Disable Error", Error);
						OnInit_Data_Access.insert_incident_detail(Error,
								Constants.Disabling_Airplane_mode);
					}
				}
			}
		} catch (Exception e) {
			String Error = this.getClass().getName() + " : " + e.getMessage();
			OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
					"AirplaneMode Disable Error", Error);
			OnInit_Data_Access.insert_incident_detail(Error,
					Constants.Disabling_Airplane_mode);
		}
	}

	

	private void toggle_airplane_mode(int status)
	{

		if (status == ACTION_DISABLE)
		{
			Log.e(TAG, "Enabled Airplane Mode");
			sendOrderedBroadcast(new Intent(CMD_ENABLE_AIRPLANE_MODE), null, new BroadcastReceiver()
			{
				@Override
				public void onReceive(Context context, Intent intent)
				{

					String responce = getResultData(); 

					if (responce != null)
					{
						if (responce.equals("-1"))
						{	
							//show_responce(responce);
						}
					}
					else
					{
						//show_responce(responce);
					}
				}
			}, null, 0, null, null);

		}
		else if (status == ACTION_ENABLE)
		{
			Log.e(TAG, "Disabled Airplane Mode");
			sendOrderedBroadcast(new Intent(CMD_DISABLE_AIRPLANE_MODE), null, new BroadcastReceiver()
			{
				@Override
				public void onReceive(Context context, Intent intent)
				{

					String responce = getResultData(); 

					if (responce != null)
					{
						if (responce.equals("-1"))
						{	
							String Error = this.getClass().getName() + " : " + responce;
							OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
									"AirplaneMode Disable Error", Error);
							OnInit_Data_Access.insert_incident_detail(Error,
									Constants.Disabling_Airplane_mode);
						}
					}
					else
					{

						String Error = this.getClass().getName() + " : NULL";
						OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
								"AirplaneMode Disable Error", Error);
						OnInit_Data_Access.insert_incident_detail(Error,
								Constants.Disabling_Airplane_mode);
					}
				}
			}, null, 0, null, null);
		}
	}
	
	private void executeCommandWithoutWait(Context context, String option, String command) {
        boolean success = false;
        String su = "su";
        for (int i=0; i < 3; i++) {
            // "su" command executed successfully.
            if (success) {
                // Stop executing alternative su commands below.
                break;
            }
            if (i == 1) {
                su = "/system/xbin/su";
            } else if (i == 2) {
                su = "/system/bin/su";
            }
            try {
                // execute command
                Process process = Runtime.getRuntime().exec(new String[]{su, option, command});
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(process.getInputStream()));
                String line = null;
                while ((line = in.readLine()) != null) {
                    System.out.println("--------"+line);
                }
                } catch (IOException e) {
                Log.e(TAG, "su command has failed due to: " + e.fillInStackTrace());
            }
        }
    }
}
