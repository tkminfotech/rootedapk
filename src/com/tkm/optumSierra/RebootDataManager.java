package com.tkm.optumSierra;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.tkm.optumSierra.util.ConnectionDetector;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Upload_Incident_Detail_Task;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.telephony.TelephonyManager;
import com.tkm.optumSierra.util.Log;
import android.view.WindowManager;

public class RebootDataManager extends Titlewindow {

	// Find the directory for the SD Card using the API
	File sdcard = Environment.getExternalStorageDirectory();
	File file = new File(sdcard, "/versions/tablet_incident.log");
	String TAG = "RebootDataManager";
	String URL_UPLOAD = "/droid_website/upload_flat_file.ashx";
	private boolean file_exists = false;
	private BluetoothAdapter mBluetoothAdapter = null;
	private int airplaneStatus;

	// private BluetoothManager manager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_hfpmain);
		Log.i(TAG, "OnCreate");
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			airplaneStatus = (Integer) extras.get("airplaneStatus");
		}

		if (airplaneStatus == 1) {
			/* CALL REBOOT ACTIVITY */
			Intent intent = new Intent(RebootDataManager.this, RebootActivity.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
		} else {
			if (file.length() == 0) {
				// empty or doesn't exist
				Log.i(TAG, "NO FILE FOUND");
			} else {
				Log.i(TAG, "FILE FOUND");
				// exists and is not empty
				file_exists = true;
			}

			// Init File Read.
			readFile();

			new Handler().postDelayed(new Runnable() {
				@SuppressLint("NewApi")
				@Override
				public void run() {
					Boolean isInternetPresent = (new ConnectionDetector(RebootDataManager.this))
							.isConnectingToInternet();
					Log.i(TAG, "before internet checking");
					if (isInternetPresent) {
						Log.i(TAG, "isInternetPresent TRUE");
						if (file_exists) {
							// Upload Error File
							Log.i(TAG, "File exist");
							new UploadErrorFile().execute();
						}
						// insert and upload incident detail
						// OnInit_Data_Access.insert_incident_detail("Starts-Up
						// Log",Constants.After_the_tablet_starts_up);
						String server_path = Constants.getPostUrl();
						Upload_Incident_Detail_Task incident_task = new Upload_Incident_Detail_Task(
								getApplicationContext(), server_path);
						incident_task.execute(new String[] { "" });
					} else {
						Log.i(TAG, "isInternetPresent FALSE");
					}

					try {
						// manager = (BluetoothManager)
						// getBaseContext().getSystemService(Context.BLUETOOTH_SERVICE);
						// mBluetoothAdapter = manager.getAdapter();

						mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

						if (mBluetoothAdapter.isEnabled()) {
							// BLUETOOTH ALREADY ENABLED
						} else {
							// BLUETOOTH NOT ENABLED
							// Enabling Bluetooth
							mBluetoothAdapter.enable();
						}
					} catch (Exception e) {
						String Error = this.getClass().getName() + " : " + e.getMessage();
						OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Bluetooth Enable Error", Error);
						OnInit_Data_Access.insert_incident_detail(Error, Constants.Enabling_Bluetooth);
					}

					try {
						WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
						if (wifi.isWifiEnabled()) {
							wifi.setWifiEnabled(false);
						}
						// for testing
						// else
						// wifi.setWifiEnabled(true);

					} catch (Exception e) {
						String Error = this.getClass().getName() + " : " + e.getMessage();
						OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Wifi disable Error", Error);
						OnInit_Data_Access.insert_incident_detail(Error, Constants.Disabling_WiFi);
					}
				}
			}, 30 * 1000);

			// SET ALARM AFTER 24 HOURS BETWEEN 9:00 AM AND 9:00 PM
			After_24_Hour_Reboot();

			Intent intentFinalMain = new Intent(RebootDataManager.this, FinalMainActivity.class);
			intentFinalMain.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intentFinalMain);
		}
	}

	private class UploadErrorFile extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... params) {
			try {
				Log.i(TAG, "UploadErrorFile");
				HttpURLConnection conn = null;
				DataOutputStream dos = null;
				String lineEnd = "\r\n";
				String twoHyphens = "--";
				String boundary = "*****";
				int bytesRead, bytesAvailable, bufferSize;
				byte[] buffer;
				int maxBufferSize = 1 * 1024 * 1024;

				if (file.isFile()) {

					try {

						TelephonyManager tManager = (TelephonyManager) getApplicationContext()
								.getSystemService(Context.TELEPHONY_SERVICE);
						String imeilNo = tManager.getDeviceId().substring(0, 14);

						String upLoadServerUri = Constants.getPostUrl() + URL_UPLOAD;

						// open a URL connection to the Servlet
						FileInputStream fileInputStream = new FileInputStream(file);
						URL url = new URL(upLoadServerUri);
						Log.i(TAG, "UploadErrorFile--------------    "+upLoadServerUri);
						// Open a HTTP connection to the URL
						conn = (HttpURLConnection) url.openConnection();
						conn.setDoInput(true); // Allow Inputs
						conn.setDoOutput(true); // Allow Outputs
						conn.setUseCaches(false); // Don't use a Cached Copy
						conn.setRequestMethod("POST");
						conn.setRequestProperty("Connection", "Keep-Alive");
						conn.setRequestProperty("ENCTYPE", "multipart/form-data");
						conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
						conn.setRequestProperty("bill", file.toString());
						conn.setRequestProperty("imei_no", imeilNo);
						dos = new DataOutputStream(conn.getOutputStream());

						dos.writeBytes(twoHyphens + boundary + lineEnd);
						dos.writeBytes("Content-Disposition: form-data; name=\"bill\";filename=\"" + file.toString()
								+ "\"" + lineEnd);

						dos.writeBytes(lineEnd);

						// create a buffer of maximum size
						bytesAvailable = fileInputStream.available();

						bufferSize = Math.min(bytesAvailable, maxBufferSize);
						buffer = new byte[bufferSize];

						// read file and write it into form...
						bytesRead = fileInputStream.read(buffer, 0, bufferSize);

						while (bytesRead > 0) {

							dos.write(buffer, 0, bufferSize);
							bytesAvailable = fileInputStream.available();
							bufferSize = Math.min(bytesAvailable, maxBufferSize);
							bytesRead = fileInputStream.read(buffer, 0, bufferSize);
						}

						// send multipart form data necesssary after file
						// data...
						dos.writeBytes(lineEnd);
						dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
						Log.i(TAG, "UploadErrorFile------    "+"serverResponseCode");
						// Responses from the server (code and message)
						int serverResponseCode = conn.getResponseCode();
						String serverResponseMessage = conn.getResponseMessage();
						Log.i(TAG, "UploadErrorFile---------------    "+"serverResponseCode"+serverResponseMessage);
						fileInputStream.close();
						Log.i(TAG, "serverResponseMessage:=>" + serverResponseMessage);
						dos.flush();
						dos.close();
						if (serverResponseCode == 200) {
							return "success";
						} else {
							return "Failed";
						}
					} catch (Exception e) {
						Log.i(TAG, "UploadErrorFile------    "+"Exception");
						e.printStackTrace();
					}
				} // End else block
			} catch (Exception ex) {
				ex.printStackTrace();
				String Error = this.getClass().getName() + " : " + ex.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Error File Upload Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.After_the_tablet_starts_up);
			}
			return "Executed";
		}

		@Override
		protected void onPostExecute(String result) {
//			Log.i(TAG, "#onPostExecute result:" + result);
			if (result.equals("success")) {
				file.delete();
			}
		}

		@Override
		protected void onPreExecute() {
			Log.i(TAG, "#onPreExecute");
		}
	}

	private String readFile() {
		String content;
		try {
			File readFile = new File(
					Environment.getExternalStorageDirectory().getAbsolutePath() + "/versions/optum_app/optum_app.ini");
			SharedPreferences settings = getSharedPreferences("file_data", 0);
			settings.edit().clear().commit();
			if (readFile.length() == 0) {
				// NO FILE FOUND OR EMPTY FILE
				Log.i(TAG, "NO INIT-FILE FOUND OR EMPTY FILE");
			} else {

				FileInputStream fileInputStream = new FileInputStream(readFile);
				@SuppressWarnings("resource")
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
				while ((content = bufferedReader.readLine()) != null) {
					// substring(content.indexOf("[")+1,
					// content.indexOf("]")).equals(key)
					if (!content.contains("#")) {
						String KEY = content.substring(0, content.indexOf("="));
						String VALUE = content.substring(content.indexOf("=") + 1, content.length());
						if (VALUE.matches("[0-9]+") && VALUE.length() > 0) {
							// VALID INIT-FILE VALUES
							Log.i(TAG, "VALID INIT-FILE VALUES");
							SharedPreferences.Editor editor = settings.edit();
							editor.putString(KEY, VALUE);
							editor.commit();
						} else {
							// INVALID INIT-FILE VALUES
							Log.i(TAG, "INVALID INIT-FILE VALUES");
						}
						if (content.contains("reboot_allow_time_period")) {
							String starttime = content.substring(content.indexOf("=") + 1, content.indexOf(":"));
							String endTime = content.substring(content.indexOf(":") + 1, content.length());
							if (starttime.matches("[0-9]+") && starttime.length() > 2 && endTime.matches("[0-9]+")
									&& endTime.length() > 2) {
								Log.i(TAG, "VALID reboot_allow_time_period DATA");
								long start_end_diff = Long.parseLong(endTime) - Long.parseLong(starttime);
								Log.i(TAG, "strattime:" + starttime + "  endTime:" + endTime + "  start_end_diff:"
										+ start_end_diff);
								starttime = convertSecondsToHMmSs(Long.parseLong(starttime));
								endTime = convertSecondsToHMmSs(Long.parseLong(endTime));
								Log.i(TAG, "strattime:" + starttime + "  endTime:" + endTime);

								SharedPreferences settings1 = getSharedPreferences("file_data", 0);
								SharedPreferences.Editor editor1 = settings1.edit();
								editor1.putString("reboot_allow_time_period_start", starttime);
								editor1.putString("reboot_allow_time_period_end", endTime);
								editor1.putString("duration", Long.toString(start_end_diff));
								editor1.commit();
							} else {
								Log.i(TAG, "INVALID reboot_allow_time_period DATA");
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
}
