package com.tkm.optumSierra;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.tkm.optumSierra.bean.ClassSchedule;
import com.tkm.optumSierra.dal.Schedule_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.Util;

public class ScheduleRedirect extends Activity {
	Schedule_db dbSchedule = new Schedule_db(this);
	Sensor_db dbSensor = new Sensor_db(this);
	int patientIdDroid = 0, measureTypeId = 0, id = 0;
	String scheduleTime, dbscheduleId = "";
	String PREFS_NAME = "DroidPrefSc";
	private String TAG = "Scheduleredirect";
	private GlobalClass globalClass;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		patientIdDroid = Constants.getdroidPatientid();
		// clear retak sp
		globalClass = (GlobalClass)getApplicationContext();
		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);
		SharedPreferences.Editor editor_retake = settings.edit();
		editor_retake.putInt("retake_status", 0);
		editor_retake.commit();
		getSchedule();
		
		Log.i(TAG,
				"-------------------oncreate--------------  schedule redirect  activity");
	}

	public Boolean preference() {

		try {
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			int scheduletatus = settings.getInt("Scheduletatus", -1);
			if (scheduletatus == 1)
				return true;
			else
				return false;
		}

		catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return false;
		}
	}

	private void getSchedule() {
		try {

			ClassSchedule classSchedule = new ClassSchedule();
			classSchedule = dbSchedule.selectSchedule("", patientIdDroid);
			measureTypeId = classSchedule.getMeasureTypeId();
			scheduleTime = classSchedule.getScheduleTime();
			dbscheduleId = classSchedule.getScheduleId();
			id = classSchedule.getId();

			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

			String scheduletatus = settings.getString("Scheduleid", "AA");
			if (dbscheduleId != null) {
				if (!scheduletatus.equals("AA")) {
					if (!dbscheduleId.equals(scheduletatus)) {
						setprefferenceForThanks("AA");
						Intent intent = new Intent(this,
								ScheduleThanksActivity.class);
						this.finish();
						startActivity(intent);
						// setprefferenceForThanks(dbscheduleId);
						return;
					}

				}

				setprefferenceForThanks(dbscheduleId);
			}

			Log.i(TAG, "Schedule measure type ID : " + measureTypeId);

			dbSchedule.updateSchedule(id);
			if (measureTypeId != 0) {
				setprefference(0); // set preffence to zero by defalt

			}

			if (measureTypeId == 2) {

				setprefference(1);
				redirectPulse();

			} else if (measureTypeId == 3) {

				setprefference(1);
				redirectBP();

			} else if (measureTypeId == 7) {

				setprefference(1);
				redirectWT();

			} else if (measureTypeId == 6) {

				setprefference(1);
				redirectTemp();

			} else if (measureTypeId == 4) {
				setprefference(1);

				Intent intent = new Intent(this, StartQuestion.class);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);

			} else if (measureTypeId == 1) {
				setprefference(1);

				// String tmpMac = dbSensor.SelectGlucoName();
				String sensor_name = dbSensor.SelectGlucose_sensor_Name();

				if (sensor_name.contains("One Touch Ultra")) {

					Intent intent = new Intent(getApplicationContext(),
							GlucoseReader.class);

					this.finish();
					startActivity(intent);
					overridePendingTransition(0, 0);

				} else if (sensor_name.contains("Bayer")) {

					Intent intent = new Intent(getApplicationContext(),
							GlucoseReader.class);

					this.finish();
					startActivity(intent);
					overridePendingTransition(0, 0);

				} else if (sensor_name.contains("Accu-Chek")) {

					if (Build.VERSION.SDK_INT >= 18) {
						Intent intent = new Intent(getApplicationContext(),
								GlucoseAccuChek.class);
						startActivity(intent);
						finish();
					} else {

						Toast.makeText(getBaseContext(), "not support ble",
								Toast.LENGTH_SHORT).show();
						Intent intent = new Intent(getApplicationContext(),
								GlucoseEntry.class);
						finish();
						startActivity(intent);

					}

				} else if (sensor_name.contains("Taidoc")) {

					if (Build.VERSION.SDK_INT >= 18) {
						Intent intent = new Intent(getApplicationContext(),
								GlucoseTaidoc.class);
						startActivity(intent);
						finish();
					} else {

						Toast.makeText(getBaseContext(), "not support ble",
								Toast.LENGTH_SHORT).show();

						Intent intent = new Intent(getApplicationContext(),
								GlucoseEntry.class);
						finish();
						startActivity(intent);

					}

				} else if (sensor_name.trim().length() > 0) {
					Intent intent = new Intent(getApplicationContext(),
							GlucoseEntry.class);
					finish();
					startActivity(intent);
					overridePendingTransition(0, 0);
				} else {
					getSchedule();
				}

			} else if (measureTypeId == 0) {

				if (preference()) {
					setprefference(0);
					final Context context = this;
					Intent intent = new Intent(context, ThanksActivity.class);
					this.finish();
					startActivity(intent);
					overridePendingTransition(0, 0);

					Log.e(TAG, "Schedule Finished");
				} else {
					// DEfault flow
					setprefference(0);
					/*
					 * Toast.makeText(this, "No Schedule for this time",
					 * Toast.LENGTH_LONG).show();
					 */

					redirectWT();
				}

			} else {

				getSchedule();
				setprefference(0);
				Intent intent = new Intent(getApplicationContext(),
						ThanksActivity.class);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			}
		} catch (Exception e) {
			Log.i(TAG, " Exception schedule redirect  activity getSchedule() ");

		}

	}

	public void setprefference(int n) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("Scheduletatus", n);
		editor.commit();
	}

	public void setprefferenceForThanks(String n) {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("Scheduleid", n);
		editor.commit();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);
			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

			// System.exit(0);
		} else if (keyCode == KeyEvent.KEYCODE_HOME) {
			Log.i(TAG, "DROID home  clicked:");
			finish();
			// System.exit(0);
		}

		return super.onKeyDown(keyCode, event);
	}

	public void redirectPulse() {

		Log.i(TAG, "Rdirctin to Datareader activity : ");

		Cursor cursorsensor = dbSensor.SelectMeasuredweight();
		String Sensor_name = "", Mac = "";
		Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));

		}
		cursorsensor.close();
		dbSensor.cursorsensor.close();
		dbSensor.close();

		Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
		if (Mac.trim().length() == 17) {

			if (Sensor_name.contains("3230")) {
				Intent intent = new Intent(getApplicationContext(),
						BLECheckActivity.class);
				intent.putExtra("macaddress", Mac);
				startActivity(intent);
				finish();
			} else if (Sensor_name.contains("9560")) {
				Intent intent = new Intent(getApplicationContext(),
						DataReaderActivity.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
			}

			overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("Manual")) {
			Intent intent = new Intent(getApplicationContext(),
					PulseEntry.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else {
			/*
			 * Intent intent = new Intent(getApplicationContext(),
			 * ThanksActivity.class); finish(); startActivity(intent);
			 */
			getSchedule();
			// Toast.makeText(this, "Please assign Pulse BT Device.",
			// Toast.LENGTH_LONG)
			// .show();

		}

	}

	private void redirectTemp() {

		String tmpMac = dbSensor.SelectTempSensorName();

		Log.i(TAG, "tmpMac" + tmpMac);

		if (tmpMac.trim().length() > 4) {
			Intent intentfr = new Intent(getApplicationContext(),
					ForaMainActivity.class);
			intentfr.putExtra("deviceType", 2);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
			intentfr.putExtra("macaddress", getForaMAC(tmpMac));
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);

		} else if (tmpMac.equals("-1")) {
			Intent intentfr = new Intent(getApplicationContext(),
					TemperatureEntry.class);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else {
			getSchedule();
			// Intent intentfr = new
			// Intent(getApplicationContext(),GlucoseEntry.class);
			// intentfr.putExtra("deviceType", 2);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
			// intentfr.putExtra("macaddress", getForaMAC(tmpMac));
			// startActivity(intentfr);
			// Toast.makeText(this, "Please assign Temperature BT device.",
			// Toast.LENGTH_LONG).show();
		}

	}

	private void redirectBP() {
		Cursor cursorsensor = dbSensor.SelectBPSensorName();
		String Sensor_name = "", Mac = "";
		Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));

		}

		cursorsensor.close();
		dbSensor.cursorsensor.close();
		dbSensor.close();
		Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
		if (Sensor_name.contains("A and D Bluetooth smart")) {
			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						AandDSmart.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no Ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		} else if (Sensor_name.contains("Omron HEM 9200-T")) {
			if (Build.VERSION.SDK_INT >= 18) {
				Constants.setPIN(cursorsensor.getString(10));
				Intent intent = new Intent(getApplicationContext(),
						OmronBlsActivity.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		} else if (Sensor_name.contains("P724-BP")) {

			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						P724BpSmart.class);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {
				Log.i(TAG,
						"SensorName P724 : no ble fund redirecting to normal page ");
				Intent intent = new Intent(this, P724Bp.class);
				finish();
				startActivity(intent);

				return;
			}
		}

		else if (Sensor_name.contains("Wellex")) {

			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						PressureWellex.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no Ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}
		} else if (Sensor_name.contains("A and D")) {

			if (Sensor_name.contains("UA-767BT-Ci")) {
				Intent intentwt = new Intent(getApplicationContext(),
						AandContinua.class);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			} else {

				Intent intentwt = new Intent(getApplicationContext(),
						AandDReader.class);
				intentwt.putExtra("deviceType", 1);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			}

		} else if (Sensor_name.contains("FORA")) {
			if (Mac.trim().length() == 17) {
				Intent intentfr = new Intent(getApplicationContext(),
						ForaMainActivity.class);
				intentfr.putExtra("deviceType", 1); // pressure
				intentfr.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intentfr);
				overridePendingTransition(0, 0);
			} else if (Sensor_name.contains("Manual")) {
				Intent intentfr = new Intent(getApplicationContext(),
						PressureEntry.class);

				this.finish();
				startActivity(intentfr);

			} else {
				getSchedule();
				// Toast.makeText(this,
				// "Please assign BT device or Check MAC id for BP",
				// Toast.LENGTH_LONG).show();
			}
		} else if (Sensor_name.contains("Manual")) {
			Intent intentfr = new Intent(getApplicationContext(),
					PressureEntry.class);

			this.finish();
			startActivity(intentfr);

		} else {
			getSchedule();
			// Toast.makeText(this, "Please assign Blood Pressure BT device",
			// Toast.LENGTH_LONG).show();
		}

	}

	private void redirectWT() {

		Cursor cursorsensor = dbSensor.SelectWeightSensorName();
		String Sensor_name = "", Mac = "";
		Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {
			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));
		}
		cursorsensor.close();
		dbSensor.cursorsensor.close();
		dbSensor.close();

		Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
		if (Sensor_name.contains("A and D")) {
			Intent intentwt = new Intent(getApplicationContext(),
					AandDReader.class);
			// Intent intentwt = new
			// Intent(getApplicationContext(),TemperatureEntry.class);
			// intentwt.putExtra("macaddress", "00:1C:05:00:40:64");
			intentwt.putExtra("deviceType", 0);
			this.finish();
			startActivity(intentwt);
			overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("FORA")) {
			Intent intentfr = new Intent(getApplicationContext(),
					ForaMainActivity.class);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:86:FB");
			intentfr.putExtra("deviceType", 0);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
			intentfr.putExtra("macaddress", Mac);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("P724")) {
			Intent intentfr = new Intent(getApplicationContext(),
					P724Activity.class);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:86:FB");
			intentfr.putExtra("deviceType", 0);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
			intentfr.putExtra("macaddress", Mac);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else if (Sensor_name.trim().length() > 0) {
			final Context context = this;
			// Intent intent = new Intent(context,ThanksActivity.class); #
			// change for skip vitals
			Intent intent = new Intent(context, WeightEntry.class);
			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);
		} else {

			if (preference()) {

				getSchedule();

			} else {
				// Toast.makeText(this, "Please assign  BT device.",
				// Toast.LENGTH_LONG).show();
				setprefference(0);
				final Context context = this;
				// Intent intent = new Intent(context,ThanksActivity.class); #
				// change for skip vitals

				String question = dbSensor.SelectQuestion();
				if (question.trim().length() > 0) {
					Intent intent = new Intent(context, StartQuestion.class);
					this.finish();
					startActivity(intent);
					overridePendingTransition(0, 0);
				} else {
					Intent intent = new Intent(context, Questions.class);
					this.finish();
					startActivity(intent);
					overridePendingTransition(0, 0);
				}
				/*
				 * String tmpMac= dbSensor.SelectQuestion();
				 * if(tmpMac.equals("-1")) {
				 * 
				 * Intent intent = new Intent(context,StartQuestion.class);
				 * startActivity(intent); } else { Intent intent = new
				 * Intent(context,Questions.class); startActivity(intent); }
				 */
			}

		}

	}

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			// macAddress.substring(i, i+2)
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		Log.i(TAG, " : Connect to macAddress " + macAddress);
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}

	@Override
	public void onStop() {

		Util.WriteLog(TAG, patientIdDroid + "");

		super.onStop();
	}

}
