package com.tkm.optumSierra;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


import com.tkm.optumSierra.bean.ClassMeasureType;
import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.dal.Schedule_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class ScheduleThanksActivity extends Titlewindow  implements  OnInitListener{

	Schedule_db dbSchedule = new Schedule_db(this);
	private TextToSpeech tts;
	private Button SaveButton;
	final Context context = this;
	private String TAG="SchedulethanksActivity Sierra";
	MeasureType_db db_measureType = new MeasureType_db(this);
	public void onInit(int status) {        
		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.US);                
			Log.i(TAG, "Initilization Success");
			tts.setSpeechRate(0); // set speech speed rate
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e(TAG, "Language is not supported");
			} else {
				//speakOut();
			}
		} else {
			Log.e(TAG, "Initilization Failed");
		}
		// Util.soundPlay("Sierra/Thankyou.wav");
		tts.speak(this.getString(R.string.thankyou), TextToSpeech.QUEUE_FLUSH, null);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule_thanks);
		tts = new TextToSpeech(this, this);

		SaveButton = (Button) findViewById(R.id.btn_nextSchedule);
		SaveButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click           	 
				check_schedule_for_skip_vitals();
				Intent intentwt = new Intent(getApplicationContext(),ScheduleRedirect.class);          			
				finish();
				startActivity(intentwt);           		

			}
		});


		

	}




	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {


		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG,"DROID KEYCODE_BACK clicked:");

			Intent intent = new Intent(context,FinalMainActivity.class);
			startActivity(intent);
			finish();
			// System.exit(0);
		}
		else if(keyCode == KeyEvent.KEYCODE_HOME)
		{
			Log.i(TAG,"DROID home  clicked:");
			finish();
			//System.exit(0);
		}

		return super.onKeyDown(keyCode, event);
	}
	@Override
	public void onStop() {
		
		Log.i(TAG,"schedule thanks on Stop");

		Util.WriteLog(TAG,Constants.getdroidPatientid()+"");
		
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		super.onStop();
	}
	//  private ProgressDialog dialog = null;
	// final ProgressDialog dialog = ProgressDialog.show(ThanksActivity.this, "", "Thank you, have a nice day", true);


	@Override
	protected void onPause()
	{
		super.onPause();

		
	}
	private void check_schedule_for_skip_vitals() {

String s_time=TestDate.getCurrentTime();
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.PREFS_NAME_date, 0);
		SharedPreferences.Editor editor_retake = settings1.edit();
		editor_retake.putString("sectiondate",  Util.get_patient_time_zone_time(this));
		editor_retake.commit();

		Cursor cs_schedule = dbSchedule.selectSchedule_skip_status("",
				Constants.getdroidPatientid());
		cs_schedule.moveToFirst();
		if (cs_schedule.getCount() > 0) {
			db_measureType.delete();
			while (cs_schedule.isAfterLast() == false) {

				ClassMeasureType sensor1 = new ClassMeasureType();
				sensor1.setMeasuretypeId(Integer.parseInt(cs_schedule
						.getString(4)));
				sensor1.setPatientId(Constants.getdroidPatientid());
				sensor1.setStatus(1);
				sensor1.setDate(s_time);
				db_measureType.insert(sensor1);
				cs_schedule.moveToNext();
			}
		}
		cs_schedule.close();
		dbSchedule.cursorSchedule.close();
		dbSchedule.close();

	}
}
