package com.tkm.optumSierra;




import com.tkm.optumSierra.util.CommonUtilities;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends Titlewindow {


	EditText server, port;
	Button save;
	Dialog dialog;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		server=(EditText)findViewById(R.id.server);
		port=(EditText) findViewById(R.id.port);
		save=(Button)findViewById(R.id.btnsave);
		addListenerOnButton();
		/*SharedPreferences settings = getSharedPreferences(
				CommonUtilities.SERVER_URL_SP, 0);
		
		String servername = settings.getString("Server_url", "-1");
		String portname=settings.getString("Server_port", "-1");
		
		if (!servername.contains("-1")) {
			server.setText(""+servername);
			port.setText(""+portname);
		
		}*/
		TextView server=(TextView) findViewById(R.id.textserver);
		TextView textport=(TextView) findViewById(R.id.textport);
		
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		server.setTypeface(type, Typeface.NORMAL);
		textport.setTypeface(type, Typeface.NORMAL);
		
		
	

	}

	private void addListenerOnButton() {
		save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				
				if ((server.getText().toString()).length() != 0
						&& (port.getText().toString()).length() != 0){
					
					
					
					SharedPreferences settings = getSharedPreferences(
							CommonUtilities.SERVER_URL_SP, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putString("Server_url", server.getText().toString().trim());
					editor.putString("Server_port", port.getText().toString().trim());
					editor.putString("Server_post_url",
							"https://"+server.getText().toString().trim()+':'+port.getText().toString().trim());
					editor.commit();
					
					
					/*Toast.makeText(SettingsActivity.this, "Saved successfully",
							  Toast.LENGTH_LONG).show();*/
					
					Intent i=new Intent(SettingsActivity.this,FinalMainActivity.class);
					startActivity(i);
				}
				else{
					 popup();
				}
				
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}
	private void popup()
	{

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		dialog = adb.setView(new View(this)).create();
		// (That new View is just there to have something inside the dialog that can grow big enough to cover the whole screen.)

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = 790;
		lp.height = 550;
		dialog = new Dialog(this, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.popup_messages);
		
		TextView msg = (TextView) dialog
				.findViewById(R.id.textpopup);
		TextView mac = (TextView) dialog
				.findViewById(R.id.txtmac);	

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);		
		msg.setText(this.getString(R.string.invalidserver));	
		mac.setVisibility(View.INVISIBLE);
		dialog.show();
		dialog.getWindow().setAttributes(lp);		
		closeversion = (Button) dialog.findViewById(R.id.exitversion);
		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				dialog.dismiss();
			}
		});

		
	}

}
