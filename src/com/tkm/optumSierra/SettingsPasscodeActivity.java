package com.tkm.optumSierra;

import java.util.Set;

import com.tkm.optumSierra.bean.PairedDevice;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.CustomKeyboard;

import android.app.Activity;
import android.app.Dialog;
import android.app.ActionBar.LayoutParams;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

public class SettingsPasscodeActivity extends Activity{
	CustomKeyboard mCustomKeyboard;
	EditText edtPasscode;
	String passcode = "";
	private BluetoothAdapter bluetoothAdapter;
	//private LinearLayout layout_passcode;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
	    // ...but notify us that it happened.
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
	   // getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
	    this.setFinishOnTouchOutside(false);
		setContentView(R.layout.devices_popup);
		edtPasscode = (EditText)findViewById(R.id.edt_passcode);
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		//layout_passcode = (LinearLayout)findViewById(R.id.layout_passcode);		
		mCustomKeyboard = new CustomKeyboard(this, R.id.popup_keyboardview,R.xml.hexkbd_popup, keyboardHandler);
		mCustomKeyboard.registerEditText(R.id.edt_passcode);
		edtPasscode.setTransformationMethod(new MyPasswordTransformationMethod());
		Button btnSubmit = (Button)findViewById(R.id.btn_devicesSubmit);
		Button btndevicesClose = (Button)findViewById(R.id.btn_devicesClose);
		 bluetoothOn();
		btnSubmit.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				passcodeVerification(v);
			}
		});
		btndevicesClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				finish();
			}
		});
	}

	private final Handler keyboardHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// switch (msg.what) {




			passcode = edtPasscode.getText().toString();

		}
	};

	private void passcodeVerification(View v){
		
		SharedPreferences settings2 = getSharedPreferences(
				CommonUtilities.CLUSTER_SP, 0);
		String storedPasscode = settings2.getString("Passcode", "-1"); // #1

		if(storedPasscode.equals(edtPasscode.getText().toString().trim())){
			
			Intent assignedDeviceIntent = new Intent(getApplicationContext(), AssignedDeviceActivity.class);
			startActivity(assignedDeviceIntent);
			finish();
		}else{
			
			Intent intentInvalidPasscode = new Intent(getApplicationContext(),InvalidPasscodeActivity.class);
			startActivity(intentInvalidPasscode);
			finish();

		}

	}

	
	
	public class MyPasswordTransformationMethod extends PasswordTransformationMethod {
	    @Override
	    public CharSequence getTransformation(CharSequence source, View view) {
	        return new PasswordCharSequence(source);
	    }

	    private class PasswordCharSequence implements CharSequence {
	        private CharSequence mSource;
	        public PasswordCharSequence(CharSequence source) {
	            mSource = source; // Store char sequence
	        }
	        public char charAt(int index) {
	            return '*'; // This is the important part
	        }
	        public int length() {
	            return mSource.length(); // Return default
	        }
	        public CharSequence subSequence(int start, int end) {
	            return mSource.subSequence(start, end); // Return default
	        }
	    }
	};
	
	
private void bluetoothOn(){
		
		if (bluetoothAdapter == null) {

		} else if (bluetoothAdapter.isEnabled()) {
				
							
		}else{
			
			bluetoothAdapter.enable();
		}
	}
}
