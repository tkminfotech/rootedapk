package com.tkm.optumSierra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tkm.optumSierra.dal.Glucose_db;
import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.Util;

public class ShowGlucoseActivity extends Titlewindow {

	private Glucose_db dbglucose = new Glucose_db(this);
	private String TAG = "SetGlucoseActivity Sierra";
	private Button NextButton = null, retryButton = null;
	private GlobalClass appClass;
	MeasureType_db dbsensor = new MeasureType_db(this);
	private int redirectFlag = 0;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_glucose);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		try {
			TextView msg = (TextView) findViewById(R.id.t);
			TextView textview1 = (TextView) findViewById(R.id.txt1);
			TextView textview2 = (TextView) findViewById(R.id.txt2);
			

			textview1.setTypeface(type, Typeface.NORMAL);
			textview2.setTypeface(type, Typeface.NORMAL);
			
			msg.setTypeface(type, Typeface.BOLD);
			String ss = extras.getString("text");
			if (extras.getString("text").trim().length() < 10) {
				
				msg.setText(extras.getString("text"));

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if (redirectFlag == 0) {
							playSound();
						}
					}
				}, 2000);

			} else {
				textview1.setText(extras.getString("text"));
				textview2.setVisibility(View.INVISIBLE);
				
				msg.setVisibility(View.INVISIBLE);
			}
			// tts.speak(extras.getString("text"), TextToSpeech.QUEUE_FLUSH,
			// null);

			
			Log.i(TAG, "----------on create  show glucose----------- ");
		} catch (Exception e) {
			// Log.e("Droid Error",e.getMessage());
			Log.i(TAG, " Exception show show glucose  on create ");
		}
		addListenerOnButton();
		
	}

@Override
protected void onResume() {
	// TODO Auto-generated method stub
	super.onResume();
	new Handler().postDelayed(new Runnable() {
		@Override
		public void run() {
			ActivityManager am = (ActivityManager) getApplicationContext()
					.getSystemService(Context.ACTIVITY_SERVICE);
			ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
			if (cn.getClassName().equals(
					"com.tkm.optumSierra.ShowGlucoseActivity"))
				redirect();
		}
	}, 24000);
}
	public void redirect() {
		if (redirectFlag == 0) {
			dbsensor.updateStatus(1);// update the vital taken

			Bundle extras = getIntent().getExtras();
			if (extras != null) {
				appClass.setBundle(extras);
			} else {
				extras = appClass.getBundle();
			}
			dbglucose.UpdateglucoseData_as_valid(extras.getInt("glucoseid"));

			Log.i(TAG, "NextButton clicked  show glucose---- ");
			SharedPreferences settings1 = getSharedPreferences(
					CommonUtilities.RETAKE_SP, 0);
			SharedPreferences.Editor editor_retake = settings1.edit();
			editor_retake.putInt("retake_status", 0);
			editor_retake.commit();

			Intent intent = new Intent(getApplicationContext(),
					UploadMeasurement.class);
			intent.putExtra("reType", 3);

			startActivity(intent);
			Util.Stopsoundplay();
			finish();
			Log.e(TAG, "Redirect to upload glucose");
		} else {
			Log.i(TAG, "Retake Pressed");
		}
	}

	@Override
	public void onStop() {
		redirectFlag = 1;
		Log.i(TAG, "------------------onStop------- show glucose----------");
		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
		Util.Stopsoundplay();
		super.onStop();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "USER KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	private void addListenerOnButton() {

		NextButton = (Button) findViewById(R.id.btnacceptglu);

		NextButton.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
//				if(appClass.isRemSessionStart){
//
//					appClass.uploadSessionStatusTime();
//				} else{

					appClass.uploadStatusTime();
//				}
				redirect();
			}
		});

		retryButton = (Button) findViewById(R.id.btnwtretakeglu);
		SharedPreferences settingsNew = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);
		Log.i(TAG, "retryButton clicked  show glucose---- ");
		int retake = settingsNew.getInt("retake_status", 0);

		if (retake == 1) {
			//retryButton.setVisibility(View.INVISIBLE);
		}

		retryButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.i(TAG, "retry clicked");
				Util.Stopsoundplay();
				delete_last_meassurement();
				// set retak sp
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.RETAKE_SP, 0);
				SharedPreferences.Editor editor_retake = settings.edit();
				editor_retake.putInt("retake_status", 1);
				editor_retake.commit();
				redirectFlag = 1;
				RedirectGlucose();
			}
		});
	}

	private void RedirectGlucose() {

		/*
		 * final Context context = this; Intent intent = new Intent(context,
		 * GlucoseEntry.class); this.finish(); startActivity(intent);
		 */

		Sensor_db dbSensor = new Sensor_db(this);
		String sensor_name = dbSensor.SelectGlucose_sensor_Name();

		if (sensor_name.contains("One Touch Ultra")) {

			Intent intent = new Intent(getApplicationContext(),
					GlucoseReader.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Bayer")) {

			Intent intent = new Intent(getApplicationContext(),
					GlucoseReader.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Accu-Chek")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						GlucoseAccuChek.class);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				finish();
				startActivity(intent);

			}

		} else if (sensor_name.contains("Taidoc")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						GlucoseTaidoc.class);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();

				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				finish();
				startActivity(intent);

			}

		}

		else if (sensor_name.trim().length() > 0) {
			Intent intent = new Intent(getApplicationContext(),
					GlucoseEntry.class);
			finish();
			startActivity(intent);
		}

		dbSensor.cursorsensor.close();
		dbSensor.close();

	}

	private void delete_last_meassurement() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		dbglucose.UpdateglucoseData(extras.getInt("glucoseid"), ShowGlucoseActivity.this);
		// FOR DELETING UPLOADED DATA CALL THIS
		dbglucose.deleteUploadedGlucose(ShowGlucoseActivity.this);

	}

	private void playSound() {

		NumberToString ns = new NumberToString();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		String no_to_string = "";

		if (extras.getString("text").contains(".")) {
			Double final_value = Double.parseDouble(extras.getString("text"));
			no_to_string = ns.getNumberToString(final_value);
		} else {
			Long final_value = Long.parseLong(extras.getString("text"));
			no_to_string = ns.getNumberToStringLong(final_value);
		}

		ArrayList<String> final_list = new ArrayList<String>();

		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){
			
			final_list.add("Messages/BLsugarLevelsat_1.wav");
		}else{
			
			final_list.add("Messages/BLsugarLevelsat.wav");
		}
		

		List<String> sellItems = Arrays.asList(no_to_string.split(" "));

		for (String item : sellItems) {
			if (item.toString().length() > 0) {
				final_list.add("Numbers/" + item.toString() + ".wav");
			}
		}

		if (final_list.get(final_list.size() - 1).toString()
				.equals("Numbers/zero.wav")) // remove if last item is zero
		{
			final_list.remove(final_list.size() - 1);
		}

		final_list.add("Messages/MllgmPerDecltr.wav");

		Util.playSound4FileList(final_list, getApplicationContext());
	}

}
