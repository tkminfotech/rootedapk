package com.tkm.optumSierra;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Util;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.text.Html;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ShowPressureActivity extends Titlewindow {

	private Button NextButton = null, reTryButton = null;
	private Sensor_db dbsensor = new Sensor_db(this);
	private String TAG = "ShowPressureActivity Sierra";
	private Pressure_db dbpressure = new Pressure_db(this);
	private GlobalClass appClass;
	MeasureType_db dbsensor1 = new MeasureType_db(this);
	private int redirectFlag = 0;

	@Override
	public void onStop() {
		Log.i(TAG, "-------------------onstop-------------- show bp  activity");
		/*
		 * Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
		 * Util.Stopsoundplay();
		 */
		redirectFlag = 1;
		super.onStop();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_pressure);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");

		// TextView systxt = (TextView) findViewById(R.id.sysp);
		// TextView diatxt = (TextView) findViewById(R.id.diap);
		// TextView pulsetxt = (TextView) findViewById(R.id.pulsep);
		TextView textview1 = (TextView) findViewById(R.id.txt1);
		// TextView textview2 = (TextView) findViewById(R.id.txt2);
		// TextView textview3 = (TextView) findViewById(R.id.txt3);

		textview1.setTypeface(type, Typeface.NORMAL);
		// textview2.setTypeface(type, Typeface.NORMAL);
		// textview3.setTypeface(type, Typeface.NORMAL);
		// systxt.setTypeface(type, Typeface.BOLD);
		// diatxt.setTypeface(type, Typeface.BOLD);
		// pulsetxt.setTypeface(type, Typeface.BOLD);
		try {

			if ((extras.getString("sys").equals("0"))
					&& (extras.getString("dia").equals("0"))) {

				textview1.setText(this
						.getString(R.string.unabletoconnectbluetooth));
				// systxt.setVisibility(View.INVISIBLE);
				// diatxt.setVisibility(View.INVISIBLE);
				// pulsetxt.setVisibility(View.INVISIBLE);
				// textview2.setVisibility(View.INVISIBLE);
				// textview3.setVisibility(View.INVISIBLE);

			} else {

				textview1.setText(Html.fromHtml(this
						.getString(R.string.yourbloodpressure)
						+ "<FONT COLOR=\"#D45D00\">"
						+ extras.getString("sys")
						+ "</FONT>"
						+ this.getString(R.string.over)
						+ "<FONT COLOR=\"#D45D00\">"
						+ extras.getString("dia")
						+ "</FONT>"
						+ this.getString(R.string.withpulserate)
						+ "<FONT COLOR=\"#D45D00\">"
						+ " "
						+ extras.getString("pulse")
						+ "</FONT>"
						+ this.getString(R.string.beatsperminute)));

				/*
				 * systxt.setText(extras.getString("sys"));
				 * diatxt.setText(extras.getString("dia"));
				 * pulsetxt.setText(extras.getString("pulse"));
				 */
				playSound();

			}

			Log.i(TAG,
					"---------------on create finished show pressure--------");
		} catch (Exception e) {
			// Log.e("Droid Error",e.getMessage());
			Log.i(TAG, " Exception show show pressure  on create ");
		}
		addListenerOnButton();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				ActivityManager am = (ActivityManager) getApplicationContext()
						.getSystemService(Context.ACTIVITY_SERVICE);
				ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
				if (cn.getClassName().equals(
						"com.tkm.optumSierra.ShowPressureActivity")) {
					if (redirectFlag == 0) {
						redirect();
					}
				}
			}
		}, 28000);
	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	public Boolean preference() {
		Boolean flag = null;
		try {

			String PREFS_NAME = "DroidPrefSc";
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			int scheduletatus = settings.getInt("Scheduletatus", -1);
			//Log.i("Droid", " SharedPreferences : " + getApplicationContext()
			//		+ " is : " + scheduletatus);
			if (scheduletatus == 1)
				flag = true;
			else
				flag = false;
		}

		catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		return flag;
	}

	private void redirectTemp() {

		String tmpMac = dbsensor.SelectTempSensorName();

		//Log.i(TAG, "tmpMac" + tmpMac);

		if (tmpMac.trim().length() > 4) {
			Intent intentfr = new Intent(getApplicationContext(),
					ForaMainActivity.class);
			intentfr.putExtra("deviceType", 2);
			intentfr.putExtra("macaddress", getForaMAC(tmpMac));

			startActivity(intentfr);
			finish();

		} else if (tmpMac.trim().length() > 0) {
			Intent intentfr = new Intent(getApplicationContext(),
					TemperatureEntry.class);

			startActivity(intentfr);
			finish();
		} else {
			/*
			 * Intent intentfr = new Intent(getApplicationContext(),
			 * ThanksActivity.class); startActivity(intentfr);
			 */
			// Toast.makeText(ShowPressureActivity.this,
			// "Please assign BT device.", Toast.LENGTH_LONG).show();
			redirectGlucode();
		}

	}

	private void redirectGlucode() {

		// String tmpMac = dbSensor.SelectGlucoName();
		String sensor_name = dbsensor.SelectGlucose_sensor_Name();

		if (sensor_name.contains("One Touch Ultra")) {

			Intent intent = new Intent(getApplicationContext(),
					GlucoseReader.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Bayer")) {

			Intent intent = new Intent(getApplicationContext(),
					GlucoseReader.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Accu-Chek")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						GlucoseAccuChek.class);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				finish();
				startActivity(intent);

			}

		} else if (sensor_name.contains("Taidoc")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						GlucoseTaidoc.class);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();

				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				finish();
				startActivity(intent);

			}

		} else if (sensor_name.trim().length() > 0) {
			Intent intent = new Intent(getApplicationContext(),
					GlucoseEntry.class);
			finish();
			startActivity(intent);
		} else {
			Intent intentfr = new Intent(getApplicationContext(),
					ThanksActivity.class);

			startActivity(intentfr);
			finish();
			// Toast.makeText(getApplicationContext(), "Please assign glucose.",
			// Toast.LENGTH_LONG).show();
		}
		dbsensor.cursorsensor.close();
		dbsensor.close();
	}

	private void addListenerOnButton() {

		NextButton = (Button) findViewById(R.id.btnacceptpulse);

		NextButton.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
//				if(appClass.isRemSessionStart){
//
//					appClass.uploadSessionStatusTime();
//				} else{

					appClass.uploadStatusTime();
//				}
				redirect();
			}
		});
		reTryButton = (Button) findViewById(R.id.btnwtretake);

		SharedPreferences settingsNew = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);

		int retake = settingsNew.getInt("retake_status", 0);

		if (retake == 1) {
			//reTryButton.setVisibility(View.INVISIBLE);
		}

		reTryButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "retry clicked");
				// set retak sp
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.RETAKE_SP, 0);
				SharedPreferences.Editor editor_retake = settings.edit();
				editor_retake.putInt("retake_status", 1);
				editor_retake.commit();
				delete_last_meassurement();
				redirectFlag = 1;
				RedirectPressure();

			}

		});

	}

	private void RedirectPressure() {

		Cursor cursorsensor = dbsensor.SelectBPSensorName();
		String Sensor_name = "", Mac = "";
		//Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));

		}
		//Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
		if (Sensor_name.contains("A and D Bluetooth smart")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						AandDSmart.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		}
		else if (Sensor_name.contains("Omron HEM 9200-T")) {
			if (Build.VERSION.SDK_INT >= 18) {
				Constants.setPIN(cursorsensor.getString(10));
				Intent intent = new Intent(getApplicationContext(),
						OmronBlsActivity.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		}

		else if (Sensor_name.contains("Wellex")) {
			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						PressureWellex.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		} else if (Sensor_name.contains("P724-BP")) {

			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						P724BpSmart.class);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {
				Log.i(TAG,
						"SensorName P724 : no ble fund redirecting to normal page ");
				Intent intent = new Intent(this, P724Bp.class);
				finish();
				startActivity(intent);
				return;
			}
		} else if (Sensor_name.contains("A and D")) {

			if (Sensor_name.contains("UA-767BT-Ci")) {
				Intent intentwt = new Intent(getApplicationContext(),
						AandContinua.class);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			} else {

				Intent intentwt = new Intent(getApplicationContext(),
						AandDReader.class);
				intentwt.putExtra("deviceType", 1);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			}

		} else if (Sensor_name.contains("FORA")) {
			if (Mac.trim().length() == 17) {
				Intent intentfr = new Intent(getApplicationContext(),
						ForaMainActivity.class);
				intentfr.putExtra("deviceType", 1); // pressure
				intentfr.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intentfr);
			}
			// Toast.makeText(this,
			// "Please assign BT device or Check MAC id for BP",
			// Toast.LENGTH_LONG).show();

		} else {
			final Context context = this;
			// Intent intent = new Intent(context,ThanksActivity.class); #
			// change for skip vitals
			Intent intent = new Intent(context, PressureEntry.class);
			this.finish();
			startActivity(intent);
			// Toast.makeText(this, "Please assign Blood Pressure BT device",
			// Toast.LENGTH_LONG).show();
		}
		cursorsensor.close();
		dbsensor.cursorsensor.close();
		dbsensor.close();
	}

	private void delete_last_meassurement() {

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		dbpressure.delete_pressure_data(extras.getInt("pressureid"));

	}

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			// macAddress.substring(i, i+2)
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		//Log.i(TAG, "DROID : Connect to macAddress " + macAddress);
		if (macAddress.trim().length() == 17) {
			return macAddress;
		} else {
			return "";
		}
	}

	String patient_id = "";

	public void UpdateMeasurementtoServer() {

		patient_id = Constants.getdroidPatientid() + "";
		Log.e(TAG, " UpdateMeasurementtoServer pressure : ");
		Update taskUpdate = new Update();
		taskUpdate.execute(new String[] { Constants.getPostUrl()
				+ "/droid_website/handler.ashx" });

	}

	private class Update extends AsyncTask<String, Void, String> {

		protected String doInBackground(String... urls) {

			String response = "";
			Log.i(TAG, "uploading started.");
			/********************************* Pressure **************************/

			try {

				Cursor cpressure = dbpressure.SelectMeasuredpressure();
				cpressure.moveToFirst();
				if (cpressure.getCount() > 0) {

					while (cpressure.isAfterLast() == false) {
						Log.i(TAG,
								"Uploading pressure  data to server started ...");

						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(Constants.getPostUrl()
								+ "/droid_website/add_tpm_blood_pressure.ashx");
						List<NameValuePair> pairs = new ArrayList<NameValuePair>();

						post.setEntity(new UrlEncodedFormEntity(pairs));

						post.setHeader("patient_id", cpressure.getString(1));
						post.setHeader("measure_date", cpressure.getString(5));
						post.setHeader("diastolic", cpressure.getString(3));
						post.setHeader("systolic", cpressure.getString(2));
						post.setHeader("pulse", cpressure.getString(4));
						post.setHeader("input_mode", cpressure.getString(7));
						post.setHeader("section_date", cpressure.getString(8));
						post.setHeader("prompt_flag", cpressure.getString(10));
						post.setHeader("body_movement", cpressure.getString(11));
						post.setHeader("irregular_pulse", cpressure.getString(12));
						post.setHeader("battery_level", cpressure.getString(13));
						post.setHeader("measurement_position_flag", cpressure.getString(14));
						post.setHeader("flag_cuff_fit_detection_flag", cpressure.getString(15));
						post.setHeader("is_reminder", cpressure.getString(16));

						HttpResponse response1 = client.execute(post);
						int a = response1.getStatusLine().getStatusCode();
						InputStream in = response1.getEntity().getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(in));
						StringBuilder str = new StringBuilder();
						String line = null;
						while ((line = reader.readLine()) != null) {
							str.append(line + "\n");
						}
						in.close();
						str.toString();
						//Log.i(TAG,
						//		"pressure response:   "+str);
						if (str.length() > 0) {
							DocumentBuilder db = DocumentBuilderFactory
									.newInstance().newDocumentBuilder();
							InputSource is1 = new InputSource();
							is1.setCharacterStream(new StringReader(str
									.toString()));
							Document doc = db.parse(is1);
							NodeList nodes = doc
									.getElementsByTagName("success_response");
							if (nodes.getLength() == 1) {
								// /// if success delete the value from the
								// tablet db
								//Log.i(TAG, "Update Pressure data with id ..."
								//		+ cpressure.getString(0));
//								if(appClass.isRemSessionStart){
//
//									appClass.uploadSessionStatus("success");
//								}else{

									appClass.uploadStatus("success");
//								}
								dbpressure.UpdatepressureData(Integer
										.parseInt(cpressure.getString(0)), ShowPressureActivity.this);
							}else{

//								if(appClass.isRemSessionStart){
//
//									appClass.uploadSessionStatus("failed");
//								}else{

									appClass.uploadStatus("failed");
//								}
							}

						}else{

//							if(appClass.isRemSessionStart){
//
//								appClass.uploadSessionStatus("failed");
//							}else{

								appClass.uploadStatus("failed");
//							}
						}
						cpressure.moveToNext();
					}
				}
				cpressure.close();
				dbpressure.cursorPreesure.close();
				dbpressure.close();

			} catch (Exception e) {
//				if(appClass.isRemSessionStart){
//
//					appClass.uploadSessionStatus("failed");
//				}else{

					appClass.uploadStatus("failed");
//				}
				String Error = this.getClass().getName()+ " : "+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Blood Pressure Data Upload Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.BP_failure_upload);
			}

			// DownloadAdviceMessage();
			return response;

		}

	}

	/*
	 * systxt.setText(extras.getString("sys"));
	 * diatxt.setText(extras.getString("dia"));
	 * pulsetxt.setText(extras.getString("pulse"));
	 * YourBPis,over,WPulseRateof,beatsPerMin
	 */

	ArrayList<String> final_list = new ArrayList<String>();

	private void playSound() {

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}

		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){

			final_list.add("Messages/YourBPis_1.wav");
		}else{

			final_list.add("Messages/YourBPis.wav");
		}

		add_to_finalList(extras.getString("sys"));		

		final_list.add("Messages/over.wav");
		
		add_to_finalList(extras.getString("dia"));
		if(languageId.equals("11")){

			final_list.add("Messages/WPulseRateof_1.wav");
		}else{

			final_list.add("Messages/WPulseRateof.wav");
		}

		add_to_finalList(extras.getString("pulse"));
		final_list.add("Messages/beatsPerMin.wav");

		Util.playSound4FileList(final_list, getApplicationContext());
	}

	private void add_to_finalList(String vitalValue) {
		String no_to_string = "";
		try {
			NumberToString ns = new NumberToString();

			if (vitalValue.contains(".")) {
				Double final_value = Double.parseDouble(vitalValue);
				no_to_string = ns.getNumberToString(final_value);
			} else {
				Long final_value = Long.parseLong(vitalValue);
				no_to_string = ns.getNumberToStringLong(final_value);
			}

			List<String> sellItems = Arrays.asList(no_to_string.split(" "));

			for (String item : sellItems) {
				if (item.toString().length() > 0) {
					final_list.add("Numbers/" + item.toString() + ".wav");
				}
			}

			if (final_list.get(final_list.size() - 1).toString()
					.equals("Numbers/zero.wav")) // remove if last item is zero
			{
				final_list.remove(final_list.size() - 1);
			}
		} catch (Exception e) {
			Log.e(TAG, "Play sound Exception in show bp page");
		}
	}

	private void redirect() {

		dbsensor1.updateStatus(3);// update the vitals taken

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		dbpressure.UpdatepressureData_as_valid(extras.getInt("pressureid"));

		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);
		SharedPreferences.Editor editor_retake = settings1.edit();
		editor_retake.putInt("retake_status", 0);
		editor_retake.commit();

		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = settings.getInt("flow", 0); // #1
		UpdateMeasurementtoServer();
		if (val == 1) {

			Intent intentSc = new Intent(getApplicationContext(),
					UploadMeasurement.class);
			Util.Stopsoundplay();
			finish();
			startActivity(intentSc);
			overridePendingTransition(0, 0);

		}

		else if (preference()) {
			Intent intentSc = new Intent(getApplicationContext(),
					UploadMeasurement.class);
			intentSc.putExtra("reType", 1);
			Util.Stopsoundplay();
			startActivity(intentSc);
			finish();
		} else {

			redirectTemp();
		}
	}
}
