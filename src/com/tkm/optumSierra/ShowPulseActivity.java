package com.tkm.optumSierra;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.tkm.optumSierra.dal.Pulse_db;
import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Util;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ShowPulseActivity extends Titlewindow {

	private Button NextButton = null, RetryButton = null;
	private Sensor_db dbSensor = new Sensor_db(this);
	private Pulse_db dbcreate1 = new Pulse_db(this);
	private String TAG = "ShowPulseActivity Sierra";
	private GlobalClass appClass;
	MeasureType_db dbsensor1 = new MeasureType_db(this);
	private int redirectFlag = 0;

	public Boolean preference() {
		Boolean flag = null;
		try {

			String PREFS_NAME = "DroidPrefSc";
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			int scheduletatus = settings.getInt("Scheduletatus", -1);
			Log.i("Droid", " SharedPreferences : " + getApplicationContext()
					+ " is : " + scheduletatus);
			if (scheduletatus == 1)
				flag = true;
			else
				flag = false;
		}

		catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		return flag;
	}

	@Override
	public void onStop() {

		/*
		 * Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
		 * Util.Stopsoundplay();
		 */
		redirectFlag = 1;
		super.onStop();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_pulse);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		TextView msg = (TextView) findViewById(R.id.t);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");

		TextView textview1 = (TextView) findViewById(R.id.txt1);
		TextView textview2 = (TextView) findViewById(R.id.txt2);
		TextView textview3 = (TextView) findViewById(R.id.txt3);

		textview1.setTypeface(type, Typeface.NORMAL);
		textview2.setTypeface(type, Typeface.NORMAL);
		textview3.setTypeface(type, Typeface.NORMAL);

		try {
			msg.setTypeface(type, Typeface.BOLD);

			msg.setText(extras.getString("pulse"));
			playSound();

			if (extras.getString("pulse").trim().length() > 10) {

				textview1.setVisibility(View.INVISIBLE);

			}

			/*
			 * try { Util.soundPlay("Sierra/Your pulse is.wav");
			 * 
			 * Thread.sleep(2000); } catch (InterruptedException e) { // TODO
			 * Auto-generated catch block //e.printStackTrace(); }
			 * //tts.speak(extras.getString("bop"), TextToSpeech.QUEUE_FLUSH,
			 * null);
			 */

			Log.i(TAG, "On create--- show pulse");

		} catch (Exception e) {
			// Log.e("Droid Error",e.getMessage());
			Log.i(TAG, " Exception show show pulse  on create ");
		}
		addListenerOnButton();
		
	}

	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				ActivityManager am = (ActivityManager) getApplicationContext()
						.getSystemService(Context.ACTIVITY_SERVICE);
				ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
				if (cn.getClassName().equals(
						"com.tkm.optumSierra.ShowPulseActivity")) {
					if (redirectFlag == 0) {
						redirect();
					}
				}
			}
		}, 22000);
	}
	@Override
	protected void onStart() {

		Log.i(TAG, "onStart");
		super.onStart();
	}

	private void addListenerOnButton() {

		NextButton = (Button) findViewById(R.id.btnaccept);

		NextButton.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
//				if(appClass.isRemSessionStart){
//
//					appClass.uploadSessionStatusTime();
//				} else{

					appClass.uploadStatusTime();
//				}
				redirect();
			}
		});
		RetryButton = (Button) findViewById(R.id.btnwtretake);

		SharedPreferences settingsNew = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);

		int retake = settingsNew.getInt("retake_status", 0);

		if (retake == 1) {
			//RetryButton.setVisibility(View.INVISIBLE);
		}

		RetryButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "retry clicked");
				Util.Stopsoundplay();
				delete_last_meassurement();
				// set retak sp
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.RETAKE_SP, 0);
				SharedPreferences.Editor editor_retake = settings.edit();
				editor_retake.putInt("retake_status", 1);
				editor_retake.commit();
				redirectFlag = 1;
				RedirectPulse();

			}

		});
	}

	private void delete_last_meassurement() {

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		dbcreate1.delete_pulse_data(extras.getInt("pulseid"));
	}

	protected void RedirectPulse() {
		Log.i(TAG, "Rdirctin to Datareader activity : ");

		Cursor cursorsensor = dbSensor.SelectMeasuredweight();
		String Sensor_name = "", Mac = "";
		Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));

		}
		cursorsensor.close();
		dbSensor.cursorsensor.close();
		dbSensor.close();
		Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
		if (Mac.trim().length() == 17) {

			if (Sensor_name.contains("3230")) {
				Intent intent = new Intent(getApplicationContext(),
						BLECheckActivity.class);
				intent.putExtra("macaddress", Mac);
				startActivity(intent);
				finish();
			} else if (Sensor_name.contains("9560")) {
				Intent intent = new Intent(getApplicationContext(),
						DataReaderActivity.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
			}

		} else if (Sensor_name.contains("Manual")) {
			Intent intent = new Intent(getApplicationContext(),
					PulseEntry.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		}

	}

	private void PresureActivity() {

		Cursor cursorsensor = dbSensor.SelectBPSensorName();
		String Sensor_name = "", Mac = "";
		Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));

		}
		Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
		if (Sensor_name.contains("A and D Bluetooth smart")) {
			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						AandDSmart.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		} else if (Sensor_name.contains("Omron HEM 9200-T")) {
			if (Build.VERSION.SDK_INT >= 18) {
				Constants.setPIN(cursorsensor.getString(10));
				Intent intent = new Intent(getApplicationContext(),
						OmronBlsActivity.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

		}

		else if (Sensor_name.contains("Wellex")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						PressureWellex.class);
				intent.putExtra("macaddress", Mac);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {

				Toast.makeText(getBaseContext(), "no ble found",
						Toast.LENGTH_SHORT).show();
				finish();
				return;
			}
		} else if (Sensor_name.contains("P724-BP")) {

			if (Build.VERSION.SDK_INT >= 18) {

				Intent intent = new Intent(getApplicationContext(),
						P724BpSmart.class);
				this.finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {
				Log.i(TAG,
						"SensorName P724 : no ble fund redirecting to normal page ");
				Intent intent = new Intent(this, P724Bp.class);
				finish();
				startActivity(intent);
				return;
			}
		} else if (Sensor_name.contains("A and D")) {

			if (Sensor_name.contains("UA-767BT-Ci")) {
				Intent intentwt = new Intent(getApplicationContext(),
						AandContinua.class);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			} else {

				Intent intentwt = new Intent(getApplicationContext(),
						AandDReader.class);
				intentwt.putExtra("deviceType", 1);
				this.finish();
				startActivity(intentwt);
				overridePendingTransition(0, 0);
			}

		} else if (Sensor_name.contains("FORA")) {
			if (Mac.trim().length() == 17) {
				Intent intentfr = new Intent(getApplicationContext(),
						ForaMainActivity.class);
				intentfr.putExtra("deviceType", 1); // pressure
				intentfr.putExtra("macaddress", Mac);

				startActivity(intentfr);
				finish();
			} else {
				Intent intentfr = new Intent(getApplicationContext(),
						ThanksActivity.class);

				startActivity(intentfr);
				finish();
				// Toast.makeText(getApplicationContext(),
				// "Please assign BT device or Check MAC id",
				// Toast.LENGTH_LONG).show();
			}
		} else if (Sensor_name.trim().length() > 0) {
			Intent intentfr = new Intent(getApplicationContext(),
					PressureEntry.class);

			startActivity(intentfr);
			finish();
		} else {
			redirectTemp();
			/*
			 * Intent intentfr = new
			 * Intent(getApplicationContext(),ThanksActivity.class);
			 * startActivity(intentfr);
			 */
			// Toast.makeText(getApplicationContext(),
			// "Please assign BT device", Toast.LENGTH_LONG).show();
		}
		cursorsensor.close();
		dbSensor.cursorsensor.close();
		dbSensor.close();

	}

	private void redirectTemp() {

		String tmpMac = dbSensor.SelectTempSensorName();

		Log.i(TAG, "tmpMac" + tmpMac);

		if (tmpMac.trim().length() > 4) {
			Intent intentfr = new Intent(getApplicationContext(),
					ForaMainActivity.class);
			intentfr.putExtra("deviceType", 2);
			intentfr.putExtra("macaddress", getForaMAC(tmpMac));

			startActivity(intentfr);
			finish();

		} else if (tmpMac.trim().length() > 0) {
			Intent intentfr = new Intent(getApplicationContext(),
					TemperatureEntry.class);

			startActivity(intentfr);
			finish();
		} else {
			/*
			 * Intent intentfr = new Intent(getApplicationContext(),
			 * ThanksActivity.class); startActivity(intentfr);
			 */
			// Toast.makeText(this,
			// "Please assign tmp BT device.", Toast.LENGTH_LONG).show();
			redirectGlucode();
		}

	}

	private void redirectGlucode() {

		// String tmpMac = dbSensor.SelectGlucoName();
		String sensor_name = dbSensor.SelectGlucose_sensor_Name();

		if (sensor_name.contains("One Touch Ultra")) {

			Intent intent = new Intent(getApplicationContext(),
					GlucoseReader.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Bayer")) {

			Intent intent = new Intent(getApplicationContext(),
					GlucoseReader.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		} else if (sensor_name.contains("Accu-Chek")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						GlucoseAccuChek.class);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				finish();
				startActivity(intent);

			}

		} else if (sensor_name.contains("Taidoc")) {

			if (Build.VERSION.SDK_INT >= 18) {
				Intent intent = new Intent(getApplicationContext(),
						GlucoseTaidoc.class);
				startActivity(intent);
				finish();
			} else {

				Toast.makeText(getBaseContext(), "not support ble",
						Toast.LENGTH_SHORT).show();

				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				finish();
				startActivity(intent);

			}

		} else if (sensor_name.trim().length() > 0) {
			Intent intent = new Intent(getApplicationContext(),
					GlucoseEntry.class);
			finish();
			startActivity(intent);
		} else {
			Intent intentfr = new Intent(getApplicationContext(),
					ThanksActivity.class);

			startActivity(intentfr);
			finish();
			// Toast.makeText(getApplicationContext(), "Please assign glucose.",
			// Toast.LENGTH_LONG).show();
		}
		dbSensor.cursorsensor.close();
		dbSensor.close();
	}

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			// macAddress.substring(i, i+2)
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		Log.i(TAG, " : Connect to macAddress " + macAddress);
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}

	String patient_id = "";
	String[] measure_date = new String[] {};
	int[] blood_oxygen, pulse, Measurement_Id;

	public void UpdateMeasurementtoServer() {

		patient_id = Constants.getdroidPatientid() + "";
		Log.e(TAG, " UpdateMeasurementtoServer pulse : ");
		Update taskUpdate = new Update();
		taskUpdate.execute(new String[] { Constants.getPostUrl()
				+ "/droid_website/handler.ashx" });

	}

	private class Update extends AsyncTask<String, Void, String> {

		protected String doInBackground(String... urls) {

			String response = "";
			Log.i(TAG, "uploading started Oxgen from show pulse.");
			try {
				/***************** Oxgen ********************/

				Cursor c = dbcreate1.SelectMeasuredData();
				measure_date = new String[c.getCount()];
				blood_oxygen = new int[c.getCount()];
				pulse = new int[c.getCount()];
				Measurement_Id = new int[c.getCount()];

				c.moveToFirst();
				if (c.getCount() > 0) {
					int i = 0;
					while (c.isAfterLast() == false) {
						Log.i(TAG,
								"Uploading oxygen measure data to server started ...");
						InputStream content = null;
						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(Constants.getPostUrl()
								+ "/droid_website/add_tpm_blood_oxygen.ashx");
						List<NameValuePair> pairs = new ArrayList<NameValuePair>();

						post.setEntity(new UrlEncodedFormEntity(pairs));

						post.setHeader("patient_id", c.getString(5));
						post.setHeader("measure_date", c.getString(3));
						post.setHeader("blood_oxygen", c.getString(1));
						post.setHeader("pulse", c.getString(2));
						post.setHeader("input_mode", c.getString(6));
						post.setHeader("section_date", c.getString(7));
						post.setHeader("is_reminder", c.getString(9));

						HttpResponse response1 = client.execute(post);
						int a = response1.getStatusLine().getStatusCode();
						InputStream in = response1.getEntity().getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(in));
						StringBuilder str = new StringBuilder();
						String line = null;
						while ((line = reader.readLine()) != null) {
							str.append(line + "\n");
						}
						in.close();
						str.toString();
						if (str.length() > 0) {
							DocumentBuilder db = DocumentBuilderFactory
									.newInstance().newDocumentBuilder();
							InputSource is1 = new InputSource();
							is1.setCharacterStream(new StringReader(str
									.toString()));
							Document doc = db.parse(is1);
							NodeList nodes = doc
									.getElementsByTagName("success_response");
							if (nodes.getLength() == 1) {
								// /// if success delete the value from the
								// tablet db
//								if(appClass.isRemSessionStart){
//
//									appClass.uploadSessionStatus("success");
//								}else{

									appClass.uploadStatus("success");
//								}
								Log.i(TAG, "Update Measure data with id ..."
										+ c.getString(0));
								dbcreate1.UpdateMeasureData(Integer.parseInt(c
										.getString(0)), ShowPulseActivity.this);
							} else{
								
//								if(appClass.isRemSessionStart){
//
//									appClass.uploadSessionStatus("failed");
//								}else{
									
									appClass.uploadStatus("failed");
//								}
							}

						} else{
							
//							if(appClass.isRemSessionStart){
//
//								appClass.uploadSessionStatus("failed");
//							}else{
								
								appClass.uploadStatus("failed");
//							}
						}
						c.moveToNext();

					}
				}
				c.close();
				dbcreate1.cursorMeasurement.close();
				dbcreate1.close();

			} catch (Exception e) {
//				if(appClass.isRemSessionStart){
//
//					appClass.uploadSessionStatus("failed");
//				}else{
					
					appClass.uploadStatus("failed");
//				}
				String Error = this.getClass().getName()+ " : "+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Pulse Data Upload Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.SpO2_failure_upload);
			}

			// DownloadAdviceMessage();
			return response;

		}

	}

	ArrayList<String> final_list = new ArrayList<String>();

	private void playSound() {

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		final_list.add("Messages/BL-OXlevelsAt.wav");
		add_to_finalList(extras.getString("pulse"));
		final_list.add("Messages/percent.wav");

		Util.playSound4FileList(final_list, getApplicationContext());
	}

	private void add_to_finalList(String vitalValue) {
		String no_to_string = "";
		NumberToString ns = new NumberToString();

		if (vitalValue.contains(".")) {
			Double final_value = Double.parseDouble(vitalValue);
			no_to_string = ns.getNumberToString(final_value);
		} else {
			Long final_value = Long.parseLong(vitalValue);
			no_to_string = ns.getNumberToStringLong(final_value);
		}

		List<String> sellItems = Arrays.asList(no_to_string.split(" "));

		for (String item : sellItems) {
			if (item.toString().length() > 0) {
				final_list.add("Numbers/" + item.toString() + ".wav");
			}
		}

		if (final_list.get(final_list.size() - 1).toString()
				.equals("Numbers/zero.wav")) // remove if last item is zero
		{
			final_list.remove(final_list.size() - 1);
		}
	}

	private void redirect() {
		dbsensor1.updateStatus(2);// vitals taken
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		dbcreate1.UpdateMeasureData_as_valid(extras.getInt("pulseid"));

		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);
		SharedPreferences.Editor editor_retake = settings1.edit();
		editor_retake.putInt("retake_status", 0);
		editor_retake.commit();

		UpdateMeasurementtoServer();
		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = settings.getInt("flow", 0); // #1

		if (val == 1) {

			Intent intentSc = new Intent(getApplicationContext(),
					UploadMeasurement.class);
			finish();
			startActivity(intentSc);
			Util.Stopsoundplay();
			overridePendingTransition(0, 0);

		}

		else if (preference()) {
			Intent intentSc = new Intent(getApplicationContext(),
					UploadMeasurement.class);
			intentSc.putExtra("reType", 1);

			startActivity(intentSc);
			finish();
			Util.Stopsoundplay();
			overridePendingTransition(0, 0);
			Log.e(TAG, "redirect to upload pulse page");
		} else {
			PresureActivity();
		}
	}
}
