package com.tkm.optumSierra;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.dal.Temperature_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Util;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ShowTemperatureActivity extends Titlewindow {

	private Button NextButton = null, retryButon = null;
	private GlobalClass appClass;
	private Sensor_db dbSensor = new Sensor_db(this);
	private String TAG = "ShowTemperatureActivity Sierra";
	private Temperature_db dbtemparature = new Temperature_db(this);
	MeasureType_db dbsensor1 = new MeasureType_db(this);
	private int redirectFlag = 0;
	OnInit_Data_Access dataAccess = new OnInit_Data_Access();
	double tempValue =0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_temperature);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");

		TextView msg = (TextView) findViewById(R.id.t);
		TextView textview1 = (TextView) findViewById(R.id.txt1);
		TextView textview2 = (TextView) findViewById(R.id.txt2);

		textview1.setTypeface(type, Typeface.NORMAL);
		textview2.setTypeface(type, Typeface.NORMAL);
		msg.setTypeface(type, Typeface.BOLD);
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		
		
		try {
			if (extras.getString("temp").trim().length() >= 1) {
				
				tempValue = Double.parseDouble(extras.getString("temp"));
				
				if(settingsUserSp.getInt("temperature_unit_id", -1) == 1){
					textview2.setText(getResources().getText(R.string.unittemperatureC));
					int manualEntry = settingsUserSp.getInt("manualEntry", 0);
					
					tempValue = Util.round((tempValue -32)/1.8);						
						
					
				} else{
					textview2.setText(getResources().getText(R.string.unittemperatureF));
					
				}
				msg.setText("" + tempValue);
				playSound();

			} else {

				textview2.setVisibility(View.INVISIBLE);
				msg.setVisibility(View.INVISIBLE);
				textview1.setText(this.getString(R.string.yourtemperature));
			}

		} catch (Exception e) {
			// Log.e("Droid Error",e.getMessage());
			Log.i(TAG, " Exception show show tepm  on create ");
		}
		SharedPreferences.Editor editor = settingsUserSp.edit();
        editor.putInt("manualEntry", 0);
        editor.commit();
		addListenerOnButton();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				ActivityManager am = (ActivityManager) getApplicationContext()
						.getSystemService(Context.ACTIVITY_SERVICE);
				ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
				if (cn.getClassName().equals(
						"com.tkm.optumSierra.ShowTemperatureActivity")) {
					if (redirectFlag == 0) {
						redirect();
					}
				}
			}
		}, 22000);
	}
	public Boolean preference() {
		Boolean flag = null;
		try {

			String PREFS_NAME = "DroidPrefSc";
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			int scheduletatus = settings.getInt("Scheduletatus", -1);
			Log.i("Droid", " SharedPreferences : " + getApplicationContext()
			+ " is : " + scheduletatus);
			if (scheduletatus == 1)
				flag = true;
			else
				flag = false;
		}

		catch (Exception e) {
			// Log.e("Droid Error",e.getMessage());
			Log.i(TAG, "preference() Exception show glucose----------- ");
		}
		return flag;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onStop() {

		Log.i(TAG, "----------on stop temp----------- ");
		/*
		 * Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
		 * Util.Stopsoundplay();
		 */
		redirectFlag = 1;
		super.onStop();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, " KEYCODE_BACK clicked: show glucose");

			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	private void addListenerOnButton() {

		NextButton = (Button) findViewById(R.id.btnacceptTemp);

		NextButton.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				//				if(appClass.isRemSessionStart){
				//
				//					appClass.uploadSessionStatusTime();
				//				} else{

				appClass.uploadStatusTime();
				//				}
				redirect();
			}
		});
		retryButon = (Button) findViewById(R.id.btnwtretaketemp);

		SharedPreferences settingsNew = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);

		int retake = settingsNew.getInt("retake_status", 0);

		if (retake == 1) {
			//retryButon.setVisibility(View.INVISIBLE);
		}

		retryButon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "retry clicked");
				Util.Stopsoundplay();
				delete_last_meassurement();

				// set retak sp
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.RETAKE_SP, 0);
				SharedPreferences.Editor editor_retake = settings.edit();
				editor_retake.putInt("retake_status", 1);
				editor_retake.commit();
				redirectFlag = 1;
				RedirectTemperature();

			}

		});

	}

	private void RedirectTemperature() {
		String tmpMac = dbSensor.SelectTempSensorName();
		dbSensor.close();

		Log.i(TAG, "tmpMac" + tmpMac);

		if (tmpMac.trim().length() > 4) {
			Intent intentfr = new Intent(getApplicationContext(),
					ForaMainActivity.class);
			intentfr.putExtra("deviceType", 2);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
			intentfr.putExtra("macaddress", getForaMAC(tmpMac));
			this.finish();
			startActivity(intentfr);

		} else {
			Intent intentfr = new Intent(getApplicationContext(),
					TemperatureEntry.class);
			this.finish();
			startActivity(intentfr);
		}

	}

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			// macAddress.substring(i, i+2)
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		Log.i(TAG, " : Connect to macAddress " + macAddress);
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}

	private void delete_last_meassurement() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		dbtemparature.delete_pulse_data(extras.getInt("tempid"));

	}

	private String patient_id;

	public void UpdateMeasurementtoServer() {

		patient_id = Constants.getdroidPatientid() + "";
		Log.e(TAG, " UpdateMeasurementtoServer Temperature : ");
		Update taskUpdate = new Update();
		taskUpdate.execute(new String[] { Constants.getPostUrl()
				+ "/droid_website/handler.ashx" });

	}

	private class Update extends AsyncTask<String, Void, String> {

		protected String doInBackground(String... urls) {

			String response = "";
			Log.i(TAG, "uploading started.");

			/********************************* Temperature **************************/
			try {

				Cursor ctemperature = dbtemparature.SelectMeasuredTemperature();
				ctemperature.moveToFirst();
				if (ctemperature.getCount() > 0) {

					while (ctemperature.isAfterLast() == false) {
						Log.i(TAG,
								"Uploading temparature data to server started ...");

						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(
								Constants.getPostUrl()
								+ "/droid_website/add_tpm_body_temperature.ashx");

						List<NameValuePair> pairs = new ArrayList<NameValuePair>();
						post.setEntity(new UrlEncodedFormEntity(pairs));
						post.setHeader("patient_id", ctemperature.getString(4));
						post.setHeader("measure_date",
								ctemperature.getString(2));
						post.setHeader("fahrenheit", ctemperature.getString(1));
						post.setHeader("input_mode", ctemperature.getString(5));
						post.setHeader("section_date",
								ctemperature.getString(6));
						post.setHeader("is_reminder", ctemperature.getString(8));
						HttpResponse response1 = client.execute(post);
						int a = response1.getStatusLine().getStatusCode();
						InputStream in = response1.getEntity().getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(in));
						StringBuilder str = new StringBuilder();
						String line = null;
						while ((line = reader.readLine()) != null) {
							str.append(line + "\n");
						}

						in.close();
						str.toString();
						if (str.length() > 0) {
							DocumentBuilder db = DocumentBuilderFactory
									.newInstance().newDocumentBuilder();
							InputSource is1 = new InputSource();
							is1.setCharacterStream(new StringReader(str
									.toString()));
							Document doc = db.parse(is1);
							NodeList nodes = doc
									.getElementsByTagName("success_response");
							if (nodes.getLength() == 1) {
								// /// if success delete the value from the
								// tablet db
								//								if(appClass.isRemSessionStart){
								//
								//									appClass.uploadSessionStatus("success");
								//								}else{

								appClass.uploadStatus("success");
								//								}
								Log.i(TAG,
										"Update temperature data with id ..."
												+ ctemperature.getString(0));
								dbtemparature.UpdatetemparatureData(Integer
										.parseInt(ctemperature.getString(0)), ShowTemperatureActivity.this);
							} else{

								//								if(appClass.isRemSessionStart){
								//
								//									appClass.uploadSessionStatus("failed");
								//								}else{

								appClass.uploadStatus("failed");
								//								}
							}
						} else{

							//							if(appClass.isRemSessionStart){
							//
							//								appClass.uploadSessionStatus("failed");
							//							}else{

							appClass.uploadStatus("failed");
							//							}
						}
						ctemperature.moveToNext();
					}
				}
				ctemperature.close();
				dbtemparature.cursorTemperature.close();
				dbtemparature.close();

			} catch (Exception e) {
				//				if(appClass.isRemSessionStart){
				//
				//					appClass.uploadSessionStatus("failed");
				//				}else{

				appClass.uploadStatus("failed");
				//				}
				String Error = this.getClass().getName()+ " : "+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Temperature Data Upload Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Temp_failure_upload);
			}
			// DownloadAdviceMessage();
			return response;

		}

	}

	ArrayList<String> final_list = new ArrayList<String>();

	private void playSound() {

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		final_list.add("Messages/TempIs.wav");
		add_to_finalList();
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		if(settingsUserSp.getInt("temperature_unit_id", -1) == 1){
			final_list.add("Messages/degreesC.wav");

		} else {
			final_list.add("Messages/degreesF.wav");
		}


		Util.playSound4FileList(final_list, getApplicationContext());
	}

	private void add_to_finalList() {
		String no_to_string = "";
		NumberToString ns = new NumberToString();

		if (Double.toString(tempValue).contains(".")) {
			Double final_value = tempValue;
			no_to_string = ns.getNumberToString(final_value);
		} else {
			Long final_value = (long) tempValue;
			no_to_string = ns.getNumberToStringLong(final_value);
		}

		List<String> sellItems = Arrays.asList(no_to_string.split(" "));

		for (String item : sellItems) {
			if (item.toString().length() > 0) {
				final_list.add("Numbers/" + item.toString() + ".wav");
			}
		}

		if (final_list.get(final_list.size() - 1).toString()
				.equals("Numbers/zero.wav")) // remove if last item is zero
		{
			final_list.remove(final_list.size() - 1);
		}
	}

	private void redirect() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		dbsensor1.updateStatus(6); // update the vitals as taken
		dbtemparature.UpdatetemparatureData_as_valid(extras.getInt("tempid"));
		Log.i(TAG, "accept clicked");
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);
		SharedPreferences.Editor editor_retake = settings1.edit();
		editor_retake.putInt("retake_status", 0);
		editor_retake.commit();

		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = settings.getInt("flow", 0); // #1
		Util.Stopsoundplay();
		if (val == 1) {

			Intent intentSc = new Intent(getApplicationContext(),
					UploadMeasurement.class);
			Util.Stopsoundplay();
			finish();
			startActivity(intentSc);

			overridePendingTransition(0, 0);

		}

		else if (preference()) {
			Intent intentSc = new Intent(getApplicationContext(),
					UploadMeasurement.class);
			intentSc.putExtra("reType", 1);

			startActivity(intentSc);
			finish();
			Log.e(TAG, "redirect to upload temp page");
		} else {
			UpdateMeasurementtoServer();

			// String tmpMac = dbSensor.SelectGlucoName();
			String sensor_name = dbSensor.SelectGlucose_sensor_Name();
			dbSensor.close();

			if (sensor_name.contains("One Touch Ultra")) {

				Intent intent = new Intent(getApplicationContext(),
						GlucoseReader.class);
				finish();
				startActivity(intent);
				overridePendingTransition(0, 0);

			} else if (sensor_name.contains("Bayer")) {

				Intent intent = new Intent(getApplicationContext(),
						GlucoseReader.class);

				finish();
				startActivity(intent);
				overridePendingTransition(0, 0);

			} else if (sensor_name.contains("Accu-Chek")) {

				if (Build.VERSION.SDK_INT >= 18) {
					Intent intent = new Intent(getApplicationContext(),
							GlucoseAccuChek.class);
					startActivity(intent);
					finish();
				} else {

					Toast.makeText(getBaseContext(), "not support ble",
							Toast.LENGTH_SHORT).show();
					Intent intent = new Intent(getApplicationContext(),
							GlucoseEntry.class);
					finish();
					startActivity(intent);

				}

			} else if (sensor_name.contains("Taidoc")) {

				if (Build.VERSION.SDK_INT >= 18) {
					Intent intent = new Intent(getApplicationContext(),
							GlucoseTaidoc.class);
					startActivity(intent);
					finish();
				} else {

					Toast.makeText(getBaseContext(), "not support ble",
							Toast.LENGTH_SHORT).show();

					Intent intent = new Intent(getApplicationContext(),
							GlucoseEntry.class);
					finish();
					startActivity(intent);

				}

			} else if (sensor_name.trim().length() > 0) {
				Intent intent = new Intent(getApplicationContext(),
						GlucoseEntry.class);
				finish();
				startActivity(intent);
				overridePendingTransition(0, 0);
			} else {
				Intent intentfr = new Intent(getApplicationContext(),
						ThanksActivity.class);

				startActivity(intentfr);
				overridePendingTransition(0, 0);
				finish();
				// Toast.makeText(getApplicationContext(),
				// "Please assign glucose.",
				// Toast.LENGTH_LONG).show();
			}
		}
	}
}
