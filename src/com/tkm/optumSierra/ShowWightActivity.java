package com.tkm.optumSierra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.dal.Weight_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.FontsOverride;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.Util;

import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ShowWightActivity extends Titlewindow {

	Sensor_db dbSensor = new Sensor_db(this);
	Weight_db dbCreateWeight = new Weight_db(this);
	private Button NextButton = null, RetryButton = null;
	private String TAG = "ShowWeightActivity";
	public int vala = 0;
	MeasureType_db dbsensor1 = new MeasureType_db(this);
	private GlobalClass appClass;
	private int redirectFlag = 0;

	@Override
	public void onStop() {

		/*
		 * Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
		 * Util.Stopsoundplay();
		 */
		redirectFlag = 1;
		super.onStop();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, " KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);

			this.finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_wight);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		TextView msg = (TextView) findViewById(R.id.t);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");

		String unit = dbSensor.measureunitName();

		TextView textview1 = (TextView) findViewById(R.id.txt1);
		TextView textview2 = (TextView) findViewById(R.id.txt2);
		int u = 0;
		try {
			if (unit.contains("Kilogram")) {

				int language = Constants.getLanguage();
				if (language == 11) {
					textview2.setText(" kilogramos");
				} else {
					textview2.setText(" kilograms");
				}

				u = 1;
			}
			textview1.setTypeface(type, Typeface.NORMAL);
			msg.setTypeface(type, Typeface.BOLD);
			textview2.setTypeface(type, Typeface.NORMAL);
			vala = extras.getInt("type");

			Log.i("Optum", "membert unit : " + unit);

			if (extras.getString("text").trim().length() < 10) {

				playSound(u);

				if (u == 1) {
					if (vala == 1) {
						String result_re = ""
								+ (Double) Util.round((((Double) Double
										.parseDouble(extras.getString("text")
												.trim()))));
						msg.setText(result_re);

					} else {
						String result_re = ""
								+ (Double) Util.round((((Double) Double
										.parseDouble(extras.getString("text")
												.trim()))) / 2.204);
						msg.setText(result_re);
					}
				} else {
					msg.setText(extras.getString("text"));
				}

			} else {
				textview1.setVisibility(View.INVISIBLE);
				textview2.setVisibility(View.INVISIBLE);
				msg.setText(extras.getString("text"));
			}
			// tts.speak(extras.getString("text"), TextToSpeech.QUEUE_FLUSH,
			// null);
			final ViewGroup mContainer = (ViewGroup) findViewById(
					android.R.id.content).getRootView();
			FontsOverride.overrideFonts(getApplicationContext(), mContainer);
		} catch (Exception e) {
			// Log.e("Droid Error",e.getMessage());
			Log.i(TAG, " Exception show wt on create ");
		}

		addListenerOnButton();
		Log.i(TAG, "--oncreate completed show wt---- ");
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				ActivityManager am = (ActivityManager) getApplicationContext()
						.getSystemService(Context.ACTIVITY_SERVICE);
				ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
				if (cn.getClassName().equals(
						"com.tkm.optumSierra.ShowWightActivity")) {
					if (redirectFlag == 0) {
						redirect();
					}
				}
			}
		}, 22000);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	private void addListenerOnButton() {

		NextButton = (Button) findViewById(R.id.btnaccept);

		NextButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
//				if(appClass.isRemSessionStart){
//
//					appClass.uploadSessionStatusTime();
//				} else{

					appClass.uploadStatusTime();
//				}
				redirect();
			}
		});
		RetryButton = (Button) findViewById(R.id.btnwtretake);

		Log.i(TAG, "RetryButton clicked wt page- ");
		SharedPreferences settingsNew = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);

		int retake = settingsNew.getInt("retake_status", 0);

		if (retake == 1) {
			//RetryButton.setVisibility(View.INVISIBLE);
		}

		RetryButton.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				Util.Stopsoundplay();
				delete_last_meassurement();

				// set retak sp
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.RETAKE_SP, 0);
				SharedPreferences.Editor editor_retake = settings.edit();
				editor_retake.putInt("retake_status", 1);
				editor_retake.commit();
				redirectFlag = 1;
				RedirectWeight();
			}

		});
	}

	private void delete_last_meassurement() {

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}
		dbCreateWeight.delete_weight_data(extras.getInt("measureId"));

	}

	private void RedirectWeight() {

		Cursor cursorsensor = dbSensor.SelectWeightSensorName();
		String Sensor_name = "", Mac = "";
		Log.i("Droid", "First SensorName : " + Sensor_name);
		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {
			Sensor_name = cursorsensor.getString(3);
			Mac = getForaMAC(cursorsensor.getString(9));
		}
		cursorsensor.close();
		dbSensor.cursorsensor.close();
		dbSensor.close();

		Log.i(TAG, "SensorName : " + Sensor_name + " MAC : " + Mac);
		if (Sensor_name.contains("A and D")) {
			Intent intentwt = new Intent(getApplicationContext(),
					AandDReader.class);
			// Intent intentwt = new
			// Intent(getApplicationContext(),TemperatureEntry.class);
			// intentwt.putExtra("macaddress", "00:1C:05:00:40:64");
			intentwt.putExtra("deviceType", 0);
			this.finish();
			startActivity(intentwt);
			overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("FORA")) {
			Intent intentfr = new Intent(getApplicationContext(),
					ForaMainActivity.class);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:86:FB");
			intentfr.putExtra("deviceType", 0);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
			intentfr.putExtra("macaddress", Mac);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else if (Sensor_name.contains("P724")) {
			Intent intentfr = new Intent(getApplicationContext(),
					P724Activity.class);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:86:FB");
			intentfr.putExtra("deviceType", 0);
			// intentfr.putExtra("macaddress", "00:12:A1:B0:9F:BD");
			intentfr.putExtra("macaddress", Mac);
			this.finish();
			startActivity(intentfr);
			overridePendingTransition(0, 0);
		} else {
			final Context context = this;
			// Intent intent = new Intent(context,ThanksActivity.class); #
			// change for skip vitals
			Intent intent = new Intent(context, WeightEntry.class);
			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);
		}

	}

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			// macAddress.substring(i, i+2)
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		Log.i(TAG, " : Connect to macAddress " + macAddress);
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}

	private void playSound(int unit_type) {
		Log.i(TAG, "play sound started ");
		try {
			NumberToString ns = new NumberToString();
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
				appClass.setBundle(extras);
			} else {
				extras = appClass.getBundle();
			}
			String no_to_string = "";
			Long final_value1;
			String Result;

			if (unit_type == 1)

			{
				Result = ""
						+ Util.round((((Double) Double.parseDouble(extras
								.getString("text").trim()))) / 2.204);
			} else {
				Result = extras.getString("text").trim();
			}

			if (vala == 1)// data from manual entry
			{
				Result = extras.getString("text").trim();
			}

			if (Result.contains(".")) {
				Double final_value;

				final_value = Double.parseDouble(Result);

				no_to_string = ns.getNumberToString(final_value);
			} else {

				final_value1 = Long.parseLong(Result);
				no_to_string = ns.getNumberToStringLong(final_value1);

			}

			ArrayList<String> final_list = new ArrayList<String>();
			final_list.add("Messages/WeightIs.wav");
			List<String> sellItems = Arrays.asList(no_to_string.split(" "));

			for (String item : sellItems) {
				if (item.toString().length() > 0) {
					final_list.add("Numbers/" + item.toString() + ".wav");
				}
			}

			if (final_list.get(final_list.size() - 1).toString()
					.equals("Numbers/zero.wav")) // remove if last item is zero
			{
				final_list.remove(final_list.size() - 1);
			}
			if (unit_type == 0) {
				final_list.add("Messages/pounds.wav");
			} else {
				final_list.add("Messages/kilograms.wav");
			}

			Util.playSound4FileList(final_list, getApplicationContext());
		} catch (Exception e) {
			Log.e(TAG, "Play sound Exception in show wt page");
		}
	}

	private void redirect() {
		Bundle extras = getIntent().getExtras();
		dbsensor1.updateStatus(7);// update the vital as taken
		dbCreateWeight.UpdateMeasureData_as_valid(extras.getInt("measureId"));
		Log.i(TAG, "next  clicked wt page- ");
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);
		SharedPreferences.Editor editor_retake = settings1.edit();
		editor_retake.putInt("retake_status", 0);
		editor_retake.commit();
		Util.Stopsoundplay();
		Intent intent = new Intent(ShowWightActivity.this,
				UploadMeasurement.class);
		intent.putExtra("reType", 1);

		startActivity(intent);
		finish();
		overridePendingTransition(0, 0);
	}
}
