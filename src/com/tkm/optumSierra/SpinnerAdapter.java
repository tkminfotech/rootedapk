package com.tkm.optumSierra;

import java.util.ArrayList;
import java.util.List;

import com.tkm.optumSierra.bean.ClassSensor;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SpinnerAdapter extends ArrayAdapter<ClassSensor>{

	private Activity activity;
	private List<ClassSensor> data;
	public Resources res;	   
	LayoutInflater inflater;
	String testFlag = "";

	public SpinnerAdapter(Activity context, int textViewResourceId, List<ClassSensor> objects,String testFlag) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub

		activity = context;
		data     = objects;	
		this.testFlag = testFlag; 

		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}





	@Override
	public View getDropDownView(int position, View convertView,ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	// This funtion called for each row ( Called data.size() times )
	public View getCustomView(int position, View convertView, ViewGroup parent) {

		/********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
		View row = inflater.inflate(R.layout.spinner_row, parent, false);

		ClassSensor sensor = data.get(position);

		TextView label        = (TextView)row.findViewById(R.id.txt_vitalName);

//		if(position==0){
//
//			// Default selected Spinner item 
//			label.setText("-Select a device-");
//
//		}
//		else
//		{
			// Set values for spinner each row 
			label.setText(sensor.getSensorName());

		//}   

		return row;
	}
}