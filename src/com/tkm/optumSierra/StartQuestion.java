package com.tkm.optumSierra;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.tkm.optumSierra.dal.Questions_db;
import com.tkm.optumSierra.service.AdviceService;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.ConnectionDetector;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Util;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class StartQuestion extends Titlewindow implements OnInitListener {
	private TextToSpeech tts;

	private String serverurl = "";
	private String serialnumber = "";
	public static String imeilNo = "";
	Questions_db dbcreate1 = new Questions_db(this);
	private String TAG = "StartQuestion Sierra";
	TextView textview, username;
	String ApkType1;
	private int click = 0;
private GlobalClass appClass;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "ON  starting .........*****************************");
		setContentView(R.layout.activity_start_question);
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		appClass  = (GlobalClass)getApplicationContext();
		appClass.isEllipsisEnable = false;
		tts = new TextToSpeech(this, this);

		try {
			Typeface type = Typeface.createFromAsset(getAssets(),
					"fonts/FrutigerLTStd-Roman.otf");

			int a = Constants.getquestionPresent();

			username = (TextView) findViewById(R.id.txtusername1);
			textview = (TextView) findViewById(R.id.txtwelcomestart);
			textview.setTypeface(type, Typeface.NORMAL);

			if (a == 1) {
				textview.setText(this.getString(R.string.welcome_question));
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.USER_SP, 0);
				String patientname = settings.getString("patient_name", "-1"); // #1

				if (patientname.trim().length() >= 2) {

					username.setText(this.getString(R.string.hello)
							+ patientname + ".");
				}
				Typeface type1 = Typeface.createFromAsset(getAssets(),
						"fonts/FrutigerLTStd-Roman.otf");
				username.setTypeface(type1, Typeface.BOLD);
				textview.setTypeface(type1, Typeface.NORMAL);

			} else {

				Typeface type1 = Typeface.createFromAsset(getAssets(),
						"fonts/FrutigerLTStd-Roman.otf");

				username.setVisibility(View.INVISIBLE);
				Util.soundPlay("Messages/AskYouAFewQ.wav");
				textview.setText(this.getString(R.string.askquestion));

				textview.setTextColor(getResources().getColor(
						R.color.optumorange));
				textview.setTypeface(type1, Typeface.NORMAL);
			}
			

			Boolean isInternetPresent = (new ConnectionDetector(
					getApplicationContext())).isConnectingToInternet();
			if (isInternetPresent) {
				DownloadQuestionFromServer();
			} else {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						Log.e(TAG, "NO INTERNET");
						Intent intent = new Intent(StartQuestion.this,
								QuestionView.class);
						finish();
						startActivity(intent);
						overridePendingTransition(0, 0);
					}
				}, 4000);
			}
			/*
			 * if (!isMyAdviceServiceRunning()) { Intent schedule = new
			 * Intent(); schedule.setClass(getApplicationContext(),
			 * AdviceService.class); startService(schedule); // Starting Advice
			 * Service }
			 */
		} catch (Exception e) {
			// Log.e("Droid Error",e.getMessage());
			Log.i(TAG, " Exception start question  on create ");
		}
		Constants.set_question_id("0");
	}

	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked:");
			final Context context = this;

			Intent intent = new Intent(context, StartQuestion.class);
			startActivity(intent);
		} else if (keyCode == KeyEvent.KEYCODE_HOME) {
			Log.i(TAG, "DROID home  clicked:");
			finish();
			// System.exit(0);
		}

		return super.onKeyDown(keyCode, event);
	}

	/*************************************** DownloadQuestionFromServer start ********************************************/

	public void DownloadQuestionFromServer() {

		serverurl = Constants.getPostUrl();// main.PostUrl.toString();
		serialnumber = Constants.getSerialNo();// m
		imeilNo = Constants.getIMEI_No();
		Log.i(TAG, "DownloadQuestionFromServer started ");
		DownloadWebPageTask task = new DownloadWebPageTask();
		task.execute(new String[] { serverurl
				+ "/droid_website/patient_questions.ashx" });
	}

	private class DownloadWebPageTask extends AsyncTask<String, Void, String> {

		protected String getTagValue(NodeList nodes, int i, String tag) {
			return getCharacterDataFromElement((org.w3c.dom.Element) (((org.w3c.dom.Element) nodes
					.item(i)).getElementsByTagName(tag)).item(0));

		}

		/*
		 * final ProgressDialog dialog = ProgressDialog.show(StartQuestion.this,
		 * "", "Loading Questions. Please wait...", true);
		 */

		@Override
		protected void onPreExecute() {

			Log.i(TAG, "onPreExecute");
		}

		@Override
		protected void onPostExecute(String result) {
			Log.i(TAG, "onPostExecute");
			try {
				Thread.sleep(2600);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (click == 0) {
				if (result.trim().length() > 500) {
					Intent intent = new Intent(StartQuestion.this,
							QuestionView.class);
					finish();
					startActivity(intent);
					overridePendingTransition(0, 0);
				} else {
					Boolean isInternetPresent = (new ConnectionDetector(
							getApplicationContext())).isConnectingToInternet();
					if (isInternetPresent) {
						Log.e(TAG, "INTERNET");
						Toast.makeText(StartQuestion.this, R.string.noquestion,
								Toast.LENGTH_LONG).show();
						Intent intent = new Intent(StartQuestion.this,
								Questions.class);
						finish();
						startActivity(intent);
						overridePendingTransition(0, 0);
					} else {
						Log.e(TAG, "NO INTERNET");
						Intent intent = new Intent(StartQuestion.this,
								QuestionView.class);
						finish();
						startActivity(intent);
						overridePendingTransition(0, 0);
					}
				}
			}
		}

		protected HttpResponse connect(String url, String[] header,
				String[] value) {
			try {
				InputStream content = null;
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(url);
				List<NameValuePair> pairs = new ArrayList<NameValuePair>();
				post.setEntity(new UrlEncodedFormEntity(pairs));
				for (int i = 0; i < header.length; i++) {
					post.setHeader(header[i], value[i]);
				}
				return client.execute(post);
			} catch (Exception e) {
				Log.e(TAG, e.toString());
			}
			return null;
		}

		public Boolean preference() {
			Boolean flag = null;
			try {

				String PREFS_NAME = "DroidPrefSc";
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				int scheduletatus = settings.getInt("Scheduletatus", -1);
				Log.i("Droid", " SharedPreferences : "
						+ getApplicationContext() + " is : " + scheduletatus);
				if (scheduletatus == 1)
					flag = true;
				else
					flag = false;
			}

			catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			return flag;
		}

		protected String readStream(InputStream in) {
			try {

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(in));
				StringBuilder str = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					str.append(line + "\n");
				}

				in.close();

				return str.toString();
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			return "";
		}

		protected String doInBackground(String... urls) {
			String response = "";
			SharedPreferences settings = getSharedPreferences("DroidPrefSc", 0);

			String scheduletatus = settings.getString("Scheduleid", "AA");
			int patientId = Constants.getdroidPatientid();
			// Log.i(TAG, " Scheduleid"+scheduletatus+"");
			try {
				HttpResponse response1 = null;
			
					if (preference()) {

						Log.i(TAG, "download question with Scheduleid"
								+ scheduletatus + "");

						Log.i(TAG,
								"Updating serverurl"
										+ serverurl
										+ "/droid_website/patient_schedule_question.ashx");
						response1 = connect(
								serverurl
										+ "/droid_website/patient_schedule_question.ashx",
								new String[] { "patient_id", "schedule_id" },
								new String[] { patientId + "", scheduletatus });
					} else {
						Log.i(TAG, "Updating serverurl" + serverurl
								+ "/droid_website/patient_questions.ashx");
						response1 = connect(
								serverurl
										+ "/droid_website/patient_questions.ashx",
								new String[] { "serial_no", "mode", "imei_no" },
								new String[] { serialnumber, "0", imeilNo });
					}
				
				int Statuscode = response1.getStatusLine().getStatusCode(); //
				// checking for 200 success code

				String str = readStream(response1.getEntity().getContent());
				// System.out.println(str);
				response = str;
				if (Statuscode == 200) {
					Log.i(TAG, "Question status 200 from the server");	
					dbcreate1.Cleartable();
				}

				if (str.trim().length() == 0) {
					Log.i(TAG, "No Question from the server");
					return "";
				}				
				Log.i(TAG, "Question from the server downloaded");
				str = str.replaceAll("&", "and");
				// str.replaceAll(" & ", " and ");
				str = str.replaceAll("\r\n", "");
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");

				// str=str.replaceAll("\\s","");

				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);

				String patient_id = "";
				String question_id = "";
				String pattern_name = "";
				String top_node_number = "";
				String node_number = "";
				String sequence_number = "";
				String item_number = "";
				String child_node_number = "";
				String guidance = "";
				String sequence_number1 = "";
				String explanation = "";
				String kind = "";
				String sequence_number2 = "";
				String item_number1 = "";
				String item_content = "";

				NodeList nodes = doc.getElementsByTagName("tree");
				for (int i = 0; i < nodes.getLength(); i++) {
					patient_id = getTagValue(nodes, i, "patient_id");
					question_id = getTagValue(nodes, i, "question_id");
					pattern_name = getTagValue(nodes, i, "pattern_name");
					top_node_number = getTagValue(nodes, i, "top_node_number");
					dbcreate1.InsertDmp(Integer.parseInt(patient_id),
							Integer.parseInt(question_id), pattern_name,
							Integer.parseInt(top_node_number));
				}

				NodeList branch_tree = doc.getElementsByTagName("branch_tree");

				for (int i = 0; i < branch_tree.getLength(); i++) {
					// b = getTagValue(nodes, i, "question_id");
					// Node item=branch_tree.item(i) ;
					NodeList branch = doc.getElementsByTagName("branch");

					for (int j = 0; j < branch.getLength(); j++) {

						node_number = getTagValue(branch, j, "node_number");
						sequence_number = getTagValue(branch, j,
								"sequence_number");
						item_number = getTagValue(branch, j, "item_number");
						child_node_number = getTagValue(branch, j,
								"child_node_number");

						if (item_number.equals("")) {
							item_number = "0";
						}

						dbcreate1.InsertDmpBranching(
								Integer.parseInt(patient_id), node_number,
								child_node_number,
								Integer.parseInt(sequence_number),
								Integer.parseInt(item_number), "",
								Integer.parseInt(question_id));
						// dbcreate1.InsertDmpBranching(Patient_Code,
						// Node_Number, Child_Node_Number, Sequence_Number,
						// Item_Number, Last_Update_Date, Question_Id)
					}
					Constants.setPatientid(Integer.parseInt(patient_id));

				}

				NodeList interview = doc.getElementsByTagName("interview");
				for (int i = 0; i < interview.getLength(); i++) {
					// b = getTagValue(nodes, i, "question_id");
					// Node item=branch_tree.item(i) ;
					NodeList interview_content = doc
							.getElementsByTagName("interview_content");

					for (int j = 0; j < interview_content.getLength(); j++) {

						sequence_number1 = getTagValue(interview_content, j,
								"sequence_number");
						guidance = getTagValue(interview_content, j, "guidance");
						explanation = getTagValue(interview_content, j,
								"explanation");
						kind = getTagValue(interview_content, j, "kind");

						dbcreate1.InsertDmpQuestion(
								Integer.parseInt(sequence_number1),
								explanation, guidance, Integer.parseInt(kind),
								"");

					}

				}

				NodeList interview_response = doc
						.getElementsByTagName("interview_response");
				for (int i = 0; i < interview_response.getLength(); i++) {
					// b = getTagValue(nodes, i, "question_id");
					// Node item=branch_tree.item(i) ;
					NodeList answer = doc.getElementsByTagName("response");

					for (int j = 0; j < answer.getLength(); j++) {

						sequence_number2 = getTagValue(answer, j,
								"sequence_number");
						item_number1 = getTagValue(answer, j, "item_number");
						item_content = getTagValue(answer, j, "item_content");
						dbcreate1.InsertDmpAnswers(
								Integer.parseInt(sequence_number2),
								Integer.parseInt(item_number1), item_content);

					}

				}

			} catch (Exception e) {
				String Error = this.getClass().getName()+ " : "+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Question Data Upload/Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Questions_failure_upload);
			}

			return response;
		}

	}

	/*************************************** DownloadQuestionFromServer end ********************************************/

	public String getCharacterDataFromElement(org.w3c.dom.Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.US);
			Log.i(TAG, "Initilization Success");
			tts.setSpeechRate(0); // set speech speed rate
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e("TTS", "Language is not supported");
			} else {
				// speakOut();
			}
		} else {
			Log.e(TAG, "Initilization Failed");
		}
		// sayText();
		// Util.soundPlay("Sierra/Please Press Start.wav");
	}

	@Override
	public void onStop() {

		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");

		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		click = 1;
		super.onStop();
	}
}
