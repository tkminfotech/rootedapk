package com.tkm.optumSierra;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tkm.optumSierra.bean.DailyReadingValues;
import com.tkm.optumSierra.bean.DateValues;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by tkmif-ws011 on 11/8/2016.
 */
public class TableChartRecyclerAdapter3 extends Adapter<TableChartRecyclerAdapter3.ViewHolder1> {
    private ArrayList<DateValues> dateList = new ArrayList<DateValues>();
    private HashMap<String, ArrayList<DailyReadingValues>> readingList;
    private String clickFlag = "";
    private Context context;

    public static class ViewHolder1 extends ViewHolder {
        // each data item is just a string in this case
        public TextView txt_date;
        public TextView txt_time;
        public TextView txt_value;
        public TextView txt_meal;
        public LinearLayout layout_tableChartRow;


        public ViewHolder1(View v) {
            super(v);
            txt_date = (TextView) v.findViewById(R.id.txt_tableChartDate);
            txt_time = (TextView) v.findViewById(R.id.txt_tableChartTime);
            txt_value = (TextView) v.findViewById(R.id.txt_tableChartValue);

            txt_meal = (TextView) v.findViewById(R.id.txt_tableChartMeal);
            layout_tableChartRow = (LinearLayout) v.findViewById(R.id.layout_tableChartRow);
        }
    }

    public TableChartRecyclerAdapter3(HashMap<String, ArrayList<DailyReadingValues>> readingList, ArrayList<DateValues> dateValueList, String clickFlag,Context ctx) {

        context = ctx;
        dateList.clear();
        this.clickFlag = clickFlag;

        this.readingList = readingList;
        for (int i = dateValueList.size() - 1; i >= 0; i--) {
            dateList.add(dateValueList.get(i));

        }
    }

    @Override
    public TableChartRecyclerAdapter3.ViewHolder1 onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tablechart_row, parent, false);

        ViewHolder1 vh = new ViewHolder1(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder1 holder, int position) {

        if (clickFlag.equals("glucose")) {
            holder.txt_value.setPadding(0, 0, 0, 0);
            holder.txt_meal.setVisibility(View.VISIBLE);


        } else if (clickFlag.equals("pressure")) {
            holder.txt_value.setPadding(0, 0, 0, 0);
            holder.txt_meal.setVisibility(View.GONE);

        } else {
            holder.txt_value.setPadding(60, 0, 0, 0);
            holder.txt_meal.setVisibility(View.GONE);

        }
        DateValues dateValues = dateList.get(position);

        String key = dateValues.getDateValue();
        ArrayList<DailyReadingValues> dailyReadingValueList = readingList.get(key);
        String time = "";
        String value = "";
        String meal = "";
        for (int i = 0; i < dailyReadingValueList.size(); i++) {

            DailyReadingValues dailyReadingValues = dailyReadingValueList.get(i);
            if (i == dailyReadingValueList.size() - 1) {

                time += parseTime(dailyReadingValues.getTime());
                if (clickFlag.equals("pressure"))

                    value += (int)dailyReadingValues.getSystolicValue()
                            + " / " + (int)dailyReadingValues.getDiastolicValue()
                            + "   " + (int)dailyReadingValues.getPulseValue();

                else if (clickFlag.equals("oxygen"))

                    value += Util.round(dailyReadingValues.getOxygenValue());
                    //+ "/" + Util.round(dailyReadingValues.getPulseValue());

                else if (clickFlag.equals("glucose")) {

                    meal += dailyReadingValues.getMeal();
                    value += Util.round(dailyReadingValues.getValue());

                } else

                    value += Util.round(dailyReadingValues.getValue());

            } else {



                if (clickFlag.equals("pressure")) {

                    value += (int)dailyReadingValues.getSystolicValue()
                            + " / " + (int)dailyReadingValues.getDiastolicValue()
                            + "   " + (int)dailyReadingValues.getPulseValue() + "\n";
                    time += parseTime(dailyReadingValues.getTime()) + "\n";
                }
                else if (clickFlag.equals("oxygen")) {

                    value += Util.round(dailyReadingValues.getOxygenValue()) + "\n";
                    //+ "/" + Util.round(dailyReadingValues.getPulseValue()) + "\n";
                    time += parseTime(dailyReadingValues.getTime()) + "\n";
                }
                else if (clickFlag.equals("glucose")) {

                    int ln=0;
                    SharedPreferences settings_n = context.getSharedPreferences(CommonUtilities.USER_SP, 0);
                    ln = Integer.parseInt(settings_n.getString("language_id", "0"));
                    if (ln == 11) {


                        if(dailyReadingValues.getMeal().equals("Other") || dailyReadingValues.getMeal().equals("Otro")){


                            meal += dailyReadingValues.getMeal() + "\n";
                            value += Util.round(dailyReadingValues.getValue()) + "\n";
                            time += parseTime(dailyReadingValues.getTime()) + "\n";
                        }else if(dailyReadingValues.getMeal().contains("Antes")){

                            meal += dailyReadingValues.getMeal() + "\n";
                            value += Util.round(dailyReadingValues.getValue()) + "\n\n";
                            time += parseTime(dailyReadingValues.getTime()) + "\n\n";
                        }else{
                           // if (context.getResources().getBoolean(R.bool.isTab)) {

                                meal += dailyReadingValues.getMeal() + "\n";
                                value += Util.round(dailyReadingValues.getValue()) + "\n\n";
                                time += parseTime(dailyReadingValues.getTime()) + "\n\n";
//                            }else{
//
//                                meal += dailyReadingValues.getMeal() + "\n";
//                                value += Util.round(dailyReadingValues.getValue()) + "\n\n\n";
//                                time += parseTime(dailyReadingValues.getTime()) + "\n\n\n";
//                            }

                        }

                    }else{

                        meal += dailyReadingValues.getMeal() + "\n";
                        value += Util.round(dailyReadingValues.getValue()) + "\n";
                        time += parseTime(dailyReadingValues.getTime()) + "\n";
                    }

                } else {

                    value += Util.round(dailyReadingValues.getValue()) + "\n";
                    time += parseTime(dailyReadingValues.getTime()) + "\n";
                }
            }
        }
        if (position % 2 != 0) {
            holder.layout_tableChartRow.setBackgroundColor(Color.parseColor("#fceed3"));
        } else {
            holder.layout_tableChartRow.setBackgroundColor(Color.parseColor("#80FFFFFF"));
        }
        holder.txt_date.setText(parseDate(key));
        holder.txt_time.setText(time);
        holder.txt_value.setText(value);

        holder.txt_meal.setText(meal);
    }

    @Override
    public int getItemCount() {
       return dateList.size();
    }


    public String parseDate(String time) {
        String inputPattern = "MM/dd/yyyy";
        String outputPattern = "EEE, MMM dd yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
            if(str.contains(".,")){

                str = str.replace(".,",",");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String parseTime(String time) {
        String inputPattern = "H:mm:ss";
        String outputPattern = "hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date).toUpperCase();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
