package com.tkm.optumSierra;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import com.tkm.optumSierra.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.tkm.optumSierra.bean.ClassTemperature;
import com.tkm.optumSierra.dal.Temperature_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.CustomKeyboard;
import com.tkm.optumSierra.util.Util;

public class TemperatureEntry extends Titlewindow {

	private View pview_back;
	int tempId;
	private String TAG = "TemperatureEntry Sierra";
	CustomKeyboard mCustomKeyboard;
	TextView tempMessage, temptext, tempUnit;
	Dialog dialog;
	private GlobalClass appClass;
	private int unitVal = 0;
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			Temperature_db dbcreatetemperature = new Temperature_db(
					TemperatureEntry.this);

			EditText edttemp = (EditText) findViewById(R.id.tempvalue);
			if ((edttemp.getText().toString()).length() != 0
					&& !(edttemp.getText().toString()).equals("0")
					&& !(edttemp.getText().toString()).equals("00")
					&& !(edttemp.getText().toString()).equals("000")
					&& !(edttemp.getText().toString()).equals(".0")
					&& (edttemp.getText().toString()).length() >= 1
					&& !(edttemp.getText().toString()).equals(".00")
					&& !(edttemp.getText().toString()).equals(".000")
					&& !(edttemp.getText().toString()).equals(".")) {
				if

				((edttemp.getText().toString()).length() == 5) {

					Character point = edttemp.getText().toString().charAt(3);

					// Log.i("Droid","Char : "+point);
					if (point != '.') {

						invalid_popup();
						return;
					}
				}

				SharedPreferences flow = getSharedPreferences(
						CommonUtilities.USER_FLOW_SP, 0);
				int val = flow.getInt("flow", 0); // #1
				if (val == 1) {

					SharedPreferences section = getSharedPreferences(
							CommonUtilities.PREFS_NAME_date, 0);
					SharedPreferences.Editor editor_retake = section.edit();
					editor_retake.putString("sectiondate",  Util.get_patient_time_zone_time(TemperatureEntry.this));
					editor_retake.commit();
				}
				// tts.speak("your temperature is "+edttemp.getText().toString()
				// +"Fahrenheit", TextToSpeech.QUEUE_FLUSH, null);
				int Patientid = Constants.getdroidPatientid();
				ClassTemperature temp = new ClassTemperature();
				double tempValue = 0.0;
if(unitVal == 0){
	
	tempValue = Util.round(Double.parseDouble(edttemp.getText().toString()));
	
}else{
	
	double res = Double.parseDouble(edttemp.getText().toString()) * 1.8;
    double res1 = res + 32;
    tempValue = Util.round(res1);
}
				

				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"MM/dd/yyyy hh:mm:ss aa");

				String currentDateandTime = dateFormat.format(new Date());
				temp.setLastUpdateDate(currentDateandTime);
				temp.setStatus(0);
				temp.setTemperatureValue(tempValue + "");

				String today;
				String tomorrow;
				Date date;
				Format formatter;
				Calendar calendar = Calendar.getInstance();

				date = calendar.getTime();
				formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
				today = formatter.format(date);

				calendar.add(Calendar.DATE, 1);
				date = calendar.getTime();
				formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
				tomorrow = formatter.format(date);
				temp.setInputmode(1);
				SharedPreferences settings1 = getSharedPreferences(
						CommonUtilities.PREFS_NAME_date, 0);		
				String sectiondate=settings1.getString("sectiondate", "0");

				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.USER_TIMESLOT_SP, 0);
				String slot = settings.getString("timeslot", "AM");

				temp.setSectionDate(sectiondate);
				temp.setTimeslot(slot);
				tempId = dbcreatetemperature.InsertTemperatureMeasurement(temp);

				Log.i(TAG, "User Entered tepm- " + tempValue);

				Intent intent = new Intent(TemperatureEntry.this,
						ShowTemperatureActivity.class);
				intent.putExtra("temp", ""+tempValue);
				intent.putExtra("tempid", tempId);

				edttemp.setText("");

				startActivity(intent);
				finish();
				overridePendingTransition(0, 0);
				Log.e(TAG, "Redirecting to show temp page");

				/*
				 * Intent intent = new
				 * Intent(getApplicationContext(),GlucoseEntry.class); finish();
				 * startActivity(intent);
				 */
			} else {
				invalid_popup();
				return;
			}

			// insert function

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		setContentView(R.layout.activity_temperature_entry);
		pview_back = inflater
				.inflate(R.layout.activity_temperature_entry, null);
		appClass  = (GlobalClass)getApplicationContext();
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = true;
		mCustomKeyboard = new CustomKeyboard(this, R.id.keyboardview,
				R.xml.hexkbd, mHandler);
		mCustomKeyboard.registerEditText(R.id.tempvalue);
		tempMessage = (TextView) findViewById(R.id.temperaturemessage);
		temptext = (TextView) findViewById(R.id.temptext);
		tempUnit = (TextView) findViewById(R.id.tempunit);
		Typeface type1 = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		tempMessage.setTypeface(type1, Typeface.NORMAL);
		temptext.setTypeface(type1, Typeface.NORMAL);
		Typeface type2 = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Light.otf");
		tempUnit.setTypeface(type2, Typeface.NORMAL);
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){

			Util.soundPlay("Messages/CheckTempEnter_1.wav");
		}else{

			Util.soundPlay("Messages/CheckTempEnter.wav");
		}		 


		if(settingsUserSp.getInt("temperature_unit_id", -1) == 1){
			tempUnit.setText(getResources().getText(R.string.tempunitC));
			unitVal = 1;

		} else {
			tempUnit.setText(getResources().getText(R.string.tempunitF));
			unitVal = 0;
		}
		
		  SharedPreferences.Editor editor = settingsUserSp.edit();
	        editor.putInt("manualEntry", 1);
	        editor.commit();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	private void popup() {

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		dialog = adb.setView(new View(this)).create();
		// (That new View is just there to have something inside the dialog that
		// can grow big enough to cover the whole screen.)

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = 790;
		lp.height = 500;
		dialog = new Dialog(this, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.popup_messages);

		TextView msg = (TextView) dialog.findViewById(R.id.textpopup);
		TextView mac = (TextView) dialog.findViewById(R.id.txtmac);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);
		msg.setText(this.getString(R.string.entervalidtemperature));

		mac.setTypeface(type, Typeface.NORMAL);
		mac.setVisibility(View.INVISIBLE);

		dialog.show();
		dialog.getWindow().setAttributes(lp);
		closeversion = (Button) dialog.findViewById(R.id.exitversion);
		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				dialog.dismiss();
			}
		});

	}

	private void invalid_popup() {

		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);

		View pview;
		pview = inflater.inflate(R.layout.popup_messages, null);

		TextView msg = (TextView) pview.findViewById(R.id.textpopup);
		TextView mac = (TextView) pview.findViewById(R.id.txtmac);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);
		msg.setText(this.getString(R.string.entervalidtemperature));

		mac.setTypeface(type, Typeface.NORMAL);
		mac.setVisibility(View.INVISIBLE);

		closeversion = (Button) pview.findViewById(R.id.exitversion);

		final PopupWindow cp = new PopupWindow(pview,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		cp.setBackgroundDrawable(new BitmapDrawable(getApplicationContext()
				.getResources()));
		cp.showAtLocation(pview_back, Gravity.CENTER, 0, 0);

		// cp.update(0, 0, 350, 350);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = (displaymetrics.heightPixels);
		int width = displaymetrics.widthPixels/2;
		height=(int) (height/1.2);


		// cp.update(0, 0, 350, 350);
		//cp.update(0, 0, 750, 500);
		cp.update(0, 0,  width,height);

		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				cp.dismiss();
			}
		});

	}
	@Override
	public void onStop() {

		/*Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
		Util.Stopsoundplay();*/
		Log.i(TAG, "--on stop temp entry---- ");
		super.onStop();
	}

}
