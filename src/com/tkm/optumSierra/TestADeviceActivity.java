package com.tkm.optumSierra;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;


public class TestADeviceActivity extends Titlewindow {

	private GlobalClass globalClass;
	
	
	private String TAG = "TestADeviceActivity";
	
	Button btnClose;
	
private Button btn_beginTest;
private Spinner spnVitals;

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_test_device);
		Log.e(TAG, "Glucose Taidoc");
		globalClass = (GlobalClass)getApplicationContext();		
		globalClass.isSupportEnable = true;
		btnClose = (Button) findViewById(R.id.testDeviceClose);
		btn_beginTest = (Button) findViewById(R.id.btn_beginTest);
		spnVitals = (Spinner)findViewById(R.id.spn_assignedVitals);
		btn_beginTest.setEnabled(false);
		btn_beginTest.setBackgroundColor(Color.parseColor("#a0a0a0"));
		
		globalClass.populateSpinner(spnVitals,-1,TestADeviceActivity.this,"testAll");
		btnClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click				
				Intent intent = new Intent(getApplicationContext(), FinalMainActivity.class);
				finish();
				startActivity(intent);
			}
		});
		btn_beginTest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click				
				
			}
		});
		
		
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			

			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}
}
