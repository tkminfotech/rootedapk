package com.tkm.optumSierra;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import com.tkm.optumSierra.bean.ClasssPressure;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.GattByteBufferAandDsmart;
import com.tkm.optumSierra.util.GattUtilsAndSmart;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.OmronPinReceiver;
import com.tkm.optumSierra.util.PairAandDSmart;
import com.tkm.optumSierra.util.Util;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class TestDeviceAandDSmart extends Titlewindow implements PairAandDSmart.GattConnectable {

	private static final int API_LEVEL = Build.VERSION.SDK_INT;
	private static final boolean FORCE_PAIRING_POPUP_TO_FOREGROUND = API_LEVEL <= 19; // will force pairing popup to foreground instead of notification center

	private Timer timer = null;	
	private boolean timer_statu=false;
	private boolean resume_statu=false;
	private String TAG = "-------TestDeviceAandDSmart---------";
	BluetoothManager manager;
	BluetoothGatt mBluetoothGatt;
	BluetoothAdapter mBluetoothAdapter;
	// BluetoothAdapter.LeScanCallback mLeScanCallback;
	// LeScanCallback mLeScanCallback;
	ArrayAdapter<String> mLeDeviceListAdapter;

	// ListAdapter mListAdapter;
	BluetoothDevice deviceL = null;
	// private int mConnectionState = STATE_DISCONNECTED;
	private static final int STATE_DISCONNECTED = 0;
	private BroadcastReceiver pnr_receiver = null;
	private static final int STATE_CONNECTED = 2;

	public final static String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
	public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
	public final static String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
	public final static String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";
	String print="",data_save;
	Button b1;
	public static final long leastSigBits = 0x800000805f9b34fbL;
	// service
	public static final UUID BLOOD_PRESSURE = new UUID(
			(0x1810L << 32) | 0x1000, leastSigBits);

	public static final UUID CHAR_BLOOD_PRESSURE_MEASUREMENT = new UUID(
			(0x2781L << 32) | 0x1000, leastSigBits);
	// char read

	public static final UUID CLIENT_CHARACTERISTIC_CONFIGURATION = new UUID(
			(0x2902L << 32) | 0x1000, leastSigBits);
	public final static UUID UUID_HEART_RATE_MEASUREMENT = UUID
			.fromString("46A970E0-0D5F-11E2-8B5E-0002A5D5C51B");

	public static final UUID BLOOD_PRESSURE_MEASUREMENT = new UUID(
			(0x2A35L << 32) | 0x1000, leastSigBits);
	public static final UUID BLOOD_PRESSURE_FEATURE = new UUID(
			(0x2A49L << 32) | 0x1000, leastSigBits);
	public static final UUID INTERMEDIATE_CUFF_PRESSURE = new UUID(
			(0x2A36L << 32) | 0x1000, leastSigBits);

	// those variables are used to fix the behavior on P724 Tablet.
	BluetoothDevice mDevice = null;
	boolean mIsComingFromPair = false;

	boolean statues;
	int flag = 0;
	TextView text, results;
	Button btnClose;
	TextView txtwelcom, txtReading;
	ImageView rocketImage;
	AnimationDrawable rocketAnimation;
	ObjectAnimator AnimPlz, ObjectAnimator;
	String macAddress = "";
	int a1, b11, c1;
	//int lastinsert_id = 0;
	Pressure_db dbcreatepressure = new Pressure_db(this);
	public String sys, dia, pulse;
	private GlobalClass appClass;
	private Button btn_beginTest;
	private Spinner spnVitals;
	private LinearLayout layout_readingSection;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_device);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		manager = (BluetoothManager) getBaseContext().getSystemService(
				Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = manager.getAdapter();	

		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);
		rocketImage = (ImageView) findViewById(R.id.loading);

		//		rocketImage.setVisibility(View.INVISIBLE);
		//		txtReading.setVisibility(View.INVISIBLE);

		btnClose = (Button) findViewById(R.id.testDeviceClose);
		btn_beginTest = (Button) findViewById(R.id.btn_beginTest);
		spnVitals = (Spinner)findViewById(R.id.spn_assignedVitals);
		layout_readingSection = (LinearLayout)findViewById(R.id.layout_readingSection);
		layout_readingSection.setVisibility(View.INVISIBLE);
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			appClass.setBundle(extras);
		}else{
			extras = appClass.getBundle();
		}
		int pos = extras.getInt("Position", -1);
		macAddress = extras.getString("macaddress");
		if(extras.getString("TestFlag").equals("pair")){

			pairADevice(pos,"pair");	
		}else if(extras.getString("TestFlag").equals("test"))	{

			pairADevice(pos,"test");
		}else if(extras.getString("TestFlag").equals("testAll"))	{

			pairADevice(pos,"testAll");
		}

		btnClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click				
				Intent intent = new Intent(getApplicationContext(), FinalMainActivity.class);
				finish();
				startActivity(intent);
			}
		});
		btn_beginTest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click	
				btn_beginTest.setEnabled(false);
				btn_beginTest.setBackgroundColor(Color.parseColor("#a0a0a0"));
				startTesting();
			}
		});



	}



	private void setwelcome() {

		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){

			Util.soundPlay("Messages/TakeBP_1.wav");
		}else{

			Util.soundPlay("Messages/TakeBP.wav");
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void Animations() {
		txtwelcom.setText(this.getString(R.string.enterpressurebluetooth));
	}

	protected void onResume() {
		super.onResume();
		Log.e("onResume", TAG+"-------------------onResume-----------------");
		if (pnr_receiver == null) {

			Constants.setPIN("111111");

			if (Build.MODEL.equals("SM-T377V")) {
				Log.e(TAG, TAG+"-------------------SM-T377V------------------");
				pnr_receiver = new OmronPinReceiver();
			} else	{
				pnr_receiver = new PairAandDSmart(this);
			}
		}

		// fixes Q7-50 (P724 Integration)
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {

				// in case of P724 tablet, LE scan shouldn't be started in onResume:
				// 1. search is already called in onCreate
				// 2. during pairing, onPause/onResume are called (pairing popup) this causes an LE scan to start DURING the connection which causes problems with the LE scanning
				if (!Build.MODEL.equals("MN-724"))
					search();
			}
		}, 900);

		IntentFilter filter = new IntentFilter();
		filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
		filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
		registerReceiver(pnr_receiver, filter);





		// this behavior only applies to P724 tablet.
		// other parts of the code closes the GATT without disconnecting first. This causes the LE scanning not to be able to detect anything!
		// the code below continues the connection with the sensor after pairing so that we'd properly disconnect from the sensor before closing the GATT
		// then the LE scan works fine and everybody is happy!
		if (Build.MODEL.equals("MN-724") && mIsComingFromPair && mDevice != null) {
			if (mBluetoothGatt != null) {
				mBluetoothGatt.close();
				mBluetoothGatt = null;
			}
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
			mBluetoothGatt = mDevice.connectGatt(getBaseContext(), true, mGattCallback);
			mIsComingFromPair = false;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		//unregisterReceiver(pnr_receiver);
		mBluetoothAdapter.stopLeScan(mLeScanCallback);
		try {
			unregisterReceiver(pnr_receiver);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (mBluetoothGatt == null) {
			// finish();
			return;
		}

		// refreshes the GATT cache (lifted from OmronBleService)
		// this is not needed for other devices, though!
		// in P724 tablet, the pairing popup appears so onPause is called which closes the GATT without disconnecting first.
		// refreshing the cache AND continuing the connection in onResume seem to make everything work ok.
		if (Build.MODEL.equals("MN-724")) {
			refreshCache(mBluetoothGatt);
			mIsComingFromPair = true;
		}

		mBluetoothGatt.close();
		mBluetoothGatt = null;
	}

	public void search() {
		Log.e("Foundddd", "inside search");
		// mLeScanCallback = null;

		boolean x = mBluetoothAdapter.startLeScan(mLeScanCallback);

		if (x) {
			Log.e(TAG+"scan status", "true");
		} else {
			Log.e(TAG+"scan status", "false");
		}

	}

	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
			//Log.d(TAG, TAG+"onLeScan: " + device.getAddress()+"+++++"+macAddress.toUpperCase()+"++++++++++"+device.toString().toUpperCase());

			if (device.toString().toUpperCase().replace(":", "").equals((macAddress.toUpperCase()))) {
				Log.d(TAG, TAG+"onLeScan: connecting to device.");

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// workaround to force showing the pairing popup to foreground instead of notification center
						if(FORCE_PAIRING_POPUP_TO_FOREGROUND) {
							mBluetoothAdapter.startDiscovery();
							mBluetoothAdapter.cancelDiscovery();
						}

						connect(device);

						mDevice = device; // this is ONLY used for P724 Tablet
					}

				});
			}
		}

	};

	public void connect (BluetoothDevice device) {

		// mBluetoothAdapter.stopLeScan(mLeScanCallback);

		// BluetoothDevice check = getDevice();

		if (mBluetoothGatt != null) {
			mBluetoothGatt.close();
			mBluetoothGatt = null;
		}
		mBluetoothAdapter.stopLeScan(mLeScanCallback);
		mBluetoothGatt = device.connectGatt(getBaseContext(), false,
				mGattCallback);

		if (!statues) {
			// deviceL.createBond();
		}
		Log.i("mBluetoothGatt", "connectttt");

		// List<BluetoothGattCharacteristic> list =
		// service.getCharacteristics();

	}

	public BluetoothDevice getDevice() {

		// Paired devices
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
				.getBondedDevices();

		if (pairedDevices.size() > 0) {
			for (BluetoothDevice device : pairedDevices) {
				// Add the name and address to an array adapter to show in a
				// ListView
				if (device.getName().contains("UA-651BLE")) {

					return device;
				}
			}
		}

		return null;
	}

	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {

			if (mBluetoothGatt == null)
				mBluetoothGatt = gatt;

			// String intentAction;
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				// intentAction = ACTION_GATT_CONNECTED;
				// mConnectionState = STATE_CONNECTED;
				// broadcastUpdate(intentAction);
				Log.i("zzzzzzz", "Connected to GATT server.");
				Log.i("zzzzzzz", "Attempting to start service discovery:"
						+ mBluetoothGatt.discoverServices());

				// int bondState = 0;
				// while (bondState == BluetoothDevice.BOND_NONE || bondState ==
				// BluetoothDevice.BOND_BONDING);

				Log.i("bond state", "finished");

			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				// intentAction = ACTION_GATT_DISCONNECTED;
				// mConnectionState = STATE_DISCONNECTED;
				Log.i("mBluetoothGatt", "Disconnected from GATT server.");
				// broadcastUpdate(intentAction);
				// mBluetoothAdapter.stopLeScan(mLeScanCallback);

				// mLeScanCallback = null;
				mBluetoothGatt.close();
				mBluetoothGatt = null;

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						/*
						 * Intent starterIntent = getIntent(); finish();
						 * startActivity(starterIntent);
						 */
						//
						Log.i("a and smart ", "reconnecting");						
						manager = (BluetoothManager) getBaseContext().getSystemService(
								Context.BLUETOOTH_SERVICE);
						mBluetoothAdapter = manager.getAdapter();
						final Handler handler = new Handler();
						handler.postDelayed(new Runnable() {
							@Override
							public void run() {
								search();
							}
						}, 900);


					}
				});

			}



		}

		public void onServicesDiscovered(BluetoothGatt gatt, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS) {

				if (mBluetoothGatt == null)
					mBluetoothGatt = gatt;

				// call getService after a 1 second delay
				// on Tab E, getService(BLOOD_PRESSURE) always returned NULL! delaying the call works! YAY!
//				new Timer().schedule(new TimerTask() {
//					@Override
//					public void run() {
						try {
							BluetoothGattService service = mBluetoothGatt.getService(BLOOD_PRESSURE);
							update(service);

						} catch (Exception e) {
							Log.e(TAG, TAG+"onServicesDiscovered - update call failed!");
							e.printStackTrace();
						}
//					}
//				}, 100);

			} else {
				Log.w("zzzzzzzz", "onServicesDiscovered received: " + status);
			}

		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {

			if (mBluetoothGatt == null)
				mBluetoothGatt = gatt;

			broadcastUpdate(characteristic);
			super.onCharacteristicChanged(gatt, characteristic);
		}

		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// if (status == BluetoothGatt.GATT_SUCCESS) {

			if (mBluetoothGatt == null)
				mBluetoothGatt = gatt;

			broadcastUpdate(characteristic);
			Log.w("zzzzzzzz", "characteristic read");
			// }

		}
		// public void onCharacteristicChanged(BluetoothGatt gatt,
		// BluetoothGattCharacteristic) {}
	};

	public void update(BluetoothGattService service) {

		List<BluetoothGattCharacteristic> list = service.getCharacteristics();

		for (int i = 0; i < list.size(); i++) {
			BluetoothGattCharacteristic characteristic = list.get(i);
			Log.e(TAG, TAG+characteristic.toString());

			mBluetoothGatt.readCharacteristic(characteristic);

			// the characteristics are repeated twice in the list!
			// sometimes the getDescriptor call returns null causing a NullPointerException, so a null check is added
			if (characteristic.getUuid().equals(INTERMEDIATE_CUFF_PRESSURE)) {
				Log.e(TAG, TAG+"characteristic: INTERMEDIATE_CUFF_PRESSURE");

				mBluetoothGatt.setCharacteristicNotification(characteristic, true);
				BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIGURATION);

				if (descriptor != null) {
					descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
					mBluetoothGatt.writeDescriptor(descriptor);
				}
			}

			// the characteristics are repeated twice in the list!
			// sometimes the getDescriptor call returns null causing a NullPointerException, so a null check is added
			if (characteristic.getUuid().equals(BLOOD_PRESSURE_MEASUREMENT)) {
				Log.e(TAG, TAG+"characteristic: BLOOD_PRESSURE_MEASUREMENT");

				mBluetoothGatt.setCharacteristicNotification(characteristic, true);
				BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIGURATION);

				if (descriptor != null) {
					descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
					mBluetoothGatt.writeDescriptor(descriptor);
				}
			}

			if (list.get(i).getUuid().equals(BLOOD_PRESSURE_FEATURE)) {
				Log.e(TAG, TAG+"characteristic: BLOOD_PRESSURE_FEATURE");
				mBluetoothGatt.readCharacteristic(characteristic);
			}

		}

	}

	private void broadcastUpdate(
			final BluetoothGattCharacteristic characteristic) {
		// final Intent intent = new Intent(action);

		// This is special handling for the Heart Rate Measurement profile. Data
		// parsing is carried out as per profile specifications.
		// if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
		// Toast.makeText(getBaseContext(), "reading",Toast.LENGTH_LONG).show();
		// int flag = characteristic.getProperties();
		// int format = -1;
		// if ((flag & 0x01) != 0) {
		// / format = BluetoothGattCharacteristic.FORMAT_UINT16;
		// Log.d("format", "Heart rate format UINT16.");
		// } else {
		// format = BluetoothGattCharacteristic.FORMAT_UINT8;
		// Log.d("format", "Heart rate format UINT8.");
		// }
		// final int heartRate = characteristic.getIntValue(format, 1);
		// Log.d("rrrrr", String.format("Received heart rate: %d", heartRate));
		// intent.putExtra(EXTRA_DATA, String.valueOf(heartRate));
		// } else {
		// For all other profiles, writes the data formatted in HEX.

		byte[] data = characteristic.getValue();
		if (data != null && data.length > 0) {
			StringBuilder stringBuilder = new StringBuilder(" ");
			;
			// = new StringBuilder(data.length);
			Log.d("data", data.toString() + "  " + data.length);
			GattByteBufferAandDsmart bb = GattByteBufferAandDsmart.wrap(data);

			byte flags = bb.getInt8();

			float a, b, c;

			a = GattUtilsAndSmart.getFloatValue(data,
					GattUtilsAndSmart.FORMAT_SFLOAT, 1);
			a1 = (int) a;
			Log.e("data", data.toString() + "  " + a + "int value " + a1);

			b = GattUtilsAndSmart.getFloatValue(data,
					GattUtilsAndSmart.FORMAT_SFLOAT, 3);
			b11 = (int) b;
			Log.e("data", data.toString() + "  " + b + "int value " + b11);
			boolean x = Time_Stamp_Flag(flags);

			Log.e("results", x + "  " + a + "  " + b);
			if (Time_Stamp_Flag(flags)) {
				c = GattUtilsAndSmart.getFloatValue(data,
						GattUtilsAndSmart.FORMAT_SFLOAT, 14);
				c1 = (int) c;
			} else {
				c = GattUtilsAndSmart.getFloatValue(data,
						GattUtilsAndSmart.FORMAT_SFLOAT, 7);
				c1 = (int) c;

			}
			Log.e("data", data.toString() + "  " + c + "int value " + c1);
			// }
			if ((a1 > 0) && (b11 > 0) && (c1 > 0)) {

				if (a1 < 999 && b11 < 999 && c1 < 999) {	
					print = a1 + "," + b11 + "," + c1;
				}
			}

			// print.

			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					Log.d("your blood pressure ", "values-"+print);					
					if ((a1 > 0) && (b11 > 0) && (c1 > 0)) {

						if (a1 < 999 && b11 < 999 && c1 < 999) {							

							data_save=print;
							calle_timer();

						}
					}
				}

			});

		}

	}
	private void calle_timer()
	{

		TimerTask task = new TimerTask() {
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {

						Log.d("mg", "save data after the delay to get last data");

						save(data_save);
						timer.cancel();
						timer_statu=false;

					}
				});
			}
		};
		if(!timer_statu)
		{
			timer = new Timer();
			timer.schedule(task, 10000, 1500);
			timer_statu=true;
		}


	}

	private void save(String result) {
		flag = 1;

		/*
		 * Toast.makeText( getBaseContext(), "save data" + deviceL.getName() +
		 * deviceL.getAddress(), Toast.LENGTH_SHORT).show();
		 */
		try {
			SharedPreferences flow = getSharedPreferences(
					CommonUtilities.USER_FLOW_SP, 0);
			int val = flow.getInt("flow", 0); // #1
			if (val == 1) {
				SharedPreferences section = getSharedPreferences(
						CommonUtilities.PREFS_NAME_date, 0);
				SharedPreferences.Editor editor_retake = section.edit();
				editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(this));
				editor_retake.commit();
			}

			String ttmessage = "";
			String message = result;
			SharedPreferences settings1 = getSharedPreferences(
					CommonUtilities.USER_SP, 0);
			int PatientIdDroid = Integer.parseInt(settings1.getString("patient_id",
					"-1"));
			String[] str = null;
			str = print.split(",");

			SharedPreferences settings = getSharedPreferences(
					CommonUtilities.USER_TIMESLOT_SP, 0);
			String slot = settings.getString("timeslot", "AM");

			ClasssPressure pressure = new ClasssPressure();
			pressure.setPatient_Id(PatientIdDroid);

			pressure.setSystolic(Integer.parseInt(str[0]));
			pressure.setDiastolic(Integer.parseInt(str[1]));
			pressure.setPulse(Integer.parseInt(str[2]));
			pressure.setInputmode(0);
			pressure.setTimeslot(slot);

			SharedPreferences settings2 = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);
			String sectiondate = settings2.getString("sectiondate", "0");
			pressure.setSectionDate(sectiondate);
			pressure.setPrompt_flag("4");
			//lastinsert_id = dbcreatepressure.InsertPressure(pressure);

			sys = "" + str[0];
			dia = "" + str[1];
			pulse = "" + str[2];
			showResult(sys, dia, pulse);
			//		Intent intent = new Intent(TestDeviceAandDSmart.this, ShowPressureActivity.class);
			//		intent.putExtra("sys", sys);
			//		intent.putExtra("dia", dia);
			//		intent.putExtra("pulse", pulse);
			//		intent.putExtra("pressureid", lastinsert_id);
			//		// Log.e(TAG, "redirecting from a and bp to show");
			//		/*
			//		 * try { mBluetoothService.stop(); } catch (Exception ec) {
			//		 * 
			//		 * }
			//		 */
			//		
			//		
			//		startActivity(intent);
			//		TestDeviceAandDSmart.this.finish();		
		} catch (Exception e) {
			Log.i("A&D",
					"Exception inserting bp from in a and d continua");

		}

	}

	private boolean Time_Stamp_Flag(byte flags) {
		if ((flags & GattUtilsAndSmart.SECOND_BITMASK) != 0)
			return true;
		return false;
	}

	@Override
	protected void onStop() {
		if (!isMyAdviceServiceRunning()) {

			/*Intent schedule = new Intent();
			schedule.setClass(AandDSmart.this, AdviceService.class);
			startService(schedule); // Starting Advice Service*/

		}
		
		super.onStop();
	}
	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private void pairADevice(int pos,String testFlag){
		//		txtwelcom.setVisibility(View.GONE);
		//		txtReading.setText(getResources().getString(R.string.pairingDevice));
		appClass.populateSpinner(spnVitals,pos,TestDeviceAandDSmart.this,testFlag);
	}

	private void startTesting(){
		layout_readingSection.setVisibility(View.VISIBLE);
		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		setwelcome();
		Animations();
		if (mBluetoothAdapter == null) {
			Toast.makeText(getBaseContext(), "No Bluetooth found",
					Toast.LENGTH_SHORT).show();
			finish();
			return;
		}

		else if (!mBluetoothAdapter.isEnabled()) {
			Toast.makeText(getBaseContext(), "No Bluetooth found",
					Toast.LENGTH_SHORT).show();
			finish();
			return;
		} else {

//			if (pnr_receiver == null) {
//
//				Constants.setPIN("111111");
//
//				if (Build.MODEL.equals("SM-T377V")) {
//					Log.e(TAG, TAG+"-------------------SM-T377V------------------");
//					pnr_receiver = new OmronPinReceiver();
//				} else	{
//					pnr_receiver = new PairAandDSmart(this);
//				}
//			}
			
			search();

//			IntentFilter filter = new IntentFilter();
//			filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
//			filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
//			registerReceiver(pnr_receiver, filter);
//
//		
//			
//			
//
//			// this behavior only applies to P724 tablet.
//			// other parts of the code closes the GATT without disconnecting first. This causes the LE scanning not to be able to detect anything!
//			// the code below continues the connection with the sensor after pairing so that we'd properly disconnect from the sensor before closing the GATT
//			// then the LE scan works fine and everybody is happy!
//			if (Build.MODEL.equals("MN-724") && mIsComingFromPair && mDevice != null) {
//				if (mBluetoothGatt != null) {
//					mBluetoothGatt.close();
//					mBluetoothGatt = null;
//				}
//				mBluetoothAdapter.stopLeScan(mLeScanCallback);
//				mBluetoothGatt = mDevice.connectGatt(getBaseContext(), true, mGattCallback);
//				mIsComingFromPair = false;
//			}
		}

	}

	private void showResult(String sys,String dia,String pulse){

		txtReading.setVisibility(View.INVISIBLE);
		rocketImage.setVisibility(View.INVISIBLE);
		txtwelcom.setText(Html.fromHtml(this
				.getString(R.string.yourbloodpressure)
				+ "<FONT COLOR=\"#D45D00\">"
				+ sys
				+ "</FONT>"
				+ this.getString(R.string.over)
				+ "<FONT COLOR=\"#D45D00\">"
				+ dia
				+ "</FONT>"
				+ this.getString(R.string.withpulserate)
				+ "<FONT COLOR=\"#D45D00\">"
				+ " "
				+ pulse
				+ "</FONT>"
				+ this.getString(R.string.beatsperminute)));

		playSound(sys,dia,pulse);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {


			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}


	ArrayList<String> final_list = new ArrayList<String>();
	private void playSound(String sys,String dia,String pulse) {


		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){

			final_list.add("Messages/YourBPis_1.wav");
		}else{

			final_list.add("Messages/YourBPis.wav");
		}

		add_to_finalList(sys);	

		final_list.add("Messages/over.wav");		
		
		add_to_finalList(dia);
		if(languageId.equals("11")){

			final_list.add("Messages/WPulseRateof_1.wav");
		}else{

			final_list.add("Messages/WPulseRateof.wav");
		}

		add_to_finalList(pulse);
		final_list.add("Messages/beatsPerMin.wav");

		Util.playSound4FileList(final_list, getApplicationContext());
	}

	private void add_to_finalList(String vitalValue) {
		String no_to_string = "";
		try {
			NumberToString ns = new NumberToString();

			if (vitalValue.contains(".")) {
				Double final_value = Double.parseDouble(vitalValue);
				no_to_string = ns.getNumberToString(final_value);
			} else {
				Long final_value = Long.parseLong(vitalValue);
				no_to_string = ns.getNumberToStringLong(final_value);
			}

			List<String> sellItems = Arrays.asList(no_to_string.split(" "));

			for (String item : sellItems) {
				if (item.toString().length() > 0) {
					final_list.add("Numbers/" + item.toString() + ".wav");
				}
			}

			if (final_list.get(final_list.size() - 1).toString()
					.equals("Numbers/zero.wav")) // remove if last item is zero
			{
				final_list.remove(final_list.size() - 1);
			}
		} catch (Exception e) {

		}
	}
	
		private void refreshCache(BluetoothGatt gatt) {
		boolean retValue = false;
		try {
			Method method = gatt.getClass().getDeclaredMethod("refresh", new Class[0]);
			boolean access = method.isAccessible();
			method.setAccessible(true);
			Log.d(TAG, TAG+"gatt.refresh()");
			retValue = (Boolean) method.invoke(gatt, new Object[0]);
			method.setAccessible(access);
		} catch (NoSuchMethodException e) {
			Log.e(TAG,TAG+ e.getMessage());
		} catch (IllegalAccessException e) {
			Log.e(TAG, TAG+e.getMessage());
		} catch (InvocationTargetException e) {
			Log.e(TAG, TAG+e.getMessage());
		}
		Log.d(TAG, TAG+"gatt.refresh() return:" + retValue);
	}
}