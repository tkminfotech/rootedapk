package com.tkm.optumSierra;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.tkm.optumSierra.TestDeviceGlucoseAccuChek.BackgroundThread;
import com.tkm.optumSierra.bean.ClassMeasurement;
import com.tkm.optumSierra.dal.Pulse_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.Util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class TestDeviceBLECheckActivity extends Titlewindow  {

	// BluetoothLeService mBluetoothLeService;
	BluetoothManager manager;
	BluetoothGatt mBluetoothGatt;
	BluetoothAdapter mBluetoothAdapter;
	// LeScanCallback mLeScanCallback;
	ArrayAdapter<String> mLeDeviceListAdapter;
	// ListAdapter mListAdapter;
	BluetoothDevice deviceL;
	TextView txtwelcom, txtReading;
	ImageView rocketImage;
	AnimationDrawable rocketAnimation;
	ObjectAnimator AnimPlz, ObjectAnimator;
	//int pulseId;
	Pulse_db dbcreate1 = new Pulse_db(this);
	private String result = "0, 0";
	int flag=0;
	int a,b;
	private GlobalClass appClass;
	public final static String ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
	public final static String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
	public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
	public final static String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
	public final static String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";
	String print,print1;
	//Button b1;
	TextView text;
	String macAddress = "";
	public final static UUID UUID_HEART_RATE_MEASUREMENT = UUID
			.fromString("46A970E0-0D5F-11E2-8B5E-0002A5D5C51B");
	Button btnClose;
	private Button btn_beginTest;
	private Spinner spnVitals;
	private LinearLayout layout_readingSection;
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// finish();
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_device);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);
		rocketImage = (ImageView) findViewById(R.id.loading);
		btnClose = (Button) findViewById(R.id.testDeviceClose);
		btn_beginTest = (Button) findViewById(R.id.btn_beginTest);
		spnVitals = (Spinner)findViewById(R.id.spn_assignedVitals);
		layout_readingSection = (LinearLayout)findViewById(R.id.layout_readingSection);
		layout_readingSection.setVisibility(View.INVISIBLE);
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			appClass.setBundle(extras);
		}else{
			extras = appClass.getBundle();
		}
		int pos = extras.getInt("Position", -1);
		if (extras.getString("macaddress").length() > 1) {
			macAddress = extras.getString("macaddress");
		}
		if(extras.getString("TestFlag").equals("pair")){

			pairADevice(pos,"pair");	
		}else if(extras.getString("TestFlag").equals("test"))	{

			pairADevice(pos,"test");
		}else if(extras.getString("TestFlag").equals("testAll"))	{

			pairADevice(pos,"testAll");
		}

		btnClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click				
				Intent intent = new Intent(getApplicationContext(), FinalMainActivity.class);
				finish();
				startActivity(intent);
			}
		});
		btn_beginTest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click	
				btn_beginTest.setEnabled(false);
				btn_beginTest.setBackgroundColor(Color.parseColor("#a0a0a0"));
				startTesting();
			}
		});		
		
		manager = (BluetoothManager) getBaseContext().getSystemService(
				Context.BLUETOOTH_SERVICE);		
		mBluetoothAdapter = manager.getAdapter();		

		//Log.i("Optum", "Optum : LE macAddress " + macAddress);

		if (mBluetoothAdapter == null) {
			
			Toast.makeText(getBaseContext(), "no adapter found",
					Toast.LENGTH_SHORT).show();
			finish();
			return;
		}

		if (!mBluetoothAdapter.isEnabled()) {
			Toast.makeText(getBaseContext(), "no adapter found",
					Toast.LENGTH_SHORT).show();
			finish();
			return;
		}
	
	}
	private void setwelcome() {
		// TextView txtwelcom=(TextView)findViewById(R.id.t);
		// String welcome="Please check your pulse oximetry now";
		// txtwelcom.setText(welcome);

		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){
			
			Util.soundPlay("Messages/ClipOnPulseOX_1.wav");
		}else{
			
			Util.soundPlay("Messages/ClipOnPulseOX.wav");
		}
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			// e.printStackTrace();
//		}

		// tts.speak(welcome, TextToSpeech.QUEUE_FLUSH, null);
	}
	private void Animations() {
		txtwelcom.setText(this.getString(R.string.enterpulsebluetooth));
//		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
//		AnimPlz.setDuration(1000);
//		AnimPlz.start();
//
//		AnimPlz.addListener(new AnimatorListenerAdapter() {
//			public void onAnimationEnd(Animator animation) {
//
//				Log.e("", "AnimPlz");
//
//				txtReading.setVisibility(View.VISIBLE);
//				rocketImage.setVisibility(View.VISIBLE);
//
//			}
//		});
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			

			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	@SuppressLint("NewApi")
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mBluetoothAdapter.stopLeScan(mLeScanCallback);

		if (mBluetoothGatt == null) {
			// finish();
			return;
		}
		mBluetoothGatt.close();
		mBluetoothGatt = null;
		// finish();

	}

	@SuppressLint("NewApi")
	public void search() {
		if (mBluetoothGatt != null) {
			mBluetoothGatt.close();
		}
		boolean x = mBluetoothAdapter.startLeScan(mLeScanCallback);
		if (x) {
			//text.setText("Searching for your device.Please turn it on...");
			Log.e("scan status", "true");
		} else {
			Log.e("scan status", "false");
		}

	}

	@SuppressLint("NewApi")
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
		public void onLeScan(final BluetoothDevice device, int rssi,
				byte[] scanRecord) {
			runOnUiThread(new Runnable() {

				public void run() {
					//Log.e("Foundddd", device.toString());

					deviceL = device;
				//	text.setText("your device is found.please press start");
					//b1.setVisibility(View.VISIBLE);

					if (device.toString().toUpperCase().replace(":", "").equals((String) (macAddress.toUpperCase()))){

						/*Intent x = new Intent(getBaseContext(),
								DataReaderBLE.class);
						x.putExtra("device", deviceL);
						startActivity(x);

						Toast.makeText(
								getBaseContext(),
								"Your device is found:\n" + deviceL.getName()
										+ deviceL.getAddress(),
								Toast.LENGTH_SHORT).show();*/
						
						connect();
						
							//b1.setVisibility(View.VISIBLE);

				}
				}
			});
		}
	};
	
	public void save(String result){
		flag=1;
		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = flow.getInt("flow", 0); // #1
		if (val == 1) {			
			SharedPreferences section = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);
			SharedPreferences.Editor editor_retake = section.edit();
			editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(this));
			editor_retake.commit();
		}
		int oxyreading = 0, HeartRate = 0;
		String[] separated = result.split(",");
		oxyreading = Integer.parseInt(separated[0].trim());
		HeartRate = Integer.parseInt(separated[1].trim());
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"MM/dd/yyyy hh:mm:ss aa");
		String currentDateandTime = dateFormat
				.format(new Date());
		ClassMeasurement measurement = new ClassMeasurement();
		measurement.setStatus(0);
		measurement.setOxygen_Value(Integer
				.parseInt(separated[0].trim()));
		measurement.setLast_Update_Date(currentDateandTime);
		measurement.setPressure_Value(Integer
				.parseInt(separated[1].trim()));
		measurement.setInputmode(0);
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.PREFS_NAME_date, 0);		
		String sectiondate=settings1.getString("sectiondate", "0");
		
		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.USER_TIMESLOT_SP, 0);
		String slot = settings.getString("timeslot", "AM");
		
		measurement.setSectionDate(sectiondate);
		measurement.setTimeslot(slot);

		if ((measurement.getOxygen_Value() + "").trim()
				.length() > 1) {
//			pulseId = dbcreate1
//					.InsertOxymeterMeasurement(measurement);
		}
//		Intent intent = new Intent(TestDeviceBLECheckActivity.this,
//				ShowPulseActivity.class);
//		intent.putExtra("pulse", oxyreading + "");
//		intent.putExtra("pulseid", pulseId);
//		startActivity(intent);
		
		showResult(oxyreading);
	}
	public void onStop() {
		//mBluetoothGatt.disconnect();
		
		flag=1;
		super.onStop();
	}

	
	
	/**********************************************************************************************/
	@SuppressLint("NewApi")
	private void connect() {

		// mBluetoothAdapter.stopLeScan(mLeScanCallback);
		// mBluetoothAdapter.cancelDiscovery();
		mBluetoothGatt = deviceL.connectGatt(getBaseContext(), false,
				mGattCallback);
		Log.i("zzzzzzz", "connectttttttttt");

		// List<BluetoothGattCharacteristic> list =
		// service.getCharacteristics();

	}

	@SuppressLint("NewApi")
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {

			// String intentAction;
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				// intentAction = ACTION_GATT_CONNECTED;
				// mConnectionState = STATE_CONNECTED;
				// broadcastUpdate(intentAction);
				Log.i("zzzzzzz", "Connected to GATT server.");
				//Log.i("zzzzzzz", "Attempting to start service discovery:"
				//		+ mBluetoothGatt.discoverServices());
				 mBluetoothGatt.discoverServices();

			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				// intentAction = ACTION_GATT_DISCONNECTED;
				// mConnectionState = STATE_DISCONNECTED;
				Log.i("zzzzzzzzz", "Disconnected from GATT server.");
				// broadcastUpdate(intentAction);
			}

		}

		public void onServicesDiscovered(BluetoothGatt gatt, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS) {
				Log.w("zzzzzzzz", "service dicovered");

				try {
					BluetoothGattService service = mBluetoothGatt
							.getService(UUID_HEART_RATE_MEASUREMENT);
					//Log.e("zzzzzzsssz", "uiiiid" + service.getUuid().toString());

					update(service);

					// Toast.makeText(getBaseContext(),
					// service.getUuid().toString()+
					// "founddd", Toast.LENGTH_LONG).show();
					
				} catch (NullPointerException e) {
					Log.e("zzzzzzz", "no uiiiid");
				}

				// BluetoothGattCharacteristic characteristic;
				// mBluetoothGatt.readCharacteristic(characteristic.)
			} else {
				//Log.w("zzzzzzzz", "onServicesDiscovered received: " + status);
			}

		}

		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			// TODO Auto-generated method stub
			broadcastUpdate(characteristic);
			super.onCharacteristicChanged(gatt, characteristic);
		}

		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				broadcastUpdate(characteristic);
				Log.w("zzzzzzzz", "characteristic sent");
			}

		}
		// public void onCharacteristicChanged(BluetoothGatt gatt,
		// BluetoothGattCharacteristic) {}
	};

	@SuppressLint("NewApi")
	public void update(BluetoothGattService service) {

		List<BluetoothGattCharacteristic> list = service.getCharacteristics();

		for (int i = 0; i < list.size(); i++) {
			//Log.e("charactaristic", list.get(i).toString());
			mBluetoothGatt.readCharacteristic(list.get(i));

			if (list.get(i)
					.getUuid()
					.equals(UUID
							.fromString("0aad7ea0-0d60-11e2-8e3c-0002a5d5c51b"))) {
				mBluetoothGatt.setCharacteristicNotification(list.get(i), true);
				BluetoothGattDescriptor descriptor = list
						.get(i)
						.getDescriptor(
								UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
				descriptor
						.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
				mBluetoothGatt.writeDescriptor(descriptor);
			}

		}
	}

	@SuppressLint("NewApi")
	private void broadcastUpdate(
			final BluetoothGattCharacteristic characteristic) {

		byte[] data = characteristic.getValue();
		if (data != null && data.length > 0) {
			StringBuilder stringBuilder = new StringBuilder("");
			StringBuilder stringBuilder2 = new StringBuilder("");

			Byte PalseRate = (Byte) data[9];
			Byte spo = (Byte) data[7];
	
			stringBuilder.append(spo.intValue() + " ");
			stringBuilder2.append(PalseRate.intValue() + " ");
		

			print = stringBuilder.toString();
			print1 = stringBuilder2.toString();
			 a=spo.intValue();
			 b=PalseRate.intValue();

			//Log.d("sa", print);
			// print.

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					
				/*	Intent intent = new Intent(BLECheckActivity.this,
							ShowPulseActivity.class);
					intent.putExtra("pulse", print + "");
					intent.putExtra("pulseid", 0);
					startActivity(intent);*/
					result = print + ", " + print1;
					if(flag==0){
						
						if(a>0&&b>0){
						
							save(result);
						}
					}
					
					// b1.setVisibility(View.GONE);
					//text.setText(print2+','+print);
					//text2.setText(print);
					
					
				}
			});
			
			
			

		}
		
		
		
	}		
	private void pairADevice(int pos,String testFlag){
		//		txtwelcom.setVisibility(View.GONE);
		//		txtReading.setText(getResources().getString(R.string.pairingDevice));
		appClass.populateSpinner(spnVitals,pos,TestDeviceBLECheckActivity.this,testFlag);
	}
	
	private void startTesting(){
		layout_readingSection.setVisibility(View.VISIBLE);
		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();		
		setwelcome();
		Animations();		

		search();
	}
	
	private void showResult(int value){
		
		SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);
		SharedPreferences.Editor contract_editor = contract_cod.edit();
		contract_editor.putString("nion_paired", "pair");
		contract_editor.commit();
		txtReading.setVisibility(View.INVISIBLE);
		rocketImage.setVisibility(View.INVISIBLE);
		txtwelcom.setText(getResources().getString(R.string.yourbloodoxygenlevel)  	+" "
				+ getResources().getString(R.string.at)
				+ value + getResources().getString(R.string.percentage));
		playSound(""+value);
	}
	
	
	ArrayList<String> final_list = new ArrayList<String>();
	private void playSound(String value) {
		
		final_list.add("Messages/BL-OXlevelsAt.wav");
		add_to_finalList(value);
		final_list.add("Messages/percent.wav");

		Util.playSound4FileList(final_list, getApplicationContext());
	}

	private void add_to_finalList(String vitalValue) {
		String no_to_string = "";
		NumberToString ns = new NumberToString();

		if (vitalValue.contains(".")) {
			Double final_value = Double.parseDouble(vitalValue);
			no_to_string = ns.getNumberToString(final_value);
		} else {
			Long final_value = Long.parseLong(vitalValue);
			no_to_string = ns.getNumberToStringLong(final_value);
		}

		List<String> sellItems = Arrays.asList(no_to_string.split(" "));

		for (String item : sellItems) {
			if (item.toString().length() > 0) {
				final_list.add("Numbers/" + item.toString() + ".wav");
			}
		}

		if (final_list.get(final_list.size() - 1).toString()
				.equals("Numbers/zero.wav")) // remove if last item is zero
		{
			final_list.remove(final_list.size() - 1);
		}
	}
	
}