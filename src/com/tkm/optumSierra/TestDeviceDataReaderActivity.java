package com.tkm.optumSierra;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import com.tkm.optumSierra.DataReaderActivity.BTConnector;
import com.tkm.optumSierra.bean.ClassMeasurement;
import com.tkm.optumSierra.dal.Pulse_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.PINReceiver;
import com.tkm.optumSierra.util.Util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.app.ActivityManager.RunningServiceInfo;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class TestDeviceDataReaderActivity extends Titlewindow {
	private final BroadcastReceiver mybroadcast = new PINReceiver();
	private static final int REQUEST_ENABLE_BT = 1;
	Pulse_db dbcreate1 = new Pulse_db(this);
	Sensor_db dbSensor = new Sensor_db(this);
	private BluetoothAdapter mBluetoothAdapter = null;
	private static final String DEVICE_NAME = "Nonin";
	private String TAG = "DataReaderActivity Sierra";
	// Progress Dialog
	//int pulseId;
	private ProgressDialog pDialog;
	private String result = "0, 0";
	// Progress dialog type (0 - for Horizontal progress bar)
	public static final int PROGRESS_BAR_TYPE = 0;
	public int back = 0;
	TextView textView = null;
	private ProgressDialog progressDialog = null;
	String macAddress = "";
	BluetoothSocket socket = null;
	BluetoothDevice device = null;
	TextView txtwelcom, txtReading;
	ObjectAnimator AnimPlz, ObjectAnimator;
	BTConnector btc = new BTConnector();
	ImageView rocketImage;
	AnimationDrawable rocketAnimation;
	Util utilobjct = new Util();
	public boolean cancel_flag = false;
	private GlobalClass appClass;
	Button btnClose;
	private Button btn_beginTest;
	private Spinner spnVitals;
	private LinearLayout layout_readingSection;
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@SuppressWarnings("static-access")
	private void setwelcome() {
		// TextView txtwelcom=(TextView)findViewById(R.id.t);
		// String welcome="Please check your pulse oximetry now";
		// txtwelcom.setText(welcome);

		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){
			
			Util.soundPlay("Messages/ClipOnPulseOX_1.wav");
		}else{
			
			Util.soundPlay("Messages/ClipOnPulseOX.wav");
		}
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			// e.printStackTrace();
//		}

		// tts.speak(welcome, TextToSpeech.QUEUE_FLUSH, null);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_device);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);
		rocketImage = (ImageView) findViewById(R.id.loading);
		btnClose = (Button) findViewById(R.id.testDeviceClose);
		btn_beginTest = (Button) findViewById(R.id.btn_beginTest);
		spnVitals = (Spinner)findViewById(R.id.spn_assignedVitals);
		layout_readingSection = (LinearLayout)findViewById(R.id.layout_readingSection);
		layout_readingSection.setVisibility(View.INVISIBLE);
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			appClass.setBundle(extras);
		}else{
			extras = appClass.getBundle();
		}
		int pos = extras.getInt("Position", -1);

		if (extras.getString("macaddress").length() > 1)
			macAddress = extras.getString("macaddress");
		if (extras.getString("macaddress").length() > 1) {
			macAddress = extras.getString("macaddress");
		}
		if(extras.getString("TestFlag").equals("pair")){

			pairADevice(pos,"pair");	
		}else if(extras.getString("TestFlag").equals("test"))	{

			pairADevice(pos,"test");
		}else if(extras.getString("TestFlag").equals("testAll"))	{

			pairADevice(pos,"testAll");
		}

		btnClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click				
				Intent intent = new Intent(getApplicationContext(), FinalMainActivity.class);
				finish();
				startActivity(intent);
			}
		});
		btn_beginTest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click	
				btn_beginTest.setEnabled(false);
				btn_beginTest.setBackgroundColor(Color.parseColor("#a0a0a0"));
				startTesting();
			}
		});		
		
		
		String Pin = dbSensor.SelectPulsePIN();
		Constants.setPIN(Pin);// set pin for auto pairing
		textView = new TextView(this);

		// requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		// setMessage("Searching for device configuration...");

		
		
		Log.i(TAG,
				"-------------------oncreate--------------  dataread  activity");
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, " KEYCODE_BACK clicked:");
			back = 1;
			btc.cancel(true);
			Log.e(TAG, "cliecked back button redirect to the starting page ");
			Intent intent = new Intent(this, FinalMainActivity.class);
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
			this.finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	private void createOKProgressDialog(String title, String message) {
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle(title);
		progressDialog.setMessage(message);
		progressDialog.setCancelable(false);
		progressDialog.setButton(DialogInterface.BUTTON_POSITIVE, this.getString(R.string.OK),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// code
						dialog.dismiss();
						// Intent intent = new Intent(DataReaderActivity.this,
						// UploadMeasurement.class);
						// finish();
						// startActivity(intent);
						// PresureActivity();
					}
				});

		progressDialog.show();
	}

	

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			// macAddress.substring(i, i+2)
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		//Log.i(TAG, "Nonin : Connect to macAddress- " + macAddress);
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub

		/*
		 * try { String command; command =
		 * "LD_LIBRARY_PATH=/vendor/lib:/system/lib service call activity 42 s16 com.android.systemui"
		 * ; Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c",
		 * command }); proc.waitFor(); } catch(Exception ex) {
		 * Toast.makeText(getApplicationContext(), ex.getMessage(),
		 * Toast.LENGTH_LONG).show(); }
		 */

		IntentFilter filter = new IntentFilter();
		filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
		registerReceiver(mybroadcast, filter);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		unregisterReceiver(mybroadcast);
		super.onPause();

	}

	@Override
	public void onStop() {

		btc.cancel(true);
		cancel_flag = true;

		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		SharedPreferences settingscluster = getSharedPreferences(
				CommonUtilities.CLUSTER_SP, 0);
		String cluster = settingscluster.getString("clustorNo", "-1"); // #1

		// Sierra

		/*	if (!isMyAdviceServiceRunning()) {
				Intent schedule = new Intent();
				schedule.setClass(DataReaderActivity.this, AdviceService.class);
				startService(schedule); // Starting Advice Service
			}*/

		Log.e(TAG, "-- ON STOP --");

		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");

		super.onStop();
	}

	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	public BluetoothDevice getDevice(String devName) {

		if (mBluetoothAdapter == null) {
			// Device does not support Bluetooth
			setMessage(this.getString(R.string.devicenotsupportbluetooth));
			Log.i(TAG, " Device does NOT support Bluetooth");
			return null;
		}

		// check whether bluetooth is enabled
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}

		if ((mBluetoothAdapter != null)) { // &&
											// (mBluetoothAdapter.isEnabled()))
											// {
			setProgressBarIndeterminateVisibility(true);

			// Paired devices
			Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
					.getBondedDevices();
			if (pairedDevices.size() > 0) {
				for (BluetoothDevice device : pairedDevices) {
					// Add the name and address to an array adapter to show in a
					// ListView
					if (device.getName().contains("Nonin")) { // "ANIL-PC",
																// BlackBerry
																// 9800,
																// UC-321PBT,
																// NONIN
						return device;
					}
				}
			}
		}
		return null;
	}

	public void setMessage(String message) {
		textView.setTextSize(20);
		textView.setText(message);
		setContentView(textView);
	}

	/*-----------------*/

	public class BTConnector extends AsyncTask<BluetoothDevice, String, String> {

		String progress = "";

		protected void onPreExecute() {
			super.onPreExecute();
			// showDialog(PROGRESS_BAR_TYPE);
		}

		/**
		 * Downloading file in background thread
		 * */
		protected String doInBackground(BluetoothDevice... device) {
			try {
				BluetoothDevice device1 = null;
				for (int i = 0; i <= 50000; i++) {
					mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

					if (macAddress.trim().length() == 17) { // getting a
															// configured device
						device1 = mBluetoothAdapter.getRemoteDevice(macAddress);
						//Log.i(TAG, " NoNin macAddress" + macAddress);
					}

					for (Integer port = 1; port <= 3; port++) {

						if (isCancelled()) {
							Log.i(TAG, "cancelled in background");
							return "";
						}

						if (cancel_flag) {
							Log.i(TAG, "cancelled in background flag");
							return "";
						}

						if (connect(device1, Integer.valueOf(port))) {
							//Log.i(TAG, "Optum : Connection successful for "
							//		+ port);
							return result;
						}
						try {
							//Log.i(TAG, "Optum : Waiting..." + port);
							Thread.sleep(1000);
						} catch (Exception e) {
							// e.printStackTrace();
						}
					}
					if(i==40000)
					{
						i=0;
						Log.i(TAG, "Optum :rest loop to 0 ");
					}
				}
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			return "";
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		@SuppressLint("SimpleDateFormat")
		@Override
		protected void onPostExecute(String result) {
			// dismiss the dialog after the file was downloaded
			try {
				int oxyreading = 0, HeartRate = 0;

				if (cancel_flag) {
					Log.i(TAG, "return on post exec)");
					return;
				}

				//Log.i(TAG, " : back " + back);
				if (back != 1) {

					// dismissDialog(PROGRESS_BAR_TYPE);
					// progressDialog.cancel();
					back = 0;
					String message = "";
					String ttsMessage = "";
					//Log.i(TAG, " : Result Bt device " + result);
					if (result.trim().equals("0, 0")) {
						message = getApplicationContext().getString(R.string.invalidreading);
						ttsMessage = message;

					} else {
						SharedPreferences flow = getSharedPreferences(
								CommonUtilities.USER_FLOW_SP, 0);
						int val = flow.getInt("flow", 0); // #1
						if (val == 1) {
							
							SharedPreferences section = getSharedPreferences(
									CommonUtilities.PREFS_NAME_date, 0);
							SharedPreferences.Editor editor_retake = section.edit();
							editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(TestDeviceDataReaderActivity.this));
							editor_retake.commit();
						}
						if (result != null && result.trim().length() > 1) {
							String[] separated = result.split(",");
							oxyreading = Integer.parseInt(separated[0].trim());
							HeartRate = Integer.parseInt(separated[1].trim());

							SimpleDateFormat dateFormat = new SimpleDateFormat(
									"MM/dd/yyyy hh:mm:ss aa");
							String currentDateandTime = dateFormat
									.format(new Date());
							ClassMeasurement measurement = new ClassMeasurement();
							measurement.setStatus(0);
							measurement.setOxygen_Value(Integer
									.parseInt(separated[0].trim()));
							measurement.setLast_Update_Date(currentDateandTime);
							measurement.setPressure_Value(Integer
									.parseInt(separated[1].trim()));
							measurement.setInputmode(0);
							SharedPreferences settings1 = getSharedPreferences(
									CommonUtilities.PREFS_NAME_date, 0);		
							String sectiondate=settings1.getString("sectiondate", "0");
							
							SharedPreferences settings = getSharedPreferences(
									CommonUtilities.USER_TIMESLOT_SP, 0);
							String slot = settings.getString("timeslot", "AM");
							
							measurement.setSectionDate(sectiondate);
							measurement.setTimeslot(slot);

							if ((measurement.getOxygen_Value() + "").trim()
									.length() > 1) {
//								pulseId = dbcreate1
//										.InsertOxymeterMeasurement(measurement);
							}
							// Util.soundPlay("Sierra/Your pulse is.wav");
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}

							message = getApplicationContext().getString(R.string.resultoxygen)
									+ result + ")";
							ttsMessage = oxyreading + "";

						} else {
							Log.e(TAG,
									"invalid packets from nonin.. need to retry... ");
							message = getApplicationContext().getString(R.string.invalidreading);
							ttsMessage = message;
							try {
								Log.i(TAG,
										" BTConnector restarted in Nonin data page ");
								BTConnector btc_new = new BTConnector();
								btc_new.execute(device);
							} catch (Exception ex) {
								Log.i(TAG,
										" BTConnector restarted in Nonin data page got  error: ");
							}
							return;

						}
					}

					if (oxyreading != 0) {
						if ((oxyreading + "").trim().length() > 1) {
//
//							Intent intent = new Intent(TestDeviceDataReaderActivity.this,
//									ShowPulseActivity.class);
//							intent.putExtra("pulse", oxyreading + "");
//							intent.putExtra("pulseid", pulseId);
//							Log.e(TAG,
//									"Data saved db and redirecting to the result page ");
//							startActivity(intent);
//							finish();
							showResult(oxyreading);
						} else {
							try {
								Log.i(TAG,
										" BTConnector restarted in Nonin data page ");
								BTConnector btc_new = new BTConnector();
								btc_new.execute(device);
							} catch (Exception ex) {
								Log.i(TAG,
										" BTConnector restarted in Nonin data page got  error: ");
							}
							return;
						}
					} else {
						Log.i(TAG,
								" : Invalid reading. Please try again! retry started");
						message =getApplicationContext().getString(R.string.invalidreading);
						//Log.i("Optum", message);
						ttsMessage = message;
						/*
						 * BTConnector btc_new=new BTConnector();
						 * btc_new.execute(device); return;
						 */
						try {
							Log.i(TAG,
									" BTConnector restarted in Nonin data page ");
							BTConnector btc_new = new BTConnector();
							btc_new.execute(device);
						} catch (Exception ex) {
							Log.i(TAG,
									" BTConnector restarted in Nonin data page got  error: ");
						}
						return;
					}
				}
			} catch (Exception e) {
				Log.e(TAG, "IOException in NoNin page " + e.getMessage());

			} finally {

			}
		}

		protected boolean connect(BluetoothDevice device, Integer port) {

			InputStream inStream = null;
			OutputStream outStream = null;
			// byte [] inputBytes = null;
			result = "0, 0";
			//Log.i(TAG, " : Connection to " + device.getName() + ":" + port);

			// The documents tell us to cancel the discovery process.
			mBluetoothAdapter.cancelDiscovery();

			try {
				Method createBond = BluetoothDevice.class.getMethod("createBond");
			     boolean x =  (Boolean) createBond.invoke(device);
			     
			     
				Method m = device.getClass().getMethod("createRfcommSocket",
						new Class[] { int.class });
				socket = (BluetoothSocket) m.invoke(device, port);

				// debug check to ensure socket was set.
				if (socket == null) {
					return false;
				}

				// attempt to connect to device
				try {
					socket.connect();
					try {
						inStream = socket.getInputStream();
						outStream = socket.getOutputStream();
					} catch (IOException exx) {
						exx.printStackTrace();
						Log.e(TAG, " : Conenction Failed...\n");
						socket.close();
						return false;
					}
					if (inStream == null || outStream == null) {
						Log.e(TAG, " : Stream is null");
						try {
							inStream.close();
							outStream.close();
							socket.close();
						} catch (Exception cl) {
						}
						return false;
					}
					//Log.i(TAG, " : Connected to device " + device.getName()
					//		+ "\n");

					long timeStart = System.currentTimeMillis();
					long timePassed = System.currentTimeMillis() - timeStart;
					boolean finished = false;

					while (true) {

						byte[] retrieve = { 0x44, 0x31 };
						outStream.write(retrieve);
						//Log.e(TAG, " : Write to device " + retrieve.toString());

						Log.e(TAG, " : Get Result ");
						byte[] ack = new byte[1];
						finished = false;
						while ((timePassed < 3000) && (!finished)) {
							if (inStream.available() > 0) {
								inStream.read(ack);
								finished = true;
							} else {
								Thread.sleep(1000);
								timePassed = System.currentTimeMillis()
										- timeStart;
							}
						}

						if (finished) {
							//Log.e(TAG, " : Received ACK " + ack.toString());

							if (ack[0] == 0x15) {
								//Log.e("TAG",
								//		" : NAK Received " + ack.toString());
								socket.close();
								return false;
							} else {
								//Log.e(TAG, " : ACK Received " + ack.toString());
							}
						} else {
							Log.e(TAG, " : NO ACK Received ");
							try {
								inStream.close();
								outStream.close();
								socket.close();
							} catch (Exception cl) {
							}
							return false;
						}

						byte[] inData = new byte[3];
						timeStart = System.currentTimeMillis();
						timePassed = System.currentTimeMillis() - timeStart;
						finished = false;
						int oxygen = 0;
						int heartRate = 0;

						while ((timePassed < 5000) && (!finished)) {
							Log.e(TAG, " : Try reading data from device ");
							// publishProgress(""+(((port+(timePassed/3000))*100)/500));
							if (inStream.available() > 0) {
								int len = inStream.read(inData);

								//Log.i(TAG, " : Data Received (" + len + ") : "
								//		+ inData.toString());
								heartRate = (int) inData[1];
								oxygen = (int) inData[2];
								if ((heartRate >= 127) || oxygen >= 127) {
									Log.i("Sample", "Sample : INVALID DATA!!!");
									oxygen = 0;
									heartRate = 0;
								}
								result = oxygen + ", " + heartRate;
								// result ="Blood Oygen percentage equals "+
								// oxygen+", Pulse equals"
								// +", "+heartRate+" beats per minute";

								//Log.i(TAG, " : Data Received (" + len + ") : "
								//		+ (int) inData[0] + " HR "
								//		+ (int) inData[1] + ", SpO2 "
								//		+ (int) inData[2]);
								//Log.i(TAG, " : Result (Oxygen, Heart Rate) = ("
								//		+ result + ")");
								finished = true;
							} else {
								Log.i(TAG, " : NO Data Received!! ");
								result = "0, 0";
							}
							Thread.sleep(1000);
							timePassed = System.currentTimeMillis() - timeStart;
						}
						try {
							inStream.close();
							outStream.close();
							socket.close();
						} catch (Exception cl) {
						}
						return finished;
					}
				} catch (Exception e) {
					Log.e(this.toString(), "IOException " + e.getMessage());
				}
			} catch (Exception ex) {
				Log.e(this.toString(), "Exception " + ex.getMessage());
			}
			try {
				inStream.close();
				outStream.close();
				socket.close();
			} catch (Exception cl) {
			}

			return false;
		}

	}

	private void Animations() {
		txtwelcom.setText(this.getString(R.string.enterpulsebluetooth));
//		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
//		AnimPlz.setDuration(1000);
//		AnimPlz.start();
//
//		AnimPlz.addListener(new AnimatorListenerAdapter() {
//			public void onAnimationEnd(Animator animation) {
//
//				Log.e(TAG, "AnimPlz");
//
//				txtReading.setVisibility(View.VISIBLE);
//				rocketImage.setVisibility(View.VISIBLE);
//
//			}
//		});
	}

	@Override
	protected void onRestart() {
		super.onRestart();

		cancel_flag = false;

		/*
		 * Intent intent = new Intent(getApplicationContext(),
		 * com.tkm.optumSierra.FinalMainActivity.class); this.finish();
		 * startActivity(intent);
		 */

		/*Log.e(TAG, "onRestart");

		Log.i(TAG, " BTConnector restarted in Nonin data page ");
		BTConnector btc_new = new BTConnector();
		btc_new.execute(device);*/
	}
	private void pairADevice(int pos,String testFlag){
		//		txtwelcom.setVisibility(View.GONE);
		//		txtReading.setText(getResources().getString(R.string.pairingDevice));
		appClass.populateSpinner(spnVitals,pos,TestDeviceDataReaderActivity.this,testFlag);
	}
	
	private void startTesting(){
		layout_readingSection.setVisibility(View.VISIBLE);
		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();		
		setwelcome();
		Animations();
		
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (macAddress.trim().length() == 17) { // getting a configured device
			device = mBluetoothAdapter.getRemoteDevice(macAddress);
			//Log.e(TAG, "connecting to nonin with mac " + macAddress);
		} else { // getting any paired device
			device = getDevice(DEVICE_NAME);
		}

		if (device == null) {
			// setMessage("Paired 'Ninin' devices NOT found...\nPlease make sure you have at least one Nonin Paired Device!");
			createOKProgressDialog(this.getString(R.string.Bluetooth),
					this.getString(R.string.unabletoconnectpulsedevice));

			// finish();
		} else {
			//Log.i(TAG, "optum : Connect to device " + device.toString());
			/*
			 * try { Thread.sleep(5000); } catch (InterruptedException e) { //
			 * TODO Auto-generated catch block e.printStackTrace(); }
			 */
			btc.execute(device);
		}
	}
	
	private void showResult(int value){
		SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);
		SharedPreferences.Editor contract_editor = contract_cod.edit();
		contract_editor.putString("nion_paired", "pair");
		contract_editor.commit();
		txtReading.setVisibility(View.INVISIBLE);
		rocketImage.setVisibility(View.INVISIBLE);
		txtwelcom.setText(getResources().getString(R.string.yourbloodoxygenlevel) 
				+ getResources().getString(R.string.at)
				+ value + getResources().getString(R.string.percentage));
	}
}