package com.tkm.optumSierra;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.taidoc.pclinklibrary.android.bluetooth.util.BluetoothUtil;
import com.taidoc.pclinklibrary.connection.AndroidBluetoothConnection;
import com.taidoc.pclinklibrary.connection.util.ConnectionManager;
import com.taidoc.pclinklibrary.constant.PCLinkLibraryConstant;
import com.taidoc.pclinklibrary.constant.PCLinkLibraryEnum;
import com.taidoc.pclinklibrary.exceptions.NotSupportMeterException;
import com.taidoc.pclinklibrary.meter.AbstractMeter;
import com.taidoc.pclinklibrary.meter.record.AbstractRecord;
import com.taidoc.pclinklibrary.meter.record.TemperatureRecord;
import com.taidoc.pclinklibrary.meter.util.MeterManager;
import com.tkm.optumSierra.bean.ClassTemperature;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.dal.Temperature_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.PINReceiver;
import com.tkm.optumSierra.util.Util;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.app.ActivityManager.RunningServiceInfo;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.speech.tts.TextToSpeech;

import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class TestDeviceForaMainActivity extends Titlewindow{
	private static final String TAG = "TestDeviceFora";

	private static final int REQUEST_ENABLE_BT = 1;

	private static final int SENSOR_TYPE_BLE = 1;
	private static final int SENSOR_TYPE_CLASSIC = 0;
	private static final int SENSOR_TYPE_UNKNOWN = -1;

	private static final String HEXES = "0123456789ABCDEF";
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

	private GlobalClass appClass;
	private TextToSpeech tts;
	ImageView rocketImage;
	AnimationDrawable rocketAnimation;
	ObjectAnimator AnimPlz;
	TextView txtwelcom, txtReading;
	Button btnClose;
	private Button btn_beginTest;
	private Spinner spnVitals;
	private LinearLayout layout_readingSection;
	private ProgressDialog pDialog;
	private ProgressDialog progressDialog = null;

	private final BroadcastReceiver pinReceiver = new PINReceiver();
	private BluetoothAdapter mBluetoothAdapter = null;
	private BluetoothSocket btSocket = null;
	private OutputStream outStream = null;
	private InputStream inStream = null;

	int Dec = 0;
	private int deviceType = -1;
	private String result = "0, 0";

	String macAddress = "";
	private BTConnector btFora = new BTConnector();
	BluetoothDevice device = null;

	Sensor_db dbSensor = new Sensor_db(this);
	Temperature_db dbtemperature = new Temperature_db(this);

	private AndroidBluetoothConnection mConnection; // BLE connection (PCLink library)
	private AbstractMeter mForaMeter = null;

	// bluetooth recovery variables
	private boolean mBluetoothSocketTimedOut = false; // used to indicate that the BluetoothSocket connection has timed out
	private int failedBleConnectionCount = 0; // used to indicate how many BLE connections have failed

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_device);
		appClass = (GlobalClass) getApplicationContext();
		//tts = new TextToSpeech(this, this);
		appClass.isSupportEnable = true;
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");

		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);

		btnClose = (Button) findViewById(R.id.testDeviceClose);
		btn_beginTest = (Button) findViewById(R.id.btn_beginTest);
		spnVitals = (Spinner)findViewById(R.id.spn_assignedVitals);
		layout_readingSection = (LinearLayout)findViewById(R.id.layout_readingSection);
		layout_readingSection.setVisibility(View.INVISIBLE);
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			appClass.setBundle(extras);
		}else{
			extras = appClass.getBundle();
		}
		int pos = extras.getInt("Position", -1);
		deviceType = extras.getInt("deviceType");
		macAddress = extras.getString("macaddress");
		if(extras.getString("TestFlag").equals("pair")) {
			pairADevice(pos,"pair");

		}else if(extras.getString("TestFlag").equals("test")) {
			pairADevice(pos,"test");

		}else if(extras.getString("TestFlag").equals("test")) {
			pairADevice(pos,"test");

		}else if(extras.getString("TestFlag").equals("testAll")) {
			pairADevice(pos,"testAll");
		}

		btnClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				if (btFora != null)
					btFora.cancel(true);

				Intent intent = new Intent(getApplicationContext(), FinalMainActivity.class);
				finish();
				startActivity(intent);
			}
		});

		btn_beginTest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click	
				btn_beginTest.setEnabled(false);
				btn_beginTest.setBackgroundColor(Color.parseColor("#a0a0a0"));
				startTesting();
			}
		});

		Log.i(TAG, "-------------------onCreate--------------  fora  activity");
	}

	@Override
	public void onStop() {
		super.onStop();
		SharedPreferences settingscluster = getSharedPreferences(CommonUtilities.CLUSTER_SP, 0);
		String cluster = settingscluster.getString("clustorNo", "-1"); // #1

		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}

		mBluetoothAdapter.cancelDiscovery();

		if (btFora != null)
			btFora.cancel(true);

		try {
			if (btSocket != null) {
				btSocket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		leDisconnect();

		Log.e(TAG, "-- ON STOP Fora activity --");
		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
	}

	// step 1
	private void startTesting(){
		layout_readingSection.setVisibility(View.VISIBLE);
		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		setwelcome();

		if (macAddress.trim().length() == 17) {
			device = mBluetoothAdapter.getRemoteDevice(macAddress);
		}

		if (device == null) {
			createOKProgressDialog(this.getString(R.string.Bluetooth), this.getString(R.string.unabletoconnectbluetoothAandD));
			finish();
		} else {
			if (!enableBluetooth()) {
				finish();
			}

			int sensorBluetoothType = getSensorBluetoothType(macAddress);

			// start the BT classic cycle if the sensor BT type is either BLE or UNKNOWN
			if (sensorBluetoothType != SENSOR_TYPE_BLE)
				btFora.execute(device);

			// start the BT classic cycle if the sensor BT type is either BLE or UNKNOWN
			if (Build.VERSION.SDK_INT >= 18 && sensorBluetoothType != SENSOR_TYPE_CLASSIC)
				leConnect();
		}
	}

	public boolean enableBluetooth() {

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		mBluetoothAdapter.enable();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return true;
	}

	// step 2 (classic)
	private class BTConnector extends AsyncTask<BluetoothDevice, String, String> {

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected String doInBackground(BluetoothDevice... device) {
			try {
				for (Integer port = 1; port <= 1500; port++) {

					if (isCancelled()) {
						Log.i(TAG, "cancelled in background");
						return "";
					}

					if (connect(device[0])) {
						return result;
					}
					try {
						Thread.sleep(2000);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			}
			return "";
		}

		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		@Override
		protected void onPostExecute(String result) {
			// dismiss the dialog after the file was downloaded

			if (pDialog != null)
				pDialog.dismiss();
			if (progressDialog != null)
				progressDialog.dismiss();

			if (result.trim().length() > 0) {
				showTempResult(result);
			}
		}

		protected boolean connect(BluetoothDevice device) {
			byte[] data = new byte[34];
			result = "0, 0";

			mBluetoothAdapter.cancelDiscovery();

			SharedPreferences flow = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
			int val = flow.getInt("flow", 0); // #1
			if (val == 1) {
				SharedPreferences section = getSharedPreferences(CommonUtilities.PREFS_NAME_date, 0);
				SharedPreferences.Editor editor_retake = section.edit();
				editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(TestDeviceForaMainActivity.this));
				editor_retake.apply();
			}
			try {
				btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);

			} catch (IOException e) {
				Log.e(TAG, "ON RESUME: Socket creation failed.", e);
				e.printStackTrace();
			}

			if (btSocket == null) {
				return false;
			}

			try {

				try {
					// (from doc) This call will block until a connection is made or the connection fails
					btSocket.connect();
					Log.e(TAG, "BTConnector#connect: BT connection established, data transfer link open.");
				} catch (IOException e) {
					try {
						Log.e(TAG, "BTConnector#connect: btSocket.connect error");
						btSocket.close();
						return false;
					} catch (IOException e2) {
						Log.e(TAG, "BTConnector#connect: btSocket.close error ", e2);
					}
				}

				// if we're able to successfully connect to the socket, it means that the sensor is classic bluetooth.
				// so we'll stop BLE stuff
				leDisconnect();
				// save the sensor type as BT classic
				saveSensorBluetoothType(macAddress, SENSOR_TYPE_CLASSIC);

				// set the Fora as paired
				Util.setPairStatus(TestDeviceForaMainActivity.this, "fora_paired", "pair");

				// attempt to connect to device
				try {

					outStream = btSocket.getOutputStream();
					inStream = btSocket.getInputStream();

					int checkSum = (0x51 + 0x26 + 0x00 + 0x00 + 0x00 + 0x00 + 0xA3) & 0xFF;
					int cmd[] = { 0x51, 0x26, 0x00, 0x00, 0x00, 0x00, 0xA3, checkSum };

					try {
						for (int aCmd : cmd) {
							outStream.write((aCmd & 0xFF));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

					if (inStream.available() > 0) {
						inStream.read(data);
					}
					try {
						for (int aCmd : cmd) {
							outStream.write((aCmd & 0xFF));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						Thread.sleep(1500);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

					byte H;
					byte L;
					try {
						System.out.print("Data received ");

						if (inStream.available() > 0) {
							inStream.read(data);
							H = data[24];
							L = data[25];
							System.out.println(getHex(data, data.length));

							H = data[3];
							L = data[2];
							String hex1 = Integer.toHexString(H & 0XFF);
							String hex2 = Integer.toHexString(L & 0XFF);
							String NewHex = hex1 + hex2;

							Dec = Integer.parseInt(NewHex, 16);
							float temp = (float) Dec / 10;

							double finalValue = (temp * (1.8)) + 32;

							double tempValue = 0.0;
							tempValue = Util.round(finalValue);

							result = tempValue + "";

							byte messageTurnOff[] = { 81, 80, 0, 0, 0, 0, -93, 68 };

							try {
								outStream.write(messageTurnOff);
							} catch (IOException e) {
								Log.e(TAG, "BTConnector#connect: Exception during turn off write cmd in temp device .", e);
								e.printStackTrace();
							}

							try {
								inStream.close();
								outStream.close();
								btSocket.close();
							} catch (IOException e2) {
								Log.e(TAG, "IOException while closing the socket or stream after the reading was taken", e2);
								e2.printStackTrace();
							}

							return true;
						}

					} catch (IOException e) {
						Log.e(TAG, "BTConnector#connect: Exception during read.", e);
					}

				} catch (Exception e) {
					Log.e(this.toString(), "IOException " + e.getMessage());
				}
			} catch (Exception ex) {
				Log.e(this.toString(), "Exception " + ex.getMessage());
			}

			try {
				inStream.close();
				outStream.close();
				btSocket.close();
			} catch (Exception cl) {
				cl.printStackTrace();
			}

			return false;
		}
	}

	// step 2 (BLE) - called in startTesting
	private void leConnect() {
		Log.d(TAG, "leConnect()");

		setupAndroidBluetoothConnection();
		connectMeter();
	}

	// step 2.1 (BLE) - called in leConnect
	private void setupAndroidBluetoothConnection() {
		Log.d(TAG, "setupAndroidBluetoothConnection() - mConnection: " + mConnection);
		if (mConnection == null) {
			try {
				mConnection = ConnectionManager.createAndroidBluetoothConnection(mBTConnectionHandler);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// step 2.2 (BLE) - called in leConnect
	private void connectMeter() {
		Log.d(TAG, "#IN connectMeter()");
		new Thread(new Runnable() {
			@Override
			public void run() {
				Looper.prepare();
				try {
					Log.d(TAG, "connectMeter -> updatePairedList");
					updatePairedList();

					if (mConnection.getState() == AndroidBluetoothConnection.STATE_NONE) {
						Log.d(TAG, "connectMeter -> setLeConnectedListener");
						mConnection.setLeConnectedListener(mLeConnectedListener);

						Log.d(TAG, "connectMeter -> LeListen!!");
						mConnection.LeListen();
					}

				} catch (Exception e) {
					Log.e(TAG, e.getMessage(), e);

				} finally {
					getTemperatureValue();
				}

				Looper.loop();
			}

		}).start();
	}

	// release BLE resources
	private void leDisconnect() {
		Log.d(TAG, "leDisconnect: stopping BLE scanning/communication.");
		new Thread(new Runnable() {
			@Override
			public void run() {
				Looper.prepare();
				try {
					if (mConnection != null) {
						mConnection.setLeConnectedListener(null);
						mConnection.LeDisconnect();
						mConnection = null;
					}

				} catch (Exception e) {
					Log.e(TAG, e.getMessage(), e);
				}

				Looper.loop();
			}

		}).start();
	}

	// release BT classic resources
	private void classicDisconnect() {
		Log.d(TAG, "classicDisconnect: stopping BT classic scanning/communication.");
		if (btFora != null) {
			btFora.cancel(true);
			btFora = null;
		}

		if (btSocket != null) {
			try {
				btSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			btSocket = null;
		}
	}

	//region BLE connection handler
	private Handler mBTConnectionHandler = new Handler() {
		@TargetApi(Build.VERSION_CODES.KITKAT)
		@Override
		public void handleMessage(Message msg) {
			try {
				switch (msg.what) {
					case PCLinkLibraryConstant.MESSAGE_STATE_CHANGE:

						int mLeConnectionState = msg.arg1;
						Log.d(TAG, "mBTConnectionHandler#handleMessage -- MESSAGE_STATE_CHANGE: " + mLeConnectionState);

						switch (msg.arg1) {
							case AndroidBluetoothConnection.STATE_CONNECTED_BY_LISTEN_MODE:

								try {
									Log.d(TAG, "mBTConnectionHandler#handleMessage -> set mForaMeter. mConnection: " + mConnection);
									mForaMeter = MeterManager.detectConnectedMeter(mConnection);

								} catch (Exception e) {
									e.printStackTrace();
									throw new NotSupportMeterException();
								}
								if (mForaMeter == null) {
									throw new NotSupportMeterException();
								}
								break;

							case AndroidBluetoothConnection.STATE_CONNECTING:
								Log.d(TAG, "mBTConnectionHandler#handleMessage: connecting");
								break;

							case AndroidBluetoothConnection.STATE_SCANED_DEVICE:
								// LE sensor found, release BT classic resources
								classicDisconnect();
								// save the sensor type as BLE
								saveSensorBluetoothType(macAddress, SENSOR_TYPE_BLE);

								Log.d(TAG, "mBTConnectionHandler#handleMessage -- connected device address: " + mConnection.getConnectedDeviceAddress());
								BluetoothDevice device = BluetoothUtil.getPairedDevice(mConnection.getConnectedDeviceAddress());
								mConnection.LeConnect(getApplicationContext(), device);

								break;

							case AndroidBluetoothConnection.STATE_LISTEN:
								break;

							case AndroidBluetoothConnection.STATE_NONE:
								break;

							case AndroidBluetoothConnection.STATE_BLUETOOTH_STACK_NULL_EXCEPTION:
								recoverBluetooth();
								break;
						} /* end of switch */
						break;

				} /* end of switch */

				Log.d(TAG, "mBTConnectionHandler#handleMessage -- Message: " + msg.what);

			} catch (NotSupportMeterException e) {
				Log.e(TAG, "not support meter", e);
			}
		}
	};
	//endregion

	private AndroidBluetoothConnection.LeConnectedListener mLeConnectedListener = new AndroidBluetoothConnection.LeConnectedListener() {

		@Override
		public void onConnectionStateChange_Disconnect(BluetoothGatt gatt, int status, int newState) {
			mConnection.LeDisconnect();
			if (shouldRecoverBluetooth())
				recoverBluetooth();
		}

		@SuppressLint("NewApi")
		@Override
		public void onDescriptorWrite_Complete(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
			Log.d(TAG , "onDescriptorWrite_Complete" );
			mConnection.LeConnected(gatt.getDevice());
		}

		@Override
		public void onCharacteristicChanged_Notify(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

			new Thread(new Runnable() {
				@TargetApi(Build.VERSION_CODES.KITKAT)
				@Override
				public void run() {
					Looper.prepare();

					try {
						mForaMeter = MeterManager.detectConnectedMeter(mConnection);
						Log.e(TAG, "mForaMeter reference: " + mForaMeter);
						Log.e(TAG, "mConnection reference: " + mConnection);

						// set sensor as "paired"
						SharedPreferences pairingSharedPreferences = getSharedPreferences(CommonUtilities.NION_SP, 0);
						SharedPreferences.Editor editor = pairingSharedPreferences.edit();
						editor.putString("fora_paired", "pair");
						editor.apply();

					} catch (Exception e) {
						e.printStackTrace();
						mConnection.LeDisconnect(); // disconnect and try again

						// nullifying the mConnection reference makes the updatePairedList() method create a new connection.
						// this makes the connection more robust in the P724 tablet (even if the sensor disconnects, it always reconnects)
						if (Build.MODEL.equals("MN-724"))
							mConnection = null;
					}

					Looper.loop();
				}
			}).start();
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {}
	};

	// updates the mConnection with the assigned MAC address
	private void updatePairedList() {
		Map<String, String> addrs = new HashMap<String, String>();
		String addrKey = "BLE_PAIRED_METER_ADDR_" + String.valueOf(0);
		addrs.put(addrKey, macAddress);

		Log.d(TAG, "updatePairedList -- set mConnection");

		if (mConnection == null) {
			mConnection = ConnectionManager.createAndroidBluetoothConnection(mBTConnectionHandler);
		}

		Log.d(TAG, "updatedPairedList -- MAC address: " + macAddress);
		mConnection.updatePairedList(addrs, 1);
	}

	public void getTemperatureValue() {
		try {
			if (mForaMeter == null) {

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						// check whether the activity that is currently shown on screen is in fact the session, rather than the test device page
						ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
						ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
						String mClassName = cn.getClassName();
						if (!mClassName.equals(TestDeviceForaMainActivity.this.getClass().getCanonicalName())) {
							Log.d(TAG, "getTemperatureValue#run: trying to run the thread while outside of the activity.");
							return;
						}

						Log.d(TAG, "getTemperatureValue -> connectMeter (handler). mForaMeter is NULL mConnection isNull: " + (mConnection == null));

						setupAndroidBluetoothConnection();
						connectMeter();
					}
				}, 10000);

			} else {
				Log.e(TAG, "ForaMeter reference is NOT NULL");

				AbstractRecord record = mForaMeter.getStorageDataRecord(0, PCLinkLibraryEnum.User.CurrentUser);

				double valueInFahrenheit = ((TemperatureRecord) record).getObjectTemperatureValue();

				double finalValue = Util.round((valueInFahrenheit * (1.8)) + 32);
				result = String.valueOf(finalValue);

				TestDeviceForaMainActivity.this.runOnUiThread(new Runnable() {
					public void run() {
						Log.d(TAG, "getTemperatureValue#runOnUiThread");
						showTempResult(result);
					}
				});


				try {
					mForaMeter.turnOffMeterOrBluetooth(0);
					leDisconnect();
					mForaMeter = null;

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//region trivial life-cycle events
	@Override
	protected void onResume() {
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
		registerReceiver(pinReceiver, filter);

		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		try {
			unregisterReceiver(pinReceiver);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, " KEYCODE_BACK clicked: fora");

			Intent intent = new Intent(this, FinalMainActivity.class);
			this.finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	public void onDestroy() {

		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		super.onDestroy();
	}
	//endregion

	//region helpers
	private String getForaMAC(String mac) {
		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		if (macAddress.trim().length() == 17) {
			return macAddress;
		} else {
			return "";
		}
	}
	public double round(double d) {
		DecimalFormat twoDForm = new DecimalFormat("#.#");
		return Double.valueOf(twoDForm.format(d));
	}
	public String getHex(byte[] raw, int len) {
		Log.i(TAG, "Get Hex...");
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(3 * raw.length);
		for (final byte b : raw) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4))
					.append(HEXES.charAt((b & 0x0F))).append(" ");
			if (--len == 0)
				break;
		}
		return hex.toString();
	}
	static String byteArrayToHexString(byte in[]) {
		byte ch = 0x00;
		int i = 0;
		if (in == null || in.length <= 0)
			return null;

		String pseudo[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };

		StringBuffer out = new StringBuffer(in.length * 2);
		while (i < in.length) {
			ch = (byte) (in[i] & 0xF0); // Strip off high nibble
			ch = (byte) (ch >>> 4);
			// shift the bits down
			ch = (byte) (ch & 0x0F);
			// must do this is high order bit is on!
			out.append(pseudo[(int) ch]); // convert the nibble to a String
			// Character
			ch = (byte) (in[i] & 0x0F); // Strip off low nibble
			out.append(pseudo[(int) ch]); // convert the nibble to a String
			// Character
			i++;
		}

		return new String(out);
	}
	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	public BluetoothDevice getDevice(String devName) {

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
			// Device does not support Bluetooth

			Log.i(TAG, " Device does NOT support Bluetooth");
			return null;
		}

		// check whether bluetooth is enabled
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}

		if ((mBluetoothAdapter != null)) { // &&
			// (mBluetoothAdapter.isEnabled()))
			// {
			setProgressBarIndeterminateVisibility(true);

			// Paired devices
			Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
					.getBondedDevices();
			if (pairedDevices.size() > 0) {
				for (BluetoothDevice device : pairedDevices) {
					if (device.getName().contains(devName)) { // "ANIL-PC", BlackBerry 9800, UC-321PBT, NONIN
						return device;
					}
				}
			}
		}
		return null;
	}

	private void saveSensorBluetoothType(String mac, int type) {
		SharedPreferences preferences = getSharedPreferences("", 0);
		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(mac, type);
		editor.apply();
	}
	private int getSensorBluetoothType(String mac) {
		SharedPreferences preferences = getSharedPreferences("", 0);
		int type = preferences.getInt(mac, SENSOR_TYPE_UNKNOWN);

		Log.d(TAG, "getSensorBluetoothType: type: " + type);

		return type;
	}

	// this method is called when disconnecting from a BLE connection
	private boolean shouldRecoverBluetooth() {
		boolean shouldRecover = mBluetoothSocketTimedOut && getSensorBluetoothType(macAddress) != SENSOR_TYPE_CLASSIC;

		failedBleConnectionCount++;
		shouldRecover = shouldRecover || failedBleConnectionCount >= 2;

		Log.d(TAG, "shouldRecoverBluetooth: shouldRecover: " + shouldRecover);

		return shouldRecover;
	}
	private void recoverBluetooth() {
		Log.d(TAG, "recoverBluetooth: called.");
		if (mBluetoothAdapter != null) {
			mBluetoothAdapter.disable();

			// reinitialize recovery variables
			mBluetoothSocketTimedOut = false;
			failedBleConnectionCount = 0;
		}
	}
	//endregion

	//region UI helpers
	private void pairADevice(int pos,String testFlag){
		appClass.populateSpinner(spnVitals,pos,TestDeviceForaMainActivity.this,testFlag);
	}
	public Boolean preference() {
		Boolean flag = null;
		try {
			String PREFS_NAME = "DroidPrefSc";
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			int scheduletatus = settings.getInt("Scheduletatus", -1);
			Log.i("Droid", " SharedPreferences : " + getApplicationContext() + " is : " + scheduletatus);

			flag = scheduletatus == 1;
		}

		catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		return flag;
	}
	private void setwelcome() {
		String deviceMessage;
		String Pin = dbSensor.SelectTempPIN();

		Constants.setPIN(Pin);// set pin for auto pairing
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){

			Util.soundPlay("Messages/TakeTemp_1.wav");
		}else{

			Util.soundPlay("Messages/TakeTemp.wav");
		}
		deviceMessage = this.getString(R.string.entertemperaturebluetooth);

		txtwelcom.setText(deviceMessage);
	}
	private void add_to_finalList(String vitalValue) {
		String no_to_string = "";
		NumberToString ns = new NumberToString();

		if (vitalValue.contains(".")) {
			Double final_value = Double.parseDouble(vitalValue);
			no_to_string = ns.getNumberToString(final_value);
		} else {
			Long final_value = Long.parseLong(vitalValue);
			no_to_string = ns.getNumberToStringLong(final_value);
		}

		List<String> sellItems = Arrays.asList(no_to_string.split(" "));

		for (String item : sellItems) {
			if (item.length() > 0) {
				final_list.add("Numbers/" + item + ".wav");
			}
		}

		if (final_list.get(final_list.size() - 1).equals("Numbers/zero.wav")) { // remove if last item is zero
			final_list.remove(final_list.size() - 1);
		}
	}
	private void createOKProgressDialog(String title, String message) {
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle(title);
		progressDialog.setMessage(message);
		progressDialog.setCancelable(false);
		progressDialog.setButton(DialogInterface.BUTTON_POSITIVE,
				this.getString(R.string.OK),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						btFora.cancel(true); // Canceling AsyTask
						if (dialog != null)
							dialog.dismiss();
						progressDialog.dismiss();
						if (tts != null) {
							tts.stop();
							tts.shutdown();
						}
						RedirectionActivity();

					}
				});
		progressDialog.show();
	}
	private void RedirectionActivity() {
		if (deviceType == 0) {
			Intent intent = new Intent(getApplicationContext(), UploadMeasurement.class);
			intent.putExtra("reType", 1);
			finish();
			startActivity(intent);

			// redirect to pulse
		} else if (deviceType == 1) {
			String macaddress = getForaMAC(dbSensor.SelectTempSensorName());
			Intent intentfr = new Intent(getApplicationContext(), ForaMainActivity.class);
			intentfr.putExtra("deviceType", 2);
			intentfr.putExtra("macaddress", macaddress);
			finish();
			startActivity(intentfr);

		} else if (deviceType == 2) {
			Intent intent = new Intent(getApplicationContext(), GlucoseEntry.class);

			finish();
			startActivity(intent);
		}
	}
	private void showTempResult(String value){
		txtReading.setVisibility(View.INVISIBLE);
		rocketImage.setVisibility(View.INVISIBLE);
		
		double tempValue;
		String tempUnit;
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		
		if(settingsUserSp.getInt("temperature_unit_id", -1) == 1){
			tempUnit = getResources().getString(R.string.unittemperatureC);
			tempValue = Util.round((Double.parseDouble(value) -32)/1.8);
		}else{
			
			tempUnit = getResources().getString(R.string.unittemperatureF);
			tempValue = Double.parseDouble(value);
		}
				
		
		txtwelcom.setText(getResources().getString(R.string.yourtemperatureis) + " " + tempValue + tempUnit);
		playSound(""+tempValue);
	}
	ArrayList<String> final_list = new ArrayList<String>();
	private void playSound(String value) {
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		final_list.add("Messages/TempIs.wav");
		add_to_finalList(value);
		if(settingsUserSp.getInt("temperature_unit_id", -1) == 1){
			final_list.add("Messages/degreesC.wav");

		} else {
			final_list.add("Messages/degreesF.wav");
		}

		Util.playSound4FileList(final_list, getApplicationContext());
	}
	//endregion
}
