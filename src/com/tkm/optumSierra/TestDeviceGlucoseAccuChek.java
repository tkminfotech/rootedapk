package com.tkm.optumSierra;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.tkm.optumSierra.bean.ClassMeasurement;
import com.tkm.optumSierra.dal.Glucose_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.Util;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@TargetApi(18)
public class TestDeviceGlucoseAccuChek extends Titlewindow {

	private String TAG = "TestGlucoseAccuChek";

	// characteristic UUID strings
	public final static String SERIAL_NUMBER_CHARACTERISTIC_STRING = "00002A25-0000-1000-8000-00805f9b34fb";

	// UUIDs
	public final static UUID SERIAL_NUMBER_CHARACTERISTIC = UUID.fromString(SERIAL_NUMBER_CHARACTERISTIC_STRING);
	public final static UUID SERIAL_NUMBER_SERVICE = UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb");

	public final static UUID GLUCOSE_SERVICE = UUID.fromString("00001808-0000-1000-8000-00805f9b34fb");
	private final static UUID GLUCOSE_MEASUREMENT_CHARACTERISTIC = UUID.fromString("00002A18-0000-1000-8000-00805f9b34fb"); // Glucose Measurement characteristic
	private final static UUID GM_CONTEXT_CHARACTERISTIC = UUID.fromString("00002A34-0000-1000-8000-00805f9b34fb"); // Glucose Measurement Context characteristic
	private final static UUID RACP_CHARACTERISTIC = UUID.fromString("00002A52-0000-1000-8000-00805f9b34fb"); // Record Access Control Point characteristic
	private static final UUID CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

	// op codes
	private final static int OP_CODE_REPORT_STORED_RECORDS = 1;
	private final static int OP_CODE_DELETE_STORED_RECORDS = 2;
	private final static int OPERATOR_GREATER_THEN_OR_EQUAL = 3;
	private final static int OPERATOR_ALL_RECORDS = 1;
	private final static int FILTER_TYPE_SEQUENCE_NUMBER = 1;

	// characteristics
	private BluetoothGattCharacteristic mGlucoseMeasurementCharacteristic;
	private BluetoothGattCharacteristic mGlucoseMeasurementContextCharacteristic;
	private BluetoothGattCharacteristic mRecordAccessControlPointCharacteristic;

	private static final String ACCU_SEQUENCE_NUMBER = "sequenceNumber";
	private static final String SHARED_PREFERENCES_NAME = "aviva";
	private static final String ACCU_CHEK_DEVICE_NAME = "Accu-Chek";

	public static final int REQUEST_DISCOVERABLE_CODE = 42;
	int glucoseId = -1;

	private BluetoothAdapter mBluetoothAdapter = null;

	BluetoothGatt mBluetoothGatt;

	Sensor_db dbSensor = new Sensor_db(this);

	BluetoothDevice detectedDevice = null;

	String detectedMac = "";
	String sharedPreferencesMac = "-1";

	private String mAssignedSerialNumber = ""; // holds the assigned AccuCheck serial number (comes from the database)

	int sequenceNumber = -1;

	// flags-akimbo
	int redirectFlag = 0;
	public int clearFlag = 0;
	boolean is_paired = false;
	boolean flagRedirected = true;
	boolean connected = false;

	boolean mIsScanning = false;

	// holds a queue of paired AccuChek sensors
	private ArrayDeque<BluetoothDevice> mPairedAccuChekSensorQueue;

	private static final int API_LEVEL = Build.VERSION.SDK_INT;
	private boolean autoConnectGatt = API_LEVEL > 19; // auto connect when android version > KitKat

	// will force pairing popup to foreground insted of notification center
	private boolean mShouldForcePairingPopupToForeground_usingDiscoveryHack = API_LEVEL <= 19;

	// this here flag disables the bluetooth on the activity entry on KITKAT or lower.
	// this is to ensure bluetooth stability
	private boolean mShouldDisableBluetooth = API_LEVEL <= 19;

	// indicates whether the connectGatt call is originating from a successful pair operation
	private boolean mIsComingFromPair = false;
	private boolean mIsBonding = false;

	//region UIElements
	ImageView rocketImage;
	AnimationDrawable rocketAnimation;
	BackgroundThread backgroundThread;
	ObjectAnimator AnimPlz, ObjectAnimator;
	TextView txtwelcom, txtReading;
	Button btnClose;
	private String resultValue = "";
	private GlobalClass appClass;
	private Button btn_beginTest;
	private Spinner spnVitals;
	private LinearLayout layout_readingSection;
	//endregion

	private boolean mHasStartedTesting = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		printLineNumber();

		setContentView(R.layout.activity_test_device);
		appClass = (GlobalClass) getApplicationContext();
		Log.e(TAG, "onCreate: TestDeviceGlucoseAccuChek");
		Log.d(TAG, "onCreate: " + this.hashCode());
		appClass.isSupportEnable = true;
		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);
		Typeface type = Typeface.createFromAsset(getAssets(), "fonts/FrutigerLTStd-Roman.otf");
		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);

		btnClose = (Button) findViewById(R.id.testDeviceClose);
		btn_beginTest = (Button) findViewById(R.id.btn_beginTest);
		spnVitals = (Spinner)findViewById(R.id.spn_assignedVitals);
		layout_readingSection = (LinearLayout)findViewById(R.id.layout_readingSection);
		layout_readingSection.setVisibility(View.INVISIBLE);
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			appClass.setBundle(extras);
		}else{
			extras = appClass.getBundle();
		}
		int pos = extras.getInt("Position", -1);
		if(extras.getString("TestFlag").equals("pair")){

			populateDevicesSpinner(pos,"pair");
		}else if(extras.getString("TestFlag").equals("test"))	{

			populateDevicesSpinner(pos,"test");
		}else if(extras.getString("TestFlag").equals("testAll"))	{

			populateDevicesSpinner(pos,"testAll");
		}

		btnClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click
				Intent intent = new Intent(getApplicationContext(), FinalMainActivity.class);
				finish();
				startActivity(intent);
			}
		});

		btn_beginTest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click
				btn_beginTest.setEnabled(false);
				btn_beginTest.setBackgroundColor(Color.parseColor("#a0a0a0"));
				startTesting();
			}
		});

		BluetoothManager mBluetoothManager = (BluetoothManager) getBaseContext().getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = mBluetoothManager.getAdapter();

		mAssignedSerialNumber = dbSensor.Select_GlucoPIN_for_AccuChek();

		// prepend 502 to the serial number of the Accu-Chek sensor
		// Accu-Chek Guide SN on its back doesn't contain the '502' however, the actual SN starts with 502!
		if (!mAssignedSerialNumber.startsWith("502"))
			mAssignedSerialNumber = "502" + mAssignedSerialNumber;

		try {
			SharedPreferences prefs = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
			Editor editor = prefs.edit();

			SharedPreferences accuCheckSharedPreferences = getSharedPreferences(ACCU_SEQUENCE_NUMBER, MODE_PRIVATE);
			Editor accuCheckSharedPreferencesEditor = accuCheckSharedPreferences.edit();

			String Shared_SN = prefs.getString("mAssignedSerialNumber", "-1");
			if (Shared_SN.equals("-1")) {
				editor.putString("mAssignedSerialNumber", mAssignedSerialNumber);
				editor.putString("ID", "-1");
				editor.apply();

			} else if (Shared_SN.equals(mAssignedSerialNumber)) {
				Log.i(TAG, "SAME mAssignedSerialNumber");
				sharedPreferencesMac = prefs.getString("ID", "-1");

			} else {
				editor.putString("mAssignedSerialNumber", mAssignedSerialNumber);
				editor.putString("ID", "-1");
				editor.apply();
				accuCheckSharedPreferencesEditor.putInt("SEQ_N" + mAssignedSerialNumber, -1);
				accuCheckSharedPreferencesEditor.apply();
			}

			Log.d(TAG, "onCreate: sharedPreferencesMac after: " + sharedPreferencesMac);

		} catch (Exception e) {
			e.printStackTrace();
			String stackTraceString = Log.getStackTrace(e);
			Log.e(TAG, stackTraceString);
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		// register the Bluetooth state change receiver
		registerReceiver(mBluetoothStateReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

		// register the pairing state change receiver
		registerReceiver(mPairReceiver, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
	}

	@Override
	protected void onResume() {
		super.onResume();

		// only init if the user has started testing
		if (mHasStartedTesting) {

			// in case of P724 tablet, LE Scanning doesn't pick up any devices if it is started before pairing is complete!
			// so, don't call init while pairing is in progress (onResume is called immediately after pairing popup is dismissed)
			// scanning will restart after the BONDED state is received
			if (!Build.MODEL.equals("MN-724") || !mIsComingFromPair)
				init();
		}
	}

	private void showResult(final String value){
		Log.d(TAG, "showResult: ");
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				playSound(value);

			}
		}, 1000);

		txtReading.setVisibility(View.INVISIBLE);
		rocketImage.setVisibility(View.INVISIBLE);
		txtwelcom.setText(getResources().getString(R.string.yourbloodsugarlevel) +" " + value + getResources().getString(R.string.bloodsugarunit));
	}

	private void playSound(String value) {

		NumberToString ns = new NumberToString();
		String no_to_string = "";
		if (value.contains(".")) {
			Double final_value = Double.parseDouble(value);
			no_to_string = ns.getNumberToString(final_value);
		} else {
			Long final_value = Long.parseLong(value);
			no_to_string = ns.getNumberToStringLong(final_value);
		}

		ArrayList<String> final_list = new ArrayList<String>();
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){

			final_list.add("Messages/BLsugarLevelsat_1.wav");
		}else{

			final_list.add("Messages/BLsugarLevelsat.wav");
		}

		List<String> sellItems = Arrays.asList(no_to_string.split(" "));

		for (String item : sellItems) {
			if (item.toString().length() > 0) {
				final_list.add("Numbers/" + item.toString() + ".wav");
			}
		}

		if (final_list.get(final_list.size() - 1).toString()
				.equals("Numbers/zero.wav")) // remove if last item is zero
		{
			final_list.remove(final_list.size() - 1);
		}

		final_list.add("Messages/MllgmPerDecltr.wav");

		Util.playSound4FileList(final_list, getApplicationContext());
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, " KEYCODE_BACK clicked: fora");

			Intent intent = new Intent(this, FinalMainActivity.class);
			startActivity(intent);
			this.finish();

		}
		return super.onKeyDown(keyCode, event);
	}

	// to add other devices to the spinner
	private void populateDevicesSpinner(int pos, String testFlag){
		appClass.populateSpinner(spnVitals,pos,TestDeviceGlucoseAccuChek.this,testFlag);
	}

	private void startTesting(){
		Log.i(TAG, "startTesting............................................");
		layout_readingSection.setVisibility(View.VISIBLE);
		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		mHasStartedTesting = true;

		backgroundThread = new BackgroundThread();
		backgroundThread.setRunning(true);
		backgroundThread.start();

		init();
	}

	// does everything :)
	private void init() {
		Log.d(TAG, "init: called");

		// on activity entry (KIT-KAT or lower), bluetooth is disabled to maximize stability!
		// bluetooth is re-enabled from a bluetooth state broadcast receiver.
		if (mShouldDisableBluetooth && isBluetoothEnabled()) {
			mShouldDisableBluetooth = false;
			mBluetoothAdapter.disable();
			return;
		}

		if(mBluetoothGatt != null){
			Log.d(TAG, "init: closed gatt");
			mBluetoothGatt.disconnect();
			mBluetoothGatt.close();
			mBluetoothGatt = null;
		}

		if (!isBluetoothEnabled()) {
			Log.e(TAG, "init: bluetooth is disabled");
			mShouldDisableBluetooth = false;
			mBluetoothAdapter.enable();

		} else if (getPairedAccuCheckDevices().size() > 0 && API_LEVEL > 19) { // API 19 -> kitkat
			// only initiate a connection in case of API level > 19 (kitkat)
			ArrayList<BluetoothDevice> pairedSensors = getPairedAccuCheckDevices();

			// get paired AccuChek sensors whose serial numbers aren't stored in the shared preferences
			mPairedAccuChekSensorQueue = getUnStoredPairedSensors(pairedSensors);

			// check shared preferences against the assigned serial number, connect to it, and call it a day
			BluetoothDevice assignedSensor = getAssignedSensor(pairedSensors);
			if (assignedSensor != null) {
				Log.d(TAG, "init: assigned sensor is found: " + assignedSensor);
				detectedDevice = assignedSensor;
				mBluetoothGatt = assignedSensor.connectGatt(TestDeviceGlucoseAccuChek.this, autoConnectGatt, mGattCallback);

			} else {
				Log.d(TAG, "init: assigned sensor WAS NOT found. paired queue size: " + mPairedAccuChekSensorQueue.size());
				// we're always sure that the queue has at least one element
				if (mPairedAccuChekSensorQueue.size() > 0)
					mBluetoothGatt = mPairedAccuChekSensorQueue.remove().connectGatt(TestDeviceGlucoseAccuChek.this, autoConnectGatt, mGattCallback);

				// TODO start a timeout timer
				else {
					Log.d(TAG, "init: Starting an LE scan");
					startLeScan();
				}
			}

		} else { // start an LE scan, if nothing's paired
			Log.d(TAG, "init: Starting an LE scan");
			startLeScan();
		}
	}

	public void updateSharedPrefsCurrentDeviceMacId(String macId) {
		try {
			SharedPreferences prefs = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
			Editor editor = prefs.edit();
			editor.putString("ID", macId);
			editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
			String stackTraceString = Log.getStackTrace(e);
			Log.e(TAG, stackTraceString);
		}
	}
	private void setWelcome() {
		Util.soundPlay("Messages/BloodGlucose.wav");
		txtwelcom.setText(R.string.pleae_take_blood_glucose);
	}

	//region scan callback
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
			try {
				if (device.getName() == null) {
					Log.d(TAG, "onLeScan: device.getName() returned null on device: " + device.getAddress());
					startLeScan();

				} else if (device.getName().contains("Accu-Chek") || device.getName().contains("meter+")) { // make sure that you're pairing with an Accu-Chek sensor
					Log.d(TAG, "onLeScan: Accu-Chek found: " + device);

					// if the scanned device is sure to be wrong (checked against the shared preferences), return
					if (getPairedSensorStoreState(device) == NOT_ASSIGNED)
						return;

					// stop the scan before connecting or pairing
					// will be restarted after connection/pairing failure, or connection with a wrong device, e.g. serial numbers don't match
					stopLeScan();

					// log to ensure that we're not replacing the detected device
					Log.d(TAG, "onLeScan: detectedDevice before: " + detectedDevice + " detectedDevice after: " + device);

					// set device info
					detectedDevice = device;
					detectedMac = device.getAddress();

					// if already paired, connect to the gatt directly (on Android 6, this never happens)
					if (isPaired(detectedDevice)) {
						Log.d(TAG, "scan callback -> run: onLeScan was invoked by an already paired device!");
						mBluetoothGatt = detectedDevice.connectGatt(TestDeviceGlucoseAccuChek.this, autoConnectGatt, mGattCallback);

					} else { // if not, pair first, then on BOND_BONDED state connect to the gatt
						pairDevice(detectedDevice);
						mIsComingFromPair = true;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				String stackTraceString = Log.getStackTrace(e);
				Log.e(TAG, stackTraceString);
			}
		}
	};
	//endregion

	//region gatt callback
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
			Log.d(TAG, "onConnectionStateChange: gatt connection state: " + newState);
			Log.d(TAG, "onConnectionStateChange: gatt connection status: " + status);

			if (mIsComingFromPair && API_LEVEL > 19) { // behavior applied only for Android 5, and 6
				Log.d(TAG, "onConnectionStateChange: coming from a pair operation. return.");
				mIsComingFromPair = false;

				// we're coming from a pair operation. will not disconnect the gatt, we'll simply return without requesting the readings.
				return;
			}

			// for states other than DISCONNECTED, and statuses other than SUCCESS, clean up everything (behavior applies only for Android 5, and 6)
			if (status != BluetoothGatt.GATT_SUCCESS && API_LEVEL > 19 && newState != BluetoothProfile.STATE_DISCONNECTED) {
				Log.d(TAG, "onConnectionStateChange: connection status is bad. cleaning up. ConnectionStatus: " + status);
				Log.d(TAG, "onConnectionStateChange: detectedDevice: " + detectedDevice);

				if (mBluetoothGatt != null) {
					mBluetoothGatt.disconnect();
					mBluetoothGatt.close();
					mBluetoothGatt = null;
				}

				mIsComingFromPair = false;

				// reconnect
				if (detectedDevice == null)
					detectedDevice = gatt.getDevice();

				mBluetoothGatt = detectedDevice.connectGatt(TestDeviceGlucoseAccuChek.this, autoConnectGatt, mGattCallback);
			}

			// if connected, discover the services
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				gatt.discoverServices();

			// disconnected probably after everything completes.
			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {

				Log.i(TAG, "Disconnected from GATT server.");

				ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
				ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
				String mClassName = cn.getClassName();
				SharedPreferences prefs = getSharedPreferences(ACCU_SEQUENCE_NUMBER, MODE_PRIVATE);

				int storedSequenceNumber = prefs.getInt("SEQ_N" + mAssignedSerialNumber , -1);

				Log.d(TAG, "onConnectionStateChange: sharedPreferences sequence number: " + sequenceNumber);
				Log.d(TAG, "onConnectionStateChange: mAllReadings: " + (mAllReadings == null ? "NULL" : mAllReadings.size()));

				if (mAllReadings != null && mAllReadings.size() > 0) {
					try {
						Editor editor = prefs.edit();
						if (sequenceNumber != -1) {
							Log.e(TAG, "shared preferences sequenceNumber: " + sequenceNumber);
							editor.putInt("SEQ_N" + mAssignedSerialNumber, sequenceNumber);
							editor.apply();
						}
					} catch (Exception e) {
						e.printStackTrace();
						String stackTraceString = Log.getStackTrace(e);
						Log.e(TAG, stackTraceString);
					}

					ArrayList<Map.Entry<Integer,GlucoseReading>> readings = new ArrayList<Map.Entry<Integer, GlucoseReading>>();
					for(Map.Entry<Integer,GlucoseReading> item : mAllReadings.entrySet()) {
						readings.add(item);
					}

					Log.d(TAG, "onConnectionStateChange: readings: " + readings.size());

					GlucoseReading lastGlucoseReading = null;
					int lastSequenceNumber = -1;

					for (int i = 0; i < readings.size(); i++) {

						int key = readings.get(i).getKey();
						GlucoseReading value = readings.get(i).getValue();

						Log.i("VALUES: ", "G VAL" + value.hexValue + " TYPE: " + value.hexType + " TIME: " + value.hexTime);

						if(lastSequenceNumber <= key ) { // this will always be true. lastSequenceNumber is coming from
							// this is a newer reading
							lastGlucoseReading = value;
							lastSequenceNumber = key;
						}
					}

					Log.d(TAG, "onConnectionStateChange: lastGlucoseReading: " + lastGlucoseReading);
					Log.d(TAG, "onConnectionStateChange: lastSequenceNumber: " + lastSequenceNumber);

					for (int i = 0; i < readings.size(); i++) {
						GlucoseReading value = readings.get(i).getValue();
						if (value == lastGlucoseReading) {
							glucose_value(value.hexValue, value.hexTime, value.hexType);

						} else {
							glucose_old_value(value.hexValue, value.hexTime, value.hexType);
						}
					}

					Log.i("Result-------------", "Result---------------------" + resultValue);

					final String hexValue = lastGlucoseReading.hexValue;

					// to avoid restarting init on resume
					mHasStartedTesting = false;
					gatt.disconnect();
					gatt.close();


					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							showResult(hexValue);
						}
					});

					emptyAllReadings();

				} else {
					if(API_LEVEL <= 19) {
						Log.d(TAG, "onConnectionStateChange: disabling bluetooth adapter");

						// free resources
						disconnectGatt(gatt);
						disconnectGatt(mBluetoothGatt);
						mBluetoothGatt = null;

						// disable bluetooth
						mBluetoothAdapter.disable();
					}
					is_paired = false;
					flagRedirected = true;
				}
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			Log.d(TAG, "onServicesDiscovered: gatt status: " + status);

			if (status == BluetoothGatt.GATT_SUCCESS) {

				// always check the serial number matches the assigned serial number at the moment the services are discovered
				BluetoothGattService serialNumberService = gatt.getService(SERIAL_NUMBER_SERVICE);
				BluetoothGattCharacteristic serialNumberCharacteristic = serialNumberService.getCharacteristic(SERIAL_NUMBER_CHARACTERISTIC);

				// read the serial number characteristic
				gatt.readCharacteristic(serialNumberCharacteristic);
			}
		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
			Log.d(TAG, "onCharacteristicRead. status: " + status);
			if (status == BluetoothGatt.GATT_SUCCESS) {
				Log.d(TAG, "handleCharacteristicRead: characteristic UUID: " + characteristic.getUuid().toString());

				if (SERIAL_NUMBER_CHARACTERISTIC_STRING.equalsIgnoreCase(characteristic.getUuid().toString())) {

					// 502 is the model number, hard-coded for your pleasure.
					String detectedSerialNumber = "502" + characteristic.getStringValue(0);

					// store the serial number in the shared preferences
					storeSerialNumber(gatt.getDevice().getAddress(), detectedSerialNumber);

					Log.d(TAG, "handleCharacteristicRead: serial number characteristic -> detected serial number: " + detectedSerialNumber);

					if (detectedSerialNumber.equals(mAssignedSerialNumber)) {
						Log.d(TAG, "handleCharacteristicRead: serial number (from DB) matches serial number from characteristic");

						// update detectedDevice reference
						detectedDevice = gatt.getDevice();

						// update the shared preferences key "ID" we don't use it here, but it might be used somewhere else in the app.
						updateSharedPrefsCurrentDeviceMacId(gatt.getDevice().getAddress());
						//
						// get the readings
						//
						BluetoothGattService glucoseService = gatt.getService(GLUCOSE_SERVICE);
						mGlucoseMeasurementCharacteristic = glucoseService.getCharacteristic(GLUCOSE_MEASUREMENT_CHARACTERISTIC);
						mGlucoseMeasurementContextCharacteristic = glucoseService.getCharacteristic(GM_CONTEXT_CHARACTERISTIC);
						mRecordAccessControlPointCharacteristic = glucoseService.getCharacteristic(RACP_CHARACTERISTIC);

						enableGlucoseMeasurementNotification(gatt);

					} else {
						Log.d(TAG, "onCharacteristicRead: serial number doesn't match. disconnecting from device.");
						// clean up
						gatt.disconnect();
						gatt.close();
						mBluetoothGatt = null;

						if (mPairedAccuChekSensorQueue != null && mPairedAccuChekSensorQueue.size() > 0) {
							Log.d(TAG, "onCharacteristicRead: querying the next sensor from the paired queue.");
							mBluetoothGatt = mPairedAccuChekSensorQueue.remove().connectGatt(TestDeviceGlucoseAccuChek.this, autoConnectGatt, mGattCallback);
							// TODO start a timeout

						} else { // we've exhausted the paired devices list, restart the LE scan
							Log.d(TAG, "onCharacteristicRead: paired sensor queue is empty, or null, starting an LE scan.");
							startLeScan();
						}
					}
				}
			}
		}

		@Override
		public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
			Log.d(TAG, "onDescriptorWrite: gatt status: " + status);
			if (status == BluetoothGatt.GATT_SUCCESS) {

				if (GLUCOSE_MEASUREMENT_CHARACTERISTIC.equals(descriptor.getCharacteristic().getUuid())) {
					Log.d(TAG, "onDescriptorWrite: writing to Glucose measurement characteristic.");
					if (mGlucoseMeasurementContextCharacteristic != null) {
						enableGlucoseMeasurementContextNotification(gatt);
					} else {
						enableRecordAccessControlPointIndication(gatt);
					}
				}

				if (GM_CONTEXT_CHARACTERISTIC.equals(descriptor.getCharacteristic().getUuid())) {
					enableRecordAccessControlPointIndication(gatt);
				}

				if (RACP_CHARACTERISTIC.equals(descriptor.getCharacteristic().getUuid())) {
					Log.d(TAG, "onDescriptorWrite: RACP_CHARACTERISTIC");
				}
				Log.d(TAG, "CALL FOR RECORDS ###.....###");
				getLatestRecords(gatt);
			}
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onCharacteristicChanged: called");

			final byte[] data = characteristic.getValue();
			final UUID uuid = characteristic.getUuid();

			if (data != null && data.length > 0) {

				if (GM_CONTEXT_CHARACTERISTIC.equals(uuid)) {
					saveGlucoseContextCharacteristic(characteristic);
				}
				else {
					final StringBuilder stringBuilder = new StringBuilder(data.length);
					for (byte byteChar : data)
						stringBuilder.append(String.format("%02X ", byteChar));

					Log.e(TAG, "DATA FROM:" + stringBuilder.toString());
					if (stringBuilder.toString().length() == 51) {
						for (byte byteChar : data)
							stringBuilder.append(String.format("%02X ", byteChar));
						int gVal = 0;
						//String Type;
						saveGlucoseCharacteristic(characteristic, stringBuilder, gVal);
					}
				}
			}
		}

		private void enableGlucoseMeasurementNotification(final BluetoothGatt gatt) {
			boolean isGlucoseMeasurementCharacteristicEnabled = gatt.setCharacteristicNotification(mGlucoseMeasurementCharacteristic, true);
			Log.d(TAG, " enableGlucoseMeasurementNotification: " + isGlucoseMeasurementCharacteristicEnabled);

			final BluetoothGattDescriptor descriptor = mGlucoseMeasurementCharacteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR_UUID);
			descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);

			gatt.writeDescriptor(descriptor);
		}
		private void enableGlucoseMeasurementContextNotification(final BluetoothGatt gatt) {
			gatt.setCharacteristicNotification(mGlucoseMeasurementContextCharacteristic, true);
			final BluetoothGattDescriptor descriptor = mGlucoseMeasurementContextCharacteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR_UUID);
			descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);

			gatt.writeDescriptor(descriptor);
		}
		private void enableRecordAccessControlPointIndication(final BluetoothGatt gatt) {
			gatt.setCharacteristicNotification(mRecordAccessControlPointCharacteristic, true);
			final BluetoothGattDescriptor descriptor = mRecordAccessControlPointCharacteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR_UUID);
			descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);

			gatt.writeDescriptor(descriptor);
		}

		void getAllRecords(BluetoothGatt gatt) {
			BluetoothGattService glucoseService = gatt.getService(GLUCOSE_SERVICE);
			BluetoothGattCharacteristic characteristic = glucoseService.getCharacteristic(RACP_CHARACTERISTIC);

			setOpCode(characteristic, OP_CODE_REPORT_STORED_RECORDS, OPERATOR_ALL_RECORDS);
			gatt.writeCharacteristic(characteristic);
		}
		void getLatestRecords(BluetoothGatt gatt) {
			try {
				emptyAllReadings();
				SharedPreferences prefs = getSharedPreferences(ACCU_SEQUENCE_NUMBER, MODE_PRIVATE);
				int Shared_SEQUENCE_NUMBER = prefs.getInt("SEQ_N" + mAssignedSerialNumber, -1);

				if (Shared_SEQUENCE_NUMBER == -1)
					getAllRecords(gatt);

				else {
					BluetoothGattService glucoseService = gatt.getService(GLUCOSE_SERVICE);
					BluetoothGattCharacteristic characteristic = glucoseService.getCharacteristic(RACP_CHARACTERISTIC);

					setOpCode(characteristic, OP_CODE_REPORT_STORED_RECORDS, OPERATOR_GREATER_THEN_OR_EQUAL, Shared_SEQUENCE_NUMBER);
					gatt.writeCharacteristic(characteristic);
				}

			} catch (Exception e) {
				e.printStackTrace();
				String stackTraceString = Log.getStackTrace(e);
				Log.e(TAG, stackTraceString);
			}
		}
		void deleteAll() {
			if (clearFlag == 0) {
				Log.d("log", "inside deleteAll() deleting measurement data--");
				clearFlag = 1;
				new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
					@Override
					public void run() {
						if (mBluetoothGatt == null || mRecordAccessControlPointCharacteristic == null)
							return;

						final BluetoothGattCharacteristic characteristic = mRecordAccessControlPointCharacteristic;
						setOpCode(characteristic, OP_CODE_DELETE_STORED_RECORDS, OPERATOR_ALL_RECORDS);
						mBluetoothGatt.writeCharacteristic(characteristic);
					}
				}, 5000);
			}
		}

		private void setOpCode(final BluetoothGattCharacteristic characteristic, final int opCode, final int operator, final Integer... params) {
			final int size = 2 + ((params.length > 0) ? 1 : 0) + params.length * 2;
			characteristic.setValue(new byte[size]);

			// write the operation code
			int offset = 0;
			characteristic.setValue(opCode, BluetoothGattCharacteristic.FORMAT_UINT8, offset);
			offset += 1;

			// write the operator. This is always present but may be equal to
			// OPERATOR_NULL
			characteristic.setValue(operator, BluetoothGattCharacteristic.FORMAT_UINT8, offset);
			offset += 1;

			// if parameters exists, append them. Parameters should be sorted from
			// minimum to maximum. Currently only one or two params are allowed
			if (params.length > 0) {
				// our implementation use only sequence number as a filer type
				characteristic.setValue(FILTER_TYPE_SEQUENCE_NUMBER, BluetoothGattCharacteristic.FORMAT_UINT8, offset);
				offset += 1;

				for (final Integer i : params) {
					characteristic.setValue(i, BluetoothGattCharacteristic.FORMAT_UINT16, offset);
					offset += 2;
				}
			}
		}
	};
	//endregion

	//region BT pairing broadcast receiver
	// this is coming from the call to createBond
	private final BroadcastReceiver mPairReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			if(!mHasStartedTesting)
				return;

			String action = intent.getAction();

			int stateDebug = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
			Log.d(TAG, "mPairReceiver.onReceive: Action: " + action);
			Log.d(TAG, "mPairReceiver.onReceive: state: " + stateDebug);

			if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
				final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
				BluetoothDevice currentDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

				if (state == BluetoothDevice.BOND_BONDED) {
					Log.d(TAG, "onReceive: BOND_BONDED");
					detectedDevice = currentDevice;
					mIsBonding = false;

					if (API_LEVEL > Build.VERSION_CODES.KITKAT) {
						Log.d(TAG, "onReceive: connecting to gatt directly after pairing (> KIT-KAT)");
						// after bonding is complete, connect to the sensor
						mBluetoothGatt = detectedDevice.connectGatt(TestDeviceGlucoseAccuChek.this, autoConnectGatt, mGattCallback);

					} else {
						Log.d(TAG, "onReceive: scanning again (KIT-KAT)");
						startLeScan();
					}

				} else if (state == BluetoothDevice.BOND_BONDING) {
					Log.d(TAG, "onReceive: BOND_BONDING");
					mIsBonding = true;

				} else if (state == BluetoothDevice.BOND_NONE) {
					Log.d(TAG, "onReceive: BOND_NONE");
					mIsBonding = false;

					// restart LE scanning if bonding has failed
					startLeScan();
				}
			}
		}
	};
	//endregion

	private void glucose_value(String data, String measure_date, String Prompt_flag) {
		Log.d(TAG, "glucose_value: value.hexValue (data): " + data);
		Log.d(TAG, "glucose_value: value.hexTime (measure_date): " + measure_date);
		Log.d(TAG, "glucose_value: value.hexType (Prompt_flag): " + Prompt_flag);

		Glucose_db dbcreateglucose = new Glucose_db(TestDeviceGlucoseAccuChek.this);
		ClassMeasurement glucose = new ClassMeasurement();
		SharedPreferences flow = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
		int val = flow.getInt("flow", 0); // #1
		Log.d(TAG, "glucose_value: val: " + val);

		if (val == 1) {
			SharedPreferences section = getSharedPreferences(CommonUtilities.PREFS_NAME_date, 0);
			SharedPreferences.Editor editor_retake = section.edit();
			editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(this));
			editor_retake.commit();
		}
		glucose.setLast_Update_Date((measure_date.replace("p.m.", "PM")).replace("a.m.", "AM"));
		glucose.setStatus(0);
		glucose.setGlucose_Value(Integer.parseInt(data.toString()));
		glucose.setInputmode(0);
		SharedPreferences settings1 = getSharedPreferences(CommonUtilities.PREFS_NAME_date, 0);
		String sectiondate = settings1.getString("sectiondate", "0");

		SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_TIMESLOT_SP, 0);
		String slot = settings.getString("timeslot", "AM");

		glucose.setSectionDate(sectiondate);
		glucose.setTimeslot(slot);
		Log.e(TAG, "Prompt_flag: " + Prompt_flag);
		glucose.setPrompt_flag(Prompt_flag);
	}

	private void glucose_old_value(String data, String measure_date, String Prompt_flag) {
		Log.d(TAG, "glucose_old_value: value.hexValue (data): " + data);
		Log.d(TAG, "glucose_old_value: value.hexTime (measure_date): " + measure_date);
		Log.d(TAG, "glucose_old_value: value.hexType (Prompt_flag): " + Prompt_flag);

		Glucose_db dbcreateglucose = new Glucose_db(TestDeviceGlucoseAccuChek.this);
		ClassMeasurement glucose = new ClassMeasurement();

		glucose.setLast_Update_Date((measure_date.replace("p.m.", "PM")).replace("a.m.", "AM"));
		glucose.setStatus(0);
		glucose.setGlucose_Value(Integer.parseInt(data.toString()));
		glucose.setInputmode(0);
		glucose.setSectionDate(Util.get_patient_time_zone_time(this));
		Log.e(TAG, "Prompt_flag: " + Prompt_flag);
		glucose.setPrompt_flag(Prompt_flag);
	}

	//region bluetooth state broadcast receiver
	private final BroadcastReceiver mBluetoothStateReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(!mHasStartedTesting)
				return;

			final String action = intent.getAction();

			if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
				final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
				switch (state) {
					case BluetoothAdapter.STATE_TURNING_OFF:
						// TODO stop everything
						Log.d(TAG, "onReceive: BLUETOOTH status Turning OFF" );
						break;
					case BluetoothAdapter.STATE_ON:
						Log.d(TAG, "onReceive: BLUETOOTH status is ON" );
						init(); // call init to start over
						break;
					case BluetoothAdapter.STATE_OFF:
						Log.d(TAG, "onReceive: BLUETOOTH status is OFF");

						if(mBluetoothAdapter == null) {
							mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
						}
						Log.d(TAG, "onReceive: enabling Bluetooth");
						mBluetoothAdapter.enable();
						break;
				}
			}
		}
	};
	//endregion

	// house keeping methods
	private boolean isBluetoothEnabled() {
		return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
	}
	private boolean isBluetoothDiscoverable() {
		return API_LEVEL >= 22 || mBluetoothAdapter.getScanMode() == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE;
	}


	// handles SCAN_MODE_CONNECTABLE_DISCOVERABLE intent
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_DISCOVERABLE_CODE) {
			init();
		}
	}

	private ArrayList<BluetoothDevice> getPairedAccuCheckDevices() {
		ArrayList<BluetoothDevice> pairedAccuCheckList = new ArrayList<BluetoothDevice>();

		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
		for (BluetoothDevice device : pairedDevices) {
			// in P724 tablet, the getName() method sometimes returns null!!
			if (device == null || device.getName() == null)
				continue;

			if (device.getName().contains(ACCU_CHEK_DEVICE_NAME) || device.getName().contains("meter+"))
				pairedAccuCheckList.add(device);
		}

		return pairedAccuCheckList;
	}

	// creates a bond with the passed BluetoothDevice.
	// pairing state changes are detected in the mPairReceiver
	// apparently, it HAS to run on the UI thread
	private void pairDevice(final BluetoothDevice device) {
		// will force pairing popup to foreground instead of notification center
		if(mShouldForcePairingPopupToForeground_usingDiscoveryHack) {
			mBluetoothAdapter.startDiscovery();
			mBluetoothAdapter.cancelDiscovery();
		}

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				try {
					Log.d(TAG, "pairDevice: pairing: " + device);
					Method method = device.getClass().getMethod("createBond", (Class[]) null);
					Object isBonded = method.invoke(device, (Object[]) null);

					Log.d(TAG, "pairDevice: isBonded: " + isBonded);

				} catch (Exception e) {
					e.printStackTrace();
					String stackTraceString = Log.getStackTrace(e);
					Log.e(TAG, stackTraceString);
				}
			}
		});

	}

	private class GlucoseReading {
		String hexType;
		String hexValue;
		String hexTime;
		int sequenceNumber;
	}

	private void saveGlucoseContextCharacteristic(BluetoothGattCharacteristic characteristic) {
		Log.d(TAG, "saveGlucoseContextCharacteristic: ");
		String Type;
		Log.e(TAG, "IN GATT=================");

		int sequenceNumber = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 1);
		Log.e(TAG, "SEQUENCE NUMBER 2 =" + sequenceNumber);

		Type = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3).toString();
		if (Type.equals("223")) {
			Type = "3";
		}
		if (Type.equals("3")) {
			Type = "6";
		}
		if (Type.equals("") || Type.equals("5")) {
			Type = "3";
		}

		getReadingRecordForSequenceNumber(sequenceNumber).hexType = Type;
		this.sequenceNumber = sequenceNumber;
	}
	private String saveGlucoseCharacteristic(BluetoothGattCharacteristic characteristic, StringBuilder stringBuilder, int gVal) {
		Log.d(TAG, "saveGlucoseCharacteristic: ");
		String measure_date;
		int offset = 0;
		final int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
		final boolean timeOffsetPresent = (flags & 0x01) > 0;

		int sequenceNumber = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 1);

		if (stringBuilder.toString().substring(39, 41).equals("B0")) {
			gVal = hex2decimal(stringBuilder.toString().substring(36, 38));

		} else if (stringBuilder.toString().substring(39, 41).equals("B1")) {
			gVal = (hex2decimal(stringBuilder.toString().substring(36, 38))) + 256;
		}

		final int year = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 3 + 0);
		final int month = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3 + 2);
		final int day = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3 + 3);
		final int hours = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3 + 4);
		final int minutes = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3 + 5);
		final int seconds = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3 + 6);

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa", Locale.getDefault());
		Calendar calendar = new GregorianCalendar(year, month - 1, day, hours, minutes, seconds);
		if (timeOffsetPresent) {
			// Convert Sensor send time to user seeing time
			int timeOff = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 10);
			Log.e(TAG, "OFFSET TIME:" + timeOff);
			calendar.add(Calendar.MINUTE, timeOff);
		}

		measure_date = sdf.format(calendar.getTime());

		GlucoseReading reading =  getReadingRecordForSequenceNumber(sequenceNumber);
		reading.hexValue = gVal + "";
		reading.hexTime = measure_date;
		reading.sequenceNumber = sequenceNumber;

		if (stringBuilder.toString().substring(42, 44).equals("4A")) {
			reading.hexType = "7";
			Log.e(TAG, "CONTROL SOLUTION: TRUE");
		} else {
			if(reading.hexType == null){
				reading.hexType = "3";
			}
			Log.e(TAG, "CONTROL SOLUTION: FALSE");
		}

		Log.e(TAG, "SEQUENCE NUMBER 1 =" + sequenceNumber);
		Log.d(TAG, "saveGlucoseCharacteristic: HEX_time: " + measure_date);
		Log.d(TAG, "saveGlucoseCharacteristic: HEX_value: " + gVal);

		this.sequenceNumber = sequenceNumber;
		return measure_date;
	}

	private HashMap<Integer, GlucoseReading> mAllReadings;
	private void emptyAllReadings(){
		Log.d(TAG, "emptyAllReadings: ");
		mAllReadings = new HashMap<Integer, GlucoseReading>();
	}
	private GlucoseReading getReadingRecordForSequenceNumber(int sequenceNumber){
		// if mAllReadings is null, initialize it
		if (mAllReadings == null)
			emptyAllReadings();

		if(!mAllReadings.containsKey(sequenceNumber))
			mAllReadings.put(sequenceNumber,new GlucoseReading());
		return mAllReadings.get(sequenceNumber);
	}

	@SuppressLint("DefaultLocale")
	public static int hex2decimal(String s) {
		String digits = "0123456789ABCDEF";
		s = s.toUpperCase();
		int val = 0;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			int d = digits.indexOf(c);
			val = 16 * val + d;
		}
		return val;
	}

	@Override
	public void onStop() {
		Log.d(TAG, "onStop: called");
		try {
			redirectFlag = 1;
			if (mBluetoothAdapter.isDiscovering()) {
				mBluetoothAdapter.cancelDiscovery();
			}
			if (mBluetoothGatt != null) {
				mBluetoothGatt.disconnect();
				mBluetoothGatt.close();
				mBluetoothGatt = null;
			}
			if (mIsScanning) {
				mIsScanning = false;
				mBluetoothAdapter.stopLeScan(mLeScanCallback);
			}

			stopLeScan();

			// unregister all the receivers
			unregisterReceiver(mPairReceiver);
			unregisterReceiver(mBluetoothStateReceiver);

		} catch (Exception e) {
			e.printStackTrace();
			String stackTraceString = Log.getStackTrace(e);
			Log.e(TAG, stackTraceString);
		}
		super.onStop();
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause: called");
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		// code here to show dialog
		// super.onBackPressed(); // optional depending on your needs
	}

	//region loading
	public class BackgroundThread extends Thread {
		volatile boolean running = false;
		int cnt;

		void setRunning(boolean b) {
			running = b;
			cnt = 6;
		}

		@Override
		public void run() {
			while (running) {
				try {
					sleep(60);
					if (cnt-- == 0) {
						running = false;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
					String stackTraceString = Log.getStackTrace(e);
					Log.e(TAG, stackTraceString);
				}
			}
			handler.sendMessage(handler.obtainMessage());
		}
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			setProgressBarIndeterminateVisibility(false);
			boolean retry = true;
			while (retry) {
				try {
					backgroundThread.join();
					retry = false;
				} catch (InterruptedException e) {
					e.printStackTrace();
					String stackTraceString = Log.getStackTrace(e);
					Log.e(TAG, stackTraceString);
				}
			}
			setWelcome();
		}
	};


	//endregion

	// LE scanning utils
	private void startLeScan() {
		mBluetoothAdapter.startLeScan(mLeScanCallback);
		mIsScanning = true;
	}
	private void stopLeScan() {
		mBluetoothAdapter.stopLeScan(mLeScanCallback);
		mIsScanning = false;
	}

	private boolean isPaired(BluetoothDevice device) {
		ArrayList<BluetoothDevice> pairedDevices = getPairedAccuCheckDevices();
		for (BluetoothDevice pairedDevice : pairedDevices) {
			if (pairedDevice.getAddress().equals(device.getAddress()))
				return true;
		}

		return false;
	}

	private static final int ASSIGNED = 0;
	private static final int NOT_ASSIGNED = 1;
	private static final int NOT_FOUND = 2;

	// returns an int representing the sharedPreferences storage state of the given device
	private int getPairedSensorStoreState(BluetoothDevice device) {
		SharedPreferences p = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
		String serialNumber = p.getString(device.getAddress(), null);

		if (serialNumber == null)
			return NOT_FOUND;

		else if (mAssignedSerialNumber.equals(serialNumber))
			return ASSIGNED;

		else
			return NOT_ASSIGNED;
	}

	// checks the shared preferences for a stored mac (key) whose serial number (value) matches the assigned serial number
	private BluetoothDevice getAssignedSensor(ArrayList<BluetoothDevice> pairedDevices) {
		for (BluetoothDevice device : pairedDevices) {
			if (getPairedSensorStoreState(device) == ASSIGNED)
				return device;
		}

		return null;
	}

	// returns an array list of bluetooth devices that haven't been stored in the shared preferences yet. used to populate the ArrayDeque member
	private ArrayDeque<BluetoothDevice> getUnStoredPairedSensors(ArrayList<BluetoothDevice> pairedSensors) {
		ArrayDeque<BluetoothDevice> unStoredPairedSensors = new ArrayDeque<BluetoothDevice>();
		for (BluetoothDevice device : pairedSensors) {
			if (getPairedSensorStoreState(device) == NOT_FOUND)
				unStoredPairedSensors.add(device);
		}

		return unStoredPairedSensors;
	}

	// stores the given serial number in the shared preferences
	private void storeSerialNumber(String mac, String serialNumber) {
		SharedPreferences p = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
		Editor editor = p.edit();

		editor.putString(mac, serialNumber);
		editor.apply();
	}

	private void disconnectGatt(BluetoothGatt gatt) {
		gatt.disconnect();
		gatt.close();
	}

	private void printLineNumber() {
		Log.w(TAG, "LineNumber: " + Thread.currentThread().getStackTrace()[2].getLineNumber());
	}
}