package com.tkm.optumSierra;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.tkm.optumSierra.GlucoseReader.BackgroundThread;

import com.tkm.optumSierra.bean.ClassMeasurement;
import com.tkm.optumSierra.dal.Glucose_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.service.OmronBleService;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.PINReceiver;
import com.tkm.optumSierra.util.Util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class TestDeviceGlucoseReader extends Titlewindow {

	private static final String PAIRING = "android.bluetooth.device.action.PAIRING_REQUEST";
	public static final int MESSAGE_OK = 0;
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;
	public static final int MESSAGE_RESULT = 6;
	public static final int MESSAGE_ERRER = 7;
	public static final int MESSAGE_DEVICE = 8;
	public static final int MESSAGE_BATTERY = 9;
	private String TAG = "P724Activity Sierra";
	public static final String PREFS_NAME_SCHEDULE = "DroidPrefSc";
	static final String HEXES = "0123456789ABCDEF";
	public static final int PROGRESS_BAR_TYPE = 0;
	public int back = 0;
	//int glucoseId;
	TextView textView = null;
	ImageView rocketImage;
	int measureId;
	AnimationDrawable rocketAnimation;
	BackgroundThread backgroundThread;
	Util utilobjct = new Util();
	ObjectAnimator AnimPlz, ObjectAnimator;
	public boolean cancel_flag = false;
	TextView txtwelcom, txtReading;
	boolean started = false;
	private BluetoothServerSocket mmServerSocket;
	BluetoothAdapter mBluetoothAdapter;
	public boolean paired_device = false;
	private final BroadcastReceiver mybroadcast = new PINReceiver();
	private static String mac_address = "";
	Sensor_db dbSensor = new Sensor_db(this);
	BluetoothDevice device;
	Button btnClose;
	private GlobalClass appClass;
	private Button btn_beginTest;
	private boolean isBeginTest = false;
	private Spinner spnVitals;
	private LinearLayout layout_readingSection;
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_device);
		Log.e(TAG, "glucose BT page started");	
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);

		btnClose = (Button) findViewById(R.id.testDeviceClose);
		btn_beginTest = (Button) findViewById(R.id.btn_beginTest);
		spnVitals = (Spinner)findViewById(R.id.spn_assignedVitals);
		layout_readingSection = (LinearLayout)findViewById(R.id.layout_readingSection);
		layout_readingSection.setVisibility(View.INVISIBLE);
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			appClass.setBundle(extras);
		}else{
			extras = appClass.getBundle();
		}
		int pos = extras.getInt("Position", -1);
		if(extras.getString("TestFlag").equals("pair")){

			pairADevice(pos,"pair");	
		}else if(extras.getString("TestFlag").equals("test"))	{

			pairADevice(pos,"test");
		}else if(extras.getString("TestFlag").equals("testAll"))	{

			pairADevice(pos,"testAll");
		}

		btnClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click				
				Intent intent = new Intent(getApplicationContext(), FinalMainActivity.class);
				finish();
				startActivity(intent);
			}
		});
		btn_beginTest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click	
				btn_beginTest.setEnabled(false);
				btn_beginTest.setBackgroundColor(Color.parseColor("#a0a0a0"));
				startTesting();
			}
		});

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		Log.i(TAG,
				"-------------------oncreate--  GlucoseReader  activity------------");

	}

	public class BackgroundThread extends Thread {
		volatile boolean running = false;
		int cnt;

		void setRunning(boolean b) {
			running = b;
			cnt = 6;
		}

		@Override
		public void run() {
			while (running) {
				try {
					sleep(60);
					if (cnt-- == 0) {
						running = false;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			handler.sendMessage(handler.obtainMessage());
		}
	}

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			setProgressBarIndeterminateVisibility(false);
			// progressDialog.dismiss();

			boolean retry = true;
			while (retry) {
				try {
					backgroundThread.join();
					retry = false;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			setwelcome();
		}

	};

	private void setwelcome() {

		Util.soundPlay("Messages/BloodGlucose.wav");

		txtwelcom.setText(R.string.pleae_take_blood_glucose);
//		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
//		AnimPlz.setDuration(1500);
//		AnimPlz.start();
//
//		AnimPlz.addListener(new AnimatorListenerAdapter() {
//			public void onAnimationEnd(Animator animation) {
//
//				Log.e(TAG, "AnimPlz started");
//
//				txtReading.setVisibility(View.VISIBLE);
//				rocketImage.setVisibility(View.VISIBLE);
//
//			}
//		});

		AcceptThread aa = new AcceptThread();
		aa.start();

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.d("onActivityResult ", "onActivityResult " + resultCode);
		Log.d("onActivityResult ", "requestCode is " + requestCode);

		switch (requestCode) {
		case 1:
			// When the request to enable Bluetooth returns
			Log.d("onactivityresult", "new thread created");
			AcceptThread aa = new AcceptThread();
			aa.start();
			started = true;

		}
	}

	public synchronized void connected(BluetoothSocket socket) {

		String TAG = "....connected..........";
		Log.d(TAG, "connected");

		ConnectedThread mConnectedThread = new ConnectedThread(socket);
		mConnectedThread.start();

	}

	@SuppressLint("NewApi")
	private class AcceptThread extends Thread {

		public AcceptThread() {
			Log.d("reached", ".........AcceptThread().......");

			BluetoothServerSocket tmp = null;

			try {
				tmp = mBluetoothAdapter
						.listenUsingInsecureRfcommWithServiceRecord(
								"BluetoothChatInsecure",
								UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
				// 00001234-0000-1000-8000-00805F9B34FB
				//
			} catch (IOException e) {
				e.printStackTrace();
			}

			mmServerSocket = tmp;
		}

		public void run() {
			BluetoothSocket socket = null;
			while (true) {
				try {
					Log.d("AcceptThread", "listening..............");

					socket = mmServerSocket.accept();
				} catch (IOException e) {
					Log.d("AcceptThread", "not listening");
					break;
				}
				if (socket != null) {
					Log.d("reached", ".........connected().......");
					connected(socket);
					try {
						if (mmServerSocket != null)
							mmServerSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					break;
				}
			}
		}

		public void cancel() {
			try {
				mmServerSocket.close();
			} catch (IOException e) {
			}
		}
	}

	private class ConnectedThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;
		String TAG = "connected Thread";

		public ConnectedThread(BluetoothSocket socket) {
			Log.d(TAG, "create ConnectedThread...." + socket);
			mmSocket = socket;
			Log.d(TAG, "print socket " + socket.getRemoteDevice());
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			// Get the BluetoothSocket input and output streams
			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;

		}

		public void run() {
			Log.i(TAG, "BEGIN mConnectedThread");

			boolean reqSent = false;
			boolean timeSet = false;
			boolean erased = false;
			boolean done = false;
			String adaptertype = "";
			StringBuffer sb = new StringBuffer("");
			int bytes = 0;
			int bytesSum = 0;
			while (true) {

				try {
					int repeat = 0;
					while ((mmInStream.available() <= 0) && (repeat >= 0)) {
						try {
							Log.i(TAG, "inside repeat.. getting data");
							Thread.sleep(500);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						repeat--;
					}

					byte[] buffer = new byte[1048];

					// Read from the InputStream
					bytes = mmInStream.read(buffer);
					bytesSum += bytes;

					sb.append(getHex(buffer, bytes));

					Log.i("sb", sb.toString());

					// notification received
					// req for new data
					if (!reqSent && bytesSum == 15) {
						bytesSum = 0;

						byte[] dd = processReq(sb);
						write(dd);

						reqSent = true;
						sb.setLength(0);
						// data received, send ack
					} else if (reqSent && bytesSum == 27) {

						bytesSum = 0;
						boolean ack = processInput(sb);

						// send Acknowledge
						if (ack) {

							ArrayList<String> notification = new ArrayList<String>();
							// String senddata =
							// "80 0F 32 02 47 48 5A 36 38 33 36 43 59 00 DE";
							notification.clear();

							String data = sb.toString();
							String[] x = data.split(" ");

							// collect array
							for (int i = 0; i <= x.length - 1; i++) {
								notification.add(x[i]);
							}

							// Create reqdata commmand from notification array
							ArrayList<String> reqData = new ArrayList<String>();

							reqData.add("80");
							reqData.add("11");
							reqData.add("34");

							for (int i = 3; i < 16; i++)
								reqData.add(notification.get(i));
							// reqData = notification.toString();

							String check = getCheckSum(reqData);

							String command = "";

							for (int i = 0; i < reqData.size(); i++)
								command = command.concat(reqData.get(i));

							command = command.concat(check);

							byte[] dd = hexStringToByteArray(command);
							write(dd);
							Log.e("writing aknowledge", command);
						}
						sb.setLength(0);
						// all data recevied, set dataTime
					} else if (reqSent && bytesSum == 17) {
						processInput(sb);
						bytesSum = 0;
						byte[] aa = processDateTime(sb);
						write(aa);
						timeSet = true;

						sb.setLength(0);

						// dateTime is set, erase all data
					} else if (!done && timeSet && bytesSum == 21) {

						bytesSum = 0;

						adaptertype = sb.substring(9, 11);
						Log.e("type", "blutooth adapter typren " + adaptertype);
						if (adaptertype.equals("02")) {

							ArrayList<String> notification = new ArrayList<String>();
							// String senddata =
							// "80 0F 32 02 47 48 5A 36 38 33 36 43 59 00 DE";
							notification.clear();

							String data = sb.toString();
							String[] x = data.split(" ");

							// collect array
							for (int i = 0; i <= x.length - 1; i++) {
								notification.add(x[i]);
							}

							// Create reqdata commmand from notification array
							ArrayList<String> reqData = new ArrayList<String>();

							reqData.add("80");
							reqData.add("0F");
							reqData.add("E2");

							for (int i = 3; i < 14; i++)
								reqData.add(notification.get(i));
							// reqData = notification.toString();

							String check = getCheckSum(reqData);

							String command = "";

							for (int i = 0; i < reqData.size(); i++)
								command = command.concat(reqData.get(i));

							command = command.concat(check);

							byte[] dd = hexStringToByteArray(command);
							write(dd);
							Log.e("erasing data" + "", command);
						} else {
							bytesSum = 16;
						}
						erased = true;
						done = true;
						sb.setLength(0);
						// ///////////////////////////////////////

					} else if (erased && bytesSum >= 16) {
						String finish;
						bytesSum = 0;

						if (adaptertype.equals("02")) {
							finish = "8005F00286";
						} else {
							finish = "8005F00385";
						}
						byte[] dd = hexStringToByteArray(finish);
						write(dd);
						sb.setLength(0);
					}

				} catch (IOException e) {
					Log.e(TAG, "disconnected a and d");
					break;
				}
			}
		}

		public void write(byte[] buffer) {
			try {
				// cleast data
				mmOutStream.write(buffer);
				mmOutStream.flush();

			} catch (IOException e) {
				Log.e(TAG, "Exception during write");
			}
		}

		public void cancel() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed");
			}
		}
	}

	public byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
					.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

	public static String getHex(byte[] raw, int len) {
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(3 * raw.length);
		for (final byte b : raw) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4))
			.append(HEXES.charAt((b & 0x0F))).append(" ");
			if (--len == 0)
				break;
		}
		return hex.toString();
	}

	// Process bytes arriving from the device
	public String getCheckSum(ArrayList<String> data) {

		int checkSum = 0;
		for (int i = 0; i < data.size(); i++)
			checkSum = checkSum + Integer.parseInt(data.get(i), 16);

		Log.e("Check Sum",
				"" + checkSum + " hex :" + Integer.toHexString(checkSum));
		String sum = "FD";
		int Check = Integer.parseInt(sum, 16) - checkSum;

		String test = Integer.toHexString(Check);
		String C = test.substring(test.length() - 2);

		Log.e("Check Sum ", "" + C);
		return C;
	}

	public static String convert(int n) {
		return Integer.toHexString(n);
	}

	private byte[] processDateTime(StringBuffer sb) {
		String command = "";
		try {

			String TAG = "processDateTime";
			Log.i(TAG, "Received  data from stream");

			Log.i(TAG, "Received bytes");
			ArrayList<String> notification = new ArrayList<String>();
			notification.clear();

			String data = sb.toString();
			String[] x = data.split(" ");

			Log.d("", data);
			for (int i = 0; i <= x.length - 1; i++) {
				notification.add(x[i]);
			}

			ArrayList<String> reqData = new ArrayList<String>();

			reqData.add("80");
			reqData.add("15");
			reqData.add("E6");

			// String senddata2 =
			// "800F320247485A36383336435900DE";
			for (int i = 3; i < 14; i++)
				reqData.add(notification.get(i));
			// reqData = notification.toString();

			String patient_time = Util.get_patient_time_zone_time(this);
			int year1 = Integer.parseInt(patient_time.substring(6, 10));
			int month1 = Integer.parseInt(patient_time.substring(0, 2));
			int day1 = Integer.parseInt(patient_time.substring(3, 5));
			int mHours = Integer.parseInt(patient_time.substring(11, 13));
			int mMinutes = Integer.parseInt(patient_time.substring(14, 16));
			int sec = Integer.parseInt(patient_time.substring(17, 19));

			int year = year1 - 2000;
			if (convert(year).length() > 1)
				reqData.add("" + convert(year));
			else
				reqData.add("0" + convert(year));

			int month = month1;
			if (convert(month).length() > 1)
				reqData.add("" + convert(month));
			else
				reqData.add("0" + convert(month));

			int day = day1;
			if (convert(day).length() > 1)
				reqData.add("" + convert(day));
			else
				reqData.add("0" + convert(day));

			int hour = mHours;
			if (hour == 0) {
				hour = 12;
			}
			if (convert(hour).length() > 1)
				reqData.add("" + convert(hour));
			else
				reqData.add("0" + convert(hour));

			int min = mMinutes;
			if (convert(min).length() > 1)
				reqData.add("" + convert(min));
			else
				reqData.add("0" + convert(min));

			int seconds = sec;
			if (convert(seconds).length() > 1)
				reqData.add("" + convert(seconds));
			else
				reqData.add("0" + convert(seconds));

			String datatime = "" + year + " " + month + " " + day + " " + hour
					+ " " + min + " " + seconds;

			String check = getCheckSum(reqData);

			for (int i = 0; i < reqData.size(); i++)
				command = command.concat(reqData.get(i));

			command = command.concat(check);
			command = command.toUpperCase();
			Log.e("reqData",
					command + "      " + datatime + "   " + command.length());

			byte[] ww = hexStringToByteArray(command);
			return ww;
		} catch (IndexOutOfBoundsException e) {
			Log.i(TAG, "IndexOutOfBoundsException----");
			byte[] ww = hexStringToByteArray(command);
			return ww;
		}
	}

	private byte[] processReq(StringBuffer sb) {

		String TAG = "processReq";
		Log.i(TAG, "Received  data from stream");

		Log.i(TAG, "Received bytes");
		ArrayList<String> notification = new ArrayList<String>();
		// String senddata =
		// "80 0F 32 02 47 48 5A 36 38 33 36 43 59 00 DE";
		notification.clear();

		String data = sb.toString();
		String[] x = data.split(" ");

		Log.d("", data);

		// collect array
		for (int i = 0; i <= x.length - 1; i++) {
			notification.add(x[i]);
		}

		// Create reqdata commmand from notification array
		ArrayList<String> reqData = new ArrayList<String>();

		reqData.add("80");
		reqData.add("0F");
		reqData.add("32");

		// String senddata2 =
		// "800F320247485A36383336435900DE";
		for (int i = 3; i < 14; i++)
			reqData.add(notification.get(i));
		// reqData = notification.toString();

		String check = getCheckSum(reqData);

		String command = "";

		for (int i = 0; i < reqData.size(); i++)
			command = command.concat(reqData.get(i));

		command = command.concat(check);
		Log.e("reqData", command);

		byte[] dd = hexStringToByteArray(command);

		return dd;
	}

	String output = "";

	private boolean processInput(StringBuffer sb) {

		boolean ack = false;
		String TAG = " process";
		String data = sb.toString();
		Log.d(TAG, "Hex data<<< " + data);
		String[] x = data.split(" ");
		String deviceType;
		if (x.length > 3) {
			if (x[2].equals("33") && x.length > 23) {

				String index = x[14].concat(x[15]);
				String hex = x[22].concat(x[23]);

				int year = Integer.parseInt(x[16], 16) + 2000;
				int month = Integer.parseInt(x[17], 16);
				int day = Integer.parseInt(x[18], 16);
				int hrs = Integer.parseInt(x[19], 16);
				int mnts = Integer.parseInt(x[20], 16);
				int sec = Integer.parseInt(x[21], 16);

				int indvalue = Integer.parseInt(index, 16);
				final int value = Integer.parseInt(hex, 16);
				Log.d(TAG, "Hex data<<< " + "value is " + value);
				//				Intent intent = new Intent(TestDeviceGlucoseReader.this,
				//						ShowGlucoseActivity.class);

				String unit = x[24];
				if (unit.equals("00")) {
					unit = "mg/dl";
				} else {
					unit = "mmol/L";
				}

				deviceType = x[25];

				output += "index:" + indvalue + " Value: " + value + " " + unit
						+ "  " + day + "/" + month + "/" + year + "  " + hrs
						+ ":" + mnts + ":" + sec + "   device type: "
						+ deviceType + "\n";
				String date = month + "/" + day + "/" + year + " " + hrs + ":"
						+ mnts + ":" + sec;
				Log.d(TAG, "date<<< " + date);
				Log.d(TAG, "output<<< " + output);
				if (indvalue == 0) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {

							showResult(value);

						}
					});

				} else {
					Log.d(TAG, " inserting old glucose date");
					//glucose_old_value(value + "", date);
				}
				ack = true;
				return ack;
			} else if (x[2].equals("35")) {
				ack = false;
				return ack;

			}
		}
		return ack;
	}

	private void glucose_valuew(String data, String measure_date) {
		Glucose_db dbcreateglucose = new Glucose_db(TestDeviceGlucoseReader.this);
		ClassMeasurement glucose = new ClassMeasurement();
		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = flow.getInt("flow", 0); // #1
		if (val == 1) {

			SharedPreferences section = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);
			SharedPreferences.Editor editor_retake = section.edit();
			editor_retake.putString("sectiondate",
					Util.get_patient_time_zone_time(this));
			editor_retake.commit();
		}
		glucose.setLast_Update_Date(measure_date);
		glucose.setStatus(0);
		glucose.setGlucose_Value(Integer.parseInt(data.toString()));
		glucose.setInputmode(0);
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.PREFS_NAME_date, 0);
		String sectiondate = settings1.getString("sectiondate", "0");

		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.USER_TIMESLOT_SP, 0);
		String slot = settings.getString("timeslot", "AM");

		glucose.setSectionDate(sectiondate);
		glucose.setTimeslot(slot);
		glucose.setPrompt_flag("3");

		Log.e(TAG, "Saving Glucose value to db from bluetooth device");

		//glucoseId = dbcreateglucose.InsertGlucoseMeasurement(glucose);
	}

	private void glucose_old_value(String data, String measure_date) {
		Glucose_db dbcreateglucose = new Glucose_db(TestDeviceGlucoseReader.this);
		ClassMeasurement glucose = new ClassMeasurement();

		glucose.setLast_Update_Date(measure_date);
		glucose.setStatus(0);
		glucose.setGlucose_Value(Integer.parseInt(data.toString()));
		glucose.setInputmode(0);
		glucose.setSectionDate(Util.get_patient_time_zone_time(this));
		glucose.setPrompt_flag("3");
		Log.e(TAG, "Saving Glucose value to db from bluetooth device");

		int 	glucose_Id = dbcreateglucose.InsertGlucoseMeasurement(glucose);
		dbcreateglucose.UpdateglucoseData_as_valid(glucose_Id);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d("ConnectThread", "---+++++++-onResume-+++++++------ ");
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
		registerReceiver(mybroadcast, filter);

	}

	@Override
	protected void onPause() {

		super.onPause();

	}

	private final BroadcastReceiver ActionFoundReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

				Log.d("device.getAddress()",
						"device.getAddress()- " + device.getAddress());
				Log.d("device.getName()",
						"device.getName()- " + device.getName());
				String name = device.getAddress();
				if (name.contains(mac_address)) {
					mBluetoothAdapter.cancelDiscovery();
					Log.d("ConnectThread", "STARTED--------------- ");
					if (!paired_device) {
						try {
							Log.d("ConnectThread",
									"Begin to pair---------------- ");
							Method method = device.getClass().getMethod(
									"createBond", (Class[]) null);
							method.invoke(device, (Object[]) null);
						} catch (Exception e) {
							e.printStackTrace();
						}

					} else {
						Log.d("ConnectThread",
								"not required already paired--- ");
					}

				}
			} else {
			}
		}
	};

	private void load() {
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothDevice.ACTION_UUID);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		filter.addAction(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothDevice.ACTION_NAME_CHANGED);
		filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
		filter.addAction(PAIRING);
		registerReceiver(ActionFoundReceiver, filter);
		mBluetoothAdapter.startDiscovery();
	}

	private void get_paired_device() {
		Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
		for (BluetoothDevice device : devices) {
			Log.e("  Device: ", device.getName() + ", " + device.getAddress());
			if (device.getAddress().contains(mac_address)) {
				Log.d("insdie ", "already paired with the tablet ");
				paired_device = true;
				return;
			}

		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mBluetoothAdapter != null) {
			mBluetoothAdapter.cancelDiscovery();
		}
		unregisterReceiver(mybroadcast);
		if(isBeginTest)
			unregisterReceiver(ActionFoundReceiver);
	}

	public void terminate() {

		if (mBluetoothAdapter.isEnabled()) {
			mBluetoothAdapter.cancelDiscovery();
		}

		try {
			if (mmServerSocket != null) {
				mmServerSocket.close();
			}
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (NullPointerException e) {
		}
		try {
			if (mmServerSocket != null) {
				mmServerSocket.close();
			}
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (NullPointerException e) {
		}

	}

	@Override
	public void onStop() {
		Log.d("onStop ", "++++++++++++++onStop+++++++++++++- ");
		/*if (!isMyAdviceServiceRunning()) {

			Intent schedule = new Intent();
			schedule.setClass(GlucoseReader.this, AdviceService.class);
			startService(schedule); // Starting Advice Service

		}*/	
		super.onStop();
		terminate();
	}

	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			// macAddress.substring(i, i+2)
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		Log.i(TAG, " : Connect to macAddress " + macAddress);
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}
	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private void pairADevice(int pos,String testFlag){
		//		txtwelcom.setVisibility(View.GONE);
		//		txtReading.setText(getResources().getString(R.string.pairingDevice));
		appClass.populateSpinner(spnVitals,pos,TestDeviceGlucoseReader.this,testFlag);
	}

	private void startTesting(){
		layout_readingSection.setVisibility(View.VISIBLE);
		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();


		//stopService(new Intent(GlucoseReader.this, AdviceService.class));
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();



		mac_address = getForaMAC(dbSensor.Select_GlucoName_adapter_mac());
		if (mac_address.length() != 17) {
			mac_address = "00:80:25:C3:05:48"; // set default MAC
		}
		device = mBluetoothAdapter.getRemoteDevice(mac_address);
		String Pin = dbSensor.Select_GlucoName_adapter_PIN();
		Constants.setPIN(Pin);

		Log.i(TAG, "MAC-" + mac_address);
		Log.i(TAG, "Pin-" + Pin);

		get_paired_device();
		load();
		backgroundThread = new BackgroundThread();
		backgroundThread.setRunning(true);
		backgroundThread.start();
		isBeginTest = true;
	}

	private void showResult(final int value){
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {

				playSound(""+value);

			}
		}, 1000);
		txtReading.setVisibility(View.INVISIBLE);
		rocketImage.setVisibility(View.INVISIBLE);
		txtwelcom.setText(getResources().getString(R.string.yourbloodsugarlevel) +" "
				
				+ value + getResources().getString(R.string.bloodsugarunit));
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			

			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}
	
	private void playSound(String value) {

		NumberToString ns = new NumberToString();		
		String no_to_string = "";
		if (value.contains(".")) {
			Double final_value = Double.parseDouble(value);
			no_to_string = ns.getNumberToString(final_value);
		} else {
			Long final_value = Long.parseLong(value);
			no_to_string = ns.getNumberToStringLong(final_value);
		}

		ArrayList<String> final_list = new ArrayList<String>();
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){
			
			final_list.add("Messages/BLsugarLevelsat_1.wav");
		}else{
			
			final_list.add("Messages/BLsugarLevelsat.wav");
		}

		List<String> sellItems = Arrays.asList(no_to_string.split(" "));

		for (String item : sellItems) {
			if (item.toString().length() > 0) {
				final_list.add("Numbers/" + item.toString() + ".wav");
			}
		}

		if (final_list.get(final_list.size() - 1).toString()
				.equals("Numbers/zero.wav")) // remove if last item is zero
		{
			final_list.remove(final_list.size() - 1);
		}

		final_list.add("Messages/MllgmPerDecltr.wav");

		Util.playSound4FileList(final_list, getApplicationContext());
	}
}