package com.tkm.optumSierra;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.taidoc.pclinklibrary.android.bluetooth.util.BluetoothUtil;
import com.taidoc.pclinklibrary.connection.AndroidBluetoothConnection;
import com.taidoc.pclinklibrary.connection.AndroidBluetoothConnection.LeConnectedListener;
import com.taidoc.pclinklibrary.connection.util.ConnectionManager;
import com.taidoc.pclinklibrary.constant.PCLinkLibraryConstant;
import com.taidoc.pclinklibrary.constant.PCLinkLibraryEnum.User;
import com.taidoc.pclinklibrary.exceptions.CommunicationTimeoutException;
import com.taidoc.pclinklibrary.exceptions.ExceedRetryTimesException;
import com.taidoc.pclinklibrary.exceptions.NotConnectSerialPortException;
import com.taidoc.pclinklibrary.exceptions.NotSupportMeterException;
import com.taidoc.pclinklibrary.meter.AbstractMeter;
import com.taidoc.pclinklibrary.meter.record.AbstractRecord;
import com.taidoc.pclinklibrary.meter.record.BloodGlucoseRecord;
import com.taidoc.pclinklibrary.meter.util.MeterManager;
import com.tkm.optumSierra.bean.ClassMeasurement;
import com.tkm.optumSierra.dal.Glucose_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.Util;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class TestDeviceGlucoseTaidoc extends Titlewindow {

	private GlobalClass globalClass;
	ImageView rocketImage;
	AnimationDrawable rocketAnimation;
	BackgroundThread backgroundThread;
	ObjectAnimator AnimPlz, mObjectAnimator;
	TextView txtwelcom, txtReading;

	private String TAG = "TD MainActivity";
	private BluetoothAdapter mBluetoothAdaptere = null;
	boolean is_paired = false;
	private static final int REQUEST_ENABLE_BTE = 1;
	//int glucoseId;
	private String mMacAddress;
	private boolean mBLEMode = true;
	// Message types sent from the meterCommuHandler Handler
	public static final int MESSAGE_STATE_CONNECTING = 1;
	public static final int MESSAGE_STATE_CONNECT_FAIL = 2;
	public static final int MESSAGE_STATE_CONNECT_DONE = 3;
	public static final int MESSAGE_STATE_CONNECT_NONE = 4;
	public static final int MESSAGE_STATE_CONNECT_METER_SUCCESS = 5;
	public static final int MESSAGE_STATE_CHECK_METER_INFORMATION = 6;
	public static final int MESSAGE_STATE_CHECK_METER_BT_DISTANCE = 7;
	public static final int MESSAGE_STATE_CHECK_METER_BT_DISTANCE_FAIL = 8;
	public static final int MESSAGE_STATE_NOT_SUPPORT_METER = 9;
	public static final int MESSAGE_STATE_NOT_CONNECT_SERIAL_PORT = 10;
	public static final int MESSAGE_STATE_SCANNED_DEVICE = 11;
	Button btnmanual;
	// Tag and Debug flag
	private static final boolean DEBUG = true;
	private static String mac_address = "";
	Sensor_db dbSensor = new Sensor_db(this);
	boolean redirectFlag = false;
	String STORAGE_COUNT = "storage_count";

	IntentFilter intFilter;
	public static final int REQUEST_DISCOVERABLE_CODE = 42;
	boolean mScanning = false, fetcherFlag = false, conSuccess = false;
	private Button btn_beginTest;
	private Spinner spnVitals;
	private LinearLayout layout_readingSection;
	private String result = "";
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.d(TAG, "onCreate: " + this.hashCode());

		setContentView(R.layout.activity_test_device);
		Log.e(TAG, "Glucose Taidoc");
		globalClass = (GlobalClass)getApplicationContext();	
		globalClass.isSupportEnable = true;
		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);

		btnmanual = (Button) findViewById(R.id.testDeviceClose);
		btn_beginTest = (Button) findViewById(R.id.btn_beginTest);
		spnVitals = (Spinner)findViewById(R.id.spn_assignedVitals);
		layout_readingSection = (LinearLayout)findViewById(R.id.layout_readingSection);
		layout_readingSection.setVisibility(View.INVISIBLE);
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			globalClass.setBundle(extras);
		}else{
			extras = globalClass.getBundle();
		}
		int pos = extras.getInt("Position", -1);
		if(extras.getString("TestFlag").equals("pair")){

			pairADevice(pos,"pair");	
		}else if(extras.getString("TestFlag").equals("test"))	{

			pairADevice(pos,"test");
		}else if(extras.getString("TestFlag").equals("testAll"))	{

			pairADevice(pos,"testAll");
		}

		btnmanual.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click				
				Intent intent = new Intent(getApplicationContext(), FinalMainActivity.class);
				finish();
				startActivity(intent);
			}
		});
		btn_beginTest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click	
				btn_beginTest.setEnabled(false);
				btn_beginTest.setBackgroundColor(Color.parseColor("#a0a0a0"));
				startTesting();
			}
		});

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		// txtWelcome.setText("Please step on the scale.");
		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);
	}

	public class BackgroundThread extends Thread {
		volatile boolean running = false;
		int cnt;

		void setRunning(boolean b) {
			running = b;
			cnt = 6;
		}

		@Override
		public void run() {
			while (running) {
				try {
					sleep(60);
					if (cnt-- == 0) {
						running = false;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			handler.sendMessage(handler.obtainMessage());
		}
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			setProgressBarIndeterminateVisibility(false);
			boolean retry = true;
			while (retry) {
				try {
					backgroundThread.join();
					retry = false;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			setwelcome();
		}

	};

	@SuppressWarnings("static-access")
	private void setwelcome() {

		Util.soundPlay("Messages/BloodGlucose.wav");

		txtwelcom.setText(R.string.test_pleae_take_blood_glucose);
		//		AnimPlz = ObjectAnimator.ofFloat(txtWelcome, "translationY", 0f, -50f);
		//		AnimPlz.setDuration(1500);
		//		AnimPlz.start();

		//		AnimPlz.addListener(new AnimatorListenerAdapter() {
		//			public void onAnimationEnd(Animator animation) {
		//
		//				Log.e(TAG, "AnimPlz");
		//
		//				txtReading.setVisibility(View.VISIBLE);
		//				rocketImage.setVisibility(View.VISIBLE);
		//
		//			}
		//		});

	}

	private void CheckBlueToothState() {
		if (mBluetoothAdaptere == null) {

		} else {
			if (mBluetoothAdaptere.isEnabled()) {
				//if(Build.MODEL.contains("Nexus"))
				//getPairedDevicesNexus();
				//else
				mBluetoothAdaptere = BluetoothAdapter.getDefaultAdapter();
				
				BTDiscoverable();
			} else {
				try {

					mBluetoothAdaptere.enable();
					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							Log.d(TAG, "CALL #getPairedDevices()");
							if (mBluetoothAdaptere.isDiscovering()) {
								mBluetoothAdaptere.cancelDiscovery();
							}
							//if(Build.MODEL.contains("Nexus"))
							//getPairedDevicesNexus();
							//else
							mBluetoothAdaptere = BluetoothAdapter.getDefaultAdapter();
							
							BTDiscoverable();
						}
					}, 2000);
				} catch (Exception e) {
				}
			}
		}
	}

	public void BTDiscoverable() {
		Log.d(TAG, "CALL #BTDiscoverable()");
		IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
		registerReceiver(onoffReceiver, filter1);

		if (Build.VERSION.SDK_INT >= 22) {
			getPairedDevices();
		} else {
			BluetoothAdapter BA;
			BA = BluetoothAdapter.getDefaultAdapter();
			if (BA.getScanMode() !=
					BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
				Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
				turnOn.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
				startActivityForResult(turnOn, REQUEST_DISCOVERABLE_CODE);
			} else {
				getPairedDevices();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_DISCOVERABLE_CODE) {
			if (resultCode == RESULT_CANCELED) {
				//errorShow(this.getString(R.string.connectivityError));
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if (!redirectFlag) {
							Intent intentwt = new Intent(getApplicationContext(),
									GlucoseEntry.class);
							startActivity(intentwt);
							overridePendingTransition(0, 0);
							finish();
						}
					}
				}, 4000);
			} else {
				getPairedDevices();
			}
		}
	}

	private void getPairedDevices() {
		if (!conSuccess) {
			Log.d(TAG, "CALL #getPairedDevices()");
			Set<BluetoothDevice> pairedDevice = mBluetoothAdaptere.getBondedDevices();

			mMacAddress = mac_address;

			setupAndroidBluetoothConnection();
			connectMeter();
		}
	}

	private LeConnectedListener mLeConnectedListener = new LeConnectedListener() {

		@Override
		public void onConnectionStateChange_Disconnect(BluetoothGatt gatt, int status, int newState) {
			disconnectMeter();
		}

		@SuppressLint("NewApi")
		@Override
		public void onDescriptorWrite_Complete(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
			Log.d(TAG, "onDescriptorWrite_Complete" );

			// in some cases, mConnection is nullified before this is executed (MN-724)
			if (mConnection == null)
				return;

			mConnection.LeConnected(gatt.getDevice());
			mConnection.setLeConnectedListener(null);
		}

		@Override
		public void onCharacteristicChanged_Notify(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					Looper.prepare();

					try {
						mTaiDocMeter = MeterManager.detectConnectedMeter(mConnection);
						getValue();
					} catch (Exception e) {

						// this does nothing
						meterCommuHandler.sendEmptyMessage(MESSAGE_STATE_NOT_SUPPORT_METER);

						if (Build.MODEL.equals("MN-724")) {

							// disconnect and try again
							// TODO consider this fix for ALL tablets. This fix was made on Fora BLE for all tablets and has made the connection more robust
							if (mConnection != null)
								mConnection.LeDisconnect();

							// nullifying the mConnection reference makes the updatePairedList() method create a new connection.
							// this makes the connection more robust in the P724 tablet (even if the sensor disconnects, it always reconnects)
							mConnection = null;
						}
					}

					TestDeviceGlucoseTaidoc.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (mTaiDocMeter == null) {

								// this does nothing
								meterCommuHandler.sendEmptyMessage(MESSAGE_STATE_NOT_SUPPORT_METER);

								if (Build.MODEL.equals("MN-724")) {

									// disconnect and try again
									// TODO consider this fix for ALL tablets. This fix was made on Fora BLE for all tablets and has made the connection more robust
									if (mConnection != null)
										mConnection.LeDisconnect();

									// nullifying the mConnection reference makes the updatePairedList() method create a new connection.
									// this makes the connection more robust in the P724 tablet (even if the sensor disconnects, it always reconnects)
									mConnection = null;
								}
							}
						}
					});

					Looper.loop();
				}
			}).start();
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
		}
	};

	private final Handler mBTConnectionHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			try {
				switch (msg.what) {
				case PCLinkLibraryConstant.MESSAGE_STATE_CHANGE:
					if (DEBUG) {
						Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
					} /* end of if */
					switch (msg.arg1) {
					case AndroidBluetoothConnection.STATE_CONNECTED_BY_LISTEN_MODE:
						try {
							Log.d(TAG, "mBTConnectionHandler -> set mTaiDocMeter. mConnection isNull: " + (mConnection == null));
							mTaiDocMeter = MeterManager
									.detectConnectedMeter(mConnection);
						} catch (Exception e) {
							throw new NotSupportMeterException();
						}
						if (mTaiDocMeter == null) {
							throw new NotSupportMeterException();
						}/* end of if */
						break;
					case AndroidBluetoothConnection.STATE_CONNECTING:
						break;
					case AndroidBluetoothConnection.STATE_SCANED_DEVICE:
						meterCommuHandler
						.sendEmptyMessage(MESSAGE_STATE_SCANNED_DEVICE);
						break;
					case AndroidBluetoothConnection.STATE_LISTEN:
						break;
					case AndroidBluetoothConnection.STATE_NONE:
						break;
					} /* end of switch */
					break;
				case PCLinkLibraryConstant.MESSAGE_TOAST:
					break;
				default:
					break;
				} /* end of switch */

				Log.d(TAG, "mBTConnectionHandler::handleMessage -- Message: " + msg.what);

			} catch (NotSupportMeterException e) {
				Log.e(TAG, "not support meter", e);
			} /* end of try-catch */
		}
	};

	/**
	 * Android BT connection
	 */
	private AndroidBluetoothConnection mConnection;

	private final Handler meterCommuHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CONNECTING:
				break;
			case MESSAGE_STATE_SCANNED_DEVICE:
				final BluetoothDevice device = BluetoothUtil
				.getPairedDevice(mConnection
						.getConnectedDeviceAddress());
				mConnection.LeConnect(getApplicationContext(), device);
				break;
			case MESSAGE_STATE_CONNECT_DONE:
				break;
			case MESSAGE_STATE_CONNECT_FAIL:
				break;
			case MESSAGE_STATE_CONNECT_NONE:
				break;
			case MESSAGE_STATE_CONNECT_METER_SUCCESS:
				break;
			case MESSAGE_STATE_CHECK_METER_BT_DISTANCE:
				ProgressDialog baCmdDialog = new ProgressDialog(
						TestDeviceGlucoseTaidoc.this);
				baCmdDialog.setCancelable(false);
				baCmdDialog.setMessage("send ba command");
				baCmdDialog.setButton(DialogInterface.BUTTON_POSITIVE,
						"cancel", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int which) {
						// Use either finish() or return() to either
						// close the activity
						// or just
						// the dialog
						dialog.dismiss();
						return;
					}
				});
				baCmdDialog.show();
				break;
			case MESSAGE_STATE_CHECK_METER_BT_DISTANCE_FAIL:
				break;
			case MESSAGE_STATE_NOT_SUPPORT_METER:
				if(Build.MODEL.contains("Nexus")){
					if (mConnection != null) {
						disconnectMeter();
					}
				}
				break;
			case MESSAGE_STATE_NOT_CONNECT_SERIAL_PORT:
				break;
			} /* end of switch */

			Log.d(TAG, "meterCommuHandler::handleMessage -- Message: " + msg.what);
		}
	};

	private AbstractMeter mTaiDocMeter = null;

	private void updatePairedList() {
		Map<String, String> addrs = new HashMap<String, String>();
		String addrKey = "BLE_PAIRED_METER_ADDR_" + String.valueOf(0);
		addrs.put(addrKey, mac_address);
		if (mConnection == null) {
			mConnection = ConnectionManager.createAndroidBluetoothConnection(mBTConnectionHandler);
		}
		mConnection.updatePairedList(addrs, 1);
	}

	/**
	 * Connect Meter
	 */
	private void connectMeter() {
		Log.d(TAG, "#IN connectMeter()");

		new Thread(new Runnable() {
			@Override
			public void run() {
				Looper.prepare();
				try {
					meterCommuHandler
					.sendEmptyMessage(MESSAGE_STATE_CONNECTING);

					if (mBLEMode) {
						Log.d(TAG, "connectMeter -> updatePairedList");
						updatePairedList();
						mConnection
						.setLeConnectedListener(mLeConnectedListener);

						if (mConnection.getState() == AndroidBluetoothConnection.STATE_NONE) {
							Log.d(TAG, "connectMeter -> setLeConnectedListener");
							mConnection.setLeConnectedListener(mLeConnectedListener);
							// Start the Android Bluetooth connection
							// services to listen mode
							Log.d(TAG, "connectMeter -> LeListen!!");
							mConnection.LeListen();

							if (DEBUG) {
								Log.i(TAG, "into listen mode");
							}
						}
					} else {
						// Only if the state is STATE_NONE, do we know
						// that we haven't started
						// already
						if (mConnection.getState() == AndroidBluetoothConnection.STATE_NONE) {
							// Start the Android Bluetooth connection
							// services to listen mode
							mConnection.listen();

							if (DEBUG) {
								Log.i(TAG, "into listen mode");
							}
						}
					}

				} catch (CommunicationTimeoutException e) {
					Log.e(TAG, e.getMessage(), e);
					meterCommuHandler
					.sendEmptyMessage(MESSAGE_STATE_CONNECT_FAIL);
				} catch (NotSupportMeterException e) {
					Log.e(TAG, "not support meter", e);
					meterCommuHandler
					.sendEmptyMessage(MESSAGE_STATE_NOT_SUPPORT_METER);
				} catch (NotConnectSerialPortException e) {
					meterCommuHandler
					.sendEmptyMessage(MESSAGE_STATE_NOT_CONNECT_SERIAL_PORT);
				} catch (ExceedRetryTimesException e) {

					meterCommuHandler
					.sendEmptyMessage(MESSAGE_STATE_NOT_SUPPORT_METER);

				} finally {
					meterCommuHandler
					.sendEmptyMessage(MESSAGE_STATE_CONNECT_DONE);
					if(Build.MODEL.contains("Nexus")){

						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								if (!redirectFlag) {
									if (!conSuccess) {
										Log.d(TAG, "connectMeter -> getValue (handler). mConnection isNull: " + (mConnection == null));
										getValue();
									}
								}
							}
						}, 5000);
					}else{
						if (!fetcherFlag) {
							Log.d(TAG, "connectMeter -> getValue. mConnection isNull: " + (mConnection == null));
							fetcherFlag = true;
							getValue();
						} else {
							new Handler().postDelayed(new Runnable() {
								@Override
								public void run() {
									if (!redirectFlag) {
										Log.d(TAG, "connectMeter -> getValue (handler). mConnection isNull: " + (mConnection == null));
										getValue();
									}
								}
							}, 5000);
						}
					}
				}
				Looper.loop();
			}
		}).start();
	}

	private void setupAndroidBluetoothConnection() {
		Log.d(TAG, "#IN setupAndroidBluetoothConnection()");
		if (mConnection == null) {
			Log.d(TAG, "setupAndroidBluetoothConnection()");
			try {
				mConnection = ConnectionManager
						.createAndroidBluetoothConnection(mBTConnectionHandler);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}

		} /* end of if */
	}

	@SuppressLint("SimpleDateFormat")
	public void getValue() {
		// Get Meter Storage Count
		try {
			if (mTaiDocMeter == null) {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {

						if (!redirectFlag) {
							Log.d(TAG, "getValue -> connectMeter (handler). mTaiDocMeter is NULL mConnection isNull: " + (mConnection == null));

							setupAndroidBluetoothConnection();
							connectMeter();
						}

					}
				}, 10000);
			} else {
				Log.e(TAG, "TaiDocMeter reference is NOT NULL");

				int storageCount = mTaiDocMeter.getStorageNumberAndNewestIndex(
						User.CurrentUser).getStorageNumber();
				Log.e(TAG, "Number of Records = " + storageCount);

				SharedPreferences pref = getSharedPreferences(STORAGE_COUNT,
						MODE_PRIVATE);
				Editor editors = pref.edit();
				int Shared_Count = pref.getInt("SC", 0);
				Log.e(TAG, "Shared Count = " + storageCount);
				if (storageCount > 0 && storageCount > Shared_Count) {
					conSuccess = true;
					// Import all medical record in meter
					int array_length = storageCount - Shared_Count;
					String[] value = new String[array_length];
					String[] mDate = new String[array_length];
					String[] type = new String[array_length];
					int count = 0;
					List<AbstractRecord> recordList = new ArrayList<AbstractRecord>();

					for (int i = 0; i < array_length; i++) {
						Log.e(TAG, "=========i=======: " + i);
						AbstractRecord record = mTaiDocMeter
								.getStorageDataRecord(i, User.CurrentUser);
						recordList.add(record);
					} /* end of for */
					// Convert the List<AbstractRecord> to Java object
					for (AbstractRecord record : recordList) {
						if (record instanceof BloodGlucoseRecord) { // BG
							// record
							SimpleDateFormat formatterDate1 = new SimpleDateFormat(
									"MM/dd/yyyy hh:mm:ss aa");
							String measurementDate = formatterDate1
									.format(((BloodGlucoseRecord) record)
											.getMeasureTime());
							int bgValue = ((BloodGlucoseRecord) record)
									.getGlucoseValue();
							String bgType = ((BloodGlucoseRecord) record)
									.getType().toString();
							Log.e("VALUES", "MEASUREMENT DATE: "
									+ measurementDate + "VALUE: " + bgValue
									+ "TYPE: " + bgType);

							if (bgType.equals("General")) {
								bgType = "3";
							} else if (bgType.equals("PC")) {
								bgType = "2";
							} else if (bgType.equals("AC")) {
								bgType = "1";
							} else if (bgType.equals("QC")) {
								bgType = "7";
							} else if (bgType.equals("")) {
								bgType = "3";
							}

							if (count == 0) {
								value[count] = bgValue + "";
								mDate[count] = measurementDate;
								type[count] = bgType;
								result = value[count];
								glucose_valuew(value[count], mDate[count],
										type[count]);



								count += 1;
							} else {
								value[count] = bgValue + "";
								mDate[count] = measurementDate;
								type[count] = bgType;
								glucose_old_value(value[count], mDate[count],
										type[count]);
								count += 1;
							}
						} /* end of if */
					} /* end of for */


					try {
						// mTaiDocMeter.clearMeasureRecords(User.CurrentUser);
						/*
						 * CLEAR DATA
						 */
						editors.putInt("SC", storageCount);
						editors.commit();
						mTaiDocMeter.turnOffMeterOrBluetooth(0);
						if (mConnection != null) {
							disconnectMeter();
						}
					} catch (Exception e) {
						Log.e(TAG, e.getMessage(), e);
					}

					Log.i("result value", "result value: "+result);
					runOnUiThread(new Runnable() {
						@Override
						public void run() {

							showResult(result);

						}
					});


				} else {
					try {
						mTaiDocMeter.turnOffMeterOrBluetooth(0);
						if (mConnection != null) {
							disconnectMeter();
						}
						if (mTaiDocMeter != null) {
							mTaiDocMeter = null;
						}
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								Log.d(TAG, "getPairedDevices -> getPairedDevices (handler). mConnection isNull: " + (mConnection == null));

								is_paired = false;
								conSuccess = false;
								getPairedDevices();
							}
						}, 5000);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void glucose_valuew(String data, String measure_date,
			String Prompt_flag) {

		Glucose_db dbcreateglucose = new Glucose_db(TestDeviceGlucoseTaidoc.this);
		ClassMeasurement glucose = new ClassMeasurement();
		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = flow.getInt("flow", 0); // #1
		if (val == 1) {

			SharedPreferences section = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);
			SharedPreferences.Editor editor_retake = section.edit();
			editor_retake.putString("sectiondate",
					Util.get_patient_time_zone_time(this));
			editor_retake.commit();
		}
		glucose.setLast_Update_Date((measure_date.replace("p.m.", "PM"))
				.replace("a.m.", "AM"));
		glucose.setStatus(0);
		glucose.setGlucose_Value(Integer.parseInt(data.toString()));
		glucose.setInputmode(0);
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.PREFS_NAME_date, 0);
		String sectiondate = settings1.getString("sectiondate", "0");

		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.USER_TIMESLOT_SP, 0);
		String slot = settings.getString("timeslot", "AM");

		glucose.setSectionDate(sectiondate);
		glucose.setTimeslot(slot);
		Log.e(TAG, "Prompt_flag: " + Prompt_flag);
		glucose.setPrompt_flag(Prompt_flag);

		Log.e(TAG, "Saving Glucose value to db from bluetooth device");

		//glucoseId = dbcreateglucose.InsertGlucoseMeasurement_TD(glucose);

	}

	private void glucose_old_value(String data, String measure_date,
			String Prompt_flag) {
		Glucose_db dbcreateglucose = new Glucose_db(TestDeviceGlucoseTaidoc.this);
		ClassMeasurement glucose = new ClassMeasurement();

		glucose.setLast_Update_Date((measure_date.replace("p.m.", "PM"))
				.replace("a.m.", "AM"));
		glucose.setStatus(0);
		glucose.setGlucose_Value(Integer.parseInt(data.toString()));
		glucose.setInputmode(0);
		glucose.setSectionDate(Util.get_patient_time_zone_time(this));
		Log.e(TAG, "Prompt_flag: " + Prompt_flag);
		glucose.setPrompt_flag(Prompt_flag);
		Log.e(TAG, "Saving Glucose value to db from bluetooth device");

		//		int glucose_Id = dbcreateglucose.InsertGlucoseMeasurement(glucose);
		//		dbcreateglucose.UpdateglucoseData_as_valid(glucose_Id);
	}

	@Override
	protected void onStop() {
		Log.d(TAG, "onStop: " + this.hashCode());
		try {
			redirectFlag = true;

			if (mConnection != null) {
				disconnectMeter();
			}
			if (mScanning) {
				mScanning = false;
			}
			if (onoffReceiver != null)
				unregisterReceiver(onoffReceiver);

		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onStop();
	}


	private String getForaMAC(String mac) {

		String macAddress = "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length(); i = i + 2) {
			// macAddress.substring(i, i+2)
			sb.append(mac.substring(i, i + 2));
			sb.append(":");
		}

		macAddress = sb.toString();
		macAddress = macAddress.substring(0, macAddress.length() - 1);

		Log.i(TAG, " : Connect to macAddress " + macAddress);
		// #
		if (macAddress.trim().length() == 17) { // 00:1C:05:00:40:64
			return macAddress;
		} else {
			return "";
		}
	}

	private void disconnectMeter() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Looper.prepare();
				try {
					mConnection.setLeConnectedListener(null);
					mConnection.LeDisconnect();
					mConnection = null;
				} catch (Exception e) {
					Log.e(TAG, e.getMessage(), e);
				} finally {
				}/* end of try-catch-finally */
				Looper.loop();
			}
		}).start();
	}



	//	@Override
	//	protected void onResume() {
	//		redirectFlag = false;
	//		is_paired = false;
	//		mConnection = null;
	//		try {
	//			if (intFilter == null) {
	//				intFilter = new IntentFilter();
	//				intFilter.addAction(BluetoothDevice.ACTION_FOUND);
	//				registerReceiver(ActionFoundReceiver, intFilter);
	//			}
	//
	//			
	//
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//		super.onResume();
	//	}

	@Override
	protected void onPause() {
		//		try {
		//			redirectFlag = true;
		//			if (mConnection != null) {
		//				disconnectMeter();
		//			}
		//			if (onoffReceiver != null) {
		//				unregisterReceiver(onoffReceiver);
		//			}
		//		} catch (Exception e) {
		//			e.printStackTrace();
		//		}
		super.onPause();
	}

	private final BroadcastReceiver onoffReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();

			if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
				final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
						BluetoothAdapter.ERROR);
				switch (state) {
				case BluetoothAdapter.STATE_OFF:
					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							try {
								if (!mBluetoothAdaptere.isEnabled()) {
									CheckBlueToothState();
								}
							} catch (Exception e) {

							}
						}
					}, 5000);
					break;
				}
			}
		}
	};

	private void pairADevice(int pos,String testFlag){
		//		txtWelcome.setVisibility(View.GONE);
		//		txtReading.setText(getResources().getString(R.string.pairingDevice));
		globalClass.populateSpinner(spnVitals,pos,TestDeviceGlucoseTaidoc.this,testFlag);
	}
	private void startTesting(){
		conSuccess = false;
		layout_readingSection.setVisibility(View.VISIBLE);
		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();		

		backgroundThread = new BackgroundThread();
		backgroundThread.setRunning(true);
		backgroundThread.start();


		mac_address = getForaMAC(dbSensor.Select_GlucoName_adapter_mac());
		if (mac_address.length() != 17) {
			mac_address = "00:80:25:C3:05:48"; // set default MAC
		}
		SharedPreferences pref = getSharedPreferences(
				STORAGE_COUNT, MODE_PRIVATE);
		Editor editors = pref.edit();
		String Shared_Mac = pref.getString("Shared_Mac", "-1");
		if (!Shared_Mac.equals(mac_address)) {
			Log.i(TAG, "TD DEVICE CHANGED");
			editors.putInt("SC", 0);
			editors.putString("Shared_Mac", mac_address);
			editors.commit();
		}		

		redirectFlag = false;
		is_paired = false;
		mConnection = null;
		try {
			mBluetoothAdaptere = BluetoothAdapter.getDefaultAdapter();
			CheckBlueToothState();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showResult(final String value){
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {

				playSound(value);

			}
		}, 1000);
		SharedPreferences contract_cod = getSharedPreferences(CommonUtilities.NION_SP, 0);
		SharedPreferences.Editor contract_editor = contract_cod.edit();
		contract_editor.putString("taidoc_paired", "pair");
		contract_editor.commit();
		txtReading.setVisibility(View.INVISIBLE);
		rocketImage.setVisibility(View.INVISIBLE);
		txtwelcom.setText(getResources().getString(R.string.yourbloodsugarlevel) +" "

				+ value + getResources().getString(R.string.bloodsugarunit));
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {


			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	private void playSound(String value) {

		NumberToString ns = new NumberToString();		
		String no_to_string = "";
		if (value.contains(".")) {
			Double final_value = Double.parseDouble(value);
			no_to_string = ns.getNumberToString(final_value);
		} else {
			Long final_value = Long.parseLong(value);
			no_to_string = ns.getNumberToStringLong(final_value);
		}

		ArrayList<String> final_list = new ArrayList<String>();
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){

			final_list.add("Messages/BLsugarLevelsat_1.wav");
		}else{

			final_list.add("Messages/BLsugarLevelsat.wav");
		}

		List<String> sellItems = Arrays.asList(no_to_string.split(" "));

		for (String item : sellItems) {
			if (item.toString().length() > 0) {
				final_list.add("Numbers/" + item.toString() + ".wav");
			}
		}

		if (final_list.get(final_list.size() - 1).toString()
				.equals("Numbers/zero.wav")) // remove if last item is zero
		{
			final_list.remove(final_list.size() - 1);
		}

		final_list.add("Messages/MllgmPerDecltr.wav");

		Util.playSound4FileList(final_list, getApplicationContext());
	}

}