package com.tkm.optumSierra;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.text.Html;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tkm.optumSierra.bean.ClasssPressure;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.service.OmronBleService;
import com.tkm.optumSierra.service.OmronBleService.MyServiceLocalBinder;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.NumberToString;
import com.tkm.optumSierra.util.OmronPinReceiver;
import com.tkm.optumSierra.util.Util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TestDeviceOmronBlsActivity extends Titlewindow {
	private String resultValue = "";
	private String macAddress = "";
	private final static String TAG = "TestDeviceOmron";
	Pressure_db dbcreatepressure = new Pressure_db(this);
	protected final static String LOG_TAG = "BLE_LOG";
	private String connectionStatus = "Connect";
	int flag = 0;
	private String bodyMovement = "";
	private String irregularPulse = "";
	private String batteryLevel = "";
	private String positionFlag = "1";
	private String cuffFitFlag = "1";
	BluetoothAdapter bluetoothAdapter;
	private static final int REQUEST_ENABLE_BTE = 1;
	private Date sensorCurrentTime;
	private Date sensorReadingTime;
	private Date tabletRecievedTime;
	private Date tabletPageOpenTime;

	protected UUID[] mScanServiceUuids = null;
	private final OmronPinReceiver mybroadcast = new OmronPinReceiver();
	protected static final int MSG_CONNECTING = 0;
	protected static final int MSG_CONNECTED = 1;
	protected static final int MSG_DISCONNECTED = 2;
	protected static final int MSG_WM_DATA_RECV = 3;
	protected static final int MSG_ADV_CATCH_DEV = 4;
	protected static final int MSG_SCAN_CANCEL = 5;
	protected static final int MSG_BATTERY_DATA_RECV = 6;
	protected static final int MSG_CTS_DATA_RECV = 7;
	protected static final int MSG_BPM_DATA_RECV = 8;
	protected static final int MSG_BPF_DATA_RECV = 9;
	protected static final int MSG_LISTVIEW_CLR = 10;
	protected static final int MSG_WSF_DATA_RECV = 11;
	protected static final int MSG_BLE_CONNECT_STATE_RECV = 101;

	protected static final int BLE_STATE_IDLE = 0;
	protected static final int BLE_STATE_SCANNING = 1;
	protected static final int BLE_STATE_CONNECTING = 2;
	protected static final int BLE_STATE_CONNECT = 3;
	protected static final int BLE_STATE_DATA_RECV = 4;
	protected int mBleState = BLE_STATE_IDLE;
	protected OmronBleService mBleService;

	private GlobalClass appClass;
	TextView text;
	Button btnClose;
	TextView txtwelcom, txtReading;
	ImageView rocketImage;
	AnimationDrawable rocketAnimation;
	ObjectAnimator AnimPlz, ObjectAnimator;
	private Button btn_beginTest;
	private Spinner spnVitals;
	private LinearLayout layout_readingSection;
	boolean isBroadcastRegisterd = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); // execute at the end

		setContentView(R.layout.activity_test_device);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		Log.d(TAG, "[IN]onCreate");
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		tabletPageOpenTime = parseTimeToMilliseconds(mdformat.format(calendar.getTime()));
		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);
		rocketImage = (ImageView) findViewById(R.id.loading);
		btnClose = (Button) findViewById(R.id.testDeviceClose);
		btn_beginTest = (Button) findViewById(R.id.btn_beginTest);
		spnVitals = (Spinner)findViewById(R.id.spn_assignedVitals);
		layout_readingSection = (LinearLayout)findViewById(R.id.layout_readingSection);
		layout_readingSection.setVisibility(View.INVISIBLE);
		Bundle extras = getIntent().getExtras();
		isBroadcastRegisterd = false;
		if(extras != null){
			appClass.setBundle(extras);
		}else{
			extras = appClass.getBundle();
		}
		int pos = extras.getInt("Position", -1);
		if(extras.getString("TestFlag").equals("pair")){

			pairADevice(pos,"pair");
		}else if(extras.getString("TestFlag").equals("test"))	{

			pairADevice(pos,"test");
		}else if(extras.getString("TestFlag").equals("testAll"))	{

			pairADevice(pos,"testAll");
		}
		macAddress = extras.getString("macaddress");
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		btnClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click
				Intent intent = new Intent(getApplicationContext(), FinalMainActivity.class);
				finish();
				startActivity(intent);
			}
		});
		btn_beginTest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click
				btn_beginTest.setEnabled(false);
				btn_beginTest.setBackgroundColor(Color.parseColor("#a0a0a0"));

				startTesting();
			}
		});

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");

		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);
		final Handler handler = new Handler();

//		mybroadcast.setOnPinEnteredSuccessfully(new OmronPinReceiver.OnPinEnteredSuccessfully(){
//			@Override
//            public void pinEnteredSuccessfully() {
//                Log.d(TAG, "pinEnteredSuccessfully: ");
//                connectionStatus = "Connect";
//                startPairing();
//            }
//		});
//
//		mybroadcast.setOnPinEnteredSuccessfully(new OmronPinReceiver.OnPinEnteredSuccessfully(){
//			@Override
//			public void pinEnteredSuccessfully() {
//				Log.d(TAG, "pinEnteredSuccessfully: ");
//				connectionStatus = "Connect";
//				startPairing();
//			}
//		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "[IN]onResume");
		resultValue = "";
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "[IN]onPause");
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "[IN]onStop");

		//mybroadcast.setOnPinEnteredSuccessfully(null);

		try{
			unregisterReceiver(mBlutoothStateReceiver);
			unregisterReceiver(mybroadcast);

		} catch(Exception ex) {
			Log.e(TAG, ex.getMessage());
			ex.printStackTrace();
		}

		try {
			unbindService(mConnection);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "[IN]onDestroy");
	}

	protected void onReceiveMessage(Message msg) {

		byte[] data;
		byte[] buf = new byte[2];
		ByteBuffer byteBuffer;

		switch (msg.what){
			case MSG_CONNECTING:
				break;

			case MSG_CONNECTED:
				Log.d(TAG, "[LOG]MSG_CONNECTED");
				connectionStatus = "Disconnect";
				break;

			case MSG_DISCONNECTED:
				Log.d(TAG, "[LOG]MSG_DISCONNECTED");
				connectionStatus = "Connect";
				break;

			case MSG_ADV_CATCH_DEV:
				Log.d(TAG, "[LOG]MSG_ADV_CATCH_DEV");
				ListViewDisp((BluetoothDevice)msg.obj);
				break;

			case MSG_BATTERY_DATA_RECV:
				connectionStatus = "Disconnect";
				byte[] batterydata = (byte[])msg.obj;
				int bl = batterydata[0];
				batteryLevel =  ""+bl;
				break;

			case MSG_CTS_DATA_RECV:
				//mConnectbtn.setText(R.string.disconnect);
				connectionStatus = "Disconnect";
				byte[] ctsdata = (byte[])msg.obj;
				byte[] buf1 = new byte[2];
				System.arraycopy(ctsdata, 0, buf1, 0, 2);
				ByteBuffer ctsyearbyteBuffer = ByteBuffer.wrap(buf1);
				ctsyearbyteBuffer.order(ByteOrder.LITTLE_ENDIAN);

				int	 ctsYear	  = ctsyearbyteBuffer.getShort();
				int	 ctsMonth	  = ctsdata[2];
				int	 ctsDay		  = ctsdata[3];
				int	 ctsHour	  = ctsdata[4];
				int	 ctsMinute	  = ctsdata[5];
				int	 ctsSecond	  = ctsdata[6];
				byte AdjustReason = ctsdata[9];

				String ctsTime =
						String.format("%1$04d", ctsYear) + "-" + String.format("%1$02d", ctsMonth)	+ "-" + String.format("%1$02d", ctsDay) + " " + String.format("%1$02d", ctsHour) + ":" + String.format("%1$02d", ctsMinute) + ":" + String.format("%1$02d", ctsSecond);

				sensorCurrentTime = parseTimeToMilliseconds(ctsTime);
				Log.i(LOG_TAG, "[LOG_OUT]CTS Data:"+ctsTime+" (AdjustReason:"+AdjustReason+")");
				AddReadingAndWaitForTime(null,null);
				break;

			case MSG_LISTVIEW_CLR:
				Log.d(TAG, "[LOG]MSG_LISTVIEW_CLR");
				break;

			case MSG_BLE_CONNECT_STATE_RECV:
				if (!BLEIsEnabled()) {
					break;
				}
				int state_code = Integer.parseInt(msg.obj.toString());
				switch(state_code) {
					case OmronBleService.ACL_STATE_CONNECTED:
						Log.i(TAG, "ACL_STATE_CONNECTED");
						break;
					case OmronBleService.ACL_STATE_DISCONNECTED:
						Log.i(TAG, "ACL_STATE_DISCONNECTED");
						break;
					case OmronBleService.GATT_STATE_CONNECTED:
						Log.i(TAG, "GATT_STATE_CONNECTED");
						break;
					case OmronBleService.GATT_STATE_DISCONNECTED:
						if(resultValue.equals("")){
							final Handler handler = new Handler();
							handler.postDelayed(new Runnable() {
								@Override
								public void run() {
									startPairing();
								}
							}, 6000);
						}
						Log.i(TAG, "GATT_STATE_DISCONNECTED");
						break;
					default:
						break;
				}
				break;


			case MSG_BPM_DATA_RECV:
				connectionStatus = "Disconnect";
				int idx = 0;
				data = (byte[])msg.obj;

				byte flags = data[idx++];

				// 0: mmHg	1: kPa
				boolean kPa = (flags & 0x01) > 0;
				// 0: No Timestamp info 1: With Timestamp info
				boolean timestampFlag = (flags & 0x02) > 0;
				// 0: No PlseRate info 1: With PulseRate info
				boolean pulseRateFlag = (flags & 0x04) > 0;
				// 0: No UserID info 1: With UserID info
				boolean userIdFlag = (flags & 0x08) > 0;
				// 0: No MeasurementStatus info 1: With MeasurementStatus info
				boolean measurementStatusFlag = (flags & 0x10) > 0;

				// Set BloodPressureMeasurement unit
				String unit;
				if (kPa) {
					unit = "kPa";
				} else {
					unit = "mmHg";
				}

				// Parse Blood Pressure Measurement
				short systolicVal = 0;
				short diastolicVal = 0;
				short meanApVal = 0;

				System.arraycopy(data, idx, buf, 0, 2);
				idx += 2;
				byteBuffer = ByteBuffer.wrap(buf);
				byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
				systolicVal = byteBuffer.getShort();

				System.arraycopy(data, idx, buf, 0, 2);
				idx += 2;
				byteBuffer = ByteBuffer.wrap(buf);
				byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
				diastolicVal = byteBuffer.getShort();

				System.arraycopy(data, idx, buf, 0, 2);
				idx += 2;
				byteBuffer = ByteBuffer.wrap(buf);
				byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
				meanApVal = byteBuffer.getShort();

				Log.i(LOG_TAG, "[LOG_OUT]systolicValue:" +systolicVal +" "+unit);
				Log.i(LOG_TAG, "[LOG_OUT]diastolicValue:"+diastolicVal+" "+unit);
				Log.i(LOG_TAG, "[LOG_OUT]meanApValue:"	 +meanApVal	  +" "+unit);

				// Parse Timestamp
				String timestampStr = "----";
				String dateStr = "--";
				String timeStr = "--";
				if (timestampFlag) {
					System.arraycopy(data, idx, buf, 0, 2);
					idx += 2;
					byteBuffer = ByteBuffer.wrap(buf);
					byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

					int year  = byteBuffer.getShort();
					int month = data[idx++];
					int day	  = data[idx++];
					int hour  = data[idx++];
					int min	  = data[idx++];
					int sec	  = data[idx++];

					dateStr = String.format("%1$04d", year) + "-" + String.format("%1$02d", month) + "-" + String.format("%1$02d", day);
					timeStr = String.format("%1$02d", hour) + ":" + String.format("%1$02d", min)   + ":" + String.format("%1$02d", sec);
					timestampStr = dateStr + " " + timeStr;
					sensorReadingTime = parseTimeToMilliseconds(timestampStr);
					//Log.i(LOG_TAG, "[LOG_OUT]Timestamp Data:" + timestampStr);
				}

				// Parse PulseRate
				short  pulseRateVal = 0;
				String pulseRateStr = "----";
				if (pulseRateFlag) {
					System.arraycopy(data, idx, buf, 0, 2);
					idx += 2;
					byteBuffer = ByteBuffer.wrap(buf);
					byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
					pulseRateVal = byteBuffer.getShort();
					pulseRateStr = Short.toString(pulseRateVal);
					Log.i(LOG_TAG, "[LOG_OUT]PulseRate Data:"+pulseRateStr);
				}

				// Parse UserID
				int	   userIDVal = 0;

				// Parse Measurement Status
				int	   measurementStatusVal = 0;
				String measurementStatusStr = "----";
				if (measurementStatusFlag) {
					System.arraycopy(data, idx, buf, 0, 2);
					idx += 2;
					byteBuffer = ByteBuffer.wrap(buf);
					byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
					measurementStatusVal = byteBuffer.getShort();
					measurementStatusStr = String.format("%1$04x", (short)measurementStatusVal);
					Log.i(LOG_TAG, "[LOG_OUT]MeasurementStatus Data:"+measurementStatusStr);

					bodyMovement = ((measurementStatusVal & 0x0001) == 0 ? "0" : "1");
					irregularPulse = ((measurementStatusVal & 0x0004) == 0 ? "0" : "1");
				}

				// Output to History
				Log.d(TAG, "Add history");
				String entry = timestampStr + "," + systolicVal + "," + diastolicVal + "," + meanApVal + "," + pulseRateStr + "," + String.format("%1$02x", flags) + "," + measurementStatusStr;

				// Output log for data aggregation
				// Log format: ## For aggregation ## timestamp(date), timestamp(time), systolic, diastolic, meanAP, current date time
				Calendar c = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String agg = "## For aggregation ## ";
				agg += dateStr + "," + timeStr;
				agg += "," + systolicVal + "," + diastolicVal + "," + meanApVal;
				agg += "," + sdf.format(c.getTime());
				//Log.i(TAG, agg);
				String recievedData = systolicVal + "," + diastolicVal + "," +
						pulseRateStr + "," + timestampStr;
				resultValue = recievedData;
				if(timestampStr.contains("0000")){

					sensorReadingTime = parseTimeToMilliseconds(sdf.format(c.getTime()));
				}
				tabletRecievedTime = parseTimeToMilliseconds(sdf.format(c.getTime()));
				AddReadingAndWaitForTime(recievedData,sensorReadingTime);

				break;

			case MSG_BPF_DATA_RECV:
				connectionStatus = "Disconnect";
				data = (byte[])msg.obj;
				System.arraycopy(data, 0, buf, 0, 2);
				ByteBuffer bpfByteBuffer = ByteBuffer.wrap(buf);
				bpfByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
				short  bpfVal = bpfByteBuffer.getShort();
				String bpfStr = String.format("%1$04x", (short)bpfVal);
				Log.i(LOG_TAG, "[LOG_OUT]Blood Pressure Feature Data:"+bpfStr);
				break;

			default:
				break;
		}
	}

	protected void onBleServiceConnected(IBinder service) {
		Log.d(TAG, "[IN]onBleServiceConnected");
		Log.d(TAG, "[IN]onBleReceiveMessage");
		mBleService = ((MyServiceLocalBinder)service).getService();
		mBleService.setCurrentContext(getApplicationContext(), (IBleListener) mBinder);
	}

	protected void onClickAssistPairingCheckBox(boolean checked) {
		mBleService.setAssistPairing(checked);

		SharedPreferences pref = getSharedPreferences("Setting", MODE_PRIVATE);
		SharedPreferences.Editor edit = pref.edit();
		edit.putBoolean("BlsAssistPairingCheckBox", checked);
		edit.commit();
	}

	protected void onClickWriteCtsCheckBox(boolean checked) {
		// implement in child class
		Log.i(TAG, "mCheckBoxClickListener:"+checked);
		mBleService.setWriteCts(checked);
	}

	@SuppressLint("NewApi")
	public boolean BLEIsEnabled() {
		// Confirm that Bluetooth is working on this device.
		final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		if (bluetoothManager != null) {
			BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
			if (bluetoothAdapter != null) {
				if (bluetoothAdapter.isEnabled()) {
					return true;
				}
			}
		}
		return false;
	}

	@SuppressLint("NewApi")
	private void ListViewDisp(BluetoothDevice dev) {
		String macId = "";
		String servMac = "";
		if(dev.getAddress().contains(":")){

			macId = dev.getAddress().replace(":", "");
		}else{

			macId = dev.getAddress();
		}
		if(macAddress.contains(":")){

			servMac = macAddress.replace(":", "");
		}else{

			servMac = macAddress;
		}

		if(macId.equals(servMac)){
			Log.i(TAG, "MACID is same.");
			if (Build.MODEL.equals("VK815"))			
				dev.createBond();
			onClickWriteCtsCheckBox(true);
			onClickAssistPairingCheckBox(false);
			mBleService.BleScanOff();
			Message msg = new Message();
			msg.what=MSG_LISTVIEW_CLR;
			mHandler.sendMessageDelayed(msg, 300);		// Wait for preventing to display received ADVs on the list before finishing scan Off process
			mBleService.BleConnectDev(dev);
		}

	}

	private final IBleListener.Stub mBinder = new IBleListener.Stub() {
		public void BleAdvCatch() throws RemoteException{
			Log.d(TAG, "[IN]BleAdvCatch");
			mBleState = BLE_STATE_CONNECTING;
			Message msg = new Message();
			msg.what=MSG_CONNECTING;
			mHandler.sendMessage(msg);
		}

		public void BleAdvCatchDevice(BluetoothDevice dev) throws RemoteException {
			Log.d(TAG, "[IN]BleAdvCatchDevice");
			Message msg = new Message();
			msg.what=MSG_ADV_CATCH_DEV;
			msg.obj = dev;
			mHandler.sendMessage(msg);
		}

		public void BleConnected() throws RemoteException {
			Log.d(TAG, "[IN]BleConnected");
			mBleState = BLE_STATE_CONNECT;
			Message msg = new Message();
			msg.what=MSG_CONNECTED;
			mHandler.sendMessage(msg);
		}

		public void BleDisConnected() throws RemoteException {
			Log.d(TAG, "[IN]BleDisConnected");
			mBleState = BLE_STATE_IDLE;
			Message msg = new Message();
			msg.what=MSG_DISCONNECTED;
			mHandler.sendMessage(msg);
		}

		public void BleWmDataRecv(byte[] data) throws RemoteException {
			Log.d(TAG, "[IN]BleDataRecv");
			Message msg = new Message();
			msg.what=MSG_WM_DATA_RECV;
			msg.obj=data;
			mHandler.sendMessage(msg);
		}

		public void BleBatteryDataRecv(byte[] data) throws RemoteException {
			Log.d(TAG, "[IN]BleBatteryDataRecv");
			Message msg = new Message();
			msg.what=MSG_BATTERY_DATA_RECV;
			msg.obj=data;
			mHandler.sendMessage(msg);
		}

		public void BleCtsDataRecv(byte[] data) throws RemoteException {
			Log.d(TAG, "[IN]BleCtsDataRecv");
			Message msg = new Message();
			msg.what=MSG_CTS_DATA_RECV;
			msg.obj=data;
			mHandler.sendMessage(msg);
		}

		public void BleBpmDataRecv(byte[] data) throws RemoteException {
			Log.d(TAG, "[IN]BleBpmDataRecv");
			Message msg = new Message();
			msg.what=MSG_BPM_DATA_RECV;
			msg.obj=data;
			mHandler.sendMessage(msg);
		}

		public void BleBpfDataRecv(byte[] data) throws RemoteException {
			Log.d(TAG, "[IN]BleBpfDataRecv");
			Message msg = new Message();
			msg.what=MSG_BPF_DATA_RECV;
			msg.obj=data;
			mHandler.sendMessage(msg);
		}

		public void BleWsfDataRecv(byte[] data) throws RemoteException {
			Log.d(TAG, "[IN]BleWsfDataRecv");
			Message msg = new Message();
			msg.what=MSG_WSF_DATA_RECV;
			msg.obj=data;
			mHandler.sendMessage(msg);
		}

		public void BleConnectionStateRecv(int state_code) throws RemoteException {
			Log.i(TAG, "[IN]BleConnectionStateRecv");
			Message msg = new Message();
			msg.what=MSG_BLE_CONNECT_STATE_RECV;
			msg.obj=state_code;
			mHandler.sendMessage(msg);
		}
	};

	// Event handler
	protected Handler mHandler = new Handler() {
		public void handleMessage(Message msg){
			onReceiveMessage(msg);
		}
	};

	protected ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			Log.d(TAG, "[IN]onServiceConnected");
			onBleServiceConnected(service);
		}

		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "[IN]onServiceDisconnected");
			mBleService = null;
		}
	};

	private void startPairing(){
		if (connectionStatus.equals("Connect")) {
			if (!BLEIsEnabled()){

				return;
			}

			// Scan BLE devices
			mBleService.BleScan(mScanServiceUuids);

			connectionStatus = "Connecting";
			Log.i(TAG, "[IN]CONNECT --> CONNECTING...");

		} else if (connectionStatus.equals("Connecting")){
			Log.i(TAG, "[IN]CONNECTING... --> CONNECT");

		} else if (connectionStatus.equals("Disconnect")) {
			mBleService.BleDisconnect();
			Log.i(TAG, "[IN]DISCONNECT --> CONNECT");
		}

	}

	private void save(String result) {
		flag = 1;

		try {
			SharedPreferences flow = getSharedPreferences(
					CommonUtilities.USER_FLOW_SP, 0);
			int val = flow.getInt("flow", 0); // #1
			if (val == 1) {
				SharedPreferences section = getSharedPreferences(
						CommonUtilities.PREFS_NAME_date, 0);
				SharedPreferences.Editor editor_retake = section.edit();
				editor_retake.putString("sectiondate", Util.get_patient_time_zone_time(this));
				editor_retake.commit();
			}

			SharedPreferences settings1 = getSharedPreferences(CommonUtilities.USER_SP, 0);
			int PatientIdDroid = Integer.parseInt(settings1.getString("patient_id", "-1"));
			String[] str = null;
			str = result.split(",");

			SharedPreferences settings = getSharedPreferences(
					CommonUtilities.USER_TIMESLOT_SP, 0);
			String slot = settings.getString("timeslot", "AM");

			ClasssPressure pressure = new ClasssPressure();
			pressure.setPatient_Id(PatientIdDroid);

			pressure.setSystolic(Integer.parseInt(str[0]));
			pressure.setDiastolic(Integer.parseInt(str[1]));
			pressure.setPulse(Integer.parseInt(str[2]));
			pressure.setInputmode(0);
			pressure.setTimeslot(slot);
			pressure.setBody_movment(bodyMovement);
			pressure.setIrregular_pulse(irregularPulse);
			pressure.setBattery_level(batteryLevel);
			pressure.setMeasurement_position_flag(positionFlag);
			pressure.setCuff_fit_flag(cuffFitFlag);

			SharedPreferences settings2 = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);
			String sectiondate = settings2.getString("sectiondate", "0");
			pressure.setSectionDate(sectiondate);
			pressure.setPrompt_flag("4");

			showResult(str[0],str[1],str[2]);

		} catch (Exception e) {
			Log.i("A&D", "Exception inserting bp from in a and d continua");

		}

	}

	private void setwelcome() {
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){
			
			Util.soundPlay("Messages/TakeBP_1.wav");
		}else{
			
			Util.soundPlay("Messages/TakeBP.wav");
		}
	}

	private void Animations() {
		txtwelcom.setText(this.getString(R.string.enterpressurebluetooth));
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, " KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);

			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	private void pairADevice(int pos,String testFlag){
		appClass.populateSpinner(spnVitals,pos,TestDeviceOmronBlsActivity.this,testFlag);
	}

	private void startTesting(){
		layout_readingSection.setVisibility(View.VISIBLE);
		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		setwelcome();
		Animations();

		CheckBlueToothState();
		bindService(new Intent(this, OmronBleService.class), mConnection, Context.BIND_AUTO_CREATE);
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				startPairing();
			}
		}, 10000);
	}

	private void showResult(String sys,String dia,String pulse){

		txtReading.setVisibility(View.INVISIBLE);
		rocketImage.setVisibility(View.INVISIBLE);
		txtwelcom.setText(Html.fromHtml(this
				.getString(R.string.yourbloodpressure)
				+ "<FONT COLOR=\"#D45D00\">"
				+ sys
				+ "</FONT>"
				+ this.getString(R.string.over)
				+ "<FONT COLOR=\"#D45D00\">"
				+ dia
				+ "</FONT>"
				+ this.getString(R.string.withpulserate)
				+ "<FONT COLOR=\"#D45D00\">"
				+ " "
				+ pulse
				+ "</FONT>"
				+ this.getString(R.string.beatsperminute)));
		playSound(sys,dia,pulse);
	}

	private void CheckBlueToothState() {
		registerReceiver(mBlutoothStateReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

		if (bluetoothAdapter != null) {
			if (bluetoothAdapter.isEnabled()) {
				IntentFilter filter1 = new IntentFilter();
				filter1.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
				registerReceiver(mybroadcast, filter1);
				isBroadcastRegisterd = true;
				if (mBleService != null){
					mBleService.setCurrentContext(getApplicationContext(), (IBleListener) mBinder);
				}
				bindService(new Intent(this, OmronBleService.class), mConnection, Context.BIND_AUTO_CREATE);
				mScanServiceUuids	= new UUID[]{ OmronBleService.Blood_Pressure_SERVICE_UUID };
			} else {
				bluetoothAdapter.enable();
			}
		}
	}

	private final BroadcastReceiver mBlutoothStateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(intent.getAction())) {
				int state = intent.getExtras().getInt(BluetoothAdapter.EXTRA_STATE);

				if(state == BluetoothAdapter.STATE_ON)
					CheckBlueToothState();
			}
		}
	};

	private Date parseTimeToMilliseconds(String time){

		Date dateTime = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			dateTime = sdf.parse(time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dateTime;

	}

	ArrayList<String> final_list = new ArrayList<String>();
	private void playSound(String sys,String dia,String pulse) {


		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){

			final_list.add("Messages/YourBPis_1.wav");
		}else{

			final_list.add("Messages/YourBPis.wav");
		}

		add_to_finalList(sys);
		
		final_list.add("Messages/over.wav");		
		
		add_to_finalList(dia);
		if(languageId.equals("11")){

			final_list.add("Messages/WPulseRateof_1.wav");
		}else{

			final_list.add("Messages/WPulseRateof.wav");
		}
		add_to_finalList(pulse);
		final_list.add("Messages/beatsPerMin.wav");

		Util.playSound4FileList(final_list, getApplicationContext());
	}

	private void add_to_finalList(String vitalValue) {
		String no_to_string = "";
		try {
			NumberToString ns = new NumberToString();

			if (vitalValue.contains(".")) {
				Double final_value = Double.parseDouble(vitalValue);
				no_to_string = ns.getNumberToString(final_value);
			} else {
				Long final_value = Long.parseLong(vitalValue);
				no_to_string = ns.getNumberToStringLong(final_value);
			}

			List<String> sellItems = Arrays.asList(no_to_string.split(" "));

			for (String item : sellItems) {
				if (item.toString().length() > 0) {
					final_list.add("Numbers/" + item.toString() + ".wav");
				}
			}

			if (final_list.get(final_list.size() - 1).toString()
					.equals("Numbers/zero.wav")) // remove if last item is zero
			{
				final_list.remove(final_list.size() - 1);
			}
		} catch (Exception e) {

		}
	}

	Handler readingCollectingHandler = new Handler();
	Runnable readingCollectingRunnable  = new Runnable() {
		@Override
		public void run() {
			Log.d(TAG, "readingCollectingRunnable: ");
			if(sensorCurrentTime == null || mLatestReading == null ||  mLatestReadingTime == null) {
				return;
			}
			checkReadingIsTaken(mLatestReading, mLatestReadingTime );
			mLatestReadingTime = null;
			mLatestReadingTime = null;
		}
	};
	String mLatestReading;
	Date mLatestReadingTime;

	private void AddReadingAndWaitForTime(String data, Date time ) {
		Log.d(TAG, "AddReadingAndWaitForTime: ");
		if(data != null) {
			Log.d(TAG, "AddReadingAndWaitForTime:  reading received");
			Log.d(TAG, "AddReadingAndWaitForTime: mLatestReading" + ( mLatestReading == null ?  "NULL" :mLatestReading ) );
			Log.d(TAG, "AddReadingAndWaitForTime: mLatestReadingTime" + (mLatestReadingTime == null ?  "NULL" :mLatestReadingTime )  );

			Log.d(TAG, "AddReadingAndWaitForTime: data" + (data == null ?  "NULL" :data ) );
			Log.d(TAG, "AddReadingAndWaitForTime: time" + (time == null ?  "NULL" :time )  );

			if(mLatestReading == null || mLatestReadingTime == null){
				Log.d(TAG, "AddReadingAndWaitForTime: `Setting First Reading");
				mLatestReading = data;
				mLatestReadingTime = time;
			}else{
				if(mLatestReadingTime.getTime() < time.getTime()  ){
					Log.d(TAG, "AddReadingAndWaitForTime: Setting New Reading");
					mLatestReading = data;
					mLatestReadingTime = time;
				} else
					Log.d(TAG, "AddReadingAndWaitForTime: Skipping Reading");
			}
		}
		if(readingCollectingHandler == null){
			readingCollectingHandler = new Handler();
		}
		readingCollectingHandler.removeCallbacks(readingCollectingRunnable);
		readingCollectingHandler.postDelayed(readingCollectingRunnable,5000);
	}

	private boolean checkReadingIsTaken(String data, Date time ) {
		// check whether the activity that is currently shown on screen is in fact the test device page, rather than the session page
		ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
		String mClassName = cn.getClassName();
		if (!mClassName.equals("com.tkm.optumSierra.TestDeviceOmronBlsActivity")) {
			Log.d(TAG, "checkReadingIsTaken: reading was intercepted in the session page while the current top activity is not the session page.");
			return false;
		}

		long sensorTime = sensorCurrentTime.getTime() - time.getTime();
		long tabletTime = tabletRecievedTime.getTime() - tabletPageOpenTime.getTime();
		Log.i(TAG, "sensorTime:   "+ sensorTime);
		Log.i(TAG, "tabletTime:   "+ tabletTime);
		if(sensorTime > tabletTime){

			Log.i(TAG, "Old data");

			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					startPairing();
				}
			}, 25000);
			return false;
		} else {
			Log.i(TAG, "New data");
			save(data);
			return true;
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
	}
}
