package com.tkm.optumSierra;

import java.util.List;
import java.util.UUID;

import com.tkm.optumSierra.bean.ClasssPressure;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.GattByteBuffer;
import com.tkm.optumSierra.util.GattUtils;
import com.tkm.optumSierra.util.Util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.Html;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class TestDeviceP724BpSmart extends Titlewindow {

	BluetoothManager manager;
	BluetoothGatt mBluetoothGatt;
	BluetoothAdapter mBluetoothAdapter;
	Button btnmanual;
	TextView txtwelcom, txtReading;
	ImageView rocketImage;
	AnimationDrawable rocketAnimation;
	ObjectAnimator AnimPlz, ObjectAnimator;
	//BluetoothAdapter.LeScanCallback mLeScanCallback;
	BluetoothDevice deviceL = null;
	//int lastinsert_id = 0;
	Pressure_db dbcreatepressure = new Pressure_db(this);
	public String sys, dia, pul;
	private String TAG = "P724BpSmart";
	public final static String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
	public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
	public final static String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
	public final static String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";
	String print;
	Button b1;
	public static final long leastSigBits = 0x800000805f9b34fbL;
	// service
	public static final UUID BLOOD_PRESSURE = new UUID(
			(0x1810L << 32) | 0x1000, leastSigBits);

	// char write
	public static final UUID CHAR_send_commands = new UUID(
			(0x2A50L << 32) | 0x1000, leastSigBits);

	// char read
	public static final UUID Char_read_response = new UUID(
			(0x2A51L << 32) | 0x1000, leastSigBits);

	boolean statues;
	Button btnClose;
	private Button btn_beginTest;
	private Spinner spnVitals;
	private LinearLayout layout_readingSection;
	private GlobalClass appClass;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_device);
		appClass = (GlobalClass) getApplicationContext();
		appClass.isSupportEnable = true;
		manager = (BluetoothManager) getBaseContext().getSystemService(
				Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = manager.getAdapter();
		txtwelcom = (TextView) findViewById(R.id.txtwelcome);
		txtReading = (TextView) findViewById(R.id.takereading);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		txtwelcom.setTypeface(type, Typeface.NORMAL);
		txtReading.setTypeface(type, Typeface.NORMAL);
		rocketImage = (ImageView) findViewById(R.id.loading);

		
		btnClose = (Button) findViewById(R.id.testDeviceClose);
		btn_beginTest = (Button) findViewById(R.id.btn_beginTest);
		spnVitals = (Spinner)findViewById(R.id.spn_assignedVitals);
		layout_readingSection = (LinearLayout)findViewById(R.id.layout_readingSection);
		layout_readingSection.setVisibility(View.INVISIBLE);
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			appClass.setBundle(extras);
		}else{
			extras = appClass.getBundle();
		}
		int pos = extras.getInt("Position", -1);
		if(extras.getString("TestFlag").equals("pair")){

			pairADevice(pos,"pair");	
		}else if(extras.getString("TestFlag").equals("test"))	{

			pairADevice(pos,"test");
		}else if(extras.getString("TestFlag").equals("testAll"))	{

			pairADevice(pos,"testAll");
		}

		btnClose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click				
				Intent intent = new Intent(getApplicationContext(), FinalMainActivity.class);
				finish();
				startActivity(intent);
			}
		});
		btn_beginTest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click	
				btn_beginTest.setEnabled(false);
				btn_beginTest.setBackgroundColor(Color.parseColor("#a0a0a0"));
				startTesting();
			}
		});		
		
		if (mBluetoothAdapter == null) {
			Toast.makeText(getBaseContext(), "no adapter found",
					Toast.LENGTH_SHORT).show();
			finish();
			return;
		}

		else if (!mBluetoothAdapter.isEnabled()) {
			Toast.makeText(getBaseContext(), "no adapter found",
					Toast.LENGTH_SHORT).show();
			finish();
			return;
		}
		/*
		 * else{
		 * 
		 * search(); }
		 */
		

	}

	private void setwelcome() {
		SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String languageId = settingsUserSp.getString("language_id", "0");
		if(languageId.equals("11")){
			
			Util.soundPlay("Messages/TakeBP_1.wav");
		}else{
			
			Util.soundPlay("Messages/TakeBP.wav");
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		// tts.speak(welcome, TextToSpeech.QUEUE_FLUSH, null);
	}

	private void Animations() {
		txtwelcom.setText(this.getString(R.string.enterpressurebluetooth));
//		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
//		AnimPlz.setDuration(1000);
//		AnimPlz.start();
//		AnimPlz.addListener(new AnimatorListenerAdapter() {
//			public void onAnimationEnd(Animator animation) {
//
//				Log.e("", "AnimPlz");
//
//				txtReading.setVisibility(View.VISIBLE);
//				rocketImage.setVisibility(View.VISIBLE);
//
//			}
//		});
	}

	

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		Log.e(TAG, "+ ON onPause P724BpSmart +");
		super.onPause();
		
		mBluetoothAdapter.stopLeScan(mLeScanCallback);

		if (mBluetoothGatt == null) {
			// finish();
			return;
		}
		try{
			mBluetoothGatt.close();
		}catch(RuntimeException e){
			Log.e("Exception", ""+e);
		}
		mBluetoothGatt = null;
		
	}
	@Override
	public void onStop() {
		super.onStop();
		
		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
			Log.e(TAG, "-- ON STOP A and D --");
	}
	@Override
	protected void onResume() {

		Log.e(TAG, "+ ON RESUME P724BpSmart +");
		

		super.onResume();
	}

	public void search() {

		boolean x = mBluetoothAdapter.startLeScan(mLeScanCallback);
		if (x) {
			Log.e(TAG, "scan status- true");

		} else {
			Log.e(TAG, "scan status- false");
		}

	}
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
		@Override
		public void onLeScan(final BluetoothDevice device, int rssi,
				byte[] scanRecord) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Log.e(TAG, "Foundddd device-"+ device.toString());

					deviceL = device;
					
					/*Toast.makeText(getBaseContext(),
							"device found " + device.toString(),
							Toast.LENGTH_SHORT).show();*/
					
						connect();
					


				}
			});
		}
	};
	private void connect() {

		mBluetoothAdapter.stopLeScan(mLeScanCallback);
		mBluetoothGatt = deviceL.connectGatt(getBaseContext(), false,
				mGattCallback);
		Log.i(TAG, "zzzzzzz- connectttttttttt");

		// List<BluetoothGattCharacteristic> list =
		// service.getCharacteristics();


	}

	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {

			// String intentAction;
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				// intentAction = ACTION_GATT_CONNECTED;
				// mConnectionState = STATE_CONNECTED;
				// broadcastUpdate(intentAction);
				Log.i(TAG, "zzzzzzz Connected to GATT server.");
				Log.i(TAG , "zzzzzzz Attempting to start service discovery:"
						+ mBluetoothGatt.discoverServices());

				// int bondState = 0;
				// while (bondState == BluetoothDevice.BOND_NONE || bondState ==
				// BluetoothDevice.BOND_BONDING);

				Log.i(TAG, "bond state finished");

			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				// intentAction = ACTION_GATT_DISCONNECTED;
				// mConnectionState = STATE_DISCONNECTED;
				Log.i(TAG , "mBluetoothGatt - Disconnected from GATT server.");
				// broadcastUpdate(intentAction);
				// mBluetoothAdapter.stopLeScan(mLeScanCallback);

				// mLeScanCallback = null;
				mBluetoothGatt.close();
				mBluetoothGatt = null;

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Intent starterIntent = getIntent();
						finish();
						startActivity(starterIntent);
						//

					}
				});

			}

		}

		public void onServicesDiscovered(BluetoothGatt gatt, int status) {

			if (status == BluetoothGatt.GATT_SUCCESS) {

				try {

					BluetoothGattService service = mBluetoothGatt
							.getService(BLOOD_PRESSURE);

					update(service);

				} catch (NullPointerException e) {
					Log.e(TAG, "service error no uiiiid");
				}

				// BluetoothGattCharacteristic characteristic;
				// mBluetoothGatt.readCharacteristic(characteristic.)
			} else {
				Log.w(TAG , "onServicesDiscovered received: " + status);
			}

		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			// TODO Auto-generated method stub
			Log.e(TAG, "zzzzzzzz characteristic change");
			broadcastUpdate(characteristic);

		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// TODO Auto-generated method stub
			Log.e(TAG, "zzzzzzzzcharacteristic write finished");
			if (!finished) {
				read();
			} else {
				mBluetoothGatt.close();
			}
		}

		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// if (status == BluetoothGatt .GATT_SUCCESS) {
			Log.e(TAG, "zzzzzzzz characteristic read");
			broadcastUpdate1(characteristic);

			if (!finished) {

				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String read = "FA21E5FA";
				byte[] ff = hexStringToByteArray(read);
				b.setValue(ff);
				mBluetoothGatt.writeCharacteristic(b);

			} else {
				Log.e(TAG, "zzzzzzzz  on finish");
				String end = "FA2001E5FA";
				byte[] aa = hexStringToByteArray(end);
				b.setValue(aa);
				mBluetoothGatt.writeCharacteristic(b);
				Log.e(TAG, "zzzzzzzz  read finish");

			}
			// }

		}

	};

	boolean deviceOn = false;
	boolean finished = false;
	boolean started = false;

	public void read() {
		mBluetoothGatt.readCharacteristic(read);
	}

	BluetoothGattCharacteristic read;
	BluetoothGattCharacteristic a = null;
	BluetoothGattCharacteristic b;

	public void update(BluetoothGattService service) {

		List<BluetoothGattCharacteristic> list = service.getCharacteristics();

		for (int i = 0; i < list.size(); i++) {
			Log.e(TAG, "charactaristic "+ list.get(i).toString());
			// mBluetoothGatt.readCharacteristic(list.get(i));

			if (list.get(i).getUuid().equals(CHAR_send_commands)) {
				Log.e(TAG, "char  fouuuuuuund"+ list.get(i).getUuid().toString());
				a = list.get(i);

				b = list.get(i);

				String Start = "FA2000E6FA";
				byte[] dd = hexStringToByteArray(Start);
				a.setValue(dd);

				mBluetoothGatt.writeCharacteristic(a);

				// mBluetoothGatt.executeReliableWrite();

				// mBluetoothGatt.writeCharacteristic(b);

			} else if (list.get(i).getUuid().equals(Char_read_response)) {

				Log.e(TAG, "charactaristic  2 "+ list.get(i).toString());

				// mBluetoothGatt.setCharacteristicNotification(list.get(i),
				// true);
				// while (true) {
				read = list.get(i);
				// try {
				// Thread.sleep(5000);
				// } catch (InterruptedException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

			}

		}

	}

	public byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
					.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

	StringBuffer sb = new StringBuffer("");

	private void broadcastUpdate1(
			final BluetoothGattCharacteristic characteristic) {
		sb.setLength(0);
		byte[] data = characteristic.getValue();
		Log.d("data", data.toString() + "  " + data.length + "   "

		+ getHex(data, data.length));

		sb.append(getHex(data, 13));

		String data2 = sb.toString();
		Log.e(TAG, "data2 >>>>>>>>>"+ data2);
		if (data != null && data.length > 0) {

			Log.d(TAG, "data"+ data.toString() + "  " + data.length);
			GattByteBuffer bb = GattByteBuffer.wrap(data);

			byte flags = bb.p724getInt8();
			if (flags != 0) {
				float a, b, c;

				// a = GattUtils.getFloatValue(data, GattUtils.FORMAT_SFLOAT,
				// 1);

				// b = GattUtils.getFloatValue(data, GattUtils.FORMAT_SFLOAT,
				// 3);
				boolean x = Time_Stamp_Flag(flags);

				// }

				if (x == false) {
					finished = true;

					String[] v = data2.split(" ");

					String systolic = v[5].concat(v[6]);
					String diastolic = v[7].concat(v[8]);
					String pulse = v[9].concat(v[10]);

					Log.e(TAG, "results"+ systolic + "  " + diastolic + "  " + pulse);

					final int indvalue = Integer.parseInt(systolic, 16);
					final int value = Integer.parseInt(diastolic, 16);
					final int pul = Integer.parseInt(pulse, 16);

					/*
					 * b = GattUtils.getFloatValue(data,
					 * GattUtils.FORMAT_SFLOAT, 5); a =
					 * GattUtils.getFloatValue(data, GattUtils.FORMAT_SFLOAT,
					 * 7); c = GattUtils.getFloatValue(data,
					 * GattUtils.FORMAT_SFLOAT, 9);
					 */

					print = "systolic " + indvalue + " diastolic  " + value
							+ " pulse  " + pul;
					Log.d(TAG, "your blood pressure is"+ print);

					print = indvalue + "," + value + "," + pul;

					runOnUiThread(new Runnable() {
						@Override
						public void run() {

							
							// results.setTextSize(14);
							// results.setText(print);
							Log.d(TAG, "your blood pressure is"+ print);
							if ((indvalue > 0) && (value > 0) && (pul > 0)) {

								if (indvalue < 999 && value < 999 && pul < 999) {

									save(print);
								}
							}

						}
					});

				}
				// Log.d("sa", print);
				// print.
			}

		}

	}

	private void broadcastUpdate(
			final BluetoothGattCharacteristic characteristic) {

		byte[] data = characteristic.getValue();
		Log.d(TAG, "data"+ data.toString() + "  " + data.length + "        "
				+ getHex(data, data.length));
		if (data != null && data.length > 0) {
			StringBuilder stringBuilder = new StringBuilder(" ");
			;

			// = new StringBuilder(data.length);
			Log.d(TAG, "data"+ data.toString() + "  " + data.length + "        "
					+ getHex(data, data.length));
			GattByteBuffer bb = GattByteBuffer.wrap(data);

			byte flags = bb.p724getInt8();

			float a, b, c;

			// a = GattUtils.getFloatValue(data, GattUtils.FORMAT_SFLOAT, 1);

			// b = GattUtils.getFloatValue(data, GattUtils.FORMAT_SFLOAT, 3);
			boolean x = Time_Stamp_Flag(flags);

			Log.e(TAG, "results "+ x + "  " + flags);

			// }

			Log.d("sa", print);
			// print.

		}
	}

	static final String HEXES = "0123456789ABCDEF";

	public static String getHex(byte[] raw, int len) {
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(3 * raw.length);
		for (final byte b : raw) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4))
					.append(HEXES.charAt((b & 0x0F))).append(" ");
			if (--len == 0)
				break;
		}
		return hex.toString();
	}

	private boolean Time_Stamp_Flag(byte flags) {
		if ((flags & GattUtils.EIGTH_BITMASK) != 0)
			return true;
		return false;
	}

	private void save(String result) {

		SharedPreferences flow = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int val = flow.getInt("flow", 0); // #1
		if (val == 1) {
			
			SharedPreferences section = getSharedPreferences(
					CommonUtilities.PREFS_NAME_date, 0);
			SharedPreferences.Editor editor_retake = section.edit();
			editor_retake.putString("sectiondate",Util.get_patient_time_zone_time(this));
			editor_retake.commit();
		}
		
		String ttmessage = "";
		String message = result;
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.USER_SP, 0);
		int PatientIdDroid = Integer.parseInt(settings1.getString("patient_id",
				"-1"));
		
		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.USER_TIMESLOT_SP, 0);
		String slot = settings.getString("timeslot", "AM");
		
		String[] str = null;
		str = print.split(",");
		ClasssPressure pressure = new ClasssPressure();
		pressure.setPatient_Id(PatientIdDroid);

		pressure.setSystolic(Integer.parseInt(str[0]));
		pressure.setDiastolic(Integer.parseInt(str[1]));
		pressure.setPulse(Integer.parseInt(str[2]));
		pressure.setInputmode(0);
		pressure.setTimeslot(slot);
		SharedPreferences settings2 = getSharedPreferences(
				CommonUtilities.PREFS_NAME_date, 0);		
		String sectiondate=settings2.getString("sectiondate", "0");
		pressure.setSectionDate(sectiondate);
		pressure.setPrompt_flag("4");
		//lastinsert_id = dbcreatepressure.InsertPressure(pressure);
		
		sys = "" + str[0];
		dia = "" + str[1];
		pul = "" + str[2];
		showResult(sys, dia, pul);
//		Intent intent = new Intent(TestDeviceP724BpSmart.this, ShowPressureActivity.class);
//		intent.putExtra("sys", sys);
//		intent.putExtra("dia", dia);
//		intent.putExtra("pulse", pul);
//		intent.putExtra("pressureid", lastinsert_id);
//		// Log.e(TAG, "redirecting from a and bp to show");
//		/*
//		 * try { mBluetoothService.stop(); } catch (Exception ec) {
//		 * 
//		 * }
//		 */
//		startActivity(intent);
//		TestDeviceP724BpSmart.this.finish();
	

	}
	
	private void pairADevice(int pos,String testFlag){
		//		txtwelcom.setVisibility(View.GONE);
		//		txtReading.setText(getResources().getString(R.string.pairingDevice));
		appClass.populateSpinner(spnVitals,pos,TestDeviceP724BpSmart.this,testFlag);
	}

	private void startTesting(){
		layout_readingSection.setVisibility(View.VISIBLE);
		rocketImage.setBackgroundResource(R.anim.rocket);
		rocketAnimation = (AnimationDrawable) rocketImage.getBackground();
		rocketAnimation.start();
		rocketImage.setVisibility(View.INVISIBLE);
		txtReading.setVisibility(View.INVISIBLE);		
		setwelcome();
		Animations();
		
		manager = (BluetoothManager) getBaseContext().getSystemService(
				Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = manager.getAdapter();

		mBluetoothAdapter.stopLeScan(mLeScanCallback);

		if (deviceL == null) {
			search();
			Log.i("onResume", "search()..........");
		} else {
			Log.i("onResume", "connect().........");
			connect();
		}
	}
	
	private void showResult(String sys,String dia,String pulse){
		txtReading.setVisibility(View.INVISIBLE);
		rocketImage.setVisibility(View.INVISIBLE);
		txtwelcom.setText(Html.fromHtml(this
				.getString(R.string.yourbloodpressure)
				+ "<FONT COLOR=\"#D45D00\">"
				+ sys
						+ "</FONT>"
						+ this.getString(R.string.over)
						+ "<FONT COLOR=\"#D45D00\">"
						+ dia
								+ "</FONT>"
								+ this.getString(R.string.withpulserate)
								+ "<FONT COLOR=\"#D45D00\">"
								+ " "
								+ pulse
										+ "</FONT>"
										+ this.getString(R.string.beatsperminute)));
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			

			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

}

