package com.tkm.optumSierra;

import java.util.Locale;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import com.tkm.optumSierra.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.tkm.optumSierra.bean.ClassReminder;
import com.tkm.optumSierra.dal.Flow_Status_db;
import com.tkm.optumSierra.dal.Reminder_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.util.Alam_util;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.Regular_Alam_util;
import com.tkm.optumSierra.util.Skip_Reminder_Values_Upload;
import com.tkm.optumSierra.util.UploadVitalReadingsTask;
import com.tkm.optumSierra.util.Util;

public class ThanksActivity extends Titlewindow implements OnInitListener {

	BackgroundThread backgroundThread;
	private TextToSpeech tts;
	TextView username, message;
	TextView txtwelcom;
	ObjectAnimator AnimPlz, ObjectAnimator;
	public int flag = 0;
	public boolean closeflag = false;
	private String TAG = "-------ThanksActivity----------";
	private String serverurl = "";
	private GlobalClass appState;
	private Reminder_db reminder_db;
	Flow_Status_db Flow_db;
	String[] Status;
	String Status_Val;
	private Sensor_db sensor_db = new Sensor_db(this);
	private GlobalClass globalClass;
	public void onInit(int status) {

		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.US);
			Log.i("TTS", "----------Initilization Success-----------");
			tts.setSpeechRate(0); // set speech speed rate
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e("TTS", "Language is not supported");
			} else {
			}
		} else {
			Log.e("TTS", "-------------Initilization Failed--------------");
		}

	}

	private void sound_play() {
		try {

			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.USER_SP, 0);

		String patientname = settings.getString("patient_name", "-1"); // #1

		if (patientname.trim().length() > 0) {

			Util.soundPlay("Messages/ThankYou.wav");

			try {

				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Log.e(TAG, "------------sound_play-------------"+patientname);
			tts.speak(patientname, TextToSpeech.QUEUE_FLUSH, null);

			try {
				Thread.sleep(1200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
			String languageId = settingsUserSp.getString("language_id", "0");
			if(languageId.equals("11")){
				
				Util.soundPlay("Messages/ResultsSubmitted_1.wav");
			}else{
				
				Util.soundPlay("Messages/ResultsSubmitted.wav");
			}
			

		}
		new Skip_Reminder_Values_Upload(this, serverurl).execute();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_thanks);
		globalClass = (GlobalClass)getApplicationContext();
		globalClass.isEllipsisEnable = false;
		globalClass.isSupportEnable = false;
		//globalClass.uploadStatus("success");
		SharedPreferences file_data_settings = getApplicationContext().getSharedPreferences("file_data", 0);
		int sleep_system_idle_timeout = Integer.parseInt(file_data_settings.getString("sleep_session_end_timeout", "300")); // defualt as 300 sec ie 5 min					
		android.provider.Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, (60000 * sleep_system_idle_timeout/60));
//		if(globalClass.isRemSessionStart){
//
//			SharedPreferences lastSessionStatus = getSharedPreferences(CommonUtilities.LASTUPLOAD_STATUS, 0);
//			String sessionUploadTime = lastSessionStatus.getString("upload_session_time", "");
//			String sessionUploadStatus = lastSessionStatus.getString("upload_session_status", "");
//
//			
//			SharedPreferences.Editor lastStatusEditor = lastSessionStatus.edit();
//			lastStatusEditor.putString("upload_time", sessionUploadTime);			
//			lastStatusEditor.putString("upload_status", sessionUploadStatus);
//			lastStatusEditor.commit();
//		}
//		globalClass.isRemSessionStart = false;

		//	android.provider.Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_OFF_TIMEOUT, (60000 * 5));
		appState = (GlobalClass) getApplicationContext();
		reminder_db = new Reminder_db(this);
		Flow_db = new Flow_Status_db(ThanksActivity.this);

		setConfig();
		tts = new TextToSpeech(this, this);

		username = (TextView) findViewById(R.id.txtusername_thanks);
		message = (TextView) findViewById(R.id.takereading);
		globalClass.isSessionStart = false;
		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.USER_SP, 0);
		String patientname = settings.getString("patient_name", "-1"); // #1
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		username.setTypeface(type, Typeface.BOLD);
		message.setTypeface(type, Typeface.NORMAL);

		if (!patientname.equals("-1")) {

			int a = sensor_db.Selectsensororder();
			if (a != 0) {
				username.setText(this.getString(R.string.thanks) + patientname
						+ ".");
			} else {
				Typeface type_in = Typeface.createFromAsset(getAssets(),
						"fonts/FrutigerLTStd-Roman.otf");
				message.setVisibility(View.INVISIBLE);
				username.setText(R.string.nothingscheduled);
				username.setTextSize(45);
				username.setTextColor(getResources().getColor(R.color.black));
				username.setTypeface(type_in, Typeface.NORMAL);
			}
			// sound_play();

		}

		// uploadSensordetails();

		txtwelcom = (TextView) findViewById(R.id.txthide);
		backgroundThread = new BackgroundThread();
		backgroundThread.setRunning(true);
		backgroundThread.start();

		// insert_skip_values(); //skip only for reminder flow. ########################
		Regular_Alam_util.cancelAlarm(ThanksActivity.this);
		Alam_util.cancelAlarm(ThanksActivity.this);

		Flow_db.open();
		Cursor cursor1 = Flow_db.selectAll();
		Status = new String[cursor1.getCount()];
		int j = 0;

		while (cursor1.moveToNext()) {
			Status[j] = cursor1.getString(cursor1.getColumnIndex("Status"));

			Status_Val = Status[0];
			j += 1;
		}
		Flow_db.close();

		if (Status_Val.equals("0")) {
			Flow_db.open();
			Flow_db.deleteAll();
			Flow_db.insert("1");
			Flow_db.close();
			ClassReminder reminder = new ClassReminder();
			if (reminder_db.GetStartedAlarm() != null) {
				reminder = reminder_db.GetStartedAlarm();
				reminder.setStatus(2); // Set status to SUCCESS
				reminder_db.UpdateReminderStatus(reminder);
			}

		} else {
			Log.i("TitleWindow", "appState.isAlarmTriggered()--> " + Status_Val);
		}
		if (appState.isRegular_Alarm_Triggered() == true) {
			Regular_Alam_util.cancelAlarm(ThanksActivity.this);
			appState.setRegular_Alarm_Triggered(false);
			appState.setStart_Alarm_Count(0);
		}

		// clear retak sp
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);
		SharedPreferences.Editor editor_retake = settings1.edit();
		editor_retake.putInt("retake_status", 0);
		editor_retake.commit();
		Log.i(TAG, "--oncreate completed thanks actt---- ");

		

	}
	private void insert_skip_values() {
		SharedPreferences flowsp = getSharedPreferences(
				CommonUtilities.USER_FLOW_SP, 0);
		int flow_type = flowsp.getInt("is_reminder_flow", 0);

		if(flow_type==1)//skip for reminder
		{
			/*Log.e("TitleWindow", "During reminder flow user click on START/HOM- Inserting skip data");
			SkipVitals.skip_patient_vitalse(getApplicationContext());
			SharedPreferences.Editor seditor = flowsp.edit();			
			seditor.putInt("is_reminder_flow", 0);
			seditor.commit();*/
		}
		else
		{
			Log.e("Thanks activity", "User click on START/HOME in normal flow.");
			SharedPreferences settings =getSharedPreferences(
					CommonUtilities.SERVER_URL_SP, 0);	
			String posturl = settings.getString("Server_post_url", "-1");
			new UploadVitalReadingsTask(ThanksActivity.this, posturl).execute();

		}

	}

	@Override
	protected void onStart() {
		super.onStart();

		// sound_play();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.thanks, menu);
		return true;
	}

	@Override
	protected void onRestart() {
		super.onRestart();

		/*
		 * Intent intent = new Intent(ThanksActivity.this,
		 * FinalMainActivity.class); this.finish(); startActivity(intent);
		 */

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		if (flag == 1) {
			Intent intent = new Intent(ThanksActivity.this,
					FinalMainActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(0, 0);
			flag = 0;
		}
		WindowManager.LayoutParams layout = getWindow().getAttributes();
		layout.screenBrightness = 1F;
		getWindow().setAttributes(layout);

		anim();

		return super.onTouchEvent(event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {

			closeflag = true;
			if (tts != null) {
				tts.stop();
				tts.shutdown();
			}
			Util.stopPlaySingle();
			Util.Stopsoundplay();

			Intent intent = new Intent(this, FinalMainActivity.class);

			this.finish();
			startActivity(intent);
			overridePendingTransition(0, 0);

		}
		return super.onKeyDown(keyCode, event);
	}

	public class BackgroundThread extends Thread {
		volatile boolean running = false;
		int cnt;

		void setRunning(boolean b) {
			running = b;
			cnt = 15;
		}

		@Override
		public void run() {
			while (running) {
				try {
					sleep(1000);
					if (cnt == 14) {

						int a = sensor_db.Selectsensororder();
						if (a != 0) {
							sound_play();
						} else {
							cnt = 1;
						}
					}
					if (cnt-- == 0) {

						running = false;
					}
				} catch (InterruptedException e) {
					// e.printStackTrace();
				}
			}
			handler.sendMessage(handler.obtainMessage());
		}
	}

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			setProgressBarIndeterminateVisibility(false);
			// progressDialog.dismiss();

			boolean retry = true;
			while (retry) {
				try {
					backgroundThread.join();
					retry = false;
				} catch (InterruptedException e) {
					// e.printStackTrace();
				}
			}

			SharedPreferences settings = getSharedPreferences(
					CommonUtilities.CLUSTER_SP, 0);	

			int	is_home_enable = settings.getInt("is_home_enable", 0);
			/*----------------------CALLING REEBOOTCHECKER ---- START-----------------------*/
			try {
				rebootTimer.cancel();
				timerReloader = 0;
				rebootTimer.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
			/*----------------------CALLING REEBOOTCHECKER ---- END-----------------------*/
			if (is_home_enable==1) { // Home is enabled for this user
				if (!closeflag) {
					Intent intent = new Intent(ThanksActivity.this, FinalMainActivity.class);
					startActivity(intent);
					overridePendingTransition(0, 0);
				} else {
					Log.i(TAG,
							"user clicked title window, re starting application");
				}
			} else  { // Home is not enabled for this user, redirecting to thanks.
				if (!closeflag) {
					anim();
					Typeface type = Typeface.createFromAsset(getAssets(),
							"fonts/FrutigerLTStd-Roman.otf");
					message.setVisibility(View.INVISIBLE);
					username.setText(R.string.nothingscheduled);
					username.setTextSize(45);
					username.setTextColor(getResources()
							.getColor(R.color.black));
					username.setTypeface(type, Typeface.NORMAL);
					int a = sensor_db.Selectsensororder();
					if (a != 0) {
						Util.soundPlay("Messages/nothingScheduled.wav");
					}
				} else {
					Log.i(TAG,
							"user clicked title window, re starting application");
				}
				
			}

		}

	};

	private void anim() {

		AnimPlz = ObjectAnimator.ofFloat(txtwelcom, "translationY", 0f, -50f);
		AnimPlz.setDuration(10000);
		AnimPlz.start();

		AnimPlz.addListener(new AnimatorListenerAdapter() {
			public void onAnimationEnd(Animator animation) {

				WindowManager.LayoutParams layout = getWindow().getAttributes();
				layout.screenBrightness = 0.05F;
				getWindow().setAttributes(layout);
				flag = 1;

			}
		});
	}

	@Override
	public void onStop() {
		// Commit the edits!
		super.onStop();
		closeflag = true;
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		Util.stopPlaySingle();
		Util.Stopsoundplay();
		Log.i(TAG, "--on stop thanks act---- ");
		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");
	}

	private void setConfig() {

		{

			SharedPreferences settings = getSharedPreferences(
					CommonUtilities.SERVER_URL_SP, 0);

			serverurl = settings.getString("Server_post_url", "-1");
			CommonUtilities.SERVER_URL = serverurl;

		}
	}
}
