package com.tkm.optumSierra;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.tkm.optumSierra.util.Log;
import com.tkm.optumSierra.util.OnInit_Data_Access;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.tkm.optumSierra.bean.ClassReminder;
import com.tkm.optumSierra.bean.ClassSensor;
import com.tkm.optumSierra.bean.ClassWeight;
import com.tkm.optumSierra.dal.Flow_Status_db;
import com.tkm.optumSierra.dal.Glucose_db;
import com.tkm.optumSierra.dal.MydataSensor_db;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.dal.Pulse_db;
import com.tkm.optumSierra.dal.Questions_db;
import com.tkm.optumSierra.dal.Reminder_db;
import com.tkm.optumSierra.dal.Temperature_db;
import com.tkm.optumSierra.dal.Weight_db;
import com.tkm.optumSierra.util.Alam_util;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.ConnectionDetector;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.CustomKeyboard;
import com.tkm.optumSierra.util.SkipVitals;
import com.tkm.optumSierra.util.UploadVitalReadingsTask;
import com.tkm.optumSierra.util.Util;

public class Titlewindow extends Activity {

	protected Button home, start, version, support, closeversion,myData;
	private EditText edtPasscode;
	public int appstatus, clickedhome, clickedhpf = 0;
	public String title_apk = "";
	private String serverurl = "";
	private Reminder_db reminder_db = new Reminder_db(this);
	Flow_Status_db Flow_db = new Flow_Status_db(this);
	private MydataSensor_db myDataSensor_db = new MydataSensor_db(this);
	String[] Status;
	String Status_Val;
	public static int double_click_flag = 0;
	Boolean isInternetPresent; 
	private Weight_db dbweight = new Weight_db(this);
	private Pulse_db dbpulse_ox = new Pulse_db(this);
	private Pressure_db dbpressure = new Pressure_db(this);
	private Glucose_db dbglucose = new Glucose_db(this);
	private Temperature_db dbtemparature = new Temperature_db(this);
	private Questions_db dbquestion = new Questions_db(this);
	boolean reboot_Status = false;

	private String TAG = "Titlewindow";

	public long startTime = 30 * 1000; // 30 sec default
	public final long interval = 1 * 1000;
	public static RebootTimer rebootTimer;
	public static int timerReloader = 0;
	public int upload_retry_times = 2;
	private int is_home_enable = 0;
	private int is_support_enable = 0;
	private int is_mydata_enable = 0;
	public String PostUrl = "";
	private GlobalClass globalClass;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setConfig();
		int ln = 0;
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);

		SharedPreferences file_data_settings = getApplicationContext().getSharedPreferences("file_data", 0);
		int upload_retry_delay = Integer.parseInt(file_data_settings.getString("upload_retry_delay", "-1"));
		upload_retry_times = Integer.parseInt(file_data_settings.getString("upload_retry_times", "-1"));
		if (upload_retry_delay == -1 || upload_retry_delay == 0) {
			startTime = 30 * 1000; // 30 sec default
		} else {
			startTime = upload_retry_delay * 1000;
		}
		if (upload_retry_times == -1 || upload_retry_times == 0) {
			upload_retry_times = 2; // 2 times default
		}

		rebootTimer = new RebootTimer(startTime, interval);

		SharedPreferences settings_n = getSharedPreferences(CommonUtilities.USER_SP, 0);
		String s = settings_n.getString("language_id", "0");
		ln = Integer.parseInt(s);
		Constants.setLanguage(ln);
		if (ln == 11) {
			Locale locale = new Locale("es");
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());

		} else {
			Locale.setDefault(Locale.US);
			Configuration config = new Configuration();
			config.locale = Locale.US;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.main);
		AudioManager audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
		audioManager.setStreamVolume(AudioManager.STREAM_RING,0, AudioManager.FLAG_PLAY_SOUND);
		globalClass = (GlobalClass)getApplicationContext();
		resetSessionTime();
		isInternetPresent = (new ConnectionDetector(getApplicationContext())).isConnectingToInternet();
		SharedPreferences settings = getSharedPreferences(CommonUtilities.CLUSTER_SP, 0);

		is_home_enable = settings.getInt("is_home_enable", 0);
		is_support_enable = settings.getInt("is_support_enable", 0);
		is_mydata_enable = settings.getInt("chart_status", 0);
		if (!checkConfigValues()) {

			// for live build call the below function
			// ********************************
			// insConfig();

			// for local testing build call the below function
			// ****************************
			// insConfig_local();
		}

		Util.stopPlaySingle();
		Util.Stopsoundplay();
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.window_title_hfp);
		home = (Button) findViewById(R.id.home);
		support = (Button) findViewById(R.id.support);
		version = (Button) findViewById(R.id.version);
		start = (Button) findViewById(R.id.start);
		myData = (Button) findViewById(R.id.myData);
		View view1 = (View)findViewById(R.id.view1);
		View view2 = (View)findViewById(R.id.view2);
		View view3 = (View)findViewById(R.id.view3);
		View view4 = (View)findViewById(R.id.view4);
		View view5 = (View)findViewById(R.id.view5);
		if (is_home_enable == 1) {

			home.setVisibility(View.VISIBLE);
			view2.setVisibility(View.VISIBLE);
		} else {
			home.setVisibility(View.GONE);
			view2.setVisibility(View.GONE);
		}

		if (is_support_enable == 1) {

			support.setVisibility(View.VISIBLE);
			view4.setVisibility(View.VISIBLE);
		} else {
			support.setVisibility(View.GONE);
			view4.setVisibility(View.GONE);
		}

		if (is_mydata_enable == 1) {

			myData.setVisibility(View.VISIBLE);
			view3.setVisibility(View.VISIBLE);
		} else {
			myData.setVisibility(View.GONE);
			view3.setVisibility(View.GONE);
		}

		ActivityManager am = (ActivityManager) getApplicationContext()
				.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
		String mClassName = cn.getClassName();

		if (mClassName.equals("com.tkm.optumSierra.FinalMainActivity")){

			start.setVisibility(View.GONE);
			view1.setVisibility(View.GONE);
		}else{

			start.setVisibility(View.VISIBLE);
			view1.setVisibility(View.VISIBLE);
		}


		support.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				if(globalClass.isSupportEnable){
					set_brightness();
					//
					LayoutInflater inflater = (LayoutInflater) Titlewindow.this.getSystemService(LAYOUT_INFLATER_SERVICE);

					View pview;
					pview = inflater.inflate(R.layout.popup_support, null);
					TextView username = (TextView) pview.findViewById(R.id.textView1);
					Button closesupport = (Button) pview.findViewById(R.id.exitsupport);

					SharedPreferences settings = getSharedPreferences(CommonUtilities.USER_SP, 0);

					Constants.setPatientid(Integer.parseInt(settings.getString("patient_id", "0")));

					String contractcode = settings.getString("contract_code_name", "-1");

					final PopupWindow cp = new PopupWindow(pview, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
							true);

					if (!contractcode.equals("-1")) {

						String mCurrentimage_path = Environment.getExternalStorageDirectory().toString()
								+ "/versions/HFP/Images/";

						File directoryFile = new File(mCurrentimage_path);

						File myFile = new File(directoryFile.getAbsolutePath() + "/" + contractcode + ".png");
						/*
						 * new DownloadContractCode(getApplicationContext(),
						 * Constants.getPostUrl()).execute();
						 */

						if (myFile.exists()) {

							String mCurrentPhotoPath = Environment.getExternalStorageDirectory().toString()
									+ "/versions/HFP/Images/" + contractcode + ".png";

							Util.stopPlaySingle();
							Util.Stopsoundplay();
							ImageView imageView = (ImageView) pview.findViewById(R.id.imageViewsuport);

							BitmapFactory.Options bmOptions = new BitmapFactory.Options();
							bmOptions.inJustDecodeBounds = true;
							BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

							bmOptions.inJustDecodeBounds = false;
							bmOptions.inPurgeable = true;
							Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
							imageView.setImageBitmap(bitmap);

							Util.soundPlay("Messages/ForClinicalHelpCall.wav");

							cp.setBackgroundDrawable(new BitmapDrawable(getApplicationContext().getResources()));
							cp.showAtLocation(v, Gravity.CENTER, 0, 0);

							DisplayMetrics displaymetrics = new DisplayMetrics();
							getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
							int height = (displaymetrics.heightPixels);
							int width = displaymetrics.widthPixels / 2;
							height = (int) (height / 1.2);

							// cp.update(0, 0, 350, 350);
							// cp.update(0, 0, 750, 500);
							cp.update(0, 0, width, height);
						}

					}
					closesupport.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							Util.stopPlaySingle();
							cp.dismiss();
						}
					});
				}

			}
		});
		version.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(globalClass.isEllipsisEnable){
					Intent intentDevicePasscode = new Intent(getApplicationContext(),EllipsisSettingsActivity.class);
					startActivity(intentDevicePasscode);
					//	new UploadErrorFile().execute();
				}

			}
		});

		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (double_click_flag == 0) {
					bluetoothChecking();
					globalClass.isEllipsisEnable = false;
					globalClass.isSupportEnable = true;
					double_click_flag = 1;
					Handler timx = new Handler();
					timx.postDelayed(new Runnable() {
						@Override
						public void run() {
							double_click_flag = 0;
						}
					}, 9000);
					//					globalClass.isRemSessionStart = false;
					SharedPreferences file_data_settings = getApplicationContext().getSharedPreferences("file_data", 0);
					int sleep_system_idle_timeout = Integer
							.parseInt(file_data_settings.getString("sleep_system_idle_timeout", "600")); // defualt
					// as
					// 600
					// sec
					// ie
					// 10
					// min
					android.provider.Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT,
							(60000 * sleep_system_idle_timeout / 60));

					start.setEnabled(false);
					Flow_db.open();
					Cursor cursor1 = Flow_db.selectAll();
					Status = new String[cursor1.getCount()];
					int j = 0;

					while (cursor1.moveToNext()) {
						Status[j] = cursor1.getString(cursor1.getColumnIndex("Status"));

						Status_Val = Status[0];
						j += 1;
					}
					Flow_db.close();
					if (Status_Val.equals(null)) {
						Status_Val = "1";
					}
					insert_skip_values();// skip only for reminder vitals
					if (Status_Val.equals("0")) {
						Alam_util.cancelAlarm(Titlewindow.this);

						Flow_db.open();
						Flow_db.deleteAll();
						Flow_db.insert("1");
						Flow_db.close();

						ClassReminder reminder = new ClassReminder();
						if (reminder_db.GetStartedAlarm() != null) {
							reminder = reminder_db.GetStartedAlarm();
							reminder.setStatus(3); // Set status to SUCCESS
							reminder_db.UpdateReminderStatus(reminder);
						}
					} else {
						Log.i("TitleWindow", "appState.isAlarmTriggered()--> " + Status_Val);
					}

					home.setEnabled(false);
					clickedhome = 1;
					set_brightness();
					// uploadSensordetails();
					// insert_skip_values();
					SharedPreferences flow = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);

					SharedPreferences.Editor editor = flow.edit();
					editor.putInt("flow", 1);
					editor.commit();

					Intent i = new Intent(Titlewindow.this, HFPMainActivity.class);
					startActivity(i);
					overridePendingTransition(0, 0);
				} else {
					Log.e("TitleWindow", "HOME BUTTON DOUBLE CLICK");
				}
			}
		});
		start.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				startSession();
			}
		});

		myData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				globalClass.isSupportEnable = true;
				DownloadMydataSensor();
			}
		});
	}

	private boolean checkConfigValues() {

		boolean flag = false;
		String serialNo = "-1";

		SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
		String server1 = settings.getString("Server_url", "-1");
		String server2 = settings.getString("Server_port", "-1");
		String server3 = settings.getString("Server_post_url", "-1");
		String server4 = settings.getString("Server_url", "-1");

		SharedPreferences settings1 = getSharedPreferences(CommonUtilities.USER_SP, 0);
		int PatientIdDroid = Integer.parseInt(settings1.getString("patient_id", "-1"));

		SharedPreferences settingseifi = getSharedPreferences(CommonUtilities.WiFi_SP, 0);
		serialNo = settingseifi.getString("wifi", "");
		String imei = settingseifi.getString("imei_no", "");

		if (server3.contains("-1")) {
			flag = false;
		}

		Constants.setPatientid(PatientIdDroid);
		Constants.setIMEI_No(imei);
		Constants.setParams(server1, server2, server3, serialNo);
		return flag;
	}

	private void set_brightness() {
		WindowManager.LayoutParams layout = getWindow().getAttributes();
		layout.screenBrightness = 1F;
		getWindow().setAttributes(layout);
	}

	private void insConfig() {
		try {

			SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("Server_url", "optumtelehealth.com");
			editor.putString("Server_port", "443");
			editor.putString("Server_post_url", "https://www.optumtelehealth.com");
			editor.commit();

			checkConfigValues();

		} catch (Exception e) {

		}
	}

	private void insConfig_local() {
		try {

			SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("Server_url", "www2.optumtelehealth.com");
			editor.putString("Server_port", "443");
			editor.putString("Server_post_url", "https://www2.optumtelehealth.com:443");
			editor.commit();

			checkConfigValues();

		} catch (Exception e) {

		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return super.onTouchEvent(event);
	}

	private void setConfig() {

		{
			SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);

			serverurl = settings.getString("Server_post_url", "-1");
			CommonUtilities.SERVER_URL = serverurl;

		}
	}

	private void insert_skip_values() {

		SharedPreferences flowsp = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
		int flow_type = flowsp.getInt("is_reminder_flow", 0);

		if (flow_type == 1)// skip for reminder
		{
			Log.e("TitleWindow", "During reminder flow user click on START/HOM- Inserting skip data");
			SkipVitals.skip_patient_vitalse(getApplicationContext());
			SharedPreferences.Editor seditor = flowsp.edit();
			seditor.putInt("is_reminder_flow", 0);
			seditor.commit();
		} else {
			Log.e("TitleWindow", "User click on START/HOME in normal flow.");
			SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
			String posturl = settings.getString("Server_post_url", "-1");
			new UploadVitalReadingsTask(Titlewindow.this, posturl).execute();

		}

	}

	private void set_flow(int a) {
		SharedPreferences flow = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);

		SharedPreferences.Editor editor = flow.edit();
		editor.putInt("start_flow", a);
		editor.commit();
	}

	public boolean rebootStatusCheck() {
		if (dbweight.reboot_Select()) {

			return true;
		} else if (dbtemparature.reboot_Select()) {

			return true;
		} else if (dbpulse_ox.reboot_Select()) {

			return true;
		} else if (dbpressure.reboot_Select()) {

			return true;
		} else if (dbglucose.reboot_Select()) {

			return true;
		} else if (dbquestion.reboot_Select()) {

			return true;
		}
		return false;
	}

	public class RebootTimer extends CountDownTimer {
		public RebootTimer(long startTime, long interval) {
			super(startTime, interval);
		}

		@Override
		public void onFinish() {
			try {
				Log.i(TAG, "CALL REBOOT CHECK");
				reboot_Status = rebootStatusCheck();
				Log.e(TAG, "reboot_Status = " + reboot_Status);
				if (reboot_Status) {
					timerReloader += 1;
					if (upload_retry_times != timerReloader) {
						rebootTimer.start();
						SharedPreferences settings = getApplicationContext()
								.getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
						String posturl = settings.getString("Server_post_url", "-1");
						if (!posturl.equals("-1")) {
							Log.e("TitleWindow", "RebootTimer flow.");
							new UploadVitalReadingsTask(getApplicationContext(), posturl).execute();
						}
					} else {
						timerReloader = 0;
						/*
						 * CHECK WHETHER THE USER IS ACTIVE WITH ANY FLOW OR NOT
						 */
						boolean activityStatus = new Util().activityCheck(Titlewindow.this);

						if (activityStatus) {
							/* CALL REBOOT ACTIVITY */
							Intent intent = new Intent(Titlewindow.this, RebootActivity.class);
							startActivity(intent);
							overridePendingTransition(0, 0);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onTick(long millisUntilFinished) {

		}
	}

	public void After_24_Hour_Reboot() {
		try {
			Intent myIntent = new Intent(Titlewindow.this, AlarmReceiverReboot.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(Titlewindow.this, 0, myIntent, 0);
			AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

			SharedPreferences settings = getApplicationContext().getSharedPreferences("file_data", 0);
			String reboot_allow_time_period_start = settings.getString("reboot_allow_time_period_start", "-1");
			String reboot_allow_time_period_end = settings.getString("reboot_allow_time_period_end", "-1");
			long reboot_delay_timer = Long.parseLong(settings.getString("reboot_delay_timer", "-1"));

			// range of Random Number
			int min = 5000;// 5 Sec
			int max = Integer.parseInt(settings.getString("duration", "-1"));

			if (reboot_allow_time_period_start.equals("-1")) {
				reboot_allow_time_period_start = "09:00";// set default as 9:00
				// AM
			}
			if (reboot_allow_time_period_end.equals("-1")) {
				reboot_allow_time_period_end = "21:00";// set default as 9:00 PM
			}
			if (reboot_delay_timer == -1) {
				reboot_delay_timer = 86400 * 1000;// set default as 24 Hour
			} else {
				reboot_delay_timer = reboot_delay_timer * 1000;

			}
			if (max == -1) {
				// 12 Hours
				max = 43200000;
			} else {
				// convert second to millisecond
				max = max * 1000;
			}

			String currentTime = Util.get_patient_time_zone_time_24_format(Titlewindow.this);
			currentTime = currentTime.substring(11, 13) + ":" + currentTime.substring(14, 16);
			String reg = "^([0-1][0-9]|2[0-3]):([0-5][0-9])$";
			if (reboot_allow_time_period_start.matches(reg) && reboot_allow_time_period_end.matches(reg)
					&& currentTime.matches(reg)) {
				// At Fire Time Calendar
				Calendar firingAt = Calendar.getInstance();

				// Start Time
				java.util.Date inTime = new SimpleDateFormat("HH:mm").parse(reboot_allow_time_period_start);
				Calendar initialCal = Calendar.getInstance();
				initialCal.setTime(inTime);

				// Current Time
				java.util.Date checkTime = new SimpleDateFormat("HH:mm").parse(currentTime);
				Calendar currentCal = Calendar.getInstance();
				currentCal.setTime(checkTime);
				// End Time
				java.util.Date finTime = new SimpleDateFormat("HH:mm").parse(reboot_allow_time_period_end);
				Calendar finalCal = Calendar.getInstance();
				finalCal.setTime(finTime);

				if (reboot_allow_time_period_end.compareTo(reboot_allow_time_period_start) < 0) {
					finalCal.add(Calendar.DATE, 1);
					currentCal.add(Calendar.DATE, 1);
				}

				java.util.Date actualTime = currentCal.getTime();

				Random r = new Random();
				int rMilli = r.nextInt(max - min + 1) + min;

				if ((actualTime.after(initialCal.getTime()) || actualTime.compareTo(initialCal.getTime()) == 0)
						&& actualTime.before(finalCal.getTime())) {
					// Current Time is in range
					Log.i(TAG, "TIME IS TRUE");

					long tempMilli = finalCal.getTimeInMillis() - currentCal.getTimeInMillis();
					if (tempMilli > reboot_delay_timer) {
						firingAt.setTimeInMillis(System.currentTimeMillis() + (int) reboot_delay_timer);
					} else {
						firingAt.set(Calendar.DAY_OF_MONTH, firingAt.get(Calendar.DAY_OF_MONTH) + 1);
					}
				} else {
					// Current Time is not in range
					Log.i(TAG, "TIME IS FALSE");
					long tempMilli = finalCal.getTimeInMillis() - currentCal.getTimeInMillis();
					if (tempMilli > reboot_delay_timer) {
						Log.i(TAG, "#IN tempMilli > reboot_delay_timer");
					} else {
						Log.i(TAG, "#IN tempMilli < reboot_delay_timer");
						initialCal.add(Calendar.DAY_OF_MONTH, 1);
					}
					long tempMilliIC = initialCal.getTimeInMillis() - currentCal.getTimeInMillis();
					// Adding random Milliseconds and difference time to Initial
					// Time
					firingAt.setTimeInMillis(System.currentTimeMillis() + (int) tempMilliIC + rMilli);
				}
				alarmManager.setRepeating(AlarmManager.RTC, firingAt.getTimeInMillis(), AlarmManager.INTERVAL_DAY,
						pendingIntent);
				Log.i(TAG, "SET TIME:" + firingAt.getTime());

			} else {
				throw new IllegalArgumentException("Not a valid time, expecting HH:MM format");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static class AlarmReceiverReboot extends BroadcastReceiver {
		String TAG = "AlarmReceiverReboot";

		@Override
		public void onReceive(Context context, Intent arg1) {
			Log.i(TAG, "###---ALARM RECEIVED---###");

			ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

			String mClassName = cn.getClassName();

			PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			boolean isScreenOn = pm.isScreenOn();

			if ((mClassName.equals("com.tkm.optumSierra.FinalMainActivity")
					|| mClassName.equals("com.tkm.optumSierra.Home")
					|| mClassName.equals("com.tkm.optumSierra.Reminder_Landing_Activity")
					|| mClassName.equals("com.tkm.optumSierra.ThanksActivity")) || isScreenOn == false) {

				/*----------------------CALLING REEBOOTCHECKER ---- START-----------------------*/
				try {
					rebootTimer.cancel();
					timerReloader = 0;
					rebootTimer.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
				/*----------------------CALLING REEBOOTCHECKER ---- END-----------------------*/
			} else {
				Log.i(TAG, "INVALID TOP ACTIVITY OR SCREEN STATE");
			}
		}
	}

	public String convertSecondsToHMmSs(long seconds) {
		// long s = seconds % 60;
		long m = (seconds / 60) % 60;
		long h = (seconds / (60 * 60)) % 24;
		return String.format("%02d:%02d", h, m);
	}

	public void startSession(){

		if (double_click_flag == 0) {
			bluetoothChecking();
			globalClass.isEllipsisEnable = false;
			globalClass.isSupportEnable = false;
			double_click_flag = 1;
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					double_click_flag = 0;

				}
			}, 9000);
			//			globalClass.isRemSessionStart = false;
			SharedPreferences file_data_settings = getApplicationContext().getSharedPreferences("file_data", 0);
			String default_time = "";
			if (Util.is_lg())
				default_time = "1800";
			else
				default_time = "2160";
			int sleep_system_idle_timeout = Integer
					.parseInt(file_data_settings.getString("sleep_system_idle_timeout", default_time)); // defualt
			// as
			// 1800
			// sec
			// ie
			// 30
			// min
			android.provider.Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT,
					(60000 * sleep_system_idle_timeout / 60));

			/*
			 * android.provider.Settings.System.putInt(
			 * getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT,
			 * //(60000 * 4 * 9)); (60000 * 30));
			 */
			start.setEnabled(false);

			clickedhome = 1;
			set_flow(1); // set flow as start click for alarm
			set_brightness();
			// insert_skip_values();

			Flow_db.open();
			Cursor cursor1 = Flow_db.selectAll();
			Status = new String[cursor1.getCount()];
			int j = 0;

			while (cursor1.moveToNext()) {
				Status[j] = cursor1.getString(cursor1.getColumnIndex("Status"));

				Status_Val = Status[0];
				j += 1;
			}
			Flow_db.close();
			insert_skip_values(); // skip only for reminder flow.
			if (Status_Val.equals("0")) {
				Alam_util.cancelAlarm(Titlewindow.this);

				Flow_db.open();
				Flow_db.deleteAll();
				Flow_db.insert("1");
				Flow_db.close();

			}

			SharedPreferences flow = getSharedPreferences(CommonUtilities.USER_FLOW_SP, 0);
			SharedPreferences.Editor editor = flow.edit();
			editor.putInt("flow", 0);
			editor.commit();

			Intent i = new Intent(Titlewindow.this, HFPMainActivity.class);
			startActivity(i);
			overridePendingTransition(0, 0);

		} else {
			Log.e("TitleWindow", "START BUTTON DOUBLE CLICK");
		}
	}



	public void DownloadMydataSensor() {

		if (isInternetPresent) {
			clickedhome = 1;
			SharedPreferences settings = getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);

			PostUrl = settings.getString("Server_post_url", "-1");
			DownloadMyDataSensorTask taskUpdate = new DownloadMyDataSensorTask();
			taskUpdate.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		} else {

			//            Cursor c = myDataSensor_db.SelectMydataSensors();
			//            int size = c.getCount();
			//
			//            if (size > 0) {
			//
			//                Intent myDataIntent = new Intent(getApplicationContext(), MyDataActivity.class);
			//                startActivity(myDataIntent);
			//            } else {

			Toast.makeText(getApplicationContext(), getResources().getString(R.string.internetFailed), Toast.LENGTH_SHORT).show();

			//           }
		}

	}


	private class DownloadMyDataSensorTask extends
	AsyncTask<String, Void, String> {
		SharedPreferences settingswifi = getSharedPreferences(CommonUtilities.WiFi_SP, 0);

		String imeilNo = settingswifi.getString("imei_no", "");

		@Override
		protected void onPreExecute() {
			Log.i("onPreExecute", "onPreExecute MainActivity");

		}

		@Override
		protected void onPostExecute(String result) {


			Cursor c = myDataSensor_db.SelectMydataSensors();
			int size = c.getCount();

			if (size > 0) {

				Intent myDataIntent = new Intent(getApplicationContext(), MyDataActivity.class);
				startActivity(myDataIntent);
			} else {

				Toast.makeText(getApplicationContext(), getResources().getString(R.string.noVitals), Toast.LENGTH_SHORT).show();

			}

		}

		protected String doInBackground(String... urls) {

			String response = "";
			try {
				//sensor_db.deleteplayorder();//
				SharedPreferences tokenPreference = getSharedPreferences(CommonUtilities.USER_SP, 0);
				String token = tokenPreference.getString("Token_ID", "-1");
				String patientId = tokenPreference.getString("patient_id", "-1");
				HttpResponse response1 = Util.connect(PostUrl
						+ "/droid_website/rtd_sensor_details_mydata.ashx", new String[]{
								"imei_no", "patient_id"}, new String[]{imeilNo,
										patientId});

				if (response1 == null) {


					return ""; // process
				}
				// Log.e("Optum_Sierra","serialNo"+serialNo);

				String str = Util
						.readStream(response1.getEntity().getContent());
				if (str.trim().length() == 0) {

					//sensor_db.delete();
					return "";
				}
				str = str.replace("&", "");
				//Log.i("response:", "sensor_response:" + str);
				DocumentBuilder dbr = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = dbr.parse(is);
				NodeList AF_Nodes = doc.getElementsByTagName("patient_sensor_details");
				for (int i = 0; i < AF_Nodes.getLength(); i++) {
					response = Util.getTagValue(AF_Nodes, i, "mob_response");

				}
				if (response.equals("AuthenticationError")) {

					return response;
				}
				NodeList nodes = doc.getElementsByTagName("sensor");
				myDataSensor_db.delete(); // deleting entire table
				ClassSensor sensor = new ClassSensor();

				for (int i = 0; i < nodes.getLength(); i++) {

					sensor.setSensorId(Integer.parseInt(Util.getTagValue(nodes, i, "sensor_id")));
					sensor.setSensorName(Util.getTagValue(nodes, i, "vital_name"));
					ContentValues values = new ContentValues();
					values.put("Sensor_id", sensor.getSensorId());
					values.put("Vital_name", sensor.getSensorName());
					myDataSensor_db.insertMydataSensor(values);// inserting downloaded date
					response = "Success";
				}
				myDataSensor_db.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			return response;
		}
	}

	private void bluetoothChecking(){
		BluetoothAdapter  mBluetoothAdaptere = BluetoothAdapter.getDefaultAdapter();		
		if (mBluetoothAdaptere == null) {

			Toast.makeText(getBaseContext(), getResources().getString(R.string.bluetoothnotavailable), Toast.LENGTH_LONG).show();
		} else if(!mBluetoothAdaptere.isEnabled()){

			mBluetoothAdaptere.enable();

		}
	}

	//	private class UploadErrorFile extends AsyncTask<String, Void, String> {
	//		File sdcard = Environment.getExternalStorageDirectory();
	//		String URL_UPLOAD = "/droid_website/upload_flat_file.ashx";
	//		File file = new File(sdcard, "/versions/tablet_incident.log");
	//		@Override
	//		protected String doInBackground(String... params) {
	//			try {
	//				Log.i(TAG, "UploadErrorFile");
	//				HttpURLConnection conn = null;
	//				DataOutputStream dos = null;
	//				String lineEnd = "\r\n";
	//				String twoHyphens = "--";
	//				String boundary = "*****";
	//				int bytesRead, bytesAvailable, bufferSize;
	//				byte[] buffer;
	//				int maxBufferSize = 1 * 1024 * 1024;
	//
	//				if (file.isFile()) {
	//
	//					try {
	//
	//						TelephonyManager tManager = (TelephonyManager) getApplicationContext()
	//								.getSystemService(Context.TELEPHONY_SERVICE);
	//						String imeilNo = tManager.getDeviceId().substring(0, 14);
	//
	//						String upLoadServerUri = Constants.getPostUrl() + URL_UPLOAD;
	//
	//						// open a URL connection to the Servlet
	//						FileInputStream fileInputStream = new FileInputStream(file);
	//						URL url = new URL(upLoadServerUri);
	//						Log.i(TAG, "UploadErrorFile--------------    "+upLoadServerUri);
	//						// Open a HTTP connection to the URL
	//						conn = (HttpURLConnection) url.openConnection();
	//						conn.setDoInput(true); // Allow Inputs
	//						conn.setDoOutput(true); // Allow Outputs
	//						conn.setUseCaches(false); // Don't use a Cached Copy
	//						conn.setRequestMethod("POST");
	//						conn.setRequestProperty("Connection", "Keep-Alive");
	//						conn.setRequestProperty("ENCTYPE", "multipart/form-data");
	//						conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
	//						conn.setRequestProperty("bill", file.toString());
	//						conn.setRequestProperty("imei_no", imeilNo);
	//						dos = new DataOutputStream(conn.getOutputStream());
	//
	//						dos.writeBytes(twoHyphens + boundary + lineEnd);
	//						dos.writeBytes("Content-Disposition: form-data; name=\"bill\";filename=\"" + file.toString()
	//								+ "\"" + lineEnd);
	//
	//						dos.writeBytes(lineEnd);
	//
	//						// create a buffer of maximum size
	//						bytesAvailable = fileInputStream.available();
	//
	//						bufferSize = Math.min(bytesAvailable, maxBufferSize);
	//						buffer = new byte[bufferSize];
	//
	//						// read file and write it into form...
	//						bytesRead = fileInputStream.read(buffer, 0, bufferSize);
	//
	//						while (bytesRead > 0) {
	//
	//							dos.write(buffer, 0, bufferSize);
	//							bytesAvailable = fileInputStream.available();
	//							bufferSize = Math.min(bytesAvailable, maxBufferSize);
	//							bytesRead = fileInputStream.read(buffer, 0, bufferSize);
	//						}
	//
	//						// send multipart form data necesssary after file
	//						// data...
	//						dos.writeBytes(lineEnd);
	//						dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
	//						Log.i(TAG, "UploadErrorFile------    "+"serverResponseCode");
	//						// Responses from the server (code and message)
	//						int serverResponseCode = conn.getResponseCode();
	//						String serverResponseMessage = conn.getResponseMessage();
	//						Log.i(TAG, "UploadErrorFile---------------    "+"serverResponseCode"+serverResponseMessage);
	//						fileInputStream.close();
	//						Log.i(TAG, "serverResponseMessage:=>" + serverResponseMessage);
	//						dos.flush();
	//						dos.close();
	//						if (serverResponseCode == 200) {
	//							return "success";
	//						} else {
	//							return "Failed";
	//						}
	//					} catch (Exception e) {
	//						Log.i(TAG, "UploadErrorFile------    "+"Exception");
	//						e.printStackTrace();
	//					}
	//				} // End else block
	//			} catch (Exception ex) {
	//				ex.printStackTrace();
	//				String Error = this.getClass().getName() + " : " + ex.getMessage();
	//				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(), "Error File Upload Error", Error);
	//				OnInit_Data_Access.insert_incident_detail(Error, Constants.After_the_tablet_starts_up);
	//			}
	//			return "Executed";
	//		}
	//
	//		@Override
	//		protected void onPostExecute(String result) {
	////			Log.i(TAG, "#onPostExecute result:" + result);
	//			if (result.equals("success")) {
	//				file.delete();
	//			}
	//		}
	//
	//		@Override
	//		protected void onPreExecute() {
	//			Log.i(TAG, "#onPreExecute");
	//		}
	//	}

	
	private void resetSessionTime(){

        SharedPreferences settings_n = getSharedPreferences("activity_name", 0);
        String className = settings_n.getString("class_name", "0");

        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        String mClassName = cn.getClassName();

        if(!className.equals(mClassName)){
        	globalClass.setRegular_alarm_count(0);
        	globalClass.setStart_Alarm_Count(0);

        }
        SharedPreferences settings = getSharedPreferences("activity_name", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("class_name", mClassName);
        editor.commit();

    }
}
