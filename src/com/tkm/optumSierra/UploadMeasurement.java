package com.tkm.optumSierra;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.dal.Glucose_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.dal.Temperature_db;
import com.tkm.optumSierra.dal.Weight_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Util;

public class UploadMeasurement extends Activity {

	private Weight_db dbweight = new Weight_db(this);
	private Glucose_db dbglucose = new Glucose_db(this);
	private Temperature_db dbtemparature = new Temperature_db(this);
	private String TAG = "UploadMeasurment Sierra";
	Sensor_db dbSensor = new Sensor_db(this);
	String[] measure_date = new String[] {};
	private String serverurl = "";
	String patient_id, item;
	int[] blood_oxygen, pulse, Measurement_Id;
	int type = 0;
	private GlobalClass appClass;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_measurement);
		appClass = (GlobalClass) getApplicationContext();
		// SetPublicParamAcitvity publicVariable= new SetPublicParamAcitvity();
		// publicVariable.SetURLParams(this); ##
		setConfig();

		// clear retak sp
		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.RETAKE_SP, 0);
		SharedPreferences.Editor editor_retake = settings1.edit();
		editor_retake.putInt("retake_status", 0);
		editor_retake.commit();

		Log.i(TAG, "UpdateMeasurementtoServer started...");
		int redirectionType = 0;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			appClass.setBundle(extras);
		} else {
			extras = appClass.getBundle();
		}

		final Context context = this;
		UpdateMeasurementtoServer();
		Log.i(TAG, "Upload measuremant ...");

		try {

			SharedPreferences settings = getSharedPreferences(
					CommonUtilities.USER_FLOW_SP, 0);
			int val = settings.getInt("flow", 0); // #1

			if (val == 1) {

				Intent intentSc = new Intent(getApplicationContext(),
						Home.class);
				finish();
				startActivity(intentSc);
				overridePendingTransition(0, 0);
				Log.i(TAG, "Redirecting to home page ...");

			}

			else if (preference()) {
				Intent intentSc = new Intent(getApplicationContext(),
						ScheduleRedirect.class);
				finish();
				startActivity(intentSc);
				overridePendingTransition(0, 0);
			} else {
				redirectionType = extras.getInt("reType");
				if (redirectionType == 1) {

					String question = dbSensor.SelectQuestion();
					Log.i(TAG, "check question ..." + question);

					if (question.trim().length() > 0) {

						Intent intent = new Intent(context, StartQuestion.class);
						finish();
						startActivity(intent);
						overridePendingTransition(0, 0);
						Log.i(TAG, "Redirecting to start question page ...");
					} else {
						Intent intent = new Intent(context, Questions.class);
						finish();
						startActivity(intent);
						overridePendingTransition(0, 0);
					}
					/*
					 * String tmpMac= dbsensor.SelectQuestion();
					 * if(tmpMac.equals("-1")) {
					 * 
					 * Intent intent = new Intent(context,StartQuestion.class);
					 * startActivity(intent); } else { Intent intent = new
					 * Intent(context,Questions.class); startActivity(intent); }
					 */
				} else if (redirectionType == 2) {
					// exit
				} else if (redirectionType == 3) {
					Intent intent = new Intent(context, ThanksActivity.class);
					finish();
					startActivity(intent);
					overridePendingTransition(0, 0);
				}

			}

		} catch (Exception e) {
			// e.printStackTrace();
		}

	}

	public Boolean preference() {
		Boolean flag = null;
		try {

			String PREFS_NAME = "DroidPrefSc";
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			int scheduletatus = settings.getInt("Scheduletatus", -1);
			Log.i("Droid", " SharedPreferences : " + getApplicationContext()
					+ " is : " + scheduletatus);
			if (scheduletatus == 1)
				flag = true;
			else
				flag = false;
		}

		catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		return flag;
	}

	public void UpdateMeasurementtoServer() {

		Update taskUpdate = new Update();
		taskUpdate.execute(new String[] { serverurl
				+ "/droid_website/handler.ashx" });

	}

	private class Update extends AsyncTask<String, Void, String> {

		protected String doInBackground(String... urls) {

			String response = "";
			Log.i(TAG, "uploading started.");
			/***************** Oxgen ********************/

			/*
			 * try {
			 * 
			 * 
			 * Cursor c = dbcreate1.SelectMeasuredData(); measure_date = new
			 * String[c.getCount()]; blood_oxygen = new int[c.getCount()]; pulse
			 * = new int[c.getCount()]; Measurement_Id = new int[c.getCount()];
			 * 
			 * c.moveToFirst(); if (c.getCount() > 0) { int i = 0; while
			 * (c.isAfterLast() == false) { Log.i(TAG,
			 * "Uploading oxygen measure data to server started ...");
			 * InputStream content = null; HttpClient client = new
			 * DefaultHttpClient(); HttpPost post = new HttpPost(serverurl +
			 * "/droid_website/add_tpm_blood_oxygen.ashx"); List<NameValuePair>
			 * pairs = new ArrayList<NameValuePair>();
			 * 
			 * post.setEntity(new UrlEncodedFormEntity(pairs));
			 * 
			 * post.setHeader("patient_id", c.getString(5));
			 * post.setHeader("measure_date", c.getString(3));
			 * post.setHeader("blood_oxygen", c.getString(1));
			 * post.setHeader("pulse", c.getString(2));
			 * 
			 * HttpResponse response1 = client.execute(post); int a =
			 * response1.getStatusLine().getStatusCode(); InputStream in =
			 * response1.getEntity().getContent(); BufferedReader reader = new
			 * BufferedReader( new InputStreamReader(in)); StringBuilder str =
			 * new StringBuilder(); String line = null; while ((line =
			 * reader.readLine()) != null) { str.append(line + "\n"); }
			 * in.close(); str.toString(); Log.i(TAG,
			 * "Response from server Oxgen upload ..." + str.toString()); if
			 * (str.length() > 0) { DocumentBuilder db = DocumentBuilderFactory
			 * .newInstance().newDocumentBuilder(); InputSource is1 = new
			 * InputSource(); is1.setCharacterStream(new StringReader(str
			 * .toString())); Document doc = db.parse(is1); NodeList nodes = doc
			 * .getElementsByTagName("success_response"); if (nodes.getLength()
			 * == 1) { // /// if success delete the value from the // tablet db
			 * Log.i(TAG, "Update Measure data with id ..." + c.getString(0));
			 * dbcreate1.UpdateMeasureData(Integer.parseInt(c .getString(0))); }
			 * 
			 * } c.moveToNext();
			 * 
			 * } } c.close(); dbcreate1.cursorMeasurement.close();
			 * dbcreate1.close();
			 * 
			 * } catch (Exception e) { // e.printStackTrace(); }
			 */
			/********************************* weight **************************/

			try {
				Cursor cweight = dbweight.SelectMeasuredweight();
				cweight.moveToFirst();
				if (cweight.getCount() > 0) {

					while (cweight.isAfterLast() == false) {
						Log.i(TAG,
								"Uploading weight data to server started ...");

						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(serverurl
								+ "/droid_website/add_tpm_body_weight.ashx");
						List<NameValuePair> pairs = new ArrayList<NameValuePair>();

						post.setEntity(new UrlEncodedFormEntity(pairs));

						post.setHeader("patient_id", cweight.getString(2));
						post.setHeader("measure_date", cweight.getString(1)
								.replace("\n", ""));
						post.setHeader("kilogram", cweight.getString(3)
								.replace("\r", ""));
						post.setHeader("kind", "1");
						post.setHeader("fat_percent", "0.0");
						post.setHeader("input_mode", cweight.getString(5));
						post.setHeader("section_date", cweight.getString(7));
						post.setHeader("is_reminder", cweight.getString(8));
						Log.i(TAG, "wt measuredate ..."
								+ cweight.getString(1).replace("\n", ""));

						HttpResponse response1 = client.execute(post);
						int a = response1.getStatusLine().getStatusCode();
						InputStream in = response1.getEntity().getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(in));
						StringBuilder str = new StringBuilder();
						String line = null;
						while ((line = reader.readLine()) != null) {
							str.append(line + "\n");
						}
						in.close();
						str.toString();
						if (str.length() > 0) {
							DocumentBuilder db = DocumentBuilderFactory
									.newInstance().newDocumentBuilder();
							InputSource is1 = new InputSource();
							is1.setCharacterStream(new StringReader(str
									.toString()));
							Document doc = db.parse(is1);
							NodeList nodes = doc
									.getElementsByTagName("success_response");
							if (nodes.getLength() == 1) {
								// /// if success delete the value from the
								// tablet db
//								if(appClass.isRemSessionStart){
//
//									appClass.uploadSessionStatus("success");
//								}else{

									appClass.uploadStatus("success");
//								}
								Log.i(TAG, "Update weight data with id ..."
										+ cweight.getString(0));
								dbweight.UpdateMeasureData(Integer
										.parseInt(cweight.getString(0)), UploadMeasurement.this);
							}else{
								
//								if(appClass.isRemSessionStart){
//
//									appClass.uploadSessionStatus("failed");
//								}else{
									
									appClass.uploadStatus("failed");
//								}
							}

						}else{
							
//							if(appClass.isRemSessionStart){
//
//								appClass.uploadSessionStatus("failed");
//							}else{
								
								appClass.uploadStatus("failed");
//							}
						}
						cweight.moveToNext();
					}
				}
				cweight.close();
				dbweight.cursorWeight.close();
				dbweight.closeOpenWTdbConnection();

			} catch (Exception e) {
//				if(appClass.isRemSessionStart){
//
//					appClass.uploadSessionStatus("failed");
//				}else{
					
					appClass.uploadStatus("failed");
//				}
				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Weight Data Upload Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.Weight_failure_upload);
			}

			/********************************* Pressure **************************/

			/*
			 * try {
			 * 
			 * Cursor cpressure = dbpressure.SelectMeasuredpressure();
			 * cpressure.moveToFirst(); if (cpressure.getCount() > 0) {
			 * 
			 * while (cpressure.isAfterLast() == false) { Log.i(TAG,
			 * "Uploading pressure data to server started ...");
			 * 
			 * HttpClient client = new DefaultHttpClient(); HttpPost post = new
			 * HttpPost(serverurl +
			 * "/droid_website/add_tpm_blood_pressure.ashx");
			 * List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			 * 
			 * post.setEntity(new UrlEncodedFormEntity(pairs));
			 * 
			 * post.setHeader("patient_id", cpressure.getString(1));
			 * post.setHeader("measure_date", cpressure.getString(5));
			 * post.setHeader("diastolic", cpressure.getString(3));
			 * post.setHeader("systolic", cpressure.getString(2));
			 * post.setHeader("pulse", cpressure.getString(4));
			 * 
			 * HttpResponse response1 = client.execute(post); int a =
			 * response1.getStatusLine().getStatusCode(); InputStream in =
			 * response1.getEntity().getContent(); BufferedReader reader = new
			 * BufferedReader( new InputStreamReader(in)); StringBuilder str =
			 * new StringBuilder(); String line = null; while ((line =
			 * reader.readLine()) != null) { str.append(line + "\n"); }
			 * in.close(); str.toString(); Log.i(TAG,
			 * "Response from server  Pressure..." + str.toString()); if
			 * (str.length() > 0) { DocumentBuilder db = DocumentBuilderFactory
			 * .newInstance().newDocumentBuilder(); InputSource is1 = new
			 * InputSource(); is1.setCharacterStream(new StringReader(str
			 * .toString())); Document doc = db.parse(is1); NodeList nodes = doc
			 * .getElementsByTagName("success_response"); if (nodes.getLength()
			 * == 1) { // /// if success delete the value from the // tablet db
			 * Log.i(TAG, "Update Pressure data with id ..." +
			 * cpressure.getString(0)); dbpressure.UpdatepressureData(Integer
			 * .parseInt(cpressure.getString(0))); }
			 * 
			 * } cpressure.moveToNext(); } } cpressure.close();
			 * dbpressure.cursorPreesure.close(); dbpressure.close();
			 * 
			 * } catch (Exception e) { // e.printStackTrace(); }
			 */
			try {

				/********************************* Glucose **************************/
				Cursor cglucose = dbglucose.SelectMeasuredglucose();
				cglucose.moveToFirst();
				if (cglucose.getCount() > 0) {

					while (cglucose.isAfterLast() == false) {
						Log.i(TAG,
								"Uploading Glucose data to server started ...");

						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(serverurl
								+ "/droid_website/add_tpm_blood_glucose.ashx");
						List<NameValuePair> pairs = new ArrayList<NameValuePair>();
						post.setEntity(new UrlEncodedFormEntity(pairs));
						post.setHeader("patient_id", cglucose.getString(4));
						post.setHeader("measure_date", cglucose.getString(2));
						post.setHeader("milligram", cglucose.getString(1));
						post.setHeader("input_mode", cglucose.getString(5));
						post.setHeader("section_date", cglucose.getString(6));
						post.setHeader("prompt_flag", cglucose.getString(8));
						post.setHeader("is_reminder", cglucose.getString(9));
						
						Log.i(TAG,
								"Uploading gc is_reminder>>>"+cglucose.getString(9));
						
						
						HttpResponse response1 = client.execute(post);
						int a = response1.getStatusLine().getStatusCode();
						InputStream in = response1.getEntity().getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(in));
						StringBuilder str = new StringBuilder();
						String line = null;
						while ((line = reader.readLine()) != null) {
							str.append(line + "\n");
						}
						Log.i(TAG,
								" glucose data with id ..."
										+ cglucose.getString(0));
						in.close();
						str.toString();
						if (str.length() > 0) {
							DocumentBuilder db = DocumentBuilderFactory
									.newInstance().newDocumentBuilder();
							InputSource is1 = new InputSource();
							is1.setCharacterStream(new StringReader(str
									.toString()));
							Document doc = db.parse(is1);
							NodeList nodes = doc
									.getElementsByTagName("success_response");
							if (nodes.getLength() == 1) {
								// /// if success delete the value from the
								// tablet db
//								if(appClass.isRemSessionStart){
//
//									appClass.uploadSessionStatus("success");
//								}else{

									appClass.uploadStatus("success");
//								}
								Log.e(TAG, "----------------Update glucose id ..."
										+ cglucose.getString(0));
								dbglucose.UpdateglucoseData(Integer
										.parseInt(cglucose.getString(0)), UploadMeasurement.this);
							
							}else{
								
//								if(appClass.isRemSessionStart){
//
//									appClass.uploadSessionStatus("failed");
//								}else{
									
									appClass.uploadStatus("failed");
//								}
							}
						}else{
							
//							if(appClass.isRemSessionStart){
//
//								appClass.uploadSessionStatus("failed");
//							}else{
								
								appClass.uploadStatus("failed");
//							}
						}
						cglucose.moveToNext();
					}
					// FOR DELETING UPLOADED DATA CALL THIS
					dbglucose.deleteUploadedGlucose(UploadMeasurement.this);
				}
				dbglucose.cursorMeasurement.close();
				dbglucose.close();

			} catch (Exception e) {
//				if(appClass.isRemSessionStart){
//
//					appClass.uploadSessionStatus("failed");
//				}else{
					
					appClass.uploadStatus("failed");
//				}
				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Glucose Data Upload Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.BG_failure_upload);
			}

			/********************************* Temperature **************************/
			try {

				Cursor ctemperature = dbtemparature.SelectMeasuredTemperature();
				ctemperature.moveToFirst();
				if (ctemperature.getCount() > 0) {

					while (ctemperature.isAfterLast() == false) {
						Log.i(TAG,
								"Uploading temparature data to server started ...");

						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(
								serverurl
										+ "/droid_website/add_tpm_body_temperature.ashx");
						List<NameValuePair> pairs = new ArrayList<NameValuePair>();
						post.setEntity(new UrlEncodedFormEntity(pairs));
						post.setHeader("patient_id", ctemperature.getString(4));
						post.setHeader("measure_date",
								ctemperature.getString(2));
						post.setHeader("fahrenheit", ctemperature.getString(1));
						post.setHeader("input_mode", ctemperature.getString(5));
						post.setHeader("section_date",
								ctemperature.getString(6));
						post.setHeader("is_reminder", ctemperature.getString(8));
						HttpResponse response1 = client.execute(post);
						int a = response1.getStatusLine().getStatusCode();
						InputStream in = response1.getEntity().getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(in));
						StringBuilder str = new StringBuilder();
						String line = null;
						while ((line = reader.readLine()) != null) {
							str.append(line + "\n");
						}

						in.close();
						str.toString();
						if (str.length() > 0) {
							DocumentBuilder db = DocumentBuilderFactory
									.newInstance().newDocumentBuilder();
							InputSource is1 = new InputSource();
							is1.setCharacterStream(new StringReader(str
									.toString()));
							Document doc = db.parse(is1);
							NodeList nodes = doc
									.getElementsByTagName("success_response");
							if (nodes.getLength() == 1) {
								// /// if success delete the value from the
								// tablet db
//								if(appClass.isRemSessionStart){
//
//									appClass.uploadSessionStatus("success");
//								}else{

									appClass.uploadStatus("success");
//								}
								Log.i(TAG,
										"Update temperature data with id ..."
												+ ctemperature.getString(0));
								dbtemparature.UpdatetemparatureData(Integer
										.parseInt(ctemperature.getString(0)), UploadMeasurement.this);
							}else{
								
//								if(appClass.isRemSessionStart){
//
//									appClass.uploadSessionStatus("failed");
//								}else{
									
									appClass.uploadStatus("failed");
//								}
							}
						} else {
							
//							if(appClass.isRemSessionStart){
//
//								appClass.uploadSessionStatus("failed");
//							}else{
								
								appClass.uploadStatus("failed");
//							}
						}
						ctemperature.moveToNext();
					}
				}
				dbtemparature.cursorTemperature.close();
				dbtemparature.close();

			} catch (Exception e) {
//				if(appClass.isRemSessionStart){
//
//					appClass.uploadSessionStatus("failed");
//				}else{
					
					appClass.uploadStatus("failed");
//				}
				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Temperature Data Upload Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.Temp_failure_upload);
			}
			// DownloadAdviceMessage();
			return response;
		}
	}

	@Override
	public void onStop() {

		Util.WriteLog(TAG, patient_id + "");
		super.onStop();
	}

	private void setConfig() {

		{

			SharedPreferences settings = getSharedPreferences(
					CommonUtilities.SERVER_URL_SP, 0);

			serverurl = settings.getString("Server_post_url", "-1");

			CommonUtilities.SERVER_URL = serverurl;

		}
	}
}