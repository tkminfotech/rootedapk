package com.tkm.optumSierra;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.tkm.optumSierra.bean.ClassWeight;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.dal.Weight_db;
import com.tkm.optumSierra.service.AdviceService;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.CustomKeyboard;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ActionBar.LayoutParams;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.DisplayMetrics;
import com.tkm.optumSierra.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class WeightEntry extends Titlewindow {

	private static String TAG = "WeightEntry";
	CustomKeyboard mCustomKeyboard;
	int measureId;
	TextView weightText, weightUnit, weightMessage;
	Sensor_db dbSensor = new Sensor_db(this);
	Dialog dialog;
	int unitval = 0;
	private View pview_back;
private GlobalClass appClass;
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// switch (msg.what) {

			Weight_db dbcreateweight = new Weight_db(WeightEntry.this);

			EditText edtpulse = (EditText) findViewById(R.id.weightvalue);
			if ((edtpulse.getText().toString()).length() != 0
					&& !(edtpulse.getText().toString()).equals("0")
					&& !(edtpulse.getText().toString()).equals("00")
					&& !(edtpulse.getText().toString()).equals("000")
					&& !(edtpulse.getText().toString()).equals(".0")
					&& !(edtpulse.getText().toString()).equals(".")
					&& (edtpulse.getText().toString()).length() >= 1
					&& !(Util.round(Double.parseDouble(edtpulse.getText()
							.toString())) > 999)
					&& !(edtpulse.getText().toString()).equals(".00")) {
				// tts.speak("your weight is "+edtpulse.getText().toString()
				// +"pounds", TextToSpeech.QUEUE_FLUSH, null);
				int Patientid = Constants.getdroidPatientid();
				ClassWeight weight = new ClassWeight();
				weight.setPatientID(Patientid);

				double weightKG = 0.0;

				if (unitval == 0) {

					weightKG = Util.round(Double.parseDouble(edtpulse.getText()
							.toString()));
				} else {
					
					weightKG = Util.round(Double.parseDouble(edtpulse.getText()
							.toString()) * 2.204);
				}
				SharedPreferences flow = getSharedPreferences(
						CommonUtilities.USER_FLOW_SP, 0);
				int val = flow.getInt("flow", 0); // #1
				if (val == 1) {					
					SharedPreferences section = getSharedPreferences(
							CommonUtilities.PREFS_NAME_date, 0);
					SharedPreferences.Editor editor_retake = section.edit();
					editor_retake.putString("sectiondate",  Util.get_patient_time_zone_time(WeightEntry.this));
					
					editor_retake.commit();
				}
				weight.setWeightInKg(weightKG + "");
				weight.setInputmode(1);
				SharedPreferences settings = getSharedPreferences(
						CommonUtilities.USER_TIMESLOT_SP, 0);
				String slot = settings.getString("timeslot", "AM");
				weight.setTimeslot(slot);

				SharedPreferences settings1 = getSharedPreferences(
						CommonUtilities.PREFS_NAME_date, 0);
				String sectiondate = settings1.getString("sectiondate", "0");

				weight.setSectionDate(sectiondate);
				Log.e(TAG, "Weightmanual section date-"+ sectiondate);
				
				measureId = dbcreateweight.InsertWeight(weight);
				Log.e(TAG, "Weight value saved");
				/*
				 * Toast.makeText(getApplicationContext(), "Saved Successfully",
				 * Toast.LENGTH_SHORT).show();
				 */
				// edtpulse.setText("");

				try {
					// Thread.sleep(7000);

					Intent intent = new Intent(WeightEntry.this,
							ShowWightActivity.class);
					intent.putExtra(
							"text",
							""
									+ Util.round(Double.parseDouble(edtpulse
											.getText().toString())));
					intent.putExtra("measureId", measureId);
					intent.putExtra("type", 1);

					edtpulse.setText("");
					Log.e(TAG, "Redirecting top show weight page");
					Util.stopPlaySingle();
					startActivity(intent);
					finish();
					overridePendingTransition(0, 0);

					/*
					 * Intent intent = new Intent(WeightEntry.this,
					 * UploadMeasurement.class); intent.putExtra("reType", 1);
					 * 
					 * startActivity(intent);
					 */
				} catch (Exception e) {

					Log.e(TAG, "Exception in delay!" + e.getMessage());

				}

			} else {

				invalid_popup();
				return;
			}

			// insert function

			// }
		}
	};

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_weight_entry);
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		pview_back = inflater.inflate(R.layout.activity_weight_entry, null);
		appClass  = (GlobalClass)getApplicationContext();
		appClass.isEllipsisEnable = true;
		appClass.isSupportEnable = true;
		mCustomKeyboard = new CustomKeyboard(this, R.id.keyboardview,
				R.xml.hexkbd, mHandler);
		weightText = (TextView) findViewById(R.id.weight);
		weightMessage = (TextView) findViewById(R.id.weightmessage);
		weightUnit = (TextView) findViewById(R.id.weightunit);
		try {
			String unit = dbSensor.measureunitName();
			if (unit.contains("Kilogram")) {
				weightUnit.setText(" kg");
				unitval = 1;
			}

			
			Typeface type1 = Typeface.createFromAsset(getAssets(),
					"fonts/FrutigerLTStd-Roman.otf");
			weightText.setTypeface(type1, Typeface.NORMAL);
			weightMessage.setTypeface(type1, Typeface.NORMAL);
			Typeface type2 = Typeface.createFromAsset(getAssets(),
					"fonts/FrutigerLTStd-Light.otf");
			weightUnit.setTypeface(type2, Typeface.NORMAL);

			mCustomKeyboard.registerEditText(R.id.weightvalue);			

			

				/*if (!isMyAdviceServiceRunning()) {
					Intent schedule = new Intent();
					schedule.setClass(getApplicationContext(),
							AdviceService.class);
					startService(schedule); // Starting Advice Service
				}*/
			
			SharedPreferences settingsUserSp = getSharedPreferences(CommonUtilities.USER_SP, 0);
			String languageId = settingsUserSp.getString("language_id", "0");
			if(languageId.equals("11")){
				
				Util.soundPlay("Messages/CheckWeightEnter_1.wav");
			}else{
				
				Util.soundPlay("Messages/CheckWeightEnter.wav");
			}
			
			Log.i(TAG,
					"-------------------oncreate--------------  weight entry  activity");
		} catch (Exception e) {
			// Log.e("Droid Error",e.getMessage());
			Log.i(TAG, " Exception  wt entry on create ");
		}

	}

	@Override
	public void onBackPressed() {
		// NOTE Trap the back key: when the CustomKeyboard is still visible hide
		// it, only when it is invisible, finish activity
		if (mCustomKeyboard.isCustomKeyboardVisible())
			mCustomKeyboard.hideCustomKeyboard();
		else
			this.finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			Log.i(TAG, "DROID KEYCODE_BACK clicked:");

			Intent intent = new Intent(this, FinalMainActivity.class);
			finish();
			startActivity(intent);

		}
		return super.onKeyDown(keyCode, event);
	}

	private boolean isMyAdviceServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AdviceService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private void popup() {

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		dialog = adb.setView(new View(this)).create();
		// (That new View is just there to have something inside the dialog that
		// can grow big enough to cover the whole screen.)

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = 790;
		lp.height = 500;
		dialog = new Dialog(this, R.style.CustomDialogTheme);
		dialog.setContentView(R.layout.popup_messages);

		TextView msg = (TextView) dialog.findViewById(R.id.textpopup);
		TextView mac = (TextView) dialog.findViewById(R.id.txtmac);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);
		msg.setText(R.string.entervalidweight);
		Log.e(TAG, "invalid weight measurement");
		mac.setTypeface(type, Typeface.NORMAL);
		mac.setVisibility(View.INVISIBLE);

		dialog.show();
		dialog.getWindow().setAttributes(lp);
		closeversion = (Button) dialog.findViewById(R.id.exitversion);
		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				dialog.dismiss();
			}
		});

	}

	private void invalid_popup() {

		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);

		View pview;
		pview = inflater.inflate(R.layout.popup_messages, null);

		TextView msg = (TextView) pview.findViewById(R.id.textpopup);
		TextView mac = (TextView) pview.findViewById(R.id.txtmac);

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/FrutigerLTStd-Roman.otf");
		msg.setTypeface(type, Typeface.NORMAL);
		msg.setText(this.getString(R.string.entervalidweight));

		mac.setTypeface(type, Typeface.NORMAL);
		mac.setVisibility(View.INVISIBLE);

		closeversion = (Button) pview.findViewById(R.id.exitversion);

		final PopupWindow cp = new PopupWindow(pview,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
		cp.setBackgroundDrawable(new BitmapDrawable(getApplicationContext()
				.getResources()));
		cp.showAtLocation(pview_back, Gravity.CENTER, 0, 0);

		// cp.update(0, 0, 350, 350);
		// cp.update(0, 0, 750, 500);

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = (displaymetrics.heightPixels);
		int width = displaymetrics.widthPixels / 2;
		height = (int) (height / 1.2);

		// cp.update(0, 0, 350, 350);
		// cp.update(0, 0, 750, 500);
		cp.update(0, 0, width, height);

		closeversion.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				cp.dismiss();
			}
		});

	}

	@Override
	public void onStop() {

		Log.i(TAG,
				"-------------------ONSTOP--------------  weight entry  activity");
		Util.WriteLog(TAG, Constants.getdroidPatientid() + "");

		super.onStop();
	}

}
