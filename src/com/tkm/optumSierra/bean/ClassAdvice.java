package com.tkm.optumSierra.bean;



public class ClassAdvice {
	private int adviceId=0;
	private String messageDate=null;
	private int patientId=0;
	private String adviceSubject=null;
	private String adviceText=null;
	private int userId=0;
	private String description=null;
	private int status=0;
	public int getAdviceId() {
		return adviceId;
	}
	public void setAdviceId(int adviceId) {
		this.adviceId = adviceId;
	}
	public String getMessageDate() {
		return messageDate;
	}
	public void setMessageDate(String messageDate) {
		this.messageDate = messageDate;
	}
	public int getPatientId() {
		return patientId;
	}
	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}
	public String getAdviceSubject() {
		return adviceSubject;
	}
	public void setAdviceSubject(String adviceSubject) {
		this.adviceSubject = adviceSubject;
	}
	public String getAdviceText() {
		return adviceText;
	}
	public void setAdviceText(String adviceText) {
		this.adviceText = adviceText;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

}


