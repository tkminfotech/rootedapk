package com.tkm.optumSierra.bean;

public class ClassMeasureType {

	int measureTypeId;
	int patientId, status;
	String date;

	public void setMeasuretypeId(int measureTypeId1) {

		this.measureTypeId = measureTypeId1;

	}

	public int getMeasuretypeId() {

		return measureTypeId;
	}

	public void setPatientId(int patientId1) {

		this.patientId = patientId1;

	}

	public int getPatientId() {

		return patientId;
	}

	public void setStatus(int i) {

		this.status = i;
	}

	public int getStatus() {

		return status;
	}

	public void setDate(String date1) {

		this.date = date1;

	}

	public String getDate() {

		return date;
	}

}
