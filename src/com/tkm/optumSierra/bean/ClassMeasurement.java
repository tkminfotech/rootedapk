package com.tkm.optumSierra.bean;

public class ClassMeasurement {
	private int inputmode;
	private String timeslot = null;
	private String prompt_flag = null;

	public String getPrompt_flag() {
		return prompt_flag;
	}

	public void setPrompt_flag(String prompt_flag) {
		this.prompt_flag = prompt_flag;
	}

	public String getTimeslot() {
		return timeslot;
	}

	public void setTimeslot(String timeslot) {
		this.timeslot = timeslot;
	}
						
	public String getLast_Update_Date() {
		return last_Update_Date;
	}
	public void setLast_Update_Date(String last_Update_Date) {
		this.last_Update_Date = last_Update_Date;
	}
	public int getMeasurement_Id() {
		return measurement_Id;
	}
	public void setMeasurement_Id(int measurement_Id) {
		this.measurement_Id = measurement_Id;
	}
	public int getOxygen_Value() {
		return oxygen_Value;
	}
	public void setOxygen_Value(int oxygen_Value) {
		this.oxygen_Value = oxygen_Value;
	}
	public int getPressure_Read_Id() {
		return pressure_Read_Id;
	}
	public void setPressure_Read_Id(int pressure_Read_Id) {
		this.pressure_Read_Id = pressure_Read_Id;
	}
	public int getPressure_Value() {
		return pressure_Value;
	}
	public void setPressure_Value(int pressure_Value) {
		this.pressure_Value = pressure_Value;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String last_Update_Date,meassure_Date;
	public String getMeassure_Date() {
		return meassure_Date;
	}
	public void setMeassure_Date(String meassure_Date) {
		this.meassure_Date = meassure_Date;
	}
	public int measurement_Id, oxygen_Value, pressure_Read_Id, pressure_Value, status, value,glucose_Id,glucose_Value;
	public int getGlucose_Id() {
		return glucose_Id;
	}
	public void setGlucose_Id(int glucose_Id) {
		this.glucose_Id = glucose_Id;
	}
	public int getGlucose_Value() {
		return glucose_Value;
	}
	public void setGlucose_Value(int glucose_Value) {
		this.glucose_Value = glucose_Value;
	}
	public int getInputmode() {
		return inputmode;
	}
	public void setInputmode(int inputmode) {
		this.inputmode = inputmode;
	}
	public String sectionDate;

	public void setSectionDate(String sectiondate) {

		this.sectionDate = sectiondate;

	}

	public String getSectionDate() {

		return sectionDate;

	}

}
