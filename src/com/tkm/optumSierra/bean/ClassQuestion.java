package com.tkm.optumSierra.bean;

public class ClassQuestion {
	private String timeslot = null;
	private int status = 0;
	public String sectionDate;
	
	public String getTimeslot() {
		return timeslot;
	}

	public void setTimeslot(String timeslot) {
		this.timeslot = timeslot;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public void setSectionDate(String sectiondate) {
		this.sectionDate = sectiondate;
	}

	public String getSectionDate() {
		return sectionDate;
	}

}
