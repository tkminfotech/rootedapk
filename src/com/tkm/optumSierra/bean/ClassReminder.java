package com.tkm.optumSierra.bean;

public class ClassReminder {

	private int patientId;
	private int reminderId;
	private String reminderTime;
	private int duration;
	private int status;
	private String createdDateAndTime;

	public ClassReminder() {
		super();
	}

	public ClassReminder(int patientId, int reminderId, String reminderTime,
			int duration, int status, String createdDateAndTime) {
		super();
		this.patientId = patientId;
		this.reminderId = reminderId;
		this.reminderTime = reminderTime;
		this.duration = duration;
		this.status = status;
		this.createdDateAndTime = createdDateAndTime;
	}

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public int getReminderId() {
		return reminderId;
	}

	public void setReminderId(int reminderId) {
		this.reminderId = reminderId;
	}

	public String getReminderTime() {
		return reminderTime;
	}

	public void setReminderTime(String reminderTime) {
		this.reminderTime = reminderTime;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCreatedDateAndTime() {
		return createdDateAndTime;
	}

	public void setCreatedDateAndTime(String createdDateAndTime) {
		this.createdDateAndTime = createdDateAndTime;
	}

}
