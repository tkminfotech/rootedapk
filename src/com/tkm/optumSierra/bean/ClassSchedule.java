package com.tkm.optumSierra.bean;

import android.text.format.Time;

public class ClassSchedule {
	
	private int patientId, status,measureTypeId,Id,timeout;
	private String scheduleId,downloadDate,lastUpdateDate;
	
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	public String getDownloadDate() {
		return downloadDate;
	}
	public void setDownloadDate(String downloadDate) {
		this.downloadDate = downloadDate;
	}
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	private Time as;
	
	public Time getAs() {
		return as;
	}
	public void setAs(Time as) {
		this.as = as;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	private String scheduleTime;

	public String getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}
	public int getPatientId() {
		return patientId;
	}
	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getMeasureTypeId() {
		return measureTypeId;
	}
	public void setMeasureTypeId(int measureTypeId) {
		this.measureTypeId = measureTypeId;
	}
	public String getScheduleTime() {
		return scheduleTime;
	}
	public void setScheduleTime(String scheduleTime) {
		this.scheduleTime = scheduleTime;
	}
	

}
