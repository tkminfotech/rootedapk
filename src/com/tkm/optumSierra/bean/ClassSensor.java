package com.tkm.optumSierra.bean;


import java.sql.Date;

public class ClassSensor {
	
	private int sensorId, measureTypeId, weightUnitId,patientId,Status,pairedStatus = 0;
	private String pin;
	private Date downloadDate;
	String weightUnitName, macId, measureTypeName, sensorName,item_sn;
	
	public String getItem_sn() {
		return item_sn;
	}
	public void setItem_sn(String item_sn) {
		this.item_sn = item_sn;
	}
	public int getPairedStatus() {
		return pairedStatus;
	}
	public void setPairedStatus(int pairedStatus) {
		this.pairedStatus = pairedStatus;
	}
	public String getMacId() {
		return macId;
	}
	public void setMacId(String macId) {
		this.macId = macId;
	}
	public int getSensorId() {
		return sensorId;
	}
	public void setSensorId(int sensorId) {
		this.sensorId = sensorId;
	}
	public int getMeasureTypeId() {
		return measureTypeId;
	}
	public void setMeasureTypeId(int measureTypeId) {
		this.measureTypeId = measureTypeId;
	}
	public int getWeightUnitId() {
		return weightUnitId;
	}
	public void setWeightUnitId(int weightUnitId) {
		this.weightUnitId = weightUnitId;
	}
	public int getPatientId() {
		return patientId;
	}
	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}
	public int getStatus() {
		return Status;
	}
	public void setStatus(int status) {
		Status = status;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public Date getDownloadDate() {
		return downloadDate;
	}
	public void setDownloadDate(Date downloadDate) {
		this.downloadDate = downloadDate;
	}
	public String getWeightUnitName() {
		return weightUnitName;
	}
	public void setWeightUnitName(String weightUnitName) {
		this.weightUnitName = weightUnitName;
	}
	public String getMeasureTypeName() {
		return measureTypeName;
	}
	public void setMeasureTypeName(String measureTypeName) {
		this.measureTypeName = measureTypeName;
	}
	public String getSensorName() {
		return sensorName;
	}
	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}
	
}
