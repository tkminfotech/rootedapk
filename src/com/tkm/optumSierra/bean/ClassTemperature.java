package com.tkm.optumSierra.bean;

public class ClassTemperature {

	private String temperatureValue = null;
	private int status = 0;
	private int inputmode = 0;
	private String lastUpdateDate = null;
	private String MeassureDate = null;
	public String SectionDate = null;
	private String timeslot = null;
	
	public String getTimeslot() {
		return timeslot;
	}

	public void setTimeslot(String timeslot) {
		this.timeslot = timeslot;
	}
	
	public String getMeassureDate() {
		return MeassureDate;
	}

	public void setMeassureDate(String meassureDate) {
		MeassureDate = meassureDate;
	}

	public String getTemperatureValue() {
		return temperatureValue;
	}

	public void setTemperatureValue(String temperatureValue) {
		this.temperatureValue = temperatureValue;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public int getInputmode() {
		return inputmode;
	}

	public void setInputmode(int inputmode) {
		this.inputmode = inputmode;
	}

	public void setSectionDate(String sectiondate) {

		this.SectionDate = sectiondate;

	}

	public String getSectionDate() {

		return SectionDate;

	}

}
