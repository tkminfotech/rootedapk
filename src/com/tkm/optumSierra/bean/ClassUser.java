package com.tkm.optumSierra.bean;



public class ClassUser {

	private int patientId=0;
	private String fullName=null;
	private String nickName=null;
	private String vsb_code=null;
	private String dbo=null;
	private String sex=null;
	private String type=null;
	private String contractcode=null;
	
	
	public String getVsb_code() {
		return vsb_code;
	}
	public void setVsb_code(String vsb_code) {
		this.vsb_code = vsb_code;
	}
	public int getPatientId() {
		return patientId;
	}
	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	public String getDbo() {
		return dbo;
	}
	public void setDbo(String dbo) {
		this.dbo = dbo;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getContractcode() {
		return contractcode;
	}
	public void setContractcode(String contractcode) {
		this.contractcode = contractcode;
	}
}


