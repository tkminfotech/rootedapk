package com.tkm.optumSierra.bean;

public class ClassWeight {

	private int weightId = 0;
	private String messageDate = null;
	private int patientID = 0;
	private String weightInKg = null;
	private int status = 0;
	private int inputmode = 0;
	private String timeslot = null;
	private String sectionDate = null;

	public String getTimeslot() {
		return timeslot;
	}

	public void setTimeslot(String timeslot) {
		this.timeslot = timeslot;
	}

	public int getWeightId() {
		return weightId;
	}

	public void setWeightId(int weightId) {
		this.weightId = weightId;
	}

	public String getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(String messageDate) {
		this.messageDate = messageDate;
	}

	public int getPatientID() {
		return patientID;
	}

	public void setPatientID(int patientID) {
		this.patientID = patientID;
	}

	public String getWeightInKg() {
		return weightInKg;
	}

	public void setWeightInKg(String weightInKg) {
		this.weightInKg = weightInKg;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getInputmode() {
		return inputmode;
	}

	public void setInputmode(int inputmode) {
		this.inputmode = inputmode;
	}

	public String getSectionDate() {

		return sectionDate;
	}

	public void setSectionDate(String sectiondate) {

		this.sectionDate = sectiondate;

	}

}
