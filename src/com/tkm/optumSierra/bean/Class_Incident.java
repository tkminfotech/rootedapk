package com.tkm.optumSierra.bean;

public class Class_Incident {

	private int incident_id;
	private int incident_type_id;
	private int patient_id;	
	private int signal_strength;
	private int signal_strength_dbm;
	private int signal_strength_asu;
	private int battery_level;
	private int sim_card_status;
	
	private long app_used_bandwidth;
	private long system_up_time;

	private String tablet_imei;
	private String incident_msg;
	private String incident_date;	
	private String app_version_number;	
	private String cell_network_type;
	private String cell_network_provider;	
	private String apn_name;
	private String apn_value;
	private String tablet_os_version;
	private String tablet_os_build_number;	
	private String created_date;

	private Boolean is_power_adapter_on;	
	private Boolean is_sim_card_in;
	private Boolean is_airplane_mode_on;
	private Boolean is_wifi_on;
	private Boolean is_bluetooth_on;


	public int get_incident_id() {
		return incident_id;
	}
	public void set_incident_id(int incident_id) {
		this.incident_id = incident_id;
	}
	public int get_incident_type_id() {
		return incident_type_id;
	}
	public void set_incident_type_id(int incident_type_id) {
		this.incident_type_id = incident_type_id;
	}
	public int get_patient_id() {
		return patient_id;
	}
	public void set_patient_id(int patient_id) {
		this.patient_id = patient_id;
	}
	public String get_tablet_imei() {
		return tablet_imei;
	}
	public void set_tablet_imei(String tablet_imei) {
		this.tablet_imei = tablet_imei;
	}
	public int get_signal_strength() {
		return signal_strength;
	}
	public void set_signal_strength(int signal_strength) {
		this.signal_strength = signal_strength;
	}
	public int get_signal_strength_dbm() {
		return signal_strength_dbm;
	}
	public void set_signal_strength_dbm(int signal_strength_dbm) {
		this.signal_strength_dbm = signal_strength_dbm;
	}
	public int get_signal_strength_asu() {
		return signal_strength_asu;
	}
	public void set_signal_strength_asu(int signal_strength_asu) {
		this.signal_strength_asu = signal_strength_asu;
	}
	public long get_app_used_bandwidth() {
		return app_used_bandwidth;
	}
	public void set_app_used_bandwidth(long app_used_bandwidth) {
		this.app_used_bandwidth = app_used_bandwidth;
	}
	public int get_battery_level() {
		return battery_level;
	}
	public void set_battery_level(int battery_level) {
		this.battery_level = battery_level;
	}
	public String get_incident_date() {
		return incident_date;
	}
	public void set_incident_date(String incident_date) {
		this.incident_date = incident_date;
	}
	public String get_app_version_number() {
		return app_version_number;
	}
	public void set_app_version_number(String app_version_number) {
		this.app_version_number = app_version_number;
	}
	public int get_sim_card_status() {
		return sim_card_status;
	}
	public void set_sim_card_status(int sim_card_status) {
		this.sim_card_status = sim_card_status;
	}
	public String get_cell_network_type() {
		return cell_network_type;
	}
	public void set_cell_network_type(String cell_network_type) {
		this.cell_network_type = cell_network_type;
	}
	public String get_cell_network_provider() {
		return cell_network_provider;
	}
	public void set_cell_network_provider(String cell_network_provider) {
		this.cell_network_provider = cell_network_provider;
	}
	public String get_apn_name() {
		return apn_name;
	}
	public void set_apn_name(String apn_name) {
		this.apn_name = apn_name;
	}
	public String get_apn_value() {
		return apn_value;
	}
	public void set_apn_value(String apn_value) {
		this.apn_value = apn_value;
	}
	public String get_tablet_os_version() {
		return tablet_os_version;
	}
	public void set_tablet_os_version(String tablet_os_version) {
		this.tablet_os_version = tablet_os_version;
	}
	public String get_tablet_os_build_number() {
		return tablet_os_build_number;
	}
	public void set_tablet_os_build_number(String tablet_os_build_number) {
		this.tablet_os_build_number = tablet_os_build_number;
	}
	public long get_system_up_time() {
		return system_up_time;
	}
	public void set_system_up_time(long system_up_time) {
		this.system_up_time = system_up_time;
	}
	public String get_created_date() {
		return created_date;
	}
	public void set_created_date(String created_date) {
		this.created_date = created_date;
	}
	public Boolean get_power_adapter_on() {
		return is_power_adapter_on;
	}
	public void set_power_adapter_on(Boolean is_power_adapter_on) {
		this.is_power_adapter_on = is_power_adapter_on;
	}
	public Boolean get_sim_card_in() {
		return is_sim_card_in;
	}
	public void set_sim_card_in(Boolean is_sim_card_in) {
		this.is_sim_card_in = is_sim_card_in;
	}
	public Boolean get_airplane_mode_on() {
		return is_airplane_mode_on;
	}
	public void set_airplane_mode_on(Boolean is_airplane_mode_on) {
		this.is_airplane_mode_on = is_airplane_mode_on;
	}
	public Boolean get_wifi_on() {
		return is_wifi_on;
	}
	public void set_wifi_on(Boolean is_wifi_on) {
		this.is_wifi_on = is_wifi_on;
	}
	public Boolean get_bluetooth_on() {
		return is_bluetooth_on;
	}
	public void set_bluetooth_on(Boolean is_bluetooth_on) {
		this.is_bluetooth_on = is_bluetooth_on;
	}
	public String get_incident_msg() {
		return incident_msg;
	}
	public void set_incident_msg(String incident_msg) {
		this.incident_msg = incident_msg;
	}	

}
