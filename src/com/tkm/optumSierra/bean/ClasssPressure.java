package com.tkm.optumSierra.bean;

public class ClasssPressure {
	private int pressure_Id, patient_Id, systolic, diastolic, pulse, status,
			inputmode;
	private String measureDate;
	private String timeslot = null;
	private String prompt_flag = null;
	private String body_movment = "";
	private String irregular_pulse = "";
	private String battery_level = "";
	private String measurement_position_flag = "";
	private String cuff_fit_flag = "";

	
	public String getBody_movment() {
		return body_movment;
	}

	public void setBody_movment(String body_movment) {
		this.body_movment = body_movment;
	}

	public String getIrregular_pulse() {
		return irregular_pulse;
	}

	public void setIrregular_pulse(String irregular_pulse) {
		this.irregular_pulse = irregular_pulse;
	}

	public String getBattery_level() {
		return battery_level;
	}

	public void setBattery_level(String battery_level) {
		this.battery_level = battery_level;
	}

	public String getMeasurement_position_flag() {
		return measurement_position_flag;
	}

	public void setMeasurement_position_flag(String measurement_position_flag) {
		this.measurement_position_flag = measurement_position_flag;
	}

	public String getCuff_fit_flag() {
		return cuff_fit_flag;
	}

	public void setCuff_fit_flag(String cuff_fit_flag) {
		this.cuff_fit_flag = cuff_fit_flag;
	}

	public int getPressure_Id() {
		return pressure_Id;
	}

	public void setPressure_Id(int pressure_Id) {
		this.pressure_Id = pressure_Id;
	}

	public int getPatient_Id() {
		return patient_Id;
	}

	public void setPatient_Id(int patient_Id) {
		this.patient_Id = patient_Id;
	}

	public int getSystolic() {
		return systolic;
	}

	public void setSystolic(int systolic) {
		this.systolic = systolic;
	}

	public int getDiastolic() {
		return diastolic;
	}

	public void setDiastolic(int diastolic) {
		this.diastolic = diastolic;
	}

	public int getPulse() {
		return pulse;
	}

	public void setPulse(int pusle) {
		this.pulse = pusle;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMeasureDate() {
		return measureDate;
	}

	public void setMeasureDate(String measureDate) {
		this.measureDate = measureDate;
	}

	public String getPrompt_flag() {
		return prompt_flag;
	}

	public void setPrompt_flag(String prompt_flag) {
		this.prompt_flag = prompt_flag;
	}

	public String getTimeslot() {
		return timeslot;
	}

	public void setTimeslot(String timeslot) {
		this.timeslot = timeslot;
	}

	public int getInputmode() {
		return inputmode;
	}

	public void setInputmode(int inputmode) {
		this.inputmode = inputmode;
	}

	private String sectionDate;

	public void setSectionDate(String sectiondate) {

		this.sectionDate = sectiondate;

	}

	public String getSectionDate() {

		return sectionDate;

	}
}
