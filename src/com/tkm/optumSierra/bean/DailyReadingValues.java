package com.tkm.optumSierra.bean;

/**
 * Created by tkmif-ws011 on 9/21/2016.
 */
public class DailyReadingValues {

    String time = "";
    double value = 0;
    int systolicValue = 0;
    int diastolicValue = 0;
    int pulseValue = 0;
    double oxygenValue = 0;
    String meal = "";

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public double getOxygenValue() {
        return oxygenValue;
    }

    public void setOxygenValue(double oxygenValue) {
        this.oxygenValue = oxygenValue;
    }

    public int getSystolicValue() {
        return systolicValue;
    }

    public void setSystolicValue(int systolicValue) {
        this.systolicValue = systolicValue;
    }

    public int getDiastolicValue() {
        return diastolicValue;
    }

    public void setDiastolicValue(int diastolicValue) {
        this.diastolicValue = diastolicValue;
    }

    public int getPulseValue() {
        return pulseValue;
    }

    public void setPulseValue(int pulseValue) {
        this.pulseValue = pulseValue;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
