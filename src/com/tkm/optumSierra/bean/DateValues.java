package com.tkm.optumSierra.bean;

/**
 * Created by tkmif-ws011 on 9/21/2016.
 */
public class DateValues {

    private String dateValue = "";
    private double dailyAverage = 0.0;
    private double dailySystolicAverage = 0.0;
    private double dailyDiastolicAverage = 0.0;
    private double dailyPulseAverage = 0.0;
    private double dailyOxygenAverage = 0.0;
    private int dataFlag = 0;

    public int getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(int dataFlag) {
        this.dataFlag = dataFlag;
    }

    public double getDailyOxygenAverage() {
        return dailyOxygenAverage;
    }

    public void setDailyOxygenAverage(double dailyOxygenAverage) {
        this.dailyOxygenAverage = dailyOxygenAverage;
    }

    public double getDailySystolicAverage() {
        return dailySystolicAverage;
    }

    public void setDailySystolicAverage(double dailySystolicAverage) {
        this.dailySystolicAverage = dailySystolicAverage;
    }

    public double getDailyDiastolicAverage() {
        return dailyDiastolicAverage;
    }

    public void setDailyDiastolicAverage(double dailyDiastolicAverage) {
        this.dailyDiastolicAverage = dailyDiastolicAverage;
    }

    public double getDailyPulseAverage() {
        return dailyPulseAverage;
    }

    public void setDailyPulseAverage(double dailyPulseAverage) {
        this.dailyPulseAverage = dailyPulseAverage;
    }

    public String getDateValue() {
        return dateValue;
    }

    public void setDateValue(String dateValue) {
        this.dateValue = dateValue;
    }

    public double getDailyAverage() {
        return dailyAverage;
    }

    public void setDailyAverage(double dailyAverage) {
        this.dailyAverage = dailyAverage;
    }
}
