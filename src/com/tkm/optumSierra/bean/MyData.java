package com.tkm.optumSierra.bean;

/**
 * Created by tkmif-ws011 on 9/21/2016.
 */
public class MyData {
    double minValue;
    double maxValue;
    double avgValue;
    double systolicMinValue;
    double systolicMaxValue;
    double systolicAvgValue;
    double diastolicMinValue;
    double diastolicMaxValue;
    double diastolicAvgValue;
    double pulseMinValue;
    double pulseMaxValue;
    double pulseAvgValue;
    double oxygenMinValue;
    double oxygenMaxValue;
    double oxygenAvgValue;
    String unit = "";
    int highestValue = 999;
    int lowestValue = 0;
    
    public int getLowestValue() {
		return lowestValue;
	}

	public void setLowestValue(int lowestValue) {
		this.lowestValue = lowestValue;
	}

	public int getHighestValue() {
        return highestValue;
    }

    public void setHighestValue(int highestValue) {
        this.highestValue = highestValue;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getOxygenMinValue() {
        return oxygenMinValue;
    }

    public void setOxygenMinValue(double oxygenMinValue) {
        this.oxygenMinValue = oxygenMinValue;
    }

    public double getOxygenMaxValue() {
        return oxygenMaxValue;
    }

    public void setOxygenMaxValue(double oxygenMaxValue) {
        this.oxygenMaxValue = oxygenMaxValue;
    }

    public double getOxygenAvgValue() {
        return oxygenAvgValue;
    }

    public void setOxygenAvgValue(double oxygenAvgValue) {
        this.oxygenAvgValue = oxygenAvgValue;
    }

    public double getSystolicMinValue() {
        return systolicMinValue;
    }

    public void setSystolicMinValue(double systolicMinValue) {
        this.systolicMinValue = systolicMinValue;
    }

    public double getSystolicMaxValue() {
        return systolicMaxValue;
    }

    public void setSystolicMaxValue(double systolicMaxValue) {
        this.systolicMaxValue = systolicMaxValue;
    }

    public double getSystolicAvgValue() {
        return systolicAvgValue;
    }

    public void setSystolicAvgValue(double systolicAvgValue) {
        this.systolicAvgValue = systolicAvgValue;
    }

    public double getDiastolicMinValue() {
        return diastolicMinValue;
    }

    public void setDiastolicMinValue(double diastolicMinValue) {
        this.diastolicMinValue = diastolicMinValue;
    }

    public double getDiastolicMaxValue() {
        return diastolicMaxValue;
    }

    public void setDiastolicMaxValue(double diastolicMaxValue) {
        this.diastolicMaxValue = diastolicMaxValue;
    }

    public double getDiastolicAvgValue() {
        return diastolicAvgValue;
    }

    public void setDiastolicAvgValue(double diastolicAvgValue) {
        this.diastolicAvgValue = diastolicAvgValue;
    }

    public double getPulseMinValue() {
        return pulseMinValue;
    }

    public void setPulseMinValue(double pulseMinValue) {
        this.pulseMinValue = pulseMinValue;
    }

    public double getPulseMaxValue() {
        return pulseMaxValue;
    }

    public void setPulseMaxValue(double pulseMaxValue) {
        this.pulseMaxValue = pulseMaxValue;
    }

    public double getPulseAvgValue() {
        return pulseAvgValue;
    }

    public void setPulseAvgValue(double pulseAvgValue) {
        this.pulseAvgValue = pulseAvgValue;
    }

    public double getMinValue() {
        return minValue;
    }

    public void setMinValue(double minValue) {
        this.minValue = minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(double maxValue) {
        this.maxValue = maxValue;
    }

    public double getAvgValue() {
        return avgValue;
    }

    public void setAvgValue(double avgValue) {
        this.avgValue = avgValue;
    }
}
