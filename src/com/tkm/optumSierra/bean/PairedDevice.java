package com.tkm.optumSierra.bean;

public class PairedDevice {
	
	private String sensorName;
	private String macId;
	public String getSensorName() {
		return sensorName;
	}
	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}
	public String getMacId() {
		return macId;
	}
	public void setMacId(String macId) {
		this.macId = macId;
	}
	

}
