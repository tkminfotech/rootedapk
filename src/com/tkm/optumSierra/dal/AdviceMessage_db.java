package com.tkm.optumSierra.dal;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassAdvice;
import com.tkm.optumSierra.util.Constants;

public class AdviceMessage_db extends SQLiteOpenHelper {

	private static final String TAG = "AdviceMessageDb Sierra";
	public static final String dbName = "DroidDB_AdviceMessage.db";
	public static final String msg_Table = "Droid_AdviceMessage1";

	public Cursor cursorAdviceMessage;

	public AdviceMessage_db(Context context) {

		super(context, dbName, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_msg_TABLE = "CREATE TABLE IF NOT EXISTS " + msg_Table
				+ "(" + "Advice_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "MessageDate TEXT," + "Patient_Id int,"
				+ "Advice_Subject  TEXT," + "Advice_Text TEXT,"
				+ "User_Id int," + "Description TEXT" + "," + "Status INTEGER,Id int"
				+ ")";
		db.execSQL(CREATE_msg_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		db.execSQL("DROP TABLE IF EXISTS " + msg_Table);
	}

//	public void Cleartable() {
//		SQLiteDatabase db = this.getWritableDatabase();
//		// db.delete(msg_Table, null, null);
//
//		db.execSQL("DELETE  FROM " + msg_Table + " WHERE Status=0");
//		db.close();
//
//	}
	public void Cleartable() {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.delete(msg_Table, null, null);

		db.execSQL("DELETE FROM "+msg_Table + " WHERE Status=0");
		db.close();

	}
	public void DeleteMessageByIdAfterAcknowldgeSuccess(int Advice_Id) {

		SQLiteDatabase db = this.getWritableDatabase();
		// db.delete(msg_Table, null, null);
		//Status=4 - got the success acknowledgment from server that client show the message
		db.execSQL("UPDATE  " + msg_Table + " Set Status=4  WHERE Id="
				+ Advice_Id);
		// db.execSQL("DELETE  FROM " + msg_Table +" WHERE Advice_Id="+Advice_Id
		// );
		db.close();

	}
	
	public void DeleteMessageByIdAfterAcknowldgeNotSuccess(int Advice_Id) {

		SQLiteDatabase db = this.getWritableDatabase();
		// db.delete(msg_Table, null, null);
		//Status=5 - if there is no internet, then not get acknowledgment from server.
		db.execSQL("UPDATE  " + msg_Table + " Set Status=5  WHERE Id="
				+ Advice_Id);
		// db.execSQL("DELETE  FROM " + msg_Table +" WHERE Advice_Id="+Advice_Id
		// );
		db.close();

	}

//	public void DeleteMessageByUser(int Advice_Id) {
//		SQLiteDatabase db = this.getWritableDatabase();
//		// db.delete(msg_Table, null, null);
//		db.execSQL("UPDATE  " + msg_Table + " Set Status=3  WHERE Advice_Id="
//				+ Advice_Id);
//		// db.execSQL("DELETE  FROM " + msg_Table +" WHERE Advice_Id="+Advice_Id
//		// );
//		db.close();
//	}

	public void Delete(int Advice_Id) {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.delete(msg_Table, null, null);
		// db.execSQL("UPDATE  " + msg_Table
		// +" Set Status=3  WHERE Advice_Id="+Advice_Id);
		db.execSQL("DELETE  FROM " + msg_Table + " WHERE Id="
				+ Advice_Id);
		db.close();
	}

	public void UpdateAdviceMessage(int Advice_Id) {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.delete(msg_Table, null, null);
		db.execSQL("UPDATE  " + msg_Table + " Set Status=1  WHERE Advice_Id="
				+ Advice_Id);
		db.close();
	}

//	public void UpdateAdviceMessageByDate(String Advice_date) {
//		SQLiteDatabase db = this.getWritableDatabase();
//		// db.delete(msg_Table, null, null);
////		db.execSQL("UPDATE  " + msg_Table
////				+ " Set Status=1  WHERE MessageDate='" + Advice_date + "'");
//		SQLiteStatement stmt = db.compileStatement("UPDATE  " + msg_Table
//			+ " Set Status=1  WHERE MessageDate= ?");
//		stmt.bindString(1, Advice_date);
//		stmt.execute();
//		db.close();
//	}

	public void UpdateAdviceMessageById(int Advice_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.delete(msg_Table, null, null);
//		db.execSQL("UPDATE  " + msg_Table
//				+ " Set Status=1  WHERE MessageDate='" + Advice_date + "'");
		SQLiteStatement stmt = db.compileStatement("UPDATE  " + msg_Table
			+ " Set Status=1  WHERE Id= ?");
		stmt.bindLong(1, Advice_id);
		stmt.execute();
		db.close();
	}
	
	public Boolean InsertAdviceMessage(ClassAdvice classAdvice) {
		SQLiteDatabase db = this.getWritableDatabase();

		Boolean flag = false;
		
		String[] columnsToReturn = { "Advice_Id", "MessageDate","Patient_Id", 
				"Advice_Subject","Advice_Text", "User_Id","Description", "Status", "Id" };
		String selection = "Id= ?";
		String[] selectionArgs = { ""+classAdvice.getAdviceId() }; // matched to "?" in selection
		cursorAdviceMessage = db.query(msg_Table, columnsToReturn, selection, selectionArgs, null, null, null);
//		cursorAdviceMessage = db.rawQuery("SELECT * FROM " + msg_Table
//				+ " WHERE  MessageDate='" + classAdvice.getMessageDate()
//				+ "' AND Advice_Subject='" + classAdvice.getAdviceSubject()
//				+ "'", null);
		if (cursorAdviceMessage.getCount() == 0) {
			ContentValues values = new ContentValues();
			values.put("Patient_Id", classAdvice.getPatientId());
			values.put("MessageDate", classAdvice.getMessageDate());
			values.put("User_Id", classAdvice.getUserId());
			values.put("Advice_Subject", classAdvice.getAdviceSubject());
			values.put("Advice_Text", classAdvice.getAdviceText());
			values.put("Description", "good");
			values.put("Status", 0);
			values.put("Id", classAdvice.getAdviceId());
			db.insert(msg_Table, null, values);
			flag = true;
		}

		cursorAdviceMessage.close();
		db.close();
		return flag;

	}

//	public Cursor SelectAdviceMessage() {
//		SQLiteDatabase db = this.getReadableDatabase();
//		int Patientid = Constants.getdroidPatientid();
//		// and Patient_Id="+Patientid
//
//		Log.i(TAG, "Patientid in db" + Patientid);
//		// Cursor c = db.query(msg_Table, null, null, null, null, null, null);
//		cursorAdviceMessage = db.rawQuery("SELECT * FROM " + msg_Table
//				+ " where Status!=3 and Patient_Id=" + Patientid, null);
//
//		return cursorAdviceMessage;
//
//	}	

//	public List<ClassAdvice> getAllAdviceList() {
//		List<ClassAdvice> classAdviceList = new ArrayList<ClassAdvice>();
//		// Select All Query
//
//		int Patientid = Constants.getdroidPatientid();
//		SQLiteDatabase db = this.getReadableDatabase();
//
//		String Query = "SELECT *,case strftime('%m',MessageDate) when '01' then 'January' when '02' then 'Febuary' when '03' then 'March' when '04' then 'April' when '05' then 'May' when '06' then 'June' when '07' then 'July' when '08' then 'August' when '09' then 'September' when '10' then 'October' when '11' then 'November' when '12' then 'December' else ''  end || ' ' || strftime('%d, %Y %H:%M', MessageDate) as temp,case strftime('%H',MessageDate)   when '00' then 'AM' when '12' then 'PM' when '13' then 'PM' when '14' then 'PM' when '15' then 'PM' when '16' then 'PM' when '17' then 'PM' when '18' then 'PM' when '19' then 'PM' when '20' then 'PM' when '21' then 'PM' when '22' then 'PM' when '23' then 'PM'else 'AM' end as temp1 FROM "
//				+ msg_Table + " where Status!=3 and Patient_Id=" + Patientid;
//
//		/*
//		 * Cursor cursor = db.rawQuery(
//		 * "SELECT *,case strftime('%m',MessageDate) when '01' then 'January' when '02' then 'Febuary' when '03' then 'March' when '04' then 'April' when '05' then 'May' when '06' then 'June' when '07' then 'July' when '08' then 'August' when '09' then 'September' when '10' then 'October' when '11' then 'November' when '12' then 'December' else ''  end || '-' || strftime('%d-%Y %H:%M:%S', MessageDate) as temp FROM "
//		 * + msg_Table + " where Status!=3 and Patient_Id=" + Patientid, null);
//		 */
//
//		Cursor cursor = db.rawQuery(Query, null);
//
//		// looping through all rows and adding to list
//		if (cursor.moveToFirst()) {
//			do {
//				ClassAdvice classAdvice = new ClassAdvice();
//
//				classAdvice.setAdviceId(Integer.parseInt(cursor.getString(0)));
//				classAdvice.setAdviceSubject(cursor.getString(3));
//				classAdvice.setAdviceText(cursor.getString(4));
//				classAdvice.setMessageDate(cursor.getString(8) + " "
//						+ cursor.getString(9));
//				// Adding contact to list
//				classAdviceList.add(classAdvice);
//			} while (cursor.moveToNext());
//		}
//		cursor.close();
//		db.close();
//		// return contact list
//		return classAdviceList;
//	}

	public Cursor SelectAdviceMessageForupload() {
		SQLiteDatabase db = this.getReadableDatabase();

		cursorAdviceMessage = db.rawQuery("SELECT * FROM " + msg_Table
				+ " where Status=1 or Status=3 ", null);

		return cursorAdviceMessage;

	}
	
	public Cursor SelectAdviceMessageForDisplay() {
		SQLiteDatabase db = this.getReadableDatabase();

		cursorAdviceMessage = db.rawQuery("SELECT * FROM " + msg_Table
				+ " where Status=0 ", null);

		return cursorAdviceMessage;

	}
}
