package com.tkm.optumSierra.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

public class Flow_Status_db {

	public static final String KEY_STATUS = "Status";
	
	private static final String TAG = "DBAdapter";
	private static final String DATABASE_NAME = "Flow_Status_db.db";

	private static final String DATABASE_TABLE = "Flow_Status_Table";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table Flow_Status_Table (id INTEGER PRIMARY KEY AUTOINCREMENT, Status TEXT);";

	private final Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	public Flow_Status_db(Context ctx) {

		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {

			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			try {
				db.execSQL(DATABASE_CREATE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS Flow_Status_Table");
			onCreate(db);
		}
	}

	// ---open Flow_Status_db---
	public Flow_Status_db open() throws SQLException {

		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---close Flow_Status_db---
	public void close() {

		DBHelper.close();
	}

	// ---insert data into Flow_Status_Table---
	public long insert(String Status) {

		ContentValues initialValues = new ContentValues();
		
		db.delete(DATABASE_TABLE, null, null);
		
		initialValues.put(KEY_STATUS, Status);
		
		return db.insert(DATABASE_TABLE, null, initialValues);
	}

	// ---Delete All Data from table in Flow_Status_Table---
	public void deleteAll() {

		db.delete(DATABASE_TABLE, null, null);
	}
	// ---Select All Data from table in Flow_Status_Table---
	public Cursor selectAll() {
		Cursor cursor = db.query(DATABASE_TABLE, new String[] { "Status" },
				null, null, null, null, null);
		return cursor;
	}

	
	
}