package com.tkm.optumSierra.dal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;
import com.tkm.optumSierra.bean.ClassMeasurement;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

public class Glucose_db extends SQLiteOpenHelper {
	public static final String dbName = "OPTC_Glucose_v2.db";
	public final String glucose_Table = "Droid_Measurement_Glucose";
	public Cursor cursorMeasurement;
	public Cursor repeatCursor;

	public Glucose_db(Context context) {
		super(context, dbName, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_GLUCOSE_NAME_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ glucose_Table + "("
				+ "Glucose_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "GlucoseValue INTEGER," + "LastUpdatedDate TEXT,"
				+ "Status  INTEGER," + "Patient_Id INTEGER,"
				+ " InputMode INTEGER," + " sectionDate TEXT,"
				+ " timeslot TEXT," + "prompt_flag TEXT," + "is_reminder INTEGER DEFAULT 0" + ")";
		db.execSQL(CREATE_GLUCOSE_NAME_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public int InsertGlucoseMeasurement(ClassMeasurement measurement) {
		SQLiteDatabase db = this.getWritableDatabase();
		int glucoseId = -1;

		try {

			ContentValues values = new ContentValues();
			int Patientid = Constants.getdroidPatientid();

			values.put("GlucoseValue", measurement.getGlucose_Value());
			// values.put("PressureValue", measurement.getPressure_Value());
			if (measurement.getInputmode() == 0) {
				values.put("LastUpdatedDate", measurement.getLast_Update_Date());

				repeatCursor = db
						.rawQuery(
								"SELECT * FROM " + glucose_Table
										+ " where LastUpdatedDate='"
										+ measurement.getLast_Update_Date()
										+ "'", null);
			} else {
				repeatCursor = db
						.rawQuery(
								"SELECT * FROM " + glucose_Table
										+ " where LastUpdatedDate='"
										+ measurement.getLast_Update_Date()
										+ "'", null);
				values.put("LastUpdatedDate", TestDate.getCurrentTime());

			}
			values.put("Status", 1);
			values.put("Patient_Id", Patientid);
			values.put("InputMode", measurement.getInputmode());
			values.put("timeslot", measurement.getTimeslot());
			values.put("sectionDate", measurement.getSectionDate());
			values.put("prompt_flag", measurement.getPrompt_flag());
			if (repeatCursor.getCount() > 0) {
				Log.e("Glucose_db", "Repeat Insert Attempt");
				repeatCursor.close();
			} else {
				repeatCursor.close();
				db.insert(glucose_Table, null, values);

				cursorMeasurement = db.rawQuery("SELECT last_insert_rowid()",
						null);

				if (cursorMeasurement.moveToFirst()) {
					do {

						glucoseId = cursorMeasurement.getInt(0);

					} while (cursorMeasurement.moveToNext());
				}
				cursorMeasurement.close();
				db.close();
			}
		} catch (SQLException e) {

		}
		Log.e("TAG", "inserted glucoseId--: " + glucoseId);
		return glucoseId;
	}

	public int InsertGlucoseMeasurement_TD(ClassMeasurement measurement) {
		SQLiteDatabase db = this.getWritableDatabase();
		int glucoseId = 0;

		try {

			ContentValues values = new ContentValues();
			int Patientid = Constants.getdroidPatientid();

			values.put("GlucoseValue", measurement.getGlucose_Value());
			// values.put("PressureValue", measurement.getPressure_Value());
			if (measurement.getInputmode() == 0) {
				values.put("LastUpdatedDate", measurement.getLast_Update_Date());

				repeatCursor = db.rawQuery(
						"SELECT * FROM " + glucose_Table
								+ " where LastUpdatedDate='"
								+ measurement.getLast_Update_Date()
								+ " and Status=0'", null);
			} else {
				repeatCursor = db.rawQuery(
						"SELECT * FROM " + glucose_Table
								+ " where LastUpdatedDate='"
								+ measurement.getLast_Update_Date()
								+ " and Status=0'", null);
				values.put("LastUpdatedDate", TestDate.getCurrentTime());

			}
			values.put("Status", 1);
			values.put("Patient_Id", Patientid);
			values.put("InputMode", measurement.getInputmode());
			values.put("timeslot", measurement.getTimeslot());
			values.put("sectionDate", measurement.getSectionDate());
			values.put("prompt_flag", measurement.getPrompt_flag());
			if (repeatCursor.getCount() > 0) {
				Log.e("Glucose_db", "Repeat Insert Attempt");
				repeatCursor.close();
			} else {
				repeatCursor.close();
				db.insert(glucose_Table, null, values);

				cursorMeasurement = db.rawQuery("SELECT last_insert_rowid()",
						null);

				if (cursorMeasurement.moveToFirst()) {
					do {

						glucoseId = cursorMeasurement.getInt(0);

					} while (cursorMeasurement.moveToNext());
				}
				cursorMeasurement.close();
				db.close();
			}
		} catch (SQLException e) {

		}
		Log.e("TAG", "inserted glucoseId--: " + glucoseId);
		return glucoseId;
	}

	public int InsertGlucoseMeasurement_skip(ClassMeasurement measurement) {
		SQLiteDatabase db = this.getWritableDatabase();
		int glucoseId = 0;

		try {

			ContentValues values = new ContentValues();
			int Patientid = Constants.getdroidPatientid();

			values.put("GlucoseValue", measurement.getGlucose_Value());
			// values.put("PressureValue", measurement.getPressure_Value());
			if (measurement.getInputmode() == 0) {
				values.put("LastUpdatedDate", measurement.getLast_Update_Date());
			} else {
				values.put("LastUpdatedDate", TestDate.getCurrentTime());
			}
			values.put("Status", 0);
			values.put("Patient_Id", Patientid);
			values.put("InputMode", measurement.getInputmode());
			values.put("timeslot", measurement.getTimeslot());
			values.put("sectionDate", measurement.getSectionDate());
			values.put("prompt_flag", measurement.getPrompt_flag());
			values.put("is_reminder", 1);
			db.insert(glucose_Table, null, values);
			cursorMeasurement = db.rawQuery("SELECT last_insert_rowid()", null);

			if (cursorMeasurement.moveToFirst()) {
				do {

					glucoseId = cursorMeasurement.getInt(0);

				} while (cursorMeasurement.moveToNext());
			}
			cursorMeasurement.close();
			db.close();

		} catch (SQLException e) {

		}
		return glucoseId;
	}

	public Cursor ExecGluQuery() {
		SQLiteDatabase db = this.getReadableDatabase();
		int Patientid = Constants.getdroidPatientid();

		cursorMeasurement = db.rawQuery("SELECT * FROM " + glucose_Table
				+ " where Patient_Id=" + Patientid, null);

		// cursorMeasurement = dbMeasuremnt.query(oxygen_Table, null, null,
		// null,null, null, null);
		return cursorMeasurement;
	}

	public Cursor SelectMeasuredglucose() {
		SQLiteDatabase db = this.getReadableDatabase();
		// Cursor c = db.query(msg_Table, null, null, null, null, null, null);
		cursorMeasurement = db.rawQuery("SELECT * FROM " + glucose_Table
				+ " where Status=0 and Patient_Id!=0", null);

		return cursorMeasurement;

	}

	public boolean reboot_Select() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("SELECT * FROM " + glucose_Table
				+ " WHERE Status=0", null);

		if (cursor.getCount() <= 0) {
			cursor.close();
			return false;
		}
		cursor.close();
		return true;
	}

	/*
	 * + "Glucose_Id INTEGER PRIMARY KEY AUTOINCREMENT," +
	 * "GlucoseValue INTEGER," + "LastUpdatedDate TEXT," + "Status  INTEGER,"
	 * +"Patient_Id INTEGER"
	 */
	public void UpdateglucoseData(int glucoseid, Context ctx) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("UPDATE  " + glucose_Table + " Set Status=1"
				+ "  WHERE Glucose_Id=" + glucoseid);
		db.close();

		// FOR DELETING UPLOADED DATA CALL THIS
		//deleteUploadedGlucose(ctx);
	}

	public void UpdateglucoseData_as_valid(int glucoseid) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("UPDATE  " + glucose_Table + " Set Status=0"
				+ "  WHERE Glucose_Id=" + glucoseid);
		db.close();
	}

	public void delete_pulse_data(int id) {

		SQLiteDatabase dbPressure = this.getWritableDatabase();
		dbPressure.execSQL("DELETE  FROM " + glucose_Table
				+ " where Glucose_Id=" + id);
		Log.e("delete_pulse_data", "DELETED ID" + id);
		dbPressure.close();

	}

	public int GetCurrentDateValues(int type, String currentTime,
			Context context) {

		// type = 1 AMPM patient ; 0 = Either AM or PM

		int no = 0;

		@SuppressWarnings("unused")
		String DatefromDB = "", TimeFromDB = "", timeslot = "";

		SQLiteDatabase dbPressure = this.getWritableDatabase();
		int patientidNew = Constants.getdroidPatientid();

		// String s = "SELECT MessageDate,timeslot FROM " + _Table
		String s = "SELECT sectionDate, timeslot FROM "
				+ glucose_Table
				+ " where  Patient_Id="
				+ patientidNew
				+ " and sectionDate !='0' and GlucoseValue != '-101' and timeslot!='AP' order by Glucose_Id DESC LIMIT 1";

		cursorMeasurement = dbPressure.rawQuery(s, null);
		cursorMeasurement.moveToFirst();

		while (cursorMeasurement.isAfterLast() == false) {

			DatefromDB = cursorMeasurement.getString(0).substring(0, 10); // get
																			// the
																			// last
																			// weight
																			// reading
																			// date
			timeslot = cursorMeasurement.getString(1);

			TimeFromDB = cursorMeasurement.getString(0).substring(
					cursorMeasurement.getString(0).length() - 2,
					cursorMeasurement.getString(0).length()); // get the last
																// weight
																// reading time
																// (AM or PM)
			cursorMeasurement.moveToNext();

		}
		cursorMeasurement.close();

		String patient_time = Util.get_patient_time_zone_time(context);
		patient_time = patient_time.substring(0, 10);

		// SimpleDateFormat dateFormat1 = new
		// SimpleDateFormat("MM/dd/yyyy");
		// Date date1 = new Date();
		Log.e("LOg Temperature>>>>", "" + type);
		if (type == 0) // AM or PM
		{

			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) // comparing
																			// current
				// at with last
				// massurement date
				no = 1;
			else
				no = 0;
		} else {
			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) {
				if (currentTime.trim().equals(timeslot)) {
					no = 1;
				} else {
					no = 0;
				}
			} else {
				no = 0;
			}

			/*
			 * if(currentTime.trim().equals(TimeFromDB)) // if AMPM { no=1;
			 * }else { no=0; }
			 */
		}

		dbPressure.close();

		return no;

	}

	// Delete Uploaded Glucose Entry from Database #REBOOT APK
	@SuppressLint("SimpleDateFormat")
	public void deleteUploadedGlucose(Context ctx) {
		try {
			Cursor cur_Selector;
			String sectionDate;

			SQLiteDatabase dbGlucose = this.getWritableDatabase();
			cur_Selector = dbGlucose.rawQuery("SELECT * FROM " + glucose_Table
					+ " where Status!=0", null);
			if (cur_Selector != null) {
				Log.e("deleteUploadedGlucose","################cur_Selector ======= "+cur_Selector.getCount());
				
				cur_Selector.moveToFirst();
				while (cur_Selector.isAfterLast() == false) {
					sectionDate = cur_Selector.getString(6);
					
					final long MILLIS_PER_2_DAY = 48 * 60 * 60 * 1000L;					
					// Session Time					
					try {
						String currentTime = Util
								.get_patient_time_zone_time(ctx);
						
						SimpleDateFormat sDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
						
						java.util.Date inTime = sDateFormat.parse(sectionDate);
						Calendar initialCal = Calendar.getInstance();
						initialCal.setTime(inTime);
						
						// Current Time
						java.util.Date checkTime = sDateFormat.parse(currentTime);
						Calendar currentCal = Calendar.getInstance();
						currentCal.setTime(checkTime);
						
						boolean moreThan2Days = Math.abs(currentCal.getTimeInMillis() - initialCal.getTimeInMillis()) > MILLIS_PER_2_DAY;
						
						Log.i("#deleteUploadedGlucose","moreThan2Day = "+moreThan2Days);
						if(moreThan2Days){
							dbGlucose.execSQL("DELETE FROM " + glucose_Table
									+ " where Status!=0 and sectionDate='"+sectionDate+"'");
						}
						
					} catch (ParseException e) {
						//e.printStackTrace();
					}
					cur_Selector.moveToNext();					
				}
			}
			cur_Selector.close();
			dbGlucose.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}