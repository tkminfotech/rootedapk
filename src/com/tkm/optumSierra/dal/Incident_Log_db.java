package com.tkm.optumSierra.dal;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.Class_Incident;

public class Incident_Log_db extends SQLiteOpenHelper {

	private static final String TAG 					= "Incident_Log_db";
	public static final String db_name 					= "Incident_Log_db.db";
	public static final String incident_table 			= "td_tablet_incident_log";
	public static final String incident_detail 			= "tablet_incident_log_detail";
	public static final String LABEL_INCIDENT_ID 		= "incident_id";
	public static final String LABEL_INCIDENT_TYPE_ID 	= "incident_type_id";
	public static final String LABEL_INCIDENT_MSG 		= "incident_msg";
	public static final String LABEL_INCIDENT_DATE 		= "incident_date";
	public static final String LABEL_PATIENT_ID			= "patient_id";
	public static final String LABEL_IMEI 				= "tablet_imei";
	public static final String LABEL_APP_VERSION 		= "app_version_number";
	public static final String LABEL_USED_BANDWIDTH		= "app_used_bandwidth";	
	public static final String LABEL_SIMCARD_IN			= "is_sim_card_in";	
	public static final String LABEL_SIMCARD_STATUS		= "sim_card_status";
	public static final String LABEL_NETWORK_TYPE		= "cell_network_type";
	public static final String LABEL_NETWORK_PROVIDER	= "cell_network_provider";
	public static final String LABEL_SIGNAL_STRENGTH	= "signal_strength";
	public static final String LABEL_SIGNAL_STRENGTH_DBM = "signal_strength_dbm";
	public static final String LABEL_SIGNAL_STRENGTH_ASU = "signal_strength_asu";
	public static final String LABEL_APN_NAME			= "apn_name";
	public static final String LABEL_APN_VALUE			= "apn_value";
	public static final String LABEL_OS_VERSION			= "tablet_os_version";
	public static final String LABEL_BUILD_NUMBER		= "tablet_os_build_number";
	public static final String LABEL_SYSTEM_UP_TIME		= "system_up_time";	
	public static final String LABEL_POWER_ADAPTOR		= "is_power_adapter_on";
	public static final String LABEL_BATTERY_LEVEL		= "battery_level";
	public static final String LABEL_AIRPLANE_MODE		= "is_airplane_mode_on";
	public static final String LABEL_WIFI				= "is_wifi_on";
	public static final String LABEL_BLUTOOTH			= "is_bluetooth_on";
	public static final String LABEL_CREATE_DATE		= "created_date";

	public Cursor cursor_incident;

	public Incident_Log_db(Context context) {

		super(context, db_name, null, 1);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

		String CREATE_INCIDENT_TABLE="CREATE TABLE IF NOT EXISTS td_tablet_incident_log" +
				"( incident_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"incident_type_id INTEGER,	" +
				"incident_date TEXT, " +
				"incident_msg TEXT, " +
				"patient_id INTEGER, " +
				"tablet_imei TEXT, " +
				"app_version_number TEXT, " +
				"app_used_bandwidth INTEGER, " +
				"is_sim_card_in TEXT, " +
				"sim_card_status INTEGER, " +
				"cell_network_type TEXT, " +
				"cell_network_provider TEXT, " +
				"signal_strength INTEGER, " +
				"signal_strength_dbm INTEGER, " +
				"signal_strength_asu INTEGER, " +
				"apn_name TEXT, " +
				"apn_value TEXT, " +
				"tablet_os_version TEXT, " +
				"tablet_os_build_number TEXT, " +
				"system_up_time INTEGER, " +
				"is_power_adapter_on TEXT, " +
				"battery_level INTEGER, " +
				"is_airplane_mode_on TEXT, " +
				"is_wifi_on TEXT, " +
				"is_bluetooth_on TEXT, " +			
				"created_date TEXT )" ;

		db.execSQL(CREATE_INCIDENT_TABLE);

	}


	public void insert_incident_log(Class_Incident class_incident) {


		//Cleartable(); // # for testing

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		//values.put(LABEL_INCIDENT_ID		, 		class_incident.get_incident_id());
		values.put(LABEL_INCIDENT_TYPE_ID	, 		class_incident.get_incident_type_id());
		values.put(LABEL_INCIDENT_MSG 		,		class_incident.get_incident_msg());
		values.put(LABEL_INCIDENT_DATE 		,		class_incident.get_incident_date());
		values.put(LABEL_PATIENT_ID			,		class_incident.get_patient_id());
		values.put(LABEL_IMEI 				,		class_incident.get_tablet_imei());
		values.put(LABEL_APP_VERSION 		,		class_incident.get_app_version_number());
		values.put(LABEL_USED_BANDWIDTH		,		class_incident.get_app_used_bandwidth());
		values.put(LABEL_SIMCARD_IN			,		class_incident.get_sim_card_in());
		values.put(LABEL_SIMCARD_STATUS		,		class_incident.get_sim_card_status());
		values.put(LABEL_NETWORK_TYPE		,		class_incident.get_cell_network_type());
		values.put(LABEL_NETWORK_PROVIDER	,		class_incident.get_cell_network_provider());
		values.put(LABEL_SIGNAL_STRENGTH	,		class_incident.get_signal_strength());
		values.put(LABEL_SIGNAL_STRENGTH_DBM ,		class_incident.get_signal_strength_dbm());
		values.put(LABEL_SIGNAL_STRENGTH_ASU ,		class_incident.get_signal_strength_asu());
		values.put(LABEL_APN_NAME			,		class_incident.get_apn_name());
		values.put(LABEL_APN_VALUE			,		class_incident.get_apn_value());
		values.put(LABEL_OS_VERSION			,		class_incident.get_tablet_os_version());
		values.put(LABEL_BUILD_NUMBER		,		class_incident.get_tablet_os_build_number());
		values.put(LABEL_SYSTEM_UP_TIME		,		class_incident.get_system_up_time());
		values.put(LABEL_POWER_ADAPTOR		,		class_incident.get_power_adapter_on());
		values.put(LABEL_BATTERY_LEVEL		,		class_incident.get_battery_level());
		values.put(LABEL_AIRPLANE_MODE		,		class_incident.get_airplane_mode_on());
		values.put(LABEL_WIFI				,		class_incident.get_wifi_on());
		values.put(LABEL_BLUTOOTH			,		class_incident.get_bluetooth_on());
		values.put(LABEL_CREATE_DATE		,		class_incident.get_created_date());

		db.insert(incident_table, null, values);

		Log.i(TAG, "Incident Inserted..");

		db.close();

	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		// TODO Auto-generated method stub
		super.onDowngrade(db, oldVersion, newVersion);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + incident_table);
	}

	public void Cleartable() {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.delete(msg_Table, null, null);

		db.execSQL("DELETE FROM " + incident_table);
		db.close();

	}

	public List<Class_Incident> get_incident_details() {
		List<Class_Incident> class_incident_list = new ArrayList<Class_Incident>();
		SQLiteDatabase db = this.getReadableDatabase();
		String Query = "SELECT * from "+incident_table;
		Cursor cursor = db.rawQuery(Query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				Class_Incident class_incident = new Class_Incident();	
				class_incident.set_incident_type_id(Integer.parseInt(cursor.getString(1)));
				class_incident.set_incident_date(cursor.getString(2));
				class_incident.set_incident_msg(cursor.getString(3));
				class_incident.set_patient_id(Integer.parseInt(cursor.getString(4)));
				class_incident.set_tablet_imei(cursor.getString(5));
				class_incident.set_app_version_number(cursor.getString(6));
				class_incident.set_app_used_bandwidth(cursor.getLong(7));
				if(cursor.getInt(8) == 1)
					class_incident.set_sim_card_in(true);
				else
					class_incident.set_sim_card_in(false);
				//class_incident.set_sim_card_in(Boolean.parseBoolean(cursor.getString(8)));
				
				class_incident.set_sim_card_status(cursor.getInt(9));
				class_incident.set_cell_network_type(cursor.getString(10));
				class_incident.set_cell_network_provider(cursor.getString(11));
				class_incident.set_signal_strength(Integer.parseInt(cursor.getString(12)));
				class_incident.set_signal_strength_dbm(Integer.parseInt(cursor.getString(13)));
				class_incident.set_signal_strength_asu(Integer.parseInt(cursor.getString(14)));
				class_incident.set_apn_name(cursor.getString(15));
				class_incident.set_apn_value(cursor.getString(16));
				class_incident.set_tablet_os_version(cursor.getString(17));
				class_incident.set_tablet_os_build_number(cursor.getString(18));			
				class_incident.set_system_up_time(Long.parseLong(cursor.getString(19)));
				if(cursor.getInt(20) == 1)
					class_incident.set_power_adapter_on(true);
				else
					class_incident.set_power_adapter_on(false);
				//class_incident.set_power_adapter_on(Boolean.parseBoolean(cursor.getString(20)));
				
				class_incident.set_battery_level(Integer.parseInt(cursor.getString(21)));
				if(cursor.getInt(22) == 1)
					class_incident.set_airplane_mode_on(true);
				else
					class_incident.set_airplane_mode_on(false);
				//class_incident.set_airplane_mode_on(Boolean.parseBoolean(cursor.getString(22)));
				
				if(cursor.getInt(23) == 1)
					class_incident.set_wifi_on(true);
				else
					class_incident.set_wifi_on(false);
				//class_incident.set_wifi_on(Boolean.parseBoolean(cursor.getString(23)));
				
				if(cursor.getInt (24) == 1)
					class_incident.set_bluetooth_on(true);
				else
					class_incident.set_bluetooth_on(false);
				//class_incident.set_bluetooth_on(Boolean.parseBoolean(cursor.getString(24)));
				
				class_incident.set_created_date(cursor.getString(25));

				class_incident_list.add(class_incident);

			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();	

		// return advice list			
		return class_incident_list;
	}

}
