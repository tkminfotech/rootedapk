package com.tkm.optumSierra.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassUser;

public class Login_db extends SQLiteOpenHelper {
	public static final String dbName = "HFP_login_merge1.db";
	public final String Settings_Table = "HFP_ConfigSettings";
	public final String Patient_Table = "HFP_PatientDetails";
	private String TAG = "loginDB HFP";
	public Cursor cursorLogin;

	/*************** Database Helper ***************/

	public Login_db(Context context) {
		super(context, dbName, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_Patient_Table = "CREATE TABLE IF NOT EXISTS "
				+ Patient_Table + "(" + "Patient_id INTEGER PRIMARY KEY," + ""
				+ "Full_name TEXT," + "Nick_name TEXT," + "Vsb_password TEXT"
				+ "," + "Dob TEXT" + "," + " Sex TEXT" + "," + "type TEXT"
				+ ")";

		db.execSQL(CREATE_Patient_Table);

		String CREATE_Settings_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ Settings_Table + "(" + "Patient_id INTEGER ,"
				+ "Username TEXT," + "Password TEXT," + "Server TEXT" + ","
				+ "Port TEXT" + "," + "SerialNo TEXT" + ")";

		db.execSQL(CREATE_Settings_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + Patient_Table);
	}

	public void delete_patient() {
		try {

			SQLiteDatabase db = this.getWritableDatabase();
			db.execSQL("DELETE  FROM " + Patient_Table + " WHERE Patient_id!=0");
			db.close();
		} catch (Exception ex) {
			Log.i(TAG, ex.getMessage());
		}
	}

	public void insert_Patient_Data(ClassUser classUser) {
		try {

			SQLiteDatabase db = this.getWritableDatabase();
			db.execSQL("DELETE  FROM " + Patient_Table + " WHERE Patient_id!=0");

			ContentValues values_Settings = new ContentValues();
			values_Settings.put("Patient_id", classUser.getPatientId());
			values_Settings.put("Full_name", classUser.getFullName());
			values_Settings.put("Nick_name", classUser.getNickName());
			values_Settings.put("Vsb_password", classUser.getVsb_code());
			values_Settings.put("Dob", classUser.getDbo());
			values_Settings.put("Sex", classUser.getSex());
			values_Settings.put("type", classUser.getType());

			db.insert(Patient_Table, null, values_Settings);
			db.close();
		} catch (Exception ex) {
			Log.i(TAG, ex.getMessage());
		}

	}

	public Cursor ExecQuery() {

		SQLiteDatabase db = this.getReadableDatabase();
		cursorLogin = db.query(Settings_Table, null, null, null, null, null,
				null);
		return cursorLogin;

	}

	public Cursor UserIsPresent(String Table_name) {
		SQLiteDatabase db = this.getReadableDatabase();
		cursorLogin = db.query(Table_name, null, null, null, null, null, null);
		return cursorLogin;

	}

	public int checkUserLogin(String Table_name, String Contidion) {

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor login = db.rawQuery("SELECT * FROM " + Table_name + " where"
				+ Contidion, null);
		int count = login.getCount();
		login.close();
		db.close();

		return count;

	}

	public ClassUser SelectNickName() {
		SQLiteDatabase db = this.getReadableDatabase();

		cursorLogin = db.rawQuery("SELECT  *  FROM " + Patient_Table, null);
		ClassUser classuser = new ClassUser();
		if (cursorLogin.getCount() > 0) {

			if (cursorLogin.moveToFirst()) {
				do {
					classuser.setNickName(cursorLogin.getString(cursorLogin
							.getColumnIndex("Nick_name")));

					classuser.setFullName(cursorLogin.getString(cursorLogin
							.getColumnIndex("Full_name")));
					classuser
							.setPatientId(Integer.parseInt(cursorLogin
									.getString(cursorLogin
											.getColumnIndex("Patient_id"))));

				} while (cursorLogin.moveToNext());
			}

		}

		cursorLogin.close();
		db.close();
		return classuser;

	}

}
