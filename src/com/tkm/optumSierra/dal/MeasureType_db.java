package com.tkm.optumSierra.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.tkm.optumSierra.bean.ClassMeasureType;

public class MeasureType_db extends SQLiteOpenHelper {

	public static final String dbName = "_Sensor1.db";
	public final String sensor_Table = "Droid_sensor_details";
	public Cursor cursorSensor;
	Cursor cursorMeasurement;

	public MeasureType_db(Context context) {
		super(context, dbName, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_SENSOR_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ sensor_Table + "(" + "Id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "patientId INTEGER," + "measuretypeId INTEGER,"
				+ "Status  INTEGER," + "Date_sensor TEXT" + ")";
		db.execSQL(CREATE_SENSOR_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	public void insert(ClassMeasureType sensor1) {
		SQLiteDatabase db = this.getWritableDatabase();

		try {

			db.execSQL("DELETE  FROM " + sensor_Table
					+ " WHERE measuretypeId='" + sensor1.getMeasuretypeId()
					+ "'"); // delete the duplicate measure type schedule
			ContentValues values = new ContentValues();
			values.put("patientId", sensor1.getPatientId());
			values.put("measuretypeId", sensor1.getMeasuretypeId());
			values.put("Status", sensor1.getStatus());
			values.put("Date_sensor", sensor1.getDate());
			db.insert(sensor_Table, null, values);
			db.close();

		} catch (SQLException e) {

		}

	}

	public Cursor Select_skipped_vitals() {

		SQLiteDatabase db = this.getReadableDatabase();

		cursorMeasurement = db.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Status=1 ", null);

		// cursorMeasurement = dbMeasuremnt.query(oxygen_Table, null, null,
		// null,null, null, null);
		return cursorMeasurement;
	}

	public Cursor Select_vitals() {

		SQLiteDatabase db = this.getReadableDatabase();

		cursorMeasurement = db.rawQuery("SELECT * FROM " + sensor_Table, null);

		// cursorMeasurement = dbMeasuremnt.query(oxygen_Table, null, null,
		// null,null, null, null);
		return cursorMeasurement;
	}

	public void delete() {

		SQLiteDatabase dbSensor = this.getWritableDatabase();
		dbSensor.execSQL("DELETE  FROM " + sensor_Table);

		dbSensor.close();

	}

	public void updateStatusforAll(int i) {

		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("UPDATE  " + sensor_Table + " Set Status=1");
		db.close();

	}

	public void updateStatusToZero() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("UPDATE  " + sensor_Table + " Set Status=0");
		db.close();
	}

	public void updateStatus(int id) {

		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("UPDATE  " + sensor_Table + " Set Status=0"
				+ "  WHERE measuretypeId= " + id);

		db.close();
	}

}
