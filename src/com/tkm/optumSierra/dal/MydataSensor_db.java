package com.tkm.optumSierra.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassSensor;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by tkmif-ws011 on 10/6/2016.
 */
public class MydataSensor_db extends SQLiteOpenHelper {

    public static final String dbName = "Mydata_Sensor.db";
    public static final String myDataSensor_Table = "Droid_MydataSensorDetails";

    private static final String TAG = "SensorDb Sierra";
    public Cursor cursorsensor;


    public MydataSensor_db(Context context) {
        super(context, dbName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_MydataSensor_TABLE = "CREATE TABLE IF NOT EXISTS "
                + myDataSensor_Table + "("
                + "Sensor_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "Vital_name  TEXT" + ")";
        db.execSQL(CREATE_MydataSensor_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
    }

     public void delete() {
        Log.i(TAG, "Deleted sucesfully");
        SQLiteDatabase db_sensor = this.getWritableDatabase();
        db_sensor.delete(myDataSensor_Table, null, null);
        db_sensor.close();

    }



    public void insertMydataSensor(ContentValues values) {
        SQLiteDatabase db_sensor = this.getWritableDatabase();
        db_sensor.insert(myDataSensor_Table, null, values);
        db_sensor.close();
        // Log.i(TAG, "inserted succesfully");
    }


    public Cursor SelectMydataSensors() {
        SQLiteDatabase db_sensor = this.getReadableDatabase();
        cursorsensor = db_sensor.rawQuery("SELECT * FROM " + myDataSensor_Table, null);

        return cursorsensor;
    }



}
