package com.tkm.optumSierra.dal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClasssPressure;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

public class Pressure_db extends SQLiteOpenHelper {

	public static final String dbName = "OPH_Pressure.db";
	public static final String dmp_Table = "tpm_blood_pressure";
	public Cursor cursorPreesure;

	public Pressure_db(Context context) {
		super(context, dbName, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase dbPressure) {

		/*
		 * String CREATE_pressure_TABLE =
		 * "CREATE TABLE IF NOT EXISTS "+dmp_Table+"(PressureId " +
		 * "INTEGER PRIMARY KEY AUTOINCREMENT,PatientId INTEGER,Systolic INTEGER,Diastolic INTEGER"
		 * + "Pulse INTEGER,MeasureDate TEXT,Status INTEGER)";
		 */

		String CREATE_pressure_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ dmp_Table + "("
				+ "PressureId INTEGER PRIMARY KEY AUTOINCREMENT," + ""
				+ "PatientId INTEGER," + "Systolic INTEGER,"
				+ "Diastolic INTEGER" + "," + "Pulse INTEGER" + ","
				+ "MeasureDate TEXT" + "," + "Status INTEGER,"
				+ "InputMode INTEGER," + " sectionDate TEXT,"
				+ "timeslot TEXT," + "prompt_flag TEXT," 
				+ "body_movment TEXT," + "irregular_pulse TEXT," 
				+ "battery_level TEXT," + "measurement_position_flag TEXT," 
				+ "cuff_fit_flag TEXT,"  + "is_reminder INTEGER DEFAULT 0" + ")";

		dbPressure.execSQL(CREATE_pressure_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

	@SuppressLint("SimpleDateFormat")
	public int InsertPressure(ClasssPressure pressure) {
		SQLiteDatabase dbPressure = this.getWritableDatabase();

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"MM/dd/yyyy hh:mm:ss aa");
		String currentDateandTime = dateFormat.format(new Date());

		ContentValues values = new ContentValues();
		values.put("PatientId", pressure.getPatient_Id());
		values.put("Systolic", pressure.getSystolic());
		values.put("Diastolic", pressure.getDiastolic());
		values.put("Pulse", pressure.getPulse());
		values.put("MeasureDate", TestDate.getCurrentTime());
		values.put("InputMode", pressure.getInputmode());
		values.put("timeslot", pressure.getTimeslot());
		values.put("sectionDate", pressure.getSectionDate());
		values.put("prompt_flag", pressure.getPrompt_flag());
		values.put("Status", 1);
		values.put("body_movment", pressure.getBody_movment());
		values.put("irregular_pulse", pressure.getIrregular_pulse());
		values.put("battery_level", pressure.getBattery_level());
		values.put("measurement_position_flag", pressure.getMeasurement_position_flag());
		values.put("cuff_fit_flag", pressure.getCuff_fit_flag());

		Log.i(dmp_Table,
				"values inserted:-" + pressure.getPatient_Id()
						+ pressure.getSystolic() + pressure.getDiastolic()
						+ pressure.getPulse() + currentDateandTime);

		// dbPressure.insert("ConfigurationSettings", null, values_Settings);

		dbPressure.insert(dmp_Table, null, values);
		int pressure_id = 0;
		cursorPreesure = dbPressure
				.rawQuery("SELECT last_insert_rowid()", null);

		if (cursorPreesure.moveToFirst()) {
			do {

				pressure_id = cursorPreesure.getInt(0);

			} while (cursorPreesure.moveToNext());
		}
		cursorPreesure.close();

		dbPressure.close();

		return pressure_id;

	}

	@SuppressLint("SimpleDateFormat")
	public int InsertPressure_skip(ClasssPressure pressure) {
		SQLiteDatabase dbPressure = this.getWritableDatabase();

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"MM/dd/yyyy hh:mm:ss aa");
		String currentDateandTime = dateFormat.format(new Date());

		ContentValues values = new ContentValues();
		values.put("PatientId", pressure.getPatient_Id());
		values.put("Systolic", pressure.getSystolic());
		values.put("Diastolic", pressure.getDiastolic());
		values.put("Pulse", pressure.getPulse());
		values.put("MeasureDate", TestDate.getCurrentTime());
		values.put("InputMode", pressure.getInputmode());
		values.put("timeslot", pressure.getTimeslot());
		values.put("sectionDate", pressure.getSectionDate());
		values.put("prompt_flag", pressure.getPrompt_flag());
		values.put("Status", 0);
		values.put("body_movment", pressure.getBody_movment());
		values.put("irregular_pulse", pressure.getIrregular_pulse());
		values.put("battery_level", pressure.getBattery_level());
		values.put("measurement_position_flag", pressure.getMeasurement_position_flag());
		values.put("cuff_fit_flag", pressure.getCuff_fit_flag());
		values.put("is_reminder", 1);

		Log.i(dmp_Table,
				"values inserted:-" + pressure.getPatient_Id()
						+ pressure.getSystolic() + pressure.getDiastolic()
						+ pressure.getPulse() + currentDateandTime);

		// dbPressure.insert("ConfigurationSettings", null, values_Settings);

		dbPressure.insert(dmp_Table, null, values);
		int pressure_id = 0;
		cursorPreesure = dbPressure
				.rawQuery("SELECT last_insert_rowid()", null);

		if (cursorPreesure.moveToFirst()) {
			do {

				pressure_id = cursorPreesure.getInt(0);

			} while (cursorPreesure.moveToNext());
		}
		cursorPreesure.close();

		dbPressure.close();

		return pressure_id;

	}

	public Cursor SelectDBValues(String query) {
		SQLiteDatabase dbPressure = this.getReadableDatabase();
		cursorPreesure = dbPressure.rawQuery(query, null);
		return cursorPreesure;
	}

	public Cursor ExecQuery() {
		SQLiteDatabase dbPressure = this.getReadableDatabase();

		int Patientid = Constants.getdroidPatientid();

		cursorPreesure = dbPressure.rawQuery("SELECT * FROM " + dmp_Table
				+ " where PatientId=" + Patientid, null);

		// cursorPreesure = dbPressure.query(dmp_Table, null, null, null,null,
		// null, null);
		return cursorPreesure;
	}

	public Cursor SelectMeasuredpressure() {
		SQLiteDatabase dbPressure = this.getReadableDatabase();
		// Cursor c = db.query(msg_Table, null, null, null, null, null, null);
		cursorPreesure = dbPressure.rawQuery("SELECT * FROM " + dmp_Table
				+ " where Status=0", null);

		return cursorPreesure;

	}

	public boolean reboot_Select() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("SELECT * FROM " + dmp_Table
				+ " WHERE Status=0", null);

		if (cursor.getCount() <= 0) {
			cursor.close();
			return false;
		}
		cursor.close();
		return true;
	}

	public void UpdatepressureData(int pressureid, Context ctx) {
		SQLiteDatabase dbPressure = this.getWritableDatabase();
		dbPressure.execSQL("UPDATE  " + dmp_Table + " Set Status=1"
				+ "  WHERE PressureId=" + pressureid);
		dbPressure.close();

		// FOR DELETING UPLOADED DATA CALL THIS
		deleteUploadedPressure(ctx);
	}

	public void delete_pressure_data(int id) {

		SQLiteDatabase dbPressure = this.getWritableDatabase();
		dbPressure.execSQL("DELETE  FROM " + dmp_Table + " where PressureId="
				+ id);

		dbPressure.close();

	}

	public void UpdatepressureData_as_valid(int pressureid) {
		SQLiteDatabase dbPressure = this.getWritableDatabase();
		dbPressure.execSQL("UPDATE  " + dmp_Table + " Set Status=0"
				+ "  WHERE PressureId=" + pressureid);
		dbPressure.close();
	}

	public int GetCurrentDateValues(int type, String currentTime,
			Context context) {

		// type = 1 AMPM patient ; 0 = Either AM or PM

		int no = 0;

		@SuppressWarnings("unused")
		String DatefromDB = "", TimeFromDB = "", timeslot = "";

		SQLiteDatabase dbPressure = this.getWritableDatabase();
		int patientidNew = Constants.getdroidPatientid();

		// String s = "SELECT MessageDate,timeslot FROM " + _Table
		String s = "SELECT sectionDate,timeslot FROM "
				+ dmp_Table
				+ " where  PatientId="
				+ patientidNew
				+ " and sectionDate !='0' and Systolic != '-101' and timeslot!='AP' order by PressureId DESC LIMIT 1";

		cursorPreesure = dbPressure.rawQuery(s, null);
		cursorPreesure.moveToFirst();

		while (cursorPreesure.isAfterLast() == false) {

			DatefromDB = cursorPreesure.getString(0).substring(0, 10); // get
																		// the
																		// last
																		// weight
																		// reading
																		// date
			timeslot = cursorPreesure.getString(1);

			TimeFromDB = cursorPreesure.getString(0).substring(
					cursorPreesure.getString(0).length() - 2,
					cursorPreesure.getString(0).length()); // get the last
															// weight
															// reading time
															// (AM or PM)
			cursorPreesure.moveToNext();

		}
		cursorPreesure.close();

		String patient_time = Util.get_patient_time_zone_time(context);
		patient_time = patient_time.substring(0, 10);

		// SimpleDateFormat dateFormat1 = new
		// SimpleDateFormat("MM/dd/yyyy");
		// Date date1 = new Date();
		Log.e("LOg Pressure>>>>", "" + type);
		if (type == 0) // AM or PM
		{

			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) // comparing
																			// current
				// at with last
				// massurement date
				no = 1;
			else
				no = 0;
		} else {
			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) {
				if (currentTime.trim().equals(timeslot)) {
					no = 1;
				} else {
					no = 0;
				}
			} else {
				no = 0;
			}

			/*
			 * if(currentTime.trim().equals(TimeFromDB)) // if AMPM { no=1;
			 * }else { no=0; }
			 */
		}

		dbPressure.close();

		return no;

	}

	// Delete Uploaded Pressure Entry from Database #REBOOT APK
	@SuppressLint("SimpleDateFormat")
	public void deleteUploadedPressure(Context ctx) {
		try {
			Cursor cur_Selector;
			String sectionDate;

			SQLiteDatabase dbPressure = this.getWritableDatabase();
			cur_Selector = dbPressure.rawQuery("SELECT * FROM " + dmp_Table
					+ " where Status!=0", null);
			if (cur_Selector != null) {
				cur_Selector.moveToFirst();
				while (cur_Selector.isAfterLast() == false) {
					sectionDate = cur_Selector.getString(8);
					Log.i("#deleteUploadedPressure", "sectionDate = "
							+ sectionDate);

					final long MILLIS_PER_2_DAY = 48 * 60 * 60 * 1000L;
					// Session Time
					try {
						String currentTime = Util
								.get_patient_time_zone_time(ctx);

						SimpleDateFormat sDateFormat = new SimpleDateFormat(
								"MM/dd/yyyy hh:mm:ss aa");

						java.util.Date inTime = sDateFormat.parse(sectionDate);
						Calendar initialCal = Calendar.getInstance();
						initialCal.setTime(inTime);

						// Current Time
						java.util.Date checkTime = sDateFormat
								.parse(currentTime);
						Calendar currentCal = Calendar.getInstance();
						currentCal.setTime(checkTime);

						boolean moreThan2Days = Math.abs(currentCal
								.getTimeInMillis()
								- initialCal.getTimeInMillis()) > MILLIS_PER_2_DAY;

						Log.i("#deleteUploadedPressure", "moreThan2Day = "
								+ moreThan2Days);
						if (moreThan2Days) {
							dbPressure.execSQL("DELETE FROM " + dmp_Table
									+ " where Status!=0 and sectionDate='"
									+ sectionDate + "'");
						}

					} catch (ParseException e) {
						//e.printStackTrace();
					}
					cur_Selector.moveToNext();
				}
			}
			cur_Selector.close();
			dbPressure.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}