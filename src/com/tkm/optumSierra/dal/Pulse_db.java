package com.tkm.optumSierra.dal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassMeasurement;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

public class Pulse_db extends SQLiteOpenHelper {

	public static final String dbName = "OTH_Pulse.db";
	public final String oxygen_Table = "tpm_bloodOxygen";
	public final String pressure_Table = "tpm_pressure";

	public Cursor cursorMeasurement;

	public Pulse_db(Context context) {
		super(context, dbName, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_OXYGEN_NAME_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ oxygen_Table + "("
				+ "Measurement_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "oxygenValue INTEGER," + "PressureValue INTEGER,"
				+ "LastUpdatedDate TEXT," + "Status  INTEGER,"
				+ "Patient_Id INTEGER," + " InputMode int,"
				+ " sectionDate TEXT," + " timeslot TEXT," + "is_reminder INTEGER DEFAULT 0" + ")";
		db.execSQL(CREATE_OXYGEN_NAME_TABLE);

		/*
		 * CREATE TABLE IF NOT EXISTS oxygen_Table ( Measurement_Id INTEGER
		 * PRIMARY KEY AUTOINCREMENT, oxygenValue intger,PressureValue intger,
		 * LastUpdatedDate TEXT,Status intger,Patient_Id intger)
		 */

		String CREATE_Pressure_NAME_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ pressure_Table + "(" + "Pressure_Reade_Id int,"
				+ "Value int," + "LastUpdatedDate TEXT," + "Status  int" + ")";
		db.execSQL(CREATE_Pressure_NAME_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public int InsertOxymeterMeasurement(ClassMeasurement measurement) {
		SQLiteDatabase db = this.getWritableDatabase();
		int pulse_id = 0;
		try {

			ContentValues values = new ContentValues();
			int Patientid = Constants.getdroidPatientid();

			values.put("oxygenValue", measurement.getOxygen_Value());
			values.put("PressureValue", measurement.getPressure_Value());
			values.put("LastUpdatedDate", TestDate.getCurrentTime());
			values.put("Status", 1);
			values.put("Patient_Id", Patientid);
			values.put("timeslot", measurement.getTimeslot());
			values.put("sectionDate", measurement.getSectionDate());
			values.put("InputMode", measurement.getInputmode());
			db.insert(oxygen_Table, null, values);

			cursorMeasurement = db.rawQuery("SELECT last_insert_rowid()", null);

			if (cursorMeasurement.moveToFirst()) {
				do {

					pulse_id = cursorMeasurement.getInt(0);

				} while (cursorMeasurement.moveToNext());
			}
			cursorMeasurement.close();

			db.close();

		} catch (SQLException e) {

		}
		return pulse_id;

	}

	public int InsertOxymeterMeasurement_skip(ClassMeasurement measurement) {
		SQLiteDatabase db = this.getWritableDatabase();
		int pulse_id = 0;
		try {

			ContentValues values = new ContentValues();
			int Patientid = Constants.getdroidPatientid();

			values.put("oxygenValue", measurement.getOxygen_Value());
			values.put("PressureValue", measurement.getPressure_Value());
			values.put("LastUpdatedDate", TestDate.getCurrentTime());
			values.put("Status", 0);
			values.put("Patient_Id", Patientid);
			values.put("timeslot", measurement.getTimeslot());
			values.put("sectionDate", measurement.getSectionDate());
			values.put("InputMode", measurement.getInputmode());
			values.put("is_reminder", 1);
			db.insert(oxygen_Table, null, values);

			cursorMeasurement = db.rawQuery("SELECT last_insert_rowid()", null);

			if (cursorMeasurement.moveToFirst()) {
				do {

					pulse_id = cursorMeasurement.getInt(0);

				} while (cursorMeasurement.moveToNext());
			}
			cursorMeasurement.close();

			db.close();

		} catch (SQLException e) {

		}
		return pulse_id;

	}

	public void InsertOxymeterMeasurementService(ClassMeasurement measurement) {
		SQLiteDatabase db = this.getWritableDatabase();

		try {

			ContentValues values = new ContentValues();
			int Patientid = Constants.getdroidPatientid();

			values.put("oxygenValue", measurement.getOxygen_Value());
			values.put("PressureValue", measurement.getPressure_Value());
			values.put("LastUpdatedDate", measurement.getMeassure_Date());
			values.put("timeslot", measurement.getTimeslot());
			values.put("Status", measurement.getStatus());
			values.put("Patient_Id", Patientid);
			db.insert(oxygen_Table, null, values);
			db.close();

		} catch (SQLException e) {

		}
	}

	public void InsertPressureMeasurement(ClassMeasurement measurement) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put("Pressure_Reade_Id", measurement.getPressure_Read_Id());
		values.put("Value", measurement.getValue());
		values.put("LastUpdatedDate", measurement.last_Update_Date);
		values.put("Status", measurement.getStatus());

		db.insert(pressure_Table, null, values);
		db.close();

	}

	public Cursor ExecQuery() {
		SQLiteDatabase db = this.getReadableDatabase();
		int Patientid = Constants.getdroidPatientid();

		cursorMeasurement = db.rawQuery("SELECT * FROM " + oxygen_Table
				+ " where Patient_Id=" + Patientid, null);

		// cursorMeasurement = dbMeasuremnt.query(oxygen_Table, null, null,
		// null,null, null, null);
		return cursorMeasurement;
	}

	public void Deletemeasurement() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(oxygen_Table, null, null);
		db.close();
	}

	public Cursor SelectMeasuredData() {
		SQLiteDatabase db = this.getReadableDatabase();
		// Cursor c = db.query(msg_Table, null, null, null, null, null, null);
		cursorMeasurement = db.rawQuery("SELECT * FROM " + oxygen_Table
				+ " where Status=0", null);

		return cursorMeasurement;

	}

	public boolean reboot_Select() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("SELECT * FROM " + oxygen_Table
				+ " WHERE Status=0", null);

		if (cursor.getCount() <= 0) {
			cursor.close();
			return false;
		}
		cursor.close();
		return true;
	}

	public void UpdateMeasureData(int Measurement_Id, Context ctx) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("UPDATE  " + oxygen_Table + " Set Status=1"
				+ "  WHERE Measurement_Id=" + Measurement_Id);
		db.close();

		// FOR DELETING UPLOADED DATA CALL THIS
		deleteUploadedPulse(ctx);
	}

	public void UpdateMeasureData_as_valid(int Measurement_Id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("UPDATE  " + oxygen_Table + " Set Status=0"
				+ "  WHERE Measurement_Id=" + Measurement_Id);
		db.close();

	}

	public void delete_pulse_data(int id) {
		SQLiteDatabase dbPulse = this.getWritableDatabase();
		dbPulse.execSQL("DELETE  FROM " + oxygen_Table
				+ " where Measurement_Id=" + id);

		dbPulse.close();

	}

	public int GetCurrentDateValues(int type, String currentTime,
			Context context) {

		// type = 1 AMPM patient ; 0 = Either AM or PM

		int no = 0;

		@SuppressWarnings("unused")
		String DatefromDB = "", TimeFromDB = "", timeslot = "";

		SQLiteDatabase dbPulse = this.getWritableDatabase();
		int patientidNew = Constants.getdroidPatientid();

		// String s = "SELECT MessageDate,timeslot FROM " + _Table
		String s = "SELECT sectionDate, timeslot FROM "
				+ oxygen_Table
				+ " where  Patient_Id="
				+ patientidNew
				+ " and sectionDate !='0' and oxygenValue != '-101' and timeslot!='AP' order by Measurement_Id DESC LIMIT 1";

		cursorMeasurement = dbPulse.rawQuery(s, null);
		cursorMeasurement.moveToFirst();

		while (cursorMeasurement.isAfterLast() == false) {

			DatefromDB = cursorMeasurement.getString(0).substring(0, 10); // get
																			// the
																			// last
																			// weight
																			// reading
																			// date
			timeslot = cursorMeasurement.getString(1);

			TimeFromDB = cursorMeasurement.getString(0).substring(
					cursorMeasurement.getString(0).length() - 2,
					cursorMeasurement.getString(0).length()); // get the last
																// weight
																// reading time
																// (AM or PM)
			cursorMeasurement.moveToNext();

		}
		cursorMeasurement.close();

		String patient_time = Util.get_patient_time_zone_time(context);
		patient_time = patient_time.substring(0, 10);

		// SimpleDateFormat dateFormat1 = new
		// SimpleDateFormat("MM/dd/yyyy");
		// Date date1 = new Date();
		Log.e("LOg Temperature>>>>", "" + type);
		if (type == 0) // AM or PM
		{

			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) // comparing
																			// current
				// at with last
				// massurement date
				no = 1;
			else
				no = 0;
		} else {
			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) {
				if (currentTime.trim().equals(timeslot)) {
					no = 1;
				} else {
					no = 0;
				}
			} else {
				no = 0;
			}

			/*
			 * if(currentTime.trim().equals(TimeFromDB)) // if AMPM { no=1;
			 * }else { no=0; }
			 */
		}

		dbPulse.close();

		return no;

	}

	// Delete Uploaded Pulse Entry from Database #REBOOT APK
	@SuppressLint("SimpleDateFormat")
	public void deleteUploadedPulse(Context ctx) {
		try {
			Cursor cur_Selector;
			String sectionDate;

			SQLiteDatabase dbPulse = this.getWritableDatabase();
			cur_Selector = dbPulse.rawQuery("SELECT * FROM " + oxygen_Table
					+ " where Status!=0", null);
			if (cur_Selector != null) {
				cur_Selector.moveToFirst();
				while (cur_Selector.isAfterLast() == false) {
					sectionDate = cur_Selector.getString(7);
					Log.i("#deleteUploadedPulse", "sectionDate = "
							+ sectionDate);

					final long MILLIS_PER_2_DAY = 48 * 60 * 60 * 1000L;
					// Session Time
					try {
						String currentTime = Util
								.get_patient_time_zone_time(ctx);

						SimpleDateFormat sDateFormat = new SimpleDateFormat(
								"MM/dd/yyyy hh:mm:ss aa");

						java.util.Date inTime = sDateFormat.parse(sectionDate);
						Calendar initialCal = Calendar.getInstance();
						initialCal.setTime(inTime);

						// Current Time
						java.util.Date checkTime = sDateFormat
								.parse(currentTime);
						Calendar currentCal = Calendar.getInstance();
						currentCal.setTime(checkTime);

						boolean moreThan2Days = Math.abs(currentCal
								.getTimeInMillis()
								- initialCal.getTimeInMillis()) > MILLIS_PER_2_DAY;

						Log.i("#deleteUploadedPulse", "moreThan2Day = "
								+ moreThan2Days);
						if (moreThan2Days) {
							dbPulse.execSQL("DELETE FROM " + oxygen_Table
									+ " where Status!=0 and sectionDate='"
									+ sectionDate + "'");
						}

					} catch (ParseException e) {
						//e.printStackTrace();
					}
					cur_Selector.moveToNext();
				}
			}
			cur_Selector.close();
			dbPulse.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}