package com.tkm.optumSierra.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassQuestion;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.Util;

public class Question_Check_db extends SQLiteOpenHelper {
	public static final String dbName = "Question_Check.db";
	public final String Question_Table = "Droid_Question_Check";
	public Cursor cursorMeasurement;

	public Question_Check_db(Context context) {
		super(context, dbName, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_GLUCOSE_NAME_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ Question_Table + "("
				+ "Question_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "Status  INTEGER," + "Patient_Id INTEGER,"
				+ "SectionDate TEXT," + "Timeslot TEXT" + ");";
		db.execSQL(CREATE_GLUCOSE_NAME_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public int InsertQuestion(ClassQuestion question) {
		SQLiteDatabase db = this.getWritableDatabase();
		int QuestionId = 0;

		try {

			ContentValues values = new ContentValues();
			int Patientid = Constants.getdroidPatientid();

			values.put("Status", 1);
			values.put("Patient_Id", Patientid);
			values.put("Timeslot", question.getTimeslot());
			values.put("SectionDate", question.getSectionDate());
			db.insert(Question_Table, null, values);
			cursorMeasurement = db.rawQuery("SELECT last_insert_rowid()", null);

			if (cursorMeasurement.moveToFirst()) {
				do {

					QuestionId = cursorMeasurement.getInt(0);

				} while (cursorMeasurement.moveToNext());
			}
			cursorMeasurement.close();
			db.close();

		} catch (SQLException e) {

		}
		return QuestionId;
	}

	public void delete_data() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(Question_Table, null, null);
		db.close();
	}

	public int GetCurrentDateValues(int type, String currentTime,
			Context context) {
		int no = 0;

		@SuppressWarnings("unused")
		String DatefromDB = "", TimeFromDB = "", timeslot = "";
		SQLiteDatabase db = this.getWritableDatabase();

		int patientidNew = Constants.getdroidPatientid();
		// String s = "SELECT MessageDate,timeslot FROM " + _Table
		String s = "SELECT SectionDate, Timeslot FROM " + Question_Table
				+ " where Patient_Id=" + patientidNew
				+ " and SectionDate !='0' and Timeslot !='AP' order by Question_Id DESC LIMIT 1";

		cursorMeasurement = db.rawQuery(s, null);
		cursorMeasurement.moveToFirst();

		while (cursorMeasurement.isAfterLast() == false) {

			DatefromDB = cursorMeasurement.getString(0).substring(0, 10); // get
																			// the
																			// last
																			// weight
																			// reading
																			// date
			timeslot = cursorMeasurement.getString(1);

			TimeFromDB = cursorMeasurement.getString(0).substring(
					cursorMeasurement.getString(0).length() - 2,
					cursorMeasurement.getString(0).length()); // get the last
																// weight
																// reading time
																// (AM or PM)
			cursorMeasurement.moveToNext();

		}
		cursorMeasurement.close();

		String patient_time = Util.get_patient_time_zone_time(context);
		patient_time = patient_time.substring(0, 10);

		// SimpleDateFormat dateFormat1 = new
		// SimpleDateFormat("MM/dd/yyyy");
		// Date date1 = new Date();
		Log.e("LOg Temperature>>>>", "" + type);
		if (type == 0) // AM or PM
		{
			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) // comparing
																			// current
				// at with last
				// massurement date
				no = 1;
			else
				no = 0;
		} else {
			if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) {
				if (currentTime.trim().equals(timeslot)) {
					no = 1;
				} else {
					no = 0;
				}
			} else {
				no = 0;
			}
			/*
			 * if(currentTime.trim().equals(TimeFromDB)) // if AMPM { no=1;
			 * }else { no=0; }
			 */
		}
		db.close();
		return no;
	}
}
