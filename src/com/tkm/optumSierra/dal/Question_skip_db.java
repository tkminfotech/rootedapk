package com.tkm.optumSierra.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassSensor;

public class Question_skip_db extends SQLiteOpenHelper {

	public static final String dbName = "Skip_Question_OP_1.db";
	public static final String sensor_Table = "skip_question_table";
	public Cursor curso_qusetion;
	ClassSensor sensor = new ClassSensor();

	public Question_skip_db(Context context) {
		super(context, dbName, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_sensor_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ sensor_Table + "("
				+ "skip_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "patient_id int," + "section_date TEXT,"
				+ "measure_date TEXT," + "ir_is_reminder int" + ")";

		db.execSQL(CREATE_sensor_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
	}

	public Cursor Select_question() {

		SQLiteDatabase db_sensor = this.getReadableDatabase();
		db_sensor.execSQL("DELETE  FROM " + sensor_Table
				+ " where section_date='0'");
		curso_qusetion = db_sensor.rawQuery("SELECT * FROM " + sensor_Table,
				null);
		return curso_qusetion;

	}

	public void InsertD_skip_Response(int Patient_Id, String section_date,
			String measure_date) {
		Log.e("Sierra", "InsertD_skip_Response----------------- qs as skipped");
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		ContentValues values = new ContentValues();
		values.put("patient_id", Patient_Id);
		values.put("section_date", section_date);
		values.put("measure_date", measure_date);
		values.put("ir_is_reminder", 1);
		db_sensor.insert(sensor_Table, null, values);

	}

	public void delete_skip_question_data(int id) {

		SQLiteDatabase dbPressure = this.getWritableDatabase();
		dbPressure.execSQL("DELETE  FROM " + sensor_Table + " where skip_Id="
				+ id);

		dbPressure.close();

	}

}