package com.tkm.optumSierra.dal;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.tkm.optumSierra.util.Util;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

public class Questions_db

{
	public static final String dbName = "OTPC_Questions_1.db";
	public static final String dmp_Table = "Droid_DMP_Name";
	public static final String qs_Table = "Droid_DMP_Questions";
	public static final String rs_Table = "Droid_DMP_Answers";
	public static final String br_Table = "Droid_DMP_Branching";
	public static final String uesr_response_table = "Droid_DMP_User_Response";
	//public static final String uesr_skip_response_table = "Droid_DMP_User_Skip_Response";

	public Cursor cursorQuestions;
	private final Context mCtx;

	private DatabaseHelper mDbHelper;
	private SQLiteDatabase dbQuestion;

	public class DatabaseHelper extends SQLiteOpenHelper {

		public DatabaseHelper(Context context) {

			super(context, dbName, null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			String CREATE_DMP_NAME_TABLE = "CREATE TABLE IF NOT EXISTS "
					+ dmp_Table + "(" + "Patient_Id int," + "Question_Id int,"
					+ "Question_Name TEXT," + "Top_Node  int" + ")";
			db.execSQL(CREATE_DMP_NAME_TABLE);

			String CREATE_DMP_Questions_TABLE = "CREATE TABLE IF NOT EXISTS "
					+ qs_Table + "(" + "Sequence_Number int,"
					+ "Explanation TEXT," + "Guidance TEXT," + "Kind  int,"
					+ "Last_Update_Date TEXT" + ")";
			db.execSQL(CREATE_DMP_Questions_TABLE);

			String CREATE_DMP_Answers_TABLE = "CREATE TABLE IF NOT EXISTS "
					+ rs_Table + "(" + "Sequence_Number int,"
					+ "Item_Number int," + "Item_Content TEXT" + ")";
			db.execSQL(CREATE_DMP_Answers_TABLE);

			String CREATE_DMP_Branching_TABLE = "CREATE TABLE IF NOT EXISTS "
					+ br_Table + "(" + "Patient_Code int,"
					+ "Node_Number TEXT," + "Child_Node_Number TEXT,"
					+ "Sequence_Number  int," + "Item_Number int,"
					+ "Last_Update_Date TEXT," + "Question_Id int" + ")";
			db.execSQL(CREATE_DMP_Branching_TABLE);

			String CREATE_DMP_User_Respons_TABLE = "CREATE TABLE IF NOT EXISTS "
					+ uesr_response_table
					+ "("
					+ "Patient_Id TEXT,Question_Message TEXT,"
					+ "Sequence_Number TEXT,Node_Type  int,Measure_Date TEXT,Item_Number int,Item_Content Text,Status int,treeNumber int, TreeName TEXT, sectionDate TEXT, ir_is_reminder int)";
			db.execSQL(CREATE_DMP_User_Respons_TABLE);
			
//			String CREATE_DMP_User_Skip_Respons_TABLE = "CREATE TABLE IF NOT EXISTS "
//					+ uesr_skip_response_table
//					+ "("
//					+ "Patient_Id TEXT,Question_Message TEXT,"
//					+ "Sequence_Number TEXT,Node_Type  int,Measure_Date TEXT,Item_Number int,Item_Content Text,Status int,treeNumber int, TreeName TEXT, sectionDate TEXT)";
//			db.execSQL(CREATE_DMP_User_Skip_Respons_TABLE);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		}

	}

	public void Cleartable() {
		try {
			open();
			dbQuestion.delete(dmp_Table, null, null);
			dbQuestion.delete(qs_Table, null, null);
			dbQuestion.delete(rs_Table, null, null);
			dbQuestion.delete(br_Table, null, null);

			// dbQuestion.execSQL("DELETE  FROM " + msg_Table
			// +" WHERE Status="+0 );
		} catch (SQLException e) {

			// e.printStackTrace();
		}
		closeopendbConnection();

	}

	public void Cleartableresponse() {
		try {
			open();
			dbQuestion.delete(uesr_response_table, "Status = 1", null);

			closeopendbConnection();
			// dbQuestion.execSQL("DELETE  FROM " + msg_Table
			// +" WHERE Status="+0 );
		} catch (SQLException e) {

			// e.printStackTrace();
		}

	}
	
//	public void CleartableSkipResponse() {
//		try {
//			open();
//			dbQuestion.delete(uesr_skip_response_table, "Status = 0", null);
//
//			closeopendbConnection();
//			// dbQuestion.execSQL("DELETE  FROM " + msg_Table
//			// +" WHERE Status="+0 );
//		} catch (SQLException e) {
//
//			// e.printStackTrace();
//		}
//
//	}

	public Questions_db open() throws SQLException {
		mDbHelper = new DatabaseHelper(mCtx);
		closeopendbConnection();
		mDbHelper = new DatabaseHelper(mCtx);
		dbQuestion = mDbHelper.getWritableDatabase();
		return this;

	}

	public Questions_db(Context ctx) {
		this.mCtx = ctx;
	}

	public void closeopendbConnection() {
		mDbHelper.close();
	}

	public void InsertDmp(int Patient_Id, int Question_Id,
			String Question_Name, int Top_Node) {
		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}

		ContentValues values = new ContentValues();
		values.put("Patient_Id", Patient_Id);
		values.put("Question_Id", Question_Id);
		values.put("Question_Name", Question_Name);
		values.put("Top_Node", Top_Node);

		dbQuestion.insert(dmp_Table, null, values);
		closeopendbConnection();

	}

	public void InsertDmpQuestion(int Sequence_Number, String Explanation,
			String Guidance, int Kind, String Last_Update_Date) {
		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}

		ContentValues values = new ContentValues();
		values.put("Sequence_Number", Sequence_Number);
		values.put("Explanation", Explanation);
		values.put("Guidance", Guidance);
		values.put("Kind", Kind);
		values.put("Last_Update_Date", Last_Update_Date);

		dbQuestion.insert(qs_Table, null, values);

		closeopendbConnection();

	}

	public void InsertDmpAnswers(int Sequence_Number, int Item_Number,
			String Item_Content) {
		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}

		ContentValues values = new ContentValues();
		values.put("Sequence_Number", Sequence_Number);
		values.put("Item_Number", Item_Number);
		values.put("Item_Content", Item_Content);
		dbQuestion.insert(rs_Table, null, values);

		closeopendbConnection();

	}

	public void InsertDmpBranching(int Patient_Code, String Node_Number,
			String Child_Node_Number, int Sequence_Number, int Item_Number,
			String Last_Update_Date, int Question_Id) {
		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}

		ContentValues values = new ContentValues();
		values.put("Patient_Code", Patient_Code);
		values.put("Node_Number", Node_Number);
		values.put("Child_Node_Number", Child_Node_Number);
		values.put("Sequence_Number", Sequence_Number);
		values.put("Item_Number", Item_Number);
		values.put("Last_Update_Date", Last_Update_Date);
		values.put("Question_Id", Question_Id);

		dbQuestion.insert(br_Table, null, values);
		closeopendbConnection();

	}

	public void InsertDmpUserResponse(String Patient_Id,
			String Question_Message, String Sequence_Number, int Node_Type,
			String Measure_Date, int Item_Number, String Item_Content,
			int Staus, int treeNumber, String treeName, String section_date, int ir_is_reminder) {
		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}

		ContentValues values = new ContentValues();
		values.put("Patient_Id", Patient_Id);
		values.put("Question_Message", Question_Message);
		values.put("Sequence_Number", Sequence_Number);
		values.put("Node_Type", Node_Type);
		values.put("Measure_Date", Measure_Date);
		values.put("Item_Number", Item_Number);
		values.put("Item_Content", Item_Content);
		values.put("Status", Staus);
		values.put("treeNumber", treeNumber);
		values.put("TreeName", treeName);
		values.put("sectionDate", section_date);
		values.put("ir_is_reminder", ir_is_reminder);
		dbQuestion.insert(uesr_response_table, null, values);
		closeopendbConnection();

	}

	
//	public void InsertDmpUserSkipResponse(String Patient_Id,
//			String Question_Message, String Sequence_Number, String Node_Type,
//			String Measure_Date, int Item_Number, String Item_Content,
//			int Staus, String treeNumber, String treeName, String section_date) {
//		try {
//			open();
//		} catch (SQLException e) {
//			// e.printStackTrace();
//		}
//
//		ContentValues values = new ContentValues();
//		values.put("Patient_Id", Patient_Id);
//		values.put("Question_Message", Question_Message);
//		values.put("Sequence_Number", Sequence_Number);
//		values.put("Node_Type", Node_Type);
//		values.put("Measure_Date", Measure_Date);
//		values.put("Item_Number", Item_Number);
//		values.put("Item_Content", Item_Content);
//		values.put("Status", Staus);
//		values.put("treeNumber", treeNumber);
//		values.put("TreeName", treeName);
//		values.put("sectionDate", section_date);
//		dbQuestion.insert(uesr_skip_response_table, null, values);
//		closeopendbConnection();
//
//	}
	
	public void updateToQuestionToServer(Context ctx) {
		// update uesr_response_table

		try {
			open();
			// db.delete(msg_Table, null, null);
			dbQuestion.execSQL("UPDATE  " + uesr_response_table
					+ " Set Status=1");
		} catch (SQLException e) {
			// e.printStackTrace();
		}

		closeopendbConnection();
		//FOR DELETING UPLOADED DATA CALL THIS
		deleteUploadedQuestions(ctx);
	}

	
	public void updateToQuestionAnswerResponse(Context ctx,int itemNumber,String itemContent,
			String currentDate,int treeNo,String treeName,String sequenceNumber, String section_date) {
		// update uesr_response_table

		try {
			open();
			dbQuestion.execSQL("UPDATE  " + uesr_response_table
					+ " Set  Status=0 where sectionDate='"+section_date+"'");
			// db.delete(msg_Table, null, null);
			dbQuestion.execSQL("UPDATE  " + uesr_response_table
					+ " Set Item_Number="+itemNumber+",Measure_Date='"+currentDate+"',"
							+ "Item_Content='"+itemContent+"' WHERE Sequence_Number="+sequenceNumber+" AND sectionDate='"+section_date+"'");
		} catch (SQLException e) {
			// e.printStackTrace();
		}

		closeopendbConnection();
		//FOR DELETING UPLOADED DATA CALL THIS
		deleteUploadedQuestions(ctx);
	}
	public Cursor SelectDMP() {
		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
		cursorQuestions = dbQuestion.query(dmp_Table, null, null, null, null,
				null, null);
		return cursorQuestions;
	}

	public Cursor SelectDBValues(String query) {

		try {
			open();
			cursorQuestions = dbQuestion.rawQuery(query, null);
			return cursorQuestions;
		} catch (SQLException e) {
			// e.printStackTrace();
		}

		return null;
	}
	
	public boolean reboot_Select() {
		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
		Cursor cursor = dbQuestion.rawQuery("SELECT * FROM Droid_DMP_User_Response"
				+ " WHERE Status=0", null);

		if (cursor.getCount() <= 0) {
			cursor.close();
			return false;
		}
		cursor.close();
		return true;
	}

	public Cursor SelectResponse() {
		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
		cursorQuestions = dbQuestion.query(rs_Table, null, null, null, null,
				null, null);
		return cursorQuestions;
	}

	public Cursor SelectBranch() {
		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
		cursorQuestions = dbQuestion.query(br_Table, null, null, null, null,
				null, null);
		return cursorQuestions;
	}

	public int SelectItemNumber(String itemContent, String sequenceNumber) {

		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
		int item = -1;

		cursorQuestions = dbQuestion.rawQuery("SELECT Item_Number  FROM "
				+ rs_Table + " where Item_Content like '" + itemContent + "'"
				+ "  and Sequence_Number=" + sequenceNumber, null);
		if (cursorQuestions.getCount() > 0) {

			if (cursorQuestions.moveToFirst()) {
				do {

					item = cursorQuestions.getInt(cursorQuestions
							.getColumnIndex("Item_Number"));

				} while (cursorQuestions.moveToNext());
			}
		}

		cursorQuestions.close();
		dbQuestion.close();
		return item;

	}

	public String SelectQuestainName() {

		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
		String Qname = "";

		cursorQuestions = dbQuestion.rawQuery("SELECT Question_Name  FROM "
				+ dmp_Table, null);
		if (cursorQuestions.getCount() > 0) {

			if (cursorQuestions.moveToFirst()) {
				do {

					Qname = cursorQuestions.getString(cursorQuestions
							.getColumnIndex("Question_Name"));

				} while (cursorQuestions.moveToNext());
			}
		}

		cursorQuestions.close();
		dbQuestion.close();
		return Qname;

	}
	
	// Delete Uploaded Questions from Database #REBOOT APK

	@SuppressLint("SimpleDateFormat")
	public void deleteUploadedQuestions(Context ctx) {
		try {
			Cursor cur_Selector;
			String sectionDate;
			open();
			cur_Selector = dbQuestion.rawQuery("SELECT * FROM "
					+ uesr_response_table + " where Status!=0", null);
			if (cur_Selector != null) {
				cur_Selector.moveToFirst();
				while (cur_Selector.isAfterLast() == false) {
					sectionDate = cur_Selector.getString(10);					

					final long MILLIS_PER_2_DAY = 48 * 60 * 60 * 1000L;
					// Session Time
					try {
						String currentTime = Util
								.get_patient_time_zone_time(ctx);

						SimpleDateFormat sDateFormat = new SimpleDateFormat(
								"MM/dd/yyyy hh:mm:ss aa");

						java.util.Date inTime = sDateFormat.parse(sectionDate);
						Calendar initialCal = Calendar.getInstance();
						initialCal.setTime(inTime);

						// Current Time
						java.util.Date checkTime = sDateFormat
								.parse(currentTime);
						Calendar currentCal = Calendar.getInstance();
						currentCal.setTime(checkTime);

						boolean moreThan2Days = Math.abs(currentCal
								.getTimeInMillis()
								- initialCal.getTimeInMillis()) > MILLIS_PER_2_DAY;
						
						if (moreThan2Days) {
							dbQuestion.execSQL("DELETE FROM "
									+ uesr_response_table
									+ " where Status!=0 and sectionDate='"
									+ sectionDate + "'");
						}

					} catch (ParseException e) {
						//e.printStackTrace();
					}
					cur_Selector.moveToNext();
				}
			}
			cur_Selector.close();
			closeopendbConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
