package com.tkm.optumSierra.dal;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassReminder;

@SuppressLint("SimpleDateFormat")
public class Reminder_db  {

	public static final String KEY_PID = "patientId";
	public static final String KEY_RID = "reminderId";
	public static final String KEY_RT = "reminderTime";
	public static final String KEY_RD = "reminderDuration";
	public static final String KEY_RCDT = "reminderCreateDateAndTime";
	public static final String KEY_RIDT = "reminderInsertDateAndTime";
	public static final String KEY_STATUS = "status";

	private static final String TAG = "DBAdapter";
	private static final String DATABASE_NAME = "Reminder.db";

	private static final String DATABASE_TABLE = "reminderDetails";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "CREATE TABLE "
			+ DATABASE_TABLE
			+ "(Id INTEGER PRIMARY KEY AUTOINCREMENT, patientId int, reminderId int, "
			+ "reminderTime text, reminderDuration int, reminderCreateDateAndTime text, "
			+ "reminderInsertDateAndTime text, status int)";

	private Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	
	/**
	 * Reminder DB constructor 
	 * 
	 * @param context
	 */
	public Reminder_db(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	
	/**
	 * Reminder DB Helper
	 *
	 */
	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {

			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			try {
				db.execSQL(DATABASE_CREATE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		/*	Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
			onCreate(db);*/
		}
	}

	
	
	
	/**
	 * Open Reminder DB
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Reminder_db open() throws SQLException {

		db = DBHelper.getWritableDatabase();
		return this;
	}

	
	/**
	 * Close Reminder DB
	 */
	public void close() {

		DBHelper.close();
	}
	
	
	/**
	 * Get the duration value from Reminder DB
	 * 
	 * @return reminderDuration
	 */
	public int GetDuration(){
		int duration = 0;
		
		try {
			open();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Cursor cursor = db.rawQuery("SELECT reminderDuration  FROM " + DATABASE_TABLE
				+ " LIMIT 1", null);
		if (cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					duration = cursor.getInt(0);
				} while (cursor.moveToNext());
				
			}
		}
			cursor.close();	
			close();
		
		return duration;
	}
	
	/**
	 * Insert function while in reminder alarm overlap condition
	 * 
	 * @param classReminder
	 * @param reminderInsertTime
	 */
	public void InsertReminderData(ClassReminder classReminder, String reminderInsertTime) {

		try {
			SimpleDateFormat dateFormatSc = new SimpleDateFormat("MM/dd/yyyy");
			String currentDateandTime = dateFormatSc.format(new Date());

			SQLiteDatabase db = DBHelper.getWritableDatabase();

			db.execSQL("DELETE  FROM " + DATABASE_TABLE
					+ " WHERE reminderCreateDateAndTime!='"
					+ classReminder.getCreatedDateAndTime() + "'"
					+ " and status !='" + 1 + "'"); // delete the old RM with
													// not taken

			db.execSQL("DELETE  FROM " + DATABASE_TABLE
					+ " WHERE reminderInsertDateAndTime!='"
					+ currentDateandTime + "'");

			Cursor cursorReminder = db.rawQuery(
					"SELECT * FROM " + DATABASE_TABLE
							+ " WHERE reminderCreateDateAndTime='"
							+ classReminder.getCreatedDateAndTime() + "'"
							+ " and reminderId='"
							+ classReminder.getReminderId() + "'", null);

			if (cursorReminder.getCount() == 0) {

				Log.i(TAG, "currentDateandTime-" + currentDateandTime
						+ "classReminder.getCreatedDateAndTime()-"
						+ classReminder.getCreatedDateAndTime());

				ContentValues initialValues = new ContentValues();
				initialValues.put(KEY_PID, classReminder.getPatientId());
				initialValues.put(KEY_RID, classReminder.getReminderId());
				initialValues.put(KEY_RT, classReminder.getReminderTime());
				initialValues.put(KEY_RD, classReminder.getDuration());
				initialValues.put(KEY_RCDT,
						classReminder.getCreatedDateAndTime());
				initialValues.put(KEY_RIDT, reminderInsertTime);
				initialValues.put(KEY_STATUS, classReminder.getStatus());
				db.insert(DATABASE_TABLE, null, initialValues);
			}
			db.close();
		} catch (Exception ex) {
			Log.i(TAG, ex.getMessage());
		}

	}
	

	
	/**
	 * Insert reminder data
	 * 
	 * @param reminder
	 * @param reminderInsertTime
	 * @return inertStatus
	 */
	public long insertReminder(ClassReminder reminder, String reminderInsertTime) {

		// -1 value if some error occurs.
		long insertVal = -1;

		try {
			open();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_PID, reminder.getPatientId());
		initialValues.put(KEY_RID, reminder.getReminderId());
		initialValues.put(KEY_RT, reminder.getReminderTime());
		initialValues.put(KEY_RD, reminder.getDuration());
		initialValues.put(KEY_RCDT, reminder.getCreatedDateAndTime());
		initialValues.put(KEY_RIDT, reminderInsertTime);
		initialValues.put(KEY_STATUS, reminder.getStatus());

		insertVal = db.insert(DATABASE_TABLE, null, initialValues);

		close();

		return insertVal;
	}
	
	
	/**
	 * Get Started Reminder Alarms
	 * 
	 * @return
	 */
	public ClassReminder GetStartedAlarm(){
		
		try {
			open();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ArrayList<ClassReminder> list  = new ArrayList<ClassReminder>();
		list.clear();
		list.addAll(GetAllLocalReminders());
		
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).getStatus() == 1){
				return list.get(i);
			}
		}
		return null;
	}
	
	
	/**
	 * Get the latest DB insert date
	 * 
	 * @return
	 */
	public String GetLatestInsertedDate(){
		
		try {
			open();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String reminderInsertTime = "";
		Cursor cursor = db.rawQuery("SELECT reminderInsertDateAndTime  FROM " + DATABASE_TABLE
				+ " LIMIT 1", null);
		if (cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					reminderInsertTime =cursor.getString(0);
				} while (cursor.moveToNext());
				
			}
		}
			cursor.close();	
			close();
		return reminderInsertTime;
	}
	

	/**
	 * Update the status of Reminder in DB
	 * 
	 * 0-INSERT
	 * 1-STARTED
	 * 2-SUCCESS
	 * 3-SKIP
	 * 
	 * @param reminder
	 */
	public void UpdateReminderStatus(ClassReminder reminder) {

		try {
			open();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		SQLiteStatement stmt = db.compileStatement("UPDATE  " + DATABASE_TABLE + " Set status="
				+ reminder.getStatus() + "  WHERE reminderTime= ?");
			stmt.bindString(1, reminder.getReminderTime());
			stmt.execute();
		close();

	}

	
	/**
	 * Delete All Reminder Data
	 */
	public void deleteAll() {
		try {
			open();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.delete(DATABASE_TABLE, null, null);
		close();
	}

	
	/**
	 * Delete a particular Reminder data from DB
	 * 
	 * @param reminder
	 */
	public void deleteReminder(ClassReminder reminder) {

		try {
			open();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.execSQL("DELETE  FROM " + DATABASE_TABLE + " where reminderId="
				+ reminder.getReminderId());
		close();

	}

	
	/**
	 * Select All function -- Reminder DB
	 * 
	 * @return
	 */
	public Cursor selectAll() {

		try {
			open();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Cursor cursor = db.query(DATABASE_TABLE, new String[] { KEY_PID,
				KEY_RID, KEY_RT, KEY_RD, KEY_RCDT, KEY_RIDT, KEY_STATUS }, null, null,
				null, null, null, null);

		return cursor;
		
	}
	
	/**
	 * Get All Reminder Object stored in Reminder DB
	 * 
	 * (excludes Reminder Insertion Time)
	 * 
	 * @return ArrayList<ClassReminder>
	 */
	public ArrayList<ClassReminder> GetAllLocalReminders() {
		ArrayList<ClassReminder> reminderList = new ArrayList<ClassReminder>();

		try {
			open();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Cursor cursor = db.query(DATABASE_TABLE, new String[] { KEY_PID,
				KEY_RID, KEY_RT, KEY_RD, KEY_RCDT, KEY_RIDT, KEY_STATUS },
				null, null, null, null, null, null);

		cursor.moveToFirst();

		while (cursor.isAfterLast() == false) {

			ClassReminder reminder = new ClassReminder();
			reminder.setPatientId(cursor.getInt(0));
			reminder.setReminderId(cursor.getInt(1));
			reminder.setReminderTime(cursor.getString(2));
			reminder.setDuration(cursor.getInt(3));
			reminder.setCreatedDateAndTime(cursor.getString(4));
			reminder.setStatus(cursor.getInt(6));

			reminderList.add(reminder);

			cursor.moveToNext();
		}

		cursor.close();
		close();

		return reminderList;

	}

	/**
	 * Get status of a particular Reminder object stored in Reminder DB
	 * 
	 * @param reminder
	 * @return ReminderStatusValue
	 */
	public ClassReminder GetReminderStatus(ClassReminder reminder) {

		try {
			open();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Cursor cursor = db.rawQuery("SELECT *  FROM " + DATABASE_TABLE
				+ " WHERE reminderId=" + reminder.getPatientId(), null);

		ClassReminder reminderObj = new ClassReminder();

		if (cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					
					reminderObj.setPatientId(cursor.getInt(0));
					reminderObj.setReminderId(cursor.getInt(1));
					reminderObj.setReminderTime(cursor.getString(2));
					reminderObj.setDuration(cursor.getInt(3));
					reminderObj.setCreatedDateAndTime(cursor.getString(4));
					reminderObj.setStatus(cursor.getInt(6));
					
				} while (cursor.moveToNext());
			}
		}

		cursor.close();
		close();
		return reminderObj;
	}

	
	
	/**
	 * Check if a particular Reminder Object exists in Reminder DB
	 * 
	 * @param reminder
	 * @return boolean reminderExistsStatus
	 */
	public boolean ReminderExists(ClassReminder reminder) {

		try {
			open();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Cursor cursor = null;
		String sql = "SELECT * FROM " + DATABASE_TABLE + " WHERE reminderTime="
				+ reminder.getReminderTime() + "AND reminderCreateDateAndTime=" + reminder.getCreatedDateAndTime();
		cursor = db.rawQuery(sql, null);
		Log.e(TAG, "Cursor Count : " + cursor.getCount());

		if (cursor.getCount() > 0) {
			cursor.close();
			close();
			return true;
		} else {
			cursor.close();
			close();
			return false;
		}
		
	}

}
