package com.tkm.optumSierra.dal;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassSchedule;

@SuppressLint("SimpleDateFormat")
public class Schedule_db extends SQLiteOpenHelper {
	public static final String dbName = "SierraSchedule_1.db";
	public final String scheduleTable = "scheduleDetails";
	private static final String TAG = "ScheduleDb Sierra";
	public Cursor cursorSchedule;

	public Schedule_db(Context context) {

		super(context, dbName, null, 1);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String createSchedule = "CREATE TABLE IF NOT EXISTS "
				+ scheduleTable
				+ "(Id INTEGER PRIMARY KEY AUTOINCREMENT, patientId int, "
				+ "scheduleId TEXT, status int, measureTypeId int, "
				+ "scheduleTime  TEXT, scTime datetime, downloadDate text,"
				+ "lastUpdateDate text,scTimeOut INT)";

		db.execSQL(createSchedule);

	}

	public Cursor UserIsPresent(String tableName) {
		SQLiteDatabase db = this.getReadableDatabase();

		cursorSchedule = db
				.query(tableName, null, null, null, null, null, null);
		return cursorSchedule;

	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

	public ClassSchedule selectSchedule(String time, int patientId) {

		SQLiteDatabase db = this.getReadableDatabase();

		/*
		 * cursorMedication = db.rawQuery("SELECT " +
		 * "time,substr(time,0,3) as inttime " + " FROM "+ medicationTable +*
		 * " where CAST(inttime  as integer) between "
		 * +time1+" and "+time2+" and patientId="+patientId,null);
		 */

		/*
		 * String query= "SELECT *  FROM " + scheduleTable +
		 * " WHERE status=0  and patientId=" + patientId
		 * 
		 * + " and scTime >='" +time+"'";
		 */

		String query = "SELECT *  FROM "
				+ scheduleTable
				+ " WHERE status=0  and patientId="
				+ patientId

				+ " and time('now', 'localtime') between scTime and  time( scTime,scTimeOut  || ' Minutes') order by Id desc LIMIT 1";

		// Log.i(TAG, "schedule  select query-"+query);

		// SELECT * FROM scheduleDetails WHERE time('now', 'localtime') between
		// scTime and time( scTime, '+15 Minute','localtime')

		// +" and "+time;

		/*
		 * String query = "SELECT *  FROM " + scheduleTable +
		 * " WHERE status=0  and patientId=" + patientId + " and scheduleTime="
		 * +"'"+ time+"'" + " order by measureTypeId desc LIMIT 1";
		 */

		cursorSchedule = db.rawQuery(query, null);
		ClassSchedule classSchedule = new ClassSchedule();

		if (cursorSchedule.getCount() > 0) {

			if (cursorSchedule.moveToFirst()) {
				do {
					classSchedule.setMeasureTypeId(Integer
							.parseInt(cursorSchedule.getString(4)));
					classSchedule.setScheduleTime(cursorSchedule.getString(5));
					classSchedule.setId(Integer.parseInt(cursorSchedule
							.getString(0)));
					classSchedule.setPatientId(Integer.parseInt(cursorSchedule
							.getString(1)));
					classSchedule.setScheduleId(cursorSchedule.getString(2));

				} while (cursorSchedule.moveToNext());
			}

		}

		cursorSchedule.close();
		db.close();
		return classSchedule;

	}

	public Cursor selectSchedule_skip_status(String time, int patientId) {

		SQLiteDatabase db = this.getReadableDatabase();

		String query = "SELECT *  FROM "
				+ scheduleTable
				+ " WHERE status=0  and patientId="
				+ patientId

				+ " and time('now', 'localtime') between scTime and  time( scTime,scTimeOut  || ' Minutes') order by Id desc";

		cursorSchedule = db.rawQuery(query, null);

		return cursorSchedule;

	}

	public void updateSchedule(int Id) {
		SQLiteDatabase db = this.getWritableDatabase();

		db.execSQL("UPDATE  " + scheduleTable + " Set status=1  WHERE Id=" + Id);
		db.close();

	}

	@SuppressLint("SimpleDateFormat")
	public void insertSchedule(ClassSchedule classSchedule) {
		try {

			SimpleDateFormat dateFormatSc = new SimpleDateFormat("MM/dd/yyyy");
			String currentDateandTime = dateFormatSc.format(new Date());

			String time = classSchedule.getScheduleTime();
			SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

			Date date1 = new Date();
			dateFormat.format(date1);

			Date date2 = new Date();
			date2 = dateFormat.parse(time);
			dateFormat.format(date2);

			@SuppressWarnings("unused")
			String S = "SELECT * FROM " + scheduleTable + " WHERE scheduleId='"
					+ classSchedule.getScheduleId() + "'"
					+ " and  scheduleTime='" + classSchedule.getScheduleTime()
					+ "'" + " and measureTypeId="
					+ classSchedule.getMeasureTypeId()
					+ " and lastUpdateDate='"
					+ classSchedule.getLastUpdateDate();

			// Log.i("Time out : ", classSchedule.getTimeout()+"");

			SQLiteDatabase db = this.getWritableDatabase();
			
			String[] columnsToReturn = { "Id", "patientId","scheduleId", 
					"status","measureTypeId", "scheduleTime","scTime", 
					"downloadDate","lastUpdateDate","scTimeOut" };
			String selection = "scheduleId = ? AND scheduleTime= ? AND measureTypeId= ? "
					+ "AND lastUpdateDate= ?";
			String[] selectionArgs = { classSchedule.getScheduleId(), 
					classSchedule.getScheduleTime(), ""+classSchedule.getMeasureTypeId(),
					classSchedule.getLastUpdateDate()}; // matched to "?" in selection
			cursorSchedule = db.query(scheduleTable, columnsToReturn, selection, selectionArgs, null, null, null);

//			cursorSchedule = db.rawQuery(
//					"SELECT * FROM " + scheduleTable + " WHERE scheduleId='"
//							+ classSchedule.getScheduleId() + "'"
//							+ " and  scheduleTime='"
//							+ classSchedule.getScheduleTime() + "'"
//							+ " and measureTypeId="
//							+ classSchedule.getMeasureTypeId()
//							+ " and lastUpdateDate='"
//							+ classSchedule.getLastUpdateDate() + "'", null);

			// db.execSQL("DELETE  FROM " + classSchedule.getPatientId()+
			// " WHERE patientId!=0");

			db.execSQL("DELETE  FROM " + scheduleTable + " WHERE patientId='"
					+ classSchedule.getPatientId() + "'"
					+ " and downloadDate!='" + currentDateandTime + "'"); // delete
																			// the
																			// old
																			// schedule

			if (cursorSchedule.getCount() == 0) {
				SQLiteStatement stmt = db.compileStatement("DELETE  FROM " + scheduleTable
						+ " WHERE scheduleId= ? and lastUpdateDate!= ?");
					stmt.bindString(1, classSchedule.getScheduleId());
					stmt.bindString(2, classSchedule.getLastUpdateDate());
					stmt.execute();
//				db.execSQL("DELETE  FROM " + scheduleTable
//						+ " WHERE scheduleId='" + classSchedule.getScheduleId()
//						+ "'" + " and lastUpdateDate!='"
//						+ classSchedule.getLastUpdateDate() + "'");// delete the
																	// deleted
																	// schedule
																	// from
																	// server

				/*
				 * db.execSQL("DELETE  FROM " + scheduleTable +
				 * " WHERE patientId='" + classSchedule.getPatientId() + "'" +
				 * " and downloadDate!='" + currentDateandTime+ "'"); // delete
				 * the old schedule
				 */

				ContentValues values_Settings = new ContentValues();

				values_Settings.put("patientId", classSchedule.getPatientId());
				values_Settings
						.put("scheduleId", classSchedule.getScheduleId());
				values_Settings.put("status", classSchedule.getStatus());
				values_Settings.put("measureTypeId",
						classSchedule.getMeasureTypeId());
				values_Settings.put("scheduleTime",
						classSchedule.getScheduleTime());
				values_Settings.put("scTime", dateFormat.format(date2));
				// values_Settings.put("scTimeOut",classSchedule.getTimeout());
				values_Settings.put("scTimeOut", 360);
				values_Settings.put("downloadDate", currentDateandTime);
				values_Settings.put("lastUpdateDate",
						classSchedule.getLastUpdateDate());

				db.insert(scheduleTable, null, values_Settings);

				// Log.i(TAG,"Inserted schedule");

			} else {
				db.execSQL("DELETE  FROM " + scheduleTable
						+ " WHERE patientId='" + classSchedule.getPatientId()
						+ "'" + " and downloadDate!='" + currentDateandTime
						+ "'"); // delete the old schedule

			}

			/*
			 * cursorSchedule = db.rawQuery( "SELECT * FROM " + scheduleTable +
			 * " WHERE scheduleTime='" + classSchedule.getScheduleTime() + "'" +
			 * " and measureTypeId=" + classSchedule.getMeasureTypeId(), null);
			 * 
			 * if (cursorSchedule.getCount() == 0) {
			 * 
			 * values_Settings.put("patientId", classSchedule.getPatientId());
			 * values_Settings .put("scheduleId",
			 * classSchedule.getScheduleId()); values_Settings.put("status",
			 * classSchedule.getStatus()); values_Settings.put("measureTypeId",
			 * classSchedule.getMeasureTypeId());
			 * values_Settings.put("scheduleTime",
			 * classSchedule.getScheduleTime());
			 * 
			 * db.insert(scheduleTable, null, values_Settings);
			 * 
			 * 
			 * 
			 * 
			 * } else {
			 * 
			 * // db.update(scheduleTable, //
			 * values_Settings,"patientId="+classUser.getPatientId() ,null); }
			 */
			db.close();
		} catch (Exception ex) {
			Log.i(TAG, ex.getMessage());
		}

	}

	public void DeletPatint() {

		try {
			SQLiteDatabase db = this.getWritableDatabase();

			db.execSQL("DELETE  FROM " + scheduleTable + " WHERE patientId!=0");
			db.close();

		} catch (Exception ex) {
			Log.i(TAG, ex.getMessage());
		}
	}

	public void DeletAllSchedule() {

		try {
			SQLiteDatabase db = this.getWritableDatabase();

			db.execSQL("DELETE  FROM " + scheduleTable + " WHERE status=0");
			db.close();

		} catch (Exception ex) {
			Log.i(TAG, ex.getMessage());
		}
	}

}
