package com.tkm.optumSierra.dal;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassSensor;

public class Sensor_db extends SQLiteOpenHelper {

	public static final String dbName = "_Sensor_v2.db";
	public static final String sensor_Table = "Droid_SensorDetails";
	public static final String sensor_order_Table = "sender_order";
	private static final String TAG = "SensorDb Sierra";
	public Cursor cursorsensor;
	ClassSensor sensor = new ClassSensor();

	public Sensor_db(Context context) {
		super(context, dbName, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_sensor_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ sensor_Table + "("
				+ "Sensor_Id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "Measure_type_id int," + "Measure_type_name TEXT,"
				+ "Sensor_name  TEXT," + "Weight_unit_id int,"
				+ "Patient_Id int," + "Status INTEGER,"
				+ "Weight_unit_name TEXT," + "Download_Date datetime,"
				+ "Mac_id TEXT," + " pin TEXT," + " item_sn TEXT" + ")";

		String CREATE_sensor_order_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ sensor_order_Table + "("
				+ "order_id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ " playorder INTEGER," + "Measure_type_name TEXT,"
				+ "measure_type_id INTEGER," + "Patient_Id INTEGER,"
				+ "Status INTEGER," + "homeOrder INTEGER" + ")";

		db.execSQL(CREATE_sensor_TABLE);
		db.execSQL(CREATE_sensor_order_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
	}

	public Cursor SelectMeasuredweight() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		// Cursor c = db.query(msg_Table, null, null, null, null, null, null);
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=2", null);

		return cursorsensor;

	}

	public Cursor ExecQuery() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.query(sensor_Table, null, null, null, null,
				null, null);
		return cursorsensor;
	}

	public Cursor SelectWeightSensorName() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=7", null);

		return cursorsensor;

	}

	public Cursor SelectBPSensorName() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=3", null);

		return cursorsensor;
	}

	public String SelectTempSensorName() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=6", null);

		String Mac = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Mac = (cursorsensor.getString(9));

		}
		cursorsensor.close();
		db_sensor.close();
		return Mac;

	}

	public String SelectGlucoName() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=1", null);

		String Mac = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Mac = (cursorsensor.getString(9));

		}
		cursorsensor.close();
		db_sensor.close();
		return Mac;

	}

	public String SelectGlucose_sensor_Name() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=1", null);

		String Mac = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Mac = (cursorsensor.getString(3));

		}
		cursorsensor.close();
		db_sensor.close();
		return Mac;

	}

	public String Select_GlucoName_adapter_mac() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=1", null);

		String Mac = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Mac = (cursorsensor.getString(9));

		}
		cursorsensor.close();
		db_sensor.close();
		return Mac;

	}
	public String Select_GlucoPIN_for_AccuChek() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=1", null);

		String Mac = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Mac = (cursorsensor.getString(10));

		}
		cursorsensor.close();
		db_sensor.close();
		return Mac;

	}

	public String Select_GlucoName_adapter_PIN() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=1", null);

		String Mac = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Mac = (cursorsensor.getString(10));

		}
		cursorsensor.close();
		db_sensor.close();
		return Mac;

	}

	public String SelectQuestion() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=101", null);

		String Mac = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Mac = (cursorsensor.getString(9));

		}
		cursorsensor.close();
		db_sensor.close();

		return Mac;

	}

	public void delete() {
		Log.i(TAG, "Deleted sucesfully");
		SQLiteDatabase db_sensor = this.getWritableDatabase();
		db_sensor.delete(sensor_Table, null, null);
		db_sensor.close();

	}

	public String SelectPulsePIN() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=2", null);

		String Mac = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Mac = (cursorsensor.getString(10));

		}
		cursorsensor.close();
		db_sensor.close();
		return Mac;

	}

	public String SelectWTPIN() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=7", null);

		String Mac = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Mac = (cursorsensor.getString(10));

		}
		cursorsensor.close();
		db_sensor.close();
		return Mac;

	}

	public String SelectBPPIN() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=3", null);

		String Mac = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Mac = (cursorsensor.getString(10));

		}
		cursorsensor.close();
		db_sensor.close();
		return Mac;

	}

	public String SelectTempPIN() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where Measure_type_id=6", null);

		String Mac = "";

		cursorsensor.moveToFirst();
		if (cursorsensor.getCount() > 0) {

			Mac = (cursorsensor.getString(10));

		}
		cursorsensor.close();
		db_sensor.close();
		return Mac;

	}

	protected static String readStream(InputStream in) {
		try {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuilder str = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				str.append(line + "\n");
			}
			in.close();
			return str.toString();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		return "";
	}

	protected String getTagValue(NodeList nodes, int i, String tag) {
		return getCharacterDataFromElement((org.w3c.dom.Element) (((org.w3c.dom.Element) nodes
				.item(i)).getElementsByTagName(tag)).item(0));
	}

	public String getCharacterDataFromElement(org.w3c.dom.Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

	public void insertSensor(ContentValues values) {
		SQLiteDatabase db_sensor = this.getWritableDatabase();
		db_sensor.insert(sensor_Table, null, values);
		db_sensor.close();
		// Log.i(TAG, "inserted succesfully");
	}

	public void insertSensor_play_order(ContentValues values) {
		SQLiteDatabase db_sensor = this.getWritableDatabase();
		db_sensor.insert(sensor_order_Table, null, values);
		db_sensor.close();

	}

	public void deleteplayorder() {
		Log.i(TAG, "Deleted sucesfully");
		SQLiteDatabase db_sensor = this.getWritableDatabase();
		db_sensor.delete(sensor_order_Table, null, null);
		db_sensor.close();

	}

	public int Selectsensororder() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_order_Table
				+ "  order by playorder asc LIMIT 1", null);
		int playorder = 0;

		if (cursorsensor.moveToFirst()) {
			do {

				playorder = cursorsensor.getInt(3);

			} while (cursorsensor.moveToNext());
		}
		cursorsensor.close();
		db_sensor.close();
		;

		return playorder;

	}

	public String measureunitName() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table
				+ " where   Measure_type_id=7 ", null);
		String unitname = "";

		if (cursorsensor.moveToFirst()) {
			do {

				unitname = cursorsensor.getString(7);

			} while (cursorsensor.moveToNext());
		}

		cursorsensor.close();
		db_sensor.close();
		return unitname;
	}

	public Cursor SelecthomeOrder() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_order_Table
				+ " order by homeOrder asc", null);

		return cursorsensor;
	}

	public Cursor Select_sensors_only() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_order_Table
				+ " order by homeOrder asc", null);

		return cursorsensor;

	}
	public Cursor SelectSensorDetails() {
		SQLiteDatabase db_sensor = this.getReadableDatabase();
		cursorsensor = db_sensor.rawQuery("SELECT * FROM " + sensor_Table , null);

		return cursorsensor;

	}

	/*
	 * public int SelectsensororderByMeasuretype(int id) { SQLiteDatabase
	 * db_sensor = this.getReadableDatabase(); cursorsensor =
	 * db_sensor.rawQuery("SELECT * FROM " + sensor_order_Table +
	 * " where measure_type_id" + "  order by playorder asc LIMIT 1", null); int
	 * playorder = 0;
	 * 
	 * if (cursorsensor.moveToFirst()) { do {
	 * 
	 * playorder = cursorsensor.getInt(3);
	 * 
	 * } while (cursorsensor.moveToNext()); } cursorsensor.close();
	 * 
	 * return playorder;
	 * 
	 * }
	 */

}