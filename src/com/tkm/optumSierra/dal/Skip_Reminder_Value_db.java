package com.tkm.optumSierra.dal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

public class Skip_Reminder_Value_db {

	public static final String KEY_ID = "id";
	public static final String KEY_PID = "patient_id";
	public static final String KEY_TIME = "time";
	public static final String KEY_STATUS = "status";

	private static final String TAG = "DBAdapter";
	private static final String DATABASE_NAME = "Skip_Reminder_Value_db.db";

	private static final String DATABASE_TABLE = "Skip_Reminder_Value_Table";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table Skip_Reminder_Value_Table (id INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ " patient_id text, time text, status text);";

	private final Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	public Skip_Reminder_Value_db(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {

			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			try {
				db.execSQL(DATABASE_CREATE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS Skip_Reminder_Value_Table");
			onCreate(db);
		}
	}

	// ---open Skip_Reminder_Value_db---
	public Skip_Reminder_Value_db open() throws SQLException {

		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---close SQLite DB---
	public void close() {

		DBHelper.close();
	}

	// ---insert data into Skip_Reminder_Value_db---
	public long insert(String patient_id, String time, String status) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_PID, patient_id);
		initialValues.put(KEY_TIME, time);
		initialValues.put(KEY_STATUS, status);
		return db.insert(DATABASE_TABLE, null, initialValues);

	}

	// ---Delete All Data from table in Skip_Reminder_Value_db---
	public void deleteAll() {

		db.delete(DATABASE_TABLE, null, null);

	}

	// ---Select All Data from table in Skip_Reminder_Value_db---
	public Cursor selectAll() {
		String[] columns = new String[] { "patient_id", "time", "status","id" };
		Cursor cursor = db.query(DATABASE_TABLE, columns, "status = '0'", null,
				null, null, null);
		return cursor;
	}

	public void update_db(int id) {

		db.execSQL("DELETE FROM  " + DATABASE_TABLE +"  WHERE id=" + id);

	}

}