package com.tkm.optumSierra.dal;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassWeight;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.TestDate;
import com.tkm.optumSierra.util.Util;

public class Weight_db {
	public static final String dbName = "OTH_Weight.db";
	public static final String _Table = "tpm_body_Weight";

	public Cursor cursorWeight;
	private final Context mCtx;

	private DatabaseHelper mDbHelper;
	private SQLiteDatabase dbWeight;

	public class DatabaseHelper extends SQLiteOpenHelper {

		public DatabaseHelper(Context context) {

			super(context, dbName, null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			String CREATE_msg_TABLE = "CREATE TABLE IF NOT EXISTS " + _Table
					+ "(" + "WeightId INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "MessageDate TEXT," + "Patient_Id INTEGER,"
					+ "WeightKg  TEXT," + " Status int," + " InputMode int,"
					+ " timeslot TEXT," + " sectionDate TEXT,"  + "is_reminder INTEGER DEFAULT 0" + ")";
			db.execSQL(CREATE_msg_TABLE);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		}

	}

	public Weight_db open() throws SQLException {
		mDbHelper = new DatabaseHelper(mCtx);
		closeOpenWTdbConnection();
		mDbHelper = new DatabaseHelper(mCtx);
		dbWeight = mDbHelper.getWritableDatabase();
		return this;

	}

	public Weight_db(Context ctx) {
		this.mCtx = ctx;
	}

	public void closeOpenWTdbConnection() {
		mDbHelper.close();
	}

	public int InsertWeight(ClassWeight weight) {

		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
		ContentValues values = new ContentValues();
		values.put("MessageDate", TestDate.getCurrentTime());
String str = TestDate.getCurrentTime();
		values.put("Patient_Id", weight.getPatientID());
		values.put("WeightKg", weight.getWeightInKg());
		values.put("Status", 1);
		values.put("InputMode", weight.getInputmode());
		values.put("timeslot", weight.getTimeslot());
		values.put("sectionDate", weight.getSectionDate());

		dbWeight.insert(_Table, null, values);
		int weight_id = 0;
		cursorWeight = dbWeight.rawQuery("SELECT last_insert_rowid()", null);

		if (cursorWeight.moveToFirst()) {
			do {

				weight_id = cursorWeight.getInt(0);

			} while (cursorWeight.moveToNext());
		}
		cursorWeight.close();

		closeOpenWTdbConnection();

		return weight_id;

	}

	public int InsertWeight_skip(ClassWeight weight) {

		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
		ContentValues values = new ContentValues();
		values.put("MessageDate", TestDate.getCurrentTime());

		values.put("Patient_Id", weight.getPatientID());
		values.put("WeightKg", weight.getWeightInKg());
		values.put("Status", 0);
		values.put("InputMode", weight.getInputmode());
		values.put("timeslot", weight.getTimeslot());
		values.put("sectionDate", weight.getSectionDate());
		values.put("is_reminder", 1);

		dbWeight.insert(_Table, null, values);
		int weight_id = 0;
		cursorWeight = dbWeight.rawQuery("SELECT last_insert_rowid()", null);

		if (cursorWeight.moveToFirst()) {
			do {

				weight_id = cursorWeight.getInt(0);

			} while (cursorWeight.moveToNext());
		}
		cursorWeight.close();

		closeOpenWTdbConnection();

		return weight_id;

	}

	public void InsertWeightService(ClassWeight weight) {

		try {
			open();
		} catch (SQLException e) {
		}
		ContentValues values = new ContentValues();
		values.put("MessageDate", weight.getMessageDate());
		values.put("Patient_Id", weight.getPatientID());
		values.put("WeightKg", weight.getWeightInKg());
		values.put("Status", 0);
		dbWeight.insert(_Table, null, values);

		closeOpenWTdbConnection();

	}

	public Cursor ExecQuery() {
		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
		int Patientid = Constants.getdroidPatientid();

		cursorWeight = dbWeight.rawQuery("SELECT * FROM " + _Table
				+ " where Patient_Id=" + Patientid, null);

		// cursorWeight = dbWeight.query(dmp_Table, null, null, null,null, null,
		// null);
		return cursorWeight;
	}

	public Cursor SelectMeasuredweight() {
		try {
			open();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
		// Cursor c = db.query(msg_Table, null, null, null, null, null, null);
		cursorWeight = dbWeight.rawQuery("SELECT * FROM " + _Table
				+ " where Status=0 and Patient_Id!=0", null);

		return cursorWeight;

	}

	public boolean reboot_Select() {
		try {
			open();
		} catch (SQLException e) {
		}
		Cursor cursor = dbWeight.rawQuery("SELECT * FROM " + _Table
				+ " WHERE Status=0", null);

		if (cursor.getCount() <= 0) {
			cursor.close();
			return false;
		}
		cursor.close();
		return true;
	}

	public void UpdateMeasureData(int weight_Id, Context ctx) {
		try {
			open();
			dbWeight.execSQL("UPDATE  " + _Table + " Set Status=1"
					+ "  WHERE WeightId=" + weight_Id);
			closeOpenWTdbConnection();
			
			//FOR DELETING UPLOADED DATA CALL THIS
			deleteUploadedWeight(ctx);
			
		} catch (SQLException e) {
			// e.printStackTrace();
		}
	}

	public void UpdateMeasureData_as_valid(int weight_Id) {
		try {
			open();
			// db.delete(msg_Table, null, null);
			dbWeight.execSQL("UPDATE  " + _Table + " Set Status=0"
					+ "  WHERE WeightId=" + weight_Id);
			closeOpenWTdbConnection();
		} catch (SQLException e) {
			// e.printStackTrace();
		}
	}

	public int GetCurrentDateValues(int type, String currentTime,
			Context context) {

		// type = 1 AMPM patient ; 0 = Either AM or PM

		int no = 0;

		@SuppressWarnings("unused")
		String DatefromDB = "", TimeFromDB = "", timeslot = "";

		try {
			open();
			int patientidNew = Constants.getdroidPatientid();

			// String s = "SELECT MessageDate,timeslot FROM " + _Table
			String s = "SELECT sectionDate,timeslot FROM "
					+ _Table
					+ " where  Patient_Id="
					+ patientidNew
				//	+ " and sectionDate !='0' and WeightKg != '-101' order by WeightId DESC LIMIT 1";
					+ " and WeightKg != '-101' and timeslot!='AP' order by WeightId DESC LIMIT 1";

			cursorWeight = dbWeight.rawQuery(s, null);
			cursorWeight.moveToFirst();

			while (cursorWeight.isAfterLast() == false) {

				DatefromDB = cursorWeight.getString(0).substring(0, 10); // get
																			// the
																			// last
																			// weight
																			// reading
																			// date
				timeslot = cursorWeight.getString(1);

				TimeFromDB = cursorWeight.getString(0).substring(
						cursorWeight.getString(0).length() - 2,
						cursorWeight.getString(0).length()); // get the last
																// weight
																// reading time
																// (AM or PM)
				cursorWeight.moveToNext();

			}
			cursorWeight.close();

			String patient_time = Util.get_patient_time_zone_time(context);
			patient_time = patient_time.substring(0, 10);

			// SimpleDateFormat dateFormat1 = new
			// SimpleDateFormat("MM/dd/yyyy");
			// Date date1 = new Date();

			if (type == 0) // AM or PM
			{

				if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) // comparing
																				// current
					// at with last
					// massurement date
					no = 1;
				else
					no = 0;
			} else {
				if (DatefromDB.trim().equalsIgnoreCase(patient_time.trim())) {
					if (currentTime.trim().equals(timeslot)) {
						no = 1;
					} else {
						no = 0;
					}
				} else {
					no = 0;
				}

				/*
				 * if(currentTime.trim().equals(TimeFromDB)) // if AMPM { no=1;
				 * }else { no=0; }
				 */
			}

			closeOpenWTdbConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return no;

	}

	public void delete_weight_data(int id) {

		try {

			open();
			dbWeight.execSQL("DELETE  FROM " + _Table + " where WeightId=" + id);
			closeOpenWTdbConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// Delete Uploaded Skip Weight Entry from Database
	public void delete_weight_101() {
		try {
			open();
			dbWeight.execSQL("DELETE FROM " + _Table
					+ " where WeightKg=-101 and Status!=0");
			dbWeight.execSQL("DELETE FROM " + _Table + " where sectionDate='0'");
			closeOpenWTdbConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Delete Uploaded Weight Entry from Database #REBOOT APK
	@SuppressLint("SimpleDateFormat")
	public void deleteUploadedWeight(Context ctx) {
		try {
			Cursor cur_Selector;
			String sectionDate;
			open();
			cur_Selector = dbWeight.rawQuery("SELECT * FROM "
					+ _Table + " where Status!=0", null);
			if (cur_Selector != null) {
				cur_Selector.moveToFirst();
				while (cur_Selector.isAfterLast() == false) {
					sectionDate = cur_Selector.getString(7);
					Log.i("#deleteUploadedWeight", "sectionDate = "
							+ sectionDate);

					final long MILLIS_PER_2_DAY = 48 * 60 * 60 * 1000L;
					// Session Time
					try {
						String currentTime = Util
								.get_patient_time_zone_time(ctx);

						SimpleDateFormat sDateFormat = new SimpleDateFormat(
								"MM/dd/yyyy hh:mm:ss aa");

						java.util.Date inTime = sDateFormat.parse(sectionDate);
						Calendar initialCal = Calendar.getInstance();
						initialCal.setTime(inTime);

						// Current Time
						java.util.Date checkTime = sDateFormat
								.parse(currentTime);
						Calendar currentCal = Calendar.getInstance();
						currentCal.setTime(checkTime);

						boolean moreThan2Days = Math.abs(currentCal
								.getTimeInMillis()
								- initialCal.getTimeInMillis()) > MILLIS_PER_2_DAY;

						Log.i("#deleteUploadedWeight", "moreThan2Day = "
								+ moreThan2Days);
						if (moreThan2Days) {
							dbWeight.execSQL("DELETE FROM "
									+ _Table
									+ " where Status!=0 and sectionDate='"
									+ sectionDate + "'");
						}

					} catch (ParseException e) {
						//e.printStackTrace();
					}
					cur_Selector.moveToNext();
				}
			}
			cur_Selector.close();
			closeOpenWTdbConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}