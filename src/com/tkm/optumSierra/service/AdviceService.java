package com.tkm.optumSierra.service;

import java.io.StringReader;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.PopActivity;
import com.tkm.optumSierra.bean.ClassAdvice;
import com.tkm.optumSierra.bean.ClassUser;
import com.tkm.optumSierra.dal.AdviceMessage_db;
import com.tkm.optumSierra.dal.Login_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Util;

public class AdviceService extends Service {

	Login_db login_db1 = new Login_db(this);
	// Droid_Schedule_db db = new Droid_Schedule_db(this);
	AdviceMessage_db dbcreate = new AdviceMessage_db(this);
	private String TAG = "AdviceService Sierra";

	@Override
	public void onCreate() {

		Log.i(TAG, "ADvice creating");
		super.onCreate();

	}

	@Override
	public IBinder onBind(Intent intent) {

		Log.i(TAG, "ADvice onBind");
		return null;
	}

	private Timer timer;

	private TimerTask updateTask = new TimerTask() {

		@Override
		public void run() {

			// DownloadAdviceMessage();

			Log.i(TAG, " Paient id : " + PatientIdDroid + " Server URL : "
					+ serverurl + " SerialNo : " + serialNo);
		}

	};

	private int PatientIdDroid = 0;
	private String serverurl = "";
	private String serialNo = "";
	private String imeilNo = "";

	@Override
	public void onDestroy() {

		super.onDestroy();

		Log.i(TAG, "Adicce destroying");

		this.stopSelf();

		timer.cancel();
		timer = null;
	}

	private Boolean setUrl() {

		Boolean flag = false;

		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.SERVER_URL_SP, 0);
		serverurl = settings.getString("Server_post_url", "-1");

		SharedPreferences settings1 = getSharedPreferences(
				CommonUtilities.WiFi_SP, 0);
		serialNo = settings1.getString("wifi", "-1");
		imeilNo = settings1.getString("imei_no", "");

		if (!serverurl.equals("-1")) {

			ClassUser classUser = new ClassUser();
			classUser = login_db1.SelectNickName();
			PatientIdDroid = classUser.getPatientId();

			flag = true;

		}
		Log.i(TAG, "Constan variables :" + serverurl + PatientIdDroid);

		return flag;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		SharedPreferences file_data_settings = getApplicationContext()
				.getSharedPreferences("file_data", 0);
		int advice_srvc_timer = Integer.parseInt(file_data_settings.getString(
				"advice_srvc_timer", "-1"));
		if (advice_srvc_timer <= 0) {
			advice_srvc_timer = 60;// 60 sec default
		}
		Log.i(TAG, "advice_srvc_timer : " + advice_srvc_timer);

		ClassUser classUser = new ClassUser();
		Login_db login_db1 = new Login_db(this);
		classUser = login_db1.SelectNickName();
		PatientIdDroid = classUser.getPatientId(); // getting patient id

		setUrl(); // get values from sp

		Log.i(TAG, " Paient id : " + PatientIdDroid + " Server URL : "
				+ serverurl + " SerialNo : " + serialNo);

		timer = new Timer();
		timer.schedule(updateTask, 0L, advice_srvc_timer * 1000L);

		super.onStart(intent, startId);
	}

//	public void DownloadAdviceMessage() {
//		DownloadWebPageTask task = new DownloadWebPageTask();
//		task.execute(new String[] { serverurl + "/droid_website/handler.ashx" });
//
//	}

//	private class DownloadWebPageTask extends AsyncTask<String, Void, String> {
//
//		protected String doInBackground(String... urls) {
//			String response = "";
//
//			try {
//
//				Log.i(TAG, "Checking device registration MAC-" + serialNo
//						+ " IMEI-" + imeilNo);
//
//				HttpResponse response1 = Util.connect(serverurl
//						+ "/droid_website/handler.ashx", new String[] {
//						"serial_no", "mode", "imei_no" }, new String[] {
//						serialNo, "0", imeilNo });
//
//				if (response1 == null) {
//					Log.e(TAG, "Connection Failed!");
//					return ""; // process
//				}
//
//				String str = Util
//						.readStream(response1.getEntity().getContent());
//
//				str = str.replaceAll("&", "and");
//				str = str.replaceAll("\r\n", "");
//				str = str.replaceAll("\n", "");
//				str = str.replaceAll("\r", "");
//
//				str.toString();
//
//				if (str.length() < 10) {
//					Log.i(TAG, "Advice message data null");
//					return "";
//				}
//
//				// dbcreate.Cleartable();
//				DocumentBuilder db = DocumentBuilderFactory.newInstance()
//						.newDocumentBuilder();
//				InputSource is = new InputSource();
//				is.setCharacterStream(new StringReader(str.toString()));
//				Document doc = db.parse(is);
//				NodeList nodes = doc.getElementsByTagName("advice");
//
//				ClassAdvice classAdvice = new ClassAdvice();
//
//				for (int i = 0; i < nodes.getLength(); i++) {
//
//					classAdvice.setPatientId(Integer.parseInt(Util.getTagValue(
//							nodes, i, "patient_id")));
//					classAdvice.setUserId(Integer.parseInt(Util.getTagValue(
//							nodes, i, "user_id")));
//					classAdvice.setAdviceSubject(Util.getTagValue(nodes, i,
//							"advice_subject"));
//					classAdvice.setAdviceText(Util.getTagValue(nodes, i,
//							"advice_text"));
//					classAdvice.setMessageDate(Util.getTagValue(nodes, i,
//							"create_date"));
//
//					// saving advice to db
//					Constants.setPatientid(classAdvice.getPatientId());
//
//					if (dbcreate.InsertAdviceMessage(classAdvice)) // if there i
//																	// a new
//																	// advice
//																	// msg
//					{
//						// notification();
//
//						dbcreate.UpdateAdviceMessageByDate(classAdvice
//								.getMessageDate());
//
//						try {
//							String message = classAdvice.getAdviceText();
//							Log.i(TAG, "Redirection start");
//							Intent intent = new Intent(AdviceService.this,
//									PopActivity.class);
//							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//							intent.putExtra("subject", "subject");
//							intent.putExtra("message", message);
//							startActivity(intent);
//
//						} catch (NullPointerException e) {
//							// TODO Auto-generated catch block
//							// e.printStackTrace();
//							Log.i(TAG, "IllegalArgumentException");
//						}
//					}
//
//				}
//
//			} catch (Exception e) {
//				// e.printStackTrace();
//				String Error = this.getClass().getName() + " : "
//						+ e.getMessage();
//				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
//						"Feedback Message Error", Error);
//				OnInit_Data_Access.insert_incident_detail(Error,
//						Constants.Other_Upload_Download_Error);
//			}
//
//			return response;
//		}
//	}

}
