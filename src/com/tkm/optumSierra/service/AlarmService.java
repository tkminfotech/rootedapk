package com.tkm.optumSierra.service;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.GlobalClass;
import com.tkm.optumSierra.bean.ClassReminder;
import com.tkm.optumSierra.bean.ClassUser;
import com.tkm.optumSierra.dal.Flow_Status_db;
import com.tkm.optumSierra.dal.Login_db;
import com.tkm.optumSierra.dal.Reminder_db;
import com.tkm.optumSierra.dal.Skip_Reminder_Value_db;
import com.tkm.optumSierra.util.Alam_util;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.ConnectionDetector;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.SkipVitals;
import com.tkm.optumSierra.util.Skip_Reminder_Values_Upload;
import com.tkm.optumSierra.util.Util;

@SuppressLint("SimpleDateFormat")
public class AlarmService extends Service {

	Login_db login_db1 = new Login_db(this);
	private String serverurl = "";
	/** interface for clients that bind */
	IBinder mBinder;
	/** indicates whether onRebind should be used */
	boolean mAllowRebind;
	/** flag to indicates whether there are reminder data available */
	private String TAG = "Alarm Service";
	private Timer timer;
	private int i = 0;
	private String PatientIdDroid = "";
	private String patientId = "";
	public Context contxt;
	public static String serialNo = "";
	public static String imeilNo = "";
	private ArrayList<ClassReminder> reminders = new ArrayList<ClassReminder>();
	private Reminder_db reminder_db = new Reminder_db(this);
	public Skip_Reminder_Value_db reminder_skip_db = new Skip_Reminder_Value_db(
			this);
	private GlobalClass appClass;
	String[] Status;
	String Status_Val;
	Flow_Status_db Flow_db = new Flow_Status_db(this);
	public Context context;
	Boolean isInternetPresent;
	int time_zone = 0, reminder_download_interval = 20;
	String apk_type = "";
	String time_slot = "";
	private Login_db login_db = new Login_db(this);

	/** Called when the service is being created. **/
	@Override
	public void onCreate() {

		Log.e(TAG, "AlarmService Created");

	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		appClass = (GlobalClass) getApplicationContext();
		contxt = getApplicationContext();
		appClass.acquireWakelock();
		context = getApplicationContext();

		SharedPreferences file_data_settings = getApplicationContext()
				.getSharedPreferences("file_data", 0);
		int reminder_srvc_timer = Integer.parseInt(file_data_settings
				.getString("reminder_srvc_timer", "-1"));
		reminder_download_interval = Integer.parseInt(file_data_settings
				.getString("reminder_download_interval", "20"));

		if (reminder_srvc_timer <= 0) {
			reminder_srvc_timer = 60;// 60 sec default
		}
		Log.i(TAG, "reminder_srvc_timer : " + reminder_srvc_timer);
		if (reminder_download_interval < 1) {
			reminder_download_interval = 20;
		}
		timer = new Timer();
		timer.schedule(updateTask, 0L, reminder_srvc_timer * 1000L); // 1 min

		super.onStart(intent, startId);
	}

	/** A client is binding to the service with bindService() */
	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	/** Called when all clients have unbound with unbindService() */
	@Override
	public boolean onUnbind(Intent intent) {
		return mAllowRebind;
	}

	/** Called when The service is no longer used and is being destroyed */
	@Override
	public void onDestroy() {
		super.onDestroy();
		appClass.releaseWakelock();
		Log.i(TAG, "ReminderService destroying");
		timer.cancel();
		timer = null;
	}

	TimerTask updateTask = new TimerTask() {
		@Override
		public void run() {
			Log.d(TAG, "------->  i=" + i + "  <-------");
			alarmTimeCheck();
		}
	};

	public void alarmTimeCheck() {
		try {
			SharedPreferences settings = getSharedPreferences(
					CommonUtilities.USER_SP, 0);
			PatientIdDroid = settings.getString("patient_id", "");

			SharedPreferences settings1 = getSharedPreferences(
					CommonUtilities.SERVER_URL_SP, 0);
			serverurl = settings1.getString("Server_post_url", "-1");

			SharedPreferences cluster_sp = getSharedPreferences(
					CommonUtilities.CLUSTER_SP, 0);

			time_zone = cluster_sp.getInt("time_zone_id", 0);

			Log.i(TAG, "Paient id: " + PatientIdDroid + "  Time_zone: "
					+ time_zone);

			if (reminders.size() == 0) {
				reminders = reminder_db.GetAllLocalReminders();
			}
			String patient_time = Util.get_patient_time_zone_time(this);
			// int mHours = calendar.get(Calendar.HOUR);
			// int mMinutes = calendar.get(Calendar.MINUTE);
			// int year1 = Integer.parseInt(time.substring(6, 10));
			// int month1 = Integer.parseInt(time.substring(0, 2));
			// int day1 = Integer.parseInt(time.substring(3, 5));
			int mHours = Integer.parseInt(patient_time.substring(11, 13));
			int mMinutes = Integer.parseInt(patient_time.substring(14, 16));
			// int sec = Integer.parseInt(time.substring(14, 16));
			String sysAmPm = patient_time.substring(20, 22);

			System.out
					.println("==================================================");
			//Log.e(TAG, "Time_zone Time:");
			Log.e(TAG, "HOUR:" + mHours + " MIN:" + mMinutes + " AM/PM:"
					+ sysAmPm);
			Log.d(TAG, "Alarm Time(Count=" + reminders.size() + "):");
			for (int i = 0; i < reminders.size(); i++) {

				String time1 = reminders.get(i).getReminderTime();

				int hr = Integer.parseInt(time1.substring(0, 2));
				int mn = Integer.parseInt(time1.substring(3, 5));
				String RmAmPm = time1.substring(6, 8);

				//Log.d(TAG, "HOUR:" + hr + " MIN:" + mn + " AM/PM:" + RmAmPm);
				if ((mHours == hr && mMinutes == mn)
						&& (sysAmPm.equals(RmAmPm))) {
					/*
					 * if(true) {
					 */

					Flow_db.open();

					Cursor cursor1 = Flow_db.selectAll();
					Status = new String[cursor1.getCount()];
					int j = 0;

					while (cursor1.moveToNext()) {
						Status[j] = cursor1.getString(cursor1
								.getColumnIndex("Status"));

						Status_Val = Status[0];

						j += 1;

					}

					Flow_db.close();

					if (Status_Val.equals("1")) {
						Log.i("appClass.isAlarmTriggered()==============>>",
								"false");
						ActivityManager am = (ActivityManager) context
								.getSystemService(Context.ACTIVITY_SERVICE);
						ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

						String mClassName = cn.getClassName();

						PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
						boolean isScreenOn = pm.isScreenOn();

						Log.i("mClassName", mClassName);
						if ((mClassName
								.equals("com.tkm.optumSierra.FinalMainActivity")
								|| mClassName
								.equals("com.tkm.optumSierra.Home")
								|| mClassName
								.equals("com.tkm.optumSierra.Reminder_Landing_Activity") || mClassName
								.equals("com.tkm.optumSierra.ThanksActivity") || mClassName
								.equals("com.tkm.optumSierra.MyDataActivity"))
								|| isScreenOn == false) {

							//SkipVitals.skip_patient_vitalse(getApplicationContext()); -- removed as part of Q1 #####


							SharedPreferences file_data_settings = getApplicationContext().getSharedPreferences("file_data", 0);
							String default_time = "";
							if(Util.is_lg())
								default_time = "1800";
							else
								default_time = "2160";
							int sleep_system_idle_timeout = Integer.parseInt(file_data_settings.getString("sleep_system_idle_timeout", default_time)); // defualt as 1800 sec ie 30 min					
							android.provider.Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, (60000 * sleep_system_idle_timeout/60));

							//android.provider.Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, (60000 * 4 * 9));

							reminders.get(i).setStatus(1);
							reminder_db.UpdateReminderStatus(reminders.get(i));

							SharedPreferences settings2 = getSharedPreferences(
									CommonUtilities.PREFS_NAME_date, 0);
							SharedPreferences.Editor editor_retake = settings2
									.edit();
							editor_retake.putString("measure_time_skip",
									Util.get_server_time());
							editor_retake
							.putString("sectiondate", patient_time);

							editor_retake.commit();

							appClass.setRegular_alarm_count(0);
							appClass.isAlarmUtilStart = true;
							Alam_util.startAlarm(getApplicationContext());
							Flow_db.open();
							Flow_db.deleteAll();
							Flow_db.insert("0");
							Flow_db.close();

						} else {
							if (mClassName.equals("com.tkm.optumSierra.SettingsPasscodeActivity")
									|| mClassName.equals("com.tkm.optumSierra.AssignedDeviceActivity")
									|| mClassName.equals("com.tkm.optumSierra.TestADeviceActivity")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceAandContinua")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceAandDReader")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceAandDSmart")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceBLECheckActivity")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceDataReaderActivity")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceForaMainActivity")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceGlucoseAccuChek")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceGlucoseReader")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceGlucoseTaidoc")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceOmronBlsActivity")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceP724Activity")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceP724Bp")
									|| mClassName.equals("com.tkm.optumSierra.TestDeviceP724BpSmart")
									|| mClassName.equals("com.tkm.optumSierra.TestDevicePressureWellex")){
								
								Log.e(TAG,"Skip Alarm 'Activity Not send Any Data .........TestDeviceActivity");
							}else{

								Log.e(TAG,
										"Skip Alarm 'Activity || ScreenOn' or not in a valid screen Reason..........");
								reminder_skip_db.open();
								reminder_skip_db.insert(PatientIdDroid,
										patient_time, "0");
								reminder_skip_db.close();
							}



							new Skip_Reminder_Values_Upload(contxt, serverurl)
							.execute();
						}

					} else {
						Log.i("appClass.isAlarmTriggered()==============>>",
								"true");
						Log.e(TAG,
								"Skip overlapping Alarm, alam is already running........");
						reminder_skip_db.open();
						reminder_skip_db.insert(PatientIdDroid, patient_time,
								"0");
						reminder_skip_db.close();

						new Skip_Reminder_Values_Upload(contxt, serverurl)
						.execute();
					}
				}
			}
			if (i == 1) {
				isInternetPresent = (new ConnectionDetector(
						getApplicationContext())).isConnectingToInternet();
				if (isInternetPresent) {

					SharedPreferences settingseifi = getSharedPreferences(
							CommonUtilities.WiFi_SP, 0);
					serialNo = settingseifi.getString("wifi", "-1");
					imeilNo = settingseifi.getString("imei_no", "");

					downloadReminderData();
				}
				i++;
			} else if (i == reminder_download_interval) {
				i = 0;

			} else {
				i++;
			}
			System.out
					.println("==================================================");
		} catch (Exception e) {
			Log.i("reminder service exception downloading reminder from server>>",
					"downloading ... ...");

			downloadReminderData();
		}

	}

	public void downloadReminderData() {

		Log.i(TAG, "downloadReminderData");
		DownloadReminderData task = new DownloadReminderData();
		task.execute();

	}

	/**
	 * Async Task to download the Reminder data and store in DB.
	 * 
	 */
	private class DownloadReminderData extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			Log.i(TAG, "onPostExecute Alarm Service");
			// alarmTimeCheck();
		}

		protected String doInBackground(String... urls) {

			String response = "";

			try {
				/*
				 * Log.e(TAG, "post url--" + serverurl +
				 * "/droid_website/reminder.ashx" + "PatientIdDroid-" +
				 * PatientIdDroid); HttpResponse response1 =
				 * Util.connect(serverurl + "/droid_website/reminder.ashx", //
				 * "http://115.111.179.7:8016/droid_website/reminder.ashx", new
				 * String[] { "patient_id" }, new String[] { PatientIdDroid });
				 */

				Log.i(TAG, "Checking device registration MAC-" + serialNo
						+ " IMEI-" + imeilNo);

				HttpResponse response1 = Util.connect(serverurl
						+ "/droid_website/get_patient_reminder_details.ashx",
						new String[] { "gw_id", "imei_no" }, new String[] {
								serialNo, imeilNo });

				if (response1 == null) {
					Log.e(TAG, "Connection Failed!");
					return "";
				}

				String str = Util
						.readStream(response1.getEntity().getContent());

				str = str.replaceAll("&", "and");
				str = str.replaceAll("\r\n", "");
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");

				str.toString();

				if (str.length() < 10) {
					Log.i(TAG, "Reminder data null");
					return "";
				}

				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);
				NodeList nodes = doc.getElementsByTagName("patient_reminder");
				NodeList nodesReminder = doc.getElementsByTagName("reminder");
				NodeList nodePatientDetails = doc
						.getElementsByTagName("patient_details");

				if (nodes.getLength() > 0) {
					reminders.clear();
					reminder_db.deleteAll();
					// ----FIRST TIME ENTRY
					for (int i = 0; i < nodesReminder.getLength(); i++) {
						ClassReminder reminder = new ClassReminder();

						reminder.setPatientId(Integer.parseInt(Util
								.getTagValue(nodePatientDetails, 0,
										"patient_id")));
						reminder.setReminderId(Integer.parseInt(Util
								.getTagValue(nodesReminder, i, "reminder_id")));
						reminder.setReminderTime(Util.getTagValue(
								nodesReminder, i, "reminder_time"));
						reminder.setDuration(Integer.parseInt(Util.getTagValue(
								nodesReminder, i, "reminder_duration")));
						reminder.setCreatedDateAndTime(Util.getTagValue(
								nodesReminder, i, "created_date"));

						reminder.setStatus(0);

						reminders.add(reminder);

					}
					// NodeList nodePatientDetails =
					// doc.getElementsByTagName("patient_details");
					time_zone = (Integer.parseInt(Util.getTagValue(
							nodePatientDetails, 0, "time_zone")));
					time_slot = Util.getTagValue(nodePatientDetails, 0,
							"patient_time_slot");
					apk_type = (Util.getTagValue(nodePatientDetails, 0,
							"cluster"));
					patientId = Util.getTagValue(nodePatientDetails, 0,
							"patient_id");

					ClassUser classUser = new ClassUser();
					classUser.setPatientId(Integer.parseInt(patientId));
					classUser.setFullName("");
					classUser.setNickName("");
					classUser.setVsb_code("");
					classUser.setDbo("");
					classUser.setSex("");

					// classUser.setType("AM"); // testing
					classUser.setType(time_slot);
					classUser.setContractcode("");
					login_db.insert_Patient_Data(classUser);

					SharedPreferences settings = getSharedPreferences(
							CommonUtilities.CLUSTER_SP, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putString("clustorNo", apk_type);
					editor.putInt("time_zone_id", time_zone);
					editor.commit();

					SharedPreferences contract_cod = getSharedPreferences(
							CommonUtilities.USER_SP, 0);
					SharedPreferences.Editor contract_editor = contract_cod
							.edit();
					contract_editor.putString("patient_id", patientId);
					contract_editor.commit();

					// Sort the reminders on ascending order
					Collections.sort(reminders,
							new Comparator<ClassReminder>() {
								DateFormat f = new SimpleDateFormat("hh:mm a");

								@Override
								public int compare(ClassReminder o1,
										ClassReminder o2) {
									try {
										if (o1.getReminderTime() == null
												|| o2.getReminderTime() == null)
											return 0;
										return f.parse(o1.getReminderTime())
												.compareTo(
														f.parse(o2
																.getReminderTime()));
									} catch (ParseException e) {
										throw new IllegalArgumentException(e);
									}
								}
							});

					// insert the sorted values to Reminder DB
					for (int i = 0; i < reminders.size(); i++) {

						DateFormat df = new SimpleDateFormat("M/dd/yyyy");// FORMAT:
																			// 4/22/2015
						String mReminderInsertTime = df.format(Calendar
								.getInstance().getTime());

						reminder_db.insertReminder(reminders.get(i),
								mReminderInsertTime);

					}

				}

			} catch (Exception e) {

				// e.printStackTrace();
				Log.e(TAG, "Error Reminder downloading and saving  ");
				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Reminder Data Downlaod Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.Other_Upload_Download_Error);
			}

			return response;
		}

	}

}