/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.

      Derived from BluetoothChat Android example.

 */

package com.tkm.optumSierra.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.tkm.optumSierra.util.Log;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for
 * incoming connections, a thread for connecting with a device, and a
 * thread for performing data transmissions when connected.
 */
public class BluetoothService {
    // Debugging
	 private static final String TAG = "AandDReaderActivity";
	    private static final boolean D = true;
	    
	    // Name for the SDP record when creating server socket
	    private static final String NAME = "PWAccessP";

	    // SPP UUID
	    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	    
	    // Port ID
	    private int mPort = -1;
	    
	    // Member fields
	    private final BluetoothAdapter mAdapter;
	    private final Handler mHandler;
	    private AcceptThread mAcceptThread;
	    private ConnectedThread mConnectedThread;
	    private int mState;
	    private BluetoothServerSocket mServerSocket;
//	    private BluetoothSocket saved_socket;
	    // Constants that indicate the current connection state
	    public static final int STATE_NONE = 0;       // we're doing nothing
	    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
	    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
	    public static final int STATE_CONNECTED = 3;  // now connected to a remote device
	    public static final int MESSAGE_STATE_CHANGE = 1;
	    public static final int MESSAGE_READ = 2;
	    public static final int MESSAGE_WRITE = 3;
	    public static final int MESSAGE_DEVICE_NAME = 4;
	    public static final int MESSAGE_TOAST = 5;
	    public static final String DEVICE_NAME = "device_name";
	    public static final String TOAST = "toast";
	    /**
	     * Constructor. Prepares a new BluetoothChat session.
	     * @param context  The UI Activity Context
	     * @param handler  A Handler to send messages back to the UI Activity
	     */
	    public BluetoothService(Context context, Handler handler, int portNumber) {
	        mAdapter = BluetoothAdapter.getDefaultAdapter();
	        mState = STATE_NONE;
	        mHandler = handler;
	        
	    }

	    /**
	     * Set the current state of the chat connection
	     * @param state  An integer defining the current connection state
	     */
	    private synchronized void setState(int state) {
	        if (D) Log.d(TAG, "setState() " + mState + " -> " + state);
	        mState = state;

	        // Give the new state to the Handler so the UI Activity can update
	        mHandler.obtainMessage(MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
	    }

	    /**
	     * Return the current connection state. */
	    public synchronized int getState() {
	        return mState;
	    }
	    
	    /**
	     * Return the current Port number. */
	    public synchronized int getPort() {
	        return mPort;
	    }


	    /**
	     * Start the chat service. Specifically start AcceptThread to begin a
	     * session in listening (server) mode. Called by the Activity onResume() */
	    public synchronized void start() {
	        if (D) Log.d(TAG, "..........start service..............");

	        // Cancel any thread currently running a connection
	        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}

	        // Start the thread to listen on a BluetoothServerSocket
	        if (mAcceptThread == null) {
	            mAcceptThread = new AcceptThread();
	            mAcceptThread.start();
	        }
	        setState(STATE_LISTEN);
	    }

	     /**
	     * Start the ConnectedThread to begin managing a Bluetooth connection
	     * @param socket  The BluetoothSocket on which the connection was made
	     * @param device  The BluetoothDevice that has been connected
	     */
	    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
	        if (D) Log.d(TAG, "**********connected***************");

	        // Cancel any thread currently running a connection
	        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}

	        // Cancel the accept thread because we only want to connect to one device
	        if (mAcceptThread != null) {mAcceptThread.cancel(); mAcceptThread = null;}

	        // Start the thread to manage the connection and perform transmissions
	        mConnectedThread = new ConnectedThread(socket);
	        mConnectedThread.start();

	        // Send the name of the connected device back to the UI Activity
	        Message msg = mHandler.obtainMessage(MESSAGE_DEVICE_NAME);
	        Bundle bundle = new Bundle();
	        bundle.putString(DEVICE_NAME, device.getName());
	        msg.setData(bundle);
	        mHandler.sendMessage(msg);

	        setState(STATE_CONNECTED);
	    }

	    /**
	     * Stop all threads
	     */
	    public synchronized void stop() {
	        if (D) Log.d(TAG, "stop");
	        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}
	        if (mAcceptThread != null) {mAcceptThread.cancel(); mAcceptThread = null;}
	        setState(STATE_NONE);
	    }

	    /**
	     * Write to the ConnectedThread in an unsynchronized manner
	     * @param out The bytes to write
	     * @see ConnectedThread#write(byte[])
	     */
	    public void write(byte[] out) {
	        // Create temporary object
	        ConnectedThread r;
	        // Synchronize a copy of the ConnectedThread
	        synchronized (this) {
	            if (mState != STATE_CONNECTED) return;
	            r = mConnectedThread;
	        }
	        // Perform the write unsynchronized
	        r.write(out);
	    }
	    
	    private int getPortNr()
	    {
	     try
	     {
	      // Retrieve the port number. For A&D devices you need this.
	      Field mSocketField = BluetoothServerSocket.class.getDeclaredField("mSocket");
	      mSocketField.setAccessible(true);
	      BluetoothSocket socket = (BluetoothSocket) mSocketField.get(mServerSocket);
	      mSocketField.setAccessible(false);

	      Field mPortField = BluetoothSocket.class.getDeclaredField("mPort");
	      mPortField.setAccessible(true);
	      int port = (Integer) mPortField.get(socket);
	      mPortField.setAccessible(false);

	      if (D)
	       Log.d(TAG, "BluetoothListenThread:getPortNr: Listening Port: " + port);

	      return port;
	     } catch (Exception e)
	     {
	      Log.e(TAG, "SensorService: getPortNr ");
	      return -1;
	     }
	    }

	    /**
	     * Indicate that the connection was lost and notify the UI Activity.
	     */
	    private void connectionLost() {
	        setState(STATE_LISTEN);

	        // Send a failure message back to the Activity
	        Message msg = mHandler.obtainMessage(MESSAGE_TOAST);
	        Bundle bundle = new Bundle();
	        bundle.putString(TOAST, "Device connection was lost");
	        msg.setData(bundle);
	        mHandler.sendMessage(msg);

	        // Restart the thread to listen on a BluetoothServerSocket
	        if (mAcceptThread == null) {
	            mAcceptThread = new AcceptThread();
	            mAcceptThread.start();
	        }
	    }

	    /**
	     * This thread runs while listening for incoming connections. It behaves
	     * like a server-side client. It runs until a connection is accepted
	     * (or until cancelled).
	     */
	    private class AcceptThread extends Thread {
	        // The local server socket
	        private final BluetoothServerSocket mmServerSocket;

	        public AcceptThread() {
	        	if (D) Log.d(TAG, "AcceptThread called!.......");
	        	BluetoothServerSocket tmp = null;
	        	
	        	/*try {
				Method createBond = BluetoothDevice.class
						.getMethod("createBond");
				boolean x = (Boolean) createBond.invoke(mDevice);
				  Log.e("weeeeeeeee", "DROID : "+ x);
			} catch (Exception ex) {
				Log.e(this.toString(), "Exception 1" + ex.getMessage());
			}*/
			     
	        	
	            // Create a new listening server socket.
	            // - Use the 'insecure' version so that we can connect from a
	            //	 no-input-no-output (unathenticated) device.
	        	// Restore preferences
	        	if (D) Log.d(TAG, "port is "+ mPort);
	        	if (mPort == -1)
	        	{
		            try {
		            	
		            	tmp = mAdapter.listenUsingInsecureRfcommWithServiceRecord(NAME, MY_UUID);
		//            	tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME, MY_UUID);
		//            	tmp = mAdapter.getRemoteDevice("BC:47:60:FE:FD:65").createRfcommSocketToServiceRecord(MY_UUID);
		            	mServerSocket = tmp;
		            	mPort = getPortNr();
		
		            } catch (IOException e) {
		               // Log.e(TAG, "listen() failed", e);
		        	}
	        	}
	        	else
	        	{
	        		  if (D)
	        		       Log.d(TAG, "In AcceptThre ad mPort is " + mPort);

	        		      // Only listen to this specific port if we have one
	        		      Method m;
						try {
							m = mAdapter.getClass().getMethod("listenUsingInsecureRfcommOn", new Class[] { int.class });
		        		    tmp = (BluetoothServerSocket) m.invoke(mAdapter, mPort); 
						} catch (NoSuchMethodException e) {
							e.printStackTrace();
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}
	        	}
	            mmServerSocket = tmp;
	        }

	        public void run() {
	            if (D) Log.d(TAG, "BEGIN mAcceptThread " + this);
	            setName("AcceptThread");
	            BluetoothSocket socket = null;

	            // Listen to the server socket if we're not connected
	            while (mState != STATE_CONNECTED) {
	                try {
	                    // This is a blocking call and will only return on a
	                    // successful connection or an exception
	                    socket = mmServerSocket.accept();
	                    //ccz: how to save and use the socket?
//	                    saved_socket = mmServerSocket;
	                } catch (IOException e) {
	                    Log.e(TAG, "accept() failed");
	                    break;
	                }
	                catch (Exception e) {
	                    Log.e(TAG, "accept() failed");
	                    break;
	                }
	                
	                if (socket == null)
	                {
	                	if (D) Log.d(TAG, "socket is null");
//	                	socket = saved_socket;
	                }
	                
	                // If a connection was accepted
	                if (socket != null) {

	                	// Close the server socket so no one else can connect
	                    cancel();

	                    synchronized (BluetoothService.this) {
	                        switch (mState) {
	                        case STATE_LISTEN:
	                        case STATE_CONNECTING:
	                            // Situation normal. Start the connected thread.
	                            connected(socket, socket.getRemoteDevice());
	                            break;
	                        case STATE_NONE:
	                        case STATE_CONNECTED:
	                            // Either not ready or already connected. Terminate new socket.
	                            if(D) Log.d(TAG, "socket closing");
	                        	try {
	                            	mServerSocket = mmServerSocket;
	                                socket.close();
	                            } catch (IOException e) {
	                                Log.e(TAG, "Could not close unwanted socket");
	                            }
	                            break;
	                        }
	                    }
	                }
	            }
	            if (D) Log.i(TAG, "END mAcceptThread");
	        }

	        public void cancel() {
	            if (D) Log.d(TAG, "cancel " + this);
	            Log.e(TAG, "close()  function called");
	            try {
	            	mServerSocket = mmServerSocket;
	                mmServerSocket.close();
	            } catch (IOException e) {
	                Log.e(TAG, "close() of server failed");
	            }
	        }
	    }


	    /**
	     * This thread runs during a connection with a remote device.
	     * It handles all incoming and outgoing transmissions.
	     */
	    private class ConnectedThread extends Thread {
	        private final BluetoothSocket mmSocket;
	        private final InputStream mmInStream;
	        private final OutputStream mmOutStream;

	        public ConnectedThread(BluetoothSocket socket) {
	            Log.d(TAG, "create ConnectedThread" + socket);
	            mmSocket = socket;
	            if (D) Log.d(TAG, "print socket "+ socket.getRemoteDevice());
	            InputStream tmpIn = null;
	            OutputStream tmpOut = null;

	            // Get the BluetoothSocket input and output streams
	            try {
	                tmpIn = socket.getInputStream();
	                tmpOut = socket.getOutputStream();
	            } catch (IOException e) {
	                Log.e(TAG, "temp sockets not created", e);
	            }

	            mmInStream = tmpIn;
	            mmOutStream = tmpOut;
	            
	        }

	        public void run() {
	            Log.i(TAG, "BEGIN mConnectedThread");

	            // Keep listening to the InputStream while connected
	            while (true) {
	                try {int repeat = 0;
	                	while ((mmInStream.available() <= 0) && (repeat >= 0)) {
	    					try {
	    						 Log.i(TAG, "inside repeat");
	    						Thread.sleep(500);
	    					} catch (InterruptedException e) {
	    						e.printStackTrace();
	    					}
	    					repeat--;
	    				}

	                    byte[] buffer = new byte[1024];
	                    int bytes;

	                    // Read from the InputStream
	                    bytes = mmInStream.read(buffer);
	                    Log.i(TAG, "Received  data from stream");

	                    Log.i(TAG, "Received "+bytes+" bytes");

	                    // Send the obtained bytes to the UI Activity
	                    mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
	                            .sendToTarget();
	                } catch (IOException e) {
	                    Log.e(TAG, "disconnected a and d");
	                    connectionLost();
	                    break;
	                }
	            }
	        }

	        /**
	         * Write to the connected OutStream.
	         * @param buffer  The bytes to write
	         */
	        public void write(byte[] buffer) {
	            try {
	            	//cleast data
	                mmOutStream.write(buffer);

	                // Share the sent message back to the UI Activity
	                mHandler.obtainMessage(MESSAGE_WRITE, -1, -1, buffer)
	                        .sendToTarget();
	            } catch (IOException e) {
	                Log.e(TAG, "Exception during write");
	            }
	        }

	        public void cancel() {
	            try {
	                mmSocket.close();
	            } catch (IOException e) {
	                Log.e(TAG, "close() of connect socket failed");
	            }
	        }
	    }
	}
