package com.tkm.optumSierra.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.tkm.optumSierra.util.Log;


import com.tkm.optumSierra.P724Bp;
import com.tkm.optumSierra.util.BatteryInfo;
import com.tkm.optumSierra.util.DeviceInfoBean;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for incoming
 * connections, a thread for connecting with a device, and a thread for
 * performing data transmissions when connected.
 */
public class BluetoothServiceP724Bp {
	// Debugging
	private static final String TAG = "BluetoothChatService";
	private static final boolean D = false;

	// Name for the SDP record when creating server socket
	private static final String NAME = "BluetoothChat";

	// Unique UUID for this application
	private static final UUID MY_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");

	// Member fields
	private final BluetoothAdapter mAdapter;
	private final Handler mHandler;
	private AcceptThread mAcceptThread;
	private ConnectThread mConnectThread;
	private ConnectedThread mConnectedThread;
	public int mState;

	// Constants that indicate the current connection state
	public static final int STATE_NONE = 0; // we're doing nothing
	public static final int STATE_LISTEN = 1; // now listening for incoming
												// connections
	public static final int STATE_CONNECTING = 2; // now initiating an outgoing
													// connection
	public static final int STATE_CONNECTED = 3; // now connected to a remote
													// device

	/**
	 * Constructor. Prepares a new BluetoothChat session.
	 * 
	 * @param context
	 *            The UI Activity Context
	 * @param handler
	 *            A Handler to send messages back to the UI Activity
	 */
	public BluetoothServiceP724Bp(Context context, Handler handler) {
		mAdapter = BluetoothAdapter.getDefaultAdapter();
		mState = STATE_NONE;
		mHandler = handler;
	}

	/**
	 * Set the current state of the chat connection
	 * 
	 * @param state
	 *            An integer defining the current connection state
	 */
	private synchronized void setState(int state) {
		if (D)
			Log.d(TAG, "setState() " + mState + " -> " + state);
		mState = state;
		// Give the new state to the Handler so the UI Activity can update
		mHandler.obtainMessage(P724Bp.MESSAGE_STATE_CHANGE, state, -1)
				.sendToTarget();
	}

	/**
	 * Return the current connection state.
	 */
	public synchronized int getState() {
		return mState;
	}

	/**
	 * Start the chat service. Specifically start AcceptThread to begin a
	 * session in listening (server) mode. Called by the Activity onResume()
	 */
	public synchronized void start() {
		if (D)
			Log.d(TAG, "start");

		// Cancel any thread attempting to make a connection
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		// Start the thread to listen on a BluetoothServerSocket
		if (mAcceptThread == null) {
			// mAcceptThread = new AcceptThread();
			// mAcceptThread.start();
		}
		Log.d("xxxxx", "start");
		setState(STATE_LISTEN);
	}

	/**
	 * Start the ConnectThread to initiate a connection to a remote device.
	 * 
	 * @param device
	 *            The BluetoothDevice to connect
	 */
	public synchronized void connect(BluetoothDevice device) {
		if (D)
			Log.d(TAG, "connect to: " + device);

		// Cancel any thread attempting to make a connection
		if (mState == STATE_CONNECTING) {
			if (mConnectThread != null) {
				mConnectThread.cancel();
				mConnectThread = null;
			}
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		// Start the thread to connect with the given device
		mConnectThread = new ConnectThread(device);
		mConnectThread.start();
		setState(STATE_CONNECTING);
	}

	/**
	 * Start the ConnectedThread to begin managing a Bluetooth connection
	 * 
	 * @param socket
	 *            The BluetoothSocket on which the connection was made
	 * @param device
	 *            The BluetoothDevice that has been connected
	 */
	public synchronized void connected(BluetoothSocket socket,
			BluetoothDevice device) {
		if (D)
			Log.d(TAG, "connected");

		// Cancel the thread that completed the connection
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		// Cancel the accept thread because we only want to connect to one
		// device
		if (mAcceptThread != null) {
			mAcceptThread.cancel();
			mAcceptThread = null;
		}

		// Start the thread to manage the connection and perform transmissions
		mConnectedThread = new ConnectedThread(socket);
		mConnectedThread.start();

		// Send the name of the connected device back to the UI Activity
		Message msg = mHandler.obtainMessage(P724Bp.MESSAGE_DEVICE_NAME);
		Bundle bundle = new Bundle();
		bundle.putString("device_name", device.getName());
		msg.setData(bundle);
		mHandler.sendMessage(msg);

		setState(STATE_CONNECTED);
	}

	/**
	 * Stop all threads
	 */
	public synchronized void stop() {
		if (D)
			Log.d(TAG, "stop");
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}
		if (mAcceptThread != null) {
			mAcceptThread.cancel();
			mAcceptThread = null;
		}
		setState(STATE_NONE);
	}

	/**
	 * Write to the ConnectedThread in an unsynchronized manner
	 * 
	 * @param out
	 *            The bytes to write
	 * @see ConnectedThread#write(byte[])
	 */
	public void write(byte[] out, int off, int cnt) {
		// Create temporary object
		ConnectedThread r;
		// Synchronize a copy of the ConnectedThread
		synchronized (this) {
			if (mState != STATE_CONNECTED) {
				return;
			}
			r = mConnectedThread;
		}
		// Perform the write unsynchronized
		r.write(out, off, cnt);
	}

	/**
	 * Indicate that the connection attempt failed and notify the UI Activity.
	 */
	private synchronized void connectionFailed() {
		setState(STATE_LISTEN);
	}

	/**
	 * Indicate that the connection was lost and notify the UI Activity.
	 */
	private void connectionLost() {
		Log.d("msg", "connectionLost");
		setState(STATE_LISTEN);
		Log.e("mg", "connectionFailed");
		mHandler.obtainMessage(P724Bp.MESSAGE_TOAST, -1, -1)
				.sendToTarget();
		// Send a failure message back to the Activity
		// Bundle bundle = new Bundle();
		// bundle.putString("toast", "Device connection was lost");
		// msg.setData(bundle);
		// setState(STATE_NONE);
	}

	/**
	 * This thread runs while listening for incoming connections. It behaves
	 * like a server-side client. It runs until a connection is accepted (or
	 * until cancelled).
	 */
	private class AcceptThread extends Thread {
		// The local server socket
		private BluetoothServerSocket mmServerSocket;

		@SuppressWarnings("unused")
		public AcceptThread() {
			BluetoothServerSocket tmp = null;

			// Create a new listening server socket
			try {
				tmp = mAdapter
						.listenUsingRfcommWithServiceRecord(NAME, MY_UUID);
			} catch (IOException e) {
				Log.e(TAG, "listen() failed", e);
			}
			mmServerSocket = tmp;
		}

		public void run() {
			if (D)
				Log.d(TAG, "BEGIN mAcceptThread" + this);
			setName("AcceptThread");
			BluetoothSocket socket = null;

			// Listen to the server socket if we're not connected
			while (mState != STATE_CONNECTED) {
				if (mmServerSocket == null)
					break;
				try {
					// This is a blocking call and will only return on a
					// successful connection or an exception
					socket = mmServerSocket.accept();
				} catch (IOException e) {
					Log.e(TAG, "accept() failed", e);
					break;
				}

				// If a connection was accepted
				if (socket != null) {
					synchronized (BluetoothServiceP724Bp.this) {
						switch (mState) {
						// case STATE_LISTEN:
						case STATE_CONNECTING:
							// Situation normal. Start the connected thread.
							connected(socket, socket.getRemoteDevice());
							break;
						case STATE_NONE:
						case STATE_CONNECTED:
							// Either not ready or already connected. Terminate
							// new socket.
							try {
								socket.close();
							} catch (IOException e) {
								Log.e(TAG, "Could not close unwanted socket", e);
							}
							break;
						}
					}
				}
			}
			if (D)
				Log.i(TAG, "END mAcceptThread");
		}

		public void cancel() {
			if (D)
				Log.d(TAG, "cancel " + this);
			if (mmServerSocket == null)
				return;
			try {
				mmServerSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of server failed", e);
			}
			mmServerSocket = null;
		}
	}

	/**
	 * This thread runs while attempting to make an outgoing connection with a
	 * device. It runs straight through; the connection either succeeds or
	 * fails.
	 */
	private class ConnectThread extends Thread {
		private BluetoothSocket mmSocket;
		private final BluetoothDevice mmDevice;

		public ConnectThread(BluetoothDevice device) {
			mmDevice = device;
			BluetoothSocket tmp = null;

			try {
				if (Build.VERSION.SDK_INT >= 10) { // API10 including this
													// function
					tmp = device
							.createInsecureRfcommSocketToServiceRecord(MY_UUID);
				} else {
					tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
				}
			} catch (IOException e) {
				Log.e(TAG, "create() failed", e);
			}
			mmSocket = tmp;
		}

		public void run() {
			Log.i(TAG, "BEGIN mConnectThread");
			setName("ConnectThread");

			// Always cancel discovery because it will slow down a connection
			mAdapter.cancelDiscovery();

			if (mmSocket == null)
				return;

			// Make a connection to the BluetoothSocket
			try {
				// This is a blocking call and will only return on a
				// successful connection or an exception
				mmSocket.connect();
			} catch (Exception e) {
				Log.e(TAG, "connect failed", e);

				connectionFailed();
				// Close the socket
				try {
					mmSocket.close();
				} catch (Exception e2) {
					Log.e(TAG,
							"unable to close() socket during connection failure",
							e2);
				}
				// Start the service over to restart listening mode
				// BluetoothChatService.this.start();
				return;
			}

			// Reset the ConnectThread because we're done
			synchronized (BluetoothServiceP724Bp.this) {
				mConnectThread = null;
			}
			// Start the connected thread
			connected(mmSocket, mmDevice);
		}

		public void cancel() {
			if (mmSocket == null)
				return;
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}
			mmSocket = null;
		}
	}

	/**
	 * This thread runs during a connection with a remote device. It handles all
	 * incoming and outgoing transmissions.
	 */
	private class ConnectedThread extends Thread {
		private BluetoothSocket mmSocket;
		private InputStream mmInStream;
		private OutputStream mmOutStream;

		public int g_press_value, g_hp_value, g_lp_value, g_hr_value;
		public int g_batt_info, g_nibp_state;

		private boolean nbp_head_flag = false;
		private boolean nbp_extra_flag = false;
		private int nbp_package_len = 0;
		private int nbp_package_size = 0;
		private final int MAX_PK_SIZE = 50;
		private byte[] nbp_data_buf = new byte[MAX_PK_SIZE];

		private int[] str = new int[4];

		private int parseNbpData(byte[] dataBuffer, int dataLen) {
			int ret;
			ret = 0;
			for (int i = 0; i < dataLen; i++) {
				if (dataBuffer[i] == (byte) 0xFA) {
					nbp_head_flag = true;
					nbp_package_len = 0;
					nbp_data_buf[nbp_package_len] = dataBuffer[i];
					nbp_package_len++;
					continue;
				}
				if (nbp_head_flag == false)
					continue;

				if (dataBuffer[i] == (byte) 0xFB) {
					nbp_extra_flag = true;
					continue;
				}
				if (nbp_extra_flag == true) {
					nbp_extra_flag = false;
					if (dataBuffer[i] == (byte) 0xFC)
						dataBuffer[i] = (byte) 0xFA;
					else if (dataBuffer[i] == (byte) 0xFD)
						dataBuffer[i] = (byte) 0xFB;
					else {
						nbp_head_flag = false;
						continue;
					}
				}

				if (nbp_package_len < MAX_PK_SIZE) {
					nbp_data_buf[nbp_package_len] = dataBuffer[i];
					nbp_package_len++;
					if (nbp_package_len == 2) {
						if (nbp_data_buf[1] == 0x01)
							nbp_package_size = 19;
						if (nbp_data_buf[1] == 0x02)
							nbp_package_size = 4;
						if (nbp_data_buf[1] == 0x20)
							nbp_package_size = 5;
						if (nbp_data_buf[1] == 0x21)
							nbp_package_size = 12;
					}
				} else {
					nbp_head_flag = false;
					continue;
				}

				if (nbp_package_len == nbp_package_size) {
					if (nbp_data_buf[1] == 0x01) {
						g_nibp_state = nbp_data_buf[2];
						DeviceInfoBean deviceBean = new DeviceInfoBean();
						String g_vendor_id = bytesToHexString(nbp_data_buf, 2,
								2);
						deviceBean.setVendor(g_vendor_id);
						String g_device_type = Integer
								.valueOf(
										Integer.toHexString(0xFF & nbp_data_buf[4]),
										16).toString();// Integer.toHexString(0xFF
														// & nbp_data_buf[4]);
						deviceBean.setDeviceType(g_device_type);
						String g_hardware_version = Integer
								.toString((nbp_data_buf[5] & 0xF0) >> 4)
								+ "."
								+ Integer.toString(nbp_data_buf[5] & 0x0F);
						deviceBean.setHardWareVersion(g_hardware_version);
						String g_software_version = Integer
								.toString((nbp_data_buf[6] & 0xF0) >> 4)
								+ "."
								+ Integer.toString(nbp_data_buf[6] & 0x0F);
						deviceBean.setSoftWareVersion(g_software_version);
						String g_product_time = Integer
								.toString(2000 + nbp_data_buf[7])
								+ "-"
								+ Integer.toString(nbp_data_buf[8])
								+ "-"
								+ Integer.toString(nbp_data_buf[9]);
						deviceBean.setProductDate(g_product_time);
						String g_product_id = bytesToHexString(nbp_data_buf,
								10, 8);
						deviceBean.setDeviceId(g_product_id);

						Log.e("mg", "g_vendor_id :" + g_vendor_id);
						Log.e("mg", "g_device_type :" + g_device_type);
						Log.e("mg", "g_hardware_version :" + g_hardware_version);
						Log.e("mg", "g_software_version :" + g_software_version);
						Log.e("mg", "g_product_time :" + g_product_time);
						Log.e("mg", "g_product_id :" + g_product_id);

						mHandler.obtainMessage(P724Bp.MESSAGE_DEVICE, -1,
								-1, deviceBean).sendToTarget();

					}
					if (nbp_data_buf[1] == 0x02) {
						Log.d("NBP", "Batt Info ACK");
						g_batt_info = nbp_data_buf[2];
						BatteryInfo info = new BatteryInfo();// ;
						info.isCharge = false;
						if ((g_batt_info & 0x80) != 0) {
							info.isCharge = true;
						}
						g_batt_info &= 0x7F;
						info.value = g_batt_info;

						Log.e("mg", "info: " + info.isCharge + "==>"
								+ info.value);

						mHandler.obtainMessage(P724Bp.MESSAGE_BATTERY,
								-1, -1, info).sendToTarget();
					}
					if (nbp_data_buf[1] == 0x20) {
						if (nbp_data_buf[2] == 0) {
							Log.d("NBP", "Start ACK");
						} else {
							Log.d("NBP", "Stop ACK");
						}
						g_nibp_state = nbp_data_buf[3];
					}

					if (nbp_data_buf[1] == 0x21) {
						g_nibp_state = nbp_data_buf[2] & 0xFF;

						if ((g_nibp_state & 0x80) != 0) {
							g_press_value = nbp_data_buf[3] & 0xFF;
							g_press_value = (g_press_value << 8)
									+ (nbp_data_buf[4] & 0xFF);
							str[0] = g_press_value;
							str[1] = g_lp_value;
							str[2] = g_hr_value;
							str[3] = g_nibp_state;
							mHandler.obtainMessage(P724Bp.MESSAGE_READ,
									-1, -1, str).sendToTarget();
						}

						if ((g_nibp_state & 0x40) != 0) {
							Log.d("NBP", "Result ACK Measure is Done");
							if ((g_nibp_state & 0x3F) != 0) { // error
								g_hp_value = 0;
								g_lp_value = 0;
								g_hr_value = 0;
								mHandler.obtainMessage(
										P724Bp.MESSAGE_ERRER, -1, -1,
										null).sendToTarget();
							} else {
								g_hp_value = nbp_data_buf[5] & 0xFF;
								g_hp_value = (g_hp_value << 8)
										+ (nbp_data_buf[6] & 0xFF);
								g_lp_value = nbp_data_buf[7];
								g_lp_value = (g_lp_value << 8)
										+ (nbp_data_buf[8] & 0xFF);
								g_hr_value = nbp_data_buf[9];
								g_hr_value = (g_hr_value << 8)
										+ (nbp_data_buf[10] & 0xFF);

								str[0] = g_hp_value;
								str[1] = g_lp_value;
								str[2] = g_hr_value;
								str[3] = g_nibp_state;
								Log.e("mg", "===> sending data to main page...");
								mHandler.obtainMessage(
										P724Bp.MESSAGE_RESULT, -1, -1,
										str).sendToTarget();
							}
						}
					}
					nbp_head_flag = false;
				}
			}

			return ret;
		}

		public ConnectedThread(BluetoothSocket socket) {
			Log.d(TAG, "create ConnectedThread");
			mmSocket = socket;
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			if (socket == null)
				return;
			// Get the BluetoothSocket input and output streams
			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;
		}

		public void run() {
			Log.i(TAG, "BEGIN mConnectedThread");
			byte[] buffer = new byte[200];
			int bytes;

			// Keep listening to the InputStream while connected
			while (mmSocket != null) {
				try {

					// Read from the InputStream
					bytes = mmInStream.read(buffer);
					// handler data
					parseNbpData(buffer, bytes);
					// mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
					// .sendToTarget();
				} catch (IOException e) {
					Log.e(TAG, "disconnected", e);
					connectionLost();
					break;
				}
			}
		}

		/**
		 * Write to the connected OutStream.
		 * 
		 * @param buffer
		 *            The bytes to write
		 */
		public void write(byte[] buffer, int off, int cnt) {
			try {
				mmOutStream.write(buffer, off, cnt);
			} catch (IOException e) {
				Log.e(TAG, "Exception during write", e);
				return;
			}

			try {
				mmOutStream.flush();
			} catch (IOException e) {
				Log.e(TAG, "Exception during flush", e);
				return;
			}

			// Share the sent message back to the UI Activity
			mHandler.obtainMessage(P724Bp.MESSAGE_WRITE, -1, -1, buffer)
					.sendToTarget();
		}

		public void cancel() {
			if (mmSocket == null)
				return;
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}
			mmSocket = null;
		}

		private String bytesToHexString(byte[] bArray, int index, int len) {
			StringBuffer sb = new StringBuffer(len + len + 2);
			String sTemp;
			// sb.append("0X");
			for (int i = 0; i < len; i++) {
				sTemp = Integer.toHexString(0xFF & bArray[index + i]);
				if (sTemp.length() < 2)
					sb.append(0);
				sb.append(String.format("%S", sTemp));
			}
			// String mp = Integer.valueOf(sb.toString(),16).toString();
			return sb.toString();
		}

	}
}
