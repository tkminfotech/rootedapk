package com.tkm.optumSierra.service;

import java.util.Timer;
import java.util.TimerTask;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;

public class CheckService extends Service {

	private String TAG = "CheckService";

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.i("Check Log--", "TimeZoneService onBind");
		return null;
	}

	private Timer timer;

	private TimerTask BTdateTask = new TimerTask() {
		@Override
		public void run() {
			try {
				if (!isMyReminderServiceRunning()) {
					Log.e(TAG,
							"Starting  AlarmService from CheckService -result");
					Intent reminder = new Intent();
					reminder.setClass(CheckService.this, AlarmService.class);
					startService(reminder);
				} else {
					Log.e(TAG,
							"Already Alarm Service working CheckService -result");
				}
			} catch (Exception e) {
				Log.e(TAG, "Exception while starting alarm #CheckService");
				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Error while starting alarm #CheckService", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.Other_Upload_Download_Error);
			}
			try {
				if (!isTenMinuteServiceRunning()) {
					Log.e(TAG,
							"*********************Starting TenMinuteService from CheckService -result********************");
					Intent tenMinute = new Intent();
					tenMinute.setClass(CheckService.this,
							TenMinuteService.class);
					startService(tenMinute);
				} else {
					Log.e(TAG,
							"Already TenMinute Service working; TenMinuteService -result");
				}
			} catch (Exception e) {
				Log.e(TAG, "Exception while starting TenMinuteService");
				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Error while starting TenMinuteService", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.Other_Upload_Download_Error);
			}
		}
	};

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i("Check Log", "Timer service destroying");
		this.stopSelf();
		timer.cancel();
		timer = null;
	}

	private boolean isMyReminderServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AlarmService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private boolean isTenMinuteServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.TenMinuteService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		SharedPreferences file_data_settings = getApplicationContext()
				.getSharedPreferences("file_data", 0);
		int reminder_srvc_assert_timer = Integer.parseInt(file_data_settings
				.getString("reminder_srvc_assert_timer", "-1"));
		if (reminder_srvc_assert_timer <= 0) {
			reminder_srvc_assert_timer = 60;// 60 sec default
		}
		Log.i(TAG, "reminder_srvc_assert_timer : " + reminder_srvc_assert_timer);

		timer = new Timer();
		// timer.schedule(updateTask, 0L, 10* 60 * 10000L); 1hr
		timer.schedule(BTdateTask, 0L, reminder_srvc_assert_timer * 1000L);
		super.onStart(intent, startId);
	}
}
