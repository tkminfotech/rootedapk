package com.tkm.optumSierra.service;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.MainActivity;
import com.tkm.optumSierra.R;
import com.tkm.optumSierra.bean.ClassSchedule;
import com.tkm.optumSierra.bean.ClassUser;
import com.tkm.optumSierra.dal.Login_db;
import com.tkm.optumSierra.dal.Schedule_db;
import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.Constants;
import com.tkm.optumSierra.util.OnInit_Data_Access;
import com.tkm.optumSierra.util.Util;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ScheduleService extends Service {

	Schedule_db dbSchedule = new Schedule_db(this);
	Login_db login_db1 = new Login_db(this);
	public static final String PREFS_NAME = "DroidPrefSc";
	String PostUrl = "";
	private String TAG = "ScheduleService Sierra";

	@Override
	public void onCreate() {

		Log.i(TAG, "ScheduleService creating");

		/*
		 * timer = new Timer();
		 * 
		 * //timer.schedule(updateTask, 0L, 10* 60 * 10000L);
		 * 
		 * timer.schedule(updateTask, 0L, 10 * 1000L); //10sec
		 */

		super.onCreate();

	}

	@Override
	public IBinder onBind(Intent intent) {

		Log.i(TAG, "ScheduleService onBind");
		return null;
	}

	private Timer timer;
	private int i = 12;

	private TimerTask updateTask = new TimerTask() {

		@Override
		public void run() {
			Log.i(TAG, "i=" + i + " download");
			if (i == 12) {

				// Log.e(TAG, " Paient id1 : " + PatientIdDroid + "PostUrl1 : "
				// + PostUrl);

				downloadSchedule();
				i = 0;

			} else {
				Log.i(TAG, "check_schedule for this time");
				check_schedule();
				i++;
			}

		}

	};

	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private void notification() {
		Intent intent = new Intent(ScheduleService.this, MainActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(ScheduleService.this,
				0, intent, 0);

		Uri soundUri = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		Notification noti = new Notification.Builder(ScheduleService.this)
				.setContentTitle("Schedule Notification:- Please check")
				// .setContentText(cn.getSubject()+" at "+cn.getTime())
				.setSmallIcon(R.drawable.ic_launcher).setContentIntent(pIntent)
				.setSound(soundUri).build();

		// .addAction(R.drawable.ic_launcher, "And more", pIntent).build();

		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		noti.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, noti);
	}

	private int PatientIdDroid = 0;

	@Override
	public void onDestroy() {

		super.onDestroy();

		Log.i(TAG, "ScheduleService destroying");

		this.stopSelf();

		timer.cancel();
		timer = null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		SharedPreferences file_data_settings = getApplicationContext()
				.getSharedPreferences("file_data", 0);
		int schedule_srvc_timer = Integer.parseInt(file_data_settings
				.getString("schedule_srvc_timer", "-1"));
		if (schedule_srvc_timer <= 0) {
			schedule_srvc_timer = 5 * 60;// 300 sec default
		}
		Log.i(TAG, "schedule_srvc_timer : " + schedule_srvc_timer);

		SharedPreferences settings = getSharedPreferences(
				CommonUtilities.SERVER_URL_SP, 0);
		PostUrl = settings.getString("Server_post_url", "-1");

		if (!PostUrl.equals("-1")) {
			ClassUser classUser = new ClassUser();
			classUser = login_db1.SelectNickName();
			PatientIdDroid = classUser.getPatientId();
		}

		Log.e(TAG, " schedule Paient id : " + PatientIdDroid + "PostUrl : "
				+ PostUrl);

		timer = new Timer();

		timer.schedule(updateTask, 0L, schedule_srvc_timer * 1000L);
		Log.e(TAG, " schedule timer started");

		super.onStart(intent, startId);
	}

	public void downloadSchedule() {

		Log.i(TAG, "Scedule message:-DownloadWebPageTask in service");
		downloadSchedule task = new downloadSchedule();
		// task.execute(new String[] { PostUrl + "/medication_handler.ashx" });
		task.execute(new String[] { "" });

	}

	@SuppressLint("SimpleDateFormat")
	private class downloadSchedule extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {

			Log.i(TAG, "onPostExecute Scheduleservice");

			check_schedule();

		}

		protected String doInBackground(String... urls) {

			String response = "";

			try {

				HttpResponse response1 = Util.connect(PostUrl
						+ "/droid_website/schedule_handler.ashx",
						new String[] { "patient_id" },
						new String[] { PatientIdDroid + "" });

				if (response1 == null) {
					Log.e(TAG, "Connection Failed!");
					return "";
				} else {
					dbSchedule.DeletAllSchedule();
				}

				String str = Util
						.readStream(response1.getEntity().getContent());

				str = str.replaceAll("&", "and");
				str = str.replaceAll("\r\n", "");
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");

				str.toString();
//				Log.i(TAG, "Schedule:-" + str);

				if (str.length() < 10) {
					Log.i(TAG, "Schedule data null");
					return "";
				} else {

					// dbSchedule.DeletPatint();
				}

				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);
				NodeList nodes = doc.getElementsByTagName("schedule");
				// NodeList timeNode = doc.getElementsByTagName("times");
				ClassSchedule classSchedule = new ClassSchedule();
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"MM/dd/yyyy hh:mm:ss aa");
				String currentDateandTime = dateFormat.format(new Date());
				for (int i = 0; i < nodes.getLength(); i++) {

					classSchedule.setScheduleId((Util.getTagValue(nodes, i,
							"schedule_id")));
					classSchedule.setPatientId(Integer.parseInt(Util
							.getTagValue(nodes, i, "patient_id")));
					classSchedule.setLastUpdateDate((Util.getTagValue(nodes, i,
							"last_updateDate")));
					classSchedule.setDownloadDate(currentDateandTime);
					classSchedule.setStatus(0);

					classSchedule.setTimeout(Integer.parseInt(Util.getTagValue(
							nodes, i, "late_minutes")));

					Constants.setPatientid(classSchedule.getPatientId());

					Element advice_idelement = (Element) nodes.item(i);
					NodeList timeNode = advice_idelement
							.getElementsByTagName("times");
					NodeList actvivitiesNode = advice_idelement
							.getElementsByTagName("actvivities");

					// SimpleDateFormat dateFormat = new
					// SimpleDateFormat("hh:mm");
					// Time date = dateFormat.parse(time);

					for (int k = 0; k < timeNode.getLength(); k++) {
						classSchedule.setScheduleTime(Util.getTagValue(
								timeNode, k, "time"));
						for (int j = 0; j < actvivitiesNode.getLength(); j++) {

							classSchedule.setMeasureTypeId(Integer
									.parseInt(Util.getTagValue(actvivitiesNode,
											j, "measure_type_id")));

							dbSchedule.insertSchedule(classSchedule);

						}

					}
				}

			} catch (Exception e) {

				Log.e(TAG,
						"Error schedule downloading and saving  "
								+ e.getMessage());
				// e.printStackTrace();

				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putInt("Scheduletatus", 0);
				editor.commit();

				String Error = this.getClass().getName() + " : "
						+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(getApplicationContext(),
						"Schedule Download Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error,
						Constants.Other_Upload_Download_Error);
			}

			return response;
		}

	}

	private void check_schedule() {
		Calendar c = Calendar.getInstance();
		String timeNow = c.get(Calendar.HOUR_OF_DAY) + ":00";
		Log.i(TAG, "Timer Now : " + timeNow + " Pid=" + PatientIdDroid);
		if (PatientIdDroid != 0) {
			ClassSchedule classSchedule = dbSchedule.selectSchedule(timeNow,
					PatientIdDroid);
			if (classSchedule.getMeasureTypeId() != 0) {
				notification();
			}
		}
	}

}
