package com.tkm.optumSierra.service;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.util.CommonUtilities;
import com.tkm.optumSierra.util.UploadVitalReadingsTask;

public class TenMinuteService extends Service {

	private String TAG = "CheckService";

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.i("Check Log--", "TimeZoneService onBind");
		return null;
	}

	private Timer timer;

	private TimerTask BTdateTask = new TimerTask() {

		@Override
		public void run() {
			try {
				SharedPreferences settings = getApplicationContext()
						.getSharedPreferences(CommonUtilities.SERVER_URL_SP, 0);
				String posturl = settings.getString("Server_post_url", "-1");
				if (!posturl.equals("-1")) {
					Log.e("TenMinuteService", "++++++++++++++------------------------------flow.");
					new UploadVitalReadingsTask(getApplicationContext(),
							posturl).execute();
				}
			} catch (Exception e) {
				Log.e(TAG,
						"Exception while starting or closing alarm #CheckService");
			}
		}
	};

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i("Check Log", "Timer service destroying");
		this.stopSelf();
		timer.cancel();
		timer = null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		SharedPreferences file_data_settings = getApplicationContext()
				.getSharedPreferences("file_data", 0);
		int upload_srvc_timer = Integer.parseInt(file_data_settings.getString(
				"upload_srvc_timer", "-1"));
		if (upload_srvc_timer <= 0) {
			upload_srvc_timer = 10 * 60;// 600 sec default
		}
		Log.i(TAG, "++++upload_srvc_timer : ++++++++++++++++++++++++++++++++++++++++++++++++++" + upload_srvc_timer);

		timer = new Timer();
		timer.schedule(BTdateTask, 0L, upload_srvc_timer * 1000L);
		super.onStart(intent, startId);
	}
}
