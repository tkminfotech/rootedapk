package com.tkm.optumSierra.util;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.AlarmReceiver;
import com.tkm.optumSierra.GlobalClass;

/**
 * Util class to start and cancel alarm object.
 * 
 */
public class Alam_util {

	private static PendingIntent pendingIntent;
	private static AlarmManager manager;
	private static int mAlarmId = 10001;
	public static GlobalClass appstate;

	/**
	 * Start Alarm
	 * 
	 * @param context
	 * @param duration
	 */
	@SuppressLint("NewApi")
	public static void startAlarm(Context context) {
		Log.i("Alam_util", "Inside #startAlarm");
		appstate = (GlobalClass) context.getApplicationContext();
		Intent alarmIntent = new Intent(context, AlarmReceiver.class);
		pendingIntent = PendingIntent.getBroadcast(context, mAlarmId,
				alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		manager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		long interval = (5 * 60 * 1000);

		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(System.currentTimeMillis());

		Calendar futureDate = Calendar.getInstance();
		futureDate.add(Calendar.SECOND, (int) (interval / 1000));

		Calendar initial = Calendar.getInstance();
		initial.add(Calendar.SECOND, (int) (1));

		if (android.os.Build.VERSION.SDK_INT >= 19) {
			if (appstate.getRegular_alarm_count() == 0) {
				manager.setExact(AlarmManager.RTC_WAKEUP, initial.getTime()
						.getTime(), pendingIntent);
			} else {
				manager.setExact(AlarmManager.RTC_WAKEUP, futureDate.getTime()
						.getTime(), pendingIntent);
			}
		} else {
			manager.setRepeating(AlarmManager.RTC_WAKEUP,
					now.getTimeInMillis(), interval, pendingIntent);
		}

	}

	/**
	 * Cancel Alarm
	 * 
	 * @param context
	 */
	public static void cancelAlarm(Context context) {

		Log.i("cancelAlarm", "cancelAlarm ---");

		if (manager != null) {
			manager.cancel(pendingIntent);
			Log.i("cancelAlarm", "cancelAlarm finish ---");
		} else {
			manager = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			Intent alarmIntent = new Intent(context, AlarmReceiver.class);
			pendingIntent = PendingIntent.getBroadcast(context, mAlarmId,
					alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			manager.cancel(pendingIntent);
		}
	}

}