package com.tkm.optumSierra.util;

import android.content.Context;
import android.content.Intent;

public final class CommonUtilities {
	
	// give your server registration url here
    public  static String SERVER_URL = "https://dev.optumtelehealth.com"; 
    public static String CLUSTER_SP="cluster_merge_sp";
    public static String SERVER_URL_SP="server_config_sp"; 
    public static String WiFi_SP="wifi_mac_merge";
    public static String USER_SP="user_details";
    public static String DISCLAIMER_SP = "disclaimer_details";
    public static String PREFS_NAME_TEMPUNIT = "TemperatureUnit";
    public static String USER_FLOW_SP="user_details_type";
    public static String USER_TIMESLOT_SP="user_timeslot_type";
    public static String Login_UserName="";
    public static String LASTUPLOAD_STATUS="last_upload_status";
    public static String RETAKE_SP="vitalsr_retake";
    public static final String PREFS_NAME_date = "sectiondate";
    public static final String REMINDER_SP = "reminder_shared_pref";
    public static String NION_SP="nion_pair";
    // Google project id
    static final String SENDER_ID = "298759888371"; 

    /**
     * Tag used on log messages.
     */
    static final String TAG = "AndroidHive GCM";

    static final String DISPLAY_MESSAGE_ACTION =
            "com.androidhive.pushnotifications.DISPLAY_MESSAGE";

    static final String EXTRA_MESSAGE = "message";
   

    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
}
