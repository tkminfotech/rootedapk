package com.tkm.optumSierra.util;

public class Constants {

	public static String patienrName = null;
	public static String server = null;
	public static String port = null;
	public static String postUrl = null;
	public static String serialNo = null;
	public static String IMEI_No = null;
	public static String PIN = null;
	public static int language = 0;
	public static String continua = "0";

	public static int PatientIdDroid = 0;
	public static int questionPresent = 0;
	public static String question_id = "";

	//REBOOT ERROR CONSTANTS
	public static int Other_Upload_Download_Error = 0;
	
	public static int Weight_failure_upload = 1;
	public static int BP_failure_upload = 2;
	public static int BG_failure_upload = 3;
	public static int Temp_failure_upload = 4;
	public static int SpO2_failure_upload = 5;
	public static int Questions_failure_upload = 6;
	public static int Before_the_tablet_restarts = 7;
	public static int After_the_tablet_starts_up = 8;
	public static int Disabling_Airplane_mode = 9;
	public static int Error_in_disabling_Airplane_mode = 10;
	public static int Disabling_WiFi = 11;
	public static int Error_in_disabling_WiFi = 12;
	public static int Enabling_Bluetooth = 13;
	public static int Error_in_enabling_Bluetooth = 14;

	public static void setParams(String server, String port, String postUrl,
			String serialNo) {
		Constants.server = server;
		Constants.port = port;
		Constants.postUrl = postUrl;
		Constants.serialNo = serialNo;

	}

	public static void setPatientid(int patientid) {
		Constants.PatientIdDroid = patientid;
	}

	public static void setpatienrName(String patienrName) {
		Constants.patienrName = patienrName;
	}

	public static String getpatienrName() {
		return patienrName;
	}

	public static String getServer() {
		return server;
	}

	public static String getPort() {
		return port;
	}

	public static String getPostUrl() {
		return postUrl;
	}

	public static String getSerialNo() {
		return serialNo;
	}

	public static int getdroidPatientid() {
		return PatientIdDroid;
	}

	public static int getquestionPresent() {
		return questionPresent;
	}

	public static void setquestionPresent(int questionPresent) {
		Constants.questionPresent = questionPresent;
	}

	public static String getIMEI_No() {
		return IMEI_No;
	}

	public static void setIMEI_No(String iMEI_No) {
		Constants.IMEI_No = iMEI_No;
	}

	public static String getPIN() {
		return PIN;
	}

	public static void setPIN(String pIN) {
		PIN = pIN;
	}

	public static void setLanguage(int ln) {
		Constants.language = ln;

	}

	public static int getLanguage() {

		return language;
	}

	public static void setcontinua(String state) {
		Constants.continua = state;
	}

	public static String getcontinua() {
		return continua;
	}

	public static void set_question_id(String qn) {
		Constants.question_id = qn;

	}

	public static String get_question_id() {

		return question_id;
	}

}