package com.tkm.optumSierra.util;

public class DeviceInfoBean {
	
	private String deviceId;
	private String productDate;
	private String softWareVersion;
	private String hardWareVersion;
	private String deviceType;
	private String vendor;
	
	
	public DeviceInfoBean(String deviceId, String productDate,
			String softWareVersion, String hardWareVersion, String deviceType,
			String vendor) {
		super();
		this.deviceId = deviceId;
		this.productDate = productDate;
		this.softWareVersion = softWareVersion;
		this.hardWareVersion = hardWareVersion;
		this.deviceType = deviceType;
		this.vendor = vendor;
	}

	public DeviceInfoBean() {
		super();
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getProductDate() {
		return productDate;
	}

	public void setProductDate(String productDate) {
		this.productDate = productDate;
	}

	public String getSoftWareVersion() {
		return softWareVersion;
	}

	public void setSoftWareVersion(String softWareVersion) {
		this.softWareVersion = softWareVersion;
	}

	public String getHardWareVersion() {
		return hardWareVersion;
	}

	public void setHardWareVersion(String hardWareVersion) {
		this.hardWareVersion = hardWareVersion;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
}
