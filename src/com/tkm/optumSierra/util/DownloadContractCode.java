package com.tkm.optumSierra.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import com.tkm.optumSierra.util.Log;

public class DownloadContractCode extends AsyncTask<String, Void, String> {

	private String TAG = "DownloadContractCode";
	private Context context;
	private String serverpath;

	public DownloadContractCode(Context ctx, String serverpath) {

		this.context = ctx;
		this.serverpath = serverpath;

	}

	@Override
	protected void onPostExecute(String result) {
		Log.i(TAG, "onPostExecute download image completed..");
	}

	@Override
	protected void onPreExecute() {

		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	protected String doInBackground(String... urls) {

		return check_contract_code_detail();

	}

	private String check_contract_code_detail() {
		// DownloadAdviceMessage();
		Log.i(TAG, "download image started ...");

		try {
			get_cotract_code_details();

			SharedPreferences settings = context.getSharedPreferences(
					CommonUtilities.USER_SP, 0);

			String contract_code = settings.getString("contract_code_name",
					"-1");
			String mCurrentPhotoPath = Environment
					.getExternalStorageDirectory().toString()
					+ "/versions/HFP/Images/";

			File directoryFile = new File(mCurrentPhotoPath);

			File myFile = new File(directoryFile.getAbsolutePath() + "/"
					+ contract_code + ".png");
			Log.i(TAG, "contractcode-" + contract_code);

			if (myFile.exists()) {

				Log.i(TAG, "contractcode already exists");

			} else {
				download_image();
				Log.i(TAG, "contractcode not  exists");
			}
			/*
			 * ActivityProfilepicture apf=new ActivityProfilepicture();
			 * apf.DownloadProfilePicServer
			 * (PatientIdDroid,serverurl,ContractCode); Thread.sleep(1000);
			 */

		} catch (Exception e) {
			Log.i(TAG, "Exception in set profile picture");
		}

		return "";
	}

	private void download_image() {

		try {
			SharedPreferences settings = context.getSharedPreferences(
					CommonUtilities.USER_SP, 0);

			String contract_code = settings.getString("contract_code_name",
					"-1");
			URL url;
			File file;
			int PatientIdDroid = Constants.getdroidPatientid();
			if (contract_code.equals("")) {
				url = new URL(serverpath + "/droid_website/user_images/"
						+ PatientIdDroid + ".jpg");
				file = new File(PatientIdDroid + ".png");
			} else {
				Log.i(TAG, "downloaded file name :" + serverpath
						+ "/droid_website/contract_images/" + contract_code
						+ ".jpg");
				url = new URL(serverpath + "/droid_website/contract_images/"
						+ contract_code + ".jpg");
				file = new File(contract_code + ".png");
				Log.d(TAG, "downloaded file name:" + contract_code + ".png");
			}

			Log.d(TAG, "download contract code  url:" + url);
			if (PatientIdDroid != 0) {
				/* Open a connection to that URL. */
				URLConnection con = url.openConnection();

				InputStream is = con.getInputStream();
				BufferedInputStream bis = new BufferedInputStream(is, 1024 * 50);
				String SDCardRoot = Environment.getExternalStorageDirectory()
						.toString() + "/versions/HFP/Images/";
				FileOutputStream fos = new FileOutputStream(SDCardRoot + file);
				byte[] buffer = new byte[1024 * 50];

				int current = 0;
				while ((current = bis.read(buffer)) != -1) {
					fos.write(buffer, 0, current);
				}

				fos.flush();
				fos.close();
				bis.close();
			}
			Log.d(TAG, "download  new contract code completed");

		} catch (Exception e) {
			e.printStackTrace();
			Log.d(TAG, "download contract code exception");
		}

	}

	private void get_cotract_code_details() {

		try {
			SharedPreferences settingseifi = context.getSharedPreferences(
					CommonUtilities.WiFi_SP, 0);
			String serialNo = settingseifi.getString("wifi", "-1");
			String imeilNo = settingseifi.getString("imei_no", "");

			Log.i(TAG, "Checking device registration MAC-" + serialNo
					+ " IMEI-" + imeilNo);

			HttpResponse response1 = Util.connect(serverpath
					+ "/droid_website/cluster.ashx", new String[] { "gw_id",
					"imei_no" }, new String[] { serialNo, imeilNo });

			String str = Util.readStream(response1.getEntity().getContent());

			if (str.trim().length() == 0) {

			} else {
				str = str.replaceAll("\n", "");
				str = str.replaceAll("\r", "");

				DocumentBuilder db = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is);

				NodeList nodes = doc.getElementsByTagName("optum");
				for (int i = 0; i < nodes.getLength(); i++) {
					String contract_code = Util.getTagValue(nodes, i,
							"contractcode");
					SharedPreferences contract_cod = context
							.getSharedPreferences(CommonUtilities.USER_SP, 0);
					SharedPreferences.Editor contract_editor = contract_cod
							.edit();
					contract_editor.putString("contract_code_name",
							contract_code);
					contract_editor.commit();

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			String Error = this.getClass().getName()+ " : "+ e.getMessage();
			OnInit_Data_Access.ErrorFileWriter(this.context, "Contract Image Error", Error);
			OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
		}

	}

}