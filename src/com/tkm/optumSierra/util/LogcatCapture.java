package com.tkm.optumSierra.util;

import android.os.Environment;
import android.os.Handler;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by kerolos.bisheer on 5/4/2017.
 * captures logcat logs
 */

public class LogcatCapture {

    private static final String TAG = "LogcatCapture";
    private static Handler loggingHandler = new Handler();
    public static void startCapturing() {
        if (Log.FILE_LOG_ACTIVE) {
            loggingHandler.post(new Runnable() {
                @Override
                public void run() {
                    executeLogcat();
                }
            });
        }
    }

    //https://cmanios.wordpress.com/2012/05/15/check-network-connectivity-and-obtain-wifi-mac-address-in-android/
    private static void executeLogcat(){
        File logFile = getLogFile(); // log file name
        int sizePerFile = 10000; // size in kilobytes
        int rotationCount = 500; // file rotation count
        String filter = "D"; // Debug priority
        Log.d(TAG , "executeLogcat: logcat logging is about to start" );
        String[] args = new String[] { "logcat",
                "-v", "time",
                "-f",logFile.getAbsolutePath(),
                "-r", Integer.toString(sizePerFile),
                "-n", Integer.toString(rotationCount),
                "*:" + filter };

        try {
            Runtime.getRuntime().exec(args);
        } catch (IOException e) {
            Log.d(TAG , "executeLogcat: logcat logging error starting" );
            e.printStackTrace();
        }
    }

    private static File logFile;

    private static File getLogFile() {

        if (logFile == null) {
            File appDirectory = new File(Environment.getExternalStorageDirectory() + "/OptumFolder");
            File logDirectory = new File(appDirectory + "/logcat");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HH_mm_ss_SSS", Locale.getDefault());

            logFile = new File(logDirectory, "logcat" + sdf.format( System.currentTimeMillis() ) + ".txt");

            // create app folder
            if (!appDirectory.exists()) {
                appDirectory.mkdir();
            }

            // create log folder
            if (!logDirectory.exists()) {
                logDirectory.mkdir();
            }

        }

        return logFile;
    }
}
