package com.tkm.optumSierra.util;

import java.text.DecimalFormat;

//This Program will display the given number in words from 0 to 999999999

public class NumberToString {

	public enum hundreds {
		onehundred, twohundred, threehundred, fourhundred, fivehundred, sixhundred, sevenhundred, eighthundred, ninehundred
	}

	public enum tens {
		twenty, thirty, forty, fifty, sixty, seventy, eighty, ninety
	}

	public enum ones {
		one, two, three, four, five, six, seven, eight, nine
	}

	public enum denom {
		thousand, lakhs, crores
	}

	public enum splNums {
		ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen, seventeen, eighteen, nineteen
	}

	// public double number = 0.0;
	public String text = "";

//	public static void main(String[] args) {
////		double no = 2345.01;
////		NumberToString nts = new NumberToString();
////		System.out.println("" + no + " : " + nts.getNumberToString(no));
////		System.out.println("23.99 : " + nts.getNumberToString(23.99));
////		System.out.println("508.77 : " + nts.getNumberToString(508.77));
////		System.out.println("9001.34 : " + nts.getNumberToString(9001.34));
////		System.out.println("9999.10 : " + nts.getNumberToString(9999.10));
////		System.out.println("9090.09 : " + nts.getNumberToString(9090.09));
//	}

	public NumberToString() {

	}

	public String getNumberToString(double number) {
		text = "";
		if (number > 9999 && number < 0) {
			System.out.println("Invalid Range!");
			return "";
		}

		long num = (long) number;
		double dec = round((number - num));
		// System.out.println("DEC "+dec+ " Number "+number + " "+num+
		// " "+(number - num));
		String numberToStr = getNumberToStringLong(num) + " point "
				+ getDigits(dec);
		
		/*String numberToStr = text + " point "
				+ getDigits(dec);*/
		return numberToStr;
	}
	
	public double round(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.#");
        String x=twoDForm.format(d);
		x=x.replace(",", ".");
		return Double.valueOf(x);
      
}


	public String getDigits(double dec) {
		// System.out.println("DEC "+dec);
		String str = "" + dec;
		String result = "";
		if (dec == 0) {
			result = "Zero";
			return result;
		}

		int digit1 = 0;
		int digit2 = 0;

		if (str.length() == 3) {
			digit1 = Integer.parseInt("" + str.charAt(2));
		} else if (str.length() > 3) {
			digit1 = Integer.parseInt("" + str.charAt(2));
			digit2 = Integer.parseInt("" + str.charAt(3));
		}

		// System.out.println("Digits " +digit1 + " "+digit2);
		if (digit1 == 0) {
			result = "zero";
		} else {
			result = "" + ones.values()[digit1 - 1];
		}

		if (digit2 == 0) {
			result = result + " zero";
		} else {
			result = result + " " + ones.values()[digit2 - 1];
		}

		return result;
	}

	public String getNumberToStringLong(long num) {
		int rem = 0;
		int i = 0;
		while (num > 0) {
			if (i == 0) {
				rem = (int) (num % 1000);
				printText(rem);
				num = num / 1000;
				i++;
			} else if (num > 0) {
				rem = (int) (num % 100);
				if (rem > 0)
					text = denom.values()[i - 1] + " " + text;
				printText(rem);
				num = num / 100;
				i++;
			}
		}
		if (i > 0)
		{
			String final_val=text.substring(text.length()-4, text.length());
			
			if(final_val.trim().equals("and")) // if the digit is next hundread vales, remove the additional "and" value
				text=text.substring(0, text.length()-4);
					
			//if(text.)
			return text;
		}
		else
			return "zero";
	}

	public void printText(int num) {

		if (!(num > 9 && num <= 19)) {
			if ((num % 100 > 9 && num % 100 <= 19)) {
				
				
				if (num % 100 > 0)

					getSplNums(num % 10);

				num = num / 100;
				if (num > 0)
					getHundreds(num);

			} else {
				if (num % 10 > 0)

					getOnes(num % 10);

				num = num / 10;
				if (num % 10 > 0)

					getTens(num % 10);

				num = num / 10;
				if (num > 0)
					getHundreds(num);
			}
		} else {

			getSplNums(num % 10);
		}

	}

	public void getSplNums(int num) {
		text = splNums.values()[num] + " " + text;
	}

	public void getHundreds(int num) {
		text = hundreds.values()[num - 1] + " and " + text;
	}

	public void getTens(int num) {
		text = tens.values()[num - 2] + " " + text;
	}

	public void getOnes(int num) {
		text = ones.values()[num - 1] + " " + text;
	}

}