package com.tkm.optumSierra.util;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.tkm.optumSierra.util.Log;


public class OmronPinReceiver extends BroadcastReceiver {

	//	public static interface OnPinEnteredSuccessfully {
	//		void pinEnteredSuccessfully ();
	//	}
	public static final String EXTRA_DEVICE = "android.bluetooth.device.extra.DEVICE";
	static String pin = "0000";

	//private OnPinEnteredSuccessfully mOnPinEnteredSuccessfully;

	//	public void setOnPinEnteredSuccessfully(OnPinEnteredSuccessfully onPinEnteredSuccessfully){
	//		mOnPinEnteredSuccessfully = onPinEnteredSuccessfully;
	//	}


	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();

		pin=Constants.getPIN();
		Log.i("Receiver", "Broadcast received PIN: " + pin);

		if (action.equals("com.blutooth.setpin")) {
			String state = intent.getExtras().getString("pin");
			pin = state;

		} else if (action
				.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {

			BluetoothDevice device = intent.getParcelableExtra(EXTRA_DEVICE);

			try {
				if (!Build.MODEL.equals("VK815")){

					device.getClass().getMethod("setPairingConfirmation",
							boolean.class).invoke(device, true);
				}
				// convert pin to bytes
				byte[] pinBytes = pin.getBytes("UTF8");

				// call method settPin by reflection
				Method ms = device.getClass().getMethod("setPin", byte[].class);
				ms.invoke(device, pinBytes);

				//				if(mOnPinEnteredSuccessfully != null){
				//					mOnPinEnteredSuccessfully.pinEnteredSuccessfully();
				//				}

			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}