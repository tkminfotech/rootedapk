package com.tkm.optumSierra.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.TelephonyManager;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.R;
import com.tkm.optumSierra.bean.Class_Incident;
import com.tkm.optumSierra.dal.Incident_Log_db;

public class OnInit_Data_Access {

	public static String TAG 		= "OnInit_Data_Access";
	final static Uri APN_TABLE_URI 	= Uri.parse("content://telephony/carriers");
	final static Uri PREF_APN_URI 	= Uri.parse("content://telephony/carriers/preferapn");
	private static final int	ACTION_ENABLE			= 1;
	private static final int	ACTION_DISABLE			= 0;
	private static final String	CMD_ENABLE_AIRPLANE_MODE		= "com.prtl724.p724_lga.CMD_ENABLE_AIRPLANE_MODE";
	private static final String	CMD_DISABLE_AIRPLANE_MODE		= "com.prtl724.p724_lga.CMD_DISABLE_AIRPLANE_MODE";

	public static Context ctx;

	public void FileReaderMethod() {
		// Find the directory for the SD Card using the API
		File sdcard = Environment.getExternalStorageDirectory();

		// Get the text file
		File file = new File(sdcard, "/versions/tablet_incident.log");
		if (file.length() == 0) {
			// empty or doesn't exist
			Log.i(TAG, "#FileReaderMethod NO FILE FOUND");
		} else {
			Log.i(TAG, "#FileReaderMethod FILE FOUND");
			// exists and is not empty
			// Read text from file
			StringBuilder text = new StringBuilder();

			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;

				while ((line = br.readLine()) != null) {
					text.append(line);
					text.append('\n');
				}
				br.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void ErrorFileWriter(Context ctx, String title, String content) {
		try {
			// Find the directory for the SD Card using the API
			File sdcard = Environment.getExternalStorageDirectory();
			content = content.replace("com.tkm.optumSierra", "");

			File file = new File(sdcard, "/versions/tablet_incident.log");
			StringBuilder text = new StringBuilder();
			if (file.length() == 0) {
				// empty or doesn't exist
				// if file doesnt exists, then create it
				Log.i("OnInit_Data_Access", "#ErrorFileWriter NO FILE FOUND");
				if (!file.exists()) {
					file.createNewFile();
				}
			} else {
				// exists and is not empty
				Log.i("OnInit_Data_Access", "#ErrorFileWriter FILE FOUND");
				text.append("\n");
			}
			text.append("[" + Util.get_patient_time_zone_time(ctx) + "] ");
			text.append("[" + title + "] ");
			text.append("[" + content + "]");

			FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(text.toString());
			bw.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void insert_incident_detail(String message,
			int incident_type_id) {
		
		message = message.replace("com.tkm.optumSierra", "");
		Incident_Log_db db_incident = new Incident_Log_db(ctx);

		/*
		 * SimpleDateFormat sdf = new
		 * SimpleDateFormat("MM/dd/yyyy HH:mm:ss aa"); Date systemDate =
		 * Calendar.getInstance().getTime();
		 */
		String current_time = Util.get_patient_time_zone_time(ctx);

		Class_Incident class_incident = new Class_Incident();
		class_incident.set_incident_type_id(incident_type_id); // incident type
		// id from
		// table; now
		// random number
		// for testing
		class_incident.set_incident_date(current_time);
		class_incident.set_incident_msg(message); // incident message; currently
		// hard-coded
		class_incident.set_patient_id(get_patient_id()); // patient id hard-coded
		class_incident.set_tablet_imei(get_imei());
		class_incident.set_app_version_number(get_app_version_nummber());
		class_incident.set_app_used_bandwidth(get_app_used_bandwidth());
		class_incident.set_sim_card_in(get_sim_card_in());
		class_incident.set_sim_card_status(get_sim_card_status());
		class_incident.set_cell_network_type(get_network_type());
		class_incident.set_cell_network_provider(get_network_provider());
		class_incident.set_signal_strength(Integer.parseInt(get_signal_strength(1)));
		class_incident.set_signal_strength_dbm(Integer.parseInt(get_signal_strength(2)));
		class_incident.set_signal_strength_asu(Integer.parseInt(get_signal_strength(3)));
		class_incident.set_apn_name(get_current_apn_name(get_pref_apn_id()));
		class_incident.set_apn_value(get_current_apn_value(get_pref_apn_id()));
		class_incident.set_tablet_os_version(get_tablet_os_version());
		class_incident.set_tablet_os_build_number(get_tablet_os_build_number());
		class_incident.set_system_up_time(get_system_up_time());
		class_incident.set_power_adapter_on(get_power_adapter_on());
		class_incident.set_battery_level(get_battery_level());
		class_incident.set_airplane_mode_on(get_airplanemode_on());
		class_incident.set_wifi_on(get_wifi_on());
		class_incident.set_bluetooth_on(get_bluetooth_on());
		class_incident.set_created_date(current_time);

		db_incident.insert_incident_log(class_incident);
	}

	public static int get_patient_id()
	{
		SharedPreferences settings = ctx.getSharedPreferences(CommonUtilities.USER_SP, 0);
		return Integer.parseInt(settings.getString("patient_id", "0"));
	}

	public static String get_network_type() {

		ConnectivityManager connMgr = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifi.isConnectedOrConnecting()) {
			return ctx.getResources().getString(R.string.lbl_wifi);
		} else if (get_sim_card_status() == 1) {

			TelephonyManager tel_manager;
			tel_manager = (TelephonyManager) ctx
					.getSystemService(Context.TELEPHONY_SERVICE);

			switch (tel_manager.getNetworkType()) {
			case TelephonyManager.NETWORK_TYPE_CDMA:
			case TelephonyManager.NETWORK_TYPE_1xRTT:
			case TelephonyManager.NETWORK_TYPE_IDEN:
				return "2G";
			case TelephonyManager.NETWORK_TYPE_GPRS:
			case TelephonyManager.NETWORK_TYPE_EDGE:
			case TelephonyManager.NETWORK_TYPE_UMTS:
			case TelephonyManager.NETWORK_TYPE_EVDO_0:
			case TelephonyManager.NETWORK_TYPE_EVDO_A:
			case TelephonyManager.NETWORK_TYPE_HSDPA:
			case TelephonyManager.NETWORK_TYPE_HSUPA:
			case TelephonyManager.NETWORK_TYPE_HSPA:
			case TelephonyManager.NETWORK_TYPE_EVDO_B:
			case TelephonyManager.NETWORK_TYPE_EHRPD:
			case TelephonyManager.NETWORK_TYPE_HSPAP:
				return "3G";
			case TelephonyManager.NETWORK_TYPE_LTE:
				return "LTE";
			default:
				return "3G";
			}

		} else {
			return ctx.getResources().getString(R.string.lbl_no_service);
		}
	}

	private static String get_app_version_nummber() {
		String version_number = "";
		Resources res = null;
		CharSequence s = null;

		try {
			// I want to use the clear_activities string in Package
			// com.android.settings
			res = ctx.getPackageManager().getResourcesForApplication(
					"com.tkm.optumSierra");
			int resourceId = res.getIdentifier(
					"com.tkm.optumSierra:string/version", null, null);
			if (0 != resourceId) {
				s = ctx.getPackageManager().getText("com.tkm.optumSierra",
						resourceId, null);
				Log.i(TAG, "resource=" + s);
			}
		} catch (NameNotFoundException e) {
			// e.printStackTrace();
			version_number = " -";
		}
		if (s != null)
			version_number = s.toString();
		return version_number;
	}

	private static int get_app_used_bandwidth() {
		int total = 0;
		final PackageManager pm = ctx.getPackageManager();
		ApplicationInfo app = null;
		try {
			app = pm.getApplicationInfo("com.tkm.optumSierra", 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			return 0;
		}
		String name = (String) pm.getApplicationLabel(app);
		// internet usage for particular app(sent and received)
		double received = (double) TrafficStats.getUidRxBytes(app.uid); // (1024
		// *
		// 1024);
		double send = (double) TrafficStats.getUidTxBytes(app.uid); // (1024 *
		// 1024);
		total = (int) (received + send);
		return total;
	}

	private static boolean get_sim_card_in() {
		TelephonyManager tm = (TelephonyManager) ctx
				.getSystemService(Context.TELEPHONY_SERVICE); // gets the
		// current
		// TelephonyManager
		if (tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT)
			return false;// "Not Present";
		else
			return true;// "Present";
	}

	private static int get_sim_card_status() {
		int status = 0; // default as unknown
		TelephonyManager telMgr = (TelephonyManager) ctx
				.getSystemService(Context.TELEPHONY_SERVICE);
		int simState = telMgr.getSimState();
		switch (simState) {
		case TelephonyManager.SIM_STATE_ABSENT:
			status = 0;
			break;
		case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
			status = 0;
			break;
		case TelephonyManager.SIM_STATE_PIN_REQUIRED:
			status = 0;
			break;
		case TelephonyManager.SIM_STATE_PUK_REQUIRED:
			status = 0;
			break;
		case TelephonyManager.SIM_STATE_READY:
			status = 1;
			break;
		case TelephonyManager.SIM_STATE_UNKNOWN:
			status = 0;
			break;
		}
		return status;
	}

	private static String get_tablet_os_version() {
		return Build.VERSION.RELEASE;
	}

	private static String get_tablet_os_build_number() {
		return Build.DISPLAY;
	}

	private static boolean get_power_adapter_on() {
		Intent intent = ctx.registerReceiver(null, new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED));
		int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
		if (plugged == BatteryManager.BATTERY_PLUGGED_AC
				|| plugged == BatteryManager.BATTERY_PLUGGED_USB)
			return true;// "Connected";
		else
			return false;// "Disconnected";

	}

	private static int get_battery_level() {
		IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		Intent batteryStatus = ctx.registerReceiver(null, ifilter);
		int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		return level;

	}

	private static boolean get_wifi_on() {
		WifiManager wifi = (WifiManager) ctx
				.getSystemService(Context.WIFI_SERVICE);
		if (wifi.isWifiEnabled()) {
			return true;// "Enabled";
		} else {
			return false;// "Disabled";
		}
	}

	private static boolean get_bluetooth_on() {
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
				.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
			return false;// "Not supported";
		} else {
			if (!mBluetoothAdapter.isEnabled()) {
				return false;// "Disabled";
			} else
				return true;// "Enabled";
		}
	}

	public static String get_current_apn_name(int _id) {
		String name = "null";
		Cursor cursor = null;
		try {

			final Uri APN_TABLE_URI = Uri.parse("content://telephony/carriers");
			final Uri PREFERRED_APN_URI = Uri
					.parse("content://telephony/carriers/preferapn");
			Cursor c = ctx.getContentResolver().query(PREFERRED_APN_URI, null,
					null, null, null);
			c.moveToFirst();
			int index = c.getColumnIndex("_id"); // getting index of required
			// column
			Short id = c.getShort(index); // getting APN's id from

			// we can get APN name by the same way
			index = c.getColumnIndex("name");
			String name1 = c.getString(index);

			cursor = ctx.getContentResolver().query(APN_TABLE_URI,
					new String[] { "name" }, "_id = ? and current = ?",
					new String[] { Integer.toString(_id), "1" }, null);

			if (cursor != null) {
				cursor.moveToFirst();

				name = cursor.getString(cursor.getColumnIndex("name"));
			}
		}

		catch (Exception e) {
			Log.d(TAG, "get_current_apn_name: " + e.getMessage());
		}

		if (cursor != null) {
			cursor.close();
			cursor = null;
		}

		Log.d(TAG, "get_current_apn_name: " + name);

		return name;
	}

	public static String get_current_apn_value(int _id) {
		String apn_value = "null";

		Cursor cursor = null;

		try {
			cursor = ctx.getContentResolver().query(APN_TABLE_URI,
					new String[] { "apn" }, "_id = ? and current = ?",
					new String[] { Integer.toString(_id), "1" }, null);

			if (cursor != null) {
				cursor.moveToFirst();

				apn_value = cursor.getString(cursor.getColumnIndex("apn"));
			}
		} catch (Exception e) {
			Log.d(TAG, "get_current_apn_value: " + e.getMessage());
		}
		if (cursor != null) {
			cursor.close();
			cursor = null;
		}
		Log.d(TAG, "get_current_apn_value: " + apn_value);

		return apn_value;
	}

	public static int get_pref_apn_id() {
		int _id = -1;
		Cursor cursor = null;
		try {
			cursor = ctx.getContentResolver().query(PREF_APN_URI,
					new String[] { "_id" }, null, null, null);

			if (cursor != null) {
				cursor.moveToFirst();

				_id = cursor.getInt(cursor.getColumnIndex("_id"));
			}
		} catch (Exception e) {
			Log.d(TAG, "get_pref_apn_id: " + e.getMessage());
		}

		if (cursor != null) {
			cursor.close();
			cursor = null;
		}
		Log.d(TAG, "get_pref_apn_id: " + _id);

		return _id;
	}

	private static long get_system_up_time() {
		long system_start_time = 0;

		try {

			long differ = SystemClock.uptimeMillis();

			/*
			 * String diffe = String.format("%02d min, %02d sec",
			 * TimeUnit.MILLISECONDS.toMinutes(differ),
			 * TimeUnit.MILLISECONDS.toSeconds(differ) -
			 * TimeUnit.MINUTES.toSeconds
			 * (TimeUnit.MILLISECONDS.toMinutes(differ)), Locale.ENGLISH);
			 * Log.e(TAG, "Time Diff : " + diffe );
			 */
			system_start_time = differ / 1000;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return system_start_time;
	}

	private static String get_network_provider() {
		TelephonyManager tel_manager;
		tel_manager = (TelephonyManager) ctx
				.getSystemService(Context.TELEPHONY_SERVICE);
		String simeopratorname = tel_manager.getSimOperatorName();
		return simeopratorname;
	}

	private static String get_imei() {
		String imei = "";
		try {
			TelephonyManager tManager = (TelephonyManager) ctx
					.getSystemService(Context.TELEPHONY_SERVICE);
			imei = tManager.getDeviceId();
			if (imei.trim().length() > 14)
				imei = imei.substring(0, 14);

		} catch (Exception e) {

		}
		return imei;
	}

	@SuppressLint("NewApi")
	private static String get_signal_strength(int type) {

		String signal_strength = ": -";

		try {			

			if (type == 1) {
				ConnectivityManager connMgr = (ConnectivityManager) ctx
						.getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo wifi = connMgr
						.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
				if (wifi.isConnectedOrConnecting()) {
					String connectivity_context = Context.WIFI_SERVICE;
					WifiManager wifi_manager = (WifiManager) ctx
							.getSystemService(connectivity_context);
					WifiInfo info = wifi_manager.getConnectionInfo();
					Log.i(TAG, "wifi strngth :- " + info.getLinkSpeed()
							+ " rssi : " + info.getRssi());
					return info.getLinkSpeed() + "";
				}
			}


			int strength = 0;
			int strength_dbm = 0;
			int strength_asu = 0;

			TelephonyManager tManager = (TelephonyManager) ctx
					.getSystemService(Context.TELEPHONY_SERVICE);
			List<CellInfo> cellInfos = tManager.getAllCellInfo(); // This will give
			// info of all
			// sims present
			// inside your
			// mobile
			if (cellInfos != null) {
				for (int i = 0; i < cellInfos.size(); i++) {
					if (cellInfos.get(i).isRegistered()) {
						if (cellInfos.get(i) instanceof CellInfoWcdma) {
							CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) tManager
									.getAllCellInfo().get(0);
							CellSignalStrengthWcdma cellSignalStrengthWcdma = cellInfoWcdma
									.getCellSignalStrength();
							strength_dbm = cellSignalStrengthWcdma.getDbm();
							strength_asu = cellSignalStrengthWcdma.getAsuLevel();
							strength = get_signal_bar_3g(strength_dbm);
						} else if (cellInfos.get(i) instanceof CellInfoGsm) {
							CellInfoGsm cellInfogsm = (CellInfoGsm) tManager
									.getAllCellInfo().get(0);
							CellSignalStrengthGsm cellSignalStrengthGsm = cellInfogsm
									.getCellSignalStrength();
							strength_dbm = cellSignalStrengthGsm.getDbm();
							strength_asu = cellSignalStrengthGsm.getAsuLevel();
							strength = get_signal_bar_3g(strength_dbm);
						} else if (cellInfos.get(i) instanceof CellInfoLte) {
							CellInfoLte cellInfoLte = (CellInfoLte) tManager
									.getAllCellInfo().get(0);
							CellSignalStrengthLte cellSignalStrengthLte = cellInfoLte
									.getCellSignalStrength();
							strength_dbm = cellSignalStrengthLte.getDbm();
							strength_asu = cellSignalStrengthLte.getAsuLevel();
							strength = get_signal_bar_4g(strength_dbm);
						}
					}
				}
			}

			if (type == 1)
				signal_strength = strength + "";
			else if (type == 2)
				signal_strength = strength_dbm + "";
			else if (type == 3)
				signal_strength = strength_asu + "";
			else
				signal_strength = ": " + strength + " bar " + strength_dbm
				+ " dbm " + strength_asu + " asu";

			Log.e(TAG, signal_strength);

		} catch (Exception e) {
			// TODO: handle exception
		}

		return signal_strength;
	}

	private static int get_signal_bar_3g(int dbm_vlaue) {
		int bar_count = 0;
		if (dbm_vlaue > -70)
			bar_count = 4; // excelent
		else if (dbm_vlaue < -70 && dbm_vlaue > -85)
			bar_count = 3; // good
		else if (dbm_vlaue < -86 && dbm_vlaue > -100)
			bar_count = 2; // fair
		else if (dbm_vlaue < -100)
			bar_count = 1; // poor

		return bar_count;
	}

	private static int get_signal_bar_4g(int dbm_vlaue) {
		int bar_count = 0;
		if (dbm_vlaue > -90)
			bar_count = 4; // excelent
		else if (dbm_vlaue < -90 && dbm_vlaue > -105)
			bar_count = 3; // good
		else if (dbm_vlaue < -106 && dbm_vlaue > -120)
			bar_count = 2; // fair
		else if (dbm_vlaue < -120)
			bar_count = 1; // poor

		return bar_count;
	}

	private static boolean get_airplanemode_on() {

		boolean airplane_is_enabled = Settings.System.getInt(
				ctx.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;

		if (airplane_is_enabled) {

			if(Util.is_lg()) // if LGE
			{
				toggle_airplane_mode(ACTION_ENABLE);
			}
			else
			{
				// turn off airplane mode
				try {
					Process su_proc;
					su_proc = Runtime.getRuntime().exec("su");
					DataInputStream dis_start = new DataInputStream(su_proc.getInputStream());
					DataOutputStream dos_start = new DataOutputStream(su_proc.getOutputStream());

					if (dis_start != null && dos_start != null) {
						dos_start.writeBytes("settings put global airplane_mode_on 0 \n");
						dos_start.flush();
					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
			}
		}
		// confirm the status of airplane mode agin
		airplane_is_enabled = Settings.System.getInt(ctx.getContentResolver(),Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
		return airplane_is_enabled;

	}

	private static void toggle_airplane_mode(int status)
	{

		if (status == ACTION_DISABLE)
		{
			Log.e(TAG, "Enabled Airplane Mode");
			ctx.sendOrderedBroadcast(new Intent(CMD_ENABLE_AIRPLANE_MODE), null, new BroadcastReceiver()
			{
				@Override
				public void onReceive(Context context, Intent intent)
				{

					String responce = getResultData(); 

					if (responce != null)
					{
						if (responce.equals("-1"))
						{	
							//show_responce(responce);
						}
					}
					else
					{
						//show_responce(responce);
					}
				}
			}, null, 0, null, null);

		}
		else if (status == ACTION_ENABLE)
		{
			Log.e(TAG, "Disabled Airplane Mode");
			ctx.sendOrderedBroadcast(new Intent(CMD_DISABLE_AIRPLANE_MODE), null, new BroadcastReceiver()
			{
				@Override
				public void onReceive(Context context, Intent intent)
				{

					String responce = getResultData(); 

					if (responce != null)
					{
						if (responce.equals("-1"))
						{	
							String Error = this.getClass().getName() + " : " + responce;
							OnInit_Data_Access.ErrorFileWriter(ctx, "AirplaneMode Disable Error", Error);
							OnInit_Data_Access.insert_incident_detail(Error, Constants.Disabling_Airplane_mode);
						}
					}
					else
					{
						String Error = this.getClass().getName() + " : NULL";
						OnInit_Data_Access.ErrorFileWriter(ctx, "AirplaneMode Disable Error", Error);
						OnInit_Data_Access.insert_incident_detail(Error, Constants.Disabling_Airplane_mode);
					}
				}
			}, null, 0, null, null);
		}
	}

}