package com.tkm.optumSierra.util;

import java.lang.reflect.Method;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.tkm.optumSierra.util.Log;

public class PINReceiver extends BroadcastReceiver {

	public static final String EXTRA_DEVICE = "android.bluetooth.device.extra.DEVICE";
	static String pin = "0000";
	private String TAG = "PINReceiver";
	@SuppressLint("NewApi")
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();

		pin=Constants.getPIN();
		//Log.i("Receiver", "Broadcast received PIN: " + pin);

		if (action.equals("com.blutooth.setpin")) {
			String state = intent.getExtras().getString("pin");
			pin = state;

		} else if (action
				.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {

			BluetoothDevice device = intent.getParcelableExtra(EXTRA_DEVICE);

			try {
				// convert pin to bytes
				byte[] pinBytes = pin.getBytes("UTF8");

				boolean isSet;
				boolean isConfirmed;

				// Bluetooth methods were added in API 19
				// use the methods directly if at or above 19
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
					isSet = device.setPin(pinBytes);
					isConfirmed = device.setPairingConfirmation(true);


				} else { // else use reflection to get the job done.
					Method ms = device.getClass().getMethod("setPin", byte[].class);
					isSet = (Boolean) ms.invoke(device, pinBytes);

					isConfirmed = (Boolean) device.getClass().getMethod("setPairingConfirmation", boolean.class).invoke(device, true);
				}
				Log.d(TAG, "onReceive: is pin set: " + isSet);

				// confirm bond
				Log.d(TAG, "onReceive: is pairing confirmed: " + isConfirmed);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}