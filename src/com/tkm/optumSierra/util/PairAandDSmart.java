package com.tkm.optumSierra.util;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.tkm.optumSierra.AandDSmart;
import com.tkm.optumSierra.TestDeviceAandDSmart;

import java.lang.ref.WeakReference;

public class PairAandDSmart  extends BroadcastReceiver {

	// interface defines the connect method which calls connectGatt on the passed bluetooth device
	public interface GattConnectable {
		void connect(BluetoothDevice device);
	}

	WeakReference<GattConnectable> mAnD; // to protect against circular references

	public PairAandDSmart(GattConnectable andObject) {
		mAnD = new WeakReference<GattConnectable> (andObject);
	}

	public static final String EXTRA_DEVICE = "android.bluetooth.device.extra.DEVICE";
	private static final String TAG="PairAandDSmart";

	public void onReceive(Context context, Intent intent) {

		String action = intent.getAction();

		Log.d(TAG, "onReceive: action: " + action);

		if (action.equals("com.blutooth.setpin")) {
			Log.d(TAG, "onReceive: set pin action received");

		} else if (action
				.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {

			BluetoothDevice device = intent.getParcelableExtra(EXTRA_DEVICE);
			setBluetoothPairingPin(device);

		} else if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {

			if (mAnD != null && mAnD.get() != null) {
				final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
				BluetoothDevice currentDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

				if (state == BluetoothDevice.BOND_BONDED) {
					Log.d(TAG, "onReceive: BOND_BONDED");

					// with the P724 tablet, it seems that the connection works better if we don't connect after pairing.
					// strangely, we get an 'ACTION_BOND_STATE_CHANGED' with all connections even AFTER pairing!
					// there's an explicit check in the A&D activity for P724 pairing...We make the connection there instead.
					if (!Build.MODEL.equals("MN-724"))
						mAnD.get().connect(currentDevice);
				}
			}
		}

	}

	public void setBluetoothPairingPin(BluetoothDevice device)
	{ 
			try {				
				device.getClass().getMethod("setPairingConfirmation", boolean.class).invoke(device, true);
				Log.d(TAG, "Success to setPairingConfirmation for a and d smart.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			} 
	}
}