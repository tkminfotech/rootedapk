package com.tkm.optumSierra.util;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.tkm.optumSierra.Regular_AlarmReceiver;

public class Regular_Alam_util {

	private static PendingIntent pendingIntent;
	private static AlarmManager manager;
	private static int mAlarmId = 10002;

	/**
	 * Start Alarm
	 * 
	 * @param context
	 * @param duration
	 */
	@SuppressLint("NewApi")
	public static void startAlarm(Context context) {
		Intent alarmIntent = new Intent(context, Regular_AlarmReceiver.class);
		pendingIntent = PendingIntent.getBroadcast(context, mAlarmId,
				alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		manager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		long interval = (5 * 60 * 1000);
		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(System.currentTimeMillis());

		Calendar futureDate = Calendar.getInstance();
		futureDate.add(Calendar.SECOND, (int) (interval / 1000));

		if (android.os.Build.VERSION.SDK_INT >= 19) {
			manager.setExact(AlarmManager.RTC_WAKEUP, futureDate.getTime()
					.getTime(), pendingIntent);
		} else {
			manager.setRepeating(AlarmManager.RTC_WAKEUP,
					now.getTimeInMillis(), interval, pendingIntent);
		}

	}

	/**
	 * Cancel Alarm
	 * 
	 * @param context
	 */
	public static void cancelAlarm(Context context) {
		if (manager != null) {
			manager.cancel(pendingIntent);
		} else {
			manager = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			Intent alarmIntent = new Intent(context,
					Regular_AlarmReceiver.class);
			pendingIntent = PendingIntent.getBroadcast(context, mAlarmId,
					alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			manager.cancel(pendingIntent);
		}
	}
}