package com.tkm.optumSierra.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.bean.ClassMeasurement;
import com.tkm.optumSierra.bean.ClassTemperature;
import com.tkm.optumSierra.bean.ClassWeight;
import com.tkm.optumSierra.bean.ClasssPressure;
import com.tkm.optumSierra.dal.Glucose_db;
import com.tkm.optumSierra.dal.MeasureType_db;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.dal.Pulse_db;
import com.tkm.optumSierra.dal.Question_skip_db;
import com.tkm.optumSierra.dal.Questions_db;
import com.tkm.optumSierra.dal.Temperature_db;
import com.tkm.optumSierra.dal.Weight_db;

public class SkipVitals {

	public static void skip_patient_vitalse(Context context) {

		System.out.println("-------------------------SKIP VITALS------------------------------");
		insert_skip_values(context);
		
	}

	private static void insert_skip_values(Context context) {
		MeasureType_db dbsensor = new MeasureType_db(
				context);		
		Cursor cSensor = dbsensor.Select_skipped_vitals();
		cSensor.moveToFirst();
		if (cSensor.getCount() > 0) {
			while (cSensor.isAfterLast() == false) {

				insert_timed_out_vitals(Integer.parseInt(cSensor.getString(2)),context);
				
				Log.e("Sierra", " checking  skipped vitals id-"+""+Integer.parseInt(cSensor.getString(2)));
				
				cSensor.moveToNext();

			}
		}
		cSensor.close();
		dbsensor.close();
		dbsensor.updateStatusToZero();
		SharedPreferences settings =context.getSharedPreferences(
				CommonUtilities.SERVER_URL_SP, 0);	
				String posturl = settings.getString("Server_post_url", "-1");
				Log.e("SkipVitals", "------------------ flow.");
		new UploadVitalReadingsTask(context, posturl).execute();
	}
	public void uploadSkipVitals()
	{
		Log.e("SkipVitals", "Uploading Skip Alarm Details");
	}
	private static void insert_timed_out_vitals(int measuretype_id, Context context) {	

		/*
		 * 1.glucose 2.Oxygen 3.pressure 4.pt/Inr 5.-- 6.Temperature 7.Weight
		 * 8.fluid level 11.Pain Level 12. Peask flow 101. Q tree 102. Qcalender
		 */
		Log.e("Sierra", "inserting measuretype_id as skipped" + measuretype_id);

		SharedPreferences settings1 = context.getSharedPreferences(
				CommonUtilities.PREFS_NAME_date, 0);
		String sectiondate = settings1.getString("sectiondate", "0");
		
		String currentDateandTime = TestDate.getCurrentTime();
		SharedPreferences settings3 = context.getSharedPreferences(
				CommonUtilities.USER_SP, 0);
		int Patientid = Integer.parseInt(settings3.getString("patient_id", ""));
		Constants.setPatientid(Patientid);
		switch (measuretype_id) {

		case 1:

			Glucose_db dbcreateglucose = new Glucose_db(context.getApplicationContext());
			ClassMeasurement glucose = new ClassMeasurement();
			glucose.setLast_Update_Date(currentDateandTime);
			glucose.setStatus(0);
			glucose.setGlucose_Value(-101);
			glucose.setSectionDate(sectiondate);
			glucose.setPrompt_flag("3");
			glucose.setInputmode(-1);
			dbcreateglucose.InsertGlucoseMeasurement_skip(glucose);
			Log.e("Sierra", "inserting glucose as skipped");

			break;

		case 2:

			Pulse_db dbcreate1 = new Pulse_db(context);
			ClassMeasurement measurement = new ClassMeasurement();
			measurement.setStatus(0);
			measurement.setOxygen_Value(-101);
			measurement.setLast_Update_Date(currentDateandTime);
			measurement.setPressure_Value(-101);
			measurement.setInputmode(-1);
			measurement.setSectionDate(sectiondate);
			dbcreate1.InsertOxymeterMeasurement_skip(measurement);
			Log.e("Sierra", "inserting pulse as skipped");

			break;

		case 3:
			Pressure_db dbcreatepressure = new Pressure_db(context);
			ClasssPressure pressure = new ClasssPressure();
			pressure.setPatient_Id(Patientid);
			pressure.setSystolic(-101);
			pressure.setDiastolic(-101);
			pressure.setPulse(-101);
			pressure.setInputmode(-1);
			pressure.setSectionDate(sectiondate);
			pressure.setPrompt_flag("4");
			Log.e("Sierra", "inserting BP as skipped");
			dbcreatepressure.InsertPressure_skip(pressure);

			break;
		// -------------
		case 4:

			break;
		// -------------

		case 6:
			Temperature_db dbcreatetemperature = new Temperature_db(context);
			ClassTemperature temp = new ClassTemperature();
			temp.setLastUpdateDate(currentDateandTime);
			temp.setStatus(0);
			temp.setTemperatureValue(-101 + "");
			temp.setInputmode(-1);
			temp.setSectionDate(sectiondate);
			dbcreatetemperature.InsertTemperatureMeasurement_skip(temp);
			Log.e("Sierra", "inserting temp as skipped");

			break;
		// -------------
		case 7:
			Weight_db dbcreateweight = new Weight_db(context);
			ClassWeight weight = new ClassWeight();
			weight.setPatientID(Patientid);
			weight.setWeightInKg(-101 + "");
			weight.setInputmode(-1);
			SharedPreferences settings = context.getSharedPreferences(
					CommonUtilities.USER_TIMESLOT_SP, 0);
			String slot = settings.getString("timeslot", "AM");
			weight.setTimeslot(slot);
			weight.setSectionDate(sectiondate);
			dbcreateweight.InsertWeight_skip(weight);
			Log.e("Sierra", "inserting wt as skipped");
			break;
			
		case 101:
			
			/*Questions_db dbcreate = new Questions_db(context);
			Questions_db dbquestion = new Questions_db(context);
			dbquestion.InsertDmpUserResponse("" + Patientid, "" +-101,					
					"" +-101, -101,					
					currentDateandTime, -101,					
					""+-101, 0, -101, "" +-101,sectiondate,1);*/
			/*String query = "Select a.Item_Number,a.Sequence_Number,a.Measure_Date,a.Patient_Id,a.treeNumber,
			 * a.Item_Content,a.sectionDate from Droid_DMP_User_Response a where a.Status=0";

			Cursor response = dbcreate.SelectDBValues(query);
			if (response.getCount() <= 0) {
				question_skip_db.InsertD_skip_Response(Patientid, sectiondate,currentDateandTime);			
				Log.e("Sierra", "inserting qs as skipped");
			}
			*/ //############# need to test tony
			Question_skip_db question_skip_db= new Question_skip_db(context);
			question_skip_db.InsertD_skip_Response(Patientid, sectiondate,currentDateandTime);			
			Log.e("Sierra", "inserting qs as skipped from reminder flow.");
			break;

		default:

		}

	
	}
	
}
