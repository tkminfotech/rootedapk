package com.tkm.optumSierra.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.tkm.optumSierra.dal.Skip_Reminder_Value_db;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import com.tkm.optumSierra.util.Log;

public class Skip_Reminder_Values_Upload extends
		AsyncTask<String, Void, String> {

	private String TAG = "Skip_Reminder_Values_Upload";
	private String ServerURL;
	public Skip_Reminder_Value_db reminder_skip_db;
	String[] temp1, temp2, temp3;
	int[] temp4;
	private Context context;
	
	public Skip_Reminder_Values_Upload(Context context,	String ServerURL) {
		this.ServerURL = ServerURL;
		this.context = context;
		reminder_skip_db = new Skip_Reminder_Value_db(context);
	}

	@Override
	protected void onPostExecute(String result) {
		Log.i(TAG, "onPostExecute vitals has been updated..");
	}

	protected String doInBackground(String... urls) {

		//String date = TestDate.getCurrentTime();
		reminder_skip_db.open();
		Cursor cursor1 = reminder_skip_db.selectAll();
		temp1 = new String[cursor1.getCount()];
		temp2 = new String[cursor1.getCount()];
		temp3 = new String[cursor1.getCount()];
		temp4 = new int[cursor1.getCount()];
		int j = 0;
	
		while (cursor1.moveToNext()) {
			temp1[j] = cursor1.getString(cursor1.getColumnIndex("patient_id"));
			temp2[j] = cursor1.getString(cursor1.getColumnIndex("time"));
			temp3[j] = cursor1.getString(cursor1.getColumnIndex("status"));
			temp4[j] = Integer.parseInt(cursor1.getString(cursor1.getColumnIndex("id")));
			
			System.out.println("patient_id==="+temp1[j]+"\ntime==="+temp2[j]+"\nstatus"+temp3[j]);
			
			try {
				StringBuilder str = new StringBuilder();
				Log.i(TAG, "Uploading skip data to server started ...");

				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(ServerURL
						+ "/droid_website/reminder_vitals.ashx");

				List<NameValuePair> pairs = new ArrayList<NameValuePair>();

				post.setEntity(new UrlEncodedFormEntity(pairs));
				post.setHeader("patient_id", temp1[j]);
				post.setHeader("section_date", temp2[j]);

				Log.i(TAG,"patient_id"+ temp1[j]);
				Log.i(TAG,"section_date"+temp2[j]);
				
				
				HttpResponse response1 = client.execute(post);

				InputStream in = response1.getEntity().getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(in));
				String line = null;
				while ((line = reader.readLine()) != null) {
					str.append(line + "\n");
				}
				in.close();
				str.toString();
				if (str.length() > 0) {
					DocumentBuilder db = DocumentBuilderFactory
							.newInstance().newDocumentBuilder();
					InputSource is1 = new InputSource();
					is1.setCharacterStream(new StringReader(str.toString()));
					Document doc = db.parse(is1);
					NodeList nodes = doc
							.getElementsByTagName("success_response");
					if (nodes.getLength() == 1) {
						reminder_skip_db.update_db(temp4[j]);
					}

				}

			} catch (Exception e) {
				e.printStackTrace();
				String Error = this.getClass().getName()+ " : "+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(this.context, "Reminder Skip Upload Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
			}
			
			j += 1;
		}
		
		reminder_skip_db.close();
		
		return null;
	}

}