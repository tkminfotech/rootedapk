package com.tkm.optumSierra.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.RebootDataManager;
import com.tkm.optumSierra.service.AlarmService;

public class StartupActivity extends BroadcastReceiver {
	String TAG = "StartupActivity";
	Context ctx;

	@Override
	public void onReceive(Context context, Intent intent) {
		ctx = context;
		Log.i(TAG, " onReceive ");
		try {
			Thread.sleep(600);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			if (isMyReminderServiceRunning(context)) {
				Log.e(TAG, " Stopping AlarmService ");
				context.stopService(new Intent(context, AlarmService.class));

			}
		} catch (Exception e) {
			Log.e(TAG, "exception while starting or closing alam... ");
		}

		OnInit_Data_Access.ErrorFileWriter(context, "system boot",
				"system boot");
		OnInit_Data_Access.insert_incident_detail("system boot",
				Constants.After_the_tablet_starts_up);
		disableAirplaneMode();
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public void disableAirplaneMode() {
		try {
			@SuppressWarnings("unused")
			int airplaneStatus = 0;
			boolean aipPlaneStatus;
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
				aipPlaneStatus = Settings.System.getInt(ctx.getContentResolver(),
						Settings.System.AIRPLANE_MODE_ON, 0) != 0;
			} else {
				aipPlaneStatus = Settings.Global.getInt(ctx.getContentResolver(),
						Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
			}
			Log.i(TAG, "aipPlaneStatus=" + aipPlaneStatus);
			if (aipPlaneStatus) {
				// turn off airplane mode
				if (android.os.Build.VERSION.SDK_INT < 17) {
					try {
						// toggle airplane mode
						Settings.System.putInt(ctx.getContentResolver(),
								Settings.System.AIRPLANE_MODE_ON,
								aipPlaneStatus ? 0 : 1);

						// Post an intent to reload
						Intent intent = new Intent(
								Intent.ACTION_AIRPLANE_MODE_CHANGED);
						intent.putExtra("state", !aipPlaneStatus);
						ctx.sendBroadcast(intent);
					} catch (Exception e) {
						Log.e("exception", e + "");
					}
				} else {
					airplaneStatus = 1;
					try {
						Process su_proc;
						su_proc = Runtime.getRuntime().exec("su");
						DataInputStream dis_start = new DataInputStream(
								su_proc.getInputStream());
						DataOutputStream dos_start = new DataOutputStream(
								su_proc.getOutputStream());

						if (dis_start != null && dos_start != null) {
							dos_start
									.writeBytes("settings put global airplane_mode_on 0 \n");// "settings put global airplane_mode_on 0 \n");
							dos_start.flush();
							dos_start
									.writeBytes("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false");
							dos_start.flush();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			Intent i = new Intent(ctx, RebootDataManager.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra("airplaneStatus", airplaneStatus);
			ctx.startActivity(i);
		} catch (Exception e) {
		}
	}

	private boolean isMyReminderServiceRunning(Context context) {
		ActivityManager manager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.tkm.optumSierra.service.AlarmService"
					.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
}