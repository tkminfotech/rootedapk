package com.tkm.optumSierra.util;

import android.annotation.SuppressLint;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class StringUtils {
	public static boolean isNumber(String number) {
		if(null==number || "".equals(number)){
			return false;
		}
		int index = number.indexOf(".");
		if (index < 0) {
			return StringUtils.isNumeric(number);
		} else {
			String num1 = number.substring(0, index);
			String num2 = number.substring(index + 1);

			return StringUtils.isNumeric(num1) && StringUtils.isNumeric(num2);
		}
	}

	@SuppressLint("SimpleDateFormat")
	@SuppressWarnings("deprecation")
	public static int getMonth(String time) {
		SimpleDateFormat df2=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int month = 1;
		try {
			Date date2 = df2.parse(time);
			month = date2.getMonth() + 1;
		} catch (ParseException e) {
			month = 1;
		}
		return month;
	}

	/*public static boolean isNumeric(String str) {
		if(null==str || "".equals(str)){
			return false;
		}
		for (int i = str.length(); --i >= 0;) {
			if (!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}*/
	
	public static boolean isNumeric(String str){ 
		if(null==str || "".equals(str)){
			return false;
		}
	    Pattern pattern = Pattern.compile("[0-9]*"); 
	    return pattern.matcher(str).matches();    
	 } 

	public static String bytes2HexString(byte[] b) {
		String ret = "";
		for (int i = 0; i < b.length; i++) {

			String hex = Integer.toHexString(b[i] & 0xFF);

			if (hex.length() == 1) {

				hex = '0' + hex;

			}

			ret += hex;

		}

		return ret;

	}

	
	public static int hexToTen(String hex) {
		if (null == hex || (null != hex && "".equals(hex))) {
			return 0;
		}
		return Integer.valueOf(hex, 16);
	}

	/**
	 * 
	 * 
	 * @param hex
	 */
	public static String hexToBirary(String hex) {
		if (null == hex || (null != hex && "".equals(hex))) {
			return null;
		}
		return Integer.toBinaryString(Integer.valueOf(hex, 16));
	}

	/**
	 * 
	 * 
	 * @param i
	 * @return
	 */
	public static String tenToBinary(int i) {
		return Integer.toBinaryString(i);
	}

	/**
	 * 
	 * 
	 * @param bianry
	 * @return
	 */
	public static int binaryToTen(String bianry) {
		if (null == bianry || (null != bianry && "".equals(bianry))) {
			return 0;
		}
		return Integer.valueOf(bianry, 2);
	}

//	public static void main(String[] args) {
////		byte[] s = new byte[10];
////		for (int i = 0; i < 10; i++) {
////			s[i] = (byte) (i);
////		}
////		System.out.println(s.toString());
//	}
}
