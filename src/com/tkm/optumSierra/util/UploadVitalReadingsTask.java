package com.tkm.optumSierra.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.GlobalClass;
import com.tkm.optumSierra.dal.Glucose_db;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.dal.Pulse_db;
import com.tkm.optumSierra.dal.Question_skip_db;
import com.tkm.optumSierra.dal.Questions_db;
import com.tkm.optumSierra.dal.Temperature_db;
import com.tkm.optumSierra.dal.Weight_db;

public class UploadVitalReadingsTask extends AsyncTask<String, Void, String> {

	private String TAG = "UploadVitalReadingsTask";
	private String serverpath;

	int[] blood_oxygen, pulse, Measurement_Id;

	private Weight_db dbweight;
	private Pulse_db dbpulse_ox;
	private Pressure_db dbpressure;
	private Glucose_db dbglucose;
	private Temperature_db dbtemparature;
	private Question_skip_db dbquestion;
	private Questions_db dbcreate1;

	String[] measure_date = new String[] {};
	String patient_id, item;
	int type = 0;
	public Context context;
private GlobalClass appClass;
	/*
	 * 1.glucose 2.Oxygen 3.pressure 4.pt/Inr 5.-- 6.Temperature 7.Weight
	 * 8.fluid level 11.Pain Level 12. Peak flow
	 */

	public UploadVitalReadingsTask(Context ctx, String serverpath) {
		
		context = ctx;
		appClass = (GlobalClass) context.getApplicationContext();
		this.serverpath = serverpath;
		this.dbweight = new Weight_db(ctx);
		this.dbpulse_ox = new Pulse_db(ctx);
		this.dbpressure = new Pressure_db(ctx);
		this.dbglucose = new Glucose_db(ctx);
		this.dbtemparature = new Temperature_db(ctx);
		this.dbquestion = new Question_skip_db(ctx);
		this.dbcreate1 = new Questions_db(ctx);
		
	}

	@Override
	protected void onPostExecute(String result) {

		// send broad cast to all listening activities
		// Intent i = new Intent();
		// i.setAction(Util.UPLOAD_VITALS);
		Log.i(TAG, "onPostExecute vitals has been updated..");
		// context.sendBroadcast(i);

		/*
		 * if(result.length()>0) // If vitals uploaded succesfully {
		 * 
		 * }
		 */

	}

	@Override
	protected void onPreExecute() {

		super.onPreExecute();
	}

	protected String doInBackground(String... urls) {
		return upload_vitals();
	}

	private String upload_vitals() {
		// DownloadAdviceMessage();
		Log.i(TAG, "Uploading vitals server started ...");
		upload_weight();
		upload_glucose();
		upload_pulse_ox();
		upload_pressure();
		upload_temperature();
		upload_question();
		upload_previous_question();

		return "";
	}

	/********************************* weight ****************************/
	private void upload_weight() {
		String value = "-101";
		try {
			
			dbweight.delete_weight_101();
			Cursor cweight = dbweight.SelectMeasuredweight();
			cweight.moveToFirst();
			if (cweight.getCount() > 0) {

				while (cweight.isAfterLast() == false) {
					Log.i(TAG, "Uploading weight data to server started ...");

					HttpClient client = new DefaultHttpClient();
					HttpPost post = new HttpPost(this.serverpath
							+ "/droid_website/add_tpm_body_weight.ashx");
					List<NameValuePair> pairs = new ArrayList<NameValuePair>();

					post.setEntity(new UrlEncodedFormEntity(pairs));

					post.setHeader("patient_id", cweight.getString(2));
					post.setHeader("measure_date", cweight.getString(1)
							.replace("\n", ""));
					post.setHeader("kilogram",
							cweight.getString(3).replace("\r", ""));
					value = cweight.getString(3).replace("\r", "");
					post.setHeader("kind", "1");
					post.setHeader("fat_percent", "0.0");
					post.setHeader("input_mode", cweight.getString(5));
					post.setHeader("section_date", cweight.getString(7));
					post.setHeader("is_reminder", cweight.getString(8));

					HttpResponse response1 = client.execute(post);
					InputStream in = response1.getEntity().getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(in));
					StringBuilder str = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						str.append(line + "\n");
					}
					in.close();
					str.toString();
					if (str.length() > 0) {
						DocumentBuilder db = DocumentBuilderFactory
								.newInstance().newDocumentBuilder();
						InputSource is1 = new InputSource();
						is1.setCharacterStream(new StringReader(str.toString()));
						Document doc = db.parse(is1);
						NodeList nodes = doc
								.getElementsByTagName("success_response");
						if (nodes.getLength() == 1) {
							// /// if success delete the value from the
							// tablet db
							//Log.i(TAG, "Update weight data with id ..."
							//		+ cweight.getString(0));
							if(!value.equals("-101"))
								appClass.uploadStatus("success");
							dbweight.UpdateMeasureData(Integer.parseInt(cweight
									.getString(0)), context);

							dbweight.delete_weight_101();
							/*
							 * dbweight.delete_weight_data(Integer.parseInt(cweight
							 * .getString(0)));
							 */

						}else{
							if(!value.equals("-101"))
								appClass.uploadStatus("failed");
						}

					}else{
						if(!value.equals("-101"))
							appClass.uploadStatus("failed");
					}
					cweight.moveToNext();
				}
			}
			cweight.close();
			dbweight.cursorWeight.close();
			dbweight.closeOpenWTdbConnection();

		} catch (Exception e) {
			if(!value.equals("-101"))
			 appClass.uploadStatus("failed");
			System.out.println("Weight Not Posted aswellas not Entered in DB");
			String Error = this.getClass().getName()+ " : "+ e.getMessage();
			OnInit_Data_Access.ErrorFileWriter(this.context, "Weight Data Upload Error", Error);
			OnInit_Data_Access.insert_incident_detail(Error, Constants.Weight_failure_upload);
		}
	}

	/********************************* Oxygen ****************************/
	private void upload_pulse_ox() {

		Log.i(TAG, "uploading started Oxgen from skip.");
		String value = "-101";
		try {
			/***************** Oxgen ********************/

			Cursor c = dbpulse_ox.SelectMeasuredData();
			/*
			 * measure_date = new String[c.getCount()]; blood_oxygen = new
			 * int[c.getCount()]; pulse = new int[c.getCount()]; Measurement_Id
			 * = new int[c.getCount()];
			 */

			c.moveToFirst();
			if (c.getCount() > 0) {
				while (c.isAfterLast() == false) {
					Log.i(TAG,
							"Uploading oxygen measure data to server started ...");
					HttpClient client = new DefaultHttpClient();
					HttpPost post = new HttpPost(this.serverpath
							+ "/droid_website/add_tpm_blood_oxygen.ashx");
					List<NameValuePair> pairs = new ArrayList<NameValuePair>();

					post.setEntity(new UrlEncodedFormEntity(pairs));

					post.setHeader("patient_id", c.getString(5));
					post.setHeader("measure_date", c.getString(3));
					post.setHeader("blood_oxygen", c.getString(1));
					value = c.getString(1);
					post.setHeader("pulse", c.getString(2));
					post.setHeader("input_mode", c.getString(6));
					post.setHeader("section_date", c.getString(7));
					post.setHeader("is_reminder", c.getString(9));

					HttpResponse response1 = client.execute(post);
					InputStream in = response1.getEntity().getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(in));
					StringBuilder str = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						str.append(line + "\n");
					}
					in.close();
					str.toString();
					if (str.length() > 0) {
						DocumentBuilder db = DocumentBuilderFactory
								.newInstance().newDocumentBuilder();
						InputSource is1 = new InputSource();
						is1.setCharacterStream(new StringReader(str.toString()));
						Document doc = db.parse(is1);
						NodeList nodes = doc
								.getElementsByTagName("success_response");
						if (nodes.getLength() == 1) {
							// /// if success delete the value from the
							// tablet db
							//Log.i(TAG,
							//		"Update Measure data with id ..."
							//				+ c.getString(0));
							if(!value.equals("-101"))
							 	appClass.uploadStatus("success");
							dbpulse_ox.UpdateMeasureData(Integer.parseInt(c
									.getString(0)), context);
						}else{
							if(!value.equals("-101"))
								appClass.uploadStatus("failed");
						}

					}else{
						if(!value.equals("-101"))
							appClass.uploadStatus("failed");
					}
					c.moveToNext();

				}
			}
			c.close();
			dbpulse_ox.cursorMeasurement.close();
			dbpulse_ox.close();

		} catch (Exception e) {
			if(!value.equals("-101"))
				appClass.uploadStatus("failed");
			// e.printStackTrace();
			String Error = this.getClass().getName()+ " : "+ e.getMessage();
			OnInit_Data_Access.ErrorFileWriter(this.context, "Blood Pulse Data Upload Error", Error);
			OnInit_Data_Access.insert_incident_detail(Error, Constants.SpO2_failure_upload);
		}

	}

	/********************************* Pressure **************************/
	private void upload_pressure() {

		Log.i(TAG, "uploading started.");
		String value = "-101";
		try {

			Cursor cpressure = dbpressure.SelectMeasuredpressure();
			cpressure.moveToFirst();
			if (cpressure.getCount() > 0) {

				while (cpressure.isAfterLast() == false) {
					Log.i(TAG, "Uploading pressure  data to server started ...");

					HttpClient client = new DefaultHttpClient();
					HttpPost post = new HttpPost(this.serverpath
							+ "/droid_website/add_tpm_blood_pressure.ashx");
					List<NameValuePair> pairs = new ArrayList<NameValuePair>();

					post.setEntity(new UrlEncodedFormEntity(pairs));

					post.setHeader("patient_id", cpressure.getString(1));
					post.setHeader("measure_date", cpressure.getString(5));
					post.setHeader("diastolic", cpressure.getString(3));
					value = cpressure.getString(3);
					post.setHeader("systolic", cpressure.getString(2));
					post.setHeader("pulse", cpressure.getString(4));
					post.setHeader("input_mode", cpressure.getString(7));
					post.setHeader("section_date", cpressure.getString(8));					
					post.setHeader("prompt_flag",cpressure.getString(10));
					post.setHeader("body_movement", cpressure.getString(11));
					post.setHeader("irregular_pulse", cpressure.getString(12));
					post.setHeader("battery_level", cpressure.getString(13));
					post.setHeader("measurement_position_flag", cpressure.getString(14));
					post.setHeader("flag_cuff_fit_detection_flag", cpressure.getString(15));
					post.setHeader("is_reminder", cpressure.getString(16));
				
					HttpResponse response1 = client.execute(post);
					InputStream in = response1.getEntity().getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(in));
					StringBuilder str = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						str.append(line + "\n");
					}
					in.close();
					str.toString();
					if (str.length() > 0) {
						DocumentBuilder db = DocumentBuilderFactory
								.newInstance().newDocumentBuilder();
						InputSource is1 = new InputSource();
						is1.setCharacterStream(new StringReader(str.toString()));
						Document doc = db.parse(is1);
						NodeList nodes = doc
								.getElementsByTagName("success_response");
						if (nodes.getLength() == 1) {
							// /// if success delete the value from the
							// tablet db
							//Log.i(TAG, "Update Pressure data with id ..."
							//		+ cpressure.getString(0));
							if(!value.equals("-101"))
								appClass.uploadStatus("success");
							dbpressure.UpdatepressureData(Integer
									.parseInt(cpressure.getString(0)), context);
						}else{
							if(!value.equals("-101"))
								appClass.uploadStatus("failed");
						}

					}else{
						if(!value.equals("-101"))
							appClass.uploadStatus("failed");
					}
					cpressure.moveToNext();
				}
			}
			cpressure.close();
			dbpressure.cursorPreesure.close();
			dbpressure.close();

		} catch (Exception e) {
			if(!value.equals("-101"))
				appClass.uploadStatus("failed");
			// e.printStackTrace();
			String Error = this.getClass().getName()+ " : "+ e.getMessage();
			OnInit_Data_Access.ErrorFileWriter(this.context, "Blood Pressure Data Upload Error", Error);
			OnInit_Data_Access.insert_incident_detail(Error, Constants.BP_failure_upload);
		}

	}

	/********************************* Glucose ***************************/
	private void upload_glucose() {
		String value = "-101";
		try {
			Cursor cglucose = dbglucose.SelectMeasuredglucose();
			cglucose.moveToFirst();
			if (cglucose.getCount() > 0) {

				while (cglucose.isAfterLast() == false) {
					Log.i(TAG, "Uploading Glucose data to server started ...");

					HttpClient client = new DefaultHttpClient();
					HttpPost post = new HttpPost(this.serverpath
							+ "/droid_website/add_tpm_blood_glucose.ashx");
					List<NameValuePair> pairs = new ArrayList<NameValuePair>();
					post.setEntity(new UrlEncodedFormEntity(pairs));
					post.setHeader("patient_id", cglucose.getString(4));
					post.setHeader("measure_date", cglucose.getString(2));
					post.setHeader("milligram", cglucose.getString(1));
					value = cglucose.getString(1);
					post.setHeader("input_mode", cglucose.getString(5));
					post.setHeader("section_date", cglucose.getString(6));					
					post.setHeader("prompt_flag", cglucose.getString(8));
					post.setHeader("is_reminder", cglucose.getString(9));
					
					HttpResponse response1 = client.execute(post);
					InputStream in = response1.getEntity().getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(in));
					StringBuilder str = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						str.append(line + "\n");
					}
					//Log.i(TAG,
					//		" glucose data with id ..." + cglucose.getString(0));
					in.close();
					str.toString();
					if (str.length() > 0) {
						DocumentBuilder db = DocumentBuilderFactory
								.newInstance().newDocumentBuilder();
						InputSource is1 = new InputSource();
						is1.setCharacterStream(new StringReader(str.toString()));
						Document doc = db.parse(is1);
						NodeList nodes = doc
								.getElementsByTagName("success_response");
						if (nodes.getLength() == 1) {
							// /// if success delete the value from the
							// tablet db
							//Log.i(TAG, "Update glucose data with id ..."
							//		+ cglucose.getString(0));
							if(!value.equals("-101"))
								appClass.uploadStatus("success");
							dbglucose.UpdateglucoseData(Integer
									.parseInt(cglucose.getString(0)), context);
						}else{
							if(!value.equals("-101"))
								appClass.uploadStatus("failed");
						}
					}else{
						if(!value.equals("-101"))
							appClass.uploadStatus("failed");
					}
					cglucose.moveToNext();
				}
			}
			dbglucose.cursorMeasurement.close();
			dbglucose.close();

		} catch (Exception e) {
			if(!value.equals("-101"))
				appClass.uploadStatus("failed");
			// e.printStackTrace();
			String Error = this.getClass().getName()+ " : "+ e.getMessage();
			OnInit_Data_Access.ErrorFileWriter(this.context, "Blood Glucose Data Upload Error", Error);
			OnInit_Data_Access.insert_incident_detail(Error, Constants.BG_failure_upload);
		}
	}

	/******************************** Temperature ************************/
	private void upload_temperature() {
		
		String value = "-101";
		try {

			Cursor ctemperature = dbtemparature.SelectMeasuredTemperature();
			ctemperature.moveToFirst();
			if (ctemperature.getCount() > 0) {

				while (ctemperature.isAfterLast() == false) {
					Log.i(TAG,
							"Uploading temparature data to server started ...");

					HttpClient client = new DefaultHttpClient();
					HttpPost post = new HttpPost(this.serverpath
							+ "/droid_website/add_tpm_body_temperature.ashx");
					List<NameValuePair> pairs = new ArrayList<NameValuePair>();
					post.setEntity(new UrlEncodedFormEntity(pairs));
					post.setHeader("patient_id", ctemperature.getString(4));
					post.setHeader("measure_date", ctemperature.getString(2));
					post.setHeader("fahrenheit", ctemperature.getString(1));
					value = ctemperature.getString(1);
					post.setHeader("input_mode", ctemperature.getString(5));
					post.setHeader("section_date", ctemperature.getString(6));
					post.setHeader("is_reminder", ctemperature.getString(8));	
					
					
					HttpResponse response1 = client.execute(post);
					InputStream in = response1.getEntity().getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(in));
					StringBuilder str = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						str.append(line + "\n");
					}

					in.close();
					str.toString();
					if (str.length() > 0) {
						DocumentBuilder db = DocumentBuilderFactory
								.newInstance().newDocumentBuilder();
						InputSource is1 = new InputSource();
						is1.setCharacterStream(new StringReader(str.toString()));
						Document doc = db.parse(is1);
						NodeList nodes = doc
								.getElementsByTagName("success_response");
						if (nodes.getLength() == 1) {
							// /// if success delete the value from the
							// tablet db
							//Log.i(TAG, "Update temperature data with id ..."
							//		+ ctemperature.getString(0));
							if(!value.equals("-101"))
								appClass.uploadStatus("success");
							dbtemparature.UpdatetemparatureData(Integer
									.parseInt(ctemperature.getString(0)), context);
						}else{
							if(!value.equals("-101"))
								appClass.uploadStatus("failed");
						}
					}else{
						if(!value.equals("-101"))
							appClass.uploadStatus("failed");
					}
					ctemperature.moveToNext();
				}
			}
			dbtemparature.cursorTemperature.close();
			dbtemparature.close();

		} catch (Exception e) {
			if(!value.equals("-101"))
				appClass.uploadStatus("failed");
			String Error = this.getClass().getName()+ " : "+ e.getMessage();
			OnInit_Data_Access.ErrorFileWriter(this.context, "Temperature Data Upload Error", Error);
			OnInit_Data_Access.insert_incident_detail(Error, Constants.Temp_failure_upload);
		}
	}

	/********************************************* question ********************************************/
	private void upload_question() {
		try {
			Log.i(TAG, "Upload question to server ");
			Cursor response = dbquestion.Select_question();
			if (response.getCount() <= 0) {

			}
			response.moveToFirst();

			String finalxml = "";
			int skip_id = 0;
			String PatientId = "";
			String MeasureDate = "";
			String sectionDate = "";
			String ir_is_reminder ="";
			while (response.isAfterLast() == false) {
				PatientId = response.getString(1);
				MeasureDate = response.getString(3);
				sectionDate = response.getString(2);
				skip_id = Integer.parseInt(response.getString(0));
				ir_is_reminder=response.getString(4);
				HttpClient client = new DefaultHttpClient();
				Log.i(TAG, "Upload question to server start------------------------ ");
				HttpPost post = new HttpPost(
						this.serverpath
								+ "/droid_website/add_tpm_interview_result_branchable.ashx");
				StringEntity se = new StringEntity(finalxml, HTTP.UTF_8);
				post.setHeader("patient_id", PatientId);
				post.setHeader("measure_date", MeasureDate);
				post.setHeader("tree_number", "-101");
				post.setHeader("section_date", sectionDate);
				post.setHeader("ir_is_reminder", ir_is_reminder);
				 
				post.setEntity(se);
				if (sectionDate.length() > 5) {
					HttpResponse response1 = client.execute(post);
					InputStream in = response1.getEntity().getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(in));
					StringBuilder str = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						str.append(line + "\n");
					}
					in.close();
					str.toString();
					if (str.length() > 0) {
						DocumentBuilder db = DocumentBuilderFactory
								.newInstance().newDocumentBuilder();
						InputSource is1 = new InputSource();
						is1.setCharacterStream(new StringReader(str.toString()));
						Document doc = db.parse(is1);
						NodeList nodes = doc
								.getElementsByTagName("success_response");
						if (nodes.getLength() == 1) {
							appClass.uploadStatus("success");
							dbquestion.delete_skip_question_data(skip_id);
							Log.i(TAG, "success_response.... .......");
						}else{
							
							appClass.uploadStatus("failed");
						}
					}else{
						appClass.uploadStatus("failed");
					}
				}
				response.moveToNext();

			}

			response.close();
			dbquestion.curso_qusetion.close();
			dbquestion.close();

		} catch (Exception e) {
			appClass.uploadStatus("failed");
			//Log.e(TAG, e.toString());
			String Error = this.getClass().getName()+ " : "+ e.getMessage();
			OnInit_Data_Access.ErrorFileWriter(this.context, "New Question Upload Error", Error);
			OnInit_Data_Access.insert_incident_detail(Error, Constants.Questions_failure_upload);
		}

	}

	private String upload_previous_question() {
		Log.i(TAG, "Upload upload_previous_question to server start ");
		ActivityManager am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

		String mClassName = cn.getClassName();
		if (mClassName.equals("com.tkm.optumSierra.QuestionView")){
			Log.i(TAG, "Upload question to server not work.. current page&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& ");
			return null;
		}
		// String query =
		// "Select a.Item_Number,a.Sequence_Number,a.Measure_Date,a.Patient_Id,a.treeNumber from Droid_DMP_User_Response a where a.Status=0";
		String query = "Select a.Item_Number,a.Sequence_Number,a.Measure_Date,a.Patient_Id,a.treeNumber,a.Item_Content,a.sectionDate,a.ir_is_reminder, a.Node_Type  from Droid_DMP_User_Response a where a.Status=0";

		Cursor response = dbcreate1.SelectDBValues(query);
		if (response.getCount() <= 0) {
			Log.i(TAG, "Upload question to server Cursor is null ");
			return null;
		}
		int j = 0;
		String[] childxml = new String[response.getCount()];
		String[] treeNumber = new String[response.getCount()];
		String[] finalxml = new String[response.getCount()];
		String[] PatientId = new String[response.getCount()];
		String[] MeasureDate = new String[response.getCount()];
		String[] ItemContent = new String[response.getCount()];
		String[] sectionDate = new String[response.getCount()];
		String[] ir_is_reminder = new String[response.getCount()];
		while (response.moveToNext()) {
			String xml1, xml2, xml3, xml4, xml5 = "";
			xml1 = "<response>";
			xml2 = "<sequencenumber>" + response.getString(1)
					+ "</sequencenumber>";
			// xml3 = "<itemnumber>" + response.getString(0) +
			// "</itemnumber>";
		
			if (response.getString(8).equals("5")) {
				if (response.getInt(0) == 0) {
					xml3 = "<itemnumber>-1</itemnumber>";
				} else {
					xml3 = "<itemnumber>-2</itemnumber>";

				}

			} else {
				ItemContent[j] = String
						.valueOf(dbcreate1.SelectItemNumber(response.getString(5), response.getString(1)));
				xml3 = "<itemnumber>" + ItemContent[j] + "</itemnumber>";
			}
			
			

			xml4 = "<measure_date>" + response.getString(2) + "</measure_date>";
			xml5 = "</response>";
			childxml[j] = childxml[j] + xml1 + xml2 + xml3 + xml4 + xml5;
			if (childxml[j] != null) {
				finalxml[j] = "<responsetree>" + childxml[j]
						+ "</responsetree>";
			}
			PatientId[j] = response.getString(3);
			MeasureDate[j] = response.getString(2);
			treeNumber[j] = response.getString(4);
			sectionDate[j] = response.getString(6);
			ir_is_reminder[j] = response.getString(7);

			try {
				InputStream content = null;
				HttpClient client = new DefaultHttpClient();

				HttpPost post = new HttpPost(
						this.serverpath
								+ "/droid_website/add_tpm_interview_result_branchable.ashx");				
				finalxml[j]=finalxml[j].replace("<responsetree>null<response>", "<responsetree><response>");
				Log.i("Upload vital service", "finalxml ..." + finalxml[j].toString());
				
				StringEntity se = new StringEntity(finalxml[j], HTTP.UTF_8);
				List<NameValuePair> pairs = new ArrayList<NameValuePair>();
				post.setHeader("patient_id", PatientId[j]);
				post.setHeader("measure_date", MeasureDate[j]);
				Log.e(TAG, "tree number==========================="+treeNumber[j]);
				post.setHeader("tree_number", treeNumber[j]);
				post.setHeader("section_date", sectionDate[j]);
				post.setHeader("ir_is_reminder", ir_is_reminder[j]);
				post.setEntity(se);
				HttpResponse response1 = client.execute(post);
				int a = response1.getStatusLine().getStatusCode();
				InputStream in = response1.getEntity().getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(in));
				StringBuilder str = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					str.append(line + "\n");
				}
				in.close();
				str.toString();
				//Log.i("upload answer responce from server....", "Response ..." + str);
				if (str.length() > 0) {
					DocumentBuilder db = DocumentBuilderFactory.newInstance()
							.newDocumentBuilder();
					InputSource is1 = new InputSource();
					is1.setCharacterStream(new StringReader(str.toString()));
					Document doc = db.parse(is1);
					NodeList nodes = doc
							.getElementsByTagName("success_response");
					appClass.uploadStatus("success");
					dbcreate1.updateToQuestionToServer(context);
					Log.i(TAG, "previous.......success_response.... .......");
				}else{
					
					appClass.uploadStatus("failed");
				}

			} catch (Exception e) {
				appClass.uploadStatus("failed");
				Log.e(TAG, e.toString());
				String Error = this.getClass().getName()+ " : "+ e.getMessage();
				OnInit_Data_Access.ErrorFileWriter(this.context, "Question Upload Error", Error);
				OnInit_Data_Access.insert_incident_detail(Error, Constants.Questions_failure_upload);
			}

			j += 1;
		}
		response.close();
		dbcreate1.cursorQuestions.close();
		dbcreate1.closeopendbConnection();
		return null;
	}

}