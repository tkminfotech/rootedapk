package com.tkm.optumSierra.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlSerializer;

import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import com.tkm.optumSierra.util.Log;
import android.util.Xml;

import com.tkm.optumSierra.RebootDataManager;
import com.tkm.optumSierra.bean.Class_Incident;
import com.tkm.optumSierra.dal.Incident_Log_db;

/***** Download Patient Login Details ********/

public class Upload_Incident_Detail_Task extends AsyncTask<String, Void, String> {

	private static final String URL = "/droid_website/Upload_Incident_Detail.ashx";
	//https://othwebtest.portal724.us:8725/droid_website/Upload_Incident_Detail.ashx
	private String IMEI_LABLE		= "imei";
	private String SUCCESS_RESPONCE	= "Success_response";
	private String TAG				= "Upload_Incident_Detail_Task";
	private String server_path 		= "";	

	Incident_Log_db db_incident;
	static TelephonyManager tManager;
	private Context context;
	

	public Upload_Incident_Detail_Task(Context ctx, String server_path) {
		
		
		this.server_path = server_path;
		this.context = ctx;
		db_incident = new Incident_Log_db(ctx);
		tManager = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
	}

	@Override
	protected void onPreExecute() {		

		Log.e(TAG, "onPreExecute Upload_Incident_Detail_Task");	
	}

	@Override
	protected void onPostExecute(String result) {	

		Log.e(TAG, "onPostExecute Upload_Incident_Detail_Task");	

	}


	protected String doInBackground(String... urls) {

		Log.e(TAG, "doInBackground Upload_Incident_Detail_Task");	
		upload_incident_detail();
		return "";
	}	
	
	private static String get_imei()
	{
		String imei = "";
		try
		{			
			imei = tManager.getDeviceId();
			if (imei.trim().length() > 14) imei = imei.substring(0, 14);

		} catch (Exception e)
		{

		}
		return imei;		
	}

	private String  upload_incident_detail()
	{
		try {

			HttpClient client = new DefaultHttpClient();
			Log.i(TAG,"Updating serverurl "+ server_path+URL);
			HttpPost post = new HttpPost(server_path+URL);

			String incident_detail = create_incident_xml();
			StringEntity se = new StringEntity(incident_detail, HTTP.UTF_8);
			post.setHeader(IMEI_LABLE, 	get_imei());
			post.setEntity(se);		

			HttpResponse response = client.execute(post);  // post

			if (response == null)
			{
				return "";
			}

			InputStream in = response.getEntity().getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			StringBuilder str = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				str.append(line + "\n");
			}

			in.close();

			if (str.length() > 0) {
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				InputSource is1 = new InputSource();
				is1.setCharacterStream(new StringReader(str.toString()));
				Document doc = db.parse(is1);
				NodeList nodes = doc.getElementsByTagName(SUCCESS_RESPONCE);

				if (nodes.getLength() == 1) {

					db_incident.Cleartable();
					return "success";
					
				}

			}

			return str.toString();

		} catch (Exception e) {

			Log.i(TAG, "Uploading Incident Error ...");
			Log.e(TAG, "Connection Failed!");	
			String Error = this.getClass().getName()+ " : "+ e.getMessage();
			OnInit_Data_Access.ErrorFileWriter(this.context, "Incident Data Upload Error", Error);
			OnInit_Data_Access.insert_incident_detail(Error, Constants.Other_Upload_Download_Error);
			return "";
		}

	}

	public String create_incident_xml() throws Exception {
		
		List<Class_Incident> class_incident_list = db_incident.get_incident_details();
		if(class_incident_list.size() == 0)
			return "";

		XmlSerializer xmlSerializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		xmlSerializer.setOutput(writer);
		xmlSerializer.startDocument("UTF-8", true);	 
		xmlSerializer.startTag("", Incident_Log_db.incident_table);	 

		for (Class_Incident class_incident : class_incident_list) {			   

			xmlSerializer.startTag("", Incident_Log_db.incident_detail);	 

			xmlSerializer.startTag("", Incident_Log_db.LABEL_INCIDENT_TYPE_ID);
			xmlSerializer.text(class_incident.get_incident_type_id()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_INCIDENT_TYPE_ID);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_INCIDENT_MSG);
			xmlSerializer.text(class_incident.get_incident_msg());
			xmlSerializer.endTag("", Incident_Log_db.LABEL_INCIDENT_MSG);		  

			xmlSerializer.startTag("", Incident_Log_db.LABEL_INCIDENT_DATE);
			xmlSerializer.text(class_incident.get_incident_date());
			xmlSerializer.endTag("", Incident_Log_db.LABEL_INCIDENT_DATE);		  

			xmlSerializer.startTag("", Incident_Log_db.LABEL_PATIENT_ID);
			xmlSerializer.text(class_incident.get_patient_id()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_PATIENT_ID);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_IMEI);
			xmlSerializer.text(class_incident.get_tablet_imei());
			xmlSerializer.endTag("", Incident_Log_db.LABEL_IMEI);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_APP_VERSION);
			xmlSerializer.text(class_incident.get_app_version_number());
			xmlSerializer.endTag("", Incident_Log_db.LABEL_APP_VERSION);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_USED_BANDWIDTH);
			xmlSerializer.text(class_incident.get_app_used_bandwidth()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_USED_BANDWIDTH);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_SIMCARD_IN);
			xmlSerializer.text(class_incident.get_sim_card_in()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_SIMCARD_IN);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_SIMCARD_STATUS);
			xmlSerializer.text(class_incident.get_sim_card_status()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_SIMCARD_STATUS);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_NETWORK_TYPE);
			xmlSerializer.text(class_incident.get_cell_network_type());
			xmlSerializer.endTag("", Incident_Log_db.LABEL_NETWORK_TYPE);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_NETWORK_PROVIDER);
			xmlSerializer.text(class_incident.get_cell_network_provider());
			xmlSerializer.endTag("", Incident_Log_db.LABEL_NETWORK_PROVIDER);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_SIGNAL_STRENGTH);
			xmlSerializer.text(class_incident.get_signal_strength()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_SIGNAL_STRENGTH);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_SIGNAL_STRENGTH_DBM);
			xmlSerializer.text(class_incident.get_signal_strength_dbm()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_SIGNAL_STRENGTH_DBM);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_SIGNAL_STRENGTH_ASU);
			xmlSerializer.text(class_incident.get_signal_strength_asu()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_SIGNAL_STRENGTH_ASU);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_APN_NAME);
			xmlSerializer.text(class_incident.get_apn_name());
			xmlSerializer.endTag("", Incident_Log_db.LABEL_APN_NAME);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_APN_VALUE);
			xmlSerializer.text(class_incident.get_apn_value());
			xmlSerializer.endTag("", Incident_Log_db.LABEL_APN_VALUE);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_OS_VERSION);
			xmlSerializer.text(class_incident.get_tablet_os_version());
			xmlSerializer.endTag("", Incident_Log_db.LABEL_OS_VERSION);		  

			xmlSerializer.startTag("", Incident_Log_db.LABEL_BUILD_NUMBER);
			xmlSerializer.text(class_incident.get_tablet_os_build_number());
			xmlSerializer.endTag("", Incident_Log_db.LABEL_BUILD_NUMBER);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_SYSTEM_UP_TIME);
			xmlSerializer.text(class_incident.get_system_up_time()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_SYSTEM_UP_TIME);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_POWER_ADAPTOR);
			xmlSerializer.text(class_incident.get_power_adapter_on()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_POWER_ADAPTOR);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_BATTERY_LEVEL);
			xmlSerializer.text(class_incident.get_battery_level()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_BATTERY_LEVEL);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_AIRPLANE_MODE);
			xmlSerializer.text(class_incident.get_airplane_mode_on()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_AIRPLANE_MODE);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_WIFI);
			xmlSerializer.text(class_incident.get_wifi_on()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_WIFI);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_BLUTOOTH);
			xmlSerializer.text(class_incident.get_bluetooth_on()+"");
			xmlSerializer.endTag("", Incident_Log_db.LABEL_BLUTOOTH);

			xmlSerializer.startTag("", Incident_Log_db.LABEL_CREATE_DATE);
			xmlSerializer.text(class_incident.get_created_date());
			xmlSerializer.endTag("", Incident_Log_db.LABEL_CREATE_DATE);


			xmlSerializer.endTag("", Incident_Log_db.incident_detail);

		}

		xmlSerializer.endTag("", Incident_Log_db.incident_table);
		xmlSerializer.endDocument();

		String final_xml = writer.toString().replace("standalone='yes'", "");

		Log.e(TAG, "final_xml : " + final_xml);

		return final_xml;
	}

}
