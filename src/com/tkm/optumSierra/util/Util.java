package com.tkm.optumSierra.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;

import com.tkm.optumSierra.Titlewindow;
import com.tkm.optumSierra.util.Log;

import com.tkm.optumSierra.dal.Glucose_db;
import com.tkm.optumSierra.dal.Pressure_db;
import com.tkm.optumSierra.dal.Pulse_db;
import com.tkm.optumSierra.dal.Question_Check_db;
import com.tkm.optumSierra.dal.Sensor_db;
import com.tkm.optumSierra.dal.Temperature_db;
import com.tkm.optumSierra.dal.Weight_db;

public class Util {
	private static MediaPlayer mediaPlayer;
	private static int stateMediaPlayer;
	private static int stateMP_NotStarter = 1;
	private static String TAG = "Utill Sierra";

	public static String getCharacterDataFromElement(org.w3c.dom.Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

	public static String getTagValue(NodeList nodes, int i, String tag) {
		return getCharacterDataFromElement((org.w3c.dom.Element) (((org.w3c.dom.Element) nodes
				.item(i)).getElementsByTagName(tag)).item(0));
	}

	public static HttpResponse connect(String url, String[] header,
			String[] value) {
		try {
			HttpClient client = new DefaultHttpClient();
			Log.i(TAG, url);
			HttpPost post = new HttpPost(url);
			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			post.setEntity(new UrlEncodedFormEntity(pairs));
			for (int i = 0; i < header.length; i++) {
				post.setHeader(header[i], value[i]);
			}
			return client.execute(post);
		} catch (Exception e) {
			Log.e("HttpResponse error", e.toString());
		}
		return null;
	}

	public static String readStream(InputStream in) {
		try {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuilder str = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				str.append(line + "\n");
			}
			in.close();
			return str.toString();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		return "";
	}

	public static void stopPlaySingle() {
		if (stateMediaPlayer == stateMP_NotStarter) {
			try {
				mediaPlayer.stop();
				mediaPlayer.release();
			} catch (Exception e) {
				//Log.e(TAG, "Error! Util error in  mediaPlayer stop!!!");
				// e.printStackTrace();
			}

		}
	}

	public static void soundPlay(String fileName) {
		if (stateMediaPlayer == stateMP_NotStarter) {
			try {
				mediaPlayer.stop();
				mediaPlayer.release();
			} catch (Exception e) {
				//Log.e(TAG, "Error! Util error in  mediaPlayer stop!!!");
				// e.printStackTrace();
			}

		}
		int language = Constants.getLanguage();
		String PATH_TO_FILE = "";
		if (language == 11) {
			PATH_TO_FILE = Environment.getExternalStorageDirectory().getPath()
					+ "/versions/Optum/spanish/";
		} else {
			PATH_TO_FILE = Environment.getExternalStorageDirectory().getPath()
					+ "/versions/Optum/";
		}

		PATH_TO_FILE = PATH_TO_FILE + fileName;

		Log.i(TAG, "path on play: " + PATH_TO_FILE);

		mediaPlayer = new MediaPlayer();

		try {
			mediaPlayer.setDataSource(PATH_TO_FILE);
			mediaPlayer.prepare();

			// Toast.makeText(QuestionView.this, PATH_TO_FILE,
			// Toast.LENGTH_LONG).show();
			mediaPlayer.start();
			stateMediaPlayer = stateMP_NotStarter;

		} catch (IllegalArgumentException e) {
			// e.printStackTrace();

		} catch (SecurityException e) {
			// e.printStackTrace();
		} catch (IllegalStateException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		}
	}

	public static double round(double d) {

		DecimalFormat twoDForm = new DecimalFormat("#.#");
		String x = twoDForm.format(d);
		x = x.replace(",", ".");
		return Double.valueOf(x);
	}

	protected static Boolean ToFile(String sFileName, String sBody) {

		Boolean on_create_flag = false;

		try {

			File root = new File(Environment.getExternalStorageDirectory(),
					"versions");
			if (!root.exists()) {
				root.mkdirs();
			}

			File gpxfile = new File(root, sFileName + ".txt");

			if (gpxfile.length() == 0) // file created date
			{
				on_create_flag = true;
			}

			FileWriter writer = new FileWriter(gpxfile, true);
			writer.append(sBody);
			writer.flush();
			writer.close();
			Runtime.getRuntime().exec("logcat -c");
		} catch (IOException e) {
			Log.i(TAG, "error " + e.getMessage());
			e.printStackTrace();
			on_create_flag = false;

		}

		return on_create_flag;
	}

	public static void ToFileClear(String sFileName) {
		try {
			File root = new File(Environment.getExternalStorageDirectory(),
					"versions");
			if (!root.exists()) {
				root.mkdirs();
			}
			File gpxfile = new File(root, sFileName + ".txt");
			FileWriter writer = new FileWriter(gpxfile);
			writer.append("");
			writer.flush();
			writer.close();
			@SuppressWarnings("unused")
			Process process = Runtime.getRuntime().exec("logcat -c");
		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	@SuppressLint("SimpleDateFormat")
	public static Boolean WriteLog(String Tag, String FileName) {
		try {
			String line;
			Process process = Runtime.getRuntime().exec("logcat -d");
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));
			Pattern pattern = Pattern.compile(Tag, 0);
			StringBuilder log = new StringBuilder();
			String patternu = "";
			while ((line = bufferedReader.readLine()) != null) {
				if (patternu != null && !pattern.matcher(line).find()) {
					continue;
				}
				if (!line.contains("<")) {
					if (!line.contains(">")) {
						SimpleDateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						Date date = new Date();
						log.append(dateFormat.format(date) + "~" + Tag + "~");
						log.append(line);
					}
					log.append('\n');
				}
			}

			return ToFile(FileName, log.toString()); // return true if log file
			// is newly created

		} catch (IOException e) {

			return false;
		}

	}

	public static String grtLogData(String FileName) {
		String aBuffer = "";

		try {
			File root = new File(Environment.getExternalStorageDirectory(),
					"versions/" + FileName + ".txt");
			FileInputStream fIn = new FileInputStream(root);
			@SuppressWarnings("resource")
			BufferedReader myReader = new BufferedReader(new InputStreamReader(
					fIn));
			String aDataRow = "";

			while ((aDataRow = myReader.readLine()) != null) {
				// <activity>" + Log_response.getString(4)
				aBuffer += "<activity>" + aDataRow + "</activity>";
			}

			ToFileClear(FileName);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return aBuffer;
	}

	public static String getMeasurement(int measure_type_id) {
		String a = "";

		if (measure_type_id == 7) {
			a = "weight";
		} else if (measure_type_id == 101) {
			a = "Question";
		} else if (measure_type_id == 2) {
			a = "Pulse";
		} else if (measure_type_id == 3) {
			a = "Pressure";
		} else if (measure_type_id == 6) {
			a = "Temperature";
		} else if (measure_type_id == 1) {
			a = "Glucose";

		} else if (measure_type_id == 4) {
			a = "Question";

		}

		return a;
	}

	public static void Stopsoundplay() {

		if (stateMediaPlayer == stateMP_NotStarter) {
			for (int i = 0; i < mPlayerList.size(); i++) // Do not include last
				// element
			{
				try {
					mPlayerList.get(i).stop();
					mPlayerList.get(i).release();

				} catch (Exception e) {
					//Log.e("DROID", "Error! Util error in  mediaPlayer stop!!!");
					e.printStackTrace();
				}

			}

		}

		mPlayerList.clear();

	}

	private static ArrayList<MediaPlayer> mPlayerList = new ArrayList<MediaPlayer>();

	@SuppressLint("NewApi")
	public static void playSound4FileList(ArrayList<String> fileList,
			Context context) {

		Stopsoundplay(); // clear all existing files..

		for (String fileName : fileList) {
			try {
				MediaPlayer mPlayerT = new MediaPlayer();

				int language = Constants.getLanguage();
				if (language == 11) {
					fileName = Environment.getExternalStorageDirectory()
							.getPath() + "/versions/Optum/spanish/" + fileName;
				} else {
					fileName = Environment.getExternalStorageDirectory()
							.getPath() + "/versions/Optum/" + fileName;
				}

				/*
				 * AssetFileDescriptor descriptor =
				 * context.getAssets().openFd(fileName);
				 * mPlayerT.setDataSource(descriptor.getFileDescriptor(),
				 * descriptor.getStartOffset(), descriptor.getLength());
				 * descriptor.close();
				 */

				mPlayerT.setDataSource(fileName);

				mPlayerT.prepare();
				mPlayerT.setVolume(100, 100);
				mPlayerT.setLooping(false);

				mPlayerList.add(mPlayerT);

			} catch (Exception e) {
			}

		}
		for (int i = 0; i < mPlayerList.size() - 1; i++) // Do not include last
			// element
		{
			mPlayerList.get(i).setNextMediaPlayer(mPlayerList.get(i + 1));

		}
		try {
			mPlayerList.get(0).start();
			stateMediaPlayer = stateMP_NotStarter;
		} catch (RuntimeException e) {
		}

	}

	@SuppressLint("SimpleDateFormat")
	public static String get_tablet_time1() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"MM/dd/yyyy hh:mm:ss aa");
		String currentDateandTime = dateFormat.format(new Date());
		return (currentDateandTime.replace("p.m.", "PM")).replace("a.m.", "AM");
	}

	@SuppressLint("SimpleDateFormat")
	public static String get_server_time() {
		Date today = new Date();
		String IST = "";
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
		df.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
		IST = df.format(today);

		return (IST.replace("p.m.", "PM")).replace("a.m.", "AM");

	}

	@SuppressLint("SimpleDateFormat")
	public static String get_patient_time_zone_time(Context context) {
		Date today = new Date();

		SharedPreferences cluster_sp = context.getSharedPreferences(
				CommonUtilities.CLUSTER_SP, 0);

		int time_zone_id = cluster_sp.getInt("time_zone_id", 0);

		String IST = "";
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
		switch (time_zone_id) {

		case 0:

			df.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
			IST = df.format(today);

			break;
			// -------------
		case 1:

			df.setTimeZone(TimeZone.getTimeZone("US/Central"));
			IST = df.format(today);

			break;
			// -------------
		case 2:

			df.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
			IST = df.format(today);

			break;
			// -------------
		case 3:

			df.setTimeZone(TimeZone.getTimeZone("US/Mountain"));
			IST = df.format(today);

			break;
			// -------------
		case 4:

			df.setTimeZone(TimeZone.getTimeZone("US/Pacific"));
			IST = df.format(today);

			break;
			// -------------
		case 5:

			df.setTimeZone(TimeZone.getTimeZone("US/Arizona"));
			IST = df.format(today);

			break;
			// -------------
		case 6:
			df.setTimeZone(TimeZone.getTimeZone("US/East-Indiana"));
			IST = df.format(today);

			break;
			// -------------
		case 7:

			df.setTimeZone(TimeZone.getTimeZone("US/Alaska"));
			IST = df.format(today);

			break;
			// -------------
		case 8:
			df.setTimeZone(TimeZone.getTimeZone("US/Hawaii"));
			IST = df.format(today);

			break;
			// -------------
		case 9:
			df.setTimeZone(TimeZone.getTimeZone("America/Puerto_Rico"));
			IST = df.format(today);

			break;

		default:

		}

		return (IST.replace("p.m.", "PM")).replace("a.m.", "AM");
	}

	@SuppressLint("SimpleDateFormat")
	public static String get_patient_time_zone_time_24_format(Context context) {
		Date today = new Date();

		SharedPreferences cluster_sp = context.getSharedPreferences(
				CommonUtilities.CLUSTER_SP, 0);

		int time_zone_id = cluster_sp.getInt("time_zone_id", 0);

		String IST = "";
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		switch (time_zone_id) {

		case 0:

			df.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
			IST = df.format(today);

			break;
			// -------------
		case 1:

			df.setTimeZone(TimeZone.getTimeZone("US/Central"));
			IST = df.format(today);

			break;
			// -------------
		case 2:

			df.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
			IST = df.format(today);

			break;
			// -------------
		case 3:

			df.setTimeZone(TimeZone.getTimeZone("US/Mountain"));
			IST = df.format(today);

			break;
			// -------------
		case 4:

			df.setTimeZone(TimeZone.getTimeZone("US/Pacific"));
			IST = df.format(today);

			break;
			// -------------
		case 5:

			df.setTimeZone(TimeZone.getTimeZone("US/Arizona"));
			IST = df.format(today);

			break;
			// -------------
		case 6:
			df.setTimeZone(TimeZone.getTimeZone("US/East-Indiana"));
			IST = df.format(today);

			break;
			// -------------
		case 7:

			df.setTimeZone(TimeZone.getTimeZone("US/Alaska"));
			IST = df.format(today);

			break;
			// -------------
		case 8:
			df.setTimeZone(TimeZone.getTimeZone("US/Hawaii"));
			IST = df.format(today);

			break;
			// -------------
		case 9:
			df.setTimeZone(TimeZone.getTimeZone("America/Puerto_Rico"));
			IST = df.format(today);

			break;

		default:

		}

		return (IST.replace("p.m.", "PM")).replace("a.m.", "AM");
	}
	
	
	@SuppressLint("SimpleDateFormat")
	public static String get_patient_time_zone_time_LastActivity(Context context) {
		Date today = new Date();

		SharedPreferences cluster_sp = context.getSharedPreferences(
				CommonUtilities.CLUSTER_SP, 0);

		int time_zone_id = cluster_sp.getInt("time_zone_id", 0);

		String IST = "";
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy , hh:mm aa");
		switch (time_zone_id) {

		case 0:

			df.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
			IST = df.format(today);

			break;
			// -------------
		case 1:

			df.setTimeZone(TimeZone.getTimeZone("US/Central"));
			IST = df.format(today);

			break;
			// -------------
		case 2:

			df.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
			IST = df.format(today);

			break;
			// -------------
		case 3:

			df.setTimeZone(TimeZone.getTimeZone("US/Mountain"));
			IST = df.format(today);

			break;
			// -------------
		case 4:

			df.setTimeZone(TimeZone.getTimeZone("US/Pacific"));
			IST = df.format(today);

			break;
			// -------------
		case 5:

			df.setTimeZone(TimeZone.getTimeZone("US/Arizona"));
			IST = df.format(today);

			break;
			// -------------
		case 6:
			df.setTimeZone(TimeZone.getTimeZone("US/East-Indiana"));
			IST = df.format(today);

			break;
			// -------------
		case 7:

			df.setTimeZone(TimeZone.getTimeZone("US/Alaska"));
			IST = df.format(today);

			break;
			// -------------
		case 8:
			df.setTimeZone(TimeZone.getTimeZone("US/Hawaii"));
			IST = df.format(today);

			break;
			// -------------
		case 9:
			df.setTimeZone(TimeZone.getTimeZone("America/Puerto_Rico"));
			IST = df.format(today);

			break;

		default:

		}

		return (IST.replace("p.m.", "PM")).replace("a.m.", "AM");
	}

	public static int hfp_vitals_taken_status(Context context, int type) {

		Weight_db droid_Weight_db = new Weight_db(context);
		Temperature_db droid_Temperature_db = new Temperature_db(context);
		Pressure_db droid_Pressure_db = new Pressure_db(context);
		Pulse_db droid_Pulse_db = new Pulse_db(context);
		Glucose_db droid_Glucose_db = new Glucose_db(context);
		Question_Check_db droid_Question_db = new Question_Check_db(context);
		Sensor_db sensor_db = new Sensor_db(context);

		int status = 0;

		Cursor cSensor = sensor_db.Select_sensors_only();
		cSensor.moveToFirst();
		if (cSensor.getCount() > 0) {
			while (cSensor.isAfterLast() == false) {

				if (Integer.parseInt(cSensor.getString(3)) == 7) {
					if (droid_Weight_db.GetCurrentDateValues(type,
							timepicker(context), context) <= 0)

						status += 1;
				}

				if (Integer.parseInt(cSensor.getString(3)) == 3) {
					if (droid_Pressure_db.GetCurrentDateValues(type,
							timepicker(context), context) <= 0)

						status += 1; 
				}

				if (Integer.parseInt(cSensor.getString(3)) == 6) {
					if (droid_Temperature_db.GetCurrentDateValues(type,
							timepicker(context), context) <= 0)

						status += 1; 
				}

				if (Integer.parseInt(cSensor.getString(3)) == 2) {
					if (droid_Pulse_db.GetCurrentDateValues(type,
							timepicker(context), context) <= 0)

						status += 1;
				}

				if (Integer.parseInt(cSensor.getString(3)) == 1) {
					if (droid_Glucose_db.GetCurrentDateValues(type,
							timepicker(context), context) <= 0)

						status += 1;
				} 
				if (Integer.parseInt(cSensor.getString(3)) == 101) {
					if (droid_Question_db.GetCurrentDateValues(type,
							timepicker(context), context) <= 0)

						status += 1;
				}
				cSensor.moveToNext();
			}

		} else {
			status=1;
		}
		cSensor.close();
		sensor_db.cursorsensor.close();
		sensor_db.close();

		return status;
	}

	private static String timepicker(Context context) {

		String patient_time = Util
				.get_patient_time_zone_time_24_format(context);
		Integer AmorPm = Integer.parseInt(patient_time.substring(11, 13));

		String type = "";
		// SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

		// Date date = new Date();
		// Integer AmorPm =
		// Integer.parseInt(dateFormat.format(date).substring(0,2));
		if (AmorPm >= 15)
			type = "PM";
		else if (AmorPm <= 14)
			type = "AM";
		return type;
	}
	public boolean activityCheck(Context ctx){
		ActivityManager am = (ActivityManager) ctx
				.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

		String mClassName = cn.getClassName();

		Log.e("mClassName", mClassName);
		if ((mClassName.equals("com.tkm.optumSierra.FinalMainActivity")
				|| mClassName.equals("com.tkm.optumSierra.Home")
				|| mClassName.equals("com.tkm.optumSierra.Reminder_Landing_Activity") 
				|| mClassName.equals("com.tkm.optumSierra.ThanksActivity")
				|| !mClassName.contains("com.tkm."))) {
			return true;
		}
		return false;
	}

	public static boolean is_lg() {
		boolean is_lg = false;
		String manufacturer = Build.MANUFACTURER;
		//String model = Build.MODEL;
		if(manufacturer.contains("LGE"))
			is_lg = true;
		return is_lg;
	}


	public static boolean isActivityOnTop(Titlewindow activity) {
		// those are used to check against the class name (probably so as to not process the readings in another activity -- maybe the test device page)
		ActivityManager am = (ActivityManager) activity.getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
		String topActivityClassName = cn.getClassName();

		return topActivityClassName.equals(activity.getClass().getCanonicalName());
	}

	public static void setPairStatus(Context context, String sharedPrefName, String pairStatus) {
		SharedPreferences pairingSharedPreferences = context.getSharedPreferences(CommonUtilities.NION_SP, 0);
		SharedPreferences.Editor editor = pairingSharedPreferences.edit();
		editor.putString(sharedPrefName, pairStatus);
		editor.apply();
	}
}
