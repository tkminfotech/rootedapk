package com.tkm.optumSierra.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilTooth {
	
	public static float changeBMI(float bmi){
		if(bmi<15){
			bmi = (float) ((Math.round(40/15)/10.0)*bmi);
		}else if(bmi>=15 && bmi<18){
			bmi = 40+(float) ((Math.round(115/3)/10.0)*bmi);
		}else if(bmi>=18 && bmi<22){
			bmi = 155+(float) ((Math.round(70/4)/10.0)*bmi);
		}else if(bmi>=22){
			bmi = 225+(float) (bmi/45);
			if(bmi>270){
				bmi=270;
			}
		}
		return bmi;
	}
	
	public static float myround( float f )   
	 {   
		 float templ = Math.round( f * 10 );   
	      float retd = (float) (templ / 10.0);   
	      return retd;   
	 }  
	
	public static float myround2( float f )   
	 {   
		 float templ = Math.round( f * 100 );   
	      float retd = (float) (templ / 100.0);   
	      return retd;   
	 }  

	
	public static String dateTimeChange(Date source) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String changeTime = format.format(source);
		return changeTime;
	}
	
	/**
	 * 
	 * @param time
	 * @return
	 */
	public static Date stringToTime(String time) {
		SimpleDateFormat df2=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date2 = null;
		try {
			 date2 = df2.parse(time);

		} catch (ParseException e) {
			
		}
		return date2;
	}
	
	public static String kgToLB(float kg){
		float lb = (float) (kg*2.2046);
		lb = myround(lb);
		DecimalFormat myformat = new  DecimalFormat("######.0"); 
		return myformat.format(lb); 
	}
	
	public static String kgToStLb(float kg){
		float st = (float) (kg*0.1575);
		int stint = (int) (st/1);
		int lb = (int) (((st%1)*16)/1);
		return stint+":"+lb; 
	}
	
	public static String kgToStLbForScaleFat(float kg){
		float st = (float) (kg*0.1575);
		int stint = (int) (st/1);
		int lbint = (int) ((st-stint)*14/1);
		float lbfloat = ((st-stint)*14%1);
		
		String fen = "";
		if(lbfloat>=0 && lbfloat<0.125){
			
		}else if(lbfloat>=0.125 && lbfloat<0.375){
			fen = "1/4";
		}else if(lbfloat>=0.375 && lbfloat<0.625){
			fen = "1/2";
		}else if(lbfloat>=0.625 && lbfloat<0.875){
			fen = "3/4";
		}else if(lbfloat>=0.875 && lbfloat<1){
			stint = stint+1;
			fen = "";
		}
		return stint+":"+lbint+"("+fen+")"; 
	}
	
	public static String kgToLbForScaleBaby(float kg){
		float lb = (float) (kg*2.2046);
		int lbint = (int) (lb/1);
		float lbloat = (lb%1)*16;
		lbloat = myround(lbloat);
		return lbint+":"+lbloat; 
	}
	
	public static String lbToST(float lb){
		float st = (float) (lb*14);
		DecimalFormat myformat = new  DecimalFormat("######.00"); 
		return myformat.format(st); 
	}
	
	public static String lbToKg(float lb){
		float kg = (float) (lb*2.2046);
		DecimalFormat myformat = new  DecimalFormat("######.00"); 
		return myformat.format(kg); 
	}
	
	/**
	 * 
	 * @param weight kg
	 * @param height  m
	 */
	public static String countBMI(float weight,float height){
		float bmi = weight /(height*height);
		DecimalFormat myformat = new  DecimalFormat("######.0");
		return myformat.format(bmi); 
	}
}
